<div class="main-navbar sticky-top bg-white">
  <!-- Main Navbar -->
  <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
    <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
      <div class="input-group input-group-seamless ml-3">
        <div class="input-group-prepend">
          <div class="input-group-text">
            <i class="fas fa-search"></i>
          </div>
        </div>
        <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
    </form>
    <ul class="navbar-nav border-left flex-row ">
      <li class="nav-item border-right dropdown notifications">
        <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="nav-link-icon__wrapper">
            <i class="material-icons">&#xE7F4;</i>
            <span class="badge badge-pill badge-danger count_notification"></span>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
          <div class="load_notifications_bar"></div>
          <a class="dropdown-item notification__all text-center" href="{{url('admin/view/notifications')}}"> View all Notifications </a>
        </div>
      </li>

      <li class="nav-item border-right dropdown notifications">
        <a class="nav-link nav-link-icon text-center" title="Find & Replace typo error" href="{{url('admin/notifications')}}" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="nav-link-icon__wrapper">
            <i class="material-icons">find_in_page</i>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" title="Search & Correct typo errors" href="{{ url("admin/notifications") }}">
            <div class="notification__icon-wrapper">
              <div class="notification__icon">
                <i class="material-icons">&#xE8D1;</i>
              </div>
            </div>
            <div class="notification__content">
              <span class="notification__category">Fixed Errors</span>
              <p>Fixed typo errors on Data Captured</p>
            </div>
          </a>
        </div>
      </li>

      <li class="nav-item border-right notifications">
        <a class="nav-link nav-link-icon text-center" title="Google analytics" href="{{ url('admin/analytics') }}">
          <div class="nav-link-icon__wrapper">
            <i class="material-icons">bubble_chart</i>
          </div>
        </a>
      </li>

      <li class="nav-item border-right notifications">
        <a class="nav-link nav-link-icon text-center" title="View groups data" href="{{ url('admin/users/group') }}">
          <div class="nav-link-icon__wrapper">
            <i class="material-icons">supervised_user_circle</i>
          </div>
        </a>
      </li>

      <li class="nav-item border-right notifications">
        <a class="nav-link nav-link-icon text-center" title="Fixed duplicated data" href="{{ url('admin/view/duplicates') }}">
          <div class="nav-link-icon__wrapper">
            <i class="material-icons">person_add_disabled</i>
          </div>
        </a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img class="user-avatar rounded-circle mr-2" src="{{asset('admin-assets/images/avatars/0.jpg')}}" alt="User Avatar">
          <span class="d-none d-md-inline-block">{{ Auth::guard('admin')->user()->email }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-small">
          <a class="dropdown-item" href="{{url('admin/settings')}}">
            <i class="material-icons">&#xE7FD;</i> Profile</a>
          <a class="dropdown-item" href="{{url('admin/blogs')}}">
            <i class="material-icons">vertical_split</i> Blog Posts</a>
          <a class="dropdown-item" href="{{url('admin/blogs')}}">
            <i class="material-icons">note_add</i> Add New Post</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item text-danger" href="{{url('admin/logout/request')}}">
            <i class="material-icons text-danger">&#xE879;</i> Logout </a>
        </div>
      </li>
    </ul>
    <nav class="nav">
      <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
        <i class="material-icons">&#xE5D2;</i>
      </a>
    </nav>
  </nav>
</div>