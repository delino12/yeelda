@extends('layouts.plain')

@section('title')
	YEELDA | Search all farmers
@endsection

@section('contents')
	<style>
        #pie_chart_div {
          width: 100%;
          height: 570px;
        }
        #bar_chart_div {
          width: 100%;
          height: 290px;
        }

        #landarea_chart_div {
            width: 100%;
            height: 370px;
        }

        #single_chart_div {
            width: 150%;
            height: 370px;
        }

        #land_map_area {
            width: 100%;
            height: 370px;
        }

        #clusters_chart_div {
            width: 100%;
            height: 600px;
        }

        #clusters_bar_chart_div {
            width: 100%;
            height: 400px;
        }

        #produce_chart_div {
            width: 150%;
            height: 400px;
        }

        tspan {
            color:#FFF;
        }                    
    </style>

	{{-- include login form --}}
	@include('web-components.reports-card')

@endsection

@section('scripts')
	<script src="{{ asset('amcharts/js/core.js') }}"></script>
    <script src="{{ asset('amcharts/js/charts.js') }}"></script>
    <script src="{{ asset('amcharts/js/maps.js') }}"></script>
    <script src="{{ asset('amcharts/js/animated.js') }}"></script>
    <script type="text/javascript">
        // loadAllStats();
        getAllStates();
        initMapAreaForStates();
        getLgas();
        getClusters();
        getEstimatedVolume();
        // initBarChartForReport();

        function loadAllStats() {
            $.get('{{ url("admin/fetch/overall/statistics") }}', function(data) {
                $(".total_male").html(data.total_male);
                $(".total_female").html(data.total_female);
                $(".highest_planted").html(data.highest_planted);
                $(".most_preferred").html(data.most_preferred.crop);
                $(".least_preferred").html(data.least_preferred.crop);
                // initPieChart(data.series);
                initBarChart(data.series);
            });
        }

        function loadLandStats() {
            $.get('{{ url("admin/fetch/geo/landarea") }}', function(data) {
                // console log data
                $("#highest_hectres").html(data.highest_hectres + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_hectares_by.owner} <br />
                        State: ${data.highest_hectares_by.state} <br />
                        LGA: ${data.highest_hectares_by.lga} <br />
                    </span>
                `);
                $("#highest_acres").html(data.highest_acres + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_acres_by.owner} <br />
                        State: ${data.highest_acres_by.state} <br />
                        LGA: ${data.highest_acres_by.lga} <br />
                    </span>
                `);
                $("#highest_plots").html(data.highest_plots + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_plots_by.owner} <br />
                        State: ${data.highest_plots_by.state} <br />
                        LGA: ${data.highest_plots_by.lga} <br />
                    </span>
                `);
                $("#highest_greenhouse").html(data.highest_greenhouse + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_greenhouse_by.owner} <br />
                        State: ${data.highest_greenhouse_by.state} <br />
                        LGA: ${data.highest_greenhouse_by.lga} <br />
                    </span>
                `);

                var series =[
                  {
                    farmland: "Hectares",
                    size: data.highest_hectres
                  },
                  {
                    farmland: "Acres",
                    size: data.highest_acres
                  },
                  {
                    farmland: "Plots",
                    size: data.highest_plots
                  },
                  {
                    farmland: "Greenhouse",
                    size: data.highest_greenhouse
                  },
                ];

                initDonut(series);
            });
        }

        function initPieChart(series) {
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create("pie_chart_div", am4charts.PieChart);
            chart.data = series;

            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "result";
            pieSeries.dataFields.category = "crop";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;

            pieSeries.hiddenState.properties.opacity = 1;
            pieSeries.hiddenState.properties.endAngle = -90;
            pieSeries.hiddenState.properties.startAngle = -90;
        }

        function initBarChart(series) {
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create("bar_chart_div", am4charts.XYChart);
            chart.data = series;

            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "crop";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                if (target.dataItem && target.dataItem.index & 2 == 2) {
                    return dy + 25;
                }
                return dy;
            });

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "result";
            series.dataFields.categoryX = "crop";
            series.name = "result";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;

            // Enable export
            chart.exporting.menu = new am4core.ExportMenu();
        }

        function initMapAreaForStates() {
            $(".loading-bar").show();
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            window.onload = function() {

                /**
                 * This demo uses our own method of determining user's location
                 * It is not public web service that you can use
                 * You'll need to find your own. We recommend http://www.maxmind.com
                 */
                var defaultMap = "nigeriaLow";
                var countryMaps = {
                    "AL": [ "albaniaLow" ],
                    "DZ": [ "algeriaLow" ],
                    "AD": [ "andorraLow" ],
                    "AO": [ "angolaLow" ],
                    "AR": [ "argentinaLow" ],
                    "AM": [ "armeniaLow" ],
                    "AU": [ "australiaLow" ],
                    "AT": [ "austriaLow" ],
                    "AZ": [ "azerbaijanLow" ],
                    "BH": [ "bahrainLow" ],
                    "BD": [ "bangladeshLow" ],
                    "BY": [ "belarusLow" ],
                    "BE": [ "belgiumLow" ],
                    "BZ": [ "belizeLow" ],
                    "BM": [ "bermudaLow" ],
                    "BT": [ "bhutanLow" ],
                    "BO": [ "boliviaLow" ],
                    "BW": [ "botswanaLow" ],
                    "BR": [ "brazilLow" ],
                    "BN": [ "bruneiDarussalamLow" ],
                    "BG": [ "bulgariaLow" ],
                    "BF": [ "burkinaFasoLow" ],
                    "BI": [ "burundiLow" ],
                    "KH": [ "cambodiaLow" ],
                    "CM": [ "cameroonLow" ],
                    "CA": [ "canandaLow" ],
                    "CV": [ "capeVerdeLow" ],
                    "CF": [ "centralAfricanRepublicLow" ],
                    "TD": [ "chadLow" ],
                    "CL": [ "chileLow" ],
                    "CN": [ "chinaLow" ],
                    "CO": [ "colombiaLow" ],
                    "CD": [ "congoDRLow" ],
                    "CG": [ "congoLow" ],
                    "CR": [ "costaRicaLow" ],
                    "HR": [ "croatiaLow" ],
                    "CZ": [ "czechRepublicLow" ],
                    "DK": [ "denmarkLow" ],
                    "DJ": [ "djiboutiLow" ],
                    "DO": [ "dominicanRepublicLow" ],
                    "EC": [ "ecuadorLow" ],
                    "EG": [ "egyptLow" ],
                    "SV": [ "elSalvadorLow" ],
                    "EE": [ "estoniaLow" ],
                    "SZ": [ "eswatiniLow" ],
                    "FO": [ "faroeIslandsLow" ],
                    "FI": [ "finlandLow" ],
                    "FR": [ "franceLow" ],
                    "GF": [ "frenchGuianaLow" ],
                    "GE": [ "georgiaLow" ],
                    "DE": [ "germanyLow" ],
                    "GR": [ "greeceLow" ],
                    "GL": [ "greenlandLow" ],
                    "GN": [ "guineaLow" ],
                    "HN": [ "hondurasLow" ],
                    "HK": [ "hongKongLow" ],
                    "HU": [ "hungaryLow" ],
                    "IS": [ "icelandLow" ],
                    "IN": [ "indiaLow" ],
                    "GB": [ "ukLow" ],
                    "IE": [ "irelandLow" ],
                    "IL": [ "israelLow" ],
                    "PS": [ "palestineLow" ],
                    "MT": [ "italyLow" ],
                    "SM": [ "italyLow" ],
                    "VA": [ "italyLow" ],
                    "IT": [ "italyLow" ],
                    "JP": [ "japanLow" ],
                    "MX": [ "mexicoLow" ],
                    "RU": [ "russiaCrimeaLow" ],
                    "KR": [ "southKoreaLow" ],
                    "ES": [ "spainLow" ],
                    "US": [ "usaAlbersLow" ],
                    "NG": [ "nigeriaLow" ],
                };

                let geo = {
                    country_code: "NG",
                    country_name: "Nigeria"
                }
                  
                // calculate which map to be used
                var currentMap = defaultMap;
                var title = "";
                if ( countryMaps[ geo.country_code ] !== undefined ) {
                    currentMap = countryMaps[ geo.country_code ][ 0 ];
                    // add country title
                    if ( geo.country_name ) {
                      title = geo.country_name;
                    }
                }
                  
                // Create map instance
                var chart = am4core.create("land_map_area", am4maps.MapChart);  
                    chart.titles.create().text = title;
                    // Set map definition      
                chart.geodataSource.url = "{{ url('admin/count/users/by/states') }}";
                chart.geodataSource.events.on("parseended", function(ev) {
                    $(".loading-bar").hide();
                    var data = [];
                    for(var i = 0; i < ev.target.data.features.length; i++) {
                        var total_users = ev.target.data.features[i].properties.total_users;
                        var child_data = {
                            id: ev.target.data.features[i].id,
                            value: total_users
                        }

                        data.push(child_data);
                    }
                    polygonSeries.data = data;
                });

                // Set projection
                chart.projection = new am4maps.projections.Miller();

                // Create map polygon series
                var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

                //Set min/max fill color for each area
                polygonSeries.heatRules.push({
                    property: "fill",
                    target: polygonSeries.mapPolygons.template,
                    min: chart.colors.getIndex(1).brighten(1),
                    max: chart.colors.getIndex(1).brighten(-0.3)
                });

                // Make map load polygon data (state shapes and names) from GeoJSON
                polygonSeries.useGeodata = true;

                // Set up heat legend
                let heatLegend = chart.createChild(am4maps.HeatLegend);
                heatLegend.series = polygonSeries;
                heatLegend.align = "right";
                heatLegend.width = am4core.percent(25);
                heatLegend.marginRight = am4core.percent(4);
                heatLegend.minValue = 0;
                heatLegend.maxValue = 40000000;

                // Set up custom heat map legend labels using axis ranges
                var minRange = heatLegend.valueAxis.axisRanges.create();
                minRange.value = heatLegend.minValue;
                minRange.label.text = "Min";
                var maxRange = heatLegend.valueAxis.axisRanges.create();
                maxRange.value = heatLegend.maxValue;
                maxRange.label.text = "Max";

                // Blank out internal heat legend value axis labels
                heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
                    return "";
                });

                // Configure series tooltip
                var polygonTemplate = polygonSeries.mapPolygons.template;
                polygonTemplate.tooltipText = "{name}: {value}";

                // Create hover state and set alternative fill color
                var hs = polygonTemplate.states.create("hover");
                hs.properties.fill = chart.colors.getIndex(1).brighten(-0.5);
            }
        }

        function initDonut(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("landarea_chart_div", am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.legend = new am4charts.Legend();

            chart.data = series;
            chart.innerRadius = 100;

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value     = "size";
            series.dataFields.category  = "farmland";
        }

        function getAllReports() {
            var states  = $("#sort_state").val();
            var lgas    = $("#sort_lgas").val();
            var cluster = $("#sort_clusters").val();

            var params = {
                states: states,
                lgas: lgas,
                cluster: cluster
            };

            // process reports via days
            $.get('{{url("admin/get/reports")}}', params, function(data, textStatus, xhr) {
               // console log data
               console.log(data);
            });
        }

        function getAllStates() {
            $.get('{{url("admin/get/all/states")}}', function(data) {
                $("#sort_state").html("");
                $("#sort_state").append(`
                    <option value="Ogun">Ogun</option>
                `);
                $.each(data, function(index, val) {
                    $("#sort_state").append(`
                        <option value="${val.name}">${val.name}</option>
                    `);
                });
            });
        }

        function getLgas() {
            // body...
            var states = $("#sort_state").val();

            if(!states){
                states = "Ogun";
            }

            $(".target-report-title").html(`${states} State`);

            var params = {states: states};
            $.get('{{url("admin/get/state/lgas")}}', params, function(data) {
                $("#sort_lgas").html("");
                $.each(data, function(index, val) {
                    $("#sort_lgas").append(`
                        <option value="${val}">${val}</option>
                    `);
                });
            });

            // find all lgas
            getReportByState(states);
        }

        function getReportByState(state) {
            var params = {states: state};
            $.get('{{url("admin/get/reports/by/state")}}', params, function(data) {
                initBarChartForStates(data);
            });
        }

        function getReportByLga(state, lga) {
            var params = {
                state: state,
                lga: lga
            }

            $.get('{{url("admin/get/clusters/reports")}}', params, function(data) {
                initDonutChartForLga(data);
                initBarChartForLga(data);
            });
        }

        function getClusters() {
            // body...
            var states = $("#sort_state").val();
            var lgas = $("#sort_lgas").val();

            if(!states){
                states = "Ogun";
            }

            if(!lgas){
                lgas = "Abeokuta North";
            }

            $(".target-report-title-cluster").html(`Data from ${states} State in ${lgas}`);

            var params = {
                states: states,
                lgas: lgas
            };
            $.get('{{url("admin/get/state/lgas/clusters")}}', params, function(data) {
                $("#sort_clusters").html("");
                $.each(data, function(index, val) {
                    $("#sort_clusters").append(`
                        <option value="${val.cluster}">${val.cluster}</option>
                    `);
                });
            });

            getReportByLga(states, lgas);
        }

        function getClusterVillages() {
            $("#villages_bar_chart").html("Loading...");
            var state   = $("#sort_state").val();
            var lga     = $("#sort_lgas").val();
            var cluster = $("#sort_clusters").val();

            if(!state){
                state = "Ogun";
            }

            if(!lga){
                lga = "Abeokuta North";
            }

            var params = {state, lga, cluster}

            $(".target-title-cluster").html(cluster);

            $.get('{{url("admin/get/cluster/villages")}}', params, function(data) {
                // console.log(data);
                // initBarChartForVillages(data)
            });
        }

        function getEstimatedVolume() {
            var state = $("#sort_state").val();

            if(!state){
                state = "Ogun";
            }

            $.get('{{url("admin/get/estimate/volume")}}', {states: state}, function(data) {
                initBarChartForProduce(data);
            });
        }

        function getTotalUsersByState(state) {
            return new Promise(function(resolve, reject){
                $.get('{{url("admin/count/users/by/states")}}', {state: state}, function(val) {
                    resolve(val);
                }).fail(function(err){
                    reject(err);
                });
            });
        }

        function initDonutChartForLga(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("clusters_chart_div", am4charts.PieChart);

            // Add data
            chart.data = series;

            // Set inner radius
            chart.innerRadius = am4core.percent(50);

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "farmers";
            pieSeries.dataFields.category = "cluster";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;

            // This creates initial animation
            pieSeries.hiddenState.properties.opacity = 1;
            pieSeries.hiddenState.properties.endAngle = -90;
            pieSeries.hiddenState.properties.startAngle = -90;
        }

        function initBarChartForLga(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("clusters_bar_chart_div", am4charts.XYChart);

            // Add percent sign to all numbers
            chart.numberFormatter.numberFormat = "#";

            chart.data = series;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "cluster";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Farmers Captured";
            valueAxis.title.fontWeight = 400;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "area";
            series.dataFields.categoryX = "cluster";
            series.clustered = false;
            series.columns.template.fill = am4core.color("#87B646");
            series.columns.template.width = am4core.percent(80);
            series.columns.template.strokeWidth = 0;
            series.tooltipText = "Arable land in {categoryX}: [bold]{valueY}[/]ha";

            var series2 = chart.series.push(new am4charts.ColumnSeries());
            series2.dataFields.valueY = "farmers";
            series2.dataFields.categoryX = "cluster";
            series2.clustered = false;
            series2.columns.template.fill = am4core.color("#E7AC51");
            series2.columns.template.width = am4core.percent(30);
            series2.columns.template.strokeWidth = 0;
            series2.tooltipText = "Farmers in {categoryX}: [bold]{valueY}[/]";

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.disabled = true;
            chart.cursor.lineY.disabled = true;

            // Enable export
            chart.exporting.menu = new am4core.ExportMenu();
        }

        function initBarChartForProduce(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("produce_chart_div", am4charts.XYChart);

            // Add percent sign to all numbers
            chart.numberFormatter.numberFormat = "#";


            chart.data = series;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "local";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 5;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Cassava Production Estimated Volume";
            valueAxis.title.fontWeight = 400;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "volume";
            series.dataFields.categoryX = "local";
            series.columns.template.fill = am4core.color("#28CC8B");
            series.columns.template.width = am4core.percent(40);
            series.columns.template.strokeWidth = 0;
            series.clustered = false;
            series.tooltipText = "Estimated Volume in {categoryX}: [bold]{valueY}[/]tons";

            var series2 = chart.series.push(new am4charts.ColumnSeries());
            series2.dataFields.valueY = "area";
            series2.dataFields.categoryX = "local";
            series2.clustered = false;
            series2.columns.template.fill = am4core.color("#E7AC59");
            series2.columns.template.width = am4core.percent(30);
            series2.columns.template.strokeWidth = 0;
            series2.tooltipText = "Land arable in {categoryX}: [bold]{valueY}[/]ha";

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.disabled = true;
            chart.cursor.lineY.disabled = true;

            // Enable export
            chart.exporting.menu = new am4core.ExportMenu();
        }

        function initBarChartForStates(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("single_chart_div", am4charts.XYChart);

            // Add percent sign to all numbers
            chart.numberFormatter.numberFormat = "#";


            chart.data = series;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "local";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 5;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Farmers Captured";
            valueAxis.title.fontWeight = 400;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "area";
            series.dataFields.categoryX = "local";
            series.columns.template.fill = am4core.color("#87B646");
            series.columns.template.width = am4core.percent(30);
            series.columns.template.strokeWidth = 0;
            series.clustered = false;
            series.tooltipText = "Arable land in {categoryX}: [bold]{valueY}[/]ha";

            var series2 = chart.series.push(new am4charts.ColumnSeries());
            series2.dataFields.valueY = "farmers";
            series2.dataFields.categoryX = "local";
            series2.clustered = false;
            series2.columns.template.fill = am4core.color("#2CA7CB");
            series2.columns.template.width = am4core.percent(20);
            series2.columns.template.strokeWidth = 0;
            series2.tooltipText = "Farmers in {categoryX}: [bold]{valueY}[/]";

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.disabled = true;
            chart.cursor.lineY.disabled = true;

            // Enable export
            chart.exporting.menu = new am4core.ExportMenu();
        }
    </script>
@endsection
