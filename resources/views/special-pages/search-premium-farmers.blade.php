@extends('layouts.web-skin')

@section('title')
	YEELDA | Search all farmers
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.farmers-card')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	<script type="text/javascript">
		
		loadRecord(limit = 50, offset = 0)

	    function loadRecord(limit, offset) {
	        $.get('{{url("fetch/all/premium/farmers")}}', {limit, offset}, function(data) {
	            $(".y-find-farmers").html("");
	            
	            $.each(data.data, function(index, val) {
	                $(".y-find-farmers").append(`
	                    <div class="col-md-3">
							<div class="y-farmer-profile-card">
								<div class="y-farmer-image text-center">
									<img src="${val.avatar}" class="img-rounded " width="120" height="auto">
								</div>
								<div class="y-farmer-initials text-center">
									<span><strong>${val.name}</strong> | Farmer</span>
									<br />
									<span class="small"><b>Grows:</b> ${val.crops}</span>
								</div>
								<div class="y-farmer-addon text-center">
									<a href="#" style="color: #2CA8C9;">
										<i class="fa fa-envelope" style="font-size: 18px;"></i>
									</a>
								</div>
							</div>
							<br />
						</div>
	                `);
	            });

	            $(".paginate-div").html(`
	                <button class="btn btn-default" onclick="goPrev(${limit}, ${offset}, ${data.total})">Prev</button>
	                <button class="btn btn-default pull-right" onclick="goNext(${limit}, ${offset}, ${data.total})">Next</button>
	            `);
	        });
	    }

	    function goPrev(limit, offset, total) {
	        if(offset !== 0){
	            offset = (offset - limit);
	            loadRecord(limit, offset);
	        }
	    }

	    function goNext(limit, offset, total) {
	        // console log data
	        console.log(total);
	        console.log(offset);

	        if(offset <= total){
	            offset = (offset + limit);
	            loadRecord(limit, offset);
	        }
	    }
	</script>
@endsection
