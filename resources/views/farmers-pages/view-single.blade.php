@extends('layouts.main')

{{-- title --}}
@section('title')
	Products | Yeelda
@endsection

{{-- contents sections --}}
@section('contents')
  <div class="container-fluid">
  	<!-- Breadcrumbs-->
  	<ol class="breadcrumb">
  		<li class="breadcrumb-item">
  	  		<a href="/farmer/dashboard">{{ Auth::user()->name }}</a>
  		</li>
  		<li class="breadcrumb-item active">Products</li>
  	</ol>

    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <br />
          <h1 class="lead">Farm Produce</h1>
          <div class="row">
            {{-- <div class="card"> --}}
              {{-- <div class="card-body"> --}}
    					 <div class="load-products"></div>
              {{-- </div> --}}
    				{{-- </div> --}}
  	      </div>
  	    </div>
  	</div>
  </div>
  <br /><br />
 
  <script type="text/javascript">
    var pid = '{{ $id }}';
    $.get('/load/single/produce/?id='+pid, function (value){
      if (value.product_image.indexOf('YEELDA-') > -1){
        var urlToImage = `/uploads/farmers/${value.product_image}`;
      }else{
        var urlToImage = value.product_image;
      }

      $(".load-products").append(`
        <div class="col-md-12">
          <div class="card h-100">
            <a href="/product/details/`+value.id+`">
            <img class="card-img-top" src="${urlToImage}" width="350" height="300" alt=""></a>
            <div class="card-body">
              <h4 class="small">
                <a href="/product/details/`+value.id+`">
                <i class="fa fa-user"></i> `+value.owner_name+` (Farmer)
                </a>
              </h4>
              <h5 class="small"> &#8358; `+value.product_total+`</h5>
              <p class="small">
                <table class="table small">
                  <tr>
                    <td>Product</td>
                    <td>`+value.product_name+`</td>
                  </tr>
                  <tr>
                    <td>Unit price</td>
                    <td> <b>&#8358;`+value.product_price+`</b> `+value.product_size_no+` QTY</td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td><b> &#8358; `+value.product_total+`</b></td>
                  </tr>
                  <tr>
                    <td>Location</td>
                    <td>`+value.product_location+`</td>
                  </tr>
                  <tr>
                    <td>Weight</td>
                    <td>`+value.product_size_type+` `+value.product_size_no+` </td>
                  </tr>
                  
                  <tr>
                    <td>Contact</td>
                    <td>`+value.owner_contact+`</td>
                  </tr>
                </table>
              </p>
            </div>
            <div class="card-footer">
              <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
            </div>
          </div>
        </div>

      `);

      $(".farmer-info").append(`
        <tr>
          <td>Name: </td>
          <td>`+value.owner_name+`</td>
        </tr>

        <tr>
          <td>Email: </td>
          <td>`+value.owner_email+`</td>
        </tr>

        <tr>
          <td>Gender: </td>
          <td>`+value.owner_gender+`</td>
        </tr>

        <tr>
          <td>Mobile: </td>
          <td>`+value.owner_contact+`</td>
        </tr>

        <tr>
          <td>Office: </td>
          <td>`+value.owner_office+`</td>
        </tr>

        <tr>
          <td>Address: </td>
          <td>`+value.owner_address+`</td>
        </tr>

        <tr>
          <td>Postal: </td>
          <td>`+value.owner_zipcode+`</td>
        </tr>

        <tr>
          <td>Nationality: </td>
          <td>`+value.owner_country+`</td>
        </tr>
      `);
    });
  </script>
@endsection