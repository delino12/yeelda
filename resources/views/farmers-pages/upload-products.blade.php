@extends('layouts.main')

@section('title')
	Yeelda | Add Products
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/farmer/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Products</li>
		</ol>

		@if(session('update_status'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 480px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('update_status') }}</p>
			</div>
		@endif

		@if(session('error_status'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 480px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_status') }}</p>
			</div>
		@endif

		<div class="row">
			<div class="col-md-6 small">
				<div class="card">
					<div class="card-body">
				  		<h1 class="lead"><img src="/images/icon-set/produce.png"> Upload Product </h1>
				 		<p class="small">
				 			<b>Note</b>: 
				 			Please make sure products is available at the specified location filled!
				 		</p>
				 		<form method="post" action="{{url('upload/farmer/products')}}">
				 			{{ csrf_field() }}
							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Status</label>
									</div>
									<div class="col-sm-10">
										<select class="form-control" onchange="return addSeasonalInput()" name="product_status">
											<option value="ready">Ready (Harvested)</option>
											<option value="seasonal">Seasonal (Future Harvest)</option>
										</select>
									</div>
								</div>
							</div>

							{{-- for season planing --}}
							<div id="season-input" style="display: none;"> 
								<div class="form-group">
									<div class="row">
										<div class="col-sm-2">
											Duration <br /> Details
										</div>
										<div class="col-sm-10">
											<div class="row">
												<div class="col-xl-6">
													<label>Start Date</label>
													<input type="date" class="form-control" name="start_date">
												</div>
												<div class="col-xl-6">
													<label>Harvest Date</label>
													<input type="date" class="form-control" name="end_date">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Product</label>
									</div>
									<div class="col-sm-10" id="the-basics">
										<input type="text" class="typeahead form-control" id="p-name" name="product_name" placeholder="Eg. Rice, Beans, Maize, Millets etc" required="" value="{{ old('product_name') }}">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Size</label>
									</div>
									<div class="col-sm-5">
										<input type="number" class="form-control" placeholder="How many?" name="product_size_no" required="" value="{{ old('product_size_no') }}">
									</div>
									<div class="col-sm-5">
										<select class="form-control" id="measure_size_type" name="product_size_type">
											{{-- <option value="tons | (907.18kg)">Ton (907.18 kg)</option>
											<option value="short-tons | (1,016.05kg)">Short Ton (1,016.05kg)</option>
											<option value="metric-tons | (1,000kg)">Metric Ton (1,000kg)</option> --}}
											<option value="tons | (1000kg)">Ton (1000kg)</option>
											<option value="kg | (1000g)">Kg (1000g)</option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Price</label>
									</div>
									<div class="col-sm-5">
										<div class="input-group">
											<span class="input-group-addon">
												&#8358;
											</span>
											<input type="text" pattern="[0-9]*" name="product_price" class="form-control" placeholder="00.00" required="" value="{{ old('product_price') }}">
										</div>
									</div>
									<div class="col-sm-4">
										<span class="text-danger">Note:</span>
										<span style="font-size: 12px;">Price is determine by size per Unit.</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Location</label>
									</div>
									<div class="col-sm-10">
										<textarea class="form-control" name="product_location" cols="20" rows="2" placeholder="Eg. 16a, farm road...">{{ old('product_location') }}</textarea>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>State</label>
									</div>
									<div class="col-sm-10">
										<select class="form-control" name="product_state" id="state">
											<option value="" selected="selected">- Select -</option>
											<option value="Abuja FCT">Abuja FCT</option>
											<option value="Abia">Abia</option>
											<option value="Adamawa">Adamawa</option>
											<option value="Akwa Ibom">Akwa Ibom</option>
											<option value="Anambra">Anambra</option>
											<option value="Bauchi">Bauchi</option>
											<option value="Bayelsa">Bayelsa</option>
											<option value="Benue">Benue</option>
											<option value="Borno">Borno</option>
											<option value="Cross River">Cross River</option>
											<option value="Delta">Delta</option>
											<option value="Ebonyi">Ebonyi</option>
											<option value="Edo">Edo</option>
											<option value="Ekiti">Ekiti</option>
											<option value="Enugu">Enugu</option>
											<option value="Gombe">Gombe</option>
											<option value="Imo">Imo</option>
											<option value="Jigawa">Jigawa</option>
											<option value="Kaduna">Kaduna</option>
											<option value="Kano">Kano</option>
											<option value="Katsina">Katsina</option>
											<option value="Kebbi">Kebbi</option>
											<option value="Kogi">Kogi</option>
											<option value="Kwara">Kwara</option>
											<option value="Lagos">Lagos</option>
											<option value="Nassarawa">Nassarawa</option>
											<option value="Niger">Niger</option>
											<option value="Ogun">Ogun</option>
											<option value="Ondo">Ondo</option>
											<option value="Osun">Osun</option>
											<option value="Oyo">Oyo</option>
											<option value="Plateau">Plateau</option>
											<option value="Rivers">Rivers</option>
											<option value="Sokoto">Sokoto</option>
											<option value="Taraba">Taraba</option>
											<option value="Yobe">Yobe</option>
											<option value="Zamfara">Zamfara</option>
											<option value="Outside Nigeria">Outside Nigeria</option>
							            </select>
									</div>
								</div>
							</div>

							<hr />
							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Images</label>
									</div>
									<div class="col-sm-10">
										<input type="hidden" name="product_image" id="files">
										<a href="javascript:void(0);" id="upload_widget_opener" class="btn btn-link">
											<i class="fa fa-camera"></i> Add Image
										</a>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-2"></div>
									<div class="col-sm-10">
										<div class="preview-image"></div>
									</div>
								</div>								
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Note</label>
									</div>
									<div class="col-sm-10">
										<textarea class="form-control" rows="2" cols="20" name="product_note" placeholder="describe the product.....">{{ old('product_note') }}</textarea>
									</div>
								</div>
							</div>

							<br /> <br />
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12 text-center">
										<button class="btn btn-success">Upload Produce</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
						<h1 class="lead"><img src="/images/icon-set/produce.png"> Produce </h1>
						<table class="table small">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Products</th>
									<th>Qty</th>
									<th>Price (&#8358;)</th>
									<th>Total (&#8358;)</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="farm-products"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<br />
	</div>
	
@endsection

@section('scripts')
	<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>  
  	<script type="text/javascript">
    	document.getElementById("upload_widget_opener").addEventListener("click", function() {
      		cloudinary.openUploadWidget({ 
	        	cloud_name: 'delino12', 
	        	upload_preset: 'znwx0uee',
	        	cropping: true, 
	        	folder: 'yeelda-produces'
	      	}, 
	      	function(error, result) { 
	        	if(error){
	          		swal(
	            		"oops",
	            		"Error trying to upload image, select image and try again",
	            		"error"
	          		);
	        	}else{
	          		$("#files").val(result[0].url);
	          		$(".preview-image").html(`
	          			<img src="${result[0].url}" width="120" height="auto" />
	          		`);
		        }
		    });
	    }, false);

		// verify number inputs
		// Select your input element.
		var numInput = document.querySelector('input[name=product_size_no]');

		// Listen for input event on numInput.
		numInput.addEventListener('input', function(){
		    // Let's match only digits.
		    var num = this.value.match(/^\d+$/);
		    if (num === null) {
		        // If we have no match, value will be empty.
		        this.value = "";
		    }
		}, false);

		// load hint products
		$.get('/load/products/hints', function(data) {
			// console.log(data);
			// scan and filtered match strings
			var substringMatcher = function(strs) {
			  	return function findMatches(q, cb) {
				    var matches, substringRegex;

				    // an array that will be populated with substring matches
				    matches = [];

				    // regex used to determine if a string contains the substring `q`
				    substrRegex = new RegExp(q, 'i');

				    // iterate through the pool of strings and for any string that
				    // contains the substring `q`, add it to the `matches` array
				    $.each(strs, function(i, str) {
				      if (substrRegex.test(str)) {
				        matches.push(str);
				      }
				    });

				    cb(matches);
			  	};
			};
			var states = data;

			$('#the-basics .typeahead').typeahead({
			  hint: true,
			  highlight: true,
			  minLength: 1
			},
			{
			  name: 'states',
			  source: substringMatcher(states)
			});
		});

		// load hint products
		$.get('/load/sizes/hints', function(data) {
			// console.log(data);
			// scan and filtered match strings
			var substringMatcher = function(strs) {
			  	return function findMatches(q, cb) {
				    var matches, substringRegex;

				    // an array that will be populated with substring matches
				    matches = [];

				    // regex used to determine if a string contains the substring `q`
				    substrRegex = new RegExp(q, 'i');

				    // iterate through the pool of strings and for any string that
				    // contains the substring `q`, add it to the `matches` array
				    $.each(strs, function(i, str) {
				      if (substrRegex.test(str)) {
				        matches.push(str);
				      }
				    });

				    cb(matches);
			  	};
			};
			var states = data;

			$('#the-basics-2 .typeahead').typeahead({
			  hint: true,
			  highlight: true,
			  minLength: 1
			},
			{
			  name: 'states',
			  source: substringMatcher(states)
			});
		});

		// show hiddent form
		function addSeasonalInput() {
			// body...
			$('#season-input').toggle();
		}

		// current logged in user
		var email = '{{ Auth::user()->email }}';

		// load farmers uploaded products
		$.get('/farmer/load/products', function (e){
			$('.farm-products').html('');
			var sn = 0;
			$.each(e, function (index, value){
				sn++;
				// console.log(value);
				// fill tables 
				$('.farm-products').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+value.name+`</td>
						<td>`+value.size_no+`</td>
						<td>`+value.price+`</td>
						<td>`+value.total+`</td>
						<td><a href="/farmers/products/`+value.id+`">view</a></td>
					</tr>
				`);
			});
		});

		// load farmers crops
		$.get('/load/farmers/crops/', {email: email}, function(data) {
			/*optional stuff to do after success */
			// console.log(data);
			if(data.status == 'info'){
				$('.add-crop-form').show();
			}

			if(data.status == 'success'){
				var sn = 0;
				$.each(data.crops, function(index, val) {
					/* iterate through array or object */
					sn++;
					$('.load-crops').append(`
						<tr>
							<td>`+sn+`<td>
							<td>`+val.type+`<td>
							<td><a href="javascript:void(0);" onclick="deleteItem('`+val.id+`')"><i class="fa fa-trash"></i> Delete</a><td>
						</tr>
					`);
				});
			}

			// load crops tags
			$.get('/commodities-price/load-index', function(crops) {
				/*optional stuff to do after success */
				// console.log(crops);
				$('.add-crop-form').html();
				$.each(crops, function(index, val) {
					/* iterate through array or object */
					$('.add-crop-form').append(`
						<button class="y-crops col-sm-2" onclick="addCrop('`+val.asset+`')">`+val.asset+`</button>
					`);
				});
			});
		});

		// add crops
		function addCrop(x) {
			// data 
			var type 	= x;
			var note    = 'this is a note..';
			var token 	= '{{ csrf_token() }}';

			// data to json
			var data = {
				_token:token,
				type:type,
				note:note
			};

			// json to array
			$.ajax({
				url: '/add/crops/',
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data){
					console.log(data);
					// alert(type+ ' crop added successfully');
					if(data.status == 'success'){
						$('.delete_msg').html(`
							<span class="text-success">`+data.message+`</span>
						`);
					}
					reloadCropDiv(data.message);
				},
				error: function (data){
					console.error(data);
					alert('Error, Fail to add crops ');
					if(data.status == 'error'){
						// alert('delete successfully !');
						$('.delete_msg').html(`
							<span class="text-danger">`+data.message+`</span>
						`);
					}
				}
			});
		}

		// del crops
		function deleteItem(x) {
			// body..
			$.get('/remove/crop/'+email+'/'+x, function(data) {
				/*optional stuff to do after success */
				// console.log(data);
				if(data.status == 'success'){
					// alert('delete successfully !');
					var message = data.message;
					reloadCropDiv(message);
				}
			});
		}

		// function reload crops divs
		function reloadCropDiv(message){

			$('.delete_msg').html(`
				<span class="text-success">`+message+`</span>
			`);

			setTimeout(function(){
				$('.delete_msg').fadeOut();
			}, 2000);

			$.get('/load/farmers/crops/'+email, function(data) {
				/*optional stuff to do after success */
				// console.log(data);
				if(data.status == 'info'){
					$('.add-crop-form').show();
				}

				if(data.status == 'success'){
					var sn = 0;
					$('.load-crops').html('');
					$.each(data.crops, function(index, val) {
						/* iterate through array or object */
						sn++;
						$('.load-crops').append(`
							<tr>
								<td>`+sn+`<td>
								<td>`+val.type+`<td>
								<td><a href="javascript:void(0);" onclick="deleteItem('`+val.id+`')"><i class="fa fa-trash"></i> Delete</a><td>
							</tr>
						`);
					});
				}

				// load crops tags
				$.get('/commodities-price/load-index', function(crops) {
					/*optional stuff to do after success */
					// console.log(crops);
					$('.add-crop-form').html('');
					$.each(crops, function(index, val) {
						/* iterate through array or object */
						$('.add-crop-form').append(`
							<button class="y-crops col-sm-2" onclick="addCrop('`+val.asset+`')">`+val.asset+`</button>
						`);
					});
				});
			});
		}
	</script>
@endsection