@extends('layouts.main')

@section('title')
	Yeelda | Farmers Dashboard
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/farmer/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
		@if(!empty($acct_status))
			<div class="alert alert-danger">
				<p class="text-danger"> {{ $acct_status }}<span class="text-info"> Did not receive any link?</span> 
					<a href="/resend/activation/link/?email={{ Auth::user()->email }}&name={{ Auth::user()->name }}">Resend</a> 
				</p>
			</div>
		@endif
		@if(session('success_msg'))
			<div class="alert alert-success">
				<p class="text-success"> {{ session('success_msg') }}</p>
			</div>
		@endif
		<div class="row">
			{{-- add crop sections --}}
			<div class="col-md-7" data-step="12" data-intro="This section shows crop specialization, you can let your buyers know what you are best at producing...">
				<div class="card" style="height: 390px; overflow-y: auto;">
					<div class="card-body">
						<h1 class="lead">
							<img class="y-img" src="/images/icon-set/produce.png"> Select crop specialization
						</h1>
						<div class="add-crop-form small"></div>
						<br /><br />
					</div>
				</div>
			</div>
			<div class="col-md-5" data-step="13" data-intro="This shows list of all crops you have selected">
				<div class="card" style="height: 390px; overflow-y: auto;">
					<div class="card-body">
						<h1 class="lead">
							<img class="y-img" src="/images/icon-set/farmer.png"> My Crops
						</h1>
						{{-- crop table --}}
						<div class="delete_msg"></div>
						<table class="table small load-crops"></table>
					</div>
				</div>
			</div>
		</div>

		<br />
		<div class="row">
			{{-- <hr class="y-hr" /> --}}
			{{-- end of add crop setion --}}

			{{-- notifications section --}}
			<div class="col-md-3" data-step="14" data-intro="This section shows recent notifications !">
				<div class="card" style="height: 390px; overflow-y: auto;">
					<div class="card-body">
						<h3 class="lead">Notifications</h3>
						<ul class="list-group">
							<div class="notifications small"></div>
						</ul>
					</div>
				</div>
			</div>

			{{-- news feeds sections --}}
			<div class="col-md-4" data-step="15" data-intro="This section show live news feeds from the World of Agriculture all over the world !">
				<div class="card" style="height: 390px; overflow-y: auto;">
					<div class="card-body">
						<h3 class="lead">News Feeds</h3>
						<ul class="list-group">
							{{-- <div class="news small"></div> --}}
							{{-- <div id="rss-feeds" class="small"></div> --}}
							<div class="load-headlines small"></div>
			        		<div class="load-headlines-two small" style="display: none;"></div>
						</ul>
					</div>
				</div>
			</div>

			{{-- inputs sections --}}
			<div class="col-md-5" data-step="16" data-intro="This sections show all uploaded produce, this will be display at the online market store.">
				<div class="card" style="height: 390px; overflow-y: auto;">
					<div class="card-body">
						<h1 class="lead">My Input</h1>
						<table class="table small">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Products</th>
									<th>Qty</th>
									{{-- <th>Weight </th> --}}
									<th>Price (&#8358;)</th>
									<th>Total (&#8358;)</th>
									{{-- <th>Date</th> --}}
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="farm-products"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<br >
	</div>

	{{-- Rss feeds --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script>
		// current logged in user
		var email = '{{ Auth::user()->email }}';
		$.get('/farmer/load/products', function (e){
			$('.farm-products').html('');
			var sn = 0;
			$.each(e, function (index, value){
				sn++;
				// console.log(value);
				// fill tables 
				$('.farm-products').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+value.name+`</td>
						<td>`+value.size_no+`</td>
						<td>`+value.price+`</td>
						<td>`+value.total+`</td>
						<td><a href="/farmers/products/`+value.id+`">view</a></td>
					</tr>
				`);
			});
		});

		// load notifications
		$.get('/load/notifications/'+email, function (e){
			$('.notifications').html('');
			let sn = 0;
			$.each(e, function (index, value){
				sn++;
				// load div
				$('.notifications').append(`
					You have message from <b>`+value.contents.from+`</b> <br /> 
					<p>`+value.contents.body+`</p>
					<a href="/message/service">view now</a> <br />
					<span class="small pull-right">`+value.date+`</span><br /><br />
				`);

				if(sn == 5){
					return false;
				}
			});
		});

		// current logged in user
		$.get('/load/farmers/crops/', function(data) {
			/*optional stuff to do after success */
			// console.log(data);
			if(data.status == 'info'){
				$('.add-crop-form').show();
			}

			if(data.status == 'success'){
				var sn = 0;
				$.each(data.crops, function(index, val) {
					/* iterate through array or object */
					sn++;
					$('.load-crops').append(`
						<tr>
							<td>`+sn+`<td>
							<td>`+val.type+`<td>
							<td><a href="javascript:void(0);" onclick="deleteItem('`+val.id+`')"><i class="fa fa-trash"></i> Delete</a><td>
						</tr>
					`);
				});
			}

			// load crops tags
			$.get('/commodities-price/load-index', function(crops) {
				/*optional stuff to do after success */
				// console.log(crops);
				$('.add-crop-form').html();
				$.each(crops, function(index, val) {
					/* iterate through array or object */
					$('.add-crop-form').append(`
						<button class="y-crops col-sm-3" onclick="addCrop('`+val.asset+`')"> `+val.asset+`</button>
					`);
				});
			});
		});

		// add crops
		function addCrop(x) {
			// data 
			var type 	= x;
			var note    = 'this is a note..';
			var token 	= '{{ csrf_token() }}';

			// data to json
			var data = {
				_token:token,
				type:type,
				note:note
			};

			// json to array
			$.ajax({
				url: '/add/crops/'+email,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data){
					// console.log(data);
					// alert(type+ ' crop added successfully');
					if(data.status == 'success'){
						$('.delete_msg').html(`
							<span class="text-success">`+data.message+`</span>
						`);
					}
					reloadCropDiv(data.message);
				},
				error: function (data){
					// console.error(data);
					alert('Error, Fail to add crops ');
					if(data.status == 'error'){
						// alert('delete successfully !');
						$('.delete_msg').html(`
							<span class="text-danger">`+data.message+`</span>
						`);
					}
				}
			});
		}

		// del crops
		function deleteItem(x) {
			// body..
			$.get('/remove/crop/'+x, function(data) {
				/*optional stuff to do after success */
				// console.log(data);
				if(data.status == 'success'){
					// alert('delete successfully !');
					var message = data.message;
					reloadCropDiv(message);
				}
			});
		}

		// function reload crops divs
		function reloadCropDiv(message){

			$('.delete_msg').html(`
				<span class="text-success">`+message+`</span>
			`);

			setTimeout(function(){
				$('.delete_msg').fadeOut();
			}, 2000);

			$.get('/load/farmers/crops/', function(data) {
				/*optional stuff to do after success */
				// console.log(data);
				if(data.status == 'info'){
					$('.add-crop-form').show();
				}

				if(data.status == 'success'){
					var sn = 0;
					$('.load-crops').html('');
					$.each(data.crops, function(index, val) {
						/* iterate through array or object */
						sn++;
						$('.load-crops').append(`
							<tr>
								<td>`+sn+`<td>
								<td>`+val.type+`<td>
								<td>
									<a href="javascript:void(0);" onclick="deleteItem('`+val.id+`')">
										<i class="fa fa-trash"></i> Delete
									</a>
								<td>
							</tr>
						`);
					});
				}

				// load crops tags
				$.get('/commodities-price/load-index', function(crops) {
					/*optional stuff to do after success */
					$('.add-crop-form').html('');
					$.each(crops, function(index, val) {
						/* iterate through array or object */
						$('.add-crop-form').append(`
							<button class="y-crops col-sm-2" onclick="addCrop('`+val.asset+`')">
								`+val.asset+`
							</button>
						`);
					});
				});
			});
		}

    	$.get('/load/api-news', function(data) {
		    // console.log(data);
		    $(".load-headlines").html("");
		    let sn = 0;
	      	
	      	$.each(data.articles, function(index, val) {
		        // console.log(val);
		        sn++;
		        $(".load-headlines").append(`
			          <table cellpadding="2">
			            <tr>
			              <td><img src="`+val.urlToImage+`" width="70" height="50"><td>
			              <td><h4 style="font-size:13px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
			            </tr>
			          </table> 
			          <p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
			    `);

		        if(sn == 4){
		          return false;
		        }
			});
	   	});

    	// dino get data
    	$.get('/load/api-news/bloomberg', function(data) {
	      // console.log(data);
	      let sn = 0;
	      $.each(data.articles, function(index, val) {
	        // console.log(val);
	        sn++;
	        $(".load-headlines-two").append(`
	          <table cellpadding="2">
	            <tr>
	              <td><img src="`+val.urlToImage+`" width="70" height="50"><td>
	              <td><h4 style="font-size:13px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
	            </tr>
	          </table> 
	          <p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
	        `);
	        if(sn == 4){
	          return false;
	        }
	      });
    	});
  	</script>
@endsection