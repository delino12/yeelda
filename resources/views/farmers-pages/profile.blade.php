@extends('layouts.main')

@section('title')
	Profile | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		<a href="/farmer/dashboard">{{ Auth::user()->name }}</a>
		</li>
		<li class="breadcrumb-item active">Profile</li>
	</ol>
	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body">
					<div id="preview-image"></div>
					<br />
					<a href="javascript:void(0)" id="upload_widget_opener" class="btn btn-link">
						<i class="fa fa-camera"></i> choose a photo
					</a>
				</div>
			</div>
			<br />
			<div class="card">
				<div class="card-body">
					Profile Details
					<div class="profile-details"></div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="card">
				<div class="card-body">
					<h2 class="lead">Edit Profile Details</h2><hr />
					{{-- edit profile --}}
					<div class="profile-form">
						<form class="update-form small" method="post" onsubmit="return updateProfile()">
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label>Contact Number </label>
										<div class="input-group col-sm-10">
											<span class="input-group-addon">+234</span>
											<input type="text" class="form-control" pattern="[0-9]*" id="office" placeholder="080---" required="" maxlength="11">
										</div>
										<br />
										<div class="input-group col-sm-10">
											<span class="input-group-addon">+234</span>
											<input type="text" class="form-control" pattern="[0-9]*" id="mobile" placeholder="080---" required="" maxlength="11">
										</div>
									</div>
									<div class="form-group">
										<label>Gender</label>
										<select id="gender" class="form-control col-sm-4 small">
											<option value="male" class="small">Male</option>
											<option value="female" class="small">Female</option>
										</select>
									</div>
									<div class="form-group">
										<label>Address</label>
										<textarea class="form-control" cols="2" rows="3" id="address" placeholder="Type address here.." required=""></textarea>
									</div>

									<div class="form-group">
										<label>Post Code</label>
										<input type="text" class="form-control" id="postal" placeholder="1002242" name="" required="">
									</div>

									<div class="form-group">
										<label>State</label>
										<select id="state" class="form-control col-sm-4 small">
											<option value="" selected="selected">- Select -</option>
												<option value="Abuja FCT">Abuja FCT</option>
												<option value="Abia">Abia</option>
												<option value="Adamawa">Adamawa</option>
												<option value="Akwa Ibom">Akwa Ibom</option>
												<option value="Anambra">Anambra</option>
												<option value="Bauchi">Bauchi</option>
												<option value="Bayelsa">Bayelsa</option>
												<option value="Benue">Benue</option>
												<option value="Borno">Borno</option>
												<option value="Cross River">Cross River</option>
												<option value="Delta">Delta</option>
												<option value="Ebonyi">Ebonyi</option>
												<option value="Edo">Edo</option>
												<option value="Ekiti">Ekiti</option>
												<option value="Enugu">Enugu</option>
												<option value="Gombe">Gombe</option>
												<option value="Imo">Imo</option>
												<option value="Jigawa">Jigawa</option>
												<option value="Kaduna">Kaduna</option>
												<option value="Kano">Kano</option>
												<option value="Katsina">Katsina</option>
												<option value="Kebbi">Kebbi</option>
												<option value="Kogi">Kogi</option>
												<option value="Kwara">Kwara</option>
												<option value="Lagos">Lagos</option>
												<option value="Nassarawa">Nassarawa</option>
												<option value="Niger">Niger</option>
												<option value="Ogun">Ogun</option>
												<option value="Ondo">Ondo</option>
												<option value="Osun">Osun</option>
												<option value="Oyo">Oyo</option>
												<option value="Plateau">Plateau</option>
												<option value="Rivers">Rivers</option>
												<option value="Sokoto">Sokoto</option>
												<option value="Taraba">Taraba</option>
												<option value="Yobe">Yobe</option>
												<option value="Zamfara">Zamfara</option>
												<option value="Outside Nigeria">Outside Nigeria</option>
										</select>
									</div>
									<div class="form-group">
										<button class="btn btn-success">Update Information</button>	
									</div>
								</div>
							</div>				
						</form>
						<br />
						<div class="success_msg"></div>
						<div class="error_msg"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
	
	<div class="row">
		<div class="col-md-5 small">
			<div class="card">
				<div class="card-body">
					<div class="add-company-form"></div>
				</div>
			</div>
			<div class="company-status"></div>
		</div>
		<div class="col-md-7 small">
			<div class="c_success_msg"></div>
			<div class="c_errors_msg"></div>
		</div>
	</div>
	<br />
</div>

	{{-- scripts here --}}
  	
@endsection