@extends('layouts.web-skin')

{{-- title section --}}
@section('title')
    Welcome to YEELDA
@endsection

{{-- contents --}}
@section('contents')
    {{-- slider and top menu --}}
    @include('web-components.slider-header')

    {{-- intro board --}}
    @include('web-components.intro-board')

    {{-- platform users --}}
    @include('web-components.platform-users')

    {{-- platform features --}}
    @include('web-components.platform-features')

    {{-- video intro --}}
    @include('web-components.video-intro')

    {{-- blogs --}}
    @include('web-components.blogs')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

{{-- scripts --}}
@section('scripts')
    @if(Auth::check())
        <script type="text/javascript">
            // load all page scripts
            // listent to chat on channels 
            var channel = pusher.subscribe('new-chat-message');
            channel.bind('YEELDA\\Events\\NewChat', function(data) {

                var audio = new Audio('/audio/notify.wav');
                    audio.play();
                
                // console.log(data);
                var logged_email = $("#user_email").val();
                var el = data;
                if(el.email !== logged_email){

                    var name = el.name;
                    var body = el.body;
                    var img  = el.image;

                    Push.create(name, {
                        body: body,
                        icon: img,
                        timeout: 4000,
                        onClick: function () {
                            window.focus();
                            this.close();
                        }
                    });
                }else{
                    // pushNotifyer;
                }
            });
        </script>
        @if(Session::has("verification_success"))
            <script type="text/javascript">
                swal(
                    "success",
                    "Account verification successful!",
                    "success"
                );
            </script>
        @elseif(Session::has("verification_error"))
            <script type="text/javascript">
                swal(
                    "Oops",
                    "Failed to verify account activation!",
                    "error"
                );
            </script>
        @endif
    @endif

    <script type="text/javascript">
        $(".collapse").click(function(event) {
            // console log data
            console.log(event.target.e);
        });;
    </script>
@endsection