@extends('layouts.web-skin')

@section('title')
	Reset Account Password | Yeelda
@endsection

@section('contents')
{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.change-password-reset-form')

	{{-- blogs --}}
    @include('web-components.blogs')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

    {{-- scripts assets --}}
    @include('web-components.scripts-assets')
@endsection

@section('scripts')
	{{-- scripts --}}
	<script type="text/javascript">
		function resetPassword(){
			$("#y-reset-pass-btn").prop("disabled", true);
			$("#y-reset-pass-btn").html(`
				<img src="/svg/three-dots.svg" width="20px" height="20px" style="display: none;" class="loading pull-right"> Processing
			`);
			// token & sections
			var token = '{{ csrf_token() }}';
			var email = '{{ $email }}';
			var resetToken = '{{ $token }}';
			var passwordA = $("#passwordA").val();
			var passwordB = $("#passwordB").val();

			if(passwordA !== passwordB){
				$('.error_msg').html(`
					<div class="alert alert-danger">
						<p class="text-danger">Password did not match !</p>
					</div>
				`);

				swal(
					"Oops",
					"Password did not match!",
					"error"
				);

				// void
				return false;

				$("#y-reset-pass-btn").prop("disabled", false);
				$("#y-reset-pass-btn").html(`
					<i class="fa fa-lock"></i> Reset Password
				`);
			}

			// json response 
			var data = {
				_token:token,
				resetToken:resetToken,
				email:email,
				password:passwordA
			}

			// send to ajax
			$.ajax({
				url: '/reset/account/password',
				type: 'post',
				dataType: 'json',
				data:data,
				success: function (data){
					$("#y-reset-pass-btn").html(`
						<i class="fa fa-lock"></i> Reset Password
					`);
					$("#y-reset-pass-btn").prop("disabled", false);
					if(data.status == 'success'){
						$('.success_msg').html(`
							<div class="alert alert-success">
								<p class="text-success">`+data.message+`</p>
							</div>
						`);
					}
					if(data.status == 'error'){
						$('.error_msg').html(`
							<div class="alert alert-danger">
								<p class="text-danger">`+data.message+`</p>
							</div>
						`);
					}

					$('#reset-form')[0].reset();
					$('.loading').show();
				},
				error: function (data){
					$("#y-reset-pass-btn").html(`
						<i class="fa fa-lock"></i> Reset Password
					`);
					$("#y-reset-pass-btn").prop("disabled", false);
				}
			});	

			return false;	
		}
	</script>
@endsection
