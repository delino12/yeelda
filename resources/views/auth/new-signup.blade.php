@extends('layouts.web-skin')

@section('title')
	YEELDA | Registration
@endsection

@section('contents')
	@include('web-components.plain-header')
	@include('web-components.registration-form')
    @include('web-components.blogs')
    @include('web-components.footer')
    @include('web-components.social-media')
    @include('web-components.scripts-assets')
@endsection

@section('scripts')
	@if(Session::has("success_status"))
	    <script type="text/javascript">
	        swal(
	            "success",
	            "Account verification successful!",
	            "success"
	        );
	    </script>        
	@elseif(Session::has("error_status"))
	    <script type="text/javascript">
	        swal(
	            "Oops",
	            "Account already registered!",
	            "error"
	        );
	    </script>
    @endif
@endsection
