@extends('layouts.admin-auth-skin')

{{-- Title --}}
@section('title')
    YEELDA | ADMIN OFFICE
@endsection

{{-- Contents  --}}
@section('contents')
    <div class="row mb-2 mt-5">
        <div class="col-md-6 offset-md-3">
            <!-- Input & Button Groups -->
            <div class="card card-small mb-4 ml-5 mr-5">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Admin Control Panel</h6>
                </div>
                <div class="card-body">
                    <form method="POST" onsubmit="return LoginAdmin()">  
                        <!-- Seamless Input Groups -->
                        <strong class="text-muted d-block mb-2">Admin username</strong>
                        <div class="input-group mb-3">
                            <div class="input-group input-group-seamless">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="material-icons">person</i>
                                    </span>
                                </span>
                                <input type="text" class="form-control" id="username" placeholder="Username" required="">
                            </div>
                        </div>

                        <!-- Seamless Input Groups -->
                        <strong class="text-muted d-block mb-2">Admin Password
                            {{-- <span> --}}
                                <a href="javascript:void(0);" class="float-right" onclick="showHidePass()">
                                    <i class="material-icons text-info">lock</i> Show
                                </a>
                            </span>
                        </strong>
                        <div class="input-group mb-3">
                            <div class="input-group input-group-seamless">
                                <span class="input-group-prepend">
                                  <span class="input-group-text">
                                    <i class="material-icons">lock</i>
                                  </span>
                                </span>
                                <input type="password" class="form-control" id="password" placeholder="**********" required="">
                            </div>
                        </div>

                        <!-- Input/Button Group -->
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <button class="btn btn-white" id="login-btn" type="submit">Login</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- / Input & Button Groups -->
        </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		function LoginAdmin(){
			// start loading...
			$("#login-btn").html('Processing...');

			// body...
			var token = '{{ csrf_token() }}';
			var username = $("#username").val();
			var password = $("#password").val();

			// data to json
			var data = {
				_token:token,
				username:username,
				password:password
			};

			// ajax query
			$.ajax({
				url: '/admin/login/request',
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(data){
					if(data.status == 'success'){
						window.location.href = '/admin/dashboard';
					}else{
						swal(
                            "oops",
                            data.message,
                            data.status
                        );
					}
				},
				error: function(data){
					swal(
                        "oops",
                        "check internet connection and try again!",
                        "info"
                    );
				}
			});

            $("#login-btn").html('Login');

			// prevent form action from loading 
			return false;
		}

        // show/hide password
        function showHidePass() {
            let passwordArea = document.getElementById("password");

            if(passwordArea.type === 'password'){
                passwordArea.type = 'text';
            }else{
                if(passwordArea.type === 'text'){
                    passwordArea.type = 'password'
                }
            }
        }
	</script>
@endsection