@extends('layouts.skin')

@section('title')
	Account Recovery | Yeelda
@endsection

@section('contents')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="lead">Let Yeelda help you get back into your account </h1>
				<hr />
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
				<div id="dinoForm">
					<div class="col-md-8">
		                <form id="reset-form" method="post" onsubmit="return searchUsers()">
	                        <div class="form-group">
	                            <label for=""><i class="fa fa-envelope"></i> Email</label>
	                            <input type="email" class="form-control" id="email" placeholder="Enter your email address" required="">
	                        </div>
		                    <div class="form-group">
		                        <button class="btn btn-success col-md-12">
		                           <i class="fa fa-lock"></i> Recover Account
		                           <img src="/svg/three-dots.svg" width="20px" height="20px" style="display: none;" class="loading pull-right">
		                        </button>
		                    </div>
		                    <div class="form-group">
		                    	<br /><br />
		                    	<div class="success_msg"></div>
		                    	<div class="error_msg"></div>
		                    </div>
		                </form>
		            </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
				<br />
				<span class="small">Type in your email address, Yeelda will help you search and recover your account.</span>
			</div>
		</div>
	</div>
	
	<div style="height: 100px;"></div>
	<hr />
	{{-- scripts --}}
	<script type="text/javascript">
		function searchUsers(){
			$('.loading').show();
			// token & sections
			var token = '{{ csrf_token() }}';
			var email = $("#email").val();

			// json response 
			var data = {
				_token:token,
				email:email
			}

			// send to ajax
			$.ajax({
				url: '/find/user/account',
				type: 'post',
				dataType: 'json',
				data: data,
				success: function (data){
					// console.log(data);
					if(data.status == 'success'){
						$('.success_msg').html(`
							<div class="alert alert-success">
								<p class="text-success">`+data.message+`</p>
							</div>
						`);
					}

					if(data.status == 'error'){
						$('.error_msg').html(`
							<div class="alert alert-danger">
								<p class="text-danger">`+data.message+`</p>
							</div>
						`);
					}

					$('#reset-form')[0].reset();
					$('.loading').hide();
				},
				error: function (data){
					// console.log(data);
					alert('Error, Fail to send request !');
					$('.loading').hide();
				}
			});	

			return false;	
		}
	</script>
@endsection
