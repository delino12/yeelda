@extends('layouts.web-skin')

@section('title')
	YEELDA | Reset Password
@endsection

@section('contents')
	@include('web-components.plain-header')
	@include('web-components.reset-password-form')
    @include('web-components.blogs')
    @include('web-components.footer')
    @include('web-components.social-media')
    @include('web-components.scripts-assets')
@endsection

@section('scripts')
	<script type="text/javascript">
		function searchUsers(){
			$('.loading').show();
			$("#y-reset-pass-btn").prop("disabled", true);
			$("#y-reset-pass-btn").html(`
				Searching...
			`);
			var token = '{{ csrf_token() }}';
			var email = $("#email").val();

			var data = {
				_token:token,
				email:email
			}

			$.ajax({
				url: '/find/user/account',
				type: 'post',
				dataType: 'json',
				data:data,
				success: function (data){
					$("#y-reset-pass-btn").html(`
						Send
					`);
					$("#y-reset-pass-btn").prop("disabled", false);
					if(data.status == 'success'){
						$('.success_msg').html(`
							<div class="alert alert-success">
								<p class="text-success">`+data.message+`</p>
							</div>
						`);
					}
					if(data.status == 'error'){
						$('.error_msg').html(`
							<div class="alert alert-danger">
								<p class="text-danger">`+data.message+`</p>
							</div>
						`);
					}

					$('#reset-form')[0].reset();
					$('.loading').hide();
				},
				error: function (data){
					$("#y-reset-pass-btn").prop("disabled", false);
					$("#y-reset-pass-btn").html(`
						Send
					`);
				}
			});	

			return false;	
		}
	</script>
@endsection
