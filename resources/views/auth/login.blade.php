@extends('layouts.web-skin')

@section('title')
	YEELDA | Signin
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.login-form')

	{{-- blogs --}}
    @include('web-components.blogs')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

    {{-- scripts assets --}}
    @include('web-components.scripts-assets')
@endsection

@section('scripts')
	{{-- google login --}}
	<script src="https://apis.google.com/js/platform.js" async defer></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '217154015705817',
                cookie     : true,
                xfbml      : true,
                version    : 'v2.12'
            });
          
            FB.AppEvents.logPageView();  
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

	{{-- facebook login --}}
	<script type="text/javascript">
		// get login status
		$(document).ready(function($) {
		    FB.getLoginStatus(function(response) {
		        statusChangeCallback(response);
		        // console.log(response);
		    });

		    // google login
		    // init google login
			function onSignIn(googleUser) {
			    var profile = googleUser.getBasicProfile();
			    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
			    console.log('Name: ' + profile.getName());
			    console.log('Image URL: ' + profile.getImageUrl());
			    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

			    loginViaEmail(profile.getEmail());
			 }
		});

		// check logged in state
	    function checkLoginState() {
		  FB.getLoginStatus(function(response) {
		    statusChangeCallback(response);
		    // console.log(response);
		  });
		}

		// on status change
		function statusChangeCallback(response) {
	    	// body...
		    if(response.status === 'connected'){
		      // setElements(true);
		      let userId = response.authResponse.userID;
		      // console.log(userId);
		      console.log('User is logged in on Facebook');
		      getUserInfo(userId);
		      
		    }else{
		      // setElements(false);
		      console.log('not logged in !');
		    }
	  	}

		// get user information
		function getUserInfo(userId) {
		    // body...
		    FB.api(
		      '/'+userId+'/?fields=id,name,email',
		      'GET',
		      {},
		      function(response) {
		        // Insert your code here
		        // console.log(response);
		        let email = response.email;
		        console.log(email);
		        loginViaEmail(email);
		      }
		    );
		}

		// login via email
		function loginViaEmail(email) {

			// body...
			let token = '{{ csrf_token() }}';

			let data = {
			  _token:token,
			  email:email
			}

			$.ajax({
			  	url: '/login/via/email',
			  	type: 'POST',
			  	dataType: 'json',
			  	data: data,
			  	success: function(data){
			    	
			    	// console.log(data);
			    	if(data.status == 'success'){
			     		window.location.href = '/';
			    	}

			    	if(data.status == 'info'){
			    		window.location.href = '/account/register';
			    		// console.log(data.message);
			    	}
			  	},
			  	error: function(data){
			    	// console.log('Error logging in via email !');
			    	alert('Fail to send social media login request !');
			  	}
			});
		}

		// logout from facebook
		function webappFacebookLogout() {
			// body...
			FB.getLoginStatus(function(response) {
		        // statusChangeCallback(response);
		        FB.logout(function(response){
		        	console.log('Logged out successfully !');
		        });
		    });
		}
	</script>
@endsection
