@extends('layouts.skin-login')

@section('title')
	Create Account | Yeelda
@endsection

@section('contents')
	<div class="container signup-div">
		<br /><br ><br /><br />
		<br /><br />
		<div class="row">
			<div class="container">
				<div class="row">
					<div style="margin-left:15%;margin-right: 15%;">
					<section>
				        <div class="wizard">
				            <h1 class="lead">Create Account</h1>
				            <div class="form-errors"></div>
				            <div class="wizard-inner">
				                <div class="connecting-line"></div>
				                <ul class="nav nav-tabs" role="tablist">
				                    <li role="presentation" class="active">
				                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
				                            <span class="round-tab">
				                                <i class="glyphicon glyphicon-folder-open"></i>
				                            </span>
				                        </a>
				                    </li>

				                    <li role="presentation" class="disabled">
				                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
				                            <span class="round-tab">
				                                <i class="glyphicon glyphicon-lock"></i>
				                            </span>
				                        </a>
				                    </li>
				                    <li role="presentation" class="disabwled">
				                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
				                            <span class="round-tab">
				                                <i class="glyphicon glyphicon-user"></i>
				                            </span>
				                        </a>
				                    </li>

				                    <li role="presentation" class="disabled">
				                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
				                            <span class="round-tab">
				                                <i class="glyphicon glyphicon-ok"></i>
				                            </span>
				                        </a>
				                    </li>
				                </ul>
				            </div>

				            <form role="form" method="POST" id="registration-form" action="/account/register" enctype="multipart/form-data">
				            	{{ csrf_field() }}
				                <div class="tab-content">
				                    <div class="tab-pane active" role="tabpanel" id="step1">
				                    	<div class="col-md-6">
					                        <h3>Step one</h3>
						                	<div class="form-group">
						                        <div class="input-group">
						                        	<span class="input-group-addon">
						                        		<i class="fa fa-user"></i>
						                        	</span>
							                        <input type="text" class="form-control" name="name" placeholder="First Name and Last Name" required="">
						                        </div>
						                        <span class="error_name small"></span>
						                    </div>
						                    <div class="form-group">
						                        <div class="input-group">
						                        	<span class="input-group-addon">
						                        		<i class="fa fa-envelope"></i>
						                        	</span>
							                        <input type="email" class="form-control" name="email" placeholder="Enter email address" required="">
						                        </div>
						                        <span class="error_email small"></span>
						                    </div>
						                </div>

						                <div class="col-md-12">
						                	<ul class="list-inline pull-right">
					                            <li><button type="button" class="btn btn-primary next-step">next</button></li>
					                        </ul>
						                </div>
				                    </div>
				                    <div class="tab-pane" role="tabpanel" id="step2">
				                        <h3>Step two</h3>
				                        <div class="col-md-6">
				                        	<div class="form-group">
						                        <div class="input-group">
						                        	<span class="input-group-addon">
						                        		<i class="fa fa-lock"></i>
						                        	</span>
							                        <input type="password" class="form-control" name="password" placeholder="choose a password" required="">
							                        <span class="error_passwd small"></span>
						                        </div>
						                    </div>
						                    <div class="form-group">
						                        <div class="input-group">
						                        	<span class="input-group-addon">
						                        		<i class="fa fa-phone"></i>
						                        	</span>
							                         <input type="text" pattern="[0-9]*" class="form-control" maxlength="11" name="phone" placeholder="Enter a phone number eg. 080000000" required="">
							                        <span class="error_phone small"></span>
						                        </div>
						                    </div>
					                    </div>

					                    <div class="col-md-12">
					                    	<ul class="list-inline pull-right">
					                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
					                            <li><button type="button" class="btn btn-primary next-step">next</button></li>
					                        </ul>
					                    </div>
				                    </div>
				                    <div class="tab-pane" role="tabpanel" id="step3">
				                        <h3>Step three</h3>
				                        <div class="form-group">
				                        	<div class="col-md-6">
						                        {{-- <div class="input-group">
						                        	<span class="input-group-addon">
						                        		<i class="fa fa-camera"></i>
						                        	</span>
							                         <input type="file" class="form-control" maxlength="11" name="file" required="">
							                        <span class="error_image small"></span>
						                        </div> --}}
						                        <div class="input-group">
						                        	<span class="input-group-addon">
						                        		<i class="fa fa-user"></i>
						                        	</span>
							                        <select class="form-control" name="accountType">
							                        	<option value="farmer">Signup as a Farmer</option>
							                        	<option value="buyer">Signup as a Buyer</option>
							                        	<option value="service">Signup as a Service Provider</option>
							                        </select>
							                        <span class="error_farmer small"></span>
						                        </div>
						                    </div>
						                </div>

				                        <div class="col-md-12">
					                    	<ul class="list-inline pull-right">
					                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
				                            	<li><button type="button" class="btn btn-primary next-step">next</button></li>
					                        </ul>
					                    </div>
				                    </div>
				                    <div class="tab-pane" role="tabpanel" id="complete">
				                        <h3>Final Step</h3>
				                        <p>	
				                        	By clicking the submit button, hereby you have accept YEELDA 
				                        	<a href="/terms/conditions" class="btn btn-link">Terms and Conditions</a> 
				                        </p>

				                        <div class="form-group">
				                        	<div class="col-md-6">
						                  		
						                    </div>
						                </div>

				                        <div class="col-md-12">
					                    	<ul class="list-inline pull-right">
					                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
				                            	<li><button type="button" class="btn btn-primary btn-info-full next-step">Submit</button></li>
					                        </ul>
					                    </div>
				                    </div>
				                    <div class="clearfix"></div>
				                </div>
				            </form>
				            {{-- End of form --}}
				        </div>
				    </section>
				</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
			<br />
			<p style="color:#fff;">
            	
            </p>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{{-- signup user --}}
	<script type="text/javascript">
		// Stop Hating..
		$(document).ready(function () {
		    // Initialize tooltips
		    $('.nav-tabs > li a[title]').tooltip();
		    
		    // Wizard
		    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
		        var $target = $(e.target);
		        if ($target.parent().hasClass('disabled')) {
		            return false;
		        }
		    });

		    $(".next-step").click(function (e) {
		    	let name 		= $('input[name=name]').val();
		    	let email 		= $('input[name=email]').val();

		    	if(email == ''){
		    		$('.error_email').html(`
		    			<span class="text-danger">
		    				Enter a valid email
		    			</span>
		    		`);
		    		return false;
		    	}

		    	if(name == ''){
		    		$('.error_name').html(`
		    			<span class="text-danger">
		    				Enter a valid name 
		    			</span>
		    		`);
		    		return false;
		    	}

		        var $active = $('.wizard .nav-tabs li.active');
		        $active.next().removeClass('disabled');
		        nextTab($active);
		    });
		    
		    $(".prev-step").click(function (e) {

		        var $active = $('.wizard .nav-tabs li.active');
		        prevTab($active);
		    });

		    $(".btn-info-full").click(function (e) {
		    	e.preventDefault();

		    	let name 		= $('input[name=name]').val();
		    	let email 		= $('input[name=email]').val();
		    	let password 	= $('input[name=password]').val();
		    	let phone 		= $('input[name=phone]').val();

		    	if(name == ''){
		    		// alert('Name field is required !');
		    		$(".form-errors").html(`
		    			<span class="text-danger">
		    				Name is required !
		    			</span>
		    		`);
		    		return false;
		    	}

		    	if(email == ''){
		    		$(".form-errors").html(`
		    			<span class="text-danger">
		    				Email field is required !
		    			</span>
		    		`);
		    		return false;
		    	}

		    	if(password == ''){
		    		$(".form-errors").html(`
		    			<span class="text-danger">
		    				Password field is required !, min of 6 characters
		    			</span>
		    		`);
		    		return false;
		    	}

		    	if(phone == '' || phone.length < 11){
		    		$(".form-errors").html(`
		    			<span class="text-danger">
		    				Phone number is invalid !, Must be 11 digits
		    			</span>
		    		`);
		    		return false;
		    	}

		    	$("#registration-form").submit();
		    });

		});

		function nextTab(elem) {
		    $(elem).next().find('a[data-toggle="tab"]').click();
		}

		function prevTab(elem) {
		    $(elem).prev().find('a[data-toggle="tab"]').click();
		}
	</script>

	@if(Session::has("success_status"))
	    <script type="text/javascript">
	        swal(
	            "success",
	            "Account verification successful!",
	            "success"
	        );
	    </script>        
	@elseif(Session::has("error_status"))
	    <script type="text/javascript">
	        swal(
	            "Oops",
	            "Account already registered!",
	            "error"
	        );
	    </script>
    @endif
@endsection
