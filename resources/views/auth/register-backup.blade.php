@extends('layouts.skin-login')

@section('title')
	Create Account | Yeelda
@endsection

@section('contents')
	<div class="container signup-div">
		<br />
		<h1 style="color: #CCB19E;">Join us today</h1>
		<hr />

		@if(session('error_status'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 40px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_status') }}</p>
			</div>
		@endif

		<div class="row" id="dQ">
			<div class="col-md-3">
				<p>Register as </p>
			</div>
			<div class="col-md-3">
				<a href="javascript:void(0);" onclick="showForm('farmer')"><button class="dino-input"><i class="fa fa-tree"></i> A Farmer</button> </a>
			</div>

			<div class="col-md-3">
				<a href="javascript:void(0);" onclick="showForm('company')"><button class="dino-input"><i class="fa fa-money"></i> A Buyer </button> </a>
			</div>

			<div class="col-md-3">
				<a href="javascript:void(0);" onclick="showForm('service')"><button class="dino-input"><i class="fa fa-user"></i> Service Provider</button> </a>
			</div>
		</div>
		<div class="row">
			<div id="dinoForm" style="display: none; font-size: 14px; color: ##CCB19E;">
				<div class="col-md-8 col-md-offset-2">
					<span id="type" class="small" style="color: #CCB19E;"></span>
	                <form role="form" method="POST" action="/account/register" id="signupform">
	                	{{ csrf_field() }}
	                	<input type="hidden" id="accountType" name="accountType">
	                	<div class="form-group col-md-6" style="display: none;" id="company">
                            <label for="company"> <i class="fa fa-users"></i> <span style="color:#fff;">Company Name</span></label>
                            <input type="text" class="form-control" name="company"  placeholder="Your Company">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="first_name"><i class="fa fa-user"></i> <span style="color:#fff;">Names</span></label>
                            <input type="text" class="form-control" name="name"  placeholder="Name here" required="">
                        </div>

                        <div class="form-group col-md-6">
                            <label for=""><i class="fa fa-envelope"></i> <span style="color:#fff;">Email</span></label>
                            <input type="email" class="form-control" name="email" placeholder="Email" required="">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password"><i class="fa fa-key"></i> <span style="color:#fff;">Password</span></label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="phone"><i class="fa fa-phone"></i> <span style="color:#fff;">Phone</span></label>
                            <input type="text" pattern="[0-9]*" class="form-control" maxlength="12" name="phone" placeholder="0813 000 00000" required="">
                        </div>

	                    <div class="form-group">
	                        <div class="col-md-12">
	                            <div class="checkbox">
	                                <label>
	                                    <input type="checkbox" required="">
	                                    <span style="color:#fff;">I accept the</span> <a href="/terms">terms and conditions</a>
	                                </label>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-md-6">
	                            <button type="submit" class="dino-input btn-primary">
	                                Get Started
	                            </button>
	                        </div>
	                    </div>

	                </form>

	                    <div class="col-md-6">
	                        <a href="/google-signin" style="color:#ecb;"><i class="fa fa-google"></i> Signup with Google Account </a> <br />
	                        <a href="/facebook-signin" style="color:#ecb;"><i class="fa fa-facebook"></i> Signup using your Facebook Account </a>
	                    </div>
	            </div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
			<br />
            <p style="color:#fff;">
            	Not new to yeelda ? <a href="/account/login">I already have an account </a>
            </p>
			</div>
		</div>
		<script type="text/javascript">
			function showForm(x){
				// show signup form
				$("#dinoForm").toggle();
				$("#dQ").hide();

				var accountType = x;
				var accType = document.getElementById("accountType").value;
				if(x == 'farmer'){
					$("#type").html('As a Farmer, this platform allow you to sell your farm products, make trade with investors and also provide various jobs in the Agro sector');
					accType = x;
				}

				if(x == 'company'){
					$("#type").html('Buy Farm products from farmers who are willing to sell their products ahead of time.');
					$("#company").show();
					accType = x;
				}

				if(x == 'service'){
					$("#type").html('Agricultural Sector needs professional skills, individuals who study the Agro world and Environment can easily get jobs and provide services such as, Machinery, Labours and other operating service related to agricultuaral development');
					accType = x;
				}

				var accType = document.getElementById("accountType").value = x;
			}
		</script>
	</div>
@endsection
