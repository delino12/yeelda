@extends('layouts.error-skin')

@section('title')
	Page not found | Yeelda
@endsection

@section('contents')
	<div style="height: 100px;"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<img src="/images/yeelda-logo.png"> <br /><br /><br />
				<h1 class="title"> Sorry, the page you are looking for could not be found</h1>
			</div>
		</div>
	</div>
@endsection