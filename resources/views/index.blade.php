<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Yeelda</title>
        <link rel="icon" href="/images/favicon.png">
        <meta name="google-site-verification" content="pzKPeRDO7Y5TiryGMjnyg7JQr8h5DMsOgs2RZuw0grw" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Connect Farmers, Services Providers and buyers, sell farm inputs and farm produce." />
        <meta name="keywords" content="Yeelda Farming Hub, make money from your farm" />
        <meta name="author" content="cavidel.com" />
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="Yeelda, No.1 Africa Agricultural Hub"/>
        <meta property="og:image" content="http://yeelda.com/images/010.jpg"/>
        <meta property="og:url" content="http://yeelda.com"/>
        <meta property="og:site_name" content="Yeelda"/>
        <meta property="og:description" content="Connecting Farmers, Services Providers and buyers, sell farm inputs and farm produce."/>

        {{-- twitter --}}
        <meta name="twitter:title" content="Yeelda, No.1 Africa Agricultural Hub" />
        <meta name="twitter:image" content="http://yeelda.com/images/010.jpg" />
        <meta name="twitter:url" content="http://yeelda.com" />
        <meta name="twitter:card" content="Connecting Farmers, Services Providers and buyers, sell farm inputs and farm produce." />

        <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- Animate.css -->
        <link rel="stylesheet" href="/css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="/css/icomoon.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="/css/bootstrap.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="/css/magnific-popup.css">

        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">

        <!-- Theme style  -->
        <link rel="stylesheet" href="/css/style.css">

        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

        {{-- sweetaleart --}}
        <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

        <!-- Modernizr JS -->
        <script src="/js/modernizr-2.6.2.min.js"></script>
        <script src="/js/greetings.js"></script>

        {{-- added AOS --}}
        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

        {{-- google analytics --}}
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118912888-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-118912888-1');
        </script>


        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="gtco-loader"></div>
        <div id="page">
            <header id="gtco-header" class="gtco-cover" role="banner" style="background-image:url(/images/bg_images/{{ $bg }});">
                <section class="" style="font-size: 13px;">
                    <nav class="navbar navbar-fixed-top" style="background-color:#FFFFFF;">
                        <div class="top-nav">
                            <div class="top-nav-item" align="right">
                            <div class="text-center">
                                <div class="enable-notify"></div>
                            </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="fa fa-navicon"></i> Menu
                          </button>
                          <a href="/">
                              <img src="/images/img-set/logo-transparent-new.png" height="48" width="auto" style="position: absolute;margin-top:4px;">
                          </a>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle dropbtn" href="/farm-equipment">
                                            <i class="fa fa-leaf"></i> Our Offerings
                                        </a>
                                        <ul class="dropdown-menu dropdown-content" style="background-color:rgba(000,000,000,0.70);">
                                            <li><a style="font-weight: 100px;" href="/farm-produce"><i class="fa fa-tree"></i> Farm Produce</a></li>
                                            <li style="border: 1px solid #FFF;"></li>
                                            <li style="border: 1px solid #FFF;"></li>
                                            <li><a style="font-weight: 100px;" href="/select-services"><i class="fa fa-truck"></i> Farm Services</a></li>
                                            <li><a style="font-weight: 100px;background-color:#FFCC00;color:#000;" href="/commodity-market"><i class="fa fa-money"></i> Commodity Market</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item"><a href="/chat/community"><i class="fa fa-comments-o"></i> Community</a></li>
                                    @if(Auth::guard('farmer')->check())
                                        <input type="hidden" id="user_email" value="{{ Auth::guard('farmer')->user()->email }}">
                                        <li class="nav-item">
                                            <a href="/farmer/dashboard">
                                                <i class="fa fa-dashboard"></i> Dashboard
                                            </a>
                                        </li>
                                        <li class="nav-item"><a href="/account/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                                    @elseif(Auth::guard('investor')->check())
                                    <input type="hidden" id="user_email" value="{{ Auth::guard('investor')->user()->email }}">
                                    <li class="nav-item">
                                        <a href="/investors/dashboard">
                                            <i class="fa fa-dashboard"></i> Dashboard
                                        </a>
                                    </li>
                                    <li class="nav-item"><a href="/account/logout/investor"><i class="fa fa-sign-out"></i> Logout</a></li> 
                                    @elseif(Auth::guard('sp')->check())
                                        <input type="hidden" id="user_email" value="{{ Auth::guard('sp')->user()->email }}">
                                        <li class="nav-item">
                                            <a href="/service-provider/dashboard">
                                                <i class="fa fa-dashboard"></i> Dashboard
                                            </a>
                                        </li> 
                                        <li class="nav-item"><a href="/account/logout/sp"><i class="fa fa-sign-out"></i> Logout</a></li> 
                                    @else
                                        <li class="nav-item"><a href="/account/login"><i class="fa fa-sign-in"></i> Login </a></li>
                                    
                                    @endif
                                    
                                    <li class=" dropdown">
                                        <a class="dropdown-toggle dropbtn" data-toggle="dropdown" href="/farm-equipment">
                                            <i class="fa fa-info"></i> About us
                                        </a>
                                        <ul class="dropdown-menu dropdown-content" style="background-color:rgba(000,000,000,0.70);">
                                            <li><a style="font-weight: 100px;" href="/about#what-is-yeelda"><i class="fa fa-question"></i> What is yeelda</a></li>
                                            <li style="border: 1px solid #FFF;"></li>

                                            <li><a style="font-weight: 100px;" href="/about#our-service"><i class="fa fa-street-view"></i> Our Service</a></li>
                                            <li style="border: 1px solid #FFF;"></li>

                                            <li><a style="font-weight: 100px;" href="/about#our-team"><i class="fa fa-sitemap"></i> Our Team</a></li>
                                            {{-- <li style="border: 1px solid #FFF;"></li> --}}
                                            <li><a style="color: #fff;"  href="/contact"><i class="fa fa-envelope"></i> Contact</a></li>
                                            <li><a style="color: #fff;"  href="/faq"><i class="fa fa-question-circle"></i> FAQ</a></li>
                                        </ul>
                                    </li>
                                    {{-- <li class="nav-item"><a href="/contact"><i class="fa fa-envelope"></i> Contact</a></li> --}}
                                    <li class="nav-item">
                                        <a href="/blogs/news"><i class="fa fa-twitch"></i> Blog</a>
                                    </li>
                                    {{-- <li class="nav-item">
                                        <a href="/faq"><i class="fa fa-question-circle"></i> FAQ</a>
                                    </li> --}}
                                    <li class="nav-item">
                                        <a href="/carts"><span class="carts_total"></span><i class="fa fa-shopping-cart"></i> Cart</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="price-discovery" style="background-color:#FFFFFF;color:#CBB39B;">
                            <marquee direction="right" speed="slow">
                                <span class="ticker"></span>
                            </marquee>
                        </div>
                    </nav>    
                </section>

                <section class="overlay"> 
                    <div class="gtco-container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-3 text-center">
                                @if(session('success_msg'))
                                    <div class="alert alert-success">
                                        <p class="text-success">{{ session('success_msg') }}</p>
                                    </div>
                                @endif
                                <div style="margin-top: -10%;">
                                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                                        <br />
                                        <h1>
                                          <span class="text-wrapper">
                                            <span class="letters">Buy, Sell & Hire</span>
                                          </span>
                                        </h1>

                                        <h3>
                                            <a href="javascript:void(0);" style="color: #FFF;" class="typewrite" data-period="2000" data-type='[ 
                                                "Hi, I am Yeelda.", 
                                                "I Connect Farmers, Buyers & Service Providers", 
                                                "Make more from your farm produce and inputs",
                                                "Click Get Started, Join Yeelda Today"]'>
                                                <span class="wrap"></span>
                                            </a>
                                        </h3>
                                        <a href="/account/register"> <button class="btn btn-success" id="get-started">Get Started</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </header>

            {{-- intro section --}}
            <div class="intro">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <br /><br />
                            <h4 style="color:#2CA8CC;font-weight: bolder;font-size:14px;">THE AGRO HUB </h4>
                            <p class="lead small">
                                Yeelda is a start-up Agric-business 
                                seeking to create an ecosystem that brings together farmers, 
                                off-takers and input/service providers with a view to 
                                connecting the demand and suppy for agricultural produce and resources.
                            </p>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>

            {{-- our values --}}
            <div id="gtco-features">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="/images/img-set/01.jpg" height="250" width="auto" class="y-img">
                            <hr class="y-hr" />
                            <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="small">
                                        With the extant challenges within the agricultural space in Nigeria, 
                                        YEELDA was birthed to provide virtual platform for key stakeholders to 
                                        interact and mutually benefit through aggregation of disperses data across country.
                                    </p>
                                    <p class="small">Put simply, we will be able to answer 2 seemingly innocuous question of 
                                        <span class="w-quotes">“who has what, where?” & “who needs what, when” </span>
                                        this will be coupled with the ability to initiate,
                                        negotiate and conclude a purchase and/or sale of 
                                        Agric-based inputs/outputs electronically after pre-determined due diligence activities by participants.
                                    </p>
                                    <p></p>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img src="/images/img-set/start-season.jpg" class="img-responsive">
                            <br />
                            <p class="lead">Connecting technology with farmers</p>
                            <hr />
                            <p class="small">
                                Agromet Services
                                The sources of weather and climate-related risks in agriculture are numerous and diverse: limited water resources,
                                drought, desertification, land degradation, erosion, hail, flooding, early frosts and many more. 

                                Effective weather and climate information and advisory services can inform the decision-making of farmers and improve 
                                their management of related agricultural risks. 
                                <button class="btn btn-link"><a href="/blogs/news">read more</a></button>
                            </p>
                        </div>
                    </div>

                    <div style="height: 50px;"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <img src="/images/img-set/pin-image-1.png">        
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-sm-7"><img src="/images/img-set/tech-plant.jpg" class="img-responsive"></div>
                                <div class="col-sm-5">
                                    <p class="small">
                                        Agricultural based trading platform connecting the agricultural value chain through technology.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br /><br /><br />
            <div id="gtco-features">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="feature-center animate-box" data-animate-effect="fadeIn" style="color:#9B7B56;">
                                <span class="icon">
                                    <i class="fa fa-tree"></i>
                                </span>
                                <h3>Harvested Produce</h3>
                                <p>Buyers are ready to buy before crop production yields, already havested farm produce help boost your personal income in no time</p>
                                <p><a href="/farm-produce" class="btn btn-primary" style="color:#FFF;">Learn More</a></p>
                            </div>
                        
                        </div>
                        <div class="col-md-4 col-sm-4">
                            
                            <div class="feature-center animate-box" data-animate-effect="fadeIn" style="color:#9B7B56;">
                                <span class="icon">
                                    <i class="icon-tree"></i>
                                </span>
                                <h3>Seasonal Sales</h3>
                                <p>Sell your farm produce before thinking of planting, Cash payment are available immediately users confirmed the produces are ready, Farmers can make money ahead of season before havest period</p>
                                <p><a href="/farm-produce" class="btn btn-primary" style="color:#FFF;">Learn More</a></p>
                            </div>
                        
                        </div>
                        <div class="col-md-4 col-sm-4">
                            
                            <div class="feature-center animate-box" data-animate-effect="fadeIn" style="color:#9B7B56;">
                                <span class="icon">
                                    <i class="icon-key"></i>
                                </span>
                                <h3>Limited Farm Produce</h3>
                                <p>Once a Farm produces is booked, Farmers can re-consider crop duration and availablity, before contract mature date</p>
                                <p><a href="#" class="btn btn-primary" style="color:#FFF;">Learn More</a></p>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>  

            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="feature-left animate-box" data-animate-effect="fadeInLeft">
                            <span class="icon">
                                <i class="icon-check"></i>
                            </span>
                            <div class="feature-copy">
                                <h3>Payment Ready</h3>
                                <p>Payment for farm produce are always available immediately buys confirmation is click. Farmers can withdraw directly into their various bank account.</p>
                            </div>
                        </div>

                        <div class="feature-left animate-box" data-animate-effect="fadeInLeft">
                            <span class="icon">
                                <i class="icon-check"></i>
                            </span>
                            <div class="feature-copy">
                                <h3>Farm Equipment Support</h3>
                                <p>You can find the nearest farm equipment, ready for hire and also equipment available for sales.</p>
                            </div>
                        </div>

                        <div class="feature-left animate-box" data-animate-effect="fadeInLeft">
                            <span class="icon">
                                <i class="icon-check"></i>
                            </span>
                            <div class="feature-copy">
                                <h3>100%  Secured</h3>
                                <p>We provide security form you farm produce information, payment request system and 2-way authentication with farmer information.</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="gtco-video gtco-bg" style="background-image: url(/images/001.jpg); ">
                            <a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo"><i class="icon-video2"></i></a>
                            <div class="overlay"></div>
                        </div>
                    </div>
                </div>
            </div>

            <br />
            <hr />
            <footer id="gtco-footer" role="contentinfo" style="font-size: 13px;">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-5 gtco-widget">
                            <img src="/images/img-set/logo-transparent-new.png" width="120" height="auto">
                            <br />
                            
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                            <ul class="gtco-footer-links">
                                <li><a href="/about">About</a></li>
                                <li><a href="/help">Help</a></li>
                                <li><a href="/contact">Contact</a></li>
                                <li><a href="/terms">Terms</a></li>
                            </ul>
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                            <ul class="gtco-footer-links">
                                <li><a href="/shops">Shop</a></li>
                                <li><a href="/privacy">Privacy</a></li>
                                <li><a href="/testimonials">Testimonials</a></li>
                                <li><a href="/contact">Help Desk</a></li>
                            </ul>
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                            <ul class="gtco-footer-links">
                                <li><a href="/search-farmers">Find Farmer</a></li>
                                <li><a href="/search-services">Find Service Providers</a></li>
                                <li><a href="/blogs/news">Advertise</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row copyright">
                        <div class="col-md-12">
                            <p class="pull-left">
                                <small class="block">&copy; {{ date('Y') }} Yeelda. All Rights Reserved.</small> 
                                {{-- <small class="block">Designed by <a href="http://cavidel.com/" target="_blank">Cavidel.com</a></small> --}}
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>

        <!-- load carts notifications -->
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
        <script type="text/javascript">
            // users mails 
            var email = $("#user_email").val();
            // load shopping carts notifications
            $.get('/load/shopping/carts/?email='+email, function(data) {
                /*optional stuff to do after success */
                // console.log('hello');
                if(data.total == 0){
                    $(".carts_total").html(`
                        ()
                    `);
                }else{
                    $(".carts_total").html(`
                        (`+data.total+`)
                    `);
                }
                // console.log(data);
            });

            // check pusher for updates
            // load
            // Pusher.logToConsole = true;
            var pusher = new Pusher('{{ env("PUSHER_APP_KEY") }}', {
                encrypted: true,
                cluster: 'eu'
            });

            var channel = pusher.subscribe('add-to-cart');
            channel.bind('Yeelda\\Events\\AddToCart', function(data) {
                // load shopping carts notifications
                $.get('/load/shopping/carts/?email='+email, function(data) {
                    /*optional stuff to do after success */
                    if(data.total == 0){
                        $(".carts_total").html(`
                            ()
                        `);
                    }else{
                        $(".carts_total").html(`
                            (`+data.total+`)
                        `);
                    }
                    // console.log(data);
                });
            });

            function goFarm(){
                window.location.href = '/farm-produce';
            }

            // commodity market
            $.get('/commodities-price/discovery', function (data){
                /* load url json response */
                console.log(data);
            });

            // load price discovery
            $.get('/commodities-price/load-index', function (data){
                /* load url json response */
                $('.ticker').html();
                $.each(data, function(index, val) {
                    /* iterate through array or object */
                    if(val.open > val.previous){
                        var stat = '<i class="fa fa-arrow-up text-success"></i>';
                    }else{
                        var stat = '<i class="fa fa-arrow-down text-danger"></i>';
                    }

                    $('.ticker').append(`
                        <span class="prices">
                            <span class="assets">`+val.asset+`</span> 
                            Open `+stat+` 
                            <span class="price-open">&#8358; `+val.open+`</span>
                            Close 
                            <span class="price-close">&#8358; `+val.close+`</span>, 
                        </span>
                    `);
                });
            });
        </script>
        {{-- required appjs --}}
        <script src="{{asset('js/app.js')}}"></script>
        {{-- xmass js --}}
        {{-- <script src="{{asset('js/xmass.js')}}"></script> --}}
        <!-- Start of Async Drift Code -->
        <script>
            !function() {
              var t;
              if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
              t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
              t.factory = function(e) {
                return function() {
                  var n;
                  return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                };
              }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
              }), t.load = function(t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
              });
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('gdgaepn42buc');
        </script>

        <!-- End of Async Drift Code -->
        <!-- jQuery Easing -->
        <script src="/js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="/js/jquery.waypoints.min.js"></script>
        <!-- Carousel -->
        <script src="/js/owl.carousel.min.js"></script>
        <!-- countTo -->
        <script src="/js/jquery.countTo.js"></script>
        <!-- Magnific Popup -->
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/js/magnific-popup-options.js"></script>
        <!-- Main -->
        <script src="/js/main.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                if(Push.Permission.has()){
                    
                    console.log('Push notication is no !');
                    $('.enable-notify').html(`
                        <br />
                    `);

                }else{
                    
                    $('.enable-notify').html(`
                        <div class="push-notify-link"><i class="fa fa-bell"></i> YEELDA notification is turned off - enable push notification </div>
                    `);
                    console.log('Push notication is off or block ');
                }

                $(".push-notify-link").click(function (){
                    // alert('push notifications has been enable !');
                    Push.create("YEELDA Notification!", {
                        body: "Notificans has been enable !",
                        icon: '/icon.png',
                        timeout: 4000,
                        onClick: function () {
                            window.focus();
                            this.close();
                        }
                    });
                });
            });

            // listent to chat on channels 
            var channel = pusher.subscribe('new-chat-message');
            channel.bind('YEELDA\\Events\\NewChat', function(data) {

                var audio = new Audio('/audio/notify.wav');
                    audio.play();
                
                // console.log(data);
                var logged_email = $("#user_email").val();
                var el = data;
                if(el.email !== logged_email){

                    var name = el.name;
                    var body = el.body;
                    var img  = el.image;

                    Push.create(name, {
                        body: body,
                        icon: img,
                        timeout: 4000,
                        onClick: function () {
                            window.focus();
                            this.close();
                        }
                    });
                }else{
                    // pushNotifyer;
                }
            });
        </script>

            @if(Session::has("verification_success"))
            <script type="text/javascript">
                swal(
                    "success",
                    "Account verification successful!",
                    "success"
                );
            </script>
                
            @elseif(Session::has("verification_error"))
            <script type="text/javascript">
                swal(
                    "Oops",
                    "Failed to verify account activation!",
                    "error"
                );
            </script>
            @endif
    </body>
</html>

