@extends('layouts.skin')

@section('title')
	Future harvest | Yeelda
@endsection


@section('contents')
	<br /><br ><br /><br />
    <br /><br />
    <div class="container">
    	<div class="row">
    		<div class="col-md-12">
    			<h1 class="lead">Future harvest inforamtion</h1>
    		</div>
    	</div>
    </div>

    <!-- Page Content -->
  <div class="container">
    <div class="row">
     	<div class="col-lg-9">
        <br />
        <h1 class="lead">Farm Produce</h1>
        <div class="row">
          <div class="col-md-6">
            <div class="load-products"></div>
          </div>
          <div class="col-md-6">
            <div class="small">
              <span class="lead">Farmer's Profile </span>
              <div class="load-basic"></div>
              <table class="table">
                <tbody class="farmer-info"></tbody>
              </table>
              <hr />
              <span class="lead">Farm Records </span>
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Land Capacity</th>
                    <th>Soil Texture</th>
                    <th>Description & Remark</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>3rd Jan 2018</td>
                    <td>11.04773mtr</td>
                    <td>Good</td>
                    <td>Community Harvest</td>
                  </tr>
                  <tr>
                    <td>2rd Jun 2018</td>
                    <td>11.04773mtr</td>
                    <td>Average</td>
                    <td>Economy brand</td>
                  </tr>
                  <tr>
                    <td>23rd Jul 2018</td>
                    <td>11.04773mtr</td>
                    <td>Quality</td>
                    <td>Economy, best with rigids</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        
      </div>
      <!-- /.col-md-9 -->
    </div>
    <!-- /.row -->
  </div>

	{{-- load future harvest details --}}
	<script type="text/javascript">
		$.get('/load/future-harvest/{{ $id }}', function(data) {
			/*optional stuff to do after success */
			// console.log(data);

			// console.log(data);
			var value = data;
			$(".load-products").append(`
				<div class="col-md-12">
				  <div class="card h-100">
				    <a href="/product/details/`+value.id+`">
				    <img class="card-img-top" src="/uploads/products/`+value.product_image+`" width="350" height="300" alt=""></a>
				    <div class="card-body">
				      <h4 class="small">
				        <a href="/product/details/`+value.id+`"><i class="fa fa-user"></i> `+value.owner_name+` (Farmer)</a>
				      </h4>
				      <h5 class="small"> &#8358; `+value.product_total+`</h5>
				      <p class="small">
				        <table class="table small">
				          <tr>
				            <td>Product</td>
				            <td>`+value.product_name+`</td>
				          </tr>
				          <tr>
				            <td>Unit price</td>
				            <td> &#8358;`+value.product_price+` in QTY: `+value.product_size_no+`</td>
				          </tr>
				          <tr>
				            <td>Product</td>
				            <td>`+value.product_name+`</td>
				          </tr>
				          <tr>
				            <td>Location</td>
				            <td>`+value.product_location+`</td>
				          </tr>
				          <tr>
				            <td>Weight</td>
				            <td>`+value.product_size_no+` in `+value.product_size_type+`</td>
				          </tr>
				          <tr>
				            <td>Contact</td>
				            <td>`+value.owner_contact+`</td>
				          </tr>
				        </table>
				        <span class="small">
				        	<a href="/ask-seller/`+value.owner_email+`"><i class="fa fa-envelope"></i> Send Message</a> -
				        </span> 
				      </p>
				    </div>
				    <div class="card-footer">
				      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
				    </div>
				  </div>
				</div>
			`);

			$(".farmer-info").append(`
				<tr>
				  <td>Name: </td>
				  <td>`+value.owner_name+`</td>
				</tr>

				<tr>
				  <td>Email: </td>
				  <td>`+value.owner_email+`</td>
				</tr>

				<tr>
				  <td>Gender: </td>
				  <td>`+value.owner_gender+`</td>
				</tr>

				<tr>
				  <td>Mobile: </td>
				  <td>`+value.owner_contact+`</td>
				</tr>

				<tr>
				  <td>Office: </td>
				  <td>`+value.owner_office+`</td>
				</tr>

				<tr>
				  <td>Address: </td>
				  <td>`+value.owner_address+`</td>
				</tr>

				<tr>
				  <td>Postal: </td>
				  <td>`+value.owner_zipcode+`</td>
				</tr>

				<tr>
				  <td>Nationality: </td>
				  <td>`+value.owner_country+`</td>
				</tr>
			`);

			if(owner_image == ""){
			$(".load-basic").append(`
			  <img class="img-rounded" src="/uploads/farmers/`+value.owner_image+`" height="200" width="200" alt="farmer+image">
			`);
			}else{
			$(".load-basic").append(`
			  <img class="img-rounded" src="/uploads/farmers/`+value.owner_image+`" height="200" width="200" alt="farmer+image">
			`);
			}
		});
	</script>
@endsection