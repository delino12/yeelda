@extends('layouts.deactivated-skin')

@section('title')
	Account Deactivated | Yeelda
@endsection

{{--  contents --}}
@section('contents')
	<style type="text/css">
		.deactivation-message {
			text-align: center;
			font-size: 36px;
			margin-top: 10%;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="deactivation-message">
					<img src="/images/icon-set/sad.png"> <br />
					Account has been deactivated or suspended !
				</div>
				<p class="small text-center">
					Dear user your account has been suspended, please contact <a class="link" href="mail-to:info@yeelda.com">info@yeelda.com</a> 
					to resolved this issues 
				</p>
			</div>
		</div>
	</div>
@endsection