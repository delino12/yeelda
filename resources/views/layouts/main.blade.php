<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>@yield('title')</title>
  <link rel="icon" href="/images/favicon.png">
  <!-- Bootstrap core CSS-->
  <link href="/extended-assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/extended-assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="/extended-assets/css/sb-admin.css" rel="stylesheet">
  {{-- sweetaleart --}}
  <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

  <!-- Custom Ajax -->
  {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script> --}}
  <script src="/js/jquery/jquery.js"></script>

  <!-- Start of Async Drift Code -->
  <script>
      !function() {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
        t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
          return function() {
            var n;
            return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
          };
        }, t.methods.forEach(function(e) {
          t[e] = t.factory(e);
        }), t.load = function(t) {
          var e, n, o, i;
          e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
          o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
          n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
        });
      }();
      drift.SNIPPET_VERSION = '0.3.1';
      drift.load('gdgaepn42buc');
  </script>

  {{-- introJs --}}
  <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
  <script type="text/javascript" src="/intro-assets/intro.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>

<style type="text/css">
  .form-control {
      font-size: 0.9rem;
    }

    .card {
      border: 0rem; 
      border-radius: 0rem; 
    }
</style>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  @if(Auth::check())
    <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
  @endif

  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #2CA8CC;">
    <a class="navbar-brand" href="/">
      <img src="/images/img-set/logo.png" height="48" width="auto" style="position: absolute;margin-top:-22px;"> 
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarResponsive">
      {{-- side bar menu --}}
      <ul class="navbar-nav navbar-sidenav small" id="exampleAccordion" style="background:url(/images/side-bar.jpg);">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="javascript:void(0);">
            <span class="nav-link-text">
              <div id="profile-image"></div>
            </span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url('farmer/dashboard')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text" data-step="1" data-intro="Welcome to YEELDA. Here is your dashboard you can always return to dashboard when you click on dashboard">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Account">
          <a class="nav-link" href="{{url('farmer/account')}}">
            <i class="fa fa-fw fa-money"></i>
            <span class="nav-link-text" data-step="3" data-intro="Click here to view Payments details.">Wallet</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Transaction">
          <a class="nav-link" href="{{url('farmer/transactions')}}">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text" data-step="4" data-intro="Click here to view all transactions..">Transactions</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Products">
          <a class="nav-link" href="{{url('farmer/products')}}">
            <i class="fa fa-fw fa-tree"></i>
            <span class="nav-link-text">My Produce</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Products">
          <a class="nav-link" href="{{url('produce-marketplace')}}">
            <i class="fa fa-fw fa-tree"></i>
            <span class="nav-link-text" data-step="5" data-intro="Click here to enter farmers market place">Market Place</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Services">
          <a class="nav-link" href="{{url('equipment-marketplace')}}">
            <i class="fa fa-fw fa-cogs"></i>
            <span class="nav-link-text" data-step="6" data-intro="Click here to view all available farm services.">Farm services</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Yeelda Community">
          <a class="nav-link" href="{{url('chat/community')}}">
            <i class="fa fa-users"></i> 
            <span class="nav-link-text" data-step="7" data-intro="Click here to enter YEELDA chat community !"> Chat</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Settings">
          <a class="nav-link" href="{{url('farmer/setting')}}">
            <i class="fa fa-fw fa-cogs"></i>
            <span class="nav-link-text" data-step="8" data-intro="Click here to edit and updates all settings and information..">Setting</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{url('message/service')}}">
            <i class="fa fa-envelope"></i>
            <span class="nav-link-text" data-step="9" data-intro="Click here to enter YEELDA messaging application">Messages</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{url('farmer/promotions')}}">
            <i class="fa fa-shopping-bag"></i>
            <span class="nav-link-text" data-step="10" data-intro="Click here to view your rating and promotions !">Promotions</span>
          </a>
        </li>
      </ul>

      {{-- top bar menu --}}
      <ul class="navbar-nav sidenav-toggler" style="background:url({{asset('images/side-bar.jpg')}});">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left" data-step="11" data-intro="To minimize menu bar click here !"></i>
          </a>
        </li>
      </ul>

      <ul class="navbar-nav ml-auto">
        <div class="load-access-link"></div>
        <li class="nav-item">
          <a class="nav-link mr-lg-2" href="javascript:void(0);" data-step="2" data-intro="This section display your account full name">
            <i class="fa fa-fw fa-user"></i>
            {{ Auth::user()->name }}
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link mr-lg-2 wallet-balance"></a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="{{url('farmer/message')}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">New Messages:</h6>
            <div class="load-mini-msg small"></div>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="{{url('message/service')}}">View all messages</a>
          </div>
        </li>
        
        <li class="nav-item">
          <a href="/carts" class="nav-link">
            <span class="carts_total"></span> <i class="fa fa-fw fa-shopping-cart"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" onclick="startUserGuide()">
            <i class="fa fa-fw fa-question-circle"></i>Help</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="stamp-box">
      <div class="stamp-wrapper">
        <div class="stamp-contents"></div>
      </div>
    </div>

    @yield('contents')
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Yeelda {{ date("Y") }} </small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="{{url('account/logout')}}">Logout</a>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Bootstrap core JavaScript-->
    <script src="/extended-assets/vendor/jquery/jquery.min.js"></script>
    <script src="/extended-assets/vendor/popper/popper.min.js"></script>
    <script src="/extended-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/extended-assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/extended-assets/js/sb-admin.min.js"></script>
    {{-- <script src="/js/jquery/jquery.slim.js"></script> --}}
    <script src="/js/typeahead/typeahead.jquery.js"></script>
    <script src="/js/typeahead/typeahead.bundle.js"></script>
    <script src="/js/typeahead/bloodhound.js"></script>

    {{-- required appjs --}}
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script> --}}
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    <script type="text/javascript">
      // init new pusher
      var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
          encrypted: true,
          cluster: "eu"
      });
      // listent to chat on channels 
      var channel = pusher.subscribe('new-chat-message');
      channel.bind('Yeelda\\Events\\NewChat', function(data) {
        var audio = new Audio('/audio/notify.wav');
            audio.play();
        
        // console.log(data);
        var logged_email = $("#user_email").val();
        var el = data;
        if(el.email !== logged_email){
          var name = el.name;
          var body = el.body;
          var img  = el.image;

          Push.create(name, {
              body: body,
              icon: img,
              timeout: 4000,
              onClick: function () {
                  window.focus();
                  this.close();
              }
          });
        }else{
          // pushNotifyer;
        }
      });

      var channel = pusher.subscribe('add-to-cart');
      channel.bind('Yeelda\\Events\\AddToCart', function(data) {
          // load shopping carts notifications
          $.get('/load/shopping/carts/?email='+email, function(data) {
              /*optional stuff to do after success */
              if(data.total == 0){
                  $(".carts_total").html(``);
              }else{
                  $(".carts_total").html(`
                    `+data.total+`
                  `);
              }
              // console.log(data);
          });
      });

      // load module
      loadMessages();
      loadProfileInfo();
      loadShoppingCart();
      loadStampMessages();
      loadWalletBalance();
      checkUserGuide();
      loadViewAccess();

      // check user guide
      function checkUserGuide() {
        $.get('{{url("start/user/guide")}}', function(data) {
          if(data.status == "success"){
            introJs().start();
          }
        });
      }

      // start user guide
      function startUserGuide() {
        // start introJs
        introJs().start();
      }

      // load message 
      function loadMessages() {
        $.get('{{url('load/messages')}}', function (data){
          // console.log(data);
          $(".load-mini-msg").html("");
          $.each(data, function (index, value){
            if(value.to == $("#user_email").val()){
              $(".load-mini-msg").append(`
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">
                  <strong>`+value.from+`</strong>
                  <span class="small float-right text-muted">`+value.date+`</span>
                  <div class="dropdown-message small">`+value.body+`</div>
                </a>
              `);
            }
          });
        });
      }

      // load farmers image
      function loadProfileInfo() {
        $.get('{{url('farmers/load/profile')}}', function(data) {
          /*optional stuff to do after success */
          if(data.avatar !== null){
            if (data.avatar.indexOf('data:') > -1){
                $('.profile-image').html(`
                    <img height="auto" width="170" src="`+data.avatar+`" alt="placeholder+image">
                `);
            }else if (data.avatar.indexOf('http://') > -1){
                $('.profile-image').html(`
                    <img height="auto" width="170" src="`+data.avatar+`" alt="placeholder+image">
                `);
            }else{
                $('.profile-image').html(`
                    <img height="auto" width="170" src="/uploads/farmers/`+data.avatar+`" alt="placeholder+image">
                `);
            }
          }else{
            $('.profile-image').html(`
                <img height="auto" width="170" src="/images/profile-image.png" alt="placeholder+image">
            `);
          }
        });
      }

      // load shopping carts notifications
      function loadShoppingCart() {
        $.get('{{url('load/shopping/carts')}}', function(data) {
          if(data.total == 0){
              $(".carts_total").html(``);
          }else{
              $(".carts_total").html(data.total);
          }
        });
      }

      // load view access
      function loadViewAccess() {
        $.get('{{url("load/view/access")}}', function(data) {
          $(".load-access-link").html("");
          $.each(data, function(index, val) {
            $(".load-access-link").append(`
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  ${val.group.name}
                </a>
                <div class="dropdown-menu" aria-labelledby="alertsDropdown" data-target="#alertsDropdown">
                  <a class="dropdown-item small" href="{{url('search-farmer/premium')}}"><i class="fa fa-fw fa-users"></i> Farmers</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item small" href="{{url('search-farmer/reports')}}"><i class="fa fa-fw fa-line-chart"></i> Report</a>
                </div>
              </li>
            `);
          });
        });
      }

      // load stamp message
      function loadStampMessages() {
        $.get('{{url('load/stamp/message')}}', function(data) {
          /*optional stuff to do after success */
          if(data.length > 0){
            $('.stamp-wrapper').append(`
              <div class="stamp-close pull-right">
                <a href="javascript:void(0);" onclick="hideStampMessage()">close</a>
              </div>
            `);
          }
          
          $.each(data, function(index, val) {
            $(".stamp-contents").append(`
              <div class="stamp-message">
                <div class="stamp-items">
                  Yeelda: `+val.body+`
                </div>
              </div>
            `);
          });

          // collapse div
          window.setTimeout(function (){
            $(".stamp-box").hide();
          }, 5000);
        });
      }

      // load wallet
      function loadWalletBalance() {
        $.get('{{url('get/wallet/balance')}}', function(data) {
          $(".wallet-balance").html(`Wallet Balance: &#8358;`+data.balance);
        });
      }

      // hide stamp message
      function hideStampMessage() {
        $(".stamp-box").hide();
      }

      // go farm
      function goFarm(){
        window.location.href = '/farm-produce';
      }
    </script>

    @yield('scripts')    
  </div>
</body>
</html>
