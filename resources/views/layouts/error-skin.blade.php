<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Connect Farmers and buyers, sell farm inputs and products." />
    <meta name="keywords" content="Yeelda Farming Hub, make money from your farm" />
    <meta name="author" content="cavidel.com" />
    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Favicon -->
    <link rel="icon" href="/images/favicon.png">

    <!-- Jquery Ajax -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="/css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="/css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="/css/style.css">

    {{-- sweetaleart --}}
    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

    <!-- Modernizr JS -->
    <script src="/js/modernizr-2.6.2.min.js"></script>
    <script src="/js/greetings.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>

    @yield('contents')

    <footer id="gtco-footer" role="contentinfo" style="font-size: 13px;">
            <div class="gtco-container">
                <div class="row row-pb-md">
                    <div class="col-md-4 gtco-widget">
                        <img src="/images/yeelda-logo.png">
                        <p>Brief explanations about yeelda and explanations </p>
                        <p><a href="#">Learn More</a></p>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                        <ul class="gtco-footer-links">
                            <li><a href="/about">About</a></li>
                            <li><a href="/help">Help</a></li>
                            <li><a href="/contact">Contact</a></li>
                            <li><a href="/terms">Terms</a></li>
                        </ul>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                        <ul class="gtco-footer-links">
                            <li><a href="/shops">Shop</a></li>
                            <li><a href="/privacy">Privacy</a></li>
                            <li><a href="/testimonials">Testimonials</a></li>
                            <li><a href="/contact">Help Desk</a></li>
                        </ul>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                        <ul class="gtco-footer-links">
                            <li><a href="/farm-harvest">Find Farmer</a></li>
                            <li><a href="/farm-equipment">Find Service Providers</a></li>
                            <li><a href="/blogs">Advertise</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row copyright">
                    <div class="col-md-12">
                        <p class="pull-left">
                            <small class="block">&copy; {{ date('Y') }} Yeelda. All Rights Reserved.</small> 
                            {{-- <small class="block">Designed by <a href="http://cavidel.com/" target="_blank">Cavidel.com</a></small> --}}
                        </p>
                        <p class="pull-right">
                            <ul class="gtco-social-icons pull-right">
                                <li><a href="#"><i class="icon-twitter" style="color:#9B7A55;"></i></a></li>
                                <li><a href="#"><i class="icon-facebook" style="color:#9B7A55;"></i></a></li>
                                <li><a href="#"><i class="icon-linkedin" style="color:#9B7A55;"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble" style="color:#9B7A55;"></i></a></li>
                            </ul>
                        </p>
                    </div>
                </div>

            </div>
        </footer>
    </div>
    
    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>
    
    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="/js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="/js/jquery.waypoints.min.js"></script>
    <!-- Carousel -->
    <script src="/js/owl.carousel.min.js"></script>
    <!-- countTo -->
    <script src="/js/jquery.countTo.js"></script>
    <!-- Magnific Popup -->
    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/magnific-popup-options.js"></script>
    <!-- Main -->
    <script src="/js/main.js"></script>
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    </body>
</html>

