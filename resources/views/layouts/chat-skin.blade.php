<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="{{asset('admin-assets/styles/shards-dashboards.1.0.0.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin-assets/styles/extras.1.0.0.min.css')}}">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

    {{-- table pagination --}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/chat.css')}}">

    {{-- sweetaleart --}}
    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">
    
  </head>
  <body class="h-100">
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3 text-center mt-4">
                {{-- @if(Request::url() !== env("APP_URL")."/chat/community/home") --}}
                @if(strpos(Request::url(), '/chat/community/home') == true)
                  
                @else
                  <img id="main-logo" class="d-inline-block align-top" src="{{asset('images/new-logo/logo-color.png')}}" alt="YEELDA">
                @endif
            </div>
        </div>
        <div class="row">
          {{-- @if(Request::url() == env("APP_URL")."/chat/community/home") --}}
          @if(strpos(Request::url(), '/chat/community/home') == true)
            <!-- Main Sidebar -->
            <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
              <div class="main-navbar">
                <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
                  <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                    <div class="d-table m-auto">
                      <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 55px;" src="{{asset('images/new-logo/logo-color.png')}}" alt="YEELDA">
                    </div>
                  </a>
                  <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                    <i class="material-icons">&#xE5C4;</i>
                  </a>
                </nav>
              </div>
              <div class="nav-wrapper">
                <ul class="nav flex-column load-chat-users"></ul>
              </div>
            </aside>     
          @endif

          @if(!empty($name))
            {{-- @if(Request::url() == env("APP_URL")."/chat/community/home/".$name) --}}
            @if(strpos(Request::url(), '/chat/community/home/'.$name) == true)
              <!-- Main Sidebar -->
              <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
                <div class="main-navbar">
                  <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
                    <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                      <div class="d-table m-auto">
                        <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 55px;" src="{{asset('images/new-logo/logo-color.png')}}" alt="YEELDA">
                      </div>
                    </a>
                    <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                      <i class="material-icons">&#xE5C4;</i>
                    </a>
                  </nav>
                </div>
                <div class="nav-wrapper">
                  <ul class="nav flex-column load-group-chat-users"></ul>
                </div>
              </aside>     
            @endif
          @endif
          
          <main class="main-content col-md-12">
              @yield('contents')
          </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script> --}}
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    @yield('scripts')
  </body>
</html>