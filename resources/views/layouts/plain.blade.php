<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>@yield('title')</title>
  <link rel="icon" href="/images/favicon.png">
  <!-- Bootstrap core CSS-->
  <link href="/extended-assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/extended-assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="/extended-assets/css/sb-admin.css" rel="stylesheet">
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> --}}
  {{-- <script src="/js/jquery.min.js"></script> --}}
  <script src="/extended-assets/vendor/jquery/jquery.min.js"></script>
  {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> --}}
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

  {{-- sweetaleart --}}
  <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

  {{-- introJs --}}
  <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
  <script type="text/javascript" src="/intro-assets/intro.js"></script>
</head>
  <style type="text/css">
    .stamp-message {
        background-color: #F8B655;
        color:#FFF;
        border-radius: 2px;
        padding: 0.6em;
        font-size: 12px;
        margin-bottom: 5px;
        box-shadow: 1px 1px 2px 1px #CCC;
      }

      .stamp-wrapper {
        position: absolute;
        top: 75px;
        margin-right: 10px;
        margin-left: 60%;
        
      }
      .stamp-close {
        font-size: 12px;
        padding: 0.8em;
        font-weight: bolder;
        cursor: pointer;
      }
  </style>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
    @if(Auth::check())
      <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
      <input type="hidden" id="token" value="{{ csrf_token() }}">
    @endif

    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #2CA8CC;">
      <a class="navbar-brand" href="/"><img src="/images/img-set/logo.png" height="48" width="auto" style="position: absolute;margin-top:-22px;"> </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">

        {{-- sidebar menu --}}
        <ul class="navbar-nav navbar-sidenav small" id="exampleAccordion" style="background:url(/images/side-bar.jpg);">
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="javascript:void(0);">
              <span class="nav-link-text">
                <div class="profile-image"></div>
              </span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="/investors/dashboard">
              <i class="fa fa-fw fa-dashboard"></i>
              <span class="nav-link-text" data-step="1" data-intro="Welcome to YEELDA. Here is your dashboard you can always return to dashboard when you click on dashboard">Dashboard</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Payments">
            <a class="nav-link" href="/investors/account">
              <i class="fa fa-fw fa-money"></i>
              <span class="nav-link-text" data-step="3" data-intro="Click here to update your payment information and account details">Account</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Transaction">
            <a class="nav-link" href="/investors/transactions">
              <i class="fa fa-fw fa-table"></i>
              <span class="nav-link-text" data-step="4" data-intro="Click here to view all Transactions.">Transactions</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Yeelda Community">
            <a class="nav-link" href="/chat/community">
              <i class="fa fa-comments-o"></i>
              <span class="nav-link-text" data-step="5" data-intro="Get updates from YEELDA community, click to join the Chat Room">Chat</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Farm produces">
            <a class="nav-link" href="/produce-marketplace">
              <i class="fa fa-tree"></i>
              <span class="nav-link-text" data-step="6" data-intro="Click here to view all farmers produces">Farm produces</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Farm services">
            <a class="nav-link" href="/equipment-marketplace">
              <i class="fa fa-truck"></i>
              <span class="nav-link-text" data-step="7" data-intro="Click here to view all farm services">Farm Services</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Settings">
            <a class="nav-link" href="/investors/setting">
              <i class="fa fa-fw fa-cogs"></i>
              <span class="nav-link-text" data-step="8" data-intro="Click the setting link to update account settings">Setting</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messaging service">
            <hr />
            <a class="nav-link" href="/message/service">
              <i class="fa fa-envelope"></i>
              <span class="nav-link-text" data-step="9" data-intro="Click here for a Quick Messaging system. Send messages to YEELDA users">Messages</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Promotions">
            <a class="nav-link" href="/investors/promotions">
              <i class="fa fa-shopping-bag"></i>
              <span class="nav-link-text" data-step="10" data-intro="Click Promotions to view Rating and Bonus">Promotions</span>
            </a>
          </li>
        </ul>

        <ul class="navbar-nav sidenav-toggler" style="background:url(/images/side-bar.jpg);">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>

        {{-- topbar menu --}}
        <ul class="navbar-nav ml-auto">
          <div class="load-access-link"></div>
          <li class="nav-item">
            <a class="nav-link mr-lg-2" href="javascript:void(0);" data-step="2" data-intro="This section display your account full name">
              <i class="fa fa-fw fa-user"></i>
              {{ Auth::user()->name }}
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link mr-lg-2">
              Wallet Balance: &#8358; 0.00
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="/investor/message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-envelope"></i>
              <span class="d-lg-none">Messages
                <span class="badge badge-pill badge-primary">12 New</span>
              </span>
              <span class="indicator text-primary d-none d-lg-block">
                <i class="fa fa-fw fa-circle"></i>
              </span>
            </a>
            <div class="dropdown-menu" aria-labelledby="messagesDropdown">
              <h6 class="dropdown-header">New Messages:</h6>
              <div class="load-mini-msg"></div>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item small" href="/message/service">View all messages</a>
            </div>
          </li>
          
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-bell"></i>
              <span class="d-lg-none">Alerts
                <span class="badge badge-pill badge-warning">6 New</span>
              </span>
              <span class="indicator text-warning d-none d-lg-block">
                <i class="fa fa-fw fa-circle"></i>
              </span>
            </a>
            <div class="dropdown-menu" aria-labelledby="alertsDropdown" data-target="#alertsDropdown">
              <h6 class="dropdown-header">New Alerts:</h6>
              <div class="dropdown-divider"></div>
              
              <div class="dropdown-divider"></div>
              <a class="dropdown-item small" href="#">View all alerts</a>
            </div>
          </li>
          <li class="nav-item">
            <a href="/carts" class="nav-link">
              <span class="carts_total"></span> <i class="fa fa-fw fa-shopping-cart"></i>
            </a>
          </li>
          <li class="nav-item">
          <a class="nav-link" onclick="startUserGuide()">
            <i class="fa fa-fw fa-question-circle"></i>Help</a>
        </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
              <i class="fa fa-fw fa-sign-out"></i>Logout</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="content-wrapper">
      <div class="stamp-box">
        <div class="stamp-wrapper">
          <div class="stamp-contents"></div>
        </div>
      </div>

      @yield('contents')
      <!-- /.container-fluid-->
      <!-- /.content-wrapper-->
      <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright © Yeelda {{ date("Y") }} </small>
          </div>
        </div>
      </footer>
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>
      <!-- Logout Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title small" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body small">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn btn-primary" href="/account/logout/investor">Logout</a>
            </div>
          </div>
        </div>
      </div>
      @if(Auth::check())
          <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
      @endif
    </div>


    <!-- load carts notifications -->
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script src="/extended-assets/vendor/jquery/jquery.min.js"></script>
    <script src="/extended-assets/vendor/popper/popper.min.js"></script>
    <script src="/extended-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/extended-assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/extended-assets/js/sb-admin.min.js"></script>
    {{-- required appjs --}}
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    <script type="text/javascript">
      // check module
      loadWalletBalance();
      checkUserGuide();
      loadViewAccess();

      // load view access
      function loadViewAccess() {
        $.get('{{url("load/view/access")}}', function(data) {
          $(".load-access-link").html("");
          $.each(data, function(index, val) {
            $(".load-access-link").append(`
              <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  ${val.group.name}
                </a>
                <div class="dropdown-menu" aria-labelledby="alertsDropdown" data-target="#alertsDropdown">
                  <a class="dropdown-item small" href="{{url('search-farmer/premium')}}"><i class="fa fa-fw fa-users"></i> Farmers</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item small" href="{{url('search-farmer/reports')}}"><i class="fa fa-fw fa-line-chart"></i> Report</a>
                </div>
              </li>
            `);
          });
        });
      }

      // check user guide
      function checkUserGuide() {
        $.get('{{url("start/user/guide")}}', function(data) {
          if(data.status == "success"){
            introJs().start();
          }
        });
      }

      // start user guide
      function startUserGuide() {
        // start introJs
        introJs().start();
      }

      // users mails 
      var email = $("#user_email").val();
      var id    = $("#user_id").val();

      $.get('/load/messages/', function (data){
        $(".load-mini-msg").html("");
        $.each(data, function (index, value){
          if(value.to == email){
            $(".load-mini-msg").append(`
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <strong>`+value.from+`</strong>
                <span class="small float-right text-muted">`+value.date+`</span>
                <div class="dropdown-message small">`+value.body+`</div>
              </a>
            `);
          }
        });
      });

      // load shopping carts notifications
      $.get('/load/shopping/carts/?email='+email, function(data) {
          if(data.total == 0){
            $(".carts_total").html(``);
          }else{
            $(".carts_total").html(data.total);
          }
      });

       // load stamp message
      $.get('/load/stamp/message/', function(data) {
        if(data.length > 0){
          $('.stamp-wrapper').append(`
            <div class="stamp-close pull-right">
              <a href="javascript:void(0);" onclick="hideStampMessage()">close</a>
            </div>
          `);
        }
        
        $.each(data, function(index, val) {
          $(".stamp-contents").append(`
            <div class="stamp-message">
              <div class="stamp-items">
                Yeelda: `+val.body+`
              </div>
            </div>
          `);
        });
      });

      // hide stamp message
      function hideStampMessage() {
        $(".stamp-box").hide();
        let data = {
          _token: '{{ csrf_token() }}',
          email:email
        }

        $.post('/update/stamp/message', data, function(data, textStatus, xhr) {
          console.log(data);
        });
      }

      var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
          encrypted: false,
          cluster: "eu"
      });

      var cat_channel = pusher.subscribe('add-to-cart');
      cat_channel.bind('Yeelda\\Events\\AddToCart', function(data) {
          $.get('/load/shopping/carts/?email='+email, function(data) {
              if(data.total == 0){
                  $(".carts_total").html(``);
              }else{
                  $(".carts_total").html(data.total);
              }
          });
      });

      // listent to chat on channels 
      var chat_channel = pusher.subscribe('new-chat-message');
      chat_channel.bind('Yeelda\\Events\\NewChat', function(data) {
        var audio = new Audio('/audio/notify.wav');
            audio.play();
      
        var logged_email = $("#user_email").val();
        var el = data;
        if(el.email !== logged_email){
          var name = el.name;
          var body = el.body;
          var img  = el.image;

          Push.create(name, {
              body: body,
              icon: img,
              timeout: 4000,
              onClick: function () {
                  window.focus();
                  this.close();
              }
          });
        }
      });

      function goFarm(){
          window.location.href = '/farm-produce';
      }

      // load wallet
      function loadWalletBalance() {
        $.get('{{url('get/wallet/balance')}}', function(data) {
          $(".wallet-balance").html(`Wallet Balance: &#8358;`+data.balance);
        });
      }
    </script>

    @yield('scripts')
  </body>
</html>
