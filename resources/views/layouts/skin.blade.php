<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Yeelda</title>
        <link rel="icon" href="/images/favicon.png">
        <meta name="google-site-verification" content="pzKPeRDO7Y5TiryGMjnyg7JQr8h5DMsOgs2RZuw0grw" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Connect Farmers, Services Providers and buyers, sell farm inputs and farm produce." />
        <meta name="keywords" content="Yeelda Farming Hub, make money from your farm" />
        <meta name="author" content="cavidel.com" />
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="Yeelda, No.1 Africa Agricultural Hub"/>
        <meta property="og:image" content="http://yeelda.com/images/010.jpg"/>
        <meta property="og:url" content="http://yeelda.com"/>
        <meta property="og:site_name" content="Yeelda"/>
        <meta property="og:description" content="Connecting Farmers, Services Providers and buyers, sell farm inputs and farm produce."/>

        {{-- twitter --}}
        <meta name="twitter:title" content="Yeelda, No.1 Africa Agricultural Hub" />
        <meta name="twitter:image" content="http://yeelda.com/images/010.jpg" />
        <meta name="twitter:url" content="http://yeelda.com" />
        <meta name="twitter:card" content="Connecting Farmers, Services Providers and buyers, sell farm inputs and farm produce." />

        <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- Animate.css -->
        <link rel="stylesheet" href="/css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="/css/icomoon.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="/css/bootstrap.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="/css/magnific-popup.css">

        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">

        <!-- Theme style  -->
        <link rel="stylesheet" href="/css/style.css">

        {{-- sweetaleart --}}
        <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

        <!-- Modernizr JS -->
        <script src="/js/modernizr-2.6.2.min.js"></script>
        <script src="/js/greetings.js"></script>

        {{-- added AOS --}}
        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

        {{-- google analytics --}}
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118912888-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-118912888-1');
        </script>


        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/toast.css">
        <script src="js/toast.js"></script>
    </head>
    <body>
        @if(Auth::check())
            <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
        @endif
        <div class="gtco-loader"></div>
        <div class="page">
            @include('site-components.menu-bar')
            <div style="height: 100px;"></div>

            @yield('contents')

            <div style="height: 120px;"></div>
            <footer id="gtco-footer" role="contentinfo" style="font-size: 13px;">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-md-5 gtco-widget">
                            <img src="/images/img-set/logo-transparent-new.png" width="120" height="auto">
                            <br />
                            
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                            <ul class="gtco-footer-links">
                                <li><a href="/about">About</a></li>
                                <li><a href="/help">Help</a></li>
                                <li><a href="/contact">Contact</a></li>
                                <li><a href="/terms/conditions">Terms</a></li>
                            </ul>
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                            <ul class="gtco-footer-links">
                                <li><a href="/shops">Shop</a></li>
                                <li><a href="/privacy">Privacy</a></li>
                                <li><a href="/testimonials">Testimonials</a></li>
                                <li><a href="/contact">Help Desk</a></li>
                            </ul>
                        </div>

                        <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                            <ul class="gtco-footer-links">
                                <li><a href="/search-farmers">Find Farmer</a></li>
                                <li><a href="/search-services">Find Service Providers</a></li>
                                <li><a href="/blogs/news">Advertise</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row copyright">
                        <div class="col-md-12">
                            <p class="pull-left">
                                <small class="block">
                                    &copy; {{ date('Y') }} Yeelda. All Rights Reserved.
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
    
        <script type="text/javascript">
            // load price discovery
            $.get('/commodities-price/load-index', function (data){
                /* load url json response */
                $('.ticker').html();
                $.each(data, function(index, val) {
                    /* iterate through array or object */
                    if(val.open > val.previous){
                        var stat = '<i class="fa fa-arrow-up text-success"></i>';
                    }else{
                        var stat = '<i class="fa fa-arrow-down text-danger"></i>';
                    }

                    $('.ticker').append(`
                        <span class="prices">
                            <span class="assets">`+val.asset+`</span> 
                            Open `+stat+` 
                            <span class="price-open">&#8358; `+val.open+`</span>
                            Close 
                            <span class="price-close">&#8358; `+val.close+`</span>, 
                        </span>
                    `);
                });
            });
        </script>
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="/js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        {{-- <script src="/js/bootstrap.min.js"></script> --}}
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="/js/jquery.waypoints.min.js"></script>
        <!-- Carousel -->
        <script src="/js/owl.carousel.min.js"></script>
        <!-- countTo -->
        <script src="/js/jquery.countTo.js"></script>
        <!-- Magnific Popup -->
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/js/magnific-popup-options.js"></script>
        <!-- Main -->
        <script src="/js/main.js"></script>
        
        <!-- Sweetalert -->
        {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script> --}}
        <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>

        {{-- required appjs --}}
        <script src="{{asset('js/app.js')}}"></script>

        <!-- load carts notifications -->
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
        <script type="text/javascript">
            // users mails 
            var email = $("#user_email").val();
            loadShoppingCart();
            loadDriftChatLib();

            // load shopping carts
            function loadShoppingCart() {
                // load shopping carts notifications
                $.get('/load/shopping/carts/?email='+email, function(data) {
                    /*optional stuff to do after success */
                    // console.log('hello');
                    if(data.total == 0){
                        $(".carts_total").html(`
                            ()
                        `);
                    }else{
                        $(".carts_total").html(`
                            (`+data.total+`)
                        `);
                    }
                });
            }
            
            // Pusher.logToConsole = true;
            var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
                encrypted: false,
                cluster: "eu"
            });

            var channel = pusher.subscribe('add-to-cart');
            channel.bind('Yeelda\\Events\\AddToCart', function(data) {
                // load shopping carts notifications
                $.get('/load/shopping/carts/?email='+email, function(data) {
                    /*optional stuff to do after success */
                    if(data.total == 0){
                        $(".carts_total").html(`
                            ()
                        `);
                    }else{
                        $(".carts_total").html(`
                            (`+data.total+`)
                        `);
                    }
                    // console.log(data);
                });
            });

            // listent to chat on channels 
            var channel = pusher.subscribe('new-chat-message');
            channel.bind('Yeelda\\Events\\NewChat', function(data) {

                var audio = new Audio('/audio/notify.wav');
                    audio.play();
                
                // console.log(data);
                var logged_email = $("#user_email").val();
                var el = data;
                if(el.email !== logged_email){

                    var name = el.name;
                    var body = el.body;
                    var img  = el.image;

                    Push.create(name, {
                        body: body,
                        icon: img,
                        timeout: 4000,
                        onClick: function () {
                            window.focus();
                            this.close();
                        }
                    });
                }else{
                    // pushNotifyer;
                }
            });

            // load chats
            function loadDriftChatLib() {
                !function() {
                  var t;
                  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
                  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
                  t.factory = function(e) {
                    return function() {
                      var n;
                      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                    };
                  }, t.methods.forEach(function(e) {
                    t[e] = t.factory(e);
                  }), t.load = function(t) {
                    var e, n, o, i;
                    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
                    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
                    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
                  });
                }();
                drift.SNIPPET_VERSION = '0.3.1';
                drift.load('gdgaepn42buc');
            }

            $(document).ready(function() {
                if(Push.Permission.has()){
                    console.log('Push notication is no !');
                    $('.enable-notify').html(`
                        <br />
                    `);

                }else{
                    
                    $('.enable-notify').html(`
                        <div class="push-notify-link"><i class="fa fa-bell"></i> YEELDA notification is turned off - enable push notification </div>
                    `);
                    console.log('Push notication is off or block ');
                }

                $(".push-notify-link").click(function (){
                    // alert('push notifications has been enable !');
                    Push.create("YEELDA Notification!", {
                        body: "Notificans has been enable !",
                        icon: '/icon.png',
                        timeout: 4000,
                        onClick: function () {
                            window.focus();
                            this.close();
                        }
                    });
                });
            });
        </script>
    </body>
</html>

