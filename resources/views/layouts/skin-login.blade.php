<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>
        <link rel="icon" href="/images/favicon.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Connect Farmers and buyers, sell farm inputs and products." />
        <meta name="keywords" content="Yeelda Farming Hub, make money from your farm" />
        <meta name="author" content="cavidel.com" />
        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>

        
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />


        {{-- bootstrap --}}
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- Animate.css -->
        <link rel="stylesheet" href="/css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="/css/icomoon.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="/css/bootstrap.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="/css/magnific-popup.css">

        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">

        <!-- Theme style  -->
        <link rel="stylesheet" href="/css/style.css">

        {{-- sweetaleart --}}
        <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

        {{-- jquery --}}
        <script src="/js/jquery.min.js"></script>

        <!-- Modernizr JS -->
        <script src="/js/modernizr-2.6.2.min.js"></script>
        <script src="/js/greetings.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <section class="" style="font-size: 13px;">
            <nav class="navbar navbar-fixed-top" style="background-color:#FFFFFF;">
                <div class="top-nav">
                    <div class="top-nav-item" align="right">
                    <br />
                </div>
                </div>
                <div class="container">
                    <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-navicon"></i> Menu
                  </button>
                  <a href="/">
                      <img src="/images/img-set/logo-transparent-new.png" height="48" width="auto" style="position: absolute;margin-top:4px;">
                  </a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class=" dropdown">
                                <a class="dropdown-toggle dropbtn" data-toggle="dropdown" href="/farm-equipment">
                                    <i class="fa fa-leaf"></i> Our Offerings
                                </a>
                                 <ul class="dropdown-menu dropdown-content" style="background-color:rgba(000,000,000,0.70);">
                                    <li><a style="font-weight: 100px;" href="/farm-produce"><i class="fa fa-tree"></i> Farm Produce</a></li>
                                    <li style="border: 1px solid #FFF;"></li>
                                    {{-- <li><a style="font-weight: 100px;" href="/farm-equipment"><i class="fa fa-cogs"></i> Farm Equipment</a></li> --}}
                                    <li style="border: 1px solid #FFF;"></li>
                                    <li><a style="font-weight: 100px;" href="/select-services"><i class="fa fa-truck"></i> Farm Services</a></li>
                                </ul>
                            </li>
                            <li class="nav-item"><a href="/chat/community"><i class="fa fa-comments-o"></i> Community</a></li>
                            @if(Auth::guard('farmer')->check())
                            <input type="hidden" id="user_email" value="{{ Auth::guard('farmer')->user()->email }}">
                            <li class="nav-item">
                                <a href="/farmer/dashboard">
                                    <i class="fa fa-dashboard"></i> Dashboard
                                </a>
                            </li>
                            <li class="nav-item"><a href="/account/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                            @elseif(Auth::guard('investor')->check())
                            <input type="hidden" id="user_email" value="{{ Auth::guard('investor')->user()->email }}">
                            <li class="nav-item">
                                <a href="/investors/dashboard">
                                    <i class="fa fa-dashboard"></i> Dashboard
                                </a>
                            </li>
                            <li class="nav-item"><a href="/account/logout/investor"><i class="fa fa-sign-out"></i> Logout</a></li> 
                            @elseif(Auth::guard('sp')->check())
                            <input type="hidden" id="user_email" value="{{ Auth::guard('sp')->user()->email }}">
                            <li class="nav-item">
                                <a href="/service-provider/dashboard">
                                    <i class="fa fa-dashboard"></i> Dashboard
                                </a>
                            </li> 
                            <li class="nav-item"><a href="/account/logout/sp"><i class="fa fa-sign-out"></i> Logout</a></li> 
                            @else
                            <li class="nav-item"><a href="/account/login"><i class="fa fa-sign-in"></i> Login </a></li>
                            {{-- <li class="nav-item"><a style="color:#CCB19E;" href="/account/register"><i class="fa fa-edit"></i> Register </a> </li> --}}
                            @endif
                            {{-- <li class="nav-item dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                                    <i class="fa fa-info"></i> 
                                </a>
                                <ul class="dropdown-menu dropdown-content" style="background-color:#9ACC3F;">
                                    <li><a style="color: #fff;" href="/about#what-is-yeelda"><i class="fa fa-question"></i> What is yeelda</a></li>
                                    <li><a style="color: #fff;" href="/about#our-service"><i class="fa fa-street-view"></i> Our Services</a></li>
                                    <li><a style="color: #fff;" href="/about#our-team"><i class="fa fa-sitemap"></i> Our Team</a></li>
                                </ul>
                            </li> --}}
                            <li class=" dropdown">
                                <a class="dropdown-toggle dropbtn" data-toggle="dropdown" href="/farm-equipment">
                                    <i class="fa fa-info"></i> About us
                                </a>
                                <ul class="dropdown-menu dropdown-content" style="background-color:rgba(000,000,000,0.70);">
                                    <li><a style="font-weight: 100px;" href="/about#what-is-yeelda"><i class="fa fa-question"></i> What is yeelda</a></li>
                                    <li style="border: 1px solid #FFF;"></li>

                                    <li><a style="font-weight: 100px;" href="/about#our-service"><i class="fa fa-street-view"></i> Our Service</a></li>
                                    <li style="border: 1px solid #FFF;"></li>

                                    <li><a style="font-weight: 100px;" href="/about#our-team"><i class="fa fa-sitemap"></i> Our Team</a></li>
                                    {{-- <li style="border: 1px solid #FFF;"></li> --}}
                                    <li><a style="color: #fff;"  href="/contact"><i class="fa fa-envelope"></i> Contact</a></li>
                                    <li><a style="color: #fff;"  href="/faq"><i class="fa fa-question-circle"></i> FAQ</a></li>
                                </ul>
                            </li>
                            {{-- <li class="nav-item"><a href="/contact"><i class="fa fa-envelope"></i> Contact</a></li> --}}
                            <li class="nav-item">
                                <a href="/blogs/news"><i class="fa fa-twitch"></i> Blog</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a href="/faq"><i class="fa fa-question-circle"></i> FAQ</a>
                            </li> --}}
                            <li class="nav-item">
                                <a href="/carts"><span class="carts_total"></span><i class="fa fa-shopping-cart"></i> Cart</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="price-discovery" style="background-color:#FFFFFF;color:#CBB39B;">
                    <marquee direction="right" speed="slow">
                        <span class="ticker"></span>
                    </marquee>
                </div>
            </nav> 
        </section>
        <br /><br />
        <div style="background-image: url(/images/rice-farm-image.jpg);top:-20px; position: relative;">
            <div style="z-index: 10; background-color: rgba(000,000,000,0.57);"> 
                @yield('contents')
            </div>
        </div>
        
        {{-- footer --}}
        <footer id="gtco-footer" role="contentinfo" style="font-size: 13px;">
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-5 gtco-widget">
                        <img src="/images/img-set/logo-transparent-new.png" width="120" height="auto">
                        <br />
                        
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                        <ul class="gtco-footer-links">
                            <li><a href="/about">About</a></li>
                            <li><a href="/help">Help</a></li>
                            <li><a href="/contact">Contact</a></li>
                            <li><a href="/terms/conditions">Terms</a></li>
                        </ul>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                        <ul class="gtco-footer-links">
                            <li><a href="/shops">Shop</a></li>
                            <li><a href="/privacy">Privacy</a></li>
                            <li><a href="/testimonials">Testimonials</a></li>
                            <li><a href="/contact">Help Desk</a></li>
                        </ul>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1">
                        <ul class="gtco-footer-links">
                            <li><a href="/search-farmers">Find Farmer</a></li>
                            <li><a href="/search-services">Find Service Providers</a></li>
                            <li><a href="/blogs/news">Advertise</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row copyright">
                    <div class="col-md-12">
                        <p class="pull-left">
                            <small class="block">&copy; {{ date('Y') }} Yeelda. All Rights Reserved.</small> 
                            {{-- <small class="block">Designed by <a href="http://cavidel.com/" target="_blank">Cavidel.com</a></small> --}}
                        </p>
                    </div>
                </div>
            </div>
        </footer>

        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>

        {{-- commodities --}}
        <script type="text/javascript">
            // load price discovery
            $.get('/commodities-price/load-index', function (data){
                /* load url json response */
                $('.ticker').html();
                $.each(data, function(index, val) {
                    /* iterate through array or object */
                    if(val.open > val.previous){
                        var stat = '<i class="fa fa-arrow-up text-success"></i>';
                    }else{
                        var stat = '<i class="fa fa-arrow-down text-danger"></i>';
                    }

                    $('.ticker').append(`
                        <span class="prices">
                            <span class="assets">`+val.asset+`</span> 
                            Open `+stat+` 
                            <span class="price-open">&#8358; `+val.open+`</span>
                            Close 
                            <span class="price-close">&#8358; `+val.close+`</span>, 
                        </span>
                    `);
                });
            });
        </script>

        <!-- Start of Async Drift Code -->
        <script>
            !function() {
              var t;
              if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
              t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
              t.factory = function(e) {
                return function() {
                  var n;
                  return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                };
              }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
              }), t.load = function(t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
              });
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('gdgaepn42buc');
        </script>
        
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="/js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="/js/jquery.waypoints.min.js"></script>
        <!-- Carousel -->
        <script src="/js/owl.carousel.min.js"></script>
        <!-- countTo -->
        <script src="/js/jquery.countTo.js"></script>
        <!-- Magnific Popup -->
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/js/magnific-popup-options.js"></script>
        {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script> --}}
        <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
        <!-- Main -->
        <script src="/js/main.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
        <!---->
        <script>
          (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
          function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
          e=o.createElement(i);r=o.getElementsByTagName(i)[0];
          e.src='//www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
          ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>

        @yield('scripts')
    </body>
</html>

