<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="{{asset('admin-assets/styles/shards-dashboards.1.0.0.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin-assets/styles/extras.1.0.0.min.css')}}">
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.css">

    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.css"> --}}

    <style type="text/css">
      .dataTables_wrapper .dataTables_paginate .paginate_button {
          box-sizing: border-box;
          display: inline-block;
          min-width: 1.5em;
          padding: 0.5em 1em;
          margin-left: 2px;
          text-align: center;
          text-decoration: none !important;
          cursor: pointer;
           color: #333 !important; 
          border: 1px solid transparent;
          border-radius: 2px;
      }

      div.dataTables_wrapper div.dataTables_paginate ul.pagination {
          margin: 2px 0;
          white-space: nowrap;
      }

      div.dataTables_wrapper div.dataTables_paginate {
          margin: 0;
          white-space: nowrap;
          text-align: right;
      }

      .dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_filter, .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing, .dataTables_wrapper .dataTables_paginate {
          /*color: #333;*/
      }
      .dataTables_wrapper .dataTables_paginate {
          float: right;
          text-align: right;
          padding-top: 0.25em;
      }

      .load-bar {
        position: relative;
        margin-top: 20px;
        width: 100%;
        height: 3px;
        background-color: #2CA7CB;
      }
      .bar {
        content: "";
        display: inline;
        position: absolute;
        width: 0;
        height: 100%;
        left: 50%;
        text-align: center;
      }
      .bar:nth-child(1) {
        background-color: #87B646;
        animation: loading 3s linear infinite;
      }
      .bar:nth-child(2) {
        background-color: #F7B857;
        animation: loading 3s linear 1s infinite;
      }
      .bar:nth-child(3) {
        background-color: #CBB39B;
        animation: loading 3s linear 2s infinite;
      }
      @keyframes loading {
          from {left: 50%; width: 0;z-index:100;}
          33.3333% {left: 0; width: 100%;z-index: 10;}
          to {left: 0; width: 100%;}
      }
    </style>
  </head>
  <body class="h-100">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <style type="text/css">
      .fixed-table-container {
        border: none;
      }
    </style>
    <div class="container-fluid">
      <div class="row">
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="{{ url('admin/dashboard') }}" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 55px;" src="{{asset('images/new-logo/logo-color.png')}}" alt="YEELDA">
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>
          <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless ml-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
          </form>
          <div class="nav-wrapper">
            <ul class="nav flex-column">
              @if(Request::url() === env("APP_URL").'/admin/dashboard')
                <li class="nav-item active">
                  <a class="nav-link" href="{{url('admin/dashboard')}}">
                    <i class="material-icons">edit</i>
                    <span>Dashboard</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/dashboard')}}">
                    <i class="material-icons">edit</i>
                    <span>Dashboard</span>
                  </a>
                </li>
              @endif

              @if(Request::url() === env("APP_URL").'/admin/financials')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/financials')}}">
                    <i class="material-icons">account_balance_wallet</i>
                    <span>Financials</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link " href="{{url('admin/financials')}}">
                    <i class="material-icons">account_balance_wallet</i>
                    <span>Financials</span>
                  </a>
                </li>
              @endif

              @if(Request::url() === env("APP_URL").'/admin/industrials')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/industrials')}}">
                    <i class="material-icons">business</i>
                    <span>Industrials</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link " href="{{url('admin/industrials')}}">
                    <i class="material-icons">business</i>
                    <span>Industrials</span>
                  </a>
                </li>
              @endif

              @if(Request::url() === env("APP_URL").'/admin/managment')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/managment')}}">
                    <i class="material-icons">device_hub</i>
                    <span>Operations</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link " href="{{url('admin/managment')}}">
                    <i class="material-icons">device_hub</i>
                    <span>Operations</span>
                  </a>
                </li>
              @endif

              @if(Request::url() === env("APP_URL").'/admin/messaging/service')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/messaging/service')}}">
                    <i class="material-icons">message</i>
                    <span>Messaging</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link " href="{{url('admin/messaging/service')}}">
                    <i class="material-icons">message</i>
                    <span>Messaging</span>
                  </a>
                </li>
              @endif     
              
              @if(Request::url() === env("APP_URL").'/admin/products')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/products')}}">
                    <i class="material-icons">table_chart</i>
                    <span>Produce</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link " href="{{url('admin/products')}}">
                    <i class="material-icons">table_chart</i>
                    <span>Produce</span>
                  </a>
                </li>
              @endif

              @if(Request::url() === env("APP_URL").'/admin/data-capture')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/data-capture')}}">
                    <i class="material-icons">assignment_ind</i>
                    <span>ODCA</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/data-capture')}}">
                    <i class="material-icons">assignment_ind</i>
                    <span>ODCA</span>
                  </a>
                </li>
              @endif 

              @if(Request::url() === env("APP_URL").'/admin/statistics')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/statistics')}}">
                    <i class="material-icons">show_chart</i>
                    <span>Statistics</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/statistics')}}">
                    <i class="material-icons">show_chart</i>
                    <span>Statistics</span>
                  </a>
                </li>
              @endif

              @if(Request::url() === env("APP_URL").'/admin/reports')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/reports')}}">
                    <i class="material-icons">bar_chart</i>
                    <span>Reports</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="{{url('admin/reports')}}">
                    <i class="material-icons">bar_chart</i>
                    <span>Reports</span>
                  </a>
                </li>
              @endif   
              
              @if(Request::url() === env("APP_URL").'/admin/settings')
                <li class="nav-item active">
                  <a class="nav-link " href="{{url('admin/settings')}}">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link " href="{{url('admin/settings')}}">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                  </a>
                </li>
              @endif
            </ul>
          </div>
        </aside>
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
            @yield('contents')
        </main>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.js"></script>
    <script src="{{asset('js/select2.js')}}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.js"></script> --}}
    <script type="text/javascript">
      loadAssocRequest();
      loadUsersFarmers();
      loadUserBuyers();
      loadUsersService();
      loadTransactionStatistics();
      loadAllNotifications();
      loadNotificationsUpdate();

      function loadTransactionStatistics() {
        $.get('{{url('admin/load/trans-statistic')}}', function(data) {
          // console.log(data);
          $("#pt").html(`
            &#8358;${data.pending_transaction}
          `);
          $("#st").html(`
            &#8358;${data.success_transaction}
          `);

          $("#tt").html(`
            &#8358;${data.total_transaction}
          `);

          $("#ft").html(`
            &#8358;${data.failed_transaction}
          `);

          $("#at").html(`
            &#8358;${data.approved_transaction}
          `);
        }); 
      }

      function loadUsersFarmers() {
        // $(".loading-bar").show();
        $.get('{{url("load/users/yeelda/farmers")}}', function(data) {
          // $(".loading-bar").hide();
          $(".list-farmers").html('');
          var sn = 0;
          $.each(data, function(index, val) {
              sn++;
              if(val.lock == 'locked'){
                  var status = `<a href="javascript:void(0);" onclick="unblockUser('`+val.email+`')">unblock</a>`;
              }else if(val.lock == null){
                  
                  var status = `<a href="javascript:void(0);" onclick="blockUser('`+val.email+`')">block</a>`;
              }

              $(".list-farmers").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+val.name+`</td>
                      <td>`+val.email+`</td>
                      <td>`+val.date+`</td>
                      <td>
                          <a href="/admin/users/profile/`+val.id+`">view </a> - 
                          `+status+`
                      </td>  
                  </tr>
              `);
          });
        });   
      }

      function loadUserBuyers() {
        // $(".loading-bar").show();
        $.get('{{url("load/users/yeelda/buyers")}}', function(data) {
          // $(".loading-bar").hide();
          var sn = 0;
          $.each(data, function(index, val) {
              /* iterate through array or object */
              sn++;

              if(val.lock == 'locked'){
                  var status = `<a href="javascript:void(0);" onclick="unblockUser('`+val.email+`')">unblock</a>`;
              }else if(val.lock == null){
                  
                  var status = `<a href="javascript:void(0);" onclick="blockUser('`+val.email+`')">block</a>`;
              }

              $(".list-investor").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+val.name+`</td>
                      <td>`+val.email+`</td>
                      <td>`+val.date+`</td>
                      <td>
                          <a href="/admin/users/profile/`+val.id+`">view </a> -
                          `+status+`
                      </td>  
                  </tr>
              `); 

              // breakout show only 5
              if(sn == 4){
                  return false;
              }
          });
        });   
      }

      function loadUsersService() {
        // $(".loading-bar").show();
        $.get('{{url("load/users/yeelda/service")}}', function(data) {
          // $(".loading-bar").hide();
          var sn = 0;
          $.each(data, function(index, val) {
              sn++;
              if(val.lock == 'locked'){
                  var status = `<a href="javascript:void(0);" onclick="unblockUser('`+val.email+`')">unblock</a>`;
              }else if(val.lock == null){
                  
                  var status = `<a href="javascript:void(0);" onclick="blockUser('`+val.email+`')">block</a>`;
              }

              $(".list-service").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+val.name+`</td>
                      <td>`+val.email+`</td>
                      <td>`+val.date+`</td>
                      <td>
                          <a href="/admin/users/profile/`+val.id+`">view </a> - 
                          `+status+`
                      </td>  
                  </tr>
              `);
          });
        });   
      }

      function loadAssocRequest() {
          // load approve and pending chat groups
          $.get('/load/group/association/request', function(data) {
              /*optional stuff to do after success */
              // console.log(data);
              if(data.status == 'info'){
                  $('.info_msg').html(`
                      <span>`+data.message+`</span>
                  `);
              }else{
                  $('.load-association-request').html('');
                  $.each(data, function (index, val){
                      if(val.status == 'pending'){
                          var stats = `
                              <a class="link text-primary" onclick="acceptRequest(`+val.id+`)" href="javascript:void(0)"> Accept 
                              </a> 
                              <i class="fa  fa-ellipsis-h"></i>
                              <a class="link text-danger" onclick="declineRequest(`+val.id+`)" href="javascript:void(0)"> Decline
                              </a>
                          `;
                      }else{
                          var stats = '<a class="link text-danger small href="javascript:void(0)"> '+val.status+' </a>'
                      }
                      $('.load-association-request').append(`
                          <tr>
                              <td>`+val.name+`</td>
                              <td>`+stats+`</td>
                              <td>
                                  <a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="deleteGroup(${val.id})">Delete</a>
                              </td>
                          </tr>
                      `);
                  });
              }    
          });
      }

      function acceptRequest(x){
        $.get('/accept/group/'+x, function(data) {
          if(data.status == 'success'){
            swal(
              "success",
              "Approved",
              "success"
            );
            loadAssocRequest();
          }
        });
      }

      function declineRequest(x){
        $.get('/decline/group/'+x, function(data) {
          if(data.status == 'success'){
            swal(
              "error",
              "Declined",
              "error"
            );
            loadAssocRequest();
          }
        });
      }

      function loadAllNotifications(){
        
      }

      function blockUser(email) {
          var token = '{{ csrf_token() }}';
          var data = {
              _token:token,
              email:email
          }

          $.post('/admin/deactivate/user', data, function(data, textStatus, xhr) {
              if(data.status == 'success'){
                  swal(
                    "Ok",
                    data.message,
                    data.status
                  );;
                  refreshDivs();
              }
          });
      }

      function unblockUser(email) {
          var token = '{{ csrf_token() }}';
          var data = {
            _token:token,
            email:email
          }

          $.post('/admin/activate/user', data, function(data, textStatus, xhr) {
              if(data.status == 'success'){
                  swal(
                    "Ok",
                    data.message,
                    data.status
                  );
                  refreshDivs();   
              }
          });
      }

      function deleteGroup(x) {
        var token = $("#token").val();
        var params = {
          _token: token,
          group_id: x
        };
        $.post('{{ url("delete/assoc/group") }}', params, function(data, textStatus, xhr) {
            if(data.status == "success"){
                swal(
                    "Ok",
                    data.message,
                    data.status
                );
                loadAssocRequest();
            }              
        });
      }    

      function loadNotificationsUpdate(){
        $.get('{{url("admin/notification/get/all")}}', function(data) {

          $(".count_notification").html(data.length);
          // update notification menu
          updateNotificationMenu(data);

          $(".notifications_list").html(``);
          $.each(data, function(index, val) {
            if(val.category == "1"){
              val.category = `has signed up as a ${val.contents.accountType}...`;
              val.title = "New User";
              val.details = `<b>${val.contents.names}</b> ${val.category}`;
            }else if(val.category == "2"){
              val.category = `has subscribed...`;
              val.title = "Subscription";
            }else if(val.category == "3"){
              val.category = `has placed new order request!`;
              val.title = "Order";
            }else if(val.category == "4"){
              val.category = `has made payment to ${val.contents.name}`;
              val.title = "Payment";
              val.details = `<b>${val.contents.buyer}</b> ${val.category}`;
            }else if(val.category == "5"){
              val.category = `has join the chat-room for the first time`;
              val.title = "Chat Room";
            }else if(val.category == "6"){
              val.category = `made an update`;
              val.title = "Update";
            }else if(val.category == "0"){
              val.category = `No activity`;
              val.title = "No Activity";
            }

            $(".notifications_list").append(`
              <div class="notification__icon-wrapper">
                <div class="notification__icon">
                  <i class="material-icons">person</i>
                </div>
              </div>
              <div class="notification__content">
                ${val.details}
                <br />
                <span class="small">${val.date}</span>
              </div>
              <hr />
            `);
          });
        });
      }

      function updateNotificationMenu(data) {
        $(".load_notifications_bar").html("");
        $.each(data, function(index, val) {
          if(val.category == "1"){
              val.category = `has signed up as a ${val.contents.accountType}...`;
              val.title = "New User";
              val.details = `<b>${val.contents.names}</b> ${val.category}`;
          }else if(val.category == "2"){
            val.category = `has subscribed...`;
            val.title = "Subscription";
          }else if(val.category == "3"){
            val.category = `has placed new order request!`;
            val.title = "Order";
          }else if(val.category == "4"){
            val.category = `has made payment to ${val.contents.name}`;
            val.title = "Payment";
            val.details = `<b>${val.contents.buyer}</b> ${val.category}`;
          }else if(val.category == "5"){
            val.category = `has join the chat-room for the first time`;
            val.title = "Chat Room";
          }else if(val.category == "6"){
            val.category = `made an update`;
            val.title = "Update";
          }else if(val.category == "0"){
            val.category = `No activity`;
            val.title = "No Activity";
          }

          $(".load_notifications_bar").append(`
            <a class="dropdown-item" href="{{ url("admin/view/notifications") }}">
              <div class="notification__icon-wrapper">
                <div class="notification__icon">
                  <i class="material-icons">person_add</i>
                </div>
              </div>
              <div class="notification__content">
                <span class="notification__category">${val.title}</span>
                <p>
                  ${val.details}
                  <br />
                  <span class="small">${val.date}</span>
                </p>
              </div>
            </a>
          `);
        });
      }

    </script>
    @yield('scripts')
  </body>
</html>