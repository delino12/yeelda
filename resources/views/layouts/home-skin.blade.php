<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/chats-assets/img/apple-icon.png">
    <!-- Favicon -->
    <link rel="icon" href="/images/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="/chats-assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/chats-assets/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/chats-assets/css/demo.css" rel="stylesheet" />
    {{-- custom css --}}
    <link rel="stylesheet" type="text/css" href="/chats-assets/css/chats.css">
    {{-- sweetaleart --}}
    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">
    <!-- Jquery Ajax -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body class="login-page sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar fixed-top" style="background-color: #000; color:#CCB19E;" color-on-scroll="400">
        <div class="container">
            <div class="dropdown button-dropdown">
                <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-header">Dropdown header</a>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">One more separated link</a>
                </div>
            </div>
            <div class="navbar-translate">
                <a class="navbar-brand" href="/chat/community" rel="tooltip" title="Designed by Cavidel." data-placement="bottom" target="_blank">
                   Yeelda Community
                </a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="/chats-assets/img/blurred-image-1.jpg">
                
                      <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item"><a style="color:#CCB19E;" href="/farm-produce"><i class="fa fa-tree"></i> Farm Produce</a></li>
                        <li class="nav-item"><a style="color:#CCB19E;" href="/farm-equipment"><i class="fa fa-cogs"></i> Farm Equipment</a></li>
                        <li class="nav-item"><a style="color:#CCB19E;" href="/farm-equipment"><i class="fa fa-cogs"></i> Farm Services</a></li>
                        
                        <li class="nav-item"><a style="color:#CCB19E;" href="/carts"><span class="carts_total"></span><i class="fa fa-shopping-cart"></i> Cart</a></li>
                        <li class="nav-item"><a style="color:#CCB19E;" href="/chat/community"><i class="fa fa-comments-o"></i> Community</a></li>
                        @if(Auth::guard('farmer')->check())
                        <input type="hidden" id="user_email" value="{{ Auth::guard('farmer')->user()->email }}">
                        <li class="nav-item">
                            <a style="color:#CCB19E;" href="/farmer/dashboard">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </a>
                        </li>
                        <li class="nav-item"><a style="color: #CCB19E;" href="/account/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                        @elseif(Auth::guard('investor')->check())
                        <input type="hidden" id="user_email" value="{{ Auth::guard('investor')->user()->email }}">
                        <li class="nav-item">
                            <a style="color:#CCB19E;" href="/investors/dashboard">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </a>
                        </li>
                        <li class="nav-item"><a style="color: #CCB19E;" href="/account/logout/investor"><i class="fa fa-sign-out"></i> Logout</a></li> 
                        @elseif(Auth::guard('sp')->check())
                        <input type="hidden" id="user_email" value="{{ Auth::guard('sp')->user()->email }}">
                        <li class="nav-item">
                            <a style="color:#CCB19E;" href="/service-provider/dashboard">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </a>
                        </li> 
                        <li class="nav-item"><a style="color: #CCB19E;" href="/account/logout/sp"><i class="fa fa-sign-out"></i> Logout</a></li> 
                        @else
                        <li class="nav-item"><a style="color:#CCB19E;" href="/account/login"><i class="fa fa-sign-in"></i> Login </a></li>
                        <li class="nav-item"><a style="color:#CCB19E;" href="/account/register"><i class="fa fa-edit"></i> Register </a> </li>
                        @endif
                        <li class="nav-item"><a style="color:#CCB19E;" href="/contact"><i class="fa fa-envelope"></i> Contact</a></li>
                        <li><a href="javascript:void(0);"></a></li>
                      </ul>
            </div>
        </div>
    </nav>
    @yield('contents')
</body>

@yield('scripts')
<!--   Core JS Files   -->
{{-- <script src="/chats-assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script> --}}
<script src="/chats-assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="/chats-assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="/chats-assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/chats-assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="/chats-assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="/chats-assets/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>

</html>