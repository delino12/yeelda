<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>@yield('title')</title>
  <link rel="icon" href="/images/favicon.png">
  <!-- Bootstrap core CSS-->
  <link href="/extended-assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/extended-assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="/extended-assets/css/sb-admin.css" rel="stylesheet">
  {{-- sweetaleart --}}
  <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

  <!-- Custom Ajax -->
  {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script> --}}
  <script src="/js/jquery/jquery.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <!-- Start of Async Drift Code -->
  <script>
      !function() {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
        t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
          return function() {
            var n;
            return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
          };
        }, t.methods.forEach(function(e) {
          t[e] = t.factory(e);
        }), t.load = function(t) {
          var e, n, o, i;
          e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
          o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
          n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
        });
      }();
      drift.SNIPPET_VERSION = '0.3.1';
      drift.load('gdgaepn42buc');
  </script>
  {{-- introJs --}}
  <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
  <script type="text/javascript" src="/intro-assets/intro.js"></script>

  {{-- summernote css --}}
  <link rel="stylesheet" type="text/css" href="/css/summernote.css">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  @if(Auth::check())
    <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
  @endif

    <style type="text/css">
      .dino_errors {
        position: absolute;
        z-index: 10;
        top: 80px;
        right: 20px;
        box-shadow: 1px 1px 2px 1px #CCC;
      }
    </style>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #2CA8CC; height: 56px;">
      {{-- <a class="navbar-brand" href="/"><img src="/images/img-set/logo.png" height="48" width="auto" style="margin-top:-10px;"></a> --}}
      <a class="navbar-brand" href="/">
        <img src="/images/img-set/logo.png" height="48" width="auto" style="position: absolute; margin-top:-25px;"> 
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav small" id="exampleAccordion" style="background:url(/images/side-bar.jpg);">
          @if(Auth::user()->account_type == "farmer")
            <li class="nav-item">
              <a class="nav-link" style="color:#CCB19E;" href="/farmer/dashboard">
                  <i class="fa fa-home"></i> <span class="nav-link-text"> Back to Home </span>
              </a>
            </li> 
          @elseif(Auth::user()->account_type == "buyer")
            <li class="nav-item">
              <a class="nav-link" style="color:#CCB19E;" href="/investors/dashboard">
                  <i class="fa fa-home"></i> <span class="nav-link-text"> Back to Home </span>
              </a>
            </li> 
          @elseif(Auth::user()->account_type == "service")
            <li class="nav-item">
              <a class="nav-link" style="color:#CCB19E;" href="/service-provider/dashboard">
                  <i class="fa fa-home"></i> <span class="nav-link-text"> Back to Home </span>
              </a>
            </li>  
          @endif

          @if(Auth::check())
            <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
          @endif

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Create New Messages">
            <a class="nav-link" href="/message/create">
              <i class="fa fa-fw fa-edit"></i>
              <span class="nav-link-text" data-step="5" data-intro="click here to compose new messages">Compose New</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inbox">
            <a class="nav-link" href="/message/service" onclick="showDiv(b)">
              <i class="fa fa-fw fa-comments-o"></i>
              <span class="nav-link-text" data-step="6" data-intro="click here to view inbox messages">Inbox (<span class="total-msg"></span>) </span>
            </a>
          </li>

          {{-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sent">
            <a class="nav-link" href="/message/sent">
              <i class="fa fa-fw fa-location-arrow"></i>
              <span class="nav-link-text" data-step="7" data-intro="click here to view all sent messages">Sent</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Draft">
            <a class="nav-link" href="/message/draft">
              <i class="fa fa-fw fa-table"></i>
              <span class="nav-link-text" data-step="8" data-intro="To view draft messages">Draft</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Trash">
            <a class="nav-link" href="#" >
              <i class="fa fa-fw fa-trash"></i>
              <span class="nav-link-text" data-step="9" data-intro="Click here to view all trash or junk messages">Trash</span>
            </a>
          </li> --}}
          {{-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Settings">
            <a class="nav-link" href="#" onclick="showDiv(f)">
              <i class="fa fa-fw fa-cogs"></i>
              <span class="nav-link-text" data-step="8" data-intro="This section allows you to configured messages">Setting</span>
            </a>
          </li> --}}
        </ul>
        <ul class="navbar-nav sidenav-toggler" style="background:url(/images/side-bar.jpg);">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>

        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link mr-lg-2" href="javascript:void(0);">
            <i class="fa fa-user"></i> {{ Auth::user()->name }}
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link mr-lg-2 wallet-balance"></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="{{url('farmer/message')}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">New Messages:</h6>
            <div class="load-mini-msg small"></div>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="{{url('message/service')}}">View all messages</a>
          </div>
        </li>
        
        <li class="nav-item">
          <a href="/carts" class="nav-link">
            <span class="carts_total"></span> <i class="fa fa-fw fa-shopping-cart"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
      </div>
    </nav> 

    <div class="content-wrapper">
      @yield('contents')
      <!-- /.container-fluid-->
      <!-- /.content-wrapper-->
      <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright © Yeelda {{ date("Y") }} </small>
          </div>
        </div>
      </footer>
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>
    </div>
     <!-- Bootstrap core JavaScript-->
    <script src="/extended-assets/vendor/jquery/jquery.min.js"></script>
    <script src="/extended-assets/vendor/popper/popper.min.js"></script>
    <script src="/extended-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/extended-assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/extended-assets/js/sb-admin.min.js"></script>
    {{-- <script src="/js/jquery/jquery.slim.js"></script> --}}
    <script src="/js/typeahead/typeahead.jquery.js"></script>
    <script src="/js/typeahead/typeahead.bundle.js"></script>
    <script src="/js/typeahead/bloodhound.js"></script>

    {{-- required appjs --}}
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script> --}}
    <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
    <script type="text/javascript" src="/js/summernote.js"></script>
    <script type="text/javascript">
      loadWalletBalance();
      // Pusher.logToConsole = true;
      var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
          encrypted: false,
          cluster: "eu"
      });
      
      // listent to chat on channels 
      var channel = pusher.subscribe('new-chat-message');
      channel.bind('App\\Events\\NewChat', function(data) {
          var audio = new Audio('/audio/notify.wav');
          audio.pause();
          audio.play();
          
          // console.log(data);
          var logged_email = $("#user_email").val();
          var el = data;
          if(el.email !== logged_email){
            var name = el.name;
            var body = el.body;
            var img  = el.image;

            Push.create(name, {
              body: body,
              icon: img,
              timeout: 4000,
              onClick: function () {
                  window.focus();
                  this.close();
              }
            });
          }else{
            // pushNotifyer;
          }
      });

      var email = $("#user_email").val();
      function showDiv(x){
        if(x == 'new'){
          $('#create-new-div').toggle();
        }
      }

      // count messages
      $.get('/count/messages/'+email, function (e){
        // console.log(e);
        $('.total-msg').html(`
          `+e.total+`
        `);
      });

      // load wallet
      function loadWalletBalance() {
        $.get('{{url('get/wallet/balance')}}', function(data) {
          $(".wallet-balance").html(`Wallet Balance: &#8358;`+data.balance);
        });
      }

      $('#message-body').summernote();
    </script>
  </body>
</html>
