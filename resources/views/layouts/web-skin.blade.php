<!DOCTYPE HTML>
<html>
    <head>
        @include('web-components.header')
    </head>
    <body>
    	<style type="text/css">
			.truncate {
				width: 50px
				white-space: nowrap;
				overflow: hidden;
				text-overflow: ellipsis;
			}

			@media screen and (max-width: 768px){
				.y-btn-large {
				    width: 100%;
				}

				.y-login-title h1 {
					font-size: 24px;
				}

				.y-signin-wrapper {
					padding-top: 70px;
				}
			}
		</style>
        @yield('contents')
    	@include('web-components.scripts-assets')
        <script type="text/javascript">
		    $(document).ready(function() {
		        if(Push.Permission.has()){
		            console.log('Push notication is no !');
		            $('.enable-notify').html(`
		                <br />
		            `);
		        }else{
		            $('.enable-notify').html(`
		                <div class="push-notify-link"><i class="fa fa-bell"></i> YEELDA notification is turned off - enable push notification </div>
		            `);
		            console.log('Push notication is off or block ');
		        }

		        $(".push-notify-link").click(function (){
		            Push.create("YEELDA Notification!", {
		                body: "Notificans has been enable !",
		                icon: '/icon.png',
		                timeout: 4000,
		                onClick: function () {
		                    window.focus();
		                    this.close();
		                }
		            });
		        });

		        // Pusher.logToConsole = true;
		        var pusher = new Pusher('{{ env("PUSHER_APP_KEY") }}', {
		            encrypted: true,
		            cluster: 'eu'
		        });

		        var channel = pusher.subscribe('add-to-cart');
		        channel.bind('Yeelda\\Events\\AddToCart', function(data) {
		            $.get('/load/shopping/carts/?email='+email, function(data) {
		                if(data.total == 0){
		                    $(".carts_total").html(``);
		                }else{
		                    $(".carts_total").html(`
		                        `+data.total+`
		                    `);
		                }
		            });
		        });

		        function goFarm(){
		            window.location.href = '/farm-produce';
		        }

		        // commodity market
		        // $.get('/commodities-price/discovery', function (data){
		        //     console.log(data);
		        // });

		        $.get('/commodities-price/load-index', function (data){
		            $(".ticker__list").html("");
		            $.each(data, function(index, val) {
		                if(val.open > val.previous){
		                    var stat = '<i class="fa fa-arrow-up text-success"></i>';
		                }else{
		                    var stat = '<i class="fa fa-arrow-down text-danger"></i>';
		                }

		                $('.ticker__list').append(`
		                	<div class="ticker__item">
		                        <span class="assets small">`+val.asset+`</span> 
		                        Open `+stat+` 
		                        <span class="price-open small">&#8358; `+val.open+`</span>
		                        Close 
		                        <span class="price-close small">&#8358; `+val.close+`</span> (Kg)
			                </div>
		                `);
		            });

		            var ticker 	= document.querySelector(".ticker");
					var list 	= document.querySelector(".ticker__list");
					var clone 	= list.cloneNode(true);
					ticker.append(clone);
		        });
		    });

		    loadShoppingCart();
			
            function loadShoppingCart() {
            	var email = $("#logged_email").val();
                $.get('/load/shopping/carts/?email='+email, function(data) {
                    if(data.total == 0){
                        $(".carts_total").html(``);
                    }else{
                        $(".carts_total").html(data.total);
                    }
                });
            }

            $(".navbar-toggle").click(function(e){
            	
            });
		</script>
        @yield('scripts')
    </body>
</html>

