<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link rel="icon" href="/images/favicon.png">
    <!-- Bootstrap core CSS-->
    <link href="/extended-assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/extended-assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="/extended-assets/css/sb-admin.css" rel="stylesheet">
  </head>

  <body>
    
    @yield('contents')

      <!-- Bootstrap core JavaScript-->
      <script src="/extended-assets/vendor/jquery/jquery.min.js"></script>
      <script src="/extended-assets/vendor/popper/popper.min.js"></script>
      <script src="/extended-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="/extended-assets/vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="/extended-assets/js/sb-admin.min.js"></script>
      <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
  </body>
</html>
