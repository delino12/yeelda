@extends('layouts.chat-skin')

{{-- Title --}}
@section('title')
    Yeelda Farmers Community
@endsection

{{-- Contents  --}}
@section('contents')
    <div class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
        <div class="row">
          <div class="col-lg-9 col-md-12">
            <!-- Add New Post Form -->
            <div class="card card-small mb-3">
              <div class="card-body">
                <h6> 
                    <a href="{{url('/chat/community')}}"> 
                        <i class="fa fa-angle-double-left"></i> Back
                    </a>
                </h6>

                <div class="chat-wrapper" style="height: 500px;overflow:auto;">
                    <div class="chat-messages"></div>
                </div>
                    {{-- <div class="chat-input"> --}}
                    <form class="chat-message-form" method="post" onsubmit="return sendChat()">
                        {{ csrf_field() }}
                        {{-- auth logged email --}}
                        <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
                        
                        {{-- chat messages --}}
                        <div class="form-group">
                            <textarea class="form-control" rows="3" cols="3" id="message" placeholder="Type a message..." required=""></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary col-md-12">
                                Send <img src="/svg/three-dots.svg" width="20px" height="20px" style="display: none;" class="loading pull-right">
                            </button>
                        </div>
                    </form>
                    {{-- </div> --}}
              </div>
            </div>
            <!-- / Add New Post Form -->
          </div>

          <div class="col-lg-3 col-md-12">
            <!-- Post Overview -->
            <div class='card card-small mb-3'>
              <div class="card-header border-bottom">
                <h6 class="m-0">More</h6>
              </div>
              <div class='card-body p-4'>
                <h1 class="lead">
                   Invite more users
                </h1>
                <form id="invite-form" method="post" onsubmit="return sendChatInvite()">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" id="email_a" placeholder="email here.." required=""><br />
                        <input type="email" class="form-control" id="email_b" placeholder="email here.."><br />
                        <input type="email" class="form-control" id="email_c" placeholder="email here.."><br />
                        <input type="email" class="form-control" id="email_d" placeholder="email here..">
                    </div>
                    <button class="btn btn-primary col-xl-12">
                        Send <img src="/svg/three-dots.svg" width="20px" height="20px" style="display: none;" class="loading-invites pull-right">
                    </button>
                </form>
                <div class="success_msg"></div>
              </div>
            </div>
            <!-- / Post Overview -->
          </div>
        </div>
    </div>    
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- required appjs --}}
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
        var group_name = '{{ $group_name }}';
        var logged_email = $('#user_email').val();
        
        // send chat messages
        function sendChat(){
            $('.loading').show();
            // chat form data
            var token   = $("input[name=_token]").val();
            var email   = $("#user_email").val();
            var message = $("#message").val();

            // data to json
            var data = {
                _token:token,
                email:email,
                message:message,
                room:group_name
            }

            // post to ajax
            $.ajax({
                url: '/send/group-chat/messages',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (data){
                    // console.log(data);
                    // reset form
                    $(".chat-message-form")[0].reset();
                    $('.loading').hide();
                },
                error: function (){
                    alert('Error, Fail to send Chat...');
                }
            });

            return false;
        }

        // fire chat on keyup
        $('#message').on('keydown', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                // send chat
                sendChat();
            }
        });

        // load messages
        $.get('/load/chat-group/messages/?name='+group_name, function(data) {
            /*optional stuff to do after success */
            var logged_email = $("#user_email").val();
            // console.log(data);
            $('.chat-messages').html('');
            $.each(data, function(index, el) {
                if(el.email !== logged_email){
                    $('.chat-messages').append(`
                        <div class="chat-box-right">
                            <span class="chat-reader-title small">${el.name}</span><br />
                            <span class="small">${el.body}</span><br />
                            <span class="small float-right">`+el.date+`</span>
                        </div>
                    `);

                }else{
                    $('.chat-messages').append(`
                        <div class="chat-box-left">
                            <span class="chat-sender-title small">${el.name}</span><br />
                            <span class="small">${el.body}</span><br />
                            <span class="small float-right">`+el.date+`</span>
                        </div>
                    `);
                }

                $('.chat-wrapper').stop().animate({
                    scrollTop: $(".chat-wrapper")[0].scrollHeight
                }, 800);
            });
        });

        // update messages with pusher
        // Pusher.logToConsole = true;
        var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
            encrypted: false,
            cluster: "eu"
        });

        // listent to chat on channels 
        var channel = pusher.subscribe('new-group-message');
        channel.bind('Yeelda\\Events\\NewChatGroup', function(data) {
            // console.log(data);
            var audio = new Audio('/audio/open-ended.mp3');
                audio.pause();
                audio.play();
            
            var el = data;
            if(el.email !== logged_email){
                $('.chat-messages').append(`
                    <div class="chat-box-right">
                        <span class="chat-reader-title small">${el.name}</span><br />
                        <span class="small">${el.body}</span><br />
                        <span class="small float-right">`+el.date+`</span>
                    </div>
                `);
                var name = el.name;
                var body = el.body;
                var img  = el.image;

                Push.create(name, {
                    body: body,
                    icon: img,
                    timeout: 4000,
                    onClick: function () {
                        window.focus();
                        this.close();
                    }
                });
            }else{
                $('.chat-messages').append(`
                    <div class="chat-box-left">
                        <span class="chat-sender-title small">${el.name}</span><br />
                        <span class="small">${el.body}</span><br />
                        <span class="small float-right">`+el.date+`</span>
                    </div>
                `);
            }

            // auto scroll chat
            $('.chat-wrapper').stop().animate({
                scrollTop: $(".chat-wrapper")[0].scrollHeight
            }, 800);
        });

        // load chat users
        $.get('/load/group-chat/users/?name='+group_name, function(data) {
            /*optional stuff to do after success */
            // console.log(data);
            $('.load-group-chat-users').html("");
            var sn = 0;
            // load farmers
            $.each(data, function(index, val) {
                var avatar;
                sn++;
                if(val.avatar !== null){
                    if (val.avatar.indexOf('data:') > -1){
                        avatar = `<img height="auto" width="30" src="`+val.avatar+`" alt="placeholder+image">`;
                    }else if (val.avatar.indexOf('http://') > -1){
                        avatar = `<img height="auto" width="30" src="`+val.avatar+`" alt="placeholder+image">`;
                    }else if(val.avatar.indexOf('profile') > -1){
                        avatar = '<img src="http://www.iconninja.com/files/373/611/612/person-user-profile-male-man-avatar-account-icon.svg" alt="person" class="img-square" width="40px" height="auto">';
                    }else{
                        avatar = `<img height="auto" width="30" src="/uploads/farmers/`+val.avatar+`" alt="placeholder+image">`;
                    }
                }else{
                    avatar = '<i class="material-icons" style="font-size:20px;">person</i>';
                }

                if(val.account_type == "service"){
                    val.account_type = "services provider";
                }

                var activeRow;
                if(val.email == logged_email){
                    activeRow = `text-info small`;
                }else{
                    activeRow = `text-info small`;
                }

                $('.load-group-chat-users').append(`
                    <li class="list-group-item" style="border-radius: 0rem;">
                        <div class="row text-left">
                            <div class="col-sm-3">`+avatar+`</div>
                            <div class="col-sm-9 ${activeRow}">
                                `+val.name+` <br />
                                (${val.account_type})
                            </div>
                        </div>
                    </li>
                `);
            });
        });

        // send chat invites
        function sendChatInvite(){
            $('.loading-invites').show();

            // html form data
            var emailA = $("#email_a").val();
            var emailB = $("#email_b").val();
            var emailC = $("#email_c").val();
            var emailD = $("#email_d").val();
            var token  = '{{ csrf_token() }}';
            var groupN = '{{ $group_name }}'; 

            // prepare json
            var data = {
                emailA:emailA,
                emailB:emailB,
                emailC:emailC,
                emailD:emailD,
                name:groupN,
                _token:token
            };

            // send ajax request
            $.ajax({
                url: '/send/groups/invites/',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data){
                    // console.log(data);
                    if(data.status == 'success'){
                        swal(
                            "success",
                            data.message,
                            "success"
                        );
                    }else{
                        swal(
                            "oops",
                            data.message,
                            "error"
                        );
                    }
                    $('#invite-form')[0].reset();
                    $('.loading-invites').hide();
                },
                error: function (data){
                    console.error(data);
                }
            });

            return false;
        }
    </script>
@endsection