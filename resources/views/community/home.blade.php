@extends('layouts.chat-skin')

{{-- Title --}}
@section('title')
    Yeelda Farmers Community
@endsection

{{-- Contents  --}}
@section('contents')
    <div class="page-header" filter-color="orange">
        <div class="page-header-image" style="background-image:url(/images/bg_images/3.jpg)"></div>
        <div class="container">
            <div class="col-md-6">
                <br /><br /><br />
                <h1 class="lead">
                    Chat Messages in (General) <hr />
                    {{-- @if(Auth::check())
                        <span class="small">
                            <i class="fa fa-user"></i> 
                            {{ Auth::user()->name }}
                        </span>
                    @endif
 --}}
                    @if(Auth::check())
                        @if(Auth::user()->account_type == "farmer")
                            <a href="/farmer/dashboard"> 
                                <span class="small"><i class="fa fa-user"></i>
                                    {{Auth::user()->name}} (Farmer) 
                                </span>
                            </a>
                        @elseif(Auth::user()->account_type == "service")
                            <a href="/investors/dashboard"> 
                                <span class="small"><i class="fa fa-user"></i>
                                    {{Auth::user()->name}} (Buyer) 
                                </span>
                            </a>
                        @elseif(Auth::user()->account_type == "buyer")
                            <a href="/service-provider/dashboard">
                                <span class="small"><i class="fa fa-user"></i>
                                    {{Auth::user()->name}} (Service)
                                </span>
                            </a>
                        @endif
                     @endif
                     
                    <span class="chat-user"></span> 
                 </h1>
                <div class="chat-wrapper">
                    <div class="chat-messages"></div>
                </div>
                {{-- <div class="chat-input"> --}}
                <form class="chat-message-form" method="post" onsubmit="return sendChat()">
                    {{ csrf_field() }}
                    {{-- auth logged email --}}
                    @if(Auth::check())
                        <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
                    @endif
                    {{-- chat messages --}}
                    <div class="form-group">
                        <textarea class="form-control" rows="1" cols="3" id="message" placeholder="type a message" required=""></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary col-md-12">Send</button>
                    </div>
                </form>
                {{-- </div> --}}
            </div>
        </div>

        {{-- Footer Section --}}
        <footer class="footer">
            <div class="container">
                
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Designed by
                    <a href="http://www.cavidel.com" target="_blank">Cavidel</a>
                </div>
            </div>
        </footer>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- pusher lib --}}
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
        // send chats with return key
        $('#message').addEventListener('keydown', function (e) {
            var key = e.which || e.keyCode;
            if (key === 13) { // 13 is enter
              // code for enter
              sendChat();
            }
        });

        // send chat messages
        function sendChat()
        {
            // chat form data
            var token   = $("input[name=_token]").val();
            var email   = $("#user_email").val();
            var message = $("#message").val();

            // data to json
            var data = {
                _token:token,
                email:email,
                message:message
            }

            // post to ajax
            $.ajax({
                url: '/send/chat/messages',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (data){
                    console.log(data);
                    // reset form
                    $(".chat-message-form")[0].reset();
                },
                error: function (){
                    alert('Error, Fail to send Chat...');
                }
            });

            return false;
        }

        // load messages
        $.get('/load/chat/messages', function(data) {
            /*optional stuff to do after success */
            var logged_email = $("#user_email").val();
            // console.log(data);
            $('.chat-messages').html('');
            $.each(data, function(index, el) {
                // console.log(el.body);
                if(el.email !== logged_email){
                    $('.chat-messages').append(`
                        <br />
                        <div class="chat-box-right">
                            <span class="">`+el.name+`</span><br />
                            <span class="text-warning">`+el.body+`</span><br />
                            <span class="small pull-right">`+el.date+`</span>
                            <br />
                        </div>
                    `);

                }else{
                    $('.chat-messages').append(`
                        <br />
                        <div class="chat-box-left">
                            <span class="">Me: </span><br />
                            <span class="text-success">`+el.body+`</span><br />
                            <span class="small pull-right">`+el.date+`</span>
                            <br />
                        </div>
                    `);
                }

                $('.chat-wrapper').stop().animate({
                    scrollTop: $(".chat-wrapper")[0].scrollHeight
                }, 800);
            });
        });

        // Pusher.logToConsole = true;
        var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
            encrypted: false,
            cluster: "eu"
        });

        var channel = pusher.subscribe('new-chat-message');
        channel.bind('App\\Events\\NewChat', function(data) {
            // console.log(data);
            var logged_email = $("#user_email").val();
            var el = data;
            if(el.email !== logged_email){
                $('.chat-messages').append(`
                    <br />
                    <div class="chat-box-right">
                        <span class="">`+el.name+`</span><br />
                        <span class="text-warning">`+el.body+`</span><br />
                        <span class="small pull-right">`+el.date+`</span>
                        <br />
                    </div>
                `);
            }else{
                $('.chat-messages').append(`
                    <br />
                    <div class="chat-box-left">
                        <span class="">Me: </span><br />
                        <span class="text-success">`+el.body+`</span><br />
                        <span class="small pull-right">`+el.date+`</span>
                        <br />
                    </div>
                `);
            }
            // auto scroll chat
            $('.chat-wrapper').stop().animate({
                scrollTop: $(".chat-wrapper")[0].scrollHeight
            }, 800);
        });
    </script>
@endsection