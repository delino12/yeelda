@extends('layouts.chat-skin')

{{-- Title --}}
@section('title')
    Yeelda Farmers Community
@endsection

{{-- Contents  --}}
@section('contents')
    <div class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
        <div class="row">
          <div class="col-lg-9 col-md-12">
            <!-- Add New Post Form -->
            <div class="card card-small mb-3">
                <div class="card-body" style="padding:1rem;>
                    <h6> 
                        <a href="{{url('/chat/community')}}"> 
                            <i class="fa fa-angle-double-left"></i> Back
                        </a>
                    </h6>

                    <div class="chat-wrapper" style="height: 500px;overflow:auto;">
                        <div class="chat-messages"></div>
                    </div>
                    <form class="chat-message-form" method="post" onsubmit="return sendChat()">
                        {{ csrf_field() }}
                        <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">                        
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <textarea class="form-control y-chat-textarea" style="resize: none;border-top: 0rem;border-left:0rem;border-right: 0rem;" rows="1" cols="23" id="message" placeholder="Type a message..." required=""></textarea>
                            </div>

                            <div class="col-sm-2">
                                <button style="background-color: transparent; border: 0rem;cursor: pointer;">
                                    <i class="fa fa-paper-plane text-primary" style="font-size: 30px;"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- / Add New Post Form -->
          </div>

          <div class="col-lg-3 col-md-12">
            <div class='card card-small mb-3'>
              <div class="card-header border-bottom">
                <h6 class="m-0">Room Categories</h6>
              </div>
              <div class='card-body p-0'>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item px-3 pb-2">
                    <div class="load-associations"></div>
                  </li>
                  <li class="list-group-item d-flex px-3">
                    <div class="create-form" style="display: none;">
                        <form method="post" onsubmit="return CreateAssoc()">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" id="name" placeholder="Type a name...." required="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <select class="form-control" id="state">
                                    <option value="" selected="selected">- Select -</option>
                                    <option value="Abuja FCT">Abuja FCT</option>
                                    <option value="Abia">Abia</option>
                                    <option value="Adamawa">Adamawa</option>
                                    <option value="Akwa Ibom">Akwa Ibom</option>
                                    <option value="Anambra">Anambra</option>
                                    <option value="Bauchi">Bauchi</option>
                                    <option value="Bayelsa">Bayelsa</option>
                                    <option value="Benue">Benue</option>
                                    <option value="Borno">Borno</option>
                                    <option value="Cross River">Cross River</option>
                                    <option value="Delta">Delta</option>
                                    <option value="Ebonyi">Ebonyi</option>
                                    <option value="Edo">Edo</option>
                                    <option value="Ekiti">Ekiti</option>
                                    <option value="Enugu">Enugu</option>
                                    <option value="Gombe">Gombe</option>
                                    <option value="Imo">Imo</option>
                                    <option value="Jigawa">Jigawa</option>
                                    <option value="Kaduna">Kaduna</option>
                                    <option value="Kano">Kano</option>
                                    <option value="Katsina">Katsina</option>
                                    <option value="Kebbi">Kebbi</option>
                                    <option value="Kogi">Kogi</option>
                                    <option value="Kwara">Kwara</option>
                                    <option value="Lagos">Lagos</option>
                                    <option value="Nassarawa">Nassarawa</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Ogun">Ogun</option>
                                    <option value="Ondo">Ondo</option>
                                    <option value="Osun">Osun</option>
                                    <option value="Oyo">Oyo</option>
                                    <option value="Plateau">Plateau</option>
                                    <option value="Rivers">Rivers</option>
                                    <option value="Sokoto">Sokoto</option>
                                    <option value="Taraba">Taraba</option>
                                    <option value="Yobe">Yobe</option>
                                    <option value="Zamfara">Zamfara</option>
                                    <option value="Outside Nigeria">Outside Nigeria</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary"><i class="fa fa-users"></i> Create</button>
                            </div>
                        </form>
                        <div class="success_msg"></div>
                        <div class="error_msg"></div>
                    </div>
                    {{-- <div class="input-group">
                      <input type="text" class="form-control" placeholder="New room" aria-label="Add new category" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                        <button class="btn btn-white px-2" type="button">
                          <i class="material-icons">add</i>
                        </button>
                      </div>
                    </div> --}}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
    </div>    
@endsection

{{-- Scripts --}}
@section('scripts')
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
        loadChatUsers();
        loadChatMessages();
        loadAssociations();
        clearOldChatMessages();

        var logged_email = $('#user_email').val();
        
        // send chat messages
        function sendChat(){
            $('.loading').show();
            // chat form data
            var token   = $("input[name=_token]").val();
            var email   = $("#user_email").val();
            var message = $("#message").val();

            // data to json
            var data = {
                _token:token,
                email:email,
                message:message
            }

            // post to ajax
            $.ajax({
                url: '/send/chat/messages',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (data){
                    console.log(data);
                    // reset form
                    $(".chat-message-form")[0].reset();
                    $('.loading').hide();
                },
                error: function (){
                    alert('Error, Fail to send Chat...');
                }
            });

            return false;
        }

        // load chat messages
        function loadChatMessages(){
            // load messages
            $.get('/load/chat/messages', function(data) {
                /*optional stuff to do after success */
                var logged_email = $("#user_email").val();
                // console.log(data);
                $('.chat-messages').html("");
                $.each(data, function(index, el) {
                    // console.log(el.body);
                    if(el.email !== logged_email){
                        $('.chat-messages').append(`
                            <div class="chat-box-right">
                                <span class="chat-reader-title small">${el.name}</span><br />
                                <span class="small">${el.body}</span><br />
                                <span class="small float-right">`+el.date+`</span>
                            </div>
                        `);

                    }else{
                        $('.chat-messages').append(`
                            <div class="chat-box-left">
                                <span class="chat-sender-title small">${el.name}</span><br />
                                <span class="small">${el.body}</span><br />
                                <span class="small float-right">`+el.date+`</span>
                            </div>
                        `);
                    }

                    $('.chat-wrapper').stop().animate({
                        scrollTop: $(".chat-wrapper")[0].scrollHeight
                    }, 800);
                });
            });
        }

        // load associations
        function loadAssociations(){
            // load chat users
            var logged_email = '{{ Auth::user()->email }}';
            $.get('/load/users/associations/?email='+logged_email, function(data) {
                /*optional stuff to do after success */
                // console.log(data);
                if(data.status == 'info'){
                    $('.load-associations').html(`
                        <span>`+data.message+`</span>
                        <hr />
                        <a href="javascript:void(0);" onclick="showAssocForm()">Create Association</a> <br />
                    `);
                }else{
                    $.each(data, function(index, val) {
                         /* iterate through array or object */
                        if(val.status == 'approved'){
                            $('.load-associations').append(`
                                <a href="/chat/community/home/`+val.name+`">
                                    <span class="text-info">`+val.name+`</span>
                                </a>
                                <hr />
                                <a href="javascript:void(0);" onclick="showAssocForm()">Create Association</a>
                            `);
                        }else{
                            $('.load-associations').append(`
                                <span class="text-info">`+val.name+`</span> <span class="small">`+val.status+`</span>
                                <span class="text-info small">waiting approval</span>
                            `);
                        }
                    });
                }
            });
        }

        // load chat users
        function loadChatUsers(){
            // load chat users
            $.get('/load/chat/users', function(data) {
                $('.load-chat-users').html("");
                var sn = 0;

                // load farmers
                $.each(data, function(index, val) {
                    var avatar;
                    sn++;
                    if(val.avatar !== null){
                        if (val.avatar.indexOf('data:') > -1){
                            avatar = `<img height="auto" width="30" src="`+val.avatar+`" alt="placeholder+image">`;
                        }else if (val.avatar.indexOf('http://') > -1){
                            avatar = `<img height="auto" width="30" src="`+val.avatar+`" alt="placeholder+image">`;
                        }else if(val.avatar.indexOf('profile') > -1){
                            avatar = '<img src="http://www.iconninja.com/files/373/611/612/person-user-profile-male-man-avatar-account-icon.svg" alt="person" class="img-square" width="40px" height="auto">';
                        }else{
                            avatar = `<img height="auto" width="30" src="/uploads/farmers/`+val.avatar+`" alt="placeholder+image">`;
                        }
                    }else{
                        avatar = '<i class="material-icons" style="font-size:20px;">person</i>';
                    }

                    if(val.account_type == "service"){
                        val.account_type = "services provider";
                    }

                    var activeRow;
                    if(val.email == logged_email){
                        activeRow = `text-info small`;
                    }else{
                        activeRow = `text-info small`;
                    }

                    $('.load-chat-users').append(`
                        <li class="list-group-item" style="border-radius: 0rem;">
                            <div class="row text-left">
                                <div class="col-sm-3">`+avatar+`</div>
                                <div class="col-sm-9 ${activeRow}">
                                    `+val.name+` <br />
                                    (${val.account_type})
                                </div>
                            </div>
                        </li>
                    `);
                });
            });
        }

        // show create form
        function showAssocForm(){
            $('.create-form').toggle();
        }

        // create new chat room
        function CreateAssoc(){
            var token = '{{ csrf_token() }}';
            var host  = '{{ Auth::user()->email }}';
            var name  = $("#name").val();
            var state = $("#state").val();

            // to json
            var data = {
                _token:token,
                name:name,
                state:state,
                host:host
            };

            // send ajax
            $.ajax({
                url: '/send/create/request',
                type: 'post',
                dataType: 'json',
                data:data,
                success: function(data){
                    console.log(data);
                    if(data.status == 'success'){
                        $('.success_msg').html(`
                            `+data.message+`
                        `);
                    }
                },
                error: function(data){
                    console.log(data);
                    alert('Fail to send request..');
                }
            });
            
            return false;
        }

        // fire chat on keyup
        $('#message').on('keydown', function(e) {
            if (e.which == 13) {
                e.preventDefault();

                var message = $("#message").val();
                if(message == ""){
                    $("#message").css('border', '1px solid #F00');
                    return false;
                }else{
                    // send chat
                    sendChat();   
                }
                
            }
        });

        // update messages with pusher
        // Pusher.logToConsole = true;
        var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
            encrypted: false,
            cluster: "eu"
        });

        // listent to chat on channels 
        var channel = pusher.subscribe('new-chat-message');
        channel.bind('Yeelda\\Events\\NewChat', function(data) {
            var audio = new Audio('/audio/notify.wav');
                audio.pause();
                audio.play();
            
            // console.log(data);
            
            var el = data;
            if(el.email !== logged_email){
                $('.chat-messages').append(`
                    <div class="chat-box-right">
                        <span class="chat-reader-title small">${el.name}</span><br />
                        <span class="small">${el.body}</span><br />
                        <span class="small float-right">`+el.date+`</span>
                    </div>
                `);
                var name = el.name;
                var body = el.body;
                var img  = el.image;

                Push.create(name, {
                    body: body,
                    icon: img,
                    timeout: 4000,
                    onClick: function () {
                        window.focus();
                        this.close();
                    }
                });
            }else{
                $('.chat-messages').append(`
                    <div class="chat-box-left">
                        <span class="chat-sender-title small">${el.name}</span><br />
                        <span class="small">${el.body}</span><br />
                        <span class="small float-right">`+el.date+`</span>
                    </div>
                `);
            }

            // auto scroll chat
            $('.chat-wrapper').stop().animate({
                scrollTop: $(".chat-wrapper")[0].scrollHeight
            }, 800);
        });

        // clear old chat
        function clearOldChatMessages() {
            var token = $("#token").val();
            var params = {_token: token};
            $.post('{{ url("clear/old/chat/messages") }}', params, function(data, textStatus, xhr) {
                // console.log(data);
            });
        }
    </script>
@endsection   