@extends('layouts.chat-skin')

{{-- Title --}}
@section('title')
    Yeelda Farmers Community
@endsection

{{-- Contents  --}}
@section('contents')
    <div class="row mb-2 mt-5">
        <div class="col-md-6 offset-md-3">
            <!-- Input & Button Groups -->
            <div class="card card-small mb-4 ml-5 mr-5">
                <div class="card-header border-bottom">
                    <h6> 
                        <a href="{{url('/')}}"> 
                            <i class="fa fa-angle-double-left"></i> Home
                        </a>
                    </h6>
                    <h1 class="lead">
                        Connect live
                        <div style="float:right;font-size: 14px;">
                            <i class="fa fa-spinner fa-spin"></i> 
                        </div>
                    </h1>
                </div>
                <div class="card-body text-center">
                    <p>
                        Chat live with Farmers, Buyers & Service Providers across the country..
                    </p>
                    <br />
                    <i class="fa fa-user"></i>

                    @if(Auth::check())
                        @if(Auth::user()->account_type == "farmer")
                            <a href="/farmer/dashboard"> 
                                {{Auth::user()->name}} (Farmer) 
                            </a>
                        @elseif(Auth::user()->account_type == "service")
                            <a href="/investors/dashboard"> 
                                {{Auth::user()->name}} (Services Provider) 
                            </a>
                        @elseif(Auth::user()->account_type == "buyer")
                            <a href="/service-provider/dashboard">
                                {{Auth::user()->name}} (Service)
                            </a>
                        @endif
                     @endif
                    <hr />
                    <a href="/chat/community/home" class="btn btn-primary btn-square">Continue</a>
                </div>
            </div>
            <!-- / Input & Button Groups -->
        </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')

@endsection