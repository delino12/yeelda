    <br />
    <footer id="gtco-footer" role="contentinfo" style="font-size: 13px;">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-2 gtco-widget">
                    <img src="/images/img-set/logo-transparent-new.png" width="120" height="auto">
                    <br />
                </div>
                <div class="col-md-2 col-md-push-1">
                    <h4 class="y-footer-title">Company</h4>
                    <ul class="gtco-footer-links">
                        <li><a href="{{url('about')}}">About us </a></li>
                        <li><a href="{{url('contact')}}">Contact us</a></li>
                        <li><a href="{{url('terms-and-conditions')}}">Terms</a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-md-push-1">
                    <h4 class="y-footer-title">Solutions</h4>
                    <ul class="gtco-footer-links">
                        <li><a href="{{url('farmer')}}">Farmer</a></li>
                        <li><a href="{{url('services')}}">Services Providers</a></li>
                        <li><a href="{{url('buyers')}}">Buyers</a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-md-push-1">
                    <h4 class="y-footer-title">Platform</h4>
                    <ul class="gtco-footer-links">
                        <li><a href="{{url('search-farmer')}}">Find a Farmer</a></li>
                        <li><a href="{{url('search-service')}}">Find a Service Providers</a></li>
                        <li><a href="{{url('produce-marketplace')}}">Marketplace</a></li>
                        <li><a href="{{url('equipment-marketplace')}}">Farm Services</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-md-push-1">
                    <h4 class="y-footer-title">Resources</h4>
                    <ul class="gtco-footer-links">
                        <li><a href="{{url('blog')}}">Blog</a></li>
                        <li><a href="{{url('faq')}}">FAQ</a></li>
                        {{-- <li><a href="{{url('contact')}}">Help Center</a></li> --}}
                    </ul>
                </div>
            </div>

            <div class="row copyright">
                <div class="col-md-12">
                    <p class="pull-left">
                        <small class="block">&copy; {{ date('Y') }} Yeelda. All Rights Reserved.</small> 
                        {{-- <small class="block">Designed by <a href="http://cavidel.com/" target="_blank">Cavidel.com</a></small> --}}
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>