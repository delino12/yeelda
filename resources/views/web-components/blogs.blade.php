<section id="platform-blog">
	<div class="container-fluid">
		<div class="row">
			<div class="y-blog-wrapper">
				<div class="blog-div">
					<div class="row all-recent-posts"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
    function loadRecentPost(){
        // load recent side bar
        $.get('/load/blog/posts/recents', function(data) {
          /*optional stuff to do after success */
          $('.all-recent-posts').html("");
          var sn = 0;
          $.each(data, function(index, val) {
            sn++;
            $('.all-recent-posts').append(`
                <div class="col-md-4">
                    <div class="y-blog-card">
                        <div class="y-blog-title">
                            <p class="small"><b>Author:</b> `+val.by+` 

                        		<span class="pull-right small">
                        			<b>Date:</b> `+val.last_updated+`
                        		</span>
                            </p>
                            <b>`+val.title+`</b>
                        </div>
                        <div class="y-blog-image">
                            <img src="`+val.image+`" width="100%" height="200">
                        </div>
                        <div class="y-blog-link">
                            <a href="/blog">
                                READ MORE

                                <span class="pull-right">
                                    <i class="fa fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            `);
            if(sn == 3){
                return false;
            }
          });
        });
    }
    loadRecentPost();
</script>