<section id="signin-form-section">
	<div class="container-fluid">
		<div class="row">

			<div class="y-login-title">
				<h1>Log in</h1>
			</div>
			<div class="y-signin-wrapper">
				<div class="y-singin-card">
					<div class="col-sm-8" style="padding: 0em;">
						<img src="{{asset('images/icon-set/new-farmer.png')}}" width="96" height="auto" class="y-signin-image">
						<div class="y-signin-leftbar">
							<h2 class="lead">LOG IN HERE!</h2>

							@if(session('success_status'))
							<div class="alert alert-success">
								<p class="small">{{ session('success_status') }}</p>
							</div>
							@endif

							@if(session('success_msg'))
							<div class="alert alert-success">
								<p class="small">{{ session('success_msg') }}</p>
							</div>
							@endif

							@if(session('login_status'))
							<div class="alert alert-danger">
								<p class="small">{{ session('login_status') }}</p>
							</div>
							@endif
							<form method="POST" action="{{url('account/login')}}">
								{{ csrf_field() }}
								<div class="form-group">
                            		<label for="email">Email</label>
		                            <input type="email" id="email" class="form-control" name="email" placeholder="Email" required="">
		                        </div>

		                        <div class="form-group">
		                            <label for="password">Password</label>
		                            <input type="password" id="password" class="form-control" name="password" placeholder="Password" required="">
		                        </div>

		                        <div class="form-group">
			                        <button type="submit" class="y-btn y-btn-info y-btn-large">
			                        	Log in
			                        </button>
			                    </div>

			                    <div class="form-group">
			                        <p>Or login via:</p>

			                        <div> 
			                        	<div class="fb-login-button" data-size="medium" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
										{{-- <fb:login-button 
										  scope="public_profile,email"
										  onlogin="checkLoginState();">
										</fb:login-button> --}}
										{{-- <div class="g-signin2" style="width: 100%;height: auto;" data-onsuccess="onSignIn"></div> --}}
			                        </div>
			                    </div>
							</form>
						</div>
					</div>

					<div class="col-sm-4" style="padding: 0em;">
						<div class="y-signin-rightbar">
							<div class="y-signup-addon text-center">
								<p style="font-size: 28px;">New to Yeelda?</p>
								<p>
									<a href="{{url('/signup')}}">
										<button class="y-btn y-btn-primary y-btn-large y-thick-border">
											Sign up
										</button> 
									</a>
								</p>

								<a href="{{url('reset-password')}}">Forgot password</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>