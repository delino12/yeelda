<section id="intro-board">
    <div class="container-fluid">
        <div class="row">
            <div class="y-card text-center">

                <div class="y-card-title" style="padding: 0.8em;">
                    Agro Hub
                </div>

                <div class="y-card-body">
                    <p class="text-justify">
                        YEELDA is a start-up Agric-business seeking to create an ecosystem that brings together farmers, off-takers and input/service providers with a view to connecting the demand and suppy for agricultural produce and resources.
                    </p>

                    <p class="text-justify">
                        With the extant challenges within the agricultural space in Nigeria, <b>YEELDA</b> was birthed to provide virtual platform for key stakeholders to interact and mutually benefit through aggregation of disperses data across country.
                    </p>

                    <button class="y-btn y-btn-primary">
                        About us
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>