<section id="search-farmers">
	<div class="container-fluid" style="margin-bottom: 20px;">
		<div class="main-content-container container-fluid px-4 mt-20">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Reports</span>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="loading-bar" style="display: none;">
                    <div class="load-bar">
                      <div class="bar"></div>
                      <div class="bar"></div>
                      <div class="bar"></div>
                    </div>
                </div>
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Land Area Mapping
                            <div class="float-right">
                                <div class="row">
                                    <div class="col-md-3">
                                        <select onchange="getLgas()" id="sort_state" class="custom-select custom-select-sm" style="min-width: 130px;">
                                          <option value="">select state</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <select onchange="getClusters()" id="sort_lgas" class="custom-select custom-select-sm" style="min-width: 130px;">
                                            <option value="">select lgas</option>
                                        </select>
                                    </div>


                                    <div class="col-md-3">
                                        <select id="sort_clusters" onchange="getClusterVillages()" class="custom-select custom-select-sm" style="min-width: 130px;">
                                            <option value="">select clusters</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <button class="btn btn-default btn-sm" onclick="getAllReports()">
                                            Show Reports
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="land_map_area">Loading map...</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0"><p class="target-report-title">Farmers Captured</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3" style="overflow-x: scroll;">
                        <div id="single_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0 target-report-title-cluster"><p>Clusters In Bar Chart</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="clusters_bar_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0 target-title-cluster"><p>Estimated Volume for Cassava</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3" style="overflow-x: scroll;">
                        <div id="produce_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0"><p>Clusters In Donut Pie Chart (Draggable)</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="clusters_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	</div>
</section>	