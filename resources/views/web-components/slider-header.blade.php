<div class="gtco-loader"></div>
<div id="page">
    <header id="gtco-header" class="gtco-cover" role="banner" style="background-image:url(/images/bg_images/{{ $bg }});">
        @include('site-components.menu-bar')

        <section class="overlay"> 
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-3 text-center">
                        @if(session('success_msg'))
                            <div class="alert alert-success">
                                <p class="text-success">{{ session('success_msg') }}</p>
                            </div>
                        @endif
                        <div style="margin-top: -20%;">
                            <div class="display-tc animate-box" data-animate-effect="fadeIn">
                                <br />
                                <h1>
                                  <span class="text-wrapper">
                                    <span class="letters">Buy, Sell & Hire</span>
                                  </span>
                                </h1>

                                <h3>
                                    <a href="javascript:void(0);" style="color: #FFF; margin-top: 10px;" class="typewrite" data-period="2000" data-type='[ 
                                        "Hi, I am Yeelda.", 
                                        "I Connect Farmers, Buyers & Service Providers", 
                                        "Make more from your farm produce and inputs",
                                        "Click Get Started, Join Yeelda Today"]'>
                                        <span class="wrap"></span>
                                    </a>
                                </h3>
                                <a href="/signup"> <button class="btn btn-primary" id="get-started">Get Started</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </header>
    @include('site-components.menu-counter')