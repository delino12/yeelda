<section id="platform-features">
    <div class="container-fluid" id="platform-features-div">
        <div class="row">
            {{-- <div cl --}}
            <div class="y-feature-card">
                <div class="col-md-3" style="padding: 0em;">
                    <div class="y-feature-sidebar text-left">
                        <span style="margin-left: 26px;color:#000;font-weight: 500;">YEELDA CLOUD</span>
                        <ul class="list-unstyled">
                            <li>
                                <a href="javascript:void(0);" id="yf-img-1" class="y-feature-sidebar-item y-active" onclick="switchMenuContent(1)">
                                    Inbox management
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" id="yf-img-2" class="y-feature-sidebar-item" onclick="switchMenuContent(2)">
                                    Product Catalog management
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" id="yf-img-3" class="y-feature-sidebar-item" onclick="switchMenuContent(3)">
                                    Payment management
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9" style="padding: 0em;">
                    <div class="y-feature-body">
                        <div class="y-feature-display-contents"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    // load images
    switchMenuContent(1);

    function switchMenuContent(x) {
        $(".y-feature-sidebar-item").removeClass('y-active');
        if(x == 1){
            $("#yf-img-1").addClass('y-active');
        }else if(x == 2){
            $("#yf-img-2").addClass('y-active');
        }else if(x == 3){
            $("#yf-img-3").addClass('y-active');
        }
        
        $(".y-feature-display-contents").html(`
            <img src="/images/theme/${x}.png" class="y-fixed-card-image">
        `);
    }
</script>