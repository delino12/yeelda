<section id="signin-form-section">
	<div class="container-fluid">
		<div class="row">
			<div class="y-login-title">
				<h1>Sign up</h1>
			</div>
			<div class="y-signin-wrapper">	
				<div class="y-singin-card">
					<div class="col-sm-8" style="padding: 0em;">
						<img src="{{asset('images/icon-set/new-farmer.png')}}" width="96" height="auto" class="y-signin-image">
						<div class="y-signin-leftbar">

							<div class="y-signup-wrapper">
								<form id="y-signup-form" method="POST" onsubmit="return registerNewUser()">
									{{ csrf_field() }}
									<div class="step-wizard">
										<div class="row">
											<div class="col-sm-12 text-center">
												<div class="progress-indicator">
													<span class="step-indicator">
														
													</span>
												</div>
											</div>
										</div>
									</div>
									
									<div class="step-1">
										<div class="form-group">
		                            		<label for="names">Full Name</label>
				                            <input type="text" id="names" class="form-control" name="names" placeholder="Enter your First name and Last name" required="">
				                        </div>

				                        <div class="form-group">
		                            		<label for="email">Email</label>
				                            <input type="email" id="email" autocomplete="" autofocus="on" class="form-control" name="email" placeholder="Enter your Email" required="">
				                        </div>

				                        <div class="form-group">
				                            <label for="phone">Phone</label>
				                            <input type="text" pattern="[0-9]*" id="phone" class="form-control" name="phone" placeholder="Enter your Phone Number" maxlength="11" required="">
				                        </div>

				                        <div class="form-group">
					                        <a href="javascript:void(0);" class="y-btn y-btn-info y-btn-large" onclick="checkStep(2)">
					                        	Continue
					                        </a>
					                    </div>
									</div>

									<div class="step-2" style="display: none;">
										<div class="form-group">
		                            		<label for="password">Password</label>
				                            <input type="password" id="password" class="form-control" name="password" placeholder="Enter your Password" required="">
				                        </div>

				                        <div class="form-group">
				                            <label for="confirm-password">Confirm Password</label>
				                            <input type="password" id="confirm-password" class="form-control" name="confirm-password" placeholder="Re-enter your Password" required="">
				                        </div>

				                        <div class="form-group">
					                        <a href="javascript:void(0);" class="y-btn y-btn-info y-btn-large" onclick="checkStep(3)">
					                        	Continue
					                        </a>
					                    </div>
									</div>

									<div class="step-3" style="display: none;">
										<div class="form-group">
				                            <label for="select-account">Select Account Type</label>
				                            <select class="form-control" id="select-account" name="accountType">
				                            	<option value="farmer"> Farmer </option>
				                            	<option value="service"> Service Providers </option>
				                            	<option value="buyer"> Buyer </option>
				                            </select>
				                        </div>

				                        <div class="form-group">
					                        <a href="javascript:void(0);" class="y-btn y-btn-info y-btn-large" onclick="checkStep(4)">
					                        	Continue
					                        </a>
					                    </div>
									</div>

									<div class="step-4" style="display: none;">
										<div class="form-group text-center">
				                            <label for="terms">
				                            	Agree to <a href="javascript:void(0);" onclick="showTermsApply()">Terms and Conditions</a>
				                            </label>
				                            <br />
				                            <input type="checkbox" id="terms" name="" required="">
				                        </div>

				                        <div class="form-group text-center">
					                        <button type="submit" id="y-signup-btn" class="y-btn y-btn-info y-btn-large">
					                        	Sign up
					                        </button>
					                    </div>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="col-sm-4" style="padding: 0em;">
						<div class="y-signin-rightbar">
							<div class="y-signup-addon text-center">
								<p style="font-size: 28px;">Already have<br /> an account?</p>
								<p>
									<a href="{{url('/signin')}}">
										<button class="y-btn y-btn-primary y-btn-large y-thick-border">
											Log in
										</button> 
									</a>
								</p>

								<a href="{{url('reset-password')}}">Forgot password</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- TERMS & CONDITIONS MODAL -->
<div class="modal fade" id="terms-condition-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Terms & Condition</h4>
      </div>
      <div class="modal-body" style="height: 500;overflow-y: scroll;">
      	<div class="small text-justify">
			<center>
                <div style="width: 80%;">
                    <h4 class="lead" style="font-weight: 600;padding: 1rem;">
                        TERMS AND CONDITIONS <br />FOR USE OF THE WEBSITE, MOBILE APPLICATION, 
                        USSD PROTOCOL, AND OTHER DIGITAL PLATFORMS PROVIDED BY    
                        YEELDA RESOURCES LIMITED
                        <br /><br />
                        February, 2018
                    </h4>
                    <br /><br />
                </div>
            </center>
			<h5 class="y-blue-title">TERMS & CONDITIONS OF USE</h5>
			<p>
			This platform is made available by Yeelda Resources Limited (“YEELDA”), a limited liability company, registered under the laws of the Federal Republic of Nigeria.</p>

			<p>Please read these Terms and Conditions, which set forth the legally binding terms and conditions for your use of the Website, Mobile Application, USSD Protocol, and other digital platforms provided by Yeelda (“the Services”).</p>

			<h5 class="y-blue-title">Summary of Service</h5>

			<p>Through its digital platforms, Yeelda connects farmers to financiers, off takers and service providers. Its objective is to facilitate productivity, commerce, trade and investments in the agricultural sector, and to achieve the overall development of agriculture on the African continent. Yeelda also provides small, medium and large scale farmers with equipment, logistic support services, business development support services, training, marketing, and advertisement, with the aid of conventional and technological means.</p>

			<h5 class="y-blue-title">Acceptance of Terms</h5>

			<p>These Terms apply to all registered users, visitors, and others who access or use the Service. The Service offered is conditioned upon your acceptance of and compliance with these Terms. Also, by accessing or using the Service, you agree to be bound by these Terms, and all other operating rules, policies, and procedures that may be published on any of Yeelda’s digital platforms.</p>

			<p>If you do not agree with any part of the terms, please do not use or access the Service.</p>

			<p>Yeelda reserves the right, at its sole discretion, to change, vary, modify or replace these Terms from time to time by publishing the updated terms on any of its digital platforms. Please check regularly to ensure you are aware of any changes, variations or modifications made, as you will be deemed to have accepted such changes, variations or modifications whether or not you are aware of them.</p>

			<p>If you do not agree with any changes, variations or modifications, your sole recourse will be to cease using the Service.</p>

			<p>Yeelda reserves the right to change, suspend, or discontinue the Service (including, but not limited to, the availability of any feature, database, or content) at any time for any reason. Yeelda may also impose limits on certain features and Services or restrict your access to parts of or the entire Service without notice or liability.</p>

			<h5 class="y-blue-title">Registration</h5>

			<p>You may not be able to use the Service until you have registered and created an account on any of the digital platforms by submitting vital details required of you.</p>
			 

			<p>When you create an account with us, you must provide accurate and complete information at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.</p>

			<p>You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.</p>

			<p>You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>

			<p>You may not use as a username, the name of another person or entity that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.</p>

			<p>Yeelda may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>

			<p>Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>

			<h5 class="y-blue-title">Charges</h5>

			<p>You may be charged by your service provider for downloading and/or accessing the Services on your mobile phone, tablet or other handheld device. These may include data roaming charges if you do this outside your home territory. All these charges are solely your responsibility. If you do not pay the bills for your device, then we assume that you have the permission from the person that does before incurring any of these charges. We will not charge a separate fee for visiting the website or downloading the mobile application, but you may be charged fees for using the Service. These fees will be communicated to you.</p>

			<h5 class="y-blue-title">Intellectual Property</h5>

			<p>The Service and its original content, features and functionality are and will remain the exclusive property of Yeelda, and you shall not directly or indirectly infringe on any and all intellectual property rights, know-how and information belonging to Yeelda, including, without limitation, copyrights, database rights, confidential information, patents, and trademarks. Our trademarks may not be used in connection with any product or service without our prior written consent.</p>

			<p>You shall not directly or indirectly: (i) decipher, decompile, disassemble, reverse engineer, or otherwise attempt to derive any source code or underlying ideas or algorithms of any part of the Service, except to the extent applicable laws specifically prohibit such restriction; (ii) modify, translate, or otherwise create derivative works of any part of the Service; or (iii) copy, rent, lease, distribute, or otherwise transfer any of the rights that you receive hereunder.</p>
			 

			<p>If you choose to provide technical, business or other feedback to Yeelda concerning the Services or any of the platforms, Yeelda will be free to use, disclose, reproduce, license, or otherwise distribute or exploit such feedback in its sole discretion without any obligations or restrictions of any kind, including intellectual property rights or licensing obligations. You understand and agree that the incorporation by Yeelda of feedback into any of its products or services does not grant you any proprietary rights therein.</p>

			<h5 class="y-blue-title">Links to Other Web Sites</h5>

			<p>Our Services may contain links to third-party web sites or services that are not owned or controlled by Yeelda.</p>

			<p>Yeelda has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third-party web sites or services. You further acknowledge and agree that Yeelda shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>

			<p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>

			<h5 class="text-danger">Prohibited Uses</h5>

			You agree not to use the website or mobile application in any way that:
			<ul class="list-group">
			<li class="list-group-item">a.  is unlawful, illegal or unauthorised;</li>
			<li class="list-group-item">b.  is defamatory of any other person;</li>
			<li class="list-group-item">c.  is obscene or offensive;</li>

			<li class="list-group-item">d.  promotes discrimination based on race, sex, tribe, religion, nationality, disability, sexual orientation or age;</li>
			<li class="list-group-item">e.  infringes any copyright, database right or trade mark of any other person;</li>
			<li class="list-group-item">f.  is likely to harass, upset, embarrass, alarm or annoy any other person;</li>
			<li class="list-group-item">g.  is likely to disrupt our service in any way; or</li>

			<li class="list-group-item">h.  advocates, promotes or assists any unlawful act such as (by way of example only) copyright infringement or computer misuse.</li>
			</ul>
			<br />

			<h5 class="y-blue-title">Indemnification</h5>

			<p>You shall defend, indemnify, and hold harmless Yeelda, and its affiliates, employees, contractors, directors, partners suppliers and representatives from all liabilities, claims, and expenses, including reasonable attorneys’ fees, that arise from or relate to your use or misuse of the Service or your violation of these Terms including without limitation to the wrongful use of any and all confidential information or intellectual property, and these include liability incurred when a third party uses your account to access the Service.</p>
			 

			<p>Yeelda reserves the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will assist and cooperate with Yeelda in asserting any available defenses.</p>

			<h5 class="y-blue-title">Limitation of Liability</h5>

			<p>Under no circumstances and in no event shall Yeelda, its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p>

			<h5 class="y-blue-title">Disclaimer</h5>

			<p>While we will endeavour to ensure that the Services are normally available 24 hours a day, we shall not be liable if for any reason the Services are not available at any time or for any period. Access to the Services may be suspended temporarily from time to time and without notice in the case of system failure, maintenance or repair or for any reason beyond our control or if we deem it necessary. For the avoidance of doubt, we shall not be liable for any loss or liability which may be suffered or incurred by you as a result of any suspension of, or interruption to, the operation of the website or mobile application.</p>

			<p>The Services are provided on an “as available” basis. All content on or available through any of the platforms, are provided on an “as is” basis and we do not make any representation or give any warranty in respect of the website and mobile application or any content therein. In particular, but without limitation, we do not give any warranty as to the accuracy, suitability, reliability, completeness, performance, fitness, freedom from viruses or timeliness of the content contained on the website or mobile application. Also, we will not be liable for any errors, omissions, or delays in this information or any losses injuries, or damages arising from its display or use.</p>

			<h4 class="text-danger">We do not accept any responsibility to you for:</h4>

			<ul class="list-group">
			<li class="list-group-item">a.  malfunctions in communications facilities which cannot reasonably be considered to be under our control and that may affect the accuracy or timeliness of messages you send or the material you access via the website or mobile application;</li>

			<li class="list-group-item">b.  any losses or delays in transmission of messages or material you access arising out of the use of any Internet access service provider or mobile network service provider or caused by any browser or other software which is not under our control;</li>

			<li class="list-group-item">c.  viruses that may infect your computer equipment or other property on account of your access to or use of the website or mobile application, or your accessing any materials on the website or mobile application;</li>
			 

			<li class="list-group-item">d.  any unauthorised use or interception of any message or information before it reaches the website or mobile application or our servers from the website or mobile application;</li>

			<li class="list-group-item">e.  any unauthorised use of or access to data relating to you or your transactions which is held by us (unless such use or access is caused by our negligence, fraud or failure to comply with laws relating to the protection of your data), to the extent permitted by local law;</li>

			<li class="list-group-item">f.  any content provided by third parties.</li>
			</ul><br />

			<h5 class="y-blue-title">Governing Law</h5>

			<p>These Terms shall be governed and construed in accordance with the laws of the Federal Republic of Nigeria, and the parties submit to the exclusive jurisdiction of the Nigerian Courts to resolve any dispute between them arising under or in connection with these Terms.</p>

			<h5 class="y-blue-title">Miscellaneous</h5>

			<p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights.</p>

			<p>If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect.</p>

			<p>These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>

			<h5 class="y-blue-title">Contact Us</h5>

			<p>If you have any questions regarding our Services, you can email us at info@yeelda.com</p>
		</div>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button class="btn btn-flat" type="button" data-dismiss="modal">
            close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	// load default step
	checkStep(1);

	function checkStep(x) {
		var names 			= $("#names").val();
		var email 			= $("#email").val();
		var phone 			= $("#phone").val();
		var password 		= $("#password").val();
		var confirm_password = $("#confirm-password").val();
		var select_account 	= $("#select-account").val();
		var terms 			= $("#terms");

		// body...
		if(x == 1){
			$(".step-indicator").html(`
				<img src="images/step-wizard/step-${x}.png">
			`);
		}else if(x == 2){
			if(names == "" && email == ""){
				return false;
			}
			if(!phone){
				return false;
			}
			$(".step-indicator").html(`
				<img src="images/step-wizard/step-${x}.png">
			`);
			$(".step-1").hide();
			$(".step-2").show();
		}else if(x == 3){	
			if(password == "" && confirm_password == ""){
				return false;
			}

			if(password !== confirm_password){
				swal(
					"Oops",
					"Password do not match!",
					"error"
				);

				// return
				return false;
			}

			$(".step-indicator").html(`
				<img src="images/step-wizard/step-${x}.png">
			`);

			$(".step-1").hide();
			$(".step-2").hide();
			$(".step-3").show();
		}else if(x == 4){
			$(".step-indicator").html(`
				<img src="images/step-wizard/step-${x}.png">
			`);

			$(".step-1").hide();
			$(".step-2").hide();
			$(".step-3").hide();
			$(".step-4").show();
		}else if(x == 5){
			if(terms.is(':checked')){
				// pass
				$("#y-signup-form").submit();
			}else{

				swal(
					"Oops",
					"Accept terms & condition to proceed",
					"info"
				);
				return false;
			}
		}

		// var params = {
		// 	names: names,
		// 	email: email,
		// 	phone: phone,
		// 	password: password,
		// 	select_account: select
		// }

		// return 
		return false;
	}

	$(document).ready(function() {
		// $('.tooltip').tooltipster();
		$('#terms').iCheck({
		    checkboxClass: 'icheckbox_flat-blue',
		    radioClass: 'iradio_flat-blue',
		    increaseArea: '20%' // optional
		});
	});

	// check agreed terms
	function checkAgreeTerms() {
		if($("#terms").is(":checked")){
			// return true;
			$("#y-signup-form").submit();
		}else{
			swal(
				"Oops",
				"Click Continue to proceed.",
				"error"
			);
			return false;
		}
	}

	// show terms & conditions
	function showTermsApply() {
		$("#terms-condition-modal").modal();
	}

	// registered new user
	function registerNewUser() {
		$("#y-signup-btn").prop("disabled", true);
		$("#y-signup-btn").html(`
			Processing...
		`);
		var token 		= '{{ csrf_token() }}';
		var names 		= $("#names").val();
		var email 		= $("#email").val();
		var phone 		= $("#phone").val();
		var password 	= $("#password").val();
		var accountType = $("#select-account").val();

		var params = {
			_token: token,
			names: names,
			email: email,
			phone: phone,
			password: password,
			accountType: accountType
		}

		$.post('{{url("account/register")}}', params, function(data, textStatus, xhr) {
			if(data.status == "success"){
				swal(
					"Ok",
					data.message,
					data.status
				);
				$("#y-signup-btn").prop("disabled", false);
				$("#y-signup-btn").html(`
					Sign up
				`);

				// set timeout
				setTimeout(function(){
					// redirect to signing
					window.location.href = "/signin";
				}, 3000);
			}else{
				swal(
					"Oops",
					data.message,
					data.status
				);
				$("#y-signup-btn").prop("disabled", false);
				$("#y-signup-btn").html(`
					Sign up
				`);
			}
		}).fail(function(err){
			$("#y-signup-btn").prop("disabled", false);
			$("#y-signup-btn").html(`
				Sign up
			`);
		});

		// return 
		return false;
	}
</script>