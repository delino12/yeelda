<section id="y-market-section">
	<div class="container-fluid">
		<div class="row">
			<div class="y-marketplace-card">
				<div class="col-md-6" style="padding: 0.3rem;width: auto;">
					<select class="select-dropdown" id="listed-product">
						<option value="">Category</option>
					</select>
					<select class="select-dropdown">
						<option value="">Location</option>
						<option value="Abuja FCT">Abuja FCT</option>
		                <option value="Abia">Abia</option>
		                <option value="Adamawa">Adamawa</option>
		                <option value="Akwa Ibom">Akwa Ibom</option>
		                <option value="Anambra">Anambra</option>
		                <option value="Bauchi">Bauchi</option>
		                <option value="Bayelsa">Bayelsa</option>
		                <option value="Benue">Benue</option>
		                <option value="Borno">Borno</option>
		                <option value="Cross River">Cross River</option>
		                <option value="Delta">Delta</option>
		                <option value="Ebonyi">Ebonyi</option>
		                <option value="Edo">Edo</option>
		                <option value="Ekiti">Ekiti</option>
		                <option value="Enugu">Enugu</option>
		                <option value="Gombe">Gombe</option>
		                <option value="Imo">Imo</option>
		                <option value="Jigawa">Jigawa</option>
		                <option value="Kaduna">Kaduna</option>
		                <option value="Kano">Kano</option>
		                <option value="Katsina">Katsina</option>
		                <option value="Kebbi">Kebbi</option>
		                <option value="Kogi">Kogi</option>
		                <option value="Kwara">Kwara</option>
		                <option value="Lagos">Lagos</option>
		                <option value="Nassarawa">Nassarawa</option>
		                <option value="Niger">Niger</option>
		                <option value="Ogun">Ogun</option>
		                <option value="Ondo">Ondo</option>
		                <option value="Osun">Osun</option>
		                <option value="Oyo">Oyo</option>
		                <option value="Plateau">Plateau</option>
		                <option value="Rivers">Rivers</option>
		                <option value="Sokoto">Sokoto</option>
		                <option value="Taraba">Taraba</option>
		                <option value="Yobe">Yobe</option>
		                <option value="Zamfara">Zamfara</option>
		                <option value="Outside Nigeria">Outside Nigeria</option>
					</select>
					<select class="select-dropdown">
						<option value="">Planted Date</option>
					</select>
					<select class="select-dropdown">
						<option value="">Yield Date</option>
					</select>
				</div>
				<div class="col-md-6" style="padding: 0rem;">
					<div class="y-search-div">
						<input type="text" class="select-search-input" placeholder="Enter search.. Maize, rice, corn etc." name="">
						<button class="y-search-btn">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<br />
				<div class="row load-products">
	            	<img src="/svg/yeelda-loading.svg" width="auto" height="70px" class="loading">
	          	</div>
	         </div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$.get('/listed/products', function (e){
      // $('#listed-product').html('');
      $.each(e, function (index, value){
        // console.log(value);
        $('#listed-product').append(`
          <option value="`+value+`">`+value+`</option>
        `);
      });
    });
</script>