<section id="social-media-footer">
	<div class="container-fluid">
		<div class="row">
			<div class="y-socials-handler">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="socials">
						<a href="javascript">
							<img src="{{asset('images/socials/pintrest.png')}}" class="y-s-media" width="44" height="auto">
						</a>
						<a href="https://twitter.com/InfoYeelda">
							<img src="{{asset('images/socials/twitter.png')}}" class="y-s-media" width="44" height="auto">
						</a>
						<a href="javascript:void(0);">
							<img src="{{asset('images/socials/facebook.png')}}" class="y-s-media" width="44" height="auto">
						</a>
						<a href="https://www.instagram.com/yeelda_africa">
							<img src="{{asset('images/socials/instagram.png')}}" class="y-s-media" width="44" height="auto">
						</a>
						<a href="javascript:void(0);">
							<img src="{{asset('images/socials/linkedin.png')}}" class="y-s-media" width="44" height="auto">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>