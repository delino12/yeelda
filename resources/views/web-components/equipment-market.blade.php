<section id="y-market-section">
	<div class="container-fluid">
		<div class="row">
			<div class="y-equipment-card">
				{{-- card menu for search --}}
				<div class="y-card-menu">
					<div class="row">
						<div class="col-md-3 text-center">
							<img class="y-image" onclick="showContents('seeds')" src="/images/icon-set/seeds.png" width="100" height="auto" />
							<h1 class="lead">Seeds</h1>
							<br />
						</div>

						<div class="col-md-3 text-center">
							<img class="y-image" onclick="showContents('pesticide')" src="/images/icon-set/pesticide.png" width="100" height="auto" />
							<h1 class="lead">Pest Control</h1>
							<br />
						</div>

						<div class="col-md-3 text-center">
							<img class="y-image" onclick="showContents('fertilizer')" src="/images/icon-set/fertilizer.png" width="100" height="auto" />
							<h1 class="lead">Fertilizer</h1>
							<br />
						</div>

						<div class="col-md-3 text-center">
							<img class="y-image" onclick="showContents('equipment')" src="/images/icon-set/equipment.png" width="100" height="auto" />
							<h1 class="lead">Equipment</h1>
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<img class="y-image" onclick="showContents('services')" src="/images/icon-set/tractor.png" width="100" height="auto" />
							<h1 class="lead">Search for Service Providers</h1>
							<br />
						</div>
						<div class="col-md-6 text-center">
							<img class="y-image" onclick="showContents('farmers')" src="/images/icon-set/new-farmer.png" width="100" height="auto" />
							<h1 class="lead">Search for a Farmer</h1>
							<br />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="y-equipment-item-card">
				{{-- load seeds and contents --}}
				<div class="row">
					<div class="col-md-12">
						<div class="load-seeds" style="display: none;">
							<img src="/images/icon-set/seeds.png" width="50" height="auto" />
							All seed contents here
						</div>
						<br />
						<div class="load-pesticide" style="display: none;">
							<img src="/images/icon-set/pesticide.png" width="50" height="auto" />
							All pesticide contents here
						</div>
						<br />
						<div class="load-fertilizer" style="display: none;">
							<img src="/images/icon-set/fertilizer.png" width="50" height="auto" />
							All fertilizer contents here
						</div>
						<br />
						<div class="load-equipments" style="display: none;">
							<img src="/images/icon-set/tractor.png" width="50" height="auto" />
							All equipment contents here
						</div>
						<br />
					</div>
				</div>
			</div>
		</div>
	</div>
</section>