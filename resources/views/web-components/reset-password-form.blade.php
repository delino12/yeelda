<section id="signin-form-section">
	<div class="container-fluid">
		<div class="row">

			<div class="y-login-title">
				<h1>FORGOT PASSWORD</h1>

			</div>
			<div class="y-signin-wrapper">
				<div class="y-singin-card">
					<div class="col-sm-8" style="padding: 0em;">
						<img src="{{asset('images/icon-set/new-farmer.png')}}" width="96" height="auto" class="y-signin-image">
						<div class="y-signin-leftbar">
							<p class="small y-blue-title">
								Enter your email below, we’ll send a password reset link to you.
							</p>
							<div class="success_msg"></div>
							<div class="error_msg"></div>
							<form method="POST" onsubmit="return searchUsers()">
								{{ csrf_field() }}
								<div class="form-group">
                            		<label for="email">Email</label>
		                            <input type="email" id="email" class="form-control" name="email" placeholder="Email" required="">
		                        </div>

		                        <div class="form-group">
			                        <button type="submit" id="y-reset-pass-btn" class="y-btn y-btn-primary y-btn-large">
			                        	Send 
			                        </button>
			                    </div>
							</form>
						</div>
					</div>

					<div class="col-sm-4" style="padding: 0em;">
						<div class="y-signin-rightbar">
							<div class="y-signup-addon text-center">
								<p style="font-size: 28px;">New to Yeelda?</p>
								<p>
									<a href="{{url('/signup')}}">
										<button class="y-btn y-btn-primary y-btn-large y-thick-border">
											Sign up
										</button> 
									</a>
								</p>

								<a href="{{url('reset-password')}}">Forgot password</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>