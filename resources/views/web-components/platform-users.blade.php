<section id="platform-users">
    <div class="container-fluid">
        <div class="row">
            <div class="y-platform-card">
                <div class="col-md-4">
                    <div class="y-platform-card-white">
                        <img src="{{asset('images/icon-set/new-farmer.png')}}" class="y-sag-image" width="90" height="auto">
                        <br /><br /><br />
                        <p class="text-justify">
                            <span class="y-platform-title">Farmer</span>
                            <br />
                            A digital platform (market place) to facilitate exchange across the agricultural value chain between potential service providers and off-takers
                        </p>

                        <p class="text-justify">
                            <div class="y-badge-success">Free</div>
                            <ul class="list-unstyled">
                                <li>
                                    <span class="y-list-data">
                                        <i class="fa fa-check"></i> Market access to farmers
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Unlimited upload of farm produce
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Enlightenment on agronomy practices
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Agricultural credit loans to farmers
                                    </span>
                                </li>
                            </ul>
                        </p>

                        <button class="y-btn-mini">Farmer</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="y-platform-card-white">
                        <img src="{{asset('images/icon-set/tractor.png')}}" class="y-sag-image" width="90" height="auto">
                        <br /><br /><br />
                        <p class="text-justify">
                            <span class="y-platform-title">Service Provider</span>
                            <br />
                            A digital platform (market place) to find the nearest farm equipment for hire, sales of equipment and farm inputs
                        </p>

                        <p class="text-justify">
                            <div class="y-badge-success">Free</div>
                            <ul class="list-unstyled">
                                <li>
                                    <span class="y-list-data">
                                        <i class="fa fa-check"></i> Provision of Special Purpose Vehicle (SPV)
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Provision of farm inputs e.g fertilizers, seeds
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Provision of farm equipments
                                    </span>
                                </li>
                            </ul>
                        </p>

                        <button class="y-btn-mini">Service Provider</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="y-platform-card-white">
                        <img src="{{asset('images/icon-set/seeds.png')}}" class="y-sag-image" width="90" height="auto">
                        <br /><br /><br />
                        <p class="text-justify">
                            <span class="y-platform-title">Buyer</span>
                            <br />
                            A digital platform (market place) to purchase quality farm produce and also enhance service delivery in the agricultural value chain
                        </p>

                        <p class="text-justify">
                            <div class="y-badge-success">Free</div>
                            <ul class="list-unstyled">
                                <li>
                                    <span class="y-list-data">
                                        <i class="fa fa-check"></i> Quality delivery time
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Good storage facility of farm produce
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Good quality control and verification
                                    </span>
                                </li>
                                <li>
                                    <span class="y-list-data"><i class="fa fa-check"></i> Provision of shop in shop services
                                    </span>
                                </li>
                            </ul>
                        </p>

                        <button class="y-btn-mini">Buyer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>