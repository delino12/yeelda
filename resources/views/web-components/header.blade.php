<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="icon" href="/images/favicon.png">
        <meta name="google-site-verification" content="pzKPeRDO7Y5TiryGMjnyg7JQr8h5DMsOgs2RZuw0grw" />
        <meta name="description" content="Connect Farmers, Services Providers and buyers, sell farm inputs and farm produce." />
        <meta name="keywords" content="Yeelda Farming Hub, make money from your farm" />
        <meta name="author" content="cavidel.com" />

        <meta property="og:title" content="Yeelda, No.1 Africa Agricultural Hub"/>
        <meta property="og:image" content="http://yeelda.com/images/010.jpg"/>
        <meta property="og:url" content="http://yeelda.com"/>
        <meta property="og:site_name" content="Yeelda"/>
        <meta property="og:description" content="Connecting Farmers, Services Providers and buyers, sell farm inputs and farm produce."/>


        <meta name="twitter:title" content="Yeelda, No.1 Africa Agricultural Hub" />
        <meta name="twitter:image" content="http://yeelda.com/images/010.jpg" />
        <meta name="twitter:url" content="http://yeelda.com" />
        <meta name="twitter:card" content="Connecting Farmers, Services Providers and buyers, sell farm inputs and farm produce." />

        <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link rel="stylesheet" href="/css/animate.css">
        <link rel="stylesheet" href="/css/icomoon.css">
        <link rel="stylesheet" href="/css/bootstrap.css">

        <link rel="stylesheet" href="/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/css/owl.theme.default.min.css">

        <link rel="stylesheet" href="/css/style.css">

        <script src="/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

        <script src="/js/modernizr-2.6.2.min.js"></script>
        <script src="/js/greetings.js"></script>

        <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert2.min.css')}}">

        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118912888-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-118912888-1');
        </script>
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">

        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        @if(Auth::check())
            <input type="hidden" value="{{ csrf_token() }}" id="token" name="">
            <input type="hidden" value="{{ Auth::user()->email }}" id="logged_email" name="">
        @endif
        <div class="gtco-loader"></div>
        <div id="page">