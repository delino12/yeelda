<section id="signin-form-section">
	<div class="container-fluid">
		<div class="row">
			<div class="y-login-title">
				<h3>RESET PASSWORD</h3>
			</div>
			<div class="y-signin-wrapper">
				<div class="y-singin-card">
					<div class="col-sm-8" style="padding: 0em;">
						<img src="{{asset('images/icon-set/new-farmer.png')}}" width="96" height="auto" class="y-signin-image">
						<div class="y-signin-leftbar">
							<div class="success_msg"></div>
							<div class="error_msg"></div>
							<form id="reset-form" method="post" onsubmit="return resetPassword()">
		                        <div class="form-group">
		                            <label for="passwordA">Password</label>
		                            <input type="password" class="form-control" id="passwordA" placeholder="Enter password" required="">
		                        </div>
		                        <div class="form-group">
		                            <label for="passwordB">Confirm Passowrd</label>
		                            <input type="password" class="form-control" id="passwordB" placeholder="Confirm password" required="">
		                        </div>
			                    <div class="form-group">
			                        <button id="y-reset-pass-btn" class="y-btn y-btn-primary" style="width: 100%;">
			                           <i class="fa fa-lock"></i> Reset Password
			                        </button>
			                    </div>
			                </form>
						</div>
					</div>

					<div class="col-sm-4" style="padding: 0em;">
						<div class="y-signin-rightbar">
							<div class="y-signup-addon text-center">
								<p style="font-size: 28px;">New to Yeelda?</p>
								<p>
									<a href="{{url('/signup')}}">
										<button class="y-btn y-btn-primary y-btn-large y-thick-border">
											Sign up
										</button> 
									</a>
								</p>

								<a href="{{url('reset-password')}}">Forgot password</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>