<section id="search-farmers">
	<div class="container-fluid">
		<div class="row" style="height: auto;background-color: rgba(242, 242, 242); margin-top: 137px;">
			@foreach($users as $user)
				<div class="col-md-3">
					<div class="y-farmer-profile-card">
						<div class="y-farmer-image text-center">
							{{-- <img src="" class="img-rounded " width="120" height="auto"> --}}
							<div class="img-rounded" style="background: url('{{ $user['avatar'] }}') 50%;width: 100%;height: 150px;background-size: cover;"></div>
						</div>
						<div class="y-farmer-initials text-center">
							<span class="truncate"><strong>{{ str_limit($user['name'], $limit = "30", $end = '...') }}</strong> | Farmer</span>
							<br />
							<span class="small"><b>Grows:</b> {{ $user['crops'] }}</span>
						</div>
						<div class="y-farmer-addon text-center">
							<a href="ask-seller/{{ $user['email'] }}" style="color: #2CA8C9;padding: 1.8rem;">
								<i class="fa fa-envelope" style="font-size: 28px;"></i>
							</a>
							-
							<a href="farmers-marketplace/{{ str_replace(" ", "_", strtolower($user['name'])) }}/{{ $user['id'] }}" style="color: #2CA8C9;padding: 1.8rem;">
								<i class="fa fa-shopping-cart" style="font-size: 28px;"></i>
							</a>
						</div>
					</div>
					<br />
				</div>
			@endforeach
		</div>
	</div>
</section>	
		</div>
		<hr />
		<div class="row">
			<div class="col-md-12">
				<div class="paginate-div"></div>
			</div>
		</div>
	</div>
</section>	