<section id="search-farmers">
	<div class="container-fluid">
		<div class="row" style="height: auto;background-color: rgba(242, 242, 242);">
			<div class="y-find-farmers" id="load-services">
				@foreach($users as $user)
					<div class="col-md-3">
						<div class="y-farmer-profile-card">
							<div class="y-farmer-image text-center">
								<img src="{{ $user['avatar'] }}" class="img-rounded " width="120" height="auto">
							</div>
							<div class="y-farmer-initials text-center">
								<span><strong>{{ $user['name'] }}</strong> | Service Providers</span>
							</div>
							<div class="y-farmer-addon text-center">
								<a href="send/message/{{ $user['email'] }}">
								<i class="fa fa-envelope" style="font-size: 18px;"></i>
							</div>
						</div>
						<br />
					</div>
				@endforeach
			</div>
		</div>
	</div>
</section>	