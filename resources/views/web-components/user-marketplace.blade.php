<section id="y-market-section">
	<div class="container-fluid">
		<div class="row">
			<div class="y-marketplace-card">
				<div class="col-md-6" style="padding: 0.3rem;width: auto;">
					<select class="select-dropdown" id="listed-product">
						<option value="">Category</option>
					</select>
					<select class="select-dropdown">
						<option value="">Location</option>
						<option value="Abuja FCT">Abuja FCT</option>
		                <option value="Abia">Abia</option>
		                <option value="Adamawa">Adamawa</option>
		                <option value="Akwa Ibom">Akwa Ibom</option>
		                <option value="Anambra">Anambra</option>
		                <option value="Bauchi">Bauchi</option>
		                <option value="Bayelsa">Bayelsa</option>
		                <option value="Benue">Benue</option>
		                <option value="Borno">Borno</option>
		                <option value="Cross River">Cross River</option>
		                <option value="Delta">Delta</option>
		                <option value="Ebonyi">Ebonyi</option>
		                <option value="Edo">Edo</option>
		                <option value="Ekiti">Ekiti</option>
		                <option value="Enugu">Enugu</option>
		                <option value="Gombe">Gombe</option>
		                <option value="Imo">Imo</option>
		                <option value="Jigawa">Jigawa</option>
		                <option value="Kaduna">Kaduna</option>
		                <option value="Kano">Kano</option>
		                <option value="Katsina">Katsina</option>
		                <option value="Kebbi">Kebbi</option>
		                <option value="Kogi">Kogi</option>
		                <option value="Kwara">Kwara</option>
		                <option value="Lagos">Lagos</option>
		                <option value="Nassarawa">Nassarawa</option>
		                <option value="Niger">Niger</option>
		                <option value="Ogun">Ogun</option>
		                <option value="Ondo">Ondo</option>
		                <option value="Osun">Osun</option>
		                <option value="Oyo">Oyo</option>
		                <option value="Plateau">Plateau</option>
		                <option value="Rivers">Rivers</option>
		                <option value="Sokoto">Sokoto</option>
		                <option value="Taraba">Taraba</option>
		                <option value="Yobe">Yobe</option>
		                <option value="Zamfara">Zamfara</option>
		                <option value="Outside Nigeria">Outside Nigeria</option>
					</select>
					<select class="select-dropdown">
						<option value="">Max Price</option>
						<option value="100000 - 500000">&#8358; 100,000.00 - 500,000.00</option>
						<option value="500000 - 999999">&#8358; 500,000.00 above</option>
					</select>
					<select class="select-dropdown">
						<option value="">Min Price</option>
						<option value="100000 - 50000">&#8358; 100,000.00 - 50,000.00</option>
						<option value="500000 - 00000">&#8358; 50,000.00 below</option>
					</select>
				</div>
				<div class="col-md-6" style="padding: 0rem;">
					<div class="y-search-div">
						<input type="text" class="select-search-input" placeholder="Enter search.. Maize, rice, corn etc." name="">
						<button class="y-search-btn">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2" style="margin: 0px;padding: 0px;">
				
				<div class="feature-bg-image" style="background-image: url('/images/bg_images/7.jpg');background-size: cover;height: 160px;">
					<div class="profile-thumbnail" style="background-image: url({{ $user->basic->avatar }});">
					</div>
				</div>
				<div class="feature-contents" style="margin: 10px;padding: 10px;">
					<br />
						<p>
							<strong>{{ $user->name }}</strong>
						</p>
						<p>
							<strong>Available produce (2)</strong>
							<ul class="list-group">
								<li>Cassava</li>
								<li>Maize</li>
							</ul>
						</p>
						<p><strong>Future harvest (0)</strong></p>
						<p>
							<strong>Rating</strong><br />
							<i class="fa fa-star" style="color:#FFCC00;"></i>
							<i class="fa fa-star" style="color:#FFCC00;"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
						</p>

						@if(Auth::check())
							@if(Auth::user()->id == $user->id)
								<p>
									<strong><a href="/settings/marketplace/{{ $user->id }}">Settings</a></strong>
								</p>
							@endif
						@endif
				</div>
			</div>
			<div class="col-md-10">
				<div class="row load-products">
	            	<img src="/svg/yeelda-loading.svg" width="auto" height="70px" class="loading">
	          	</div>
	         </div>
		</div>

		
	</div>
</section>

<script type="text/javascript">
	$.get('/listed/products', function (e){
      // $('#listed-product').html('');
      $.each(e, function (index, value){
        // console.log(value);
        $('#listed-product').append(`
          <option value="`+value+`">`+value+`</option>
        `);
      });
    });
</script>