
<section id="intro-video">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12" style="padding: 0rem;">
				{{-- <div class="y-video-intro-overlay"></div> --}}
				<div class="y-video-intro text-center">
					<a href="javascript:void(0);" class="y-play-btn">
						<i class="fa fa-play-circle-o fa-3x"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>