@extends('layouts.admin-pages-skin')

@section('title')
  YEELDA | Dashboard
@endsection

@section('contents')
    <style>
      #chartdiv {
        width: 100%;
        height: 290px;
      }                 
    </style>

    @include('components.sticky-menu')

    <div class="main-content-container container-fluid px-4">
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Dashboard</span>
            <h3 class="page-title">Overview</h3>
          </div>
        </div>

        <div class="row">
          <div class="col-lg col-md-6 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
              <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                  <div class="stats-small__data text-center">
                    <span class="stats-small__label text-uppercase">Total Payment</span>
                    <h6 class="my-3" id="tt"></h6>
                  </div>
                  <div class="stats-small__data">
                    <span class="stats-small__percentage stats-small__percentage--increase">4.7%</span>
                  </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-1"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg col-md-6 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
              <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                  <div class="stats-small__data text-center">
                    <span class="stats-small__label text-uppercase">Successful Transfer</span>
                    <h6 class="my-3" id="st"></h6>
                  </div>
                  <div class="stats-small__data">
                    <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                  </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-2"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg col-md-4 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
              <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                  <div class="stats-small__data text-center">
                    <span class="stats-small__label text-uppercase">Pending Transfer</span>
                    <h6 class="my-3" id="pt"></h6>
                  </div>
                  <div class="stats-small__data">
                    <span class="stats-small__percentage stats-small__percentage--decrease">3.8%</span>
                  </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-3"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg col-md-4 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
              <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                  <div class="stats-small__data text-center">
                    <span class="stats-small__label text-uppercase">Failed Transfer</span>
                    <h6 class="my-3" id="ft"></h6>
                  </div>
                  <div class="stats-small__data">
                    <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                  </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-4"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg col-md-4 col-sm-12 mb-4">
            <div class="stats-small stats-small--1 card card-small">
              <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                  <div class="stats-small__data text-center">
                    <span class="stats-small__label text-uppercase">Approved Transaction</span>
                    <h6 class="my-3" id="at"></h6>
                  </div>
                  <div class="stats-small__data">
                    <span class="stats-small__percentage stats-small__percentage--decrease">2.4%</span>
                  </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-5"></canvas>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
            <div class="card card-small">
              <div class="card-header border-bottom">
                <h6 class="m-0">Transactions</h6>
              </div>
              <div class="card-body pt-0">
                <div class="row border-bottom py-2 bg-light">
                  <div class="col-12 col-sm-6">
                    <div id="blog-overview-date-range" class="input-daterange input-group input-group-sm my-auto ml-auto mr-auto ml-sm-auto mr-sm-0" style="max-width: 350px;">
                      <input type="text" class="input-sm form-control" name="start" placeholder="Start Date" id="blog-overview-date-range-1">
                      <input type="text" class="input-sm form-control" name="end" placeholder="End Date" id="blog-overview-date-range-2">
                      <span class="input-group-append">
                        <span class="input-group-text">
                          <i class="material-icons"></i>
                        </span>
                      </span>
                    </div>
                  </div>
                  <div class="col-12 col-sm-6 d-flex mb-2 mb-sm-0">
                    <button type="button" class="btn btn-sm btn-white ml-auto mr-auto ml-sm-auto mr-sm-0 mt-3 mt-sm-0">View Full Report &rarr;</button>
                  </div>
                </div>
                <canvas height="130" style="max-width: 100% !important;" class="blog-overview-users"></canvas>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
            <div class="card card-small h-100">
              <div class="card-header border-bottom">
                <h6 class="m-0">Chart Analytics</h6>
              </div>
              <div class="card-body d-flex py-0">
                <div id="chartdiv"></div>
              </div>
              <div class="card-footer border-top">
                <div class="row">
                  <div class="col">
                    <select class="custom-select custom-select-sm" style="max-width: 130px;">
                      <option selected>Last Week</option>
                      <option value="1">Today</option>
                      <option value="2">Last Month</option>
                      <option value="3">Last Year</option>
                    </select>
                  </div>
                  <div class="col text-right view-report">
                    <a href="#">Full report &rarr;</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
          <div class="card card-small blog-comments">
            <div class="card-header border-bottom">
              <h6 class="m-0">Yeelda Users</h6>
            </div>
            <div class="card-body p-4">
              <h1 class="lead"><img src="/images/icon-set/farmer.png"> Farmers</h1>
              <div class="loading-bar" style="display: none;">
                <div class="load-bar">
                  <div class="bar"></div>
                  <div class="bar"></div>
                  <div class="bar"></div>
                </div>
              </div>
              <table id="farmers" class="table">
                  <thead>
                      <tr>
                          <th>S/N</th>
                          <th><i class="fa fa-user"></i> Name</th>
                          <th><i class="fa fa-envelope"></i> Email</th>
                          <th>Date</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody class="list-farmers"></tbody>
              </table>
              <center>
                  <a href="/admin/all/farmers">View All</a>
              </center>
              <br /><br />

              <h1 class="lead"><img src="/images/icon-set/farm-service.png"> Services providers</h1>
              <div class="loading-bar" style="display: none;">
                <div class="load-bar">
                  <div class="bar"></div>
                  <div class="bar"></div>
                  <div class="bar"></div>
                </div>
              </div>
              <table id="service" class="table">
                  <thead>
                      <tr>
                          <th>S/N</th>
                          <th><i class="fa fa-user"></i> Name</th>
                          <th><i class="fa fa-envelope"></i> Email</th>
                          <th>Date</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody class="list-service"></tbody>
              </table>
              <center>
                  <a href="/admin/all/services-providers">View All</a>
              </center>

              <br /><br />
              <h1 class="lead"><img src="/images/icon-set/produce.png"> Buyers</h1>
              <div class="loading-bar" style="display: none;">
                <div class="load-bar">
                  <div class="bar"></div>
                  <div class="bar"></div>
                  <div class="bar"></div>
                </div>
              </div>
              <table id="investor" class="table">
                  <thead>
                      <tr>
                          <th>S/N</th>
                          <th><i class="fa fa-user"></i> Name</th>
                          <th><i class="fa fa-envelope"></i> Email</th>
                          <th>Date</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody class="list-investor"></tbody>
              </table>
              <center>
                  <a href="/admin/all/buyers">View All</a>
              </center>
              <br />
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
          <div class="card card-small h-100">
            <div class="card-header border-bottom">
              <h6 class="m-0">Users Statistic</h6>
            </div>
            <div class="card-body d-flex flex-column">
              <h1 class="lead"><img src="/images/icon-set/farm-band.png"> Details</h1>
              <div class="info_msg"></div>
              <table class="table">
                  <thead>
                      <tr>
                          <th>Details</th>
                          <th>Registered</th>
                          <th>Pending</th>
                      </tr>
                  </thead>
                  <tbody class="users-statistic"></tbody>
              </table>

              <br /><br />
              <h1 class="lead"><img src="/images/icon-set/farm-band.png"> Associations & Groups</h1>
              <div class="info_msg"></div>
              <table class="table">
                  <thead>
                      <tr>
                          <th>Groups</th>
                          <th>status</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody class="load-association-request"></tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('amcharts/js/core.js') }}"></script>
    <script src="{{ asset('amcharts/js/charts.js') }}"></script>
    <script src="{{ asset('amcharts/js/animated.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
    <script src="{{asset('admin-assets/scripts/extras.1.0.0.min.js')}}"></script>
    <script src="{{asset('admin-assets/scripts/shards-dashboards.1.0.0.min.js')}}"></script>
    <script src="{{asset('admin-assets/scripts/app/app-blog-overview.1.0.0.js')}}"></script>
    <script type="text/javascript">
      loadUsersStatistics();
      
      function loadChartData(series) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart3D);

        // Add data
        chart.data = [
          {
            "users": "Farmer",
            "total": series.total_farmers
          },
          {
            "users": "Service",
            "total": series.total_services
          },
          {
            "users": "Buyers",
            "total": series.total_buyers
          },
        ];

        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "users";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.renderer.labels.template.hideOversized = false;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.tooltip.label.rotation = 270;
        categoryAxis.tooltip.label.horizontalCenter = "right";
        categoryAxis.tooltip.label.verticalCenter = "middle";

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Users";
        valueAxis.title.fontWeight = "bold";

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = "total";
        series.dataFields.categoryX = "users";
        series.name = "Visits";
        series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", (fill, target) => {
          return chart.colors.getIndex(target.dataItem.index);
        })

        columnTemplate.adapter.add("stroke", (stroke, target) => {
          return chart.colors.getIndex(target.dataItem.index);
        })

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;
      }
      
      function loadUsersStatistics() {
        $.get('{{url('load/charts/statistic')}}', function(data) {
          $(".users-statistic").html(`
              <tr>
                  <td>Total Farmers</td>
                  <td>${data.total_farmers}</td>
                  <td>${data.temp_farmers}</td>
              </tr>

              <tr>
                  <td>Total Services Providers</td>
                  <td>${data.total_services}</td>
                  <td>${data.temp_services}</td>
              </tr>

              <tr>
                  <td>Total Buyers</td>
                  <td>${data.total_buyers}</td>
                  <td>${data.temp_buyers}</td>
              </tr>

              <tr>
                  <td>Total Users</td>
                  <td>${data.total_overall}</td>
                  <td>${data.temp_overall}</td>
              </tr>
          `);

          loadChartData(data);
        });
      }
    </script>
@endsection