@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | Equipment Details
@endsection

{{-- Contents  --}}
@section('contents')
    <!-- / .main-navbar -->
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">YEELDA PRODUCT</span>
          </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0">
                  <img class="y-img" src="/images/icon-set/tractor.png" height="64" width="auto">
                  <span id="equipment-title"></span>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                  <div class="col-md-6">
                    <a href="{{url('/admin/products')}}" style="margin-left: 20px;"><i class="fa fa-chevron-left"></i> Go back</a>
                    <div class="load-equipment"></div>
                  </div>
                  <div class="col-md-6">
                    <div class="load-others"></div>
                  </div>
              </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- custom js --}}
    <script type="text/javascript">
      // load single equipment
      loadSingleEquipment();

      function loadSingleEquipment() {
          var pid = '{{ $id }}';
          $.get('/admin/load-equipment/'+pid, function (value){

            $(".load-equipment").html(`
              <div class="col-md-12">
                <div class="card h-100" style="padding: 1em;">
                  <h4 class="lead">Product Information</h4><br />
                  <a href="#">
                  <img class="card-img-top" src="/uploads/products/`+value.image+`" width="350" height="300" alt=""></a>
                  <div class="card-body">
                    <h4 class="small">
                      <a href="#"><i class="fa fa-user"></i> `+value.owner+` (Farmer)</a>
                    </h4>
                    <h5> &#8358; `+value.price+`</h5>
                    <p>
                      <table class="table">
                        <tr>
                          <td>Equipment Name</td>
                          <td>`+value.no+` `+value.name+`</td>
                        </tr>
                        <tr>
                          <td>Unit price</td>
                          <td> <b>&#8358;`+value.price+`</b></td>
                        </tr>
                        <tr>
                          <td>No of equipment available</td>
                          <td><b>`+value.no+`</b></td>
                        </tr>
                        <tr>
                          <td>Location</td>
                          <td>`+value.address+`</td>
                        </tr>
                        <tr>
                          <td>Description</td>
                          <td>`+value.note+`. `+value.descriptions+`</td>
                        </tr>
                      </table>
                    </p>
                  </div>
                  <div class="card-footer">
                    <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                  </div>
                </div>
              </div>
            `);

            $("#equipment-title").html(value.name);

            $(".farmer-info").append(`
              <tr>
                <td>Name: </td>
                <td>`+value.owner+`</td>
              </tr>

              <tr>
                <td>Email: </td>
                <td>`+value.email+`</td>
              </tr>

              <tr>
                <td>Mobile: </td>
                <td>`+value.phone+`</td>
              </tr>
            `);

            if(owner_image == ""){
              $(".load-basic").append(`
                <img class="img-rounded" src="/uploads/equipments/`+value.image+`" height="200" width="200" alt="farmer+image">
              `);
            }else{
              $(".load-basic").append(`
                <img class="img-rounded" src="/uploads/equipment/`+value.image+`" height="200" width="200" alt="farmer+image">
              `);
            }
          });
      }
    </script>
@endsection