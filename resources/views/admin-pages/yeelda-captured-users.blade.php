@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Temporary Profile
@endsection

{{-- Contents  --}}
@section('contents')
    <style type="text/css">
        .y-thumbnail {
            height: auto;
            border-radius: 4px;
            border:1px solid #FFF;
            width: auto;
        }
    </style>

    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">View user</span>
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ url('admin/data-capture') }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                    
                </h6>
                <div align="right">
                    Captured By: <span id="agent_id"></span>

                    <button class="btn btn-default" onclick="showEditModal()">
                        <i class="fa fa-edit"></i> Edit
                    </button>
                </div>
              </div>
              <div class="card-body p-5 pb-3">
                <div class="row">
                    <div class="col-md-5">
                        <h1 class="lead"><i class="fa fa-user"></i>  Personal Details</h1>
                        <div class="image-preview"></div>
                        <div class="profile-img y-thumbnail"></div>
                        <div id="edit-image-div" style="display: none;">
                            <div class="dropzone" style="min-height: 10px;padding: 0px;background-color: rgba(000,000,000,0.50);border-radius: 4px;"></div>
                        </div>
                        <a href="javascript:void(0);" onclick="showModalEditPhoto()"><i class="fa fa-edit"></i> Edit image</a>
                        <br />
                        <div class="load-profile"></div>
                    </div>
                    <div class="col-md-7">
                        <br />
                        <br />
                        
                        <h1 class="lead"><i class="fa fa-tags"></i> Crops </h1>
                        <div class="load-farm-data"></div>
                        <table class="table">
                            <thead></thead>
                            <tbody class="load-crops"></tbody>
                        </table>

                        {{-- get road map --}}
                        <h1 class="lead"><i class="fa fa-map"></i> Road Map </h1>
                        <div class="map-results">
                            <img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading-b">
                            <img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading-a">
                            <div class="row">
                                <div class="col-sm-6"><div id="map-a"><img src="/svg/yeelda-loading.svg" height="60" width="auto" /></div></div>
                                <div class="col-sm-6"><div id="map-b"><img src="/svg/yeelda-loading.svg" height="60" width="auto" /></div></div>
                            </div>
                        </div>
                        <table>
                            <tr>
                                <td><span id="latitude"></span></td>
                            </tr>
                            <tr>
                                <td><span id="longitude"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>


    <div class="modal" id="show-edit-modal-odca" role="modal" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    Edit Farmers Records
                </div>
                <div class="modal-body">
                    <form class="post-form" id="capture-form" method="post" onsubmit="return false">
                        <input type="hidden" id="edit_accountType" value="{{ $accountType }}" name="">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>First name</label>
                                    <input type="text" class="form-control" id="edit_firstname" placeholder="Enter firstname" required="">
                                </div>

                                <div class="col-sm-6">
                                    <label>Last name</label>
                                    <input type="text" class="form-control" id="edit_lastname" placeholder="Enter lastname" required="">
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Email</label>
                                    <input type="email" id="edit_email" class="form-control" placeholder="example@domain.com">
                                </div>

                                 <div class="col-sm-6">
                                    <label>Phone number</label>
                                    <input type="text" id="edit_mobile" required="" class="form-control" placeholder="0803000*****" maxlength="11">
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>State</label>
                                    <input type="text" id="edit_state" class="form-control" required="">
                                </div>

                                <div class="col-md-6">
                                    <label>LGA</label>
                                    <input type="text" id="edit_lga" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Prefered Language</label>
                                    <input type="text" id="edit_language" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Address</label>
                                    <textarea id="edit_address" class="form-control" rows="4" cols="7" placeholder="Type address here"></textarea>
                                </div>
                            </div>
                        </div>
                        <hr />
                        
                        <div class="add-on"></div>
                        <div class="form-group">
                            <button class="btn btn-primary" id="update-btn">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript" src="/dropzone/js/dropzone.js"></script>
    <script type="text/javascript">
        // show hidden div
        function showModalEditPhoto() {
            // trigger modal
            $(".y-thumbnail").toggle();
            $("#edit-image-div").toggle();
        }

        // account type
        var accountType = '{{$accountType}}';
        var userid = '{{$id}}';

        // update image
        $(".dropzone").dropzone({
            url: "/admin/update/captured/images",
            params: {
                _token:'{{ csrf_token() }}',
                accountType:accountType,
                userid:userid
            },
            success: function(e, res) {
                // body...
                // console.log(e.status);
                // console.log(res);
                $("#avatar").val(res);
                $(".image-preview").html(`
                    <img src="/images/captured-images/`+res+`" width="120" height="120">
                `);

                if(e.status == 'success'){
                    // hide upload image
                    $(".dropzone").hide();
                    $(".dropzone-label").hide();
                }
            }
        });

        $.get('/load-temp/{{$accountType}}/{{$id}}', function(data) {
            /*optional stuff to do after success */
            // console.log(data);
            if(data.accountType == 'farmer'){
                $('.l-other-details').html(`
                    <br /><br />
                `);

                

                if(data.agent_id == null){
                    data.agent_id = "No agent id";
                }
                // agent id
                $("#agent_id").html(data.agent_id);

                $(".load-farm-data").html(`
                    <h1 class="lead">
                        <i class="fa fa-edit"></i> Farm Record 

                        <span class="float-right">
                            Reg Date: ${data.date_captured}
                        </span>
                    </h1>
                    <table class="table">
                        <tr>
                            <td>Land Capacity</td>
                            <td>`+data.farmSizeNo+` `+data.farmSizetype+`  </td>
                        </tr>
                        <tr>
                            <td>Soil Texture</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Road Map</td>
                            <td>`+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Distance</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Discription & Remark</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Crop Produce</td>
                            <td>${data.farmSizeCrop}</td>
                        </tr>
                        <tr>
                            <td>Previous harvested</td>
                            <td>${data.prevCropPlant}</td>
                        </tr>
                        <tr>
                            <td>Amount Sold (&#8358;)</td>
                            <td>&#8358;${data.prevYieldHaves}</td>
                        </tr>
                    </table>
                `);
                $('.load-profile').html(`
                    <h1 class="lead">`+data.names+` (`+data.accountType+`)</h1>
                    <table class="table">
                        <tr>
                            <td>Name</td>
                            <td>`+data.names+`</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>`+data.gender+`</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>`+data.email+`</td>
                        </tr>
                        <tr>
                            <td>Mobile No.</td>
                            <td>`+data.phone+`</td>
                        </tr>
                    </table>
                    <br /><br />
                    <h1 class="lead">Other Details</h1>
                    <table class="table">
                        <tr>
                            <td>Language</td>
                            <td>`+data.language+`</td>
                        </tr>

                        <tr>
                            <td>State</td>
                            <td>`+data.state+`</td>
                        </tr>
                        <tr>
                            <td>Local Government Area</td>
                            <td>`+data.lga+`</td>
                        </tr>
                        <tr>
                            <td>Cluster</td>
                            <td>`+data.ward+`</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>`+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Office No</td>
                            <td>`+data.phone+`</td>
                        </tr>
                    </table>
                `);
            }

            if(data.accountType == 'service'){
                $(".load-farm-data").html(`
                    <h1 class="lead"><i class="fa fa-edit"></i> Farm Record </h1>
                    <table class="table">
                        <tr>
                            <td>Storage Capacity</td>
                            <td>`+data.storageCapicity+` `+data.typeOfStroage+`</td>
                        </tr>
                        <tr>
                            <td>Type of produce</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Road Map</td>
                            <td>`+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Distance</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Discription & Remark</td>
                            <td></td>
                        </tr>
                    </table>
                `);

                $('.load-profile').html(`
                    <h1 class="lead">`+data.names+` (`+data.accountType+`)</h1>
                    <table class="table">
                        <tr>
                            <td>Name</td>
                            <td>`+data.names+`</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>`+data.gender+`</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>`+data.email+`</td>
                        </tr>
                        <tr>
                            <td>Mobile No.</td>
                            <td>`+data.phone+`</td>
                        </tr>
                    </table>
                    <br /><br />
                    <h1 class="lead">Other Details</h1>
                    <table class="table">
                        <tr>
                            <td>Language</td>
                            <td>`+data.language+`</td>
                        </tr>

                        <tr>
                            <td>State</td>
                            <td>`+data.state+`</td>
                        </tr>
                        <tr>
                            <td>Local Government Area</td>
                            <td>`+data.lga+`</td>
                        </tr>
                        <tr>
                            <td>Cluster</td>
                            <td>`+data.ward+`</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>`+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Office No</td>
                            <td>`+data.phone+`</td>
                        </tr>
                    </table>
                `);
            }

            // buyers section
            if(data.accountType == 'buyer'){
                $(".load-farm-data").html(`
                    <h1 class="lead"><i class="fa fa-edit"></i> Farm Record </h1>
                    <table class="table">
                        <tr>
                            <td>Company Name</td>
                            <td>`+data.companyName+`</td>
                        </tr>
                        <tr>
                            <td>Preferred Produces</td>
                            <td>`+data.comPerferPurchase+`</td>
                        </tr>
                        <tr>
                            <td>Road Map</td>
                            <td>`+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Distance</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Discription & Remark</td>
                            <td></td>
                        </tr>
                    </table>
                `);
                $('.load-profile').html(`
                    <h1 class="lead">`+data.names+` (`+data.accountType+`)</h1>
                    <table class="table">
                        <tr>
                            <td>Name</td>
                            <td>`+data.names+`</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>`+data.email+`</td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>`+data.gender+`</td>
                        </tr>
                        <tr>
                            <td>Mobile No.</td>
                            <td>`+data.phone+`</td>
                        </tr>
                    </table>
                    <br /><br />
                    <h1 class="lead">Other Details</h1>
                    <table class="table">
                        <tr>
                            <td>Language</td>
                            <td>`+data.language+`</td>
                        </tr>

                        <tr>
                            <td>State</td>
                            <td>`+data.state+`</td>
                        </tr>
                        <tr>
                            <td>Local Government Area</td>
                            <td>`+data.lga+`</td>
                        </tr>
                        <tr>
                            <td>Cluster</td>
                            <td>`+data.ward+`</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>`+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Office No</td>
                            <td>`+data.phone+`</td>
                        </tr>
                    </table>
                `);   
            }

            if(data.avatar !== null){

                if (data.avatar.indexOf('data:') > -1){
                    $('.profile-img').html(`
                        <img height="auto" width="170" src="`+data.avatar+`" alt="placeholder+image">
                    `);
                }else if (data.avatar.indexOf('http://') > -1){
                    $('.profile-img').html(`
                        <img height="auto" width="170" src="`+data.avatar+`" alt="placeholder+image">
                    `);
                }else{
                    $('.profile-img').html(`
                        <img height="auto" width="170" src="/images/captured-images/`+data.avatar+`" alt="placeholder+image">
                    `);
                }
            }else{
                $('.profile-img').html(`
                    <img height="auto" width="170" src="/images/profile-image.png" alt="placeholder+image">
                `);
            }
               
            searchFarmByAddress(data.address, data.state);         
        });

        function searchFarmByAddress(search_address, search_state) {
            $('.loading-a').show();
            let address = search_address+' '+search_state;
            let country = 'Nigeria'; // default radius
            let token   = '{{ csrf_token() }}';

            // get data
            let data = {
                _token:token,
                address:address,
                country:country
            }

            // post data 
            $.post('/admin/map/farmers/address', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                var val = data.response;

                // console.log(res);
                // console.log(data.response);
                if(data.status == 'success'){
                    $('.capture-results').html(`
                        <table class="table">
                            <tr>
                                <td>Acurracy</td>
                                <td>`+val.accuracy+`</td>
                            </tr>

                            <tr>
                                <td>Address</td>
                                <td>`+val.formatted_address+`</td>
                            </tr>

                            <tr>
                                <td>Longitude</td>
                                <td>`+val.lng+`</td>
                            </tr>

                            <tr>
                                <td>Latitude</td>
                                <td>`+val.lat+`</td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    `);
                    $('.loading-a').hide();
                }

                $("#longitude").html(`Longitude: `+val.lng);
                $("#latitude").html(`Latitude: `+val.lat);

                // search area geolocation by co-ordinate
                searchFarmByCodes(val.lng, val.lat);
            });
        }

        // get codes
        function searchFarmByCodes(long, lat) {
            $('.loading-b').show();
            $('.loading-c').show();
            let longitude   = long;
            let latitude    = lat;
            let token       = '{{ csrf_token() }}';

            // get data
            let data = {
                _token:token,
                longitude:longitude,
                latitude:latitude
            }

            // post data 
            $.post('/admin/map/farmers/latlong', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                var val = data.response;

                // console.log(res);
                // console.log(data.response);
                if(data.status == 'success'){
                    $('.capture-results-2').html(`
                        <table class="table">
                            <tr>
                                <td>Acurracy</td>
                                <td>`+val.accuracy+`</td>
                            </tr>

                            <tr>
                                <td>Address</td>
                                <td>`+val.formatted_address+`</td>
                            </tr>

                            <tr>
                                <td>Longitude</td>
                                <td>`+val.lng+`</td>
                            </tr>

                            <tr>
                                <td>Latitude</td>
                                <td>`+val.lat+`</td>
                            </tr>
                        </table>
                    `);
                    $('.loading-b').hide();
                    // Mark image
                    fetchMapImage(val.lat, val.lng);
                }
            });

            // void form load
            return false;
        }

        // fetch map images 
        function fetchMapImage(lat, lng) {
            // body...
            var imageUrl = `https://maps.googleapis.com/maps/api/staticmap?center=`+lat+`,`+lng+`&zoom=15&size=400x400&key=AIzaSyDeD3MPYcPiDFrQxLWLYkHVy-bV02LsyiA`;

            var imageUrlSat = `https://maps.googleapis.com/maps/api/staticmap?center=`+lat+`,`+lng+`&zoom=17&size=400x400&maptype=satellite&key=AIzaSyDeD3MPYcPiDFrQxLWLYkHVy-bV02LsyiA`;

            $('.loading-c').hide();
            $("#map-a").html(`
                <img src="`+imageUrl+`" class="img img-responsive">
            `);

            $("#map-b").html(`
                <img src="`+imageUrlSat+`" class="img img-responsive">
            `);
        }

        // show edit modal
        function showEditModal() {
            $.get('/load-temp/{{$accountType}}/{{$id}}', function(data) {
                // console.log(data);
                var names = data.names.split(' ');
                $("#edit_firstname").val(names[0]);
                $("#edit_lastname").val(names[1]);
                $("#edit_email").val(data.email);
                $("#edit_mobile").val(data.phone);
                $("#edit_state").val(data.state);
                $("#edit_lga").val(data.lga);
                $("#edit_language").val(data.language);
                $("#edit_address").val(data.address);
                $("#edit_state").val(data.state);
                $("#edit_lga").val(data.lga);
            });

            $("#show-edit-modal-odca").modal();
        }

        // update profile
        $("#update-btn").click(function(e){
            e.preventDefault();
            $("#update-btn").html("Updating...");
            var temp_id     = '{{ $id }}';
            var token       = $("#token").val();
            var firstname   = $("#edit_firstname").val();
            var lastname    = $("#edit_lastname").val();
            var email       = $("#edit_email").val();
            var phone       = $("#edit_mobile").val();
            var address     = $("#edit_address").val();
            var accountType = '{{ $accountType }}';

            var language    = $("#edit_language").val();
            var state       = $("#edit_state").val();
            var lga         = $("#edit_lga").val();

            // data to json
            var params = {
                _token:token,
                firstname:firstname,
                lastname:lastname,
                email:email,
                phone:phone,
                address:address,
                temp_id: temp_id,
                language:language,
                state:state,
                lga:lga,
                accountType: accountType
            };

            // save data 
            $.post('{{ url("admin/update/temp/user") }}', params, function(data, textStatus, xhr) {
                if(data.status == 'success'){
                    window.location.reload();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }
                $("#update-btn").html("Update");
            });


            // void form
            return false; 
        });
    </script>
@endsection