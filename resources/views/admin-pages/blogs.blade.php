@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | Blog Update
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">

    	<!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">New blog post</span>
          </div>
        </div>
        <!-- End Page Header -->

    	<!-- Default Light Table -->
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0">Farm products</h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
			    	<div class="col-md-12">
			        	<h1 class="lead">Upload Blog news</h1><hr />
				        <form class="post-form" method="post" onsubmit="return uploadPost()">
				        	<input type="hidden" id="blog_image">
				        	<div class="form-group">
				        		<label>Title</label>
				        		<input type="text" class="form-control" maxlength="30" id="title" placeholder="Blog Title" required="">
				        	</div>
				        	<div class="form-group">
				        		<label>Body</label>
				        		<textarea class="form-control" id="body" cols="10" rows="10" placeholder="Write a blog post..." required=""></textarea>
				        	</div>
				        	<div class="form-group">
				        		<a href="javascript:void(0);" id="upload_widget_opener" class="btn btn-default">Attached Images</a>
				        	</div>
				        	<div class="form-group">
				        		<label>Category</label>
				        		<select id="category" class="form-control">
				        			<option value="">--none--</option>
				        			<option value="agricultural">Agricultural</option>
				        			<option value="produce">Produce</option>
				        			<option value="poultry">Poultry</option>
				        			<option value="livestocks">Livestocks</option>
				        			<option value="research">Research</option>
				        			<option value="meet-up">Farmers Meet up</option>
				        		</select>
				        	</div>
				        	<div class="form-group">
				        		<button class="btn btn-primary col-md-12">Publish Post 
				        			<img src="/svg/three-dots.svg" width="20px" height="20px" style="display: none;" class="loading pull-right">
				        		</button>
				        	</div>
				        	<div class="preview-image"></div>
				        	<div class="success_msg"></div>
				        </form>
			    	</div>
			    	<div class="col-md-12 mt-4">
			    		<div class="last_uploaded_news">
			    			<img src="/svg/yeelda-loading.svg" width="auto" height="90px" >
			    		</div>
			    		<hr />
			    		<div class="load-messages"></div>
			    	</div>
			    </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->	
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>  
  	<script type="text/javascript">
  		$("#body").summernote({
  			toolbar: [
			    // [groupName, [list of button]]
			    ['style', ['bold', 'italic', 'underline', 'clear']],
			    ['font', ['strikethrough', 'superscript', 'subscript']],
			    ['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['height', ['height']]
			],
			shortcuts: false
  		});
    	document.getElementById("upload_widget_opener").addEventListener("click", function() {
      		cloudinary.openUploadWidget({ 
	        	cloud_name: 'delino12', 
	        	upload_preset: 'znwx0uee',
	        	cropping: true, 
	        	folder: 'yeelda-blog'
	      	}, 
	      	function(error, result) { 
	        	if(error){
	          		swal(
	            		"oops",
	            		"Error trying to upload image, select image and try again",
	            		"error"
	          		);
	        	}else{
	          		$("#blog_image").val(result[0].url);
	          		$(".preview-image").html(`
	          			<img src="${result[0].url}" width="120" height="auto" />
	          		`);
		        }
		    });
	    }, false);

		// send messages
		function uploadPost(){
			// get form data 
			var title = $("#title").val();
			var body  = $("#body").val();
			var token = '{{ csrf_token() }}';
			var image = $("#blog_image").val();
			var category = $("#category").val();

			// data to json
			var data = {
				_token:token,
				title:title,
				body:body,
				image:image,
				category: category
			};

			// ajax send post
			$.ajax({
				url: '/save/post/blogs/',
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data){
					console.log(data);
					if(data.status == 'success'){
						$('.success_msg').html(`
							<p class="text-success">
								`+data.message+`
							</p>
						`);
					}
					// refresh div
					refreshPreview();
					$('.post-form')[0].reset();
				},
				error: function (data){
					console.log(data);
				}
				// reset post form
			});

    		return false;
    	}

    	// get last uploaded blog news 
    	$(document).ready(function (){
    		refreshPreview();
    	});

    	// refresh preview
    	function refreshPreview(){
    		// get last uploaded blog news 
	    	$.get('/blog/last/uploaded', function(data) {
	    		/*optional stuff to do after success */
	    		// console.log(data);
	    		if(data.title){
	    			$('.last_uploaded_news').html(`
					    <h1 class="mt-4">`+data.title+`</h1>
					    	<!-- Author -->
					      	<p class="lead">
					       		by <a href="#">`+data.by+`</a>
					      	</p>
					      	<hr>
					      	
					      	<!-- Date/Time -->
					      	<p>Posted on `+data.created_at+`</p>
					      	<hr>
					      	<!-- Preview Image -->
					      	<img class="img-fluid rounded" src="`+data.docs+`" width="70%" height="70%" alt="">
					      	<hr>
					      	<!-- Post Content -->
					      	<p class="small">`+data.body+`</p>
					      	<a href="javascript:void(0);" onclick="deleteBlogPost(${data.id})"> <i class="fa fa-trash"></i> delete</a>
					      	<hr>
		    			</div>
		    		`);
	    		}else{
	    			$('.last_uploaded_news').html(`
					    No Blog post found !
		    		`);
	    		}
	    	});
    	}

    	// delete blog post
    	function deleteBlogPost(post_id) {
    		var token = $("#token").val();
    		var params = {
    			_token: token,
    			post_id: post_id
    		}

    		$.post('{{ url('admin/delete/blog/post') }}', params, function(data, textStatus, xhr) {
    			if(data.status == "success"){
    				refreshPreview();
    			}
    		});
    	}
	</script>
@endsection