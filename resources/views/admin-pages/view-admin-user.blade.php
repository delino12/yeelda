@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Admin
@endsection

{{-- Contents  --}}
@section('contents')
    <style type="text/css">
        .y-thumbnail {
            height: 152px;
            border-radius: 4px;
            border:1px solid #FFF;
            width: 170px;
        }
    </style>

    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">{{ Auth::user()->email }}</span>
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                </h6>
              </div>
              <div class="card-body p-5 pb-3">
                <div class="row">
                    <!-- edit form column -->
                    <div class="col-md-6">
                        <div class="success_msg"></div>
                        <div class="error_msg"></div>
                        <h3></h3>

                        <h1 class="lead">Profile Avatar</h1>
                        <hr>
                        
                        <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                        <h6 class="small">Upload a different photo...</h6>
                        <input type="file" class="form-control">

                        <br />
                        <p class="lead">Update info</p>
                        <form role="form" onsubmit="return updateUserInformation()">
                            <div class="form-group">
                                <label>Names</label>
                                <input type="text" class="form-control" id="names">
                            </div>
                            <div class="form-group">
                                <label>Email (Note: all email ends at @yeelda.com or @yeelda.ng)</label>
                                <input type="text" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                <label>Admin Level</label>
                                <select class="form-control" id="level">
                                    <option value="none">--none--</option>
                                    <option value="alpha">Alpha</option>
                                    <option value="beta">Beta</option>
                                    <option value="omega">Omega</option>
                                    <option value="support">Customer Support</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Update & Save Information</button>
                            </div>
                        </form>

                        <form role="form">
                            <br />
                            <br />
                            <div class="form-group">
                                <label>Reset Password</label>
                                <input type="text" class="form-control" id="passwd1" placeholder="**********">
                            </div>
                            <div class="form-group">
                                <label>Confirm Reset Password</label>
                                <input type="text" class="form-control" id="passwd2" placeholder="**********">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Update Password</button>
                            </div>
                        </form>
                    </div>

                    <!-- preview updates -->
                    <div class="col-md-6">
                        <p class="lead">Profile Information</p>
                        <div class="load-information">
                            <img src="/svg/yeelda-loading.svg" height="60" width="auto" />
                        </div>
                    </div>
                </div>

                @if(Auth::user()->level == 'Alpha')
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-danger float-right" onclick="removeUserInformation()">
                                Delete Account
                            </button>
                        </div>
                    </div>
                @endif
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        // get view user id
        // load user information
        function loadUserInformation() {
            // body...
            $.get('/admin/load/admin/'+{{ $user_id }}, function(data) {
                // console.log(data);
                $(".load-information").html("");
                $(".load-information").append(`
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name: </label><br />
                            `+data.names+`
                        </div>

                        <div class="col-md-6">
                            <label>Email: </label><br />
                            `+data.email+`
                        </div>
                    </div>

                    <br /><br />
                    <table class="table">
                        <tr>
                            <td>Status</td>
                            <td><a class="text-success" href="">Active</a></td>
                        </tr>
                        <tr>
                            <td>Access Level</td>
                            <td>`+data.level.toUpperCase()+`</td>
                        </tr>
                        <tr>
                            <td>Permissions</td>
                            <td></td>
                        </tr>
                    </table>
                `);

                // assign default fields
                $("#names").val(data.names);
                $("#email").val(data.email);
                $("#level").val(data.level);
            });
        }

        // update user information
        function updateUserInformation() {
            // body...
            var names = $("#names").val();
            var email = $("#email").val();
            var level = $("#level").val();

            var data = {
                _token: '{{ csrf_token() }}',
                userid: '{{ $user_id }}',
                names: names,
                email: email,
                level: level
            };

            $.post('/admin/update/admin-info', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == 'success'){
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                    // refresh divs 
                    loadUserInformation();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }
            });

            // void form
            return false;
        }

        // delete user information
        function removeUserInformation() {
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                // body...
                var token = '{{ csrf_token() }}';
                var admin_id = '{{ $user_id }}';

                var params = {
                    _token: token,
                    admin_id: admin_id
                };

                $.post('{{url('admin/delete/admin/user')}}', params, function(data, textStatus, xhr) {
                    if(data.status == "success"){
                        swal(
                            "Ok",
                            data.message,
                            data.status
                        );
                        window.location.href = '{{ url('admin/settings') }}';
                    }else{
                        swal(
                            "oops",
                            data.message,
                            data.status
                        );
                    }
                });
              }
            });
        }

        // document on ready
        loadUserInformation();
    </script>
@endsection