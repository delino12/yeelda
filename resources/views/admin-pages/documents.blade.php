@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda Cpanel | Documents
@endsection

{{-- Contents  --}}
@section('contents')
    <div style="height: 80px;"></div>
    <div class="container text-left">
    	<div class="row">
    		<div class="col-md-6">
        		<h1 class="lead">Documents & Verifications</h1><hr />
    			<div class="form-group">
    				<label>Descriptions</label>
    				<textarea class="form-control" cols="3" rows="4" placeholder="Tell us more...."></textarea>
    			</div>
    			<div class="form-group">
    				<div class="dropzone" style="background-color: rgba(000,000,000,0.50);border-radius: 4px;"></div>
    			</div>
    			<div class="form-group">
    				<button class="btn btn-primary">Upload Document</button>
    			</div>
    		</div>
    	</div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript" src="/dropzone/js/dropzone.js"></script>
	<script type="text/javascript">
		$(".dropzone").dropzone({ 
			url: "/admin/upload/documents",
			params: {
				_token:'{{ csrf_token() }}'
			},
			success: function(e, res) {
				// body...
				console.log(e.status);
				console.log(res);
			}
		});
	</script>
@endsection