@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | Buyers
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')

    <div class="main-content-container container-fluid px-4">
      <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">
                <a href="{{ url('admin/dashboard') }}"> <i class="fa fa-angle-double-left"></i> Back</a>
            </span>
          </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">
                            <img src="/images/icon-set/produce.png"> Buyers
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div class="loading-bar" style="display: none;">
                            <div class="load-bar">
                              <div class="bar"></div>
                              <div class="bar"></div>
                              <div class="bar"></div>
                            </div>
                        </div>
                        <table id="investor" class="table" data-pagination="true" data-search="true">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th><i class="fa fa-user"></i> Name</th>
                                    <th><i class="fa fa-envelope"></i> Email</th>
                                    <th><i class="fa fa-phone"></i> Office</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                    <th>More</th>
                                </tr>
                            </thead>
                            <tbody class="list-all-investor">
                                <tr>
                                    <td>Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Default Light Table -->
        </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        // load modules
        fetchUsersBuyers();

        // fetch users on yeelda (buyers)
        function fetchUsersBuyers() {
            $(".loading-bar").show();
            // body...
            var params = {account_type: "buyer"};
            $.get('/load/users/yeelda', params, function(data) {
                $(".loading-bar").hide();
                /*optional stuff to do after success */
                $(".list-all-investor").html("");
                var sn = 0;
                $.each(data.Investor, function(index, val) {
                    /* iterate through array or object */
                    sn++;

                    if(val.lock == 'locked'){
                        var status = `<a href="javascript:void(0);" onclick="unblockUser('`+val.email+`')">unblock</a>`;
                    }else if(val.lock == null){
                        
                        var status = `<a href="javascript:void(0);" onclick="blockUser('`+val.email+`')">block</a>`;
                    }

                    $(".list-all-investor").append(`
                        <tr>
                            <td>`+sn+`</td>
                            <td>`+val.name+`</td>
                            <td>`+val.email+`</td>
                            <td>`+val.phone+`</td>
                            <td>`+val.date+`</td>
                            <td>
                                <a href="/admin/users/profile/`+val.id+`">view </a> -
                                `+status+`
                            </td>
                            <td>
                                <a href="javascript:void(0);" onclick="deleteUser('${val.email}')">Delete </a>
                            </td>
                        </tr>
                    `);    
                });

                $('#investor').DataTable({
                    'paging'      : true,
                    'lengthChange': false,
                    'searching'   : true,
                    'ordering'    : false,
                    'info'        : true,
                    'autoWidth'   : false,
                    'dom'         : 'Bfrtip',
                    'buttons'     : [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            });
        }

        // refresh divs
        function refreshDivs(){
            fetchUsersBuyers();
        }

        // block user
        function blockUser(email) {
            // get data
            let token = '{{ csrf_token() }}';

            // data to object
            let data = {
                _token:token,
                email:email
            }

            // post data
            $.post('/admin/deactivate/user', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == 'success'){
                    // body...
                    alert(data.message);
                    refreshDivs();
                }
            });
        }

        // unblock user
        function unblockUser(email) {
            // get data
            let token = '{{ csrf_token() }}';

            // data to object
            let data = {
                _token:token,
                email:email
            }

            // post data
            $.post('/admin/activate/user', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == 'success'){
                    // body...
                    alert(data.message);
                    refreshDivs();   
                }
            });
        }

        // delete user
        function deleteUser(email) {
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                // get data
                var token = '{{ csrf_token() }}';

                // data to object
                var data = {
                    _token:token,
                    email:email
                };

                // post data
                $.post('/admin/delete/active/user', data, function(data, textStatus, xhr) {
                    /*optional stuff to do after success */
                    if(data.status == 'success'){
                        swal(
                            "Deleted!",
                            data.message,
                            data.success
                        );
                        // loadAllUsersData();
                        window.location.reload();
                    }
                });
              }
            });
        }
    </script>
@endsection