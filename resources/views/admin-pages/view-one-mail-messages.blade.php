@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | Mail Messages
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle" >Read Message</span>
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 id="message-title"></h6>
              </div>
              <div class="card-body p-5 pb-3">
                <div id="message-section"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- custom js --}}
    <script type="text/javascript">
        $(document).ready(function(){
            loadAllMailLogs();
        });

        // load Mail logs
        function loadAllMailLogs() {
            $.get('/admin/load/sent/mail/{{$id}}', function(data) {
                // console.log(data);
                // console.log(data);
                $("#message-section").html(`
                    <h1 class="lead">Subject: ${data.subject}</h1>
                    <p>${data.body}</p>
                `);

                $("#message-title").html(data.subject);
            });
        }

    </script>
@endsection