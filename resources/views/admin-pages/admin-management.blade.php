@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | Management
@endsection

{{-- Contents  --}}
@section('contents')
  <style>
    #chartdiv {
      width: 100%;
      height: 290px;
    }

    tspan {
        color:#FFF;
    }                    
  </style>

  @include('components.sticky-menu')
  
  <!-- / .main-navbar -->
  <div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Managements</span>
      </div>
    </div>
    <!-- End Page Header -->
    @include("admin-components.logistics")
    @include("admin-components.statistics")
  </div>
@endsection

{{-- Scripts --}}
@section('scripts')
  {{-- custom js --}}
  <script type="text/javascript">
    // on document ready
    // $(document).ready(function (){
    loadSmsInfo();
    loadUsersStatistics();
    loadAllAgents();
    loadUserByLocation();
    getTrackedProduct();

    // $("#add_logistic").modal("show");
    $("#body").summernote();
    // });

    // load sms info
    function loadSmsInfo(argument) {
      $.get('/admin/sms-contact/status', function(data) {
        $(".load-sms-log").html(`
            <tr>
                <td>Farmer </td>
                <td>`+data.farmers+`</td>
            </tr>
            <tr>
                <td>Services Provider </td>
                <td>`+data.services+`</td>
            </tr>
            <tr>
                <td>Buyers </td>
                <td>`+data.buyers+`</td>
            </tr>
            <tr>
                <td> Total Contact Found ! </td>
                <td>`+data.valid_no+`</td>
            </tr>
        `);

        $(".total_sent_sms").html(`
            Total SMS sent: `+data.total+`
        `);
      });
    }

    // set recepient sms
    function setRecievers() {
      // SMS system
      var type = $('#sms_type').val();

      if(type == 'none'){
          alert("select a recipient Type ");
          return false;
      }

      if(type == 'single'){
          // body...

          $('#single').show();
          $('#multiple').hide();
      } 

      if(type == 'multiple'){
          $('#single').hide();
          $('#multiple').show();
      }

      if(type == 'all'){
          $('#single').hide();
          $('#multiple').hide();
      }
    }

    // set recepient for mail
    function setRecieversMail() {
      // SMS system
      var type = $('#type').val();

      if(type == 'single'){
        // body...
        $('#single').show();
        $('#mail-multiple').hide();
        $('#multiple').hide();
      } 

      if(type == 'multiple'){
        $('#single').hide();
        $('#mail-multiple').show();
      }

      if(type == 'all'){
        $('#single').hide();
        $('#mail-multiple').hide();
        $('#multiple').hide();
      }
    }

    // verify recepient
    function checkReceiver() {
      // body...
      let type = $("#type").val();
      if(type == 'single'){
        $("#mail-single").show();
        $('#mail-multiple').hide();
      }

      if(type == 'multiple'){
        $('#mail-single').hide();
        $('#mail-multiple').show();
      }
    }

    // calculate characters limit
    function caculateCharacters() {
      // body...
      var body = $('#sms_message').val();

      // characters 
      var pageLimit = 160;
      var curentLength = body.length;
      var pageOne = pageLimit - curentLength;

      // get page section
      var pageCount = 1;

      $(`.count-character`).html(`
          `+pageOne+`/`+pageCount+`
      `);
    }

    // send sms
    function sendSMS() {
      $('#sms_loading').show();

      // body...
      var token    = '{{ csrf_token() }}';
      var type     = $("#sms_type").val();
      var single   = $("#single-receiver").val();
      var multiple = $("#multiple-receiver").val();
      var body     = $("#sms_message").val();

      if(type == 'none'){
          alert("select a recipient Type ");
          $('#sms_loading').hide();
          return false;
      }

      // data to object
      var data = {
          _token: token,
          single: single,
          multiple: multiple,
          type: type,
          body: body
      };

      $.ajax({
        url: '/send/sms/message',
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          $('#sms_loading').hide();

          // console.log(data);
          if(data.status == 'success'){
            swal(
              "Sent!",
              data.message,
              data.status
            );
          }

          // 
          if(data.status == 'error'){
            swal(
              "Oops!",
              data.message,
              data.status
            );
          }

          // reset form
          $('.sms-message-form')[0].reset();
        },
        error: function (data){
          // console.log(data);
          $('#sms_loading').hide(); 
          swal(
            "Oops!",
            data.message,
            data.status
          );
        }
      });
        
      return false;
    }

    // send mail messages
    function sendMessages(){
      $("#loading").show();
      // get form data
      let token     = '{{ csrf_token() }}';
      let recipient = $("#type").val();
      let subject   = $("#subject").val();
      let body      = $("#body").val();
      let single    = $("#single-mail").val();
      let multiple  = $("#multiple-emails").val();

      // data to json
      let data = {
        _token: token,
        recipient:recipient,
        single:single,
        multiple:multiple,
        subject:subject,
        body:body
      };

      // json to ajax
      $.ajax({
        url: '/send/announce/message',
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          if(data.status == 'success'){
            swal(
              "Sent!",
              data.message,
              data.status
            );
          }

          if(data.status == 'error'){
            swal(
              "Oops!",
              data.message,
              data.status
            );
          }
          $("#loading").hide();
        },
        error: function (data){
          console.log(data);
          $("#loading").hide();
          swal(
            "Oops!",
            "Failed to sent request, check internet connectivity...",
            "error"
          );
        }
      });
      

      return false;
    }

    // send stamp messages
    function sendStampMessage() {
        // body...
        $("#sta-loading").show();
        // get form data
        let token       = '{{ csrf_token() }}';
        let recipient   = $("#stamp-type").val();
        let body        = $("#stamp-message").val();

        // data to json
        let data = {
            _token: token,
            type:recipient,
            body:body
        };

        // json to ajax
        $.ajax({
            url: '/send/stamp/message',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data){
                // console.log(data);
                if(data.status == 'success'){
                    $(".stamp_success_msg").html(`
                        <p class="text-success">
                            `+data.message+`
                        </p>
                    `);
                }

                if(data.status == 'error'){
                    $(".stamp_error_msg").html(`
                        <p class="text-danger">
                            `+data.message+`
                        </p>
                    `);
                }
                $("#sta-loading").hide();
            },
            error: function (data){
                console.log(data);
                $("#sta-loading").hide();
                alert('Error, fail to send request !');
            }
        });
        

        return false;
    }

    // load users statistic
    function loadUsersStatistics() {
        $.get('{{url('/load/charts/statistic')}}', function(data) {
            // console log data
            $(".users-statistic").html(`
                <tr>
                    <td>Total Farmers</td>
                    <td class="text-success">${data.total_farmers}</td>
                    <td class="text-info">${data.temp_farmers}</td>
                </tr>

                <tr>
                    <td>Total Services Providers</td>
                    <td class="text-success">${data.total_services}</td>
                    <td class="text-info">${data.temp_services}</td>
                </tr>

                <tr>
                    <td>Total Buyers</td>
                    <td class="text-success">${data.total_buyers}</td>
                    <td class="text-info">${data.temp_buyers}</td>
                </tr>

                <tr>
                    <td>Total Users</td>
                    <td class="text-success">${data.total_overall}</td>
                    <td class="text-info">${data.temp_overall}</td>
                </tr>
            `);
        });
    }

    // load all agents
    function loadAllAgents() {
      $.get('/admin/load/agents', function(data) {
        $(".load-all-agents").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $('.load-all-agents').append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.names+`</td>
              <td>`+val.agent_id+`</td>
              <td><a href="/admin/agent/`+val.id+`/view">view</a></td>
            </tr>
          `);
        });
        $('#load-agent-table').bootstrapTable();
      });
    }

    // load user by regions
    function loadUserByLocation() {
      $.get('/admin/load/users-by-location', function(data) {
        $('.farmers-by-regions').html("");
        $('.services-by-regions').html("");
        $('.buyers-by-regions').html("");

        // console log data
        console.log(data);

        $.each(data.farmers, function(index, val) {
          $('.farmers-by-regions').append(`
            <tr>
              <td>${val.state}</td>
              <td>${val.total}</td>
            </tr>
          `);
        });

        $.each(data.services, function(index, val) {
          $('.services-by-regions').append(`
            <tr>
              <td>${val.state}</td>
              <td>${val.total}</td>
            </tr>
          `);
        });

        $.each(data.buyers, function(index, val) {
          $('.buyers-by-regions').append(`
            <tr>
              <td>${val.state}</td>
              <td>${val.total}</td>
            </tr>
          `);
        });
      });
    }

    // show add logistic modal
    function showAddLogistic() {
      $("#add_logistic").modal("show");
    }

    function getTransactionInfo() {
      var reference = $("#product_ref").val();
      var params = {reference: reference}

      $.get('{{url('admin/load/trans_ref')}}', params, function(data) {
        // console log data
        if(data.status == "error"){
          swal(
            "oops",
            data.message,
            data.status
          );
        }else{
          $("#status").val(data.data.status);
          $("#seller").val(data.data.seller_name);
          $("#buyer").val(data.data.buyer_name);
        }
      });
    }

    // add tracking to product
    $("#add_product_btn").click(function(c){
      c.preventDefault();
      $("#add_product_btn").html("Generating....");

      var token               = $("#token").val();
      var product_ref         = $("#product_ref").val();
      var carrier_name        = $("#carrier_name").val();
      var assignee            = $("#assignee").val();
      var status              = $("#status").val();
      var seller              = $("#seller").val();
      var buyer               = $("#buyer").val();
      var start_date          = $("#start_date").val();
      var deliver_date        = $("#deliver_date").val();
      var pickup_address      = $("#pickup_address").val();
      var destination_address = $("#destination_address").val();

      var params = {
        _token: token,
        product_ref: product_ref,
        carrier_name: carrier_name,
        assignee: assignee,
        status: status,
        seller: seller,
        buyer: buyer,
        start_date: start_date,
        deliver_date: deliver_date,
        pickup_address: pickup_address,
        destination_address: destination_address
      };

      $.post('{{url('admin/create/tracking')}}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            data.status,
            data.message,
            data.status
          );

          $("#add_logistic").modal("hide");
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }

        $("#add_product_btn").html("Create Receipt");
      });
    });

    // get tracked products
    function getTrackedProduct() {
      $.get('{{url('admin/load/tracking')}}', function(data) {
        $(".load-tracking-products").html("");
        $.each(data, function(index, val) {
          $(".load-tracking-products").append(`
            <tr>
              <td>${val.carrier}</td>
              <td>${val.assignee}</td>
              <td>${val.location}</td>
              <td>${val.destination}</td>
              <td>${val.tracking_ref}</td>
              <td>${val.status}</td>
              <td><a href="tracking/${val.id}">view</a></td>
            </tr>
          `);
        });
      });
    }
  </script>
@endsection