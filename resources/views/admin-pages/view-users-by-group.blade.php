@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Farmers Group
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">
                {{ Auth::guard('admin')->user()->email }}
            </span>            
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                    <div class="row float-right">
                        <div class="col-sm-4">
                            <a href="javascript:void(0);" class="btn btn-info">
                                <i class="fa fa-users"></i> {{ ucfirst($group->name) }}
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="javascript:void(0);" class="btn btn-info" onclick="searchReplaceModal()">
                                <i class="fa fa-search"></i> Search & Replace
                            </a>
                        </div>
                    </div>   
                </h6>
              </div>
              <div class="card-body p-4 pb-2" style="overflow-x: scroll;">
                <div class="loading-bar" style="display: none;">
                    <div class="load-bar">
                      <div class="bar"></div>
                      <div class="bar"></div>
                      <div class="bar"></div>
                    </div>
                </div>
                <table class="table display nowrap" id="group_captured_users">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Names</th>
                            <th>Phone</th>
                            <th>State</th>
                            <th>LGA.</th>
                            <th>Address</th>
                            <th>Cluster</th>
                            <th>Size</th>
                            <th>Type</th>
                            <th>Produce</th>
                            <th>Previous Havested</th>
                            <th>Amount</th>
                            <th>Group</th>
                        </tr>
                    </thead>
                    <tbody class="load-all-groups"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->

        @include('admin-components.modals')
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        var group_name = '{{ $group->name }}';
        var group_id   = '{{ $group->id }}';

        // get view user id
        loadAllUsersInGroup();

        // fetch groups data
        function loadAllUsersInGroup() {
            $(".loading-bar").show();
            var params = {
                group_id: group_id
            };

            $.get('{{ url("admin/all/groups/users") }}', params, function(data) {
                /*optional stuff to do after success */
                $(".loading-bar").hide();
                $(".load-all-groups").html("");
                var sn = 0;

                $.each(data, function(index, val) {
                    // console.log('val', val);
                    sn++;
                    $(".load-all-groups").append(`
                        <tr>
                            <td>${sn}</td>
                            <td>${val.name}</td>
                            <td>${val.phone}</td>
                            <td>${val.state}</td>
                            <td>${val.lga}.</td>
                            <td>${val.address}.</td>
                            <td>${val.cluster}.</td>
                            <td>${val.farm_size}</td>
                            <td>${val.farm_type}</td>
                            <td>${val.farm_crop}</td>
                            <td>${val.prevYieldHarvested}</td>
                            <td>${val.prevCropHarvested}</td>
                            <td>{{ $group->name }}</td>
                        </tr>
                    `);
                });

                $('#group_captured_users').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : false,
                'info'        : true,
                'autoWidth'   : false,
                'scrollX'     : true,
                'dom'         : 'Bfrtip',
                'buttons'     : [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
              });
            });
        }

        function searchReplaceModal() {
            $("#show-search-replace-modal").modal();
        }

        // start find and replace
        function startFindReplace() {
            var token = $("#token").val();
            var find  = $("#l-find").val();
            var replace = $("#l-replace").val();

            var params = {
                _token: token,
                find: find,
                replace: replace
            }

            // console log
            // console.log(params);
            $.post('{{ url('api/search/replace/lga') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == "success"){
                    $("#fr-results").html(data.message);
                }
            });

            return false;
        }

        // remove form inline class
        $("#group_captured_users_wrapper").removeClass('form-inline')
    </script>
@endsection