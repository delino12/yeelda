@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | SMS messages
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">YEELDA SENT SMS MESSAGES</span>
          </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0"><i class="fa fa-location-arrow"></i> All sent messages!</h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="lead">All sent SMS</h1>
                        <table class="table" id="sms-message-table" data-pagination="true" data-search="true"data-page-size="10">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th width="50%">Messages</th>
                                    <th width="30%">Reciever's</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody class="load-sms-logs"></tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- custom js --}}
    <script type="text/javascript">
        $(document).ready(function(){
            loadAllSmsLogs();
        });

        // load SMS info
        function loadAllSmsLogs() {
            $.get('/admin/load/sent/sms', function(data) {
                // console.log(data);
                $(".load-sms-logs").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    $(".load-sms-logs").append(`
                        <tr>
                            <td>${sn}</td>
                            <td>${val.body}</td>
                            <td>${val.phone}</td>
                            <td>${val.status}</td>
                            <td>${val.date}</td>
                            <td><a href="javascript:void(0);">-----</a></td>
                        </tr>
                    `);
                });

                $('#sms-message-table').bootstrapTable();
            });
        }
    </script>
@endsection