@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | Payment
@endsection

{{-- Contents  --}}
@section('contents')
<style type="text/css">
    .y-btn {
        border:1px solid #FFF;
        border-radius: 4px;
        padding: 0.5em;
        margin: 0.5em;
        text-decoration: none;
    }
</style>
    <div style="height: 80px;"></div>
    <div class="container text-left">
        <h1 class="lead">YEELDA Payment | Incoming </h1><hr />
        <div class="row">
            <div class="col-md-12">
                <h3>Payment (Pending)</h3>
                <div class="pull-right">
                    @if(session('success_msg'))
                        <p class="text-success">{{ session('success_msg') }}</p>
                    @endif
                    @if(session('error_msg'))
                        <p class="text-danger">{{ session('error_msg') }}</p>
                    @endif
                </div>
                {{-- <span class="total-payment-pending pull-right"></span> --}}
                <table class="table small" id="incoming-payment" data-pagination="true" data-search="true" data-page-size="10">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Buyer Name</th>
                            <th>Buyer Email</th>
                            <th>Buyer Phone</th>
                            <th>Seller Email</th>
                            <th>Tran. Ref</th>
                            <th>Amount(&#8358;)</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="load-payment-list"></tbody>
                </table>
            </div>
        </div>
        <hr />
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <h3>Payment (Successful)</h3>
                <table class="table small" id="verified-payment" data-pagination="true" data-search="true"data-page-size="10">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Buyer Name</th>
                            <th>Buyer Email</th>
                            <th>Buyer Phone</th>
                            <th>Seller Email</th>
                            <th>Tran. Ref</th>
                            <th>Amount(&#8358;)</th>
                            <th>Status</th>
                            <th>Last updated</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="load-payment-paid"></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        $.get('/load/payment/request', function(data) {
            /*optional stuff to do after success */
            // console.log(data);
            $('.load-payment-list').html();
            var sn = 0;
            $.each(data, function(index, val) {
                /* iterate through array or object */
                sn++;
                if(val.status !== 'settle'){
                    $('.load-payment-list').append(`
                        <tr>
                            <td>`+sn+`</td>
                            <td>`+val.name+`</td>
                            <td>`+val.buyer+`</td>
                            <td>`+val.phone+`</td>
                            <td>`+val.seller+`</td>
                            <td>`+val.pay_id+`</td>
                            <td>&#8358;`+val.amount+`</td>
                            <td>`+val.status+`</td>
                            <td>`+val.date+`</td>
                            <td>
                                <a class="y-btn" href="/admin/settle/payment/`+val.id+`"><i class="fa fa-lock"></i> Release</a>
                                <a class="y-btn" href="/admin/mail/payment/`+val.id+`"><i class="fa fa-envelope"></i> Mail</a>
                            </td>
                        </tr>
                    `);
                }
            });

            $('#incoming-payment').bootstrapTable();
        });

        $.get('/load/payment/paid', function(data) {
            /*optional stuff to do after success */
            // console.log(data);
            $('.load-payment-paid').html();
            var sn = 0;
            $.each(data, function(index, val) {
                /* iterate through array or object */
                // console log data
                console.log(val);
                sn++;
                $('.load-payment-paid').append(`
                    <tr>
                        <td>`+sn+`</td>
                        <td>`+val.name+`</td>
                        <td>`+val.buyer+`</td>
                        <td>`+val.phone+`</td>
                        <td>`+val.seller+`</td>
                        <td>`+val.pay_id+`</td>
                        <td>&#8358;`+val.amount+`</td>
                        <td>success</td>
                        <td>`+val.date+`</td>
                        <td>
                            <a class="y-btn" href="/admin/mail/payment/`+val.id+`"><i class="fa fa-envelope"></i> Mail</a>
                        </td>
                    </tr>
                `);
            });

            $('#verified-payment').bootstrapTable();

            $('.total-payment-successful').html('Total: '+data.length);
        });
    </script>
@endsection