@extends('layouts.admin-pages-skin')

@section('title')
  YEELDA | Reports
@endsection

@section('contents')
    <style>
        #pie_chart_div {
          width: 100%;
          height: 570px;
        }
        #bar_chart_div {
          width: 100%;
          height: 290px;
        }

        #landarea_chart_div {
            width: 100%;
            height: 370px;
        }

        #singleChartDiv {
            width: 100%;
            height: 370px;
        }

        #land_map_area {
            width: 100%;
            height: 370px;
        }

        #clusters_chart_div {
            width: 100%;
            height: 600px;
        }

        #clusters_bar_chart_div {
            width: 100%;
            height: 400px;
        }

        #villages_bar_chart {
            width: 100%;
            height: 400px;
        }

        tspan {
            color:#FFF;
        }                    
    </style>

    @include('components.sticky-menu')
    
    <div class="main-content-container container-fluid px-4">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Reports</span>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Land Area Mapping
                            <div class="float-right">
                                <div class="row">
                                    <div class="col-md-3">
                                        <select onchange="getLgas()" id="sort_state" class="custom-select custom-select-sm" style="min-width: 130px;">
                                          <option value="">select state</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <select onchange="getClusters()" id="sort_lgas" class="custom-select custom-select-sm" style="min-width: 130px;">
                                            <option value="">select lgas</option>
                                        </select>
                                    </div>


                                    <div class="col-md-3">
                                        <select id="sort_clusters" onchange="getClusterVillages()" class="custom-select custom-select-sm" style="min-width: 130px;">
                                            <option value="">select clusters</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <button class="btn btn-default btn-sm" onclick="getAllReports()">
                                            Show Reports
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="land_map_area">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0"><p class="target-report-title">Farmers Captured</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="single_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0 target-report-title-cluster"><p>Clusters In Bar Chart</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="clusters_bar_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0 target-title-cluster"><p>Cluster</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="villages_bar_chart">Select a cluster...</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0"><p>Clusters In Donut Pie Chart (Draggable)</p>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="clusters_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/maps.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script type="text/javascript">
        // loadAllStats();
        getAllStates();
        initMapAreaForStates();
        getLgas();
        getClusters();
        getClusterVillages();
        // initBarChartForReport();

        function loadAllStats() {
            $.get('{{ url("admin/fetch/overall/statistics") }}', function(data) {
                $(".total_male").html(data.total_male);
                $(".total_female").html(data.total_female);
                $(".highest_planted").html(data.highest_planted);
                $(".most_preferred").html(data.most_preferred.crop);
                $(".least_preferred").html(data.least_preferred.crop);
                // initPieChart(data.series);
                initBarChart(data.series);
            });
        }

        function loadLandStats() {
            $.get('{{ url("admin/fetch/geo/landarea") }}', function(data) {
                // console log data
                $("#highest_hectres").html(data.highest_hectres + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_hectares_by.owner} <br />
                        State: ${data.highest_hectares_by.state} <br />
                        LGA: ${data.highest_hectares_by.lga} <br />
                    </span>
                `);
                $("#highest_acres").html(data.highest_acres + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_acres_by.owner} <br />
                        State: ${data.highest_acres_by.state} <br />
                        LGA: ${data.highest_acres_by.lga} <br />
                    </span>
                `);
                $("#highest_plots").html(data.highest_plots + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_plots_by.owner} <br />
                        State: ${data.highest_plots_by.state} <br />
                        LGA: ${data.highest_plots_by.lga} <br />
                    </span>
                `);
                $("#highest_greenhouse").html(data.highest_greenhouse + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_greenhouse_by.owner} <br />
                        State: ${data.highest_greenhouse_by.state} <br />
                        LGA: ${data.highest_greenhouse_by.lga} <br />
                    </span>
                `);

                var series =[
                  {
                    farmland: "Hectares",
                    size: data.highest_hectres
                  },
                  {
                    farmland: "Acres",
                    size: data.highest_acres
                  },
                  {
                    farmland: "Plots",
                    size: data.highest_plots
                  },
                  {
                    farmland: "Greenhouse",
                    size: data.highest_greenhouse
                  },
                ];

                initDonut(series);
            });
        }

        function initPieChart(series) {
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create("pie_chart_div", am4charts.PieChart);
            chart.data = series;

            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "result";
            pieSeries.dataFields.category = "crop";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;

            pieSeries.hiddenState.properties.opacity = 1;
            pieSeries.hiddenState.properties.endAngle = -90;
            pieSeries.hiddenState.properties.startAngle = -90;
        }

        function initBarChart(series) {
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create("bar_chart_div", am4charts.XYChart);
            chart.data = series;

            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "crop";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                if (target.dataItem && target.dataItem.index & 2 == 2) {
                    return dy + 25;
                }
                return dy;
            });

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "result";
            series.dataFields.categoryX = "crop";
            series.name = "result";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
        }

        function initBarChartForStates(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("single_chart_div", am4charts.XYChart);

            // Add percent sign to all numbers
            chart.numberFormatter.numberFormat = "#";


            chart.data = series;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "local";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Farmers Captured";
            valueAxis.title.fontWeight = 400;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "area";
            series.dataFields.categoryX = "local";
            series.columns.template.fill = am4core.color("#2CA7CB");
            series.columns.template.width = am4core.percent(80);
            series.columns.template.strokeWidth = 0;
            series.clustered = false;
            series.tooltipText = "Arable land in {categoryX}: [bold]{valueY}[/]ha";

            var series2 = chart.series.push(new am4charts.ColumnSeries());
            series2.dataFields.valueY = "farmers";
            series2.dataFields.categoryX = "local";
            series2.clustered = false;
            series2.columns.template.fill = am4core.color("#E7AC59");
            series2.columns.template.width = am4core.percent(30);
            series2.columns.template.strokeWidth = 0;
            series2.tooltipText = "Farmers in {categoryX}: [bold]{valueY}[/]";

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.disabled = true;
            chart.cursor.lineY.disabled = true;
        }

        function initMapAreaForStates() {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            window.onload = function() {

                /**
                 * This demo uses our own method of determining user's location
                 * It is not public web service that you can use
                 * You'll need to find your own. We recommend http://www.maxmind.com
                 */
                var defaultMap = "nigeriaLow";
                var countryMaps = {
                    "AL": [ "albaniaLow" ],
                    "DZ": [ "algeriaLow" ],
                    "AD": [ "andorraLow" ],
                    "AO": [ "angolaLow" ],
                    "AR": [ "argentinaLow" ],
                    "AM": [ "armeniaLow" ],
                    "AU": [ "australiaLow" ],
                    "AT": [ "austriaLow" ],
                    "AZ": [ "azerbaijanLow" ],
                    "BH": [ "bahrainLow" ],
                    "BD": [ "bangladeshLow" ],
                    "BY": [ "belarusLow" ],
                    "BE": [ "belgiumLow" ],
                    "BZ": [ "belizeLow" ],
                    "BM": [ "bermudaLow" ],
                    "BT": [ "bhutanLow" ],
                    "BO": [ "boliviaLow" ],
                    "BW": [ "botswanaLow" ],
                    "BR": [ "brazilLow" ],
                    "BN": [ "bruneiDarussalamLow" ],
                    "BG": [ "bulgariaLow" ],
                    "BF": [ "burkinaFasoLow" ],
                    "BI": [ "burundiLow" ],
                    "KH": [ "cambodiaLow" ],
                    "CM": [ "cameroonLow" ],
                    "CA": [ "canandaLow" ],
                    "CV": [ "capeVerdeLow" ],
                    "CF": [ "centralAfricanRepublicLow" ],
                    "TD": [ "chadLow" ],
                    "CL": [ "chileLow" ],
                    "CN": [ "chinaLow" ],
                    "CO": [ "colombiaLow" ],
                    "CD": [ "congoDRLow" ],
                    "CG": [ "congoLow" ],
                    "CR": [ "costaRicaLow" ],
                    "HR": [ "croatiaLow" ],
                    "CZ": [ "czechRepublicLow" ],
                    "DK": [ "denmarkLow" ],
                    "DJ": [ "djiboutiLow" ],
                    "DO": [ "dominicanRepublicLow" ],
                    "EC": [ "ecuadorLow" ],
                    "EG": [ "egyptLow" ],
                    "SV": [ "elSalvadorLow" ],
                    "EE": [ "estoniaLow" ],
                    "SZ": [ "eswatiniLow" ],
                    "FO": [ "faroeIslandsLow" ],
                    "FI": [ "finlandLow" ],
                    "FR": [ "franceLow" ],
                    "GF": [ "frenchGuianaLow" ],
                    "GE": [ "georgiaLow" ],
                    "DE": [ "germanyLow" ],
                    "GR": [ "greeceLow" ],
                    "GL": [ "greenlandLow" ],
                    "GN": [ "guineaLow" ],
                    "HN": [ "hondurasLow" ],
                    "HK": [ "hongKongLow" ],
                    "HU": [ "hungaryLow" ],
                    "IS": [ "icelandLow" ],
                    "IN": [ "indiaLow" ],
                    "GB": [ "ukLow" ],
                    "IE": [ "irelandLow" ],
                    "IL": [ "israelLow" ],
                    "PS": [ "palestineLow" ],
                    "MT": [ "italyLow" ],
                    "SM": [ "italyLow" ],
                    "VA": [ "italyLow" ],
                    "IT": [ "italyLow" ],
                    "JP": [ "japanLow" ],
                    "MX": [ "mexicoLow" ],
                    "RU": [ "russiaCrimeaLow" ],
                    "KR": [ "southKoreaLow" ],
                    "ES": [ "spainLow" ],
                    "US": [ "usaAlbersLow" ],
                    "NG": [ "nigeriaLow" ],
                };

                let geo = {
                    country_code: "NG",
                    country_name: "Nigeria"
                }
                  
                // calculate which map to be used
                var currentMap = defaultMap;
                var title = "";
                if ( countryMaps[ geo.country_code ] !== undefined ) {
                    currentMap = countryMaps[ geo.country_code ][ 0 ];
                    // add country title
                    if ( geo.country_name ) {
                      title = geo.country_name;
                    }
                }
                  
                // Create map instance
                var chart = am4core.create("land_map_area", am4maps.MapChart);  
                    chart.titles.create().text = title;
                    // Set map definition      
                chart.geodataSource.url = "{{ url('admin/count/users/by/states') }}";
                chart.geodataSource.events.on("parseended", function(ev) {
                    var data = [];
                    for(var i = 0; i < ev.target.data.features.length; i++) {
                        var total_users = ev.target.data.features[i].properties.total_users;
                        var child_data = {
                            id: ev.target.data.features[i].id,
                            value: total_users
                        }

                        data.push(child_data);
                    }
                    polygonSeries.data = data;
                });

                // Set projection
                chart.projection = new am4maps.projections.Miller();

                // Create map polygon series
                var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

                //Set min/max fill color for each area
                polygonSeries.heatRules.push({
                    property: "fill",
                    target: polygonSeries.mapPolygons.template,
                    min: chart.colors.getIndex(1).brighten(1),
                    max: chart.colors.getIndex(1).brighten(-0.3)
                });

                // Make map load polygon data (state shapes and names) from GeoJSON
                polygonSeries.useGeodata = true;

                // Set up heat legend
                let heatLegend = chart.createChild(am4maps.HeatLegend);
                heatLegend.series = polygonSeries;
                heatLegend.align = "right";
                heatLegend.width = am4core.percent(25);
                heatLegend.marginRight = am4core.percent(4);
                heatLegend.minValue = 0;
                heatLegend.maxValue = 40000000;

                // Set up custom heat map legend labels using axis ranges
                var minRange = heatLegend.valueAxis.axisRanges.create();
                minRange.value = heatLegend.minValue;
                minRange.label.text = "Min";
                var maxRange = heatLegend.valueAxis.axisRanges.create();
                maxRange.value = heatLegend.maxValue;
                maxRange.label.text = "Max";

                // Blank out internal heat legend value axis labels
                heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
                    return "";
                });

                // Configure series tooltip
                var polygonTemplate = polygonSeries.mapPolygons.template;
                polygonTemplate.tooltipText = "{name}: {value}";

                // Create hover state and set alternative fill color
                var hs = polygonTemplate.states.create("hover");
                hs.properties.fill = chart.colors.getIndex(1).brighten(-0.5);
            }
        }

        function initDonut(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("landarea_chart_div", am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.legend = new am4charts.Legend();

            chart.data = series;
            chart.innerRadius = 100;

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value     = "size";
            series.dataFields.category  = "farmland";
        }

        function getAllReports() {
            var states  = $("#sort_state").val();
            var lgas    = $("#sort_lgas").val();
            var cluster = $("#sort_clusters").val();

            var params = {
                states: states,
                lgas: lgas,
                cluster: cluster
            };

            // process reports via days
            $.get('{{url("admin/get/reports")}}', params, function(data, textStatus, xhr) {
               // console log data
               console.log(data);
            });
        }

        function getAllStates() {
            $.get('{{url("admin/get/all/states")}}', function(data) {
                $("#sort_state").html("");
                $("#sort_state").append(`
                    <option value="Ogun">Ogun</option>
                `);
                $.each(data, function(index, val) {
                    $("#sort_state").append(`
                        <option value="${val.name}">${val.name}</option>
                    `);
                });
            });
        }

        function getLgas() {
            // body...
            var states = $("#sort_state").val();

            if(!states){
                states = "Ogun";
            }

            $(".target-report-title").html(`${states} State`);

            var params = {states: states};
            $.get('{{url("admin/get/state/lgas")}}', params, function(data) {
                $("#sort_lgas").html("");
                $.each(data, function(index, val) {
                    $("#sort_lgas").append(`
                        <option value="${val}">${val}</option>
                    `);
                });
            });

            // find all lgas
            getReportByState(states);
        }

        function getReportByState(state) {
            var params = {states: state};
            $.get('{{url("admin/get/reports/by/state")}}', params, function(data) {
                initBarChartForStates(data);
            });
        }

        function getReportByLga(state, lga) {
            var params = {
                state: state,
                lga: lga
            }

            $.get('{{url("admin/get/clusters/reports")}}', params, function(data) {
                initDonutChartForLga(data);
                initBarChartForLga(data);
            });
        }

        function getClusters() {
            // body...
            var states = $("#sort_state").val();
            var lgas = $("#sort_lgas").val();

            if(!states){
                states = "Ogun";
            }

            if(!lgas){
                lgas = "Abeokuta North";
            }

            $(".target-report-title-cluster").html(`Data from ${states} State in ${lgas}`);

            var params = {
                states: states,
                lgas: lgas
            };
            $.get('{{url("admin/get/state/lgas/clusters")}}', params, function(data) {
                $("#sort_clusters").html("");
                $.each(data, function(index, val) {
                    $("#sort_clusters").append(`
                        <option value="${val.cluster}">${val.cluster}</option>
                    `);
                });
            });

            getReportByLga(states, lgas);
        }

        function getClusterVillages() {
            $("#villages_bar_chart").html("Loading...");
            var state   = $("#sort_state").val();
            var lga     = $("#sort_lgas").val();
            var cluster = $("#sort_clusters").val();

            if(!state){
                state = "Ogun";
            }

            if(!lga){
                lga = "Abeokuta North";
            }

            var params = {state, lga, cluster}

            $(".target-title-cluster").html(cluster);

            $.get('{{url("admin/get/cluster/villages")}}', params, function(data) {
                // console.log(data);
                initBarChartForVillages(data)
            });
        }

        function getTotalUsersByState(state) {
            return new Promise(function(resolve, reject){
                $.get('{{url("admin/count/users/by/states")}}', {state: state}, function(val) {
                    resolve(val);
                }).fail(function(err){
                    reject(err);
                });
            });
        }

        function initDonutChartForLga(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            series[0].disabled      = true;
            series[0].color         = am4core.color("#dadada");
            series[0].opacity       = 0.3;
            series[0].strokeDasharray = "4,4";

            var data = series;
            
            // cointainer to hold both charts
            var container = am4core.create("clusters_chart_div", am4core.Container);
            container.width = am4core.percent(100);
            container.height = am4core.percent(100);
            container.layout = "horizontal";

            container.events.on("maxsizechanged", function () {
                chart1.zIndex = 0;
                separatorLine.zIndex = 1;
                dragText.zIndex = 2;
                chart2.zIndex = 3;
            })

            var chart1 = container.createChild(am4charts.PieChart);
            chart1 .fontSize = 11;
            chart1.hiddenState.properties.opacity = 0; // this makes initial fade in effect
            chart1.data = data;
            chart1.radius = am4core.percent(70);
            chart1.innerRadius = am4core.percent(40);
            chart1.zIndex = 1;

            var series1 = chart1.series.push(new am4charts.PieSeries());
            series1.dataFields.value = "farmers";
            series1.dataFields.category = "cluster";
            series1.colors.step = 2;
            series1.alignLabels = false;
            series1.labels.template.bent = true;
            series1.labels.template.radius = 3;
            series1.labels.template.padding(0,0,0,0);

            var sliceTemplate1 = series1.slices.template;
            sliceTemplate1.cornerRadius = 5;
            sliceTemplate1.draggable = true;
            sliceTemplate1.inert = true;
            sliceTemplate1.propertyFields.fill = "color";
            sliceTemplate1.propertyFields.fillOpacity = "opacity";
            sliceTemplate1.propertyFields.stroke = "color";
            sliceTemplate1.propertyFields.strokeDasharray = "strokeDasharray";
            sliceTemplate1.strokeWidth = 1;
            sliceTemplate1.strokeOpacity = 1;

            var zIndex = 5;

            sliceTemplate1.events.on("down", function (event) {
                event.target.toFront();
                // also put chart to front
                var series = event.target.dataItem.component;
                series.chart.zIndex = zIndex++;
            })

            series1.ticks.template.disabled = true;

            sliceTemplate1.states.getKey("active").properties.shiftRadius = 0;

            sliceTemplate1.events.on("dragstop", function (event) {
                handleDragStop(event);
            })

            // separator line and text
            var separatorLine = container.createChild(am4core.Line);
            separatorLine.x1 = 3;
            separatorLine.y2 = 300;
            separatorLine.strokeWidth = 3;
            separatorLine.stroke = am4core.color("#dadada");
            separatorLine.valign = "middle";
            separatorLine.strokeDasharray = "5,5";


            var dragText = container.createChild(am4core.Label);
            dragText.text = "Drag slices over the line";
            dragText.rotation = 90;
            dragText.valign = "middle";
            dragText.align = "center";
            dragText.paddingBottom = 5;

            // second chart
            var chart2 = container.createChild(am4charts.PieChart);
            chart2.hiddenState.properties.opacity = 0; // this makes initial fade in effect
            chart2 .fontSize = 11;
            chart2.radius = am4core.percent(70);
            chart2.data = data;
            chart2.innerRadius = am4core.percent(40);
            chart2.zIndex = 1;

            var series2 = chart2.series.push(new am4charts.PieSeries());
            series2.dataFields.value = "farmers";
            series2.dataFields.category = "cluster";
            series2.colors.step = 2;

            series2.alignLabels = false;
            series2.labels.template.bent = true;
            series2.labels.template.radius = 3;
            series2.labels.template.padding(0,0,0,0);
            series2.labels.template.propertyFields.disabled = "disabled";

            var sliceTemplate2 = series2.slices.template;
            sliceTemplate2.copyFrom(sliceTemplate1);

            series2.ticks.template.disabled = true;

            function handleDragStop(event) {
                var targetSlice = event.target;
                var dataItem1;
                var dataItem2;
                var slice1;
                var slice2;

                if (series1.slices.indexOf(targetSlice) != -1) {
                    slice1 = targetSlice;
                    slice2 = series2.dataItems.getIndex(targetSlice.dataItem.index).slice;
                }
                else if (series2.slices.indexOf(targetSlice) != -1) {
                    slice1 = series1.dataItems.getIndex(targetSlice.dataItem.index).slice;
                    slice2 = targetSlice;
                }


                dataItem1 = slice1.dataItem;
                dataItem2 = slice2.dataItem;

                var series1Center = am4core.utils.spritePointToSvg({ x: 0, y: 0 }, series1.slicesContainer);
                var series2Center = am4core.utils.spritePointToSvg({ x: 0, y: 0 }, series2.slicesContainer);

                var series1CenterConverted = am4core.utils.svgPointToSprite(series1Center, series2.slicesContainer);
                var series2CenterConverted = am4core.utils.svgPointToSprite(series2Center, series1.slicesContainer);

                // tooltipY and tooltipY are in the middle of the slice, so we use them to avoid extra calculations
                var targetSlicePoint = am4core.utils.spritePointToSvg({ x: targetSlice.tooltipX, y: targetSlice.tooltipY }, targetSlice);

                if (targetSlice == slice1) {
                    if (targetSlicePoint.x > container.pixelWidth / 2) {
                        var value = dataItem1.value;

                        dataItem1.hide();

                        var animation = slice1.animate([{ property: "x", to: series2CenterConverted.x }, { property: "y", to: series2CenterConverted.y }], 400);
                        animation.events.on("animationprogress", function (event) {
                            slice1.hideTooltip();
                        })

                        slice2.x = 0;
                        slice2.y = 0;

                        dataItem2.show();
                    }
                    else {
                        slice1.animate([{ property: "x", to: 0 }, { property: "y", to: 0 }], 400);
                    }
                }
                if (targetSlice == slice2) {
                    if (targetSlicePoint.x < container.pixelWidth / 2) {

                        var value = dataItem2.value;

                        dataItem2.hide();

                        var animation = slice2.animate([{ property: "x", to: series1CenterConverted.x }, { property: "y", to: series1CenterConverted.y }], 400);
                        animation.events.on("animationprogress", function (event) {
                            slice2.hideTooltip();
                        })

                        slice1.x = 0;
                        slice1.y = 0;
                        dataItem1.show();
                    }
                    else {
                        slice2.animate([{ property: "x", to: 0 }, { property: "y", to: 0 }], 400);
                    }
                }

                toggleDummySlice(series1);
                toggleDummySlice(series2);

                series1.hideTooltip();
                series2.hideTooltip();
            }

            function toggleDummySlice(series) {
                var show = true;
                for (var i = 1; i < series.dataItems.length; i++) {
                    var dataItem = series.dataItems.getIndex(i);
                    if (dataItem.slice.visible && !dataItem.slice.isHiding) {
                        show = false;
                    }
                }

                var dummySlice = series.dataItems.getIndex(0);
                if (show) {
                    dummySlice.show();
                }
                else {
                    dummySlice.hide();
                }
            }

            series2.events.on("datavalidated", function () {

                var dummyDataItem = series2.dataItems.getIndex(0);
                dummyDataItem.show(0);
                dummyDataItem.slice.draggable = false;
                dummyDataItem.slice.tooltipText = undefined;

                for (var i = 1; i < series2.dataItems.length; i++) {
                    series2.dataItems.getIndex(i).hide(0);
                }
            })

            series1.events.on("datavalidated", function () {
                var dummyDataItem = series1.dataItems.getIndex(0);
                dummyDataItem.hide(0);
                dummyDataItem.slice.draggable = false;
                dummyDataItem.slice.tooltipText = undefined;
            })
        }

        function initBarChartForLga(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("clusters_bar_chart_div", am4charts.XYChart);

            // Add percent sign to all numbers
            chart.numberFormatter.numberFormat = "#";

            chart.data = series;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "cluster";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Farmers Captured";
            valueAxis.title.fontWeight = 400;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "area";
            series.dataFields.categoryX = "cluster";
            series.clustered = false;
            series.columns.template.fill = am4core.color("#87B646");
            series.columns.template.width = am4core.percent(80);
            series.columns.template.strokeWidth = 0;
            series.tooltipText = "Arable land in {categoryX}: [bold]{valueY}[/]ha";

            var series2 = chart.series.push(new am4charts.ColumnSeries());
            series2.dataFields.valueY = "farmers";
            series2.dataFields.categoryX = "cluster";
            series2.clustered = false;
            series2.columns.template.fill = am4core.color("#E7AC51");
            series2.columns.template.width = am4core.percent(30);
            series2.columns.template.strokeWidth = 0;
            series2.tooltipText = "Farmers in {categoryX}: [bold]{valueY}[/]";

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.disabled = true;
            chart.cursor.lineY.disabled = true;
        }

        function initBarChartForVillages(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("villages_bar_chart", am4charts.XYChart);

            // Add data
            chart.data = series;

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "cluster";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 10;

            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
              if (target.dataItem && target.dataItem.index & 2 == 2) {
                return dy + 25;
              }
              return dy;
            });

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "total";
            series.dataFields.categoryX = "cluster";
            series.name = "Total";
            series.columns.template.tooltipText = "Total village(s) found in {categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;
            series.columns.template.fill = amcharts.color("#CBB39B");
            series.columns.template.strokeWidth = 0;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
        }
    </script>
@endsection