@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | Tracking
@endsection

{{-- Contents  --}}
@section('contents')
<style type="text/css">
  .signature {
    position: absolute;
    z-index: 1;
    margin-left: 15%;
    margin-top: 10%;
  }
</style>
  <input type="hidden" id="tracking_id" value="{{ $id }}">
  @include('components.sticky-menu')

  <!-- / .main-navbar -->
  <div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Tracking</span>
      </div>
    </div>

    <!-- Default Light Table -->
    <div class="row">
      <div class="col-md-12">
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">
              Tracking
              <a style="float: right;" href="javascript:void(0);" onclick="showUpdateLogistic()">
                <i class="fa fa-edit"></i> Update 
              </a>
            </h6>
          </div>
          <div class="card-body p-4 pb-3">
            <div class="tracking-information"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Default Light Table -->
  </div>

  @include('admin-components.track-logistics')
@endsection

{{-- Scripts --}}
@section('scripts')
  {{-- custom js --}}
  <script type="text/javascript">
    // on document ready
    // $(document).ready(function (){
    getTrackedProductById();
    // });

    // show track location modal
    function showUpdateLogistic() {
      $("#update_logistic").modal("show");
    }

    // get tracked products
    function getTrackedProductById() {
      var tracking_id = $("#tracking_id").val();
      $.get('{{url('admin/load/tracking')}}/'+tracking_id, function(data) {
        // console log data
        console.log(data);
        $(".tracking-information").html(`
          <section class="invoice printableArea">
            <div class="row">
              <div class="col-12">
                <img src="{{asset('images/img-set/logo-transparent-new.png')}}" width="160" height="auto">
                <img src="{{asset('images/img-set/yeelda-signature.jpg')}}" class="signature">
                <br /><br />
                <div class="page-header">
                  <h4 class="d-inline">
                    <span class="font-size-30">
                      BWL NO: ${data.tracking.tracking_ref}
                    </span>
                  </h4>
                  <div class="pull-right text-right">
                    <h3 class="payment_date"></h3>
                  </div>  
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-md-6 invoice-col">
                <strong>From</strong> 
                <address>
                  <strong class="text-blue font-size-24">{{ env("APP_NAME") }} Admin</strong><br>
                  <strong class="d-inline">12a Reeve Road Ikoyi Lagos.
                  </strong><br>
                  <strong>
                    <b>Phone:</b> (+234) 8076352686 &nbsp;&nbsp;&nbsp;&nbsp; <b>Email:</b> info@yeelda.com
                  </strong>  
                </address>
              </div>
              <!-- /.col -->
              <div class="col-md-6 invoice-col text-right">
                
                <address>
                  <h4>
                   <strong>To</strong><br /> <span class="client_name">${data.buyer_info.name}</span>
                  </h4>
                  <strong class="text-blue font-size-13">
                      Destination: <span class="client_address">${data.tracking.destination}</span>
                    <br>
                    Email: <span class="client_email">${data.buyer_info.email}</span>
                  </strong>
                  <br />
                  <strong>
                    Phone: <span class="client_phone">${data.buyer_info.phone}</span>
                  </strong>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-12 invoice-col mb-15">
                <div class="invoice-details row no-margin">
                  <div class="col-md-6">
                  <h5>Logistics</h5> 
                  <address>
                    <strong class="d-inline">
                      Courier Service: ${data.tracking.carrier}
                    </strong><br />
                    <strong>
                      Courier Agent: ${data.tracking.assignee}
                    </strong>  
                  </address>

                    <b>Transaction ID:</b> <span class="text-green trans_ref">
                    ${data.tracking.tracking_ref}
                    </span>
                  </div>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            
            <!-- Table row -->
            <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Description</th>
                      <th>Transport Type</th>
                      <th>Current Location</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody class="load-tracking-info">
                    <tr>
                      <td>${data.tracking.pick_date}</td>
                      <td></td>
                      <td>NILL</td>
                      <td>${data.tracking.location}</td>
                      <td>${data.tracking.status}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-12">
                <button id="print" class="btn btn-warning" type="button">
                  <span>
                    <i class="fa fa-print"></i> Print
                  </span>
                </button>
              </div>
            </div>
          </section>
        `);

        $("#bwl_ref").val(data.tracking.tracking_ref);
        $("#bwl_assignee").val(data.tracking.assignee);
        $("#bwl_delivery_date").val(data.tracking.drop_date);
        $(".bwl_no").val();


        $.each(data.transit, function(index, val) {
          $(".load-tracking-info").append(`
            <tr>
              <td>${moment(val.created_at).format("YYYY-MM-D")}</td>
              <td>${data.product.product_name}</td>
              <td>NILL</td>
              <td>${val.location}</td>
              <td>${val.status}</td>
            </tr>
          `);
        });
      });
    }

    $("#update_product_btn").click(function (c){
      c.preventDefault();

      var token         = $("#token").val();
      var bwl_ref       = $("#bwl_ref").val();
      var bwl_assignee  = $("#bwl_assignee").val();
      var bwl_status    = $("#bwl_status").val();
      var bwl_delivery_date   = $("#bwl_delivery_date").val();
      var bwl_transport_type  = $("#bwl_transport_type").val();
      var bwl_location_address = $("#bwl_location_address").val();
      var bwl_reasons   = $("#bwl_reasons").val();

      var params = {
        _token: token,
        bwl_ref: bwl_ref,
        bwl_assignee: bwl_assignee,
        bwl_status: bwl_status,
        bwl_delivery_date: bwl_delivery_date,
        bwl_transport_type: bwl_transport_type,
        bwl_location_address: bwl_location_address,
        bwl_reasons: bwl_reasons
      }

      $.post('{{url('admin/update/tracking')}}', params, function(data) {
        if(data.status == "success"){
          swal(
            data.status,
            data.message,
            data.status
          );

          $("#update_logistic").modal("hide");
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });
    });

    // print
    $("#print").click(function(c){
      window.print();
    });
  </script>
@endsection