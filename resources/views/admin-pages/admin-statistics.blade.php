@extends('layouts.admin-pages-skin')

@section('title')
  YEELDA | Statistics
@endsection

@section('contents')
    <style>
        #pie_chart_div {
          width: 100%;
          height: 570px;
        }
        #bar_chart_div {
          width: 100%;
          height: 290px;
        }

        #landarea_chart_div {
            width: 100%;
            height: 370px;
        }
        tspan {
            color:#FFF;
        }                    
    </style>

    <div class="main-navbar sticky-top bg-white">
        <!-- Main Navbar -->
        <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
            <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                <div class="input-group input-group-seamless ml-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="fas fa-search"></i>
                    </div>
                  </div>
                  <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
            </form>
            <ul class="navbar-nav border-left flex-row ">
                <li class="nav-item border-right dropdown notifications">
          <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="nav-link-icon__wrapper">
              <i class="material-icons">&#xE7F4;</i>
              <span class="badge badge-pill badge-danger">2</span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="#">
              <div class="notification__icon-wrapper">
                <div class="notification__icon">
                  <i class="material-icons">&#xE6E1;</i>
                </div>
              </div>
              <div class="notification__content">
                <span class="notification__category">Statistic</span>
                <p>Your website’s active users count increased by
                  <span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
              </div>
            </a>
            <a class="dropdown-item" href="#">
              <div class="notification__icon-wrapper">
                <div class="notification__icon">
                  <i class="material-icons">&#xE8D1;</i>
                </div>
              </div>
              <div class="notification__content">
                <span class="notification__category">Sales</span>
                <p>Last week your store’s sales count decreased by
                  <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
              </div>
            </a>
            <a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
          </div>
                </li>
                <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="user-avatar rounded-circle mr-2" src="{{asset('admin-assets/images/avatars/0.jpg')}}" alt="User Avatar">
            <span class="d-none d-md-inline-block">{{ Auth::user()->email }}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-small">
            <a class="dropdown-item" href="{{url('admin/account')}}">
              <i class="material-icons">&#xE7FD;</i> Profile</a>
            <a class="dropdown-item" href="{{url('admin/blogs')}}">
              <i class="material-icons">vertical_split</i> Blog Posts</a>
            <a class="dropdown-item" href="add-new-post.html">
              <i class="material-icons">note_add</i> Add New Post</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item text-danger" href="{{url('admin/logout')}}">
              <i class="material-icons text-danger">&#xE879;</i> Logout </a>
          </div>
                </li>
            </ul>
        
            <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                    <i class="material-icons">&#xE5D2;</i>
                </a>
            </nav>
        </nav>
    </div>
    
    <div class="main-content-container container-fluid px-4">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Statistics</span>
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12">
                <div class="loading-bar" style="display: none;">
                    <div class="load-bar">
                      <div class="bar"></div>
                      <div class="bar"></div>
                      <div class="bar"></div>
                    </div>
                </div>
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Bar Chart

                            <div class="float-right">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select onchange="showReportsGraph(0)" id="sort_0" class="custom-select custom-select-sm" style="min-width: 130px;">
                                          <option selected>Overall</option>
                                          <option value="0">Last Week</option>
                                          <option value="today">Today</option>
                                          <option value="month">Last Month</option>
                                          <option value="year">Last Year</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <select onchange="showReportsGraph(1)" id="sort_1" class="custom-select custom-select-sm" style="min-width: 130px;">
                                            <option selected>Overall</option>
                                            <option value="state">By State</option>
                                            <option value="lga">By LGA</option>
                                            <option value="cluster">By Clusters</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        
                        <div id="bar_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Pie Chart</h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="pie_chart_div">Loading..</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Statistics Overview</h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Farmer(s) Record</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Highest Crop Planted</td>
                                    <td><span class="most_preferred">Loading...</span></td>
                                </tr>
                                <tr>
                                    <td>Least Crop Planted</td>
                                    <td><span class="least_preferred">Loading...</span></td>
                                </tr>
                                <tr>
                                    <td>Total Male Farmers</td>
                                    <td><span class="total_male">Loading...</span></td>
                                </tr>
                                <tr>
                                    <td>Total Female Farmers</td>
                                    <td><span class="total_female">Loading...</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <br />
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Statistics Overview</h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Farmer(s) Land Measurement</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Hectare(s)</td>
                                    <td><span class="total_hectares">Loading..</span></td>
                                </tr>
                                <tr>
                                    <td>Total Acre(s)</td>
                                    <td><span class="total_acres">Loading..</span></td>
                                </tr>
                                <tr>
                                    <td>Total Plot(s)</td>
                                    <td><span class="total_plots">Loading..</span></td>
                                </tr>
                                <tr>
                                    <td>Total Land area in Hectare</td>
                                    <td><span class="total_land_area">Loading..</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Land Size by Measurement (Highest Hectares, Acres, Plots and Greenhouse(s))</h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <div id="landarea_chart_div">Loading...</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Geo Location and Mapping</h6>
                    </div>
                    <div class="card-body p-4 pb-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Farmer(s) Land Area & Sizes</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Highest No. of Hectares</td>
                                    <td><span id="highest_hectres">Loading...</span></td>
                                </tr>
                                <tr>
                                    <td>Highest No. of Acres</td>
                                    <td><span id="highest_acres">Loading...</span></td>
                                </tr>
                                <tr>
                                    <td>Highest No. of Plot(s)</td>
                                    <td><span id="highest_plots">Loading...</span></td>
                                </tr>
                                <tr>
                                    <td>Highest No. of Greenhouse(s)</td>
                                    <td><span id="highest_greenhouse">Loading...</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('amcharts/js/core.js') }}"></script>
    <script src="{{ asset('amcharts/js/charts.js') }}"></script>
    <script src="{{ asset('amcharts/js/animated.js') }}"></script>
    <script type="text/javascript">
        loadAllStats();
        initBarChart();
        loadLandStats();
        getLandMeasurement();

        function loadAllStats() {
            $(".loading-bar").show();
            $.get('{{ url("admin/fetch/overall/statistics") }}', function(data) {
                $(".loading-bar").hide();
                $(".total_male").html(data.total_male);
                $(".total_female").html(data.total_female);
                $(".highest_planted").html(data.highest_planted);
                $(".most_preferred").html(data.most_preferred.crop);
                $(".least_preferred").html(data.least_preferred.crop);
                initPieChart(data.series);
                initBarChart(data.series);
            });
        }

        function loadLandStats() {
            $.get('{{ url("admin/fetch/geo/landarea") }}', function(data) {
                // console log data
                $("#highest_hectres").html(data.highest_hectres + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_hectares_by.owner} <br />
                        State: ${data.highest_hectares_by.state} <br />
                        LGA: ${data.highest_hectares_by.lga} <br />
                    </span>
                `);
                $("#highest_acres").html(data.highest_acres + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_acres_by.owner} <br />
                        State: ${data.highest_acres_by.state} <br />
                        LGA: ${data.highest_acres_by.lga} <br />
                    </span>
                `);
                $("#highest_plots").html(data.highest_plots + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_plots_by.owner} <br />
                        State: ${data.highest_plots_by.state} <br />
                        LGA: ${data.highest_plots_by.lga} <br />
                    </span>
                `);
                $("#highest_greenhouse").html(data.highest_greenhouse + `
                    <br />
                    <span class="small">
                        Owner's: ${data.highest_greenhouse_by.owner} <br />
                        State: ${data.highest_greenhouse_by.state} <br />
                        LGA: ${data.highest_greenhouse_by.lga} <br />
                    </span>
                `);

                var series =[
                  {
                    farmland: "Hectares",
                    size: data.highest_hectres
                  },
                  {
                    farmland: "Acres",
                    size: data.highest_acres
                  },
                  {
                    farmland: "Plots",
                    size: data.highest_plots
                  },
                  {
                    farmland: "Greenhouse",
                    size: data.highest_greenhouse
                  },
                ];

                initDonut(series);
            });
        }

        function initPieChart(series) {
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create("pie_chart_div", am4charts.PieChart);
            chart.data = series;

            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "result";
            pieSeries.dataFields.category = "crop";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;

            pieSeries.hiddenState.properties.opacity = 1;
            pieSeries.hiddenState.properties.endAngle = -90;
            pieSeries.hiddenState.properties.startAngle = -90;
        }

        function initBarChart(series) {
            am4core.useTheme(am4themes_animated);
            var chart = am4core.create("bar_chart_div", am4charts.XYChart);
            chart.data = series;

            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "crop";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                if (target.dataItem && target.dataItem.index & 2 == 2) {
                    return dy + 25;
                }
                return dy;
            });

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "result";
            series.dataFields.categoryX = "crop";
            series.name = "result";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
        }

        function initDonut(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("landarea_chart_div", am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.legend = new am4charts.Legend();

            chart.data = series;
            chart.innerRadius = 100;

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value     = "size";
            series.dataFields.category  = "farmland";
        }

        function showReportsGraph(x) {
            if(x === 0){
                var params = {
                    sort_value: $("#sort_0").val(),
                    sort_type: x
                };
            }else if(x === 1){
                var params = {
                    sort_value: $("#sort_1").val(),
                    sort_type: x
                };
            }

            // process reports via days
            $.get('{{url("admin/get/reports")}}', params, function(data, textStatus, xhr) {
               // console log data
               console.log(data);
            });
        }

        function getLandMeasurement() {
            var states = $("#select_state").val();
            if(!states){
                states = "Ogun";
            }

            $.get('{{url("admin/get/total/landtype")}}', {states}, function(data) {
                // console log data
                // console.log(data);
                $(".total_hectares").html(data.total_hectares);
                $(".total_acres").html(data.total_acres);
                $(".total_plots").html(data.total_plots);
                $(".total_greenhouse").html(data.total_greenhouse);
                $(".total_land_area").html(data.total_arable_hectares);
            });
        }
    </script>
@endsection