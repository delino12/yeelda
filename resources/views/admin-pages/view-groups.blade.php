@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Farmers Group
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">
                {{ Auth::guard('admin')->user()->email }}
            </span>            
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>

                    <div class="">
                        <a href="javascript:void(0);" onclick="showCreateGroupModal()" class="btn btn-info float-right">
                            <i class="fa fa-plus"></i> Create Members's Group
                        </a>
                    </div>
                </h6>

              </div>
              <div class="card-body p-5 pb-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Group's Name</th>
                            <th>Total Members Found</th>
                            <th>Option</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="load-all-groups"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->

        @include('admin-components.modals')
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        // get view user id
        loadGroupInformations();

        // fetch groups data
        function loadGroupInformations() {
            $.get('{{ url("admin/all/groups") }}', function(data) {
                /*optional stuff to do after success */
                $(".load-all-groups").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    $(".load-all-groups").append(`
                        <tr>
                            <td>${sn}</td>
                            <td>${val.name}</td>
                            <td>${val.total}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ url('view/group-captured/users') }}/${val.id}">
                                    <i class="fa fa-users"></i> View
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0);" onclick="showEditGroupModal(${val.id})" class="btn btn-info btn-sm">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                            </td>
                        </tr>
                    `);
                });
            });
        }

        // show create form modal
        function showCreateGroupModal() {
            $("#create-group-modal").modal();
        }

        // show edit form modal
        function showEditGroupModal(group_id) {
            var params = {group_id: group_id};

            $.get('{{url("admin/one/group")}}', params, function(data) {
                
                $(".edit-group-name").html(data.name);
                $("#edit_group_id").val(group_id);
                $("#edit_group_name").val(data.name);
            });

            $("#edit-group-modal").modal();
        }

        // create group
        function createGroup() {
            $("#create-group-btn").prop('disabled', true);
            $("#create-group-btn").html('Adding...');
            var token = $("#token").val();
            var name  = $("#group_name").val();

            var params = {
                _token: token,
                name: name
            }

            $.post('{{ url('admin/group/create') }}', params, function(data, textStatus, xhr) {
                if(data.status == "success"){
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                    $("#create-group-form")[0].reset();
                    $("#create-group-btn").prop('disabled', false);
                    $("#create-group-btn").html('Add Group');

                    // refresh
                    loadGroupInformations();
                    $("#create-group-modal").modal('hide');
                }else{
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                    $("#create-group-btn").prop('disabled', false);
                    $("#create-group-btn").html('Add Group');
                }
            });

            // return
            return false;
        }

        // edit group
        function updateGroup() {
            $("#edit-group-btn").prop('disabled', true);
            $("#edit-group-btn").html('Updating...');
            var token = $("#token").val();
            var group_id  = $("#edit_group_id").val();
            var group_name  = $("#edit_group_name").val();

            var params = {
                _token: token,
                group_id: group_id,
                group_name: group_name
            }

            $.post('{{ url('admin/group/edit') }}', params, function(data, textStatus, xhr) {
                if(data.status == "success"){
                    $("#edit-group-form")[0].reset();
                    $("#edit-group-btn").prop('disabled', false);
                    $("#edit-group-btn").html('Update Group');
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                    // refresh
                    loadGroupInformations();
                    $("#edit-group-modal").modal('hide');
                }else{
                    $("#edit-group-btn").prop('disabled', false);
                    $("#edit-group-btn").html('Update Group');
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                }
            });

            // return
            return false;
        }

        // delete group
        function deleteGroup(group_id) {
            $("#delele-group-btn").html('Deleting...');

            var token = $("#token").val();
            var params = {
                _token: token,
                group_id: group_id
            }

            $.post('{{ url("admin/group/delete") }}', params, function(data, textStatus, xhr) {
                if(data.status == "success"){
                    $("#delele-group-btn").prop('disabled', false);
                    $("#delele-group-btn").html('Delete');
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                    // refresh
                    loadGroupInformations();
                }else{
                    $("#delete-group-btn").prop('disabled', false);
                    $("#delete-group-btn").html('Delete');
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                }
            });

            // return 
            return false;
        }
    </script>
@endsection