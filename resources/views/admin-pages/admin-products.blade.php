@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | Products & Services
@endsection

{{-- Contents  --}}
@section('contents')
  @include('components.sticky-menu')
  
  <div class="main-content-container container-fluid px-4">
      <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Seasonal & Harvested Produce</span>
        </div>
      </div>

      <div class="row">
        <div class="col">
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">Farm produce</h6>
            </div>
            <div class="card-body p-0 pb-3">
              <table class="table mb-0">
                <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Name</th>
                    <th scope="col" class="border-0">Phone</th>
                    <th scope="col" class="border-0">Produce</th>
                    <th scope="col" class="border-0">Amount (&#8358;)</th>
                    <th scope="col" class="border-0">Option</th>
                    <th scope="col" class="border-0">Action</th>
                  </tr>
                </thead>
                <tbody class="all-produces">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-9">
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">Configure produce
                <span class="float-right">
                  <a href="javascript:void(0);" onclick="showAddProduceModal()">
                    <i class="fa fa-plus"></i> Add
                  </a>
                </span>
              </h6>
            </div>
            <div class="card-body p-4 pb-3">
              <table class="table mb-0" id="config-produce-table" data-pagination="true" data-search="true" data-page-size="10">
                <thead class="bg-light">
                  <tr>
                    <th>#</th>
                    <th>Produce</th>
                    <th>Open (&#8358;)</th>
                    <th>Close (&#8358;)</th>
                    <th>Action</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody class="list-allowed-produces">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- Add new produce modal -->
  <div class="modal fade" id="add-new-produce-modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Add Produce</h4>
        </div>
        <div class="modal-body">
          <form method="post" onsubmit="return addTickerProduce()">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Enter produce/crops name" id="new-default-produce" required="" name="">
            </div>
            <div class="form-group">
              <button class="btn btn-primary" type="submit">Add</button>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button class="btn btn-flat" type="button" data-dismiss="modal">
              close
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

{{-- Scripts --}}
@section('scripts')
  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript">
    loadProducts();
    loadAllowedProduces();

    function showAddProduceModal() {
      $("#add-new-produce-modal").modal();
    }

    function loadProducts() {
      $.get('/admin/load-products', function (data){
        $(".all-produces").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $(".all-produces").append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.owner+`</td>
              <td>`+val.phone+`</td>
              <td>`+val.name+`</td>
              <td>&#8358; `+val.amount+`</td>
              <td>
                <a href="javascript:void();" onclick="deleteItem(`+val.id+`)">
                  <i class="fa fa-trash"></i> Delete
                </a>
              </td>
              <td>
                <a href="/admin/view-products/`+val.id+`">
                  <i class="fa fa-copy"></i> Details
                </a>
              </td>
            </tr>
          `);
        });
      });
    }
    
    function deleteItem(x) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {

          $.get('/admin/delete-products/'+x, function (data){
            if(data.status == 'success'){
              swal(
                "deleted!",
                data.message,
                data.status
              );
              loadProducts();
            }
          });
        }
      });
    }

    function loadAllowedProduces() {
      $.get('{{url("admin/load/allowed/produces")}}', function(data) {
        $(".list-allowed-produces").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $(".list-allowed-produces").append(`
            <tr>
              <td>${sn}</td>   
              <td>${val.asset}</td>
              <td>
                <input type="number" class="form-control" value="${val.open}" id="open_price_${val.id}" placeholder="Update price" />
              </td>  
              <td>
                <input type="number" class="form-control" value="${val.close}" id="close_price_${val.id}" placeholder="Update price"/>
              </td>   
              <td>
                <a href="javascript:void(0);" onclick="changePrice(${val.id})" class="btn btn-success"> Update</a>
              </td>
              <td>
                <a href="javascript:void(0);" onclick="deleteProduce(${val.id})" class="btn btn-danger"> Delete</a>
              </td>
            </tr>
          `);
        });

        $("#config-produce-table").DataTable({
          'autoWidth'   : true
        });
      });
    }

    function changePrice(x) {
      var token       = $("#token").val();
      var open_price  = $(`#open_price_${x}`).val();
      var close_price = $(`#close_price_${x}`).val();
      
      var params = {
        _token: token,
        open_price: open_price,
        close_price: close_price,
        produce_id: x
      };

      $.post('{{url("admin/update/price/ticker")}}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            "Ok!",
            data.message,
            data.status
          );
          window.location.reload();
        }else{
          swal(
            "Oops!",
            data.message,
            data.status
          );
        }
      });
    }

    function deleteProduce(x) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.get('/admin/delete-default-products', {produce_id: x}, function (data){
            if(data.status == 'success'){
              swal(
                "deleted!",
                data.message,
                data.status
              );
              // loadProducts();
              window.location.reload();
            }
          });
        }
      });
    }

    function addTickerProduce() {
      var token   = $("#token").val();
      var produce = $("#new-default-produce").val();

      var params = {
        _token: token,
        produce: produce
      }

      $.post('{{url("admin/add/allowed/produce")}}', params, function(data, textStatus, xhr) {
        if(data.status == 'success'){
          swal(
            "Ok",
            data.message,
            data.status
          );
          // loadProducts();
          window.location.reload();
        }else{
          swal(
            "Oops!",
            data.message,
            data.status
          );
        }
      });

      // return
      return false;
    }
  </script>
@endsection