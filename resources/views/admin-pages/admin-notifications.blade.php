@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Notifications
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">All Notifications</span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="notification-wrapper">
                  <div class="notifications_list">
                    Loading...
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                  Recent Update!
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                {{-- data here --}}
              </div>
            </div>
          </div>
        </div>
    </div>
    @include('admin-components.modals')
@endsection

@section('scripts')
  <script type="text/javascript">

  </script>
@endsection