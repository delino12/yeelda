@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Duplicates
@endsection

{{-- Contents  --}}
@section('contents')
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">All Duplicated Records</span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                  <a href="{{ URL::previous() }}"> 
                      <i class="fa fa-angle-double-left"></i> Previous
                  </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <table class="table" id="duplicate_table" data-pagination="true" data-search="true">
                  <thead>
                    <tr>
                      <th>S/N</th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>State</th>
                      <th>LGA</th>
                      <th>Cluster</th>
                      <th>Address</th>
                      <th>Gender</th>
                    </tr>
                  </thead>
                  <tbody class="view-duplicates"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
    @include('admin-components.modals')
@endsection

@section('scripts')
  <script type="text/javascript">
    getAllDuplicates();

    function getAllDuplicates() {
      $.get('{{url("admin/fetch/duplicates")}}', function(data) {
        $(".view-duplicates").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $(".view-duplicates").append(`
            <tr>
              <td>${sn}</td>
              <td>${val.names}</td>
              <td>${val.phone}</td>
              <td>${val.state}</td>
              <td>${val.lga}</td>
              <td>${val.ward}</td>
              <td>${val.address}</td>
              <td>${val.gender}</td>
            </tr>
          `);
        });

        $("#duplicate_table").DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false,
          'dom'         : 'Bfrtip',
          'buttons'     : [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ]
        });
      });
    }
  </script>
@endsection