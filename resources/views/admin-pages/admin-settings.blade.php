@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | Accounts
@endsection

{{-- Contents  --}}
@section('contents')
  @include('components.sticky-menu')
  <div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Account Settings</span>
      </div>
    </div>
    <!-- End Page Header -->

    {{-- List Admin --}}
    <div class="row">
      <div class="col-md-12">
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <button class="btn btn-primary float-right" onclick="showAdminModal()">
              <i class="fa fa-plus"></i> Admin
            </button>
          </div>
          <div class="card-body p-4 pb-3">
            <h1 class="lead"><i class="fa fa-users"></i> Admin User(s)</h1><hr />
            <table class="table" id="load-admin-table"  data-pagination="true" data-search="true"data-page-size="10">
              <thead>
                <tr>
                  <th>S/N</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Level</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody class="load-all-admins">
                <tr>
                  <td><img src="/svg/yeelda-loading.svg" height="60" width="auto" /></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    {{-- List Agent --}}
    <div class="row">
      <div class="col-md-12">
        <div class="card card-small mb-4">
          <div class="card-header">
            <button class="btn btn-primary float-right" onclick="showAgentModal()">
              <i class="fa fa-plus"></i> Agent
            </button>
          </div>
          <div class="card-body p-4 pb-3">
            <h1 class="lead"><i class="fa fa-users"></i> Agent User(s)</h1><hr />
            <table class="table" id="load-agent-table" data-pagination="true" data-search="true" data-page-size="10">
              <thead>
                <tr>
                  <th>S/N</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Agent Code</th>
                  <th>Total Users (ODCA)</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody class="load-all-agents">
                <tr>
                  <td><img src="/svg/yeelda-loading.svg" height="60" width="auto" /></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- create agent modal --}}
  <div class="modal" id="show-agent-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <h6 class="m-0">Create Agent Users</h6>
          </div>
          <div class="modal-body p-2">
            <form id="add-agent-form" method="post" onsubmit="return addNewAgent()">
              <div class="form-group">
                <label>Agent Code</label>
                <select class="form-control" id="agent-code"></select>
              </div>

              <div class="form-group">
                <label>Names</label>
                <input type="text" class="form-control" id="agent-names" placeholder="Eg: John Doe" required="">
              </div>

              <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" id="agent-email" placeholder="Eg: username@yeelda.com" required="">
              </div>

              <div class="form-group">
                <label>phone</label>
                <input type="phone" class="form-control" id="agent-phone" placeholder="phone" maxlength="11" required="">
              </div>

              <div class="form-group">
                <button class="btn btn-primary col-md-12">
                  Add Agent 
                  <img src="/svg/loading.svg" id="agent-loading" class="pull-right" width="40" height="20" style="display: none;">
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- create admin modal --}}
  <div class="modal" id="show-admin-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header border-bottom">
          <h6 class="m-0">Create Admin User</h6>
        </div>
        <div class="modal-body p-2 pb-3">
          <form id="add-user-form" method="post" onsubmit="return addNewUser()">
            <div class="form-group">
              <label>Admin Level</label>
              <select class="form-control" id="level" onchange="displayPermission()">
                <option value="alpha">Alpha</option>
                <option value="beta">Beta</option>
                <option value="omega">Omega</option>
                <option value="support">Customer Support</option>
              </select>
            </div>

            <div class="form-group">
              <label>Names</label>
              <input type="text" class="form-control" id="names" placeholder="Eg: John Doe" required="">
            </div>

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" id="email" placeholder="Eg: username@yeelda.com" required="">
            </div>

            <div class="form-group">
              <label>password</label>
              <span class="pull-right">
                <a href="javascript:void(0);" onclick="showHidePass()"><i class="fa fa-eye"></i></a>
              </span>
              <input type="password" class="form-control" id="password" placeholder="password" required="">
            </div>

            <div class="form-group">
              <button class="btn btn-primary col-md-12">
                Add Admin 
                <img src="/svg/loading.svg" id="loading" class="pull-right" width="40" height="20" style="display: none;">
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

{{-- Scripts --}}
@section('scripts')
  <script type="text/javascript">
    // load all admin
    refreshAdminList();
    showAgentCode();
    loadAllAgents();
      
    // show admin modal
    function showAdminModal() {
      $("#show-admin-modal").modal();
    }

    // show agent modal
    function showAgentModal() {
      $("#show-agent-modal").modal();
    }

    function createNewUser() {
      // body...
      return false;
    }

    function displayPermission() {
      // body...
      return false;
    }

    // add new admin user
    function addNewUser(argument) {
      var token    = '{{ csrf_token() }}';
      var names    = $("#names").val();
      var email    = $("#email").val();
      var password = $("#password").val();
      var level    = $("#level").val();

      var data = {
        _token:token,
        names:names,
        email:email,
        level:level,
        password:password
      };

      $.post('/admin/create/admin/user', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        if(data.status == 'success'){
          $(".success_msg").append(`
            <span class="text-success">${data.message}</span>
          `);

          // reset form data
          $("#add-user-form")[0].reset();

          swal(
            data.status,
            data.message,
            'success'
          );

          // refresh list
          refreshAdminList();
        }else{
          swal(
            data.status,
            data.message,
            'error'
          );
        }
      });

      // void form
      return false;
    }

    // show/hide password
    function showHidePass() {
      let passwordArea = document.getElementById("password");

      if(passwordArea.type === 'password'){
        passwordArea.type = 'text';
      }else{
        if(passwordArea.type === 'text'){
          passwordArea.type = 'password'
        }
      }
    }

    // load admin list
    function refreshAdminList() {
      // body...
      $.get('/admin/load/admin', function(data) {
        $(".load-all-admins").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $('.load-all-admins').append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.names+`</td>
              <td>`+val.email+`</td>
              <td>`+val.level+`</td>
              <td><a href="/admin/users/`+val.id+`/view">view</a></td>
            </tr>
          `);
        });
        $('#load-admin-table').bootstrapTable();
      });
    }

    // add new Agent
    function addNewAgent() {
      // body...
      $("#agent-loading").show();
      var token   = '{{ csrf_token() }}';
      var names   = $("#agent-names").val();
      var email   = $("#agent-email").val();
      var phone   = $("#agent-phone").val();
      var code  = $("#agent-code").val();

      var data = {
        _token:token,
        names:names,
        email:email,
        phone:phone,
        agent_code:code
      };

      $.post('/admin/create/agent/user', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        if(data.status == 'success'){
          $(".success_msg").append(`
            <span class="text-success">${data.message}</span>
          `);

          swal(
            data.status,
            data.message,
            'success'
          );

          // reset form data
          $("#add-agent-form")[0].reset();

          // refresh list
          loadAllAgents();
          $("#agent-loading").hide();
        }else{
          swal(
            data.status,
            data.message,
            'error'
          );
        }
      });

      // void form
      return false;
    }

    // load all agents
    function loadAllAgents() {
      $.get('/admin/load/agents', function(data) {
        $(".load-all-agents").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $('.load-all-agents').append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.names+`</td>
              <td>`+val.email+`</td>
              <td>`+val.agent_id+`</td>
              <td>`+val.total+`</td>
              <td><a href="/admin/agent/`+val.id+`/view">view</a></td>
            </tr>
          `);
        });
        $('#load-agent-table').bootstrapTable();
      });
    }

    // show Agent Code
    function showAgentCode() {
      // body...
      var agentCodes = ["Y0123", "Y0124", "Y0125", "Y0126", "Y0127", "Y0128", "Y0129", "Y0130", "Y0131", "Y0132"];
      for(var i = 0; i < agentCodes.length; i++){
        $("#agent-code").append(`
          <option value="${agentCodes[i]}"> ${agentCodes[i]} </option>
        `);
      }
    }
  </script>
@endsection