@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | View Product Details
@endsection

{{-- Contents  --}}
@section('contents')
    <!-- / .main-navbar -->
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">YEELDA PRODUCT</span>
          </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0">
                  <img class="y-img" src="/images/icon-set/poultry.png">
                  <span id="product-title"></span>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                  <div class="col-md-6">
                    <a href="{{url('/admin/products')}}" style="margin-left: 20px;"><i class="fa fa-chevron-left"></i> Go back</a>
                    <div class="load-products"></div>
                  </div>
                  <div class="col-md-6">
                    <div class="load-others"></div>
                  </div>
              </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- custom js --}}
    <script type="text/javascript">
        // $(document).ready(function(){
        loadSingleProduce();
        // });

        function loadSingleProduce() {
            var pid = '{{ $id }}';
            $.get('/load/single/produce/?id='+pid, function (data){
              // console.log(data);
              var value = data;
              $(".load-products").append(`
                <div class="col-md-12">
                  <div class="card h-100" style="padding: 1em;">
                    <h4 class="lead">Product Information</h4><br />
                    <a href="/product/details/`+value.id+`">
                    <img class="card-img-top" src="/uploads/products/`+value.product_image+`" width="350" height="300" alt=""></a>
                    <div class="card-body">
                      <h4 class="small">
                        <a href="/product/details/`+value.id+`"><i class="fa fa-user"></i> `+value.owner_name+` (Farmer)</a>
                      </h4>
                      <h5> &#8358; `+value.product_total+`</h5>
                      <p>
                        <table class="table">
                          <tr>
                            <td>Product</td>
                            <td>`+value.product_name+`</td>
                          </tr>
                          <tr>
                            <td>Unit price</td>
                            <td> <b>&#8358;`+value.product_price+`</b> `+value.product_size_no+` QTY</td>
                          </tr>
                          <tr>
                            <td>Total</td>
                            <td><b> &#8358; `+value.product_total+`</b></td>
                          </tr>
                          <tr>
                            <td>Location</td>
                            <td>`+value.product_location+`</td>
                          </tr>
                          <tr>
                            <td>Weight</td>
                            <td>`+value.product_size_type+` `+value.product_size_no+` </td>
                          </tr>
                          
                          <tr>
                            <td>Contact</td>
                            <td>`+value.owner_contact+`</td>
                          </tr>
                        </table>
                      </p>
                    </div>
                    <div class="card-footer">
                      <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                  </div>
                </div>
              `);

              $("#product-title").html(value.product_name);

              $(".farmer-info").append(`
                <tr>
                  <td>Name: </td>
                  <td>`+value.owner_name+`</td>
                </tr>

                <tr>
                  <td>Email: </td>
                  <td>`+value.owner_email+`</td>
                </tr>

                <tr>
                  <td>Gender: </td>
                  <td>`+value.owner_gender+`</td>
                </tr>

                <tr>
                  <td>Mobile: </td>
                  <td>`+value.owner_contact+`</td>
                </tr>

                <tr>
                  <td>Office: </td>
                  <td>`+value.owner_office+`</td>
                </tr>

                <tr>
                  <td>Address: </td>
                  <td>`+value.owner_address+`</td>
                </tr>

                <tr>
                  <td>Postal: </td>
                  <td>`+value.owner_zipcode+`</td>
                </tr>

                <tr>
                  <td>Nationality: </td>
                  <td>`+value.owner_country+`</td>
                </tr>
              `);

              if(owner_image == ""){
                $(".load-basic").append(`
                  <img class="img-rounded" src="/uploads/farmers/`+value.owner_image+`" height="200" width="200" alt="farmer+image">
                `);
              }else{
                $(".load-basic").append(`
                  <img class="img-rounded" src="/uploads/farmers/`+value.owner_image+`" height="200" width="200" alt="farmer+image">
                `);
              }
            });
        }
    </script>
@endsection