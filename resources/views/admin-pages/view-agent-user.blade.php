@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Agent
@endsection

{{-- Contents  --}}
@section('contents')
    <style type="text/css">
        .y-thumbnail {
            height: 152px;
            border-radius: 4px;
            border:1px solid #FFF;
            width: 170px;
        }

        #chartdiv {
          width: 100%;
          height: 300px;
        }

        #show_user_chart {
          width: 100%;
          height: 300px;
        }

        .amcharts-export-menu-top-right {
          top: 10px;
          right: 0;
        }
    </style>
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">{{ Auth::user()->email }}</span>
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          
          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                </h6>
              </div>
              <div class="card-body p-5 pb-3">
                <div class="row">
                    <!-- edit form column -->
                    <div class="col-md-12">
                        <div class="success_msg"></div>
                        <div class="error_msg"></div>
                        <p class="lead">Update info</p>
                        <hr>
                        <div class="text-left">
                            <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                            <h6 class="small">Upload a different photo...</h6>
                            <input type="file" class="form-control">
                        </div>
                        <br />
                    
                        <form role="form" onsubmit="return updateAgentInformation()">
                            <div class="form-group">
                                <label>Names</label>
                                <input type="text" class="form-control" id="agent-names">
                            </div>
                            <div class="form-group">
                                <label>Email (<span class="text-danger">Note: </span>Yeelda will only accept all email with prefix; @yeelda.com or @yeelda.ng)</label>
                                <input type="text" class="form-control" id="agent-email">
                            </div>

                            <div class="pt-4"></div>
                            <div class="form-group">
                                <button class="btn btn-primary">Update & Save Information</button>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="#"> 
                        <i class="fa fa-user"></i> Details
                    </a>
                </h6>
              </div>
              <div class="card-body p-5 pb-3">
                <div class="row">
                    <!-- preview updates -->
                    <div class="col-md-12">
                        <p class="lead">Profile Information</p>
                        <hr>
                        <div class="load-information">
                            <img src="/svg/yeelda-loading.svg" height="60" width="auto" />
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="javascript:void(0);"> 
                        <i class="fa fa-chart-line"></i> Updates Report
                    </a>

                    <div class="float-right">
                        <div class="row">
                            <div class="col-md-6">
                                <select onchange="fetchDataCaptured()" id="update_report" class="custom-select custom-select-sm" style="min-width: 130px;">
                                  <option selected>Last Week</option>
                                  <option value="1">Today</option>
                                  <option value="2">Last Month</option>
                                  <option value="3">Last Year</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </h6>
              </div>
              <div class="card-body p-5 pb-3">
                <div id="chartdiv"></div>
                <br />
                <div id="show_user_chart"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="javascript:void(0);"> 
                        <i class="fa fa-chart-line"></i> Geographical Area Report
                    </a>

                    <div class="float-right">
                        <div class="row">
                            <div class="col-md-6">
                                <select onchange="fetchDataCapturedByRegion()" id="geo_area" class="custom-select custom-select-sm" style="min-width: 130px;">
                                    <option value="state" selected>Select Region</option>
                                    <option value="state">By State(s)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </h6>
              </div>
              <div class="card-body p-5 pb-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th>State</th>
                            <th>LGA(s)</th>
                            <th>Cluster(s)</th>
                        </tr>
                    </thead>
                    <tbody class="load_region_report_by_state"></tbody>
                </table>
                <div class="load_region_report_by_lga"></div>
                <div class="load_region_report_by_cluster"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="javascript:void(0);" class="text-danger"> 
                        <i class="fa fa-trash"></i> Danger Zone
                    </a>
                </h6>
              </div>
              <div class="card-body p-5 pb-3">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-danger" onclick="removeUserInformation()">
                            Delete Account
                        </button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <!-- Resources -->
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script type="text/javascript">
        // document on ready 
        loadUserInformation();
        showUserGraph();
        // fetchDataCapturedByRegion();

        // load user information
        function loadUserInformation() {
            // body...
            $.get('/admin/load/agent/'+{{ $agent_id }}, function(data) {
                // console.log(data);
                $(".load-information").html("");
                $(".load-information").append(`
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name: </label><br />
                            `+data.names+`
                        </div>

                        <div class="col-md-6">
                            <label>Email: </label><br />
                            `+data.email+`
                        </div>
                    </div>

                    <br /><br />
                    <table class="table">
                        <tr>
                            <td>Status</td>
                            <td><a class="text-success" href="">Active</a></td>
                        </tr>
                        <tr>
                            <td>Agent Code</td>
                            <td>`+data.agent_code.toUpperCase()+`</td>
                        </tr>
                    </table>
                    <br />


                    <p class="lead">Data Captured by ${data.names}</p>
                    <table class="table">
                        <tbody class="load-captured-users">
                            <tr>
                                <td>Farmers</td>
                                <td>${data.reg_users.farmers}</td>
                            </tr>
                            <tr>
                                <td>Services Providers</td>
                                <td>${data.reg_users.services}</td>
                            </tr>
                            <tr>
                                <td>Buyers</td>
                                <td>${data.reg_users.buyers}</td>
                            </tr>
                            <tr>
                                <td>Overall Captured Users</td>
                                <td>${data.reg_users.total}</td>
                            </tr>
                        </tbody>
                    </table>
                `);

                // assign default fields
                $("#names").val(data.names);
                $(".names").val(data.names);
                $("#email").val(data.email);
                $("#level").val(data.level);

                showCharts(data.reg_users);
                showUserGraph(data.reg_users);
            });
        }

        // update user information
        function updateAgentInformation() {
            // body...
            var names = $("#names").val();
            var email = $("#email").val();
            var level = $("#level").val();

            var data = {
                _token: '{{ csrf_token() }}',
                userid: '{{ $agent_id }}',
                names: names,
                email: email,
                level: level
            };

            $.post('/admin/update/agent-info', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                // console.log(data);
                if(data.status == 'success'){
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                    // refresh divs 
                    loadUserInformation();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }
            });

            // void form
            return false;
        }

        // delete user information
        function removeUserInformation() {

            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                // body...
                var token = '{{ csrf_token() }}';
                var agent_id = '{{ $agent_id }}';

                var params = {
                    _token: token,
                    agent_id: agent_id
                };

                $.post('{{ url('admin/delete/agent/user') }}', params, function(data, textStatus, xhr) {
                    if(data.status == "success"){
                        swal(
                            "Ok",
                            data.message,
                            data.status
                        );
                        window.location.href = '{{ url('admin/settings') }}';
                    }else{
                        swal(
                            "oops",
                            data.message,
                            data.status
                        );
                    }
                }); 
              }
            });
        }

        // show chart statistic
        function showCharts(series) {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);
            chart.scrollbarX = new am4core.Scrollbar();

            chart.data = [{
              "users": "Farmer",
              "total": series.farmers
            },{
              "users": "Services",
              "total": series.services
            }, {
              "users": "Buyers",
              "total": series.buyers
            }];

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "users";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.tooltip.disabled = true;
            categoryAxis.renderer.minHeight = 110;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "total";
            series.dataFields.categoryX = "users";
            series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

            // on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", (fill, target)=>{
              return chart.colors.getIndex(target.dataItem.index);
            })

            // Cursor
            chart.cursor = new am4charts.XYCursor();
        }

        function showUserGraph(series) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("show_user_chart", am4charts.XYChart);

            // Add data
            chart.data = [{
                "name": "Farmers",
                "points": series.farmers,
                "color": chart.colors.next(),
                "bullet": "{{ url('images/icon-set/farmer.png') }}"
            }, {
                "name": "Services",
                "points": series.services,
                "color": chart.colors.next(),
                "bullet": "{{ url('images/icon-set/tractor.png') }}"
            }, {
                "name": "Buyers",
                "points": series.buyers,
                "color": chart.colors.next(),
                "bullet": "{{ url('images/icon-set/produce.png') }}"
            }];

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "name";
            categoryAxis.renderer.grid.template.disabled = true;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.inside = true;
            categoryAxis.renderer.labels.template.fill = am4core.color("#fff");
            categoryAxis.renderer.labels.template.fontSize = 20;

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.strokeDasharray = "4,4";
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.min = 0;

            // Do not crop bullets
            chart.maskBullets = false;

            // Remove padding
            chart.paddingBottom = 0;

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "points";
            series.dataFields.categoryX = "name";
            series.columns.template.propertyFields.fill = "color";
            series.columns.template.propertyFields.stroke = "color";
            series.columns.template.column.cornerRadiusTopLeft = 15;
            series.columns.template.column.cornerRadiusTopRight = 15;
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/b]";

            // Add bullets
            var bullet = series.bullets.push(new am4charts.Bullet());
            var image = bullet.createChild(am4core.Image);
            image.horizontalCenter = "middle";
            image.verticalCenter = "bottom";
            image.dy = 20;
            image.y = am4core.percent(100);
            image.propertyFields.href = "bullet";
            image.tooltipText = series.columns.template.tooltipText;
            image.propertyFields.fill = "color";
            image.filters.push(new am4core.DropShadowFilter());
        }

        function fetchDataCaptured() {
            var update_report   = $("#update_report").val();
            var agent_id        = '{{ $agent_id }}';
            var params = {
                update_report: update_report,
                agent_id: agent_id
            }

            $.get('{{url('admin/get/agent/update/report')}}', params, function(data) {
                // console.log(data);
                showUserGraph(data);
            });
        }

        function fetchDataCapturedByRegion() {
            var geo_area = $("#geo_area").val();
            var agent_id = '{{ $agent_id }}';
            var params = {
                geo_area: geo_area,
                agent_id: agent_id
            }
            $.get('{{url("admin/get/agent/region/report")}}', params, function(data) {
                $(".load_region_report_by_state").html("");
                $.each(data, function(index, val) {
                    getRegionLgas(val.state);
                    $(".load_region_report_by_state").append(`
                        <tr>
                            <td>${val.state} - ${val.state_total}</td>
                            <td>
                                <table class="table">
                                    <tbody class="load-${val.state}-lgas">
                                        <tr>
                                            <td>Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table">
                                    <tbody class="load-${val.state}-clusters">
                                        <tr>
                                            <td>Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        <tr>
                    `);
                });
            });
        }

        function getRegionLgas(state) {
            var agent_id = '{{ $agent_id }}';
            $.get('{{url("admin/get/region/report/callback")}}', {states: state, agent_id: agent_id}, function(data) {
                // console.log(data);
                $(`.load-${state}-lgas`).html("");
                $.each(data, function(index, val) {
                    $(`.load-${state}-lgas`).append(`
                        <tr>
                            <td>${val.lga}</td>
                            <td>${val.lga_total}</td>
                        </tr>
                    `);
                });
            });
        }
    </script>
@endsection