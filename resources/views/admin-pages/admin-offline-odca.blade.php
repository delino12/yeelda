@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | ODCA
@endsection

{{-- Contents  --}}
@section('contents')
    <style>
      #chartdiv {
        width: 100%;
        height: 290px;
      }

      tspan {
          color:#FFF;
      }                    
    </style>

    @include('components.sticky-menu')
    
    <!-- / .main-navbar -->
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Yeelda Offline Data Capture (ODCA)</span>
          </div>
        </div>
        <!-- End Page Header -->
        
        <!-- Default Light Table -->
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0">
                    Capture new user

                    <a href="javascript:void(0);" class="btn btn-primary float-right" onclick="showCaptureForm()">
                        <i class="fa fa-plus"></i> Add 
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3" id="capture-div" style="display: none;">
                <h1 class="lead">Capture User Data</h1><hr />
                <form class="post-form" id="capture-form" method="post" onsubmit="return createUserByAdmin()">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="image-preview">
                                    <img src="/images/profile-image.png" width="100" height="auto">
                                </div>
                                <label class="dropzone-label">Add User Image</label>
                                <div class="dropzone"></div>
                                <input type="hidden" id="avatar" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>First name</label>
                                <input type="text" class="form-control" id="firstname" placeholder="Enter firstname" required="">
                            </div>

                            <div class="col-sm-6">
                                <label>Last name</label>
                                <input type="text" class="form-control" id="lastname" placeholder="Enter lastname" required="">
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Email</label>
                                <input type="email" id="email" class="form-control" placeholder="example@domain.com">
                            </div>

                             <div class="col-sm-6">
                                <label>Phone number</label>
                                <input type="text" id="mobile" required="" class="form-control" placeholder="0803000*****" maxlength="11">
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>State</label>
                                <select id="state" onchange="onSelectLga()" class="form-control y-select-state">
                                    <option value="">-select-</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label>LGA</label>
                                <select id="lga" class="form-control y-select-lga">
                                    <option value="">-select-</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label>Ward</label>
                                <select id="ward" class="form-control">
                                    <option value="">-select-</option>
                                    <option value="ward I">Ward I</option>
                                    <option value="ward II">Ward II</option>
                                    <option value="ward III">Ward III</option>
                                    <option value="ward IV">Ward IV</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br />

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Prefered Language</label>
                                <select id="language" class="form-control">
                                    <option value="english">English</option>
                                    <option value="yoruba">Yoruba</option>
                                    <option value="hausa">Hausa</option>
                                    <option value="igbo">Igbo</option>
                                </select>
                            </div>

                            <div class="col-sm-6">
                                <label>Signup As</label>
                                <select class="form-control" onchange="onSelectAccType()" id="accountType">
                                    <option value="">-select-</option>
                                    <option value="farmer">As a Farmer</option>
                                    <option value="farmer">As a Farmer and Services Provider</option>
                                    <option value="service">As a Service Provider</option>
                                    <option value="buyer">As a Buyer</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br />

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Address</label>
                                <textarea id="address" class="form-control" rows="4" cols="7" placeholder="Type address here"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-group" id="servicesDivs" style="display: none;">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Storage Capacity</label>
                                <input type="text" class="form-control" placeholder="Storage Capacity Level" id="storageCapicity">
                            </div>
                            <div class="col-sm-6">
                                <label>Type of storage</label>
                                <input type="text" class="form-control" placeholder="Type of produce stored " id="typeOfStroage">
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="farmersDivs" style="display: none;">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Farm Size</label>
                                <input type="number" class="form-control" step="any" min="1" id="farmSizeNo" placeholder="eg. 10">
                            </div>

                            <div class="col-sm-3">
                                <label>Farm Type</label>
                                <select id="farmSizetype" class="form-control">
                                    <option value="plot">Plot</option>
                                    <option value="hectare">Hectare</option>
                                    <option value="acre">Acre</option>
                                    <option value="greenhouse">Greenhouse(s)</option>
                                </select>
                            </div>

                            <div class="col-sm-6">
                                <label>Crops</label>
                                <input type="text" id="farmSizeCrop" class="form-control" placeholder="Eg: Rice, Cassava, Yam, Vegetales and etc">
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="buyersDivs" style="display: none;">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>What is your company name ?</label>
                                <input type="text" class="form-control" id="companyName" placeholder="Company name">
                            </div>

                            <div class="col-sm-6">
                                <label>What you will like to purchase ? </label>
                                <input type="text" class="form-control" id="comPerferPurchase" placeholder="Eg: Rice, Cassava, Yam, Vegetales and etc">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary">
                            <img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading">
                            Save and Continue
                        </button>
                    </div>
                    
                    <div class="success_msg"></div>
                    <div class="error_msg"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->

        <!-- Default Light Table -->
        <div class="row">
          <div class="col">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0">Farming equipment</h6>
              </div>
              <div class="card-body p-4 pb-3">
                <h1 class="lead">List of capture users</h1><hr />
                <span class="sync-state pull-right"></span>
                <table class="table" id="temp_yeelda_users" data-pagination="true" data-search="true"data-page-size="10">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Names</th>
                            <th>Phone</th>
                            <th>State</th>
                            <th>LGA.</th>
                            <th>Type</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="load-all-users">
                        <tr>
                            <td><img src="/svg/yeelda-loading.svg" height="60" width="auto" /></td>
                        </tr>
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript" src="/dropzone/js/dropzone.js"></script>
    <script type="text/javascript">
        $(".dropzone").dropzone({
          url: "/admin/upload/captured/images",
          params: {
              _token:'{{ csrf_token() }}'
          },
          success: function(e, res) {
              // body...
              // console.log(e.status);
              // console.log(res);
              $("#avatar").val(res);
              $(".image-preview").html(`
                  <img src="/images/captured-images/`+res+`" width="120" height="120">
                  <a href="javascript:void(0);" onclick="removeImage('`+res+`')"><i class="fa fa-times"></i> Remove image</a>
              `);

              if(e.status == 'success'){
                  // hide upload image
                  $(".dropzone").hide();
                  $(".dropzone-label").hide();
              }
          }
        });

        // cancel or remove image
        function removeImage(image_name) {
        // body...
        var token    = '{{ csrf_token() }}';
        var filename = image_name;

        // data to json
        var data = {
            _token: token,
            filename:filename
        }

        // post data
        $.post('/admin/remove/image', data, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            // console.log(data);
            if(data.status == 'success'){
                window.location.reload();
            }
        });
        }

        // upload register client information
        function createUserByAdmin() {
            $(".loading").show();
            // pass for post request
            var token = '{{ csrf_token() }}';

            var firstname   = $("#firstname").val();
            var lastname    = $("#lastname").val();
            var email       = $("#email").val();
            var phone       = $("#mobile").val();
            var avatar      = $("#avatar").val();
            var state       = $("#state").val();
            var lga         = $("#lga").val();
            var ward        = $("#ward").val();
            var language    = $("#language").val();
            var accountType = $("#accountType").val();
            var address     = $("#address").val();

            if(accountType == ''){
                alert("Please specify user account signup Type");
                return false;
            }

            // for farmers
            var farmSizeNo      = $("#farmSizeNo").val();
            var farmSizetype    = $("#farmSizetype").val();
            var farmSizeCrop    = $("#farmSizeCrop").val();

            // for buyers
            var storageCapicity = $("#storageCapicity").val();;
            var typeOfStroage   = $("#typeOfStroage").val();;

            // for services providers
            var companyName        = $("#companyName").val();
            var comPerferPurchase  = $("#comPerferPurchase").val();

            // data to json
            var data = {
                _token:token,
                firstname:firstname,
                lastname:lastname,
                email:email,
                phone:phone,
                avatar:avatar,
                state:state,
                lga:lga,
                ward:ward,
                language:language,
                accountType:accountType,
                address:address,
                farmSizeNo:farmSizeNo,
                farmSizetype:farmSizetype,
                farmSizeCrop:farmSizeCrop,
                storageCapicity:storageCapicity,
                typeOfStroage:typeOfStroage,
                companyName:companyName,
                comPerferPurchase:comPerferPurchase
            };

            // save data 
            $.post('/admin/create/temp/user', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                // console.log(data);
                if(data.status == 'success'){
                    // window.location.reload();
                    // reset form
                    $("#capture-form")[0].reset();
                    window.location.reload();
                    // loadAllUsersData();
                }
                if(data.status == 'error'){
                    $(".loading").hide();
                    alert(data.message);
                    $(".loading").hide();
                }
            });
            // void form
            return false;
        }

        // on select LGA
        function onSelectLga() {
            // body...
            var state = $("#state").val();
            $(".y-select-lga").html();
            $.get('https://locationsng-api.herokuapp.com/api/v1/states/'+state+'/lgas', function(data) {
                /*optional stuff to do after success */
                // console.log(data);
                $(".y-select-lga").html("");
                $.each(data, function(index, val) {
                    $(".y-select-lga").append(`
                       <option value="`+val+`">`+val+`</option>
                    `);
                });
            });
        }

        // on select Account Type
        function onSelectAccType(argument) {
            // body...
            var accountType = $("#accountType").val();
            if(accountType == 'farmer'){
                $("#farmersDivs").show();
                $("#servicesDivs").hide();
                $("#buyersDivs").hide();
            }

            if(accountType == 'service'){
                $("#servicesDivs").show();
                $("#farmersDivs").hide();
                $("#buyersDivs").hide();
            }

            if(accountType == 'buyer'){
                $("#buyersDivs").show();
                $("#farmersDivs").hide();
                $("#servicesDivs").hide();
            }
        }

        // delete user
        function deleteUser(id, accountType) {
            // body...
            // alert(id+accountType);
            var token = '{{ csrf_token() }}';

            // data to json
            var data = {
                _token: token,
                userid:id,
                accountType:accountType
            };

            // post data for delete
            $.post('/admin/delete/user', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                // console.log(data);
                if(data.status == 'success'){
                    // loadAllUsersData();
                    window.location.reload();
                }
            });
        }

        // load farmers data
        function loadAllUsersData() {
            // load all temp users
            $(".load-all-users").html("");
            $.get('/load/all/temp/user', function(data) {
              /*optional stuff to do after success */
              // console.log(data);
              var sn = 0;
              $.each(data.farmers, function(index, val) {
                  sn++;
                  // console.log(val);
                  $(".load-all-users").append(`
                      <tr>
                          <td>`+sn+`</td>
                          <td>`+val.names+`</td>
                          <td>`+val.phone+`</td>
                          <td>`+val.state+`</td>
                          <td>`+val.lga+`</td>
                          <td>`+val.accountType+`</td>
                          <td>
                              <a href="/view/`+val.accountType+`/`+val.id+`" style="margin-left: 15px;">
                                  <i class="material-icons">account_box</i> 
                                  View
                              </a> 
                          </td>
                          <td>
                              <a href="javascript:void(0);" style="margin-left: 15px;" onclick="syncUser('`+sn+`', '`+val.id+`', '`+val.accountType+`')">
                                  <i id="sync-icon-${sn}" class="material-icons">sync</i>
                                  <span class="process-state-${sn}">Sync</span>
                              </a>
                          </td>
                          <td> 
                              <a href="javascript:void(0);" style="margin-left: 15px;" onclick="deleteUser('`+val.id+`', '`+val.accountType+`')">
                                <i class="material-icons">restore_from_trash</i>
                                  Delete
                              </a>
                          </td>
                      </tr>
                  `);
              });

              $.each(data.service, function(index, val) {
                  sn++;
                  $(".load-all-users").append(`
                      <tr>
                          <td>`+sn+`</td>
                          <td>`+val.names+`</td>
                          <td>`+val.phone+`</td>
                          <td>`+val.state+`</td>
                          <td>`+val.lga+`</td>
                          <td>`+val.accountType+`</td>
                          <td>
                              <a href="/view/`+val.accountType+`/`+val.id+`" style="margin-left: 15px;">
                                  <i class="material-icons">account_box</i> 
                                  View
                              </a> 
                          </td>
                          <td>
                              <a href="javascript:void(0);" style="margin-left: 15px;" onclick="syncUser('`+sn+`', '`+val.id+`', '`+val.accountType+`')">
                                  <i id="sync-icon-${sn}" class="material-icons">sync</i>

                                  Sync
                              </a>
                          </td>
                          <td> 
                              <a href="javascript:void(0);" style="margin-left: 15px;" onclick="deleteUser('`+val.id+`', '`+val.accountType+`')">
                                <i class="material-icons">restore_from_trash</i>
                                  Delete
                              </a>
                          </td>
                      </tr>
                  `);
              });

              $.each(data.buyer, function(index, val) {
                  sn++;
                  $(".load-all-users").append(`
                      <tr>
                          <td>`+sn+`</td>
                          <td>`+val.names+`</td>
                          <td>`+val.phone+`</td>
                          <td>`+val.state+`</td>
                          <td>`+val.lga+`</td>
                          <td>`+val.accountType+`</td>
                          <td>
                              <a href="/view/`+val.accountType+`/`+val.id+`" style="margin-left: 15px;">
                                  <i class="material-icons">account_box</i> 
                                  View
                              </a> 
                          </td>
                          <td>
                              <a href="javascript:void(0);" style="margin-left: 15px;" onclick="syncUser('`+sn+`', '`+val.id+`', '`+val.accountType+`')">
                                  <i id="sync-icon-${sn}" class="material-icons">sync</i>

                                  Sync
                              </a>
                          </td>
                          <td> 
                              <a href="javascript:void(0);" style="margin-left: 15px;" onclick="deleteUser('`+val.id+`', '`+val.accountType+`')">
                                <i class="material-icons">restore_from_trash</i>
                                  Delete
                              </a>
                          </td>
                      </tr>
                  `);
              });

              $('#temp_yeelda_users').bootstrapTable();
            });
        }

        // load state and capital data
        function loadStateCapitalData() {
          // populate select option with states  
          $.get('https://locationsng-api.herokuapp.com/api/v1/states', function(data) {
              // console.log(data);
              $(".y-select-state").html("");
              $.each(data, function(index, val) {
                  $(".y-select-state").append(`
                     <option value="`+val.name+`">`+val.name+`</option>
                  `);
                  // console.log(val);
              });
          });   
        }

        // synchronized users account
        function syncUser(sn, id, accountType) {
          $(".process-state-"+sn).html('Synching...').fadeIn();
          var token   = '{{ csrf_token() }}';
          
          // data to json
          var data = {
              _token: token,
              userid: id,
              accountType: accountType
          };

          // post to endpoint
          $.post('{{url('admin/sync/user/account')}}', data, function(data, textStatus, xhr) {
              // console.log(data);
              if(data.status == 'success'){
                swal(
                  "success",
                  data.message,
                  "success"
                );
                // window.location.reload();
                $(".process-state-"+sn).html('successful!');
                $('#temp_yeelda_users').bootstrapTable().destroy();
                loadAllUsersData();
              }

              if(data.status == 'error'){
                swal(
                  "error",
                  data.message,
                  "error"
                );
                $(".process-state-"+sn).html('Sync');
              }

              setTimeout((e) => {
                 $("#sync-icon").removeClass('loading-sync'); 
              }, 3000);
              
          }).fail(function (err){
              console.log(err);
              console.log("Fail to send post request !");
              $("#sync-icon").removeClass('loading-sync');
          });
        }

        function showCaptureForm() {
            $("#capture-div").toggle();
        }

        loadStateCapitalData();
        loadAllUsersData();
    </script>
@endsection