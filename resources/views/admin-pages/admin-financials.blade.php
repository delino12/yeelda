@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | Financials
@endsection

{{-- Contents  --}}
@section('contents')
  <style>
    #chartdiv {
      width: 100%;
      height: 290px;
    }

    #product_chart_div {
      width: 100%;
      height: 390px;
    }           
  </style>

  @include('components.sticky-menu')
  
  <!-- / .main-navbar -->
  <div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Financials</span>
      </div>
    </div>
    
    <div class="row">
      <div class="col">
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">Pending transactions</h6>
          </div>
          <div class="card-body p-0 pb-3 text-center">
            <table class="table mb-0">
              <thead class="bg-light">
                <tr>
                  <th scope="col" class="border-0">#</th>
                  <th scope="col" class="border-0">Buyer Name</th>
                  <th scope="col" class="border-0">Buyer Phone</th>
                  <th scope="col" class="border-0">Seller Email</th>
                  <th scope="col" class="border-0">Tran. Ref</th>
                  <th scope="col" class="border-0">Amount(&#8358;)</th>
                  <th scope="col" class="border-0">Status</th>
                  <th scope="col" class="border-0">Date</th>
                  <th scope="col" class="border-0">Action</th>
                </tr>
              </thead>
              <tbody class="load-payment-list">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="card card-small overflow-hidden mb-4">
          <div class="card-header bg-dark">
            <h6 class="m-0 text-white">Successul transactions</h6>
          </div>
          <div class="card-body p-0 pb-3 bg-dark text-center">
            <table class="table table-dark mb-0">
              <thead class="thead-dark">
                <tr>
                  <th scope="col" class="border-bottom-0">#</th>
                  <th scope="col" class="border-bottom-0">Buyer Name</th>
                  <th scope="col" class="border-bottom-0">Buyer Phone</th>
                  <th scope="col" class="border-bottom-0">Seller Email</th>
                  <th scope="col" class="border-bottom-0">Tran. Ref</th>
                  <th scope="col" class="border-bottom-0">Amount(&#8358;)</th>
                  <th scope="col" class="border-bottom-0">Status</th>
                  <th scope="col" class="border-bottom-0">Last updated</th>
                  <th scope="col" class="border-bottom-0">Action</th>
                </tr>
              </thead>
              <tbody class="load-payment-paid">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Farm Produce</span>
                <h6 class="my-3" id="tfc"></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Farm Equipment</span>
                <h6 class="my-3" id="tfe"></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Farm Services</span>
                <h6 class="my-3" id="tfs"></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Highest Sold (Farm Produce)</span>
                <h6 class="my-3" id="tpp"></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-12 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Lowest Sold (Farm Produce)</span>
                <h6 class="my-3" id="lpp"></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">Highest and Lowest selling Farm Inputs</h6>
          </div>
          <div class="card-body p-3 pb-3">
            <table class="table small mb-0" id="rank-services-table">
              <thead class="bg-light">
                <tr>
                  <th scope="col" class="border-0">Owner</th>
                  <th scope="col" class="border-0">Qty.</th>
                  <th scope="col" class="border-0">Amount.</th>
                  <th scope="col" class="border-0">Produce</th>
                </tr>
              </thead>
              <tbody class="rank-by-services">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        {{-- Equipment Ranking Section --}}
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">Highest and Lowest selling Equipment</h6>
          </div>
          <div class="card-body p-3 pb-3">
            <table class="table small mb-0" id="rank-equipment-table">
              <thead class="bg-light">
                <tr>
                  <th scope="col" class="border-0">Owner</th>
                  <th scope="col" class="border-0">Qty.</th>
                  <th scope="col" class="border-0">Amount.</th>
                  <th scope="col" class="border-0">Produce</th>
                </tr>
              </thead>
              <tbody class="rank-by-equipment">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">Total Sold Produces</h6>
          </div>
          <div class="card-body p-3 pb-3">
            <div id="produce_chart_div"></div>
          </div>
        </div>

      </div>
      <div class="col-md-4">
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">Total Sold Produces</h6>
          </div>
          <div class="card-body p-3 pb-3">
            <table class="table small mb-0" id="total-sold-table">
              <thead class="bg-light">
                <tr>
                  <th scope="col" class="border-0">Category</th>
                  <th scope="col" class="border-0">Total</th>
                </tr>
              </thead>
              <tbody class="filtered-total-produces">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              <div class="col">
                <select class="custom-select custom-select-sm" id="scan-ts-bydate" onchange="totalAmountSold()" style="max-width: 130px;">
                  <option value="0" selected>Today</option>
                  <option value="1">Last Week</option>
                  <option value="2">Last Month</option>
                  <option value="3">Last Year</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        {{-- Produce Ranking Section --}}
        <div class="card card-small mb-4">
          <div class="card-header border-bottom">
            <h6 class="m-0">Highest and Lowest selling farm Produce</h6>
          </div>
          <div class="card-body p-3 pb-3">
            <table class="table small mb-0" id="rank-produce-table">
              <thead class="bg-light">
                <tr>
                  <th scope="col" class="border-0">Owner</th>
                  <th scope="col" class="border-0">Qty.</th>
                  <th scope="col" class="border-0">Amount.</th>
                  <th scope="col" class="border-0">Produce</th>
                </tr>
              </thead>
              <tbody class="rank-by-produces">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

{{-- Scripts --}}
@section('scripts')
  <script src="https://www.amcharts.com/lib/4/core.js"></script>
  <script src="https://www.amcharts.com/lib/4/charts.js"></script>
  {{-- <script src="https://www.amcharts.com/lib/4/maps.js"></script> --}}
  <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
  <script type="text/javascript">
    // load users list 
    loadPaymentRequest();
    loadPaymentCompleted();
    loadProducts();
    loadEquipment();
    loadServices();
    loadFinancialStats();
    totalAmountSold();
    producesReports();

    // load payment request
    function loadPaymentRequest() {
      $.get('/load/payment/request', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $('.load-payment-list').html("");
        var sn = 0;
        $.each(data, function(index, val) {
            /* iterate through array or object */
            let approve_btn;
            if(val.status == 'approved'){
              approve_btn = `
                <a class="p-2 text-success" href="/admin/settle/payment/${val.id}">
                  <i class="fa fa-check-circle"></i> Approved
                </a>
              `;
            }else{
              approve_btn = `
                <a class="p-2" href="javascript:void(0);">
                  <i class="fa fa-edit"></i> Pending
                </a>
              `;
            }

            sn++;
            if(val.status !== 'settle'){
                $('.load-payment-list').append(`
                    <tr>
                        <td>`+sn+`</td>
                        <td>`+val.name+`</td>
                        <td>`+val.phone+`</td>
                        <td>`+val.seller+`</td>
                        <td>`+val.pay_id+`</td>
                        <td>&#8358;`+val.amount+`</td>
                        <td>`+val.status+`</td>
                        <td>`+val.date+`</td>
                        <td>
                          ${approve_btn}
                          <a class="p-2" href="javascript:void(0);"><i class="fa fa-envelope"></i> Mail</a>
                        </td>
                    </tr>
                `);
            }
        });
      });
    }

    // load payment completed
    function loadPaymentCompleted() {
      $.get('/load/payment/paid', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $('.load-payment-paid').html("");
        var sn = 0;
        $.each(data, function(index, val) {
            /* iterate through array or object */
            // console log data
            // console.log(val);
            sn++;
            $('.load-payment-paid').append(`
                <tr>
                    <td>`+sn+`</td>
                    <td>`+val.name+`</td>
                    <td>`+val.phone+`</td>
                    <td>`+val.seller+`</td>
                    <td>`+val.pay_ref+`</td>
                    <td>&#8358;`+val.amount+`</td>
                    <td>success</td>
                    <td>`+val.date+`</td>
                    <td>
                        <a class="p-4" href="/admin/mail/payment/`+val.id+`"><i class="fa fa-envelope"></i> Mail</a>
                    </td>
                </tr>
            `);
        });
        $('.total-payment-successful').html('Total: '+data.length);
      });
    }

    // load farm products
    function loadProducts() {
      // body...
      $.get('/admin/load-products', function (data){
        /* load url json response */
        // console.log(data);
        $(".all-produces").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          /* iterate through array or object */
          sn++;
          $(".all-produces").append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.owner+`</td>
              <td>`+val.name+`</td>
              <td>&#8358; `+val.amount+`</td>
              <td>
                <a href="javascript:void();" onclick="deleteItem(`+val.id+`)">
                  <i class="fa fa-trash"></i> Delete
                </a>
              </td>
              <td>
                <a href="/admin/view-products/`+val.id+`">
                  <i class="fa fa-copy"></i> Details
                </a>
              </td>
            </tr>
          `);
        });
      });
    }

    // load equipments
    function loadEquipment() {
      $.get('/admin/load-equipment', function (data){
        /* load url json response */
        $('.all-inputs').html("");
        var sn = 0;
        $.each(data, function(index, val) {
          /* iterate through array or object */
          // console log data
          // console.log(val);
          sn++;
          $('.all-inputs').append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.owner+`</td>
              <td>`+val.phone+`</td>
              <td>`+val.name+`</td>
              <td>&#8358; `+val.amount+`</td>
              <td>
                <a href="javascript:void();" onclick="deleteEquipmentItem(`+val.id+`)">
                  <i class="fa fa-trash"></i> Delete
                </a>
              </td>
              <td>
                <a href="/admin/view-equipment/`+val.id+`">
                  <i class="fa fa-copy"></i> Details
                </a>
              </td>
            </tr>
          `);
        });
      });
    }

    // load farm services
    function loadServices() {
      // body...
      $.get('{{url('admin/load-farm-services')}}', function(data) {
        $(".load-services").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          console.log(val);
          sn++;
          $(".load-services").append(`
            <tr>
              <td>${sn}</td>
              <td>${val.owner}</td>
              <td>${val.phone}</td>
              <td>${val.name}</td>
              <td>${val.amount}</td>
              <td>
                <a href="javascript:void();" onclick="deleteFarmServices('${val.id}', '${val.type}')">
                  <i class="fa fa-trash"></i> Delete
                </a>
              </td>
              <td>
                <a href="/admin/view-equipment/`+val.id+`">
                  <i class="fa fa-copy"></i> Details
                </a>
              </td>
            </tr>
          `);
        });
      });
    }
    
    // delete equipment
    function deleteItem(x) {
      // body...
      $.get('/admin/delete-products/'+x, function (data){
        /* load url json response */
        // console.log(data);
        if(data.status == 'success'){
          alert('item deleted !');
          window.location.reload();
        }
      });
    }

    // delete equipment
    function deleteEquipmentItem(x) {
      // body...
      $.get('/admin/delete-equipment/'+x, function (data){
        /* load url json response */
        // console.log(data);
        if(data.status == 'success'){
          loadEquipment();
        }
      });
    }

    // delete services
    function deleteFarmServices(itemid, type) {
      // body...
      $.get('/admin/delete-services/'+itemid+'/'+type, function (data){
        /* load url json response */
        // console.log(data);
        if(data.status == 'success'){
          alert('item deleted !');
          window.location.reload();
        }
      });
    }

    // financial stats
    function loadFinancialStats() {
      $.get('{{url('admin/load/financial-statistic')}}', function(data) {
        /*optional stuff to do after success */
        $("#tfc").html(`&#8358;`+data.total_farm_cost);
        $("#tfe").html(`&#8358;`+data.total_equip_cost);
        $("#tfs").html(`&#8358;`+data.total_service_cost);
        $("#tpp").html(`${data.highest_sold_produce.produce}`);
        $("#lpp").html(`${data.lowest_sold_produce.produce}`);

        $(".rank-by-produces").html("");
        $.each(data.sales_statistics, function(index, val) {
          $(".rank-by-produces").append(`
            <tr>
              <td>${val.owner}</td>
              <td>${val.qty}</td>
              <td>&#8358; ${val.amount}</td>
              <td>${val.produce}</td>
            </tr>
          `);
        });

        $("#rank-produce-table").DataTable({
          responsive: true,
          paging: false,
          searching: false
        });
      });
    }

    // total sold amount
    function totalAmountSold() {
      var interval = $("#scan-ts-bydate").val();
      $.get('{{url('admin/load/total/soldout')}}/'+interval, function(data) {
        // console.log(data);
        $(".filtered-total-produces").html(`
          <tr>
            <td>Farm Produces</td>
            <td>&#8358;${data.total_transaction}</td>
          </tr>
          <tr>
            <td>Farm Fertilizers</td>
            <td>&#8358;${data.total_fert}</td>
          </tr>
          <tr>
            <td>Farm Seeds</td>
            <td>&#8358;${data.total_seeds}</td>
          </tr>
          <tr>
            <td>Farm Equipment Hired</td>
            <td>&#8358;${data.total_equip}</td>
          </tr>
        `);
      });
    }

    function producesReports() {
      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      // Create chart instance
      var chart = am4core.create("produce_chart_div", am4charts.XYChart);

      // Add data
      chart.data = [{
        "produce": "Rice",
        "amount": 2025
      }, {
        "produce": "Maize",
        "amount": 1882
      }, {
        "produce": "Corn",
        "amount": 1809
      }, {
        "produce": "Beans",
        "amount": 1322
      }];

      // Create axes

      var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "produce";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 30;

      categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
        if (target.dataItem && target.dataItem.index & 2 == 2) {
          return dy + 25;
        }
        return dy;
      });

      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "amount";
      series.dataFields.categoryX = "produce";
      series.name = "Amount";
      series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
      series.columns.template.fillOpacity = .8;

      var columnTemplate = series.columns.template;
      columnTemplate.strokeWidth = 2;
      columnTemplate.strokeOpacity = 1;
    }
  </script>
  <!-- Chart code -->
@endsection