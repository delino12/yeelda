@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | Notifications
@endsection

{{-- Contents  --}}
@section('contents')
<style type="text/css">
    .y-btn {
        border:1px solid #FFF;
        border-radius: 4px;
        padding: 0.5em;
        margin: 0.5em;
        text-decoration: none;
    }

    #chartdiv {
        width: 100%;
        height: 290px;
    }
</style>
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Notifications</span>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="lead"><img src="/images/icon-set/farm-band.png"> Users Statistic</h1>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Details</th>
                                    <th>Counts</th>
                                </tr>
                            </thead>
                            <tbody class="users-statistic"></tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>

            <br /><br />
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    Group Request
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                   <div class="col-md-12">
                        <h1 class="lead"><img src="/images/icon-set/farm-band.png"> Association</h1>
                        <div class="info_msg"></div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Groups</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="load-association-request"></tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    Bar Chart
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div id="chartdiv"></div>
              </div>
            </div>
          </div>
        </div>

        <div style="height: 200px;"></div>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="javascript:void(0);"> 
                        Chat History
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0);" class="btn btn-info" onclick="clearChatHistory()">
                            Clear Chat History
                        </a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="javascript:void(0);"> 
                      Fixed Wrong Keywords
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                  <div class="col-md-12">
                    <a href="javascript:void(0);" class="btn btn-info" onclick="searchReplaceModal()">
                      <i class="fa fa-search"></i> Search & Replace
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="javascript:void(0);"> 
                      Fixed Wrong Keywords for States
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                  <div class="col-md-12">
                    <a href="javascript:void(0);" class="btn btn-info" onclick="searchReplaceModal2()">
                      <i class="fa fa-search"></i> Search & Replace
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    @include('admin-components.modals')
@endsection

@section('scripts')
  <script src="{{ asset('amcharts/js/core.js') }}"></script>
  <script src="{{ asset('amcharts/js/charts.js') }}"></script>
  <script src="{{ asset('amcharts/js/animated.js') }}"></script>
  <script type="text/javascript">
    loadUsersService();
    loadUsersStatistics();
    getAllStates();
    
    function loadUsersService() {
      $.get('{{url("load/users/yeelda/service")}}', function(data) {
        var sn = 0;
        $.each(data, function(index, val) {
            sn++;
            if(val.lock == 'locked'){
                var status = `<a href="javascript:void(0);" onclick="unblockUser('`+val.email+`')">unblock</a>`;
            }else if(val.lock == null){
                
                var status = `<a href="javascript:void(0);" onclick="blockUser('`+val.email+`')">block</a>`;
            }

            $(".list-service").append(`
                <tr>
                    <td>`+sn+`</td>
                    <td>`+val.name+`</td>
                    <td>`+val.email+`</td>
                    <td>`+val.date+`</td>
                    <td>
                        <a href="/admin/users/profile/`+val.id+`">view </a> - 
                        `+status+`
                    </td>  
                </tr>
            `);
        });
      });   
    }

    function loadChartData(series) {
      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      // Create chart instance
      var chart = am4core.create("chartdiv", am4charts.XYChart3D);

      // Add data
      chart.data = [
        {
          "users": "Farmer",
          "total": series.total_farmers
        },
        {
          "users": "Service",
          "total": series.total_services
        },
        {
          "users": "Buyers",
          "total": series.total_buyers
        },
      ];

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "users";
      categoryAxis.renderer.labels.template.rotation = 270;
      categoryAxis.renderer.labels.template.hideOversized = false;
      categoryAxis.renderer.minGridDistance = 20;
      categoryAxis.renderer.labels.template.horizontalCenter = "right";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      categoryAxis.tooltip.label.rotation = 270;
      categoryAxis.tooltip.label.horizontalCenter = "right";
      categoryAxis.tooltip.label.verticalCenter = "middle";

      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.title.text = "Users";
      valueAxis.title.fontWeight = "bold";

      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries3D());
      series.dataFields.valueY = "total";
      series.dataFields.categoryX = "users";
      series.name = "Visits";
      series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
      series.columns.template.fillOpacity = .8;

      var columnTemplate = series.columns.template;
      columnTemplate.strokeWidth = 2;
      columnTemplate.strokeOpacity = 1;
      columnTemplate.stroke = am4core.color("#FFFFFF");

      columnTemplate.adapter.add("fill", (fill, target) => {
        return chart.colors.getIndex(target.dataItem.index);
      })

      columnTemplate.adapter.add("stroke", (stroke, target) => {
        return chart.colors.getIndex(target.dataItem.index);
      })

      chart.cursor = new am4charts.XYCursor();
      chart.cursor.lineX.strokeOpacity = 0;
      chart.cursor.lineY.strokeOpacity = 0;
    }
    
    function loadUsersStatistics() {
      $.get('{{url('load/charts/statistic')}}', function(data) {
        $(".users-statistic").html(`
            <tr>
                <td>Total Farmers</td>
                <td>${data.total_farmers}</td>
                <td>${data.temp_farmers}</td>
            </tr>

            <tr>
                <td>Total Services Providers</td>
                <td>${data.total_services}</td>
                <td>${data.temp_services}</td>
            </tr>

            <tr>
                <td>Total Buyers</td>
                <td>${data.total_buyers}</td>
                <td>${data.temp_buyers}</td>
            </tr>

            <tr>
                <td>Total Users</td>
                <td>${data.total_overall}</td>
                <td>${data.temp_overall}</td>
            </tr>
        `);

        loadChartData(data);
      });
    }

    function clearChatHistory() {
        var token = $("#token").val();
        $.post('{{ url("delete/general/chat/messages") }}', {_token: token},  function(data, textStatus, xhr) {
          if(data.status == "success"){
            swal(
              "Ok",
              data.message,
              data.status
            );
          }
        });
    }

    function searchReplaceModal() {
      // show search and find modal
      $("#show-search-replace-modal-errors").modal();
    }

    function searchReplaceModal2() {
      // show search and find modal
      $("#show-search-replace-modal-state").modal();
    }

    function getAllStates() {
      $.get('{{url("admin/get/all/states")}}', function(data) {
          $("#sort_state").html("");
          $("#c_sort_state").html("");
          $("#s_sort_state_2").html("");

          $("#sort_state").append(`<option value="">select state</option>`);
          $("#c_sort_state").append(`<option value="">select state</option>`);

          $.each(data, function(index, val) {
            $("#sort_state").append(`<option value="${val.name}">${val.name}</option>`);
            $("#c_sort_state").append(`<option value="${val.name}">${val.name}</option>`);
            $("#s_sort_state_2").append(`<option value="${val.name}">${val.name}</option>`);
          });
      });
    }

    function getLgas() {
      // body...
      var states = $("#sort_state").val();
      var params = {states: states};
      $.get('{{url("admin/get/state/lgas")}}', params, function(data) {
        $("#sort_lgas").html("");
        $.each(data, function(index, val) {
          $("#sort_lgas").append(`<option value="${val}">${val}</option>`);
        });
      });
    }

    function getLgas2() {
      // body...
      var states = $("#c_sort_state").val();
      var params = {states: states};
      $.get('{{url("admin/get/state/lgas")}}', params, function(data) {
        $("#c_sort_lgas").html("");
        $.each(data, function(index, val) {
          $("#c_sort_lgas").append(`<option value="${val}">${val}</option>`);
        });
      });
    }

    function getLgas3() {
      // body...
      var states = $("#s_sort_state_2").val();
      var params = {states: states};
      $.get('{{url("admin/get/state/lgas")}}', params, function(data) {
        $("#s_sort_lgas_2").html("");
        $.each(data, function(index, val) {
          $("#s_sort_lgas_2").append(`<option value="${val}">${val}</option>`);
        });
      });
    }

    function getClusters2() {
      var lgas   = $("#c_sort_lgas").val();
      var states = $("#c_sort_state").val();
      var params = {
        lga: lgas,
        state: states
      }
      $.get('{{url('admin/get/clusters')}}', params, function(data) {
        $("#c_sort_error_clusters").html("");
        $.each(data, function(index, val) {
          $("#c_sort_error_clusters").append(`
            <option value="${val.name}">${val.name}</option>
          `);
        });
      });
    }

    function startFindReplaceKeywordsState() {
      $("#state-search-replace-btn").prop("disabled", true);
      $("#state-search-replace-btn").html(`
        Searching...
      `);

      var token   = $("#token").val();
      var keyword = $("#sort_keywords_state").val();
      var states  = $("#s_sort_state_2").val();
      var lgas    = $("#s_sort_lgas_2").val();

      var params = {
        _token: token,
        state: states,
        lga: lgas,
        keyword: keyword
      }

      // console log
      $.post('{{ url('admin/search/replace/states') }}', params, function(data, textStatus, xhr) {

        if(data.status == "success"){
          swal(
            "Ok",
            data.message,
            data.status
          );

          $("#state-search-replace-btn").prop("disabled", false);
          $("#state-search-replace-btn").html(`
            Search and Replace State
          `);
        }else{
          $("#state-search-replace-btn").prop("disabled", false);
          $("#state-search-replace-btn").html(`
            Search and Replace State
          `);
        }

      }).fail(function(err){
        $("#state-search-replace-btn").prop("disabled", false);
        $("#state-search-replace-btn").html(`
          Search and Replace State
        `);
      });

      return false;
    }

    function startFindReplaceKeywordsLga() {
      $("#lga-search-replace-btn").prop("disabled", true);
      $("#lga-search-replace-btn").html(`
        Searching...
      `);

      var token   = $("#token").val();
      var states  = $("#sort_state").val();
      var lgas    = $("#sort_lgas").val();
      var keyword = $("#sort_keywords_lga").val();

      var params = {
        _token: token,
        states: states,
        lgas: lgas,
        keyword: keyword
      }

      // console log
      $.post('{{ url('admin/search/replace/lgas') }}', params, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        if(data.status == "success"){
          swal(
            "Ok",
            data.message,
            data.status
          );

          $("#lga-search-replace-btn").prop("disabled", false);
          $("#lga-search-replace-btn").html(`
            Search and Replace LGA
          `);
        }else{
          $("#lga-search-replace-btn").prop("disabled", false);
          $("#lga-search-replace-btn").html(`
            Search and Replace LGA
          `);
        }

      }).fail(function(err){
        $("#lga-search-replace-btn").prop("disabled", false);
        $("#lga-search-replace-btn").html(`
          Search and Replace LGA
        `);
      });

      return false;
    }

    function startFindReplaceKeywordsCluster() {
      $("#c-search-replace-btn").prop("disabled", true);
      $("#c-search-replace-btn").html(`
        Searching...
      `);

      var token   = $("#token").val();
      var replace = $("#c_sort_keywords_clusters").val();
      var state   = $("#c_sort_state").val();
      var lga     = $("#c_sort_lgas").val();
      var find    = $("#c_sort_error_clusters").val();

      var params = {
        _token: token,
        state: state,
        lga: lga,
        find: find,
        replace: replace
      }

      // console log
      $.post('{{ url('admin/search/replace/clusters') }}', params, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        if(data.status == "success"){
          swal(
            "Ok",
            data.message,
            data.status
          );

          $("#c-search-replace-btn").prop("disabled", false);
          $("#c-search-replace-btn").html(`
            Search and Replace Clusters
          `);
        }else{
          $("#c-search-replace-btn").prop("disabled", false);
          $("#c-search-replace-btn").html(`
            Search and Replace Clusters
          `);
        }

      }).fail(function(err){
        $("#c-search-replace-btn").prop("disabled", false);
        $("#c-search-replace-btn").html(`
          Search and Replace Clusters
        `);
      });

      return false;
    }
  </script>
@endsection