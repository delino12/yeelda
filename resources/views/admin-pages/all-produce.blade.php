@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | Products
@endsection

{{-- Contents  --}}
@section('contents')
    <div style="height: 80px;"></div>
    <div class="container text-left">
    	<div class="row">
    		<div class="col-md-12">
	    		<h1 class="lead">All Farm Produce </h1>
	    		<table class="table small" id="produce-table" data-pagination="true" data-search="true"data-page-size="10">
	    			<thead>
	    				<tr>
	    					<th>S/N</th>
	    					<th>Name</th>
	    					<th>Phone</th>
	    					<th>Produce</th>
	    					<th>Amount (&#8358;)</th>
	    					<th>Option</th>
	    					<th>Action</th>
	    				</tr>
	    			</thead>
	    			<tbody class="all-produces"></tbody>
	    		</table>
	    	</div>
    	</div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		// load module
		loadProducts();

		// load farm products
		function loadProducts() {
			// body...
			$.get('/admin/load-products', function (data){
				/* load url json response */
				// console.log(data);
				$('.all-produces').html();
				var sn = 0;
				$.each(data, function(index, val) {
					/* iterate through array or object */
					sn++;
					$('.all-produces').append(`
						<tr>
							<td>`+sn+`</td>
							<td>`+val.owner+`</td>
							<td>`+val.phone+`</td>
							<td>`+val.name+`</td>
							<td>&#8358; `+val.amount+`</td>
							<td><a href="javascript:void();" onclick="deleteItem(`+val.id+`)">Delete</a></td>
							<td><a href="/admin/view-products/`+val.id+`">Details</a></td>
						</tr>
					`);
				});

				$('#produce-table').bootstrapTable();
			});
		}

		// delete equipment
		function deleteItem(x) {
			// body...
			$.get('/admin/delete-products/'+x, function (data){
				/* load url json response */
				// console.log(data);
				if(data.status == 'success'){
					alert('item deleted !');
					window.location.reload();
				}
			});
		}
	</script>
@endsection