@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
  YEELDA | Industrials
@endsection

{{-- Contents  --}}
@section('contents')
  @include('components.sticky-menu')

  <div class="main-content-container container-fluid px-4">
      <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Equipment & Services</span>
        </div>
      </div>

      <div class="row">
        <div class="col">
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">Farming equipment</h6>
            </div>
            <div class="card-body p-0 pb-3 text-center">
              <table class="table mb-0">
                <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Name</th>
                    <th scope="col" class="border-0">Phone</th>
                    <th scope="col" class="border-0">Equipment</th>
                    <th scope="col" class="border-0">Amount</th>
                    <th scope="col" class="border-0">Option</th>
                    <th scope="col" class="border-0">More</th>
                  </tr>
                </thead>
                <tbody class="all-inputs">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      {{-- <div class="row">
        <div class="col">
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">
                Farm services 
                <span class="small">
                  (Fertilizers, Seeds, Insecticide etc.)
                </span>
              </h6>
            </div>
            <div class="card-body p-0 pb-3 text-center">
              <table class="table mb-0">
                <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Name</th>
                    <th scope="col" class="border-0">Phone</th>
                    <th scope="col" class="border-0">Product</th>
                    <th scope="col" class="border-0">Amount</th>
                    <th scope="col" class="border-0">Option</th>
                    <th scope="col" class="border-0">More</th>
                  </tr>
                </thead>
                <tbody class="load-services">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> --}}

      <div class="row">
        <div class="col">
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">
                Farm Fertilizers
              </h6>
            </div>
            <div class="card-body p-0 pb-3 text-center">
              <table class="table mb-0">
                <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Name</th>
                    <th scope="col" class="border-0">Phone</th>
                    <th scope="col" class="border-0">Product</th>
                    <th scope="col" class="border-0">Amount</th>
                    <th scope="col" class="border-0">Option</th>
                    <th scope="col" class="border-0">More</th>
                  </tr>
                </thead>
                <tbody class="load-services-fert">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col">
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">
                Plant Seeds
              </h6>
            </div>
            <div class="card-body p-0 pb-3 text-center">
              <table class="table mb-0">
                <thead class="bg-light">
                  <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Name</th>
                    <th scope="col" class="border-0">Phone</th>
                    <th scope="col" class="border-0">Product</th>
                    <th scope="col" class="border-0">Amount</th>
                    <th scope="col" class="border-0">Option</th>
                    <th scope="col" class="border-0">More</th>
                  </tr>
                </thead>
                <tbody class="load-services-seeds">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Default Light Table -->
  </div>
@endsection

{{-- Scripts --}}
@section('scripts')
  <script type="text/javascript">
    loadEquipment();
    loadServices();

    // load equipments
    function loadEquipment() {
      $.get('/admin/load-equipment', function (data){
        /* load url json response */
        $('.all-inputs').html("");
        var sn = 0;
        $.each(data, function(index, val) {
          /* iterate through array or object */
          // console log data
          // console.log(val);
          sn++;
          $('.all-inputs').append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.owner+`</td>
              <td>`+val.phone+`</td>
              <td>`+val.name+`</td>
              <td>&#8358; `+val.amount+`</td>
              <td>
                <a href="javascript:void(0);" onclick="deleteEquipmentItem(`+val.id+`)">
                  <i class="fa fa-trash"></i> Delete
                </a>
              </td>
              <td>
                <a href="/admin/view-equipment/`+val.id+`">
                  <i class="fa fa-copy"></i> Details
                </a>
              </td>
            </tr>
          `);
        });
      });
    }

    // load farm services
    function loadServices() {
      // body...
      $.get('{{url('admin/load-farm-services')}}', function(data) {
        $(".load-services-fert").html("");
        $(".load-services-seeds").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;

          if(val.type === "fertilizer"){
            $(".load-services-fert").append(`
              <tr>
                <td>${sn}</td>
                <td>${val.owner}</td>
                <td>${val.phone}</td>
                <td>${val.name}</td>
                <td>${val.amount}</td>
                <td>
                  <a href="javascript:void(0);" onclick="deleteFarmServices('${val.id}', '${val.type}')">
                    <i class="fa fa-trash"></i> Delete
                  </a>
                </td>
                <td>
                  <a href="/admin/view-equipment/`+val.id+`">
                    <i class="fa fa-copy"></i> Details
                  </a>
                </td>
              </tr>
            `);
          }

          if(val.type === "seeds"){
            $(".load-services-seeds").append(`
              <tr>
                <td>${sn}</td>
                <td>${val.owner}</td>
                <td>${val.phone}</td>
                <td>${val.name}</td>
                <td>${val.amount}</td>
                <td>
                  <a href="javascript:void(0);" onclick="deleteFarmServices('${val.id}', '${val.type}')">
                    <i class="fa fa-trash"></i> Delete
                  </a>
                </td>
                <td>
                  <a href="/admin/view-equipment/`+val.id+`">
                    <i class="fa fa-copy"></i> Details
                  </a>
                </td>
              </tr>
            `);
          }
        });
      });
    }

    // delete equipment
    function deleteEquipmentItem(x) {
      // body...
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.get('/admin/delete-equipment/'+x, function (data){
            if(data.status == 'success'){
              swal(
                "deleted!",
                data.message,
                data.status
              );
              loadEquipment();
            }
          });
        }
      });
    }

    // delete services
    function deleteFarmServices(itemid, type) {
      // body...
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.get('/admin/delete-services/'+itemid+'/'+type, function (data){
            /* load url json response */
            // console.log(data);
            if(data.status == 'success'){
              swal(
                "deleted!",
                data.message,
                data.status
              );
              loadServices();
            }
          });
        }
      });
    }
  </script>
@endsection