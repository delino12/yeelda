@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | Stamp Messages
@endsection

{{-- Contents  --}}
@section('contents')
    <style type="text/css">
        .fold-p {
            height:18px;
            width:250px;
            overflow:hidden; /* Limits amount of text being displayed */
            margin:0 auto;
            color:#2CA8CC;
        }
    </style>
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Stamp messages</span>
          </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6> 
                    <a href="{{ URL::previous() }}"> 
                        <i class="fa fa-angle-double-left"></i> Previous
                    </a>
                </h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                    <div class="col-md-12" id="message-section">
                        <h1 class="lead">All sent Stamp messages</h1>
                        <table class="table small" id="stamp-message-table" data-pagination="true" data-search="false" data-page-size="10">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Email</th>
                                    <th>Body</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody class="load-stamp-logs"></tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- custom js --}}
    <script type="text/javascript">
        $(document).ready(function(){
            loadStampMessages();
        });

        // load Mail logs
        function loadStampMessages() {
            $.get('/admin/load/sent/stamp', function(data) {
                // console.log(data);
                $(".load-stamp-logs").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    // console log data
                    // console.log(val);
                    $(".load-stamp-logs").append(`
                        <tr>
                            <td>${sn}</td>
                            <td>${val.email}</td>
                            <td>
                                <div class="fold-p">${val.body}</div>
                            </td>
                            <td>${val.status}</td>
                            <td>${val.date}</td>
                        </tr>
                    `);
                });

                $('#stamp-message-table').bootstrapTable();
            });
        }

    </script>
@endsection