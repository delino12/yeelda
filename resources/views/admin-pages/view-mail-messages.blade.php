@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda | Mail Messages
@endsection

{{-- Contents  --}}
@section('contents')
    <style type="text/css">
        .fold-p {
            height:18px;
            width:250px;
            overflow:hidden; /* Limits amount of text being displayed */
            margin:0 auto;
            color:#2CA8CC;
        }
    </style>
    <!-- / .main-navbar -->
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">YEELDA SENT MAIL MESSAGES</span>
          </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">
          <div class="col-md-12">
            <div class="card card-small mb-4">
              <div class="card-header border-bottom">
                <h6 class="m-0"><i class="fa fa-location-arrow"></i> All sent messages!</h6>
              </div>
              <div class="card-body p-4 pb-3">
                <div class="row">
                    <div class="col-md-12" id="message-section">
                        <table class="table" id="mail-message-table"  data-pagination="true" data-search="true"data-page-size="10">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Reciever's</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody class="load-mail-logs"></tbody>
                        </table>
                    </div>
                    <div class="col-md-12 read-more" hidden="">
                        <span class="m-subject"></span>
                        <span class="m-date pull-right"></span>
                        <hr />
                        <div>
                            <p class="m-contents"></p>  
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    {{-- custom js --}}
    <script type="text/javascript">
        $(document).ready(function(){
            loadAllMailLogs();
        });

        // load Mail logs
        function loadAllMailLogs() {
            $.get('/admin/load/sent/mail', function(data) {
                // console.log(data);
                $(".load-mail-logs").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    $(".load-mail-logs").append(`
                        <tr>
                            <td>${sn}</td>
                            <td>${val.reciever}</td>
                            <td>${val.subject}</td>
                            <td>
                                <div class="fold-p">${val.body}</div>
                            </td>
                            <td>${val.status}</td>
                            <td>${val.date}</td>
                            <td><a href="/admin/read/mail/message/${val.id}">view</a></td>
                        </tr>
                    `);
                });

                $('#mail-message-table').bootstrapTable();
            });
        }

    </script>
@endsection