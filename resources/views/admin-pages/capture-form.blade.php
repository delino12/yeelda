@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    Yeelda Cpanel
@endsection

{{-- Contents  --}}
@section('contents')
	<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
    </style>
    <div style="height: 80px;"></div>
    <div class="container text-left">
    	<div class="row">
	    	<div class="col-md-6">
	        	<h1 class="lead">Capture Farm by Address</h1><hr />
		        <form class="post-form" method="post" onsubmit="return searchFarmByAddress()">
		        	<div class="form-group">
		        		<label>Enter farm address</label>
		        		<input type="text" class="form-control" style="background-color: rgba(000,000,000,0.50);color:#fff;" id="address" placeholder="Enter farm address, eg: st peter road, lake side Ogun state" required="">
		        	</div>
		        	<div class="form-group">
		        		<button class="btn btn-primary">
		        			<img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading-a">
		        			Search and Capture
		        		</button>
		        	</div>
		        	
		        	<div class="success_msg"></div>
		        	<div class="error_msg"></div>
		        </form>

		        <hr /><hr />
		        <h1 class="lead">Capture Farm by Co-ordinate</h1><hr />
		        <form class="post-form" method="post" onsubmit="return searchFarmByCodes()">
		        	<div class="form-group">
		        		<div class="row">
		        			<div class="col-sm-6">
		        				<label>Enter <b>Longitude</b> cordinate</label>
		        				<input type="text" class="form-control" style="background-color: rgba(000,000,000,0.50);color:#fff;" id="longitude" placeholder="Enter Long eg: 2.348874737" required="">
		        			</div>

		        			<div class="col-sm-6">
		        				<label>Enter <b>Latitude</b> cordinate</label>
		        				<input type="text" class="form-control" style="background-color: rgba(000,000,000,0.50);color:#fff;" id="latitude" placeholder="Enter Lat eg: 5.348874737" required="">
		        			</div>
		        		</div>
		        	</div>
		        	<div class="form-group">
		        		<button class="btn btn-primary">
		        			<img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading-b">
		        			Search and Capture
		        		</button>
		        	</div>
		        	
		        	<div class="success_msg"></div>
		        	<div class="error_msg"></div>
		        </form>
	    	</div>
	    	<div class="col-md-6">
	    		<h1 class="lead">Map area</h1>
	    		<div class="capture-results"></div>
	    		<div class="capture-results-2"></div>
	    		<div class="capture-image-area"></div>
	    	</div>
	    </div>
	    <div class="row">
	    	<div class="col-md-6">
	    		<div class="capture-image-area">
	    			<div id="map-a">
	    				<img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading-c">
                        <img src="/svg/yeelda-loading.svg" height="60" width="auto" />
	    			</div>
	    		</div>
	    	</div>

	    	<div class="col-md-6">
	    		<div class="capture-image-area">
	    			<div id="map-b">
	    				<img src="/svg/three-dots.svg" height="20" width="30" style="display:none;" class="loading-c">
                        <img src="/svg/yeelda-loading.svg" height="60" width="auto" />
	    			</div>
	    		</div>
	    	</div>
	    </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
    	function searchFarmByAddress() {
    		$('.loading-a').show();
    		let address = $("#address").val();
    		let country = 'Nigeria'; // default radius
    		let token 	= '{{ csrf_token() }}';

    		// get data
    		let data = {
    			_token:token,
    			address:address,
    			country:country
    		}

    		// post data 
    		$.post('/admin/map/farmers/address', data, function(data, textStatus, xhr) {
    			/*optional stuff to do after success */
    			var val = data.response;

    			// console.log(res);
    			// console.log(data.response);
    			if(data.status == 'success'){
    				$('.capture-results').html(`
    					<table class="table">
    						<tr>
    							<td>Acurracy</td>
    							<td>`+val.accuracy+`</td>
    						</tr>

    						<tr>
    							<td>Address</td>
    							<td>`+val.formatted_address+`</td>
    						</tr>

    						<tr>
    							<td>Longitude</td>
    							<td>`+val.lng+`</td>
    						</tr>

    						<tr>
    							<td>Latitude</td>
    							<td>`+val.lat+`</td>
    						</tr>
    					</table>
    					<br />
    					<br />
    				`);
    				$('.loading-a').hide();
    			}

    			let longitude 	= $("#longitude").val(val.lng);
    			let latitude 	= $("#latitude").val(val.lat);
    		});

    		// void form load
    		return false;
    	}

    	// get codes
    	function searchFarmByCodes() {
    		$('.loading-b').show();
    		$('.loading-c').show();
    		let longitude 	= $("#longitude").val();
    		let latitude 	= $("#latitude").val();
    		let token 		= '{{ csrf_token() }}';

    		// get data
    		let data = {
    			_token:token,
    			longitude:longitude,
    			latitude:latitude
    		}

    		// post data 
    		$.post('/admin/map/farmers/latlong', data, function(data, textStatus, xhr) {
    			/*optional stuff to do after success */
    			var val = data.response;

    			// console.log(res);
    			// console.log(data.response);
    			if(data.status == 'success'){
    				$('.capture-results-2').html(`
    					<table class="table">
    						<tr>
    							<td>Acurracy</td>
    							<td>`+val.accuracy+`</td>
    						</tr>

    						<tr>
    							<td>Address</td>
    							<td>`+val.formatted_address+`</td>
    						</tr>

    						<tr>
    							<td>Longitude</td>
    							<td>`+val.lng+`</td>
    						</tr>

    						<tr>
    							<td>Latitude</td>
    							<td>`+val.lat+`</td>
    						</tr>
    					</table>
    				`);
    				$('.loading-b').hide();
    				// Mark image
    				fetchMapImage(val.lat, val.lng);
    			}
    		});

    		// void form load
    		return false;
    	}

    	// fetch map images 
    	function fetchMapImage(lat, lng) {
    		// body...
	     	var imageUrl = `https://maps.googleapis.com/maps/api/staticmap?center=`+lat+`,`+lng+`&zoom=15&size=400x400&key=AIzaSyDeD3MPYcPiDFrQxLWLYkHVy-bV02LsyiA`;

	     	var imageUrlSat = `https://maps.googleapis.com/maps/api/staticmap?center=`+lat+`,`+lng+`&zoom=17&size=400x400&maptype=satellite&key=AIzaSyDeD3MPYcPiDFrQxLWLYkHVy-bV02LsyiA`;

	     	$('.loading-c').hide();
	     	$("#map-a").html(`
	     		<img src="`+imageUrl+`" class="img img-responsive">
	     	`);

	     	$("#map-b").html(`
	     		<img src="`+imageUrlSat+`" class="img img-responsive">
	     	`);
    	}
    </script>
@endsection