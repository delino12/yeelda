@extends('layouts.admin-pages-skin')

{{-- Title --}}
@section('title')
    YEELDA | View Profile
@endsection

{{-- Contents  --}}
@section('contents')
<style type="text/css">
    .y-thumbnail {
        height: 152px;
        border-radius: 4px;
        border:1px solid #FFF;
        width: 170px;
    }
</style>
    @include('components.sticky-menu')
    <div class="main-content-container container-fluid px-4">
      <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
          <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">
                <a href="{{ url('admin/dashboard') }}"> <i class="fa fa-angle-double-left"></i> Back</a>
            </span>
          </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->

        <div class="row">
            <div class="col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">
                            Edit Details
                        </h6>
                    </div>

                    <div class="card-body p-4 pb-3">
                        <form method="post" onsubmit="return saveEditChanges()">
                            <h3>Bio Data</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        @if(empty($user->basic->avatar))
                                            <div class="img-frame form-control" style="width: 200px;height: 200px;">

                                            </div>
                                        @else
                                            <div class="img-frame">
                                                <img src="{{ $user->basic->avatar }}" style="height: 172px;width: 172px;border-radius: 0.3rem;border:1px solid #CCC;">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Names</label>
                                        <input type="text" id="names" class="form-control" placeholder="Enter names" required="" value="{{ $user->basic->name }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" id="email" class="form-control" placeholder="Enter email" required="" value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" id="phone" class="form-control" placeholder="Enter phone" required="" value="{{ $user->phone }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <input type="text" id="gender" class="form-control" placeholder="Enter Gender" required="" value="{{ $user->basic->gender }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>State</label>
                                        <input type="text" id="state" class="form-control" placeholder="Enter State" required="" value="{{ $user->basic->state }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Local Government</label>
                                        <input type="text" id="lga" class="form-control" placeholder="Enter LGA" required="" value="{{ $user->basic->lga }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Language</label>
                                        <input type="text" id="language" class="form-control" placeholder="Language" required="" value="{{ $user->basic->language }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cluster</label>
                                        <input type="text" id="cluster" class="form-control" placeholder="Enter cluster" required="" value="{{ $user->basic->cluster }}">
                                    </div>
                                </div>
                            </div>

                            <br />
                            <br />

                            <h4>Others</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Office</label>
                                        <input type="text" id="office" class="form-control" placeholder="Office No."  value="{{ $user->basic->office }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input type="text" id="mobile" class="form-control" placeholder="Mobile No"  value="{{ $user->basic->mobile }}">
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea id="address" cols="2" rows="5" class="form-control" placeholder="Enter address" required="">{{ $user->basic->address }}</textarea>
                                    </div>
                                </div>
                            </div>

                            @if($user->account_type == "farmer")
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Farm Measurement Type</label>
                                        <input type="text" id="farm_measurement_type" class="form-control" placeholder="Farm size type" required="" value="{{ $user->basic->farm_type }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Farm Measurement Harvest Size</label>
                                        <input type="text" id="farm_measurement_size" class="form-control" placeholder="Harvest Measurement" required="" value="{{ $user->basic->farm_size }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Types of Crops Harvested</label>
                                        <input type="text" id="farm_crop_harvested" class="form-control" placeholder="Enter crops harvest" required="" value="{{ $user->basic->farm_crop }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Previous Amount Harvested</label>
                                        <input type="text" id="prev_crop_harvested" class="form-control" placeholder="Previous Amount Harvested" required="" value="{{ $user->basic->prevCropHarvested }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Previous Yield Harvest Size</label>
                                        <input type="text" id="prev_yield_harvested" class="form-control" placeholder="Previous Yield Harvest Size" required="" value="{{ $user->basic->prevYieldHarvested }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Previous Crops Planted</label>
                                        <input type="text" id="prev_crops_planted" class="form-control" placeholder="Previous Crops Planted" required="" value="{{ $user->basic->prevCropPlanted }}">
                                    </div>
                                </div>
                            </div>
                            @endif

                            <br />
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button class="btn btn-primary">Update Profile</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Create Membership Access -->
    <div class="modal fade" id="add-membership-access" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Grant View Access</h5>
          </div>
          <div class="modal-body">
            <form class="" method="post" onsubmit="return createMembershipAccess()">
                <div class="form-group">
                    <label for="select-group">Group Name</label>
                    <select class="form-control" id="select-group">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">
                        Grant Access
                    </button>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="pull-right">
              <button class="btn btn-flat" type="button" data-dismiss="modal">
                close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        // load modules
        loadGroupInformations();
        getViewAccessList();

        function showCreateAccess() {
            // 
            $("#add-membership-access").modal();
        }

        function createMembershipAccess() {
            var group_id    = $("#select-group").val();
            var user_id     = '{{ $user->id }}';
            var _token      = $("#token").val();

            $.post('{{ url("admin/create/membership") }}', {_token, user_id, group_id}, function(data, textStatus, xhr) {
                if(data.status == "success"){
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                }else{
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                }
            });

            // return
            return false;
        }

        function getViewAccessList(){
            var user_id = "{{ $user->id }}";
            $.get('{{url("admin/list/membership/access")}}', {user_id}, function(data) {
                $(".load-view-access").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    var new_status;
                    if(val.status == "active"){
                        new_status = "Access Enabled";
                        val.access_btn = `
                            <button class="btn btn-danger small" onclick="blockAccess(${val.id})"> Block Access</button>
                        `;
                    }else if(val.status == "inactive"){
                        new_status = "Access Disabled";
                        val.access_btn = `
                            <button class="btn btn-success small" onclick="enableAccess(${val.id})"> Enable Access</button>
                        `;
                    }

                    $(".load-view-access").append(`
                        <tr>
                            <td>${sn}</td>
                            <td>${val.group.name}</td>
                            <td>${new_status}</td>
                            <td>${val.access_btn}</td>
                        </tr>
                    `);
                });
            });
        }

        $.get('/load/user/profile/{{ $id }}', function(data) {
            /*optional stuff to do after success */
            // console.log(data);
            if(data.type == 'farmer'){
                $('.other-details').html(`
                    <h1 class="lead"><i class="fa fa-edit"></i> Farm Record 

                        <span class="float-right">
                            Reg Date: ${data.date_captured}
                        </span>
                    </h1>
                    <table class="table">
                        <tr>
                            <td>Land Capacity</td>
                            <td>`+data.size_of_farm+` `+data.type_of_farm+`</td>
                        </tr>
                        <tr>
                            <td>Soil Texture</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Road Map</td>
                            <td> ${data.state} - `+data.address+`</td>
                        </tr>
                        <tr>
                            <td>Distance</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Discription & Remark</td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Type of Produce</td>
                            <td>${data.type_of_produce}</td>
                        </tr>
                        <tr>
                            <td>Previous harvested</td>
                            <td>${data.previous_harvested}</td>
                        </tr>
                        <tr>
                            <td>Amount Sold (&#8358;)</td>
                            <td>&#8358;${data.amount}</td>
                        </tr>
                    </table>
                    <br /><br />
                `);

                $('.load-crops').html('');
                $.each(data.crops, function(index, val) {
                    /* iterate through array or object */
                    $('.load-crops').append(`
                        <tr>
                            <td>`+val.type+`</td>
                        </td>
                    `);
                });

                if(data.avatar !== null){
                    if (data.avatar.indexOf('data:') > -1){
                        $('.profile-img').html(`
                            <img height="auto" width="170" src="`+data.avatar+`" alt="placeholder+image">
                        `);
                    }else if (data.avatar.indexOf('http://') > -1){
                        $('.profile-img').html(`
                            <img height="auto" width="170" src="`+data.avatar+`" alt="placeholder+image">
                        `);
                    }else{
                        $('.profile-img').html(`
                            <img height="auto" width="170" src="/images/captured-images/`+data.avatar+`" alt="placeholder+image">
                        `);
                    }
                }else{
                    $('.profile-img').html(`
                        <img height="auto" width="170" src="/images/profile-image.png" alt="placeholder+image">
                    `);
                }

                // get more information about user
                searchFarmByAddress(data.address, data.state);
            }

            if(data.type == 'service'){
                if(data.avatar !== null){
                    // profile picture section
                     $('.profile-img').html(`
                        <img height="150" width="170" src="/uploads/service/`+data.avatar+`" alt="placeholder+image">
                    `);
                }else{
                    $('.profile-img').html(`
                        <img 
                        class="img-circle" 
                        src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=50" 
                        alt="Choose a profile picture" 
                        />
                    `);
                }

                // get more information about user
                searchFarmByAddress(data.address, data.state);
            }

            $('.load-profile').html(`
                <h1 class="lead">`+data.name+` (`+data.type+`)</h1>
                <table class="table">
                    <tr>
                        <td>Name</td>
                        <td>`+data.name+`</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>`+data.email+`</td>
                    </tr>
                    <tr>
                        <td>Mobile No.</td>
                        <td>`+data.phone+`</td>
                    </tr>
                    <tr>
                        <td>Office No</td>
                        <td>`+data.office+`</td>
                    </tr>
                </table>
                <br /><br />
                <h1 class="lead">Other Details</h1>
                <table class="table">
                    <tr>
                        <td>Location</td>
                        <td>`+data.state+`</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>`+data.address+`</td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td>`+data.zipcode+`</td>
                    </tr>
                    <tr>
                        <td>Office No</td>
                        <td>`+data.office+`</td>
                    </tr>
                </table>
            `);
        });

        function blockAccess(access_id){
            var _token = $("#token").val();

            $.post('{{url("admin/block/view/access")}}', {_token, access_id}, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == "success"){
                    getViewAccessList();
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                }else{
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                }
            });
        }

        function enableAccess(access_id){
            var _token = $("#token").val();

            $.post('{{url("admin/enable/view/access")}}', {_token, access_id}, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == "success"){
                    getViewAccessList();
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                }else{
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                }
            });
        }

        // fetch groups data
        function loadGroupInformations() {
            $.get('{{ url("admin/all/groups") }}', function(data) {
                /*optional stuff to do after success */
                $("#select-group").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    $("#select-group").append(`
                        <option value="${val.id}">${val.name}</option>
                    `);
                });
            });
        }

        function searchFarmByAddress(search_address, search_state) {
            $('.loading-a').show();
            let address = search_address+' '+search_state;
            let country = 'Nigeria'; // default radius
            let token   = '{{ csrf_token() }}';

            // get data
            let data = {
                _token:token,
                address:address,
                country:country
            }

            // post data 
            $.post('/admin/map/farmers/address', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                var val = data.response;

                // console.log(res);
                // console.log(data.response);
                if(data.status == 'success'){
                    $('.capture-results').html(`
                        <table class="table">
                            <tr>
                                <td>Acurracy</td>
                                <td>`+val.accuracy+`</td>
                            </tr>

                            <tr>
                                <td>Address</td>
                                <td>`+val.formatted_address+`</td>
                            </tr>

                            <tr>
                                <td>Longitude</td>
                                <td>`+val.lng+`</td>
                            </tr>

                            <tr>
                                <td>Latitude</td>
                                <td>`+val.lat+`</td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    `);
                    $('.loading-a').hide();
                }

                $("#longitude").html(`Longitude: `+val.lng);
                $("#latitude").html(`Latitude: `+val.lat);

                // search area geolocation by co-ordinate
                searchFarmByCodes(val.lng, val.lat);
            });
        }

        // get codes
        function searchFarmByCodes(long, lat) {
            $('.loading-b').show();
            $('.loading-c').show();
            let longitude   = long;
            let latitude    = lat;
            let token       = '{{ csrf_token() }}';

            // get data
            let data = {
                _token:token,
                longitude:longitude,
                latitude:latitude
            }

            // post data 
            $.post('/admin/map/farmers/latlong', data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                var val = data.response;

                // console.log(res);
                // console.log(data.response);
                if(data.status == 'success'){
                    $('.capture-results-2').html(`
                        <table class="table">
                            <tr>
                                <td>Acurracy</td>
                                <td>`+val.accuracy+`</td>
                            </tr>

                            <tr>
                                <td>Address</td>
                                <td>`+val.formatted_address+`</td>
                            </tr>

                            <tr>
                                <td>Longitude</td>
                                <td>`+val.lng+`</td>
                            </tr>

                            <tr>
                                <td>Latitude</td>
                                <td>`+val.lat+`</td>
                            </tr>
                        </table>
                    `);
                    $('.loading-b').hide();
                    // Mark image
                    fetchMapImage(val.lat, val.lng);
                }
            });

            // void form load
            return false;
        }

        // fetch map images 
        function fetchMapImage(lat, lng) {
            // body...
            var imageUrl = `https://maps.googleapis.com/maps/api/staticmap?center=`+lat+`,`+lng+`&zoom=15&size=400x400&key=AIzaSyDeD3MPYcPiDFrQxLWLYkHVy-bV02LsyiA`;

            var imageUrlSat = `https://maps.googleapis.com/maps/api/staticmap?center=`+lat+`,`+lng+`&zoom=17&size=400x400&maptype=satellite&key=AIzaSyDeD3MPYcPiDFrQxLWLYkHVy-bV02LsyiA`;

            $('.loading-c').hide();
            $("#map-a").html(`
                <img src="`+imageUrl+`" class="img img-responsive">
            `);

            $("#map-b").html(`
                <img src="`+imageUrlSat+`" class="img img-responsive">
            `);
        }

        // update profile 
        function saveEditChanges() {
            var user_id = '{{ $id }}';
            var token = $("#token").val();
            var names = $("#names").val();
            var email = $("#email").val();
            var phone = $("#phone").val();
            var gender = $("#gender").val();
            var state = $("#state").val();
            var lga = $("#lga").val();
            var language = $("#language").val();
            var cluster = $("#cluster").val();
            var office = $("#office").val();
            var mobile = $("#mobile").val();
            var address = $("#address").val();
            var farm_measurement_type = $("#farm_measurement_type").val();
            var farm_measurement_size = $("#farm_measurement_size").val();
            var farm_crop_harvested = $("#farm_crop_harvested").val();

            var cluster = $("#cluster").val();
            var prev_crop_harvested = $("#prev_crop_harvested").val();
            var prev_yield_harvested = $("#prev_yield_harvested").val();
            var prev_crops_planted = $("#prev_crops_planted").val();
            
            var params = {
                user_id: user_id,
                _token: token,
                names: names,
                email: email,
                phone: phone,
                gender: gender,
                state: state,
                lga: lga,
                language: language,
                cluster: cluster,
                office: office,
                mobile: mobile,
                address: address,
                farm_measurement_type: farm_measurement_type,
                farm_measurement_size: farm_measurement_size,
                cluster: cluster,
                prev_crop_harvested: prev_crop_harvested,
                prev_yield_harvested: prev_yield_harvested,
                prev_crops_planted: prev_crops_planted,
                farm_crop_harvested: farm_crop_harvested
            };

            $.post('{{url("admin/update/basic/profile")}}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == "success"){
                    swal(
                        "Ok",
                        data.message,
                        data.status
                    );
                    // refresh page
                    window.location.reload();
                }else{
                    swal(
                        "Oops",
                        data.message,
                        data.status
                    );
                }
            });

            // return
            return false;
        }
    </script>
@endsection