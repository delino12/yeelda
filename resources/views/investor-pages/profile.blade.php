@extends('layouts.main-in')

@section('title')
	Profile | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		<a href="/investors/dashboard">{{ Auth::user()->name }}</a>
		</li>
		<li class="breadcrumb-item active">Profile</li>
	</ol>

	<div class="row">
		<div class="col-md-5">
			<div class="card">
				<div class="card-body">
					<div class="profile-image"></div>
					<a href="javascript:void(0)" id="choose" class="btn btn-link" onclick="showPicsForm()">
						<i class="fa fa-camera"></i> choose a photo
					</a>
					<div id="upload-pp" style="display: none;">
						<form method="post" action="/buyers/upload/images" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="file" name="file" style="font-size: 12px;" required="">
							<button style="font-size: 12px;">upload </button>
						</form>
					</div>
					<br />
					<br />
					<div class="profile-details"></div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="card">
				<div class="card-body">
					<table class="table small">
						<tr>
							<td>Edit Profile's Information</td>
							<td></td>
						</tr>
					</table>
					{{-- edit profile --}}
					<div class="profile-form">
						<form class="update-form small" method="post" onsubmit="return updateProfile()">
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label>Contact Number </label>
										<div class="input-group col-sm-10">
											<span class="input-group-addon">+234</span>
											<input type="text" class="form-control" pattern="[0-9]*" id="office" placeholder="080---" required="" maxlength="11">
										</div>
										<br />
										<div class="input-group col-sm-10">
											<span class="input-group-addon">+234</span>
											<input type="text" class="form-control" pattern="[0-9]*" id="mobile" placeholder="080---" required="" maxlength="11">
										</div>
									</div>
									<div class="form-group">
										<label>Gender</label>
										<select id="gender" class="form-control col-sm-4 small">
											<option value="male" class="small">Male</option>
											<option value="female" class="small">Female</option>
										</select>
									</div>
									<div class="form-group">
										<label>Address</label>
										<textarea class="form-control" cols="2" rows="3" id="address" placeholder="Type address here.." required=""></textarea>
									</div>

									<div class="form-group">
										<label>Post Code</label>
										<input type="text" class="form-control" id="postal" placeholder="1002242" name="" required="">
									</div>

									<div class="form-group">
										<label>State</label>
										<select id="state" class="form-control col-sm-4 small">
											<option value="" selected="selected">- Select -</option>
												<option value="Abuja FCT">Abuja FCT</option>
												<option value="Abia">Abia</option>
												<option value="Adamawa">Adamawa</option>
												<option value="Akwa Ibom">Akwa Ibom</option>
												<option value="Anambra">Anambra</option>
												<option value="Bauchi">Bauchi</option>
												<option value="Bayelsa">Bayelsa</option>
												<option value="Benue">Benue</option>
												<option value="Borno">Borno</option>
												<option value="Cross River">Cross River</option>
												<option value="Delta">Delta</option>
												<option value="Ebonyi">Ebonyi</option>
												<option value="Edo">Edo</option>
												<option value="Ekiti">Ekiti</option>
												<option value="Enugu">Enugu</option>
												<option value="Gombe">Gombe</option>
												<option value="Imo">Imo</option>
												<option value="Jigawa">Jigawa</option>
												<option value="Kaduna">Kaduna</option>
												<option value="Kano">Kano</option>
												<option value="Katsina">Katsina</option>
												<option value="Kebbi">Kebbi</option>
												<option value="Kogi">Kogi</option>
												<option value="Kwara">Kwara</option>
												<option value="Lagos">Lagos</option>
												<option value="Nassarawa">Nassarawa</option>
												<option value="Niger">Niger</option>
												<option value="Ogun">Ogun</option>
												<option value="Ondo">Ondo</option>
												<option value="Osun">Osun</option>
												<option value="Oyo">Oyo</option>
												<option value="Plateau">Plateau</option>
												<option value="Rivers">Rivers</option>
												<option value="Sokoto">Sokoto</option>
												<option value="Taraba">Taraba</option>
												<option value="Yobe">Yobe</option>
												<option value="Zamfara">Zamfara</option>
												<option value="Outside Nigeria">Outside Nigeria</option>
										</select>
									</div>

									<div class="form-group">
										<button class="btn btn-success">Update Information</button>	
									</div>

									<div class="form-group">
										<div class="success_msg"></div>
										<div class="errors_msg"></div>
									</div>
								</div>
							</div>				
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr />

	{{-- check company existence --}}
	<div class="row">
		<div class="col-md-12">
			<h1 class="lead text-center"><i class="fa fa-copy"></i> Company Details</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 small">
			<div class="card">
				<div class="card-body">
					<div class="c_success_msg"></div>
					<div class="c_errors_msg"></div>
					
					<div class="add-company-form"></div>
					<div class="company-status"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6 small">
			<div class="card">
				<div class="card-body">

				</div>
			</div>
		</div>
	</div>
</div>

{{-- scripts --}}
<script type="text/javascript">
	var id = '{{ Auth::user()->id }}';
	var email = '{{ Auth::user()->email }}';
	$.get('/investor/basic/profile/'+id, function(data) {
		/*optional stuff to do after success */
		// console.log(data);
		$('.profile-details').html(`
			<table class="table small">
				<tr>
					<td>Name</td>
					<td>`+data.name+`</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>`+data.email+`</td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>`+data.gender+`</td>
				</tr>
				<tr>
					<td>Mobile</td>
					<td>`+data.mobile+`</td>
				</tr>
				<tr>
					<td>Office</td>
					<td>`+data.office+`</td>
				</tr>
			</table>
			<br /><br />
			<table class="table small">
				<tr>
					<td>State</td>
					<td>`+data.state+`</td>
				</tr>
				<tr>
					<td>Address</td>
					<td>`+data.address+`</td>
				</tr>
				<tr>
					<td>Postal</td>
					<td>`+data.zipcode+`</td>
				</tr>
			</table>
		`);

		$("#mobile").val(data.mobile);
		$("#office").val(data.office);
		$("#address").val(data.address);
	});

	// show picture form
	function showPicsForm() {
		// body...
		$('#upload-pp').toggle();
		$('#choose').toggle();
	}

	// update profile information 
	function updateProfile(){
		// form data
		var token   = '{{ csrf_token() }}';
		var id      = $('#basic_id').val();
		var state   = $('#state').val();
		var address = $('#address').val();
		var postal  = $('#postal').val();
		var office  = $('#office').val();
		var mobile  = $('#mobile').val();
		var gender  = $('#gender').val();

		// data to json
		var data = {
			_token:token,
			id:id,
			state:state,
			address:address,
			postal:postal,
			office:office,
			mobile:mobile,
			gender:gender
		}

		// ajax send request
		$.ajax({
			url: '/investor/update/profile',
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data){
				console.log(data);
				// check for success
				if(data.status == 'success'){
					$('.success_msg').html(`
						<span class="text-success">
							`+data.message+`
						</span>
					`);
				}

				// check for errors
				if(data.status == 'errors'){
					$('.error_msg').html(`
						<span class="text-danger">
							`+data.message+`
						</span>
					`);
				}
			},
			error: function (data){
				console.log(data);
				alert('Error, Fail to send request update profile');
			}
		});

		// void form function
		return false;
	}

	// load company 
	$.get('/load/company/information/'+email, function(data) {
		/*optional stuff to do after success */
		console.log(data);

		// little del button
		if(data.name == 'none'){
			var del = '';
			$('.add-company-form').html(`
				<form id="addForm" method="post" onsubmit="return addCompany()">
					<div class="form-group">
						<label>Name of Company</label>
						<input type="text" class="form-control" placeholder="Company name here. eg. ALC Limited" required="" id="c-name">
					</div>
					<div class="form-group">
						<label>Company Address</label>
						<textarea class="form-control" cols="12" rows="2" placeholder="Company address here.." id="c-address" required=""></textarea>
					</div>
					<div class="form-group">
						<label>Company Location</label>
						<select class="form-control col-md-4" id="c-state">
							<option value="" selected="selected">- Select -</option>
							<option value="Abuja FCT">Abuja FCT</option>
							<option value="Abia">Abia</option>
							<option value="Adamawa">Adamawa</option>
							<option value="Akwa Ibom">Akwa Ibom</option>
							<option value="Anambra">Anambra</option>
							<option value="Bauchi">Bauchi</option>
							<option value="Bayelsa">Bayelsa</option>
							<option value="Benue">Benue</option>
							<option value="Borno">Borno</option>
							<option value="Cross River">Cross River</option>
							<option value="Delta">Delta</option>
							<option value="Ebonyi">Ebonyi</option>
							<option value="Edo">Edo</option>
							<option value="Ekiti">Ekiti</option>
							<option value="Enugu">Enugu</option>
							<option value="Gombe">Gombe</option>
							<option value="Imo">Imo</option>
							<option value="Jigawa">Jigawa</option>
							<option value="Kaduna">Kaduna</option>
							<option value="Kano">Kano</option>
							<option value="Katsina">Katsina</option>
							<option value="Kebbi">Kebbi</option>
							<option value="Kogi">Kogi</option>
							<option value="Kwara">Kwara</option>
							<option value="Lagos">Lagos</option>
							<option value="Nassarawa">Nassarawa</option>
							<option value="Niger">Niger</option>
							<option value="Ogun">Ogun</option>
							<option value="Ondo">Ondo</option>
							<option value="Osun">Osun</option>
							<option value="Oyo">Oyo</option>
							<option value="Plateau">Plateau</option>
							<option value="Rivers">Rivers</option>
							<option value="Sokoto">Sokoto</option>
							<option value="Taraba">Taraba</option>
							<option value="Yobe">Yobe</option>
							<option value="Zamfara">Zamfara</option>
							<option value="Outside Nigeria">Outside Nigeria</option>
			            </select>
					</div>
					<div class="form-group">
						<label>Type of Company </label>
						<select class="form-control col-md-4" id="c-type">
							<option value="">- Select-</option>
							<option value="public">Public</option>
							<option value="private">Private</option>
							<option value="start-up">Start up</option>
						</select>
					</div>
					<div class="form-group">
						<label>Number of employee</label>
						<select id="c-employee" class="form-control col-md-4">
							<option value="0-5">0-5 </option>
							<option value="5-10">5-10 </option>
							<option value="10-20">10-20 </option>
							<option value="25-50">25-50 </option>
							<option value="50-100">50-100 </option>
						</select>
					</div>
					<div class="form-group">
						<button class="btn btn-primary">Add company</button>
					</div>
				</form>
			`);
		}else{
			var del = '<a href="/delete/company/information/'+data.id+'"><button class="btn btn-danger small">Delete company</button></a>';
			$('.company-status').html(`
				<h1 class="lead">
				</h1>
				<table class="table">
					<tr>
						<td>Name</td>
						<td>`+data.name+`</td>
					</tr>

					<tr>
						<td>Type of Company</td>
						<td>`+data.type+`</td>
					</tr>

					<tr>
						<td>Company Address</td>
						<td>`+data.location+`</td>
					</tr>

					<tr>
						<td>Company Location</td>
						<td>`+data.state+`</td>
					</tr>

					<tr>
						<td>No of Staffs</td>
						<td>`+data.staff+`</td>
					</tr>

					<tr>
						<td>Comapny Name</td>
						<td>`+data.name+`</td>
					</tr>
				</table>
				<hr />
				`+del+`
			`);
		}
	});

	// add company
	function addCompany(){
		// form to var
		var token    = '{{ csrf_token() }}';
		var email    = '{{ Auth::user()->email }}';
		var name     = $('#c-name').val();
		var address  = $('#c-address').val();
		var state    = $('#c-state').val();
		var type  	 = $('#c-type').val();
		var employee = $('#c-employee').val();

		// data to json
		var data = {
			_token:token,
			name:name,
			email:email,
			address:address,
			state:state,
			type:type,
			employee:employee	
		}
	
		// data to ajax
		$.ajax({
			type: 'POST',
			url: '/add/company/information',
			dataType: 'json',
			data: data,
			success: function (data) {
				// console.log(data);
				if(data.status == 'success'){
					$('.c_success_msg').html(`
						<p class="text-success">`+data.message+`</p>
					`);
				}else{
					$('.c_errors_msg').html(`
						<p class="text-danger">`+data.message+`</p>
					`);
				}

				// hide form
				$('#addForm').hide();
			},
			error: function (data) {
				console.log(data);
				alert('Error, fail to send request !');
			}
		});
	
		return false;
	}
</script>
@endsection