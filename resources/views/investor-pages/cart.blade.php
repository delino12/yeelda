@extends('layouts.main-in')

@section('title')
	Carts | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		<a href="/investors/dashboard">{{ Auth::user()->name }}</a>
		</li>
		<li class="breadcrumb-item active">Product Lists</li>
	</ol>
	<div class="row">
		<div class="col-12">
	  		<h1>Carts</h1>
	 		<p>Shopping Carts</p>
		</div>
	</div>
</div>

<script type="text/javascript">
	var logged_email = '{{ Auth::user()->email }}';
</script>
@endsection