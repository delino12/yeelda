@extends('layouts.main-in')

@section('title')
	Account Setting | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/investors/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Setting</li>
		</ol>
		@if(session('update_status'))
			<div class="alert alert-success dino_errors" role="alert">
				<p class="text-success">{{ session('update_status') }}</p>
			</div>
			<script type="text/javascript">
				var exitApp = function (){
					window.location.href = "/account/logout";
				};
				setTimeout(exitApp, 5000);
			</script>
		@endif

		@if(session('error_status'))
			<div class="alert alert-danger dino_errors" role="alert">
				<p class="text-danger">{{ session('error_status') }}</p>
			</div>
		@endif

		<div class="alert alert-danger dino_errors" id="flash_error_div" role="alert" style="display: none;">
			<p class="text-danger">
				<span id="flash_error"></span>
			</p>
		</div>
		
		<div class="row">
			<div class="col-md-5">
				<div class="card">
					<div class="card-body">
						<div id="preview-image"></div>
						<br />
						<a href="javascript:void(0)" id="upload_widget_opener" class="btn btn-link">
							<i class="fa fa-camera"></i> choose a photo
						</a>
					</div>
				</div>
				<br />
				<div class="card">
					<div class="card-body">
						Profile Details
						<div class="profile-details"></div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="card">
					<div class="card-body">
						<h2 class="lead">Edit Profile Details</h2><hr />
						{{-- edit profile --}}
						<div class="profile-form">
							<form class="update-form small" method="post" onsubmit="return updateProfile()">
								<div class="row">
									<div class="col-sm-8">
										<div class="form-group">
											<label>Contact Number </label>
											<div class="input-group col-sm-10">
												<span class="input-group-addon">+234</span>
												<input type="text" class="form-control" pattern="[0-9]*" id="office" placeholder="080---" required="" maxlength="11">
											</div>
											<br />
											<div class="input-group col-sm-10">
												<span class="input-group-addon">+234</span>
												<input type="text" class="form-control" pattern="[0-9]*" id="mobile" placeholder="080---" required="" maxlength="11">
											</div>
										</div>
										<div class="form-group">
											<label>Gender</label>
											<select id="gender" class="form-control col-sm-4 small">
												<option value="male" class="small">Male</option>
												<option value="female" class="small">Female</option>
											</select>
										</div>
										<div class="form-group">
											<label>Address</label>
											<textarea class="form-control" cols="2" rows="3" id="address" placeholder="Type address here.." required=""></textarea>
										</div>

										<div class="form-group">
											<label>Post Code</label>
											<input type="text" class="form-control" id="postal" placeholder="1002242" name="" required="">
										</div>

										<div class="form-group">
											<label>State</label>
											<select id="state" class="form-control col-sm-4 small">
												<option value="" selected="selected">- Select -</option>
													<option value="Abuja FCT">Abuja FCT</option>
													<option value="Abia">Abia</option>
													<option value="Adamawa">Adamawa</option>
													<option value="Akwa Ibom">Akwa Ibom</option>
													<option value="Anambra">Anambra</option>
													<option value="Bauchi">Bauchi</option>
													<option value="Bayelsa">Bayelsa</option>
													<option value="Benue">Benue</option>
													<option value="Borno">Borno</option>
													<option value="Cross River">Cross River</option>
													<option value="Delta">Delta</option>
													<option value="Ebonyi">Ebonyi</option>
													<option value="Edo">Edo</option>
													<option value="Ekiti">Ekiti</option>
													<option value="Enugu">Enugu</option>
													<option value="Gombe">Gombe</option>
													<option value="Imo">Imo</option>
													<option value="Jigawa">Jigawa</option>
													<option value="Kaduna">Kaduna</option>
													<option value="Kano">Kano</option>
													<option value="Katsina">Katsina</option>
													<option value="Kebbi">Kebbi</option>
													<option value="Kogi">Kogi</option>
													<option value="Kwara">Kwara</option>
													<option value="Lagos">Lagos</option>
													<option value="Nassarawa">Nassarawa</option>
													<option value="Niger">Niger</option>
													<option value="Ogun">Ogun</option>
													<option value="Ondo">Ondo</option>
													<option value="Osun">Osun</option>
													<option value="Oyo">Oyo</option>
													<option value="Plateau">Plateau</option>
													<option value="Rivers">Rivers</option>
													<option value="Sokoto">Sokoto</option>
													<option value="Taraba">Taraba</option>
													<option value="Yobe">Yobe</option>
													<option value="Zamfara">Zamfara</option>
													<option value="Outside Nigeria">Outside Nigeria</option>
											</select>
										</div>
										<div class="form-group">
											<button class="btn btn-success">Update Information</button>	
										</div>
									</div>
								</div>				
							</form>
							<br />
							<div class="success_msg"></div>
							<div class="error_msg"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br />
		
		<div class="row">
			<div class="col-md-5 small">
				<div class="card">
					<div class="card-body">
						<h1 class="small"><i class="fa fa-times text-danger"></i> To deactivate your account, click on the button below!</h1>
						<button class="btn btn-danger" data-toggle="collapse" data-target="#deactivate">Deactivate</button>
						<hr />
						<br /><br />
						<div id="deactivate" class="collapse">
							<form method="post" onsubmit="return deactivateAccount()">
								<div class="form-group col-md-6">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-key"></i>
										</span>
										<input type="password" id="password" class="form-control" maxlength="30" placeholder="enter password" required="">
									</div>
								</div>
								<div class="form-group col-md-6">
									<button class="btn btn-danger">
										deactivate
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="card">
					<div class="card-body">
						<p>Change Account Password</p>
						<hr />
						<form method="POST" onsubmit="return verifyPassword()">
							{{ csrf_field() }}
							<div class="row small">
								<div class="col-md-8">
									<div class="form-group">
										<div class="col-xm-6">
											<label><i class="fa fa-key"></i> Create New Password</label>
										</div>
										<div class="col-xm-6">
											<input type="password" id="new_password" name="new_password" placeholder="Type a new Password" class="form-control" required="">
										</div>
									</div>

									<div class="form-group">
										<div class="col-xm-6">
											<label><i class="fa fa-key"></i> Confirm Password</label>
										</div>
										<div class="col-xm-6">
											<input type="password" id="confirm_new_password" name="confirm_new_password" placeholder="Confirm new Password" class="form-control" required="" onblur="return verifyPassword()">
										</div>
									</div>

									<div class="form-group">
										<button class="btn btn-info">change password</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<br />
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		function changePassword()
		{
			// form data
			var token       = '{{ csrf_token() }}';
			var passwordOne = $("#new_password").val();
			var passwordTwo = $("#confirm_new_password").val();

			// compare password
			if(passwordOne !== passwordTwo){
				$("#flash_error_div").show();
				$("#flash_error").text('password did not match, try again');
				return false;
			}

			// data json
			var data = {
				_token:token,
				password:passwordOne
			}

			// send ajax
			$.ajax({
				url: '/investor/change-password',
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data){
					console.log(data);
					// check for success
					if(data.status == 'success'){
						$('.success_msg').html(`
							<span class="text-success">
								`+data.message+`
							</span>
						`);

						$('.change-password-form').hide();

						window.setTimeout(function (){
							window.location.href = '/account/logout/investor';
						}, 1000 * 5);
					}

					// check for errors
					if(data.status == 'errors'){
						$('.error_msg').html(`
							<span class="text-danger">
								`+data.message+`
							</span>
						`);
					}
				},
				error: function (data){
					console.log(data);
					alert('Error, Fail to send request change password..');
				}
			});
			


			return false;
		}

		// deactivate account 
		function deactivateAccount() {
			// body...
			let token		= '{{ csrf_token() }}';
			let userid      = '{{ Auth::user()->id }}';
			let password 	= $("#password").val();

			// to json
			let data = {
				_token:token,
				userid:userid,
				password:password
			};

			// data to ajax
			$.post('/deactivate/account/buyer', data, function(data, textStatus, xhr) {
				/*optional stuff to do after success */
				// console.log(data);
				if(data.status == 'success'){
					window.location.href = '/account/logout/investor';
				}else{
					alert(data.message);
				}
			}).then(function (e){
				console.log(e);
			});

			return false;
		}

		loadProfile();

		// load farmers profile
		function loadProfile(argument) {
			// load farmers profile
			$.get('/investor/load/profile', function (data){
				$('.profile-details').html(`
					<table class="table small">
						<tr>
							<td>Name</td>
							<td>`+data.name+`</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>`+data.email+`</td>
						</tr>
						<tr>
							<td>Gender</td>
							<td>`+data.gender+`</td>
						</tr>
						<tr>
							<td>Mobile number</td>
							<td>`+data.mobile+`</td>
						</tr>
						<tr>
							<td>Office number</td>
							<td>`+data.office+`</td>
						</tr>
					</table>
					<br /><br />
					<table class="table small">
						<tr>
							<td>State</td>
							<td>`+data.state+`</td>
						</tr>
						<tr>
							<td>Address</td>
							<td>`+data.address+`</td>
						</tr>
						<tr>
							<td>Postal</td>
							<td>`+data.zipcode+`</td>
						</tr>
					</table>
				`);

				$("#mobile").val(data.mobile);
				$("#office").val(data.office);
				$("#address").val(data.address);

				if(data.avatar !== null){
					// profile picture section
					$('#profile-image').html(`
						<img class="img-rounded" src="${data.avatar}" width="84" height="84" alt="">
					`);
				}else{
					$('#profile-image').html(`
						<img 
						class="img-circle" 
						src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=50" 
						alt="Choose a profile picture" 
					/>
					`);
				}
			});
		}

		// send update profile details
		function updateProfile(){
			var token   = '{{ csrf_token() }}';
			var state 	= $('#state').val();
			var address = $('#address').val();
			var postal 	= $('#postal').val();
			var office 	= $('#office').val();
			var mobile 	= $('#mobile').val();
			var gender 	= $('#gender').val();

			// data to json
			var data = {
				_token:token,
				state:state,
				address:address,
				postal:postal,
				office:office,
				mobile:mobile,
				gender:gender
			}
		
			// data to ajax
			$.ajax({
				type: 'POST',
				url: '/investor/update/profile',
				dataType: 'json',
				data: data,
				success: function (data) {
					// console.log(data);
					// alert('Successful, check console data');
					if(data.status == 'success'){
						$('.success_msg').html(`
							<p class="text-success">`+data.message+`</p>
						`);
					}
				},
				error: function (data) {
					// console.log(data);
					alert('Error, fail to send request !');
				}
			});
		
			return false;
		}	

		// show image upload form
		function showImageForm() {
			// body...
			$('.choose-profile').toggle();
			$('.profile-img').toggle();
		}
	</script>
@endsection