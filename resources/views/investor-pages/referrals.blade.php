@extends('layouts.main-in')

@section('title')
	Account Setting | Yeelda
@endsection

@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		<a href="/investors/dashboard">{{ Auth::user()->name }}</a>
		</li>
		<li class="breadcrumb-item active">Promotions</li>
	</ol>

	<div class="alert alert-danger dino_errors" id="flash_error_div" role="alert" style="display: none;">
		<p class="text-danger">
			<span id="flash_error"></span>
		</p>
	</div>

	<br />
	<div class="row">
		<div class="col-md-6">
			<div class="card">
				<div class="card-body">
					<h3 class="lead">Rating</h3>
					<span class="rating-count">00.00</span>
				</div>
			</div>
			<br />
			<div class="card">
				<div class="card-body">
					<h3 class="lead">Referral Bonus</h3>
					<span class="referral-balance"></span>
					<div class="pull-right">
						<i class="fa fa-users"></i>	count: <span class="r-count"></span>
					</div>
					<hr />
					<span class="referral-info"></span>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="card">
				<div class="card-body">
			  		<h1 class="lead"><i class="fa fa-wifi"></i> Referrals</h1>
			 		<p class="small">Promotions show your current rating and referral account.</p>
			 	</div>
			</div>
			<br />
			<div class="card">
				<div class="card-body">
			  		<h1 class="lead">
			  			<i class="fa fa-diamond"></i> Subscribe to premium
			  			{{-- <span class="pull-right">Status: free</span> --}}
			  		</h1>
		 			<table class="table small">
		 				<tr>
		 					<td>3 Months</td>
		 					<td>&#8358;3,000.00</td>
		 					<td>
		 						<button class="btn btn-primary btn-sm">Subscribe</button>
		 					</td>
		 				</tr>

		 				<tr>
		 					<td>6 Months</td>
		 					<td>&#8358;5,500.00</td>
		 					<td>
		 						<button class="btn btn-primary btn-sm">Subscribe</button>
		 					</td>
		 				</tr>

		 				<tr>
		 					<td>1 Year</td>
		 					<td>&#8358;7,500.00</td>
		 					<td>
		 						<button class="btn btn-primary btn-sm">Subscribe</button>
		 					</td>
		 				</tr>
		 			</table>
			 	</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$.get('/load/referrals/{{ Auth::user()->email }}', function (data){
		/* load url json response */
		$('.referral-balance').html(`
			`+data.amount+`
		`);

		// count vote
		$('.r-count').html(data.vote);

		// load referral information
		$('.referral-info').html(`
			<p class="small">Your referral code: `+data.code+`</p>
			<p class="small">
				<a href="http://yeelda.com?refId=`+data.code+`">http://yeelda.com?refId=`+data.code+`</a>
			</p>
		`);
	});
</script>
@endsection