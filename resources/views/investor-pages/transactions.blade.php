@extends('layouts.main-in')

@section('title')
	Transactions | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		<a href="/investors/dashboard">{{ Auth::user()->name }}</a>
		</li>
		<li class="breadcrumb-item active">Transactions Lists</li>
	</ol>
	<div class="row">
		<div class="col-md-8">
	 		<p>{{-- Transaction details here ! --}}</p>
		</div>
		<div class="col-md-8">
	 		<p>{{-- Transaction details here ! --}}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h1 class="lead">Pending Transactions </h1>
					<table class="table small">
						<thead>
							<tr>
								<th>S/N</th>
								<th>Ref ID</th>
								<th>Buyers Name</th>
								<th>Product</th>
								<th>Qty</th>
								<th>Amount (&#8358;)</th>
								<th>Total (&#8358;)</th>
								<th>Status</th>
								<th>Last updated</th>
								<th>Option</th>
							</tr>
						</thead>
						<tbody class="load-pending-transactions"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<br />

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h1 class="lead">Successful Transactions</h1>
					<table class="table small">
						<thead>
							<tr>
								<th>S/N</th>
								<th>Ref ID</th>
								<th>Buyers Name</th>
								<th>Product</th>
								<th>Qty</th>
								<th>Amount (&#8358;)</th>
								<th>Total (&#8358;)</th>
								<th>Status</th>
								<th>Last updated</th>
								<th>Option</th>
							</tr>
						</thead>
						<tbody class="load-approved-transaction"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<br />
</div>

{{-- load transaction details --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">
	$(document).ready(function (){
    	// loadUserPayments();
    	loadUserTransactions();
    });

    // load users transactions
    function loadUserTransactions() {
    	// body...
    	var email = '{{ Auth::user()->email }}';
    	var params = {email: email};

    	$.get('/load/buyer/payments', params, function(data) {
	    	// console.log(data);
	    	$('.load-pending-transactions').html("");
	    	$('.load-approved-transaction').html("");
	    	var sn = 0;
	    	$.each(data, function(index, val) {
	    		sn++;
	    		// console.log(val);
				if(val.status == "pending"){
					var actionButton = `<a style="border:1px solid #2AA6CA;padding:0.3em;text-decoration:none;border-radius:4px;" href="/confirm/payment/`+val.trans_id+`">Confirm</a>`;

					$('.load-pending-transactions').append(`
		    			<tr>
		    				<td>${sn}</td>
		    				<td>${val.details.trans_id}</td>
		    				<td>${val.seller}</td>
		    				<td>${val.produce.product_name}</td>
		    				<td>${val.details.qty}</td>
		    				<td>&#8358;${numeral(val.details.price).format("0,0.00")}</td>
		    				<td>&#8358;${numeral(val.details.amount).format("0,0.00")}</td>
		    				<td>${val.status}</td>
		    				<td>${val.date}</td>
		    				<td>${actionButton}</td>
		    			</tr>
		    		`);
				}else{
					if(val.status == "settle"){
						val.status = "Confirmed";
					}
					var actionButton = `<a style="border:1px solid #2AA6CA;padding:0.3em;text-decoration:none;border-radius:4px;" href="javascript:void(0);">Approved</a>`;

					$('.load-approved-transaction').append(`
		    			<tr>
		    				<td>${sn}</td>
		    				<td>${val.details.trans_id}</td>
		    				<td>${val.seller}</td>
		    				<td>${val.produce.product_name}</td>
		    				<td>${val.details.qty}</td>
		    				<td>&#8358;${numeral(val.details.price).format("0,0.00")}</td>
		    				<td>&#8358;${numeral(val.details.amount).format("0,0.00")}</td>
		    				<td>${val.status}</td>
		    				<td>${val.date}</td>
		    				<td>${actionButton}</td>
		    			</tr>
		    		`);
				}
	    	});
	    });
    }
</script>
@endsection