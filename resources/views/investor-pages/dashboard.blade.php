@extends('layouts.main-in')

@section('title')
	Dashboard | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		<a href="/investors/dashboard">{{ Auth::user()->name }}</a>
		</li>
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>
	{{-- @if(!empty($acct_status))
		<div class="alert alert-danger">
			<p class="text-danger"> {{ $acct_status }}<span class="text-info"> Did not receive any link?</span> 
			<a href="/resend/activation/link/?email={{ Auth::user()->email }}&name={{ Auth::user()->name }}">Resend</a> </p>
		</div>
	@endif
	@if(session('success_msg'))
		<div class="alert alert-success">
			<p class="text-success"> {{ session('success_msg') }}</p>
		</div>
	@endif --}}
	<div class="row">
		<div class="col-md-7">
			<div class="card">
	 			<div class="card-body">
					<h1 class="lead">Pending Transactions </h1>
					<table class="table small">
						<thead>
							<tr>
								<th>S/N</th>
								<th>Seller Name</th>
								<th>Product</th>
								<th>Amount (&#8358;)</th>
								<th>Last updated</th>
								<th>Option</th>
							</tr>
						</thead>
						<tbody class="load-pending-transactions">
							<tr>
								<td>Loading...</td>
							</tr>
						</tbody>
					</table>
					<br />
					<div class="show-view-more" align="center"></div>
				</div>
			</div>
			<br />
			<div class="card">
	 			<div class="card-body">
					<h1 class="lead">Approved Transactions </h1>
					<table class="table small">
						<thead>
							<tr>
								<th>S/N</th>
								<th>Seller Name</th>
								<th>Product</th>
								<th>Amount (&#8358;)</th>
								<th>Last updated</th>
								<th>Option</th>
							</tr>
						</thead>
						<tbody class="load-approved-transaction">
							<tr>
								<td>Loading...</td>
							</tr>
						</tbody>
					</table>
					<br />
					<div class="show-view-more" align="center"></div>
				</div>
			</div>
		</div>

		<div class="col-md-5">
			<div class="card">
	 			<div class="card-body">
					<h3 class="lead">Notifications</h3>
					<div class="notifications small"></div>
				</div>
			</div>
			<br />
			<div class="card">
	 			<div class="card-body">
					<h3 class="lead">News Feeds</h3>
					<ul class="list-group">
						{{-- <div class="news small"></div> --}}
						{{-- <div id="rss-feeds" class="small"></div> --}}
						<div class="load-headlines small"></div>
		        		<div class="load-headlines-two small" style="display: none;"></div>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">
	displayApiNewsFeeds();
	displayUserNotifications();
	displayBuyersTransaction();

	// display notifications
	function displayUserNotifications() {
		// load notifications
		var email = '{{ Auth::user()->email }}';
		$.get('/load/notifications/'+email, function (e){
			$('.notifications').html('');
			$.each(e, function (index, value){
				// value = e.contents;
				// console.log(value);
				// load div
				$('.notifications').append(`
					You have message from <b>`+value.contents.from+`</b> <br /> 
					<p>`+value.contents.body+`</p>
					<a href="/message/service">view now</a> <br />
					<span class="small pull-right">`+value.date+`</span><br /><br />
				`);
			});
			// console.log(e.contents);
		});
	}

	// load news 
	function displayApiNewsFeeds() {
		$.get('/load/api-news', function(data) {
	      	// console.log(data);
	      	$(".load-headlines").html("");
	      	var sn = 0;
	      	$.each(data.articles, function(index, val) {
	        	// console.log(val);
	        	sn++;
	        	$(".load-headlines").append(`
	          		<table cellpadding="2">
	            		<tr>
	              			<td><img src="`+val.urlToImage+`" width="70" height="50"><td>
	              			<td><h4 style="font-size:13px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
	            		</tr>
	          		</table> 
	          		<p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
	        	`);
	        	if(sn == 4){
	          	return false;
	        	}
	      	});
    	});
	}

	// fetch buyers transactions
	function displayBuyersTransaction() {
		// body...
    	var email = '{{ Auth::user()->email }}';
    	var params = {email: email};

    	$.get('/load/buyer/payments/', params, function(data) {
	    	// console.log(data);
	    	$('.load-pending-transactions').html("");
	    	$('.load-approved-transaction').html("");
	    	var sn = 0;
	    	$.each(data, function(index, val) {
	    		sn++;

	    		if(val.produce !== null){
	    			// console.log(val);
					if(val.status == "pending"){
						var actionButton = `
						<a style="padding:0.3em;text-decoration:none;border-radius:4px;" href="javascript:void(0);">
							view
						</a>`;

						$('.load-pending-transactions').append(`
			    			<tr>
			    				<td>${sn}</td>
			    				<td>${val.seller}</td>
			    				<td>${val.produce.product_name}</td>
			    				<td>&#8358;${numeral(val.details.amount).format("0,0.00")}</td>
			    				<td>${val.date}</td>
			    				<td>${actionButton}</td>
			    			</tr>
			    		`);
					}else{

						if(val.status == "settle"){
							val.status = "Confirmed";
						}
						var actionButton = `
						<a style="padding:0.3em;text-decoration:none;border-radius:4px;" href="javascript:void(0);">
							view
						</a>`;

						$('.load-approved-transaction').append(`
			    			<tr>
			    				<td>${sn}</td>
			    				<td>${val.seller}</td>
			    				<td>${val.produce.product_name}</td>
			    				<td>&#8358;${numeral(val.details.amount).format("0,0.00")}</td>
			    				<td>${val.date}</td>
			    				<td>${actionButton}</td>
			    			</tr>
			    		`);
					}

					if(sn > 3){
						return false;
					}
	    		}
	    	});

	    	$('.show-view-more').append(`
				<a class="small" style="border:1px solid #2AA6CA;padding:0.3em;text-decoration:none;border-radius:4px;" href="/investors/transactions">
					view more
				</a>
			`);
	    });
	}
</script>
@endsection