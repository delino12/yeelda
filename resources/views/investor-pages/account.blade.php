@extends('layouts.main-in')

@section('title')
	Account | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/service-provider/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Account</li>
			<li class="pull-right">
				@if(session('update_status'))
					<p class="text-success">{{ session('update_status') }}</p>
				@endif

				@if(session('update_error'))
					<p class="text-danger">{{ session('update_error') }}</p>
				@endif
			</li>
		</ol>
		<div class="row">
			<div class="col-7">
				<div class="card">
					<div class="card-header">
						<p class="lead"><i class="fa fa-bank"></i> 
				  			Setup Bank Account
				  			<a href="javascript:void(0);" onclick="addAccount()" class="pull-right lead"><i class="fa fa-edit"></i></a>
				  		</p>
					</div>
					<div class="card-body">
						@if(count($account_details) > 0)
							@foreach($account_details as $details)
					  			@if($details->account_no == "")
					  				<p class="small">
					  					Currently you do not have any account details uploaded yet, please make sure you upload your account details
					  				</p>
							 		<p class="small">
							 			<b class="text-danger">Warning:</b> Buyer can not make payment if you do not provide your account information
							 			<br />
							 			<b class="text-info">Note:</b> Payment system is automated!
							 		</p>
							 	@else
									<table class="table">
						  				<tbody>
						  					<tr>
						  						<td><i class="fa fa-bank"></i> Bank Name</td>
						  						<td>{{ strtoupper($details->bank_name) }}</td>
						  					</tr>
						  					<tr>
						  						<td><i class="fa fa-user"></i> Account Holder's Name</td>
						  						<td>{{ strtoupper($details->account_name) }}</td>
						  					</tr>
						  					<tr>
						  						<td><i class="fa fa-calculator"></i> Account Number</td>
						  						<td>{{ $details->account_no }}</td>
						  					</tr>
						  					<tr>
						  						<td><i class="fa fa-database"></i> Account Type</td>
						  						<td>{{ strtoupper($details->account_type) }}</td>
						  					</tr>
						  				</tbody>
						  			</table>
					  			@endif
					  		@endforeach
						@endif
				  	</div>
				</div>
			</div>
			<div class="col-5">
				<div class="card small">
					<div class="card-header">
						<p class="lead"><i class="fa fa-credit-card"></i> 
				  			Wallet
				  		</p>
					</div>
					<div class="card-body">
				  		<h5 class="wallet-balance"></h5>
				  		<br />
				  		<div class="mb-4">
							<button class="btn btn-primary btn-sm" onclick="showFundForm()">
				  				Fund Wallet
				  			</button>

				  			<button class="btn btn-success btn-sm pull-right">
				  				Withdraw
				  			</button>
				  		</div>

				  		<form class="fund-form" method="post" onsubmit="return false" style="display: none;">
				  			<div class="row">
				  				<div class="col-md-6">
				  					<div class="form-group">
						  				<label>Enter Amount</label>
						  				<input type="number" min="0" step="any" id="fund_amount" placeholder="00.00" class="form-control" name="">
						  			</div>
				  				</div>
				  			</div>

				  			<div class="row">
				  				<div class="col-md-6">
				  					<button class="btn btn-primary btn-sm" id="fund-wallet" type="submit">
				  						Proceed
				  					</button>
				  				</div>
				  			</div>
				  		</form>
				  	</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="add-banking" tabindex="-1" role="dialog" aria-labelledby="add-banking" aria-hidden="true">
	      <div class="modal-dialog" role="document">
	        <div class="modal-content">
	          <div class="modal-header">
	            <h5 class="modal-title" id="exampleModalLabel">System will request information to proccess transactions</h5>
	            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">×</span>
	            </button>
	          </div>
	          <div class="modal-body">
					<form action="/update/account-details" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="small">Bank Name</label>
								</div>
								<div class="col-sm-6">
									<select type="text" class="form-control " name="bank_name">
										<option value="none">Select Bank </option>
										<option value="Access Bank">Access Bank</option>
										<option value="Citi Bank">Citibank</option>
										<option value="Diamond Bank">Diamond Bank</option>
										<option value="Eco Bank">Ecobank</option>
										<option value="Fidelity">Fidelity Bank</option>
										<option value="First City Monument Bank">First City Monument Bank (FCMB)</option>
										<option value="FSDH Merchant Bank">FSDH Merchant Bank</option>
										<option value="Guarantee Trust Bank">Guarantee Trust Bank (GTB)</option>
										<option value="Heritage Bank">Heritage Bank</option>
										<option value="Keyston Bank">Keystone Bank</option>
										<option value="Rand Merchant Bank">Rand Merchant Bank</option>
										<option value="Skye Bank">Skye Bank</option>
										<option value="Stanbic IBTC Bank">Stanbic IBTC Bank</option>
										<option value="Standard Chartered Bank">Standard Chartered Bank</option>
										<option value="Sterling Bank">Sterling Bank</option>
										<option value="Suntrust Bank">Suntrust Bank</option>
										<option value="Union Bank">Union Bank</option>
										<option value="United Bank for Africa">United Bank for Africa (UBA)</option>
										<option value="Unity Bank">Unity Bank</option>
										<option value="Wema Bank">Wema Bank</option>
										<option value="Zenith Bank">Zenith Bank</option>
									</select>
								</div>
							</div>
						</div>
					
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="small">Account Holder</label>
								</div>
								<div class="col-sm-6">
									<input type="text" name="account_name" placeholder="John Emeka Afolabi" class="form-control" required="">
								</div>
							</div>
						</div>
		 
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="small">Account Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" pattern="[0-9]*" name="account_no" placeholder="0373XXXXXXXX" maxlength="12" class="form-control" required="">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label class="small">Account Type</label>
								</div>
								<div class="col-sm-6">
									<select class="form-control" name="account_type">
										<option value="saving">Savings Account</option>
										<option value="current">Current Account</option>
										<option value="fixed-deposit">Fixed Deposit Account</option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									
								</div>
								<div class="col-sm-6">
									
								</div>
							</div>
						</div>

						<br />
						<div class="form-group">
							<button class="btn btn-info">Save & Update Account </button>
						</div>
					</form>
				</div>
	        </div>
	      </div>
	    </div>
	</div>

	{{-- paystack integration --}}
	<script src="https://js.paystack.co/v1/inline.js"></script>
	<script type="text/javascript">
		// load module
		fetchAllBanks();

		$("#fund-wallet").click(function (){
			var amount = parseFloat($("#fund_amount").val());
			var handler = PaystackPop.setup({
		    key: '{{env("PAYSTACK_PK_TEST")}}',
		    email: '{{ Auth::user()->email }}',
		    amount:  (amount * 100),
		    ref: 'YLD-'+Math.floor((Math.random() * 1000000000) + 1),
		    metadata: {
		      custom_fields: [
		        {
		          display_name: '',
		          variable_name: '',
		          value: ''
		        }
		      ]
		    },
		    callback: function(response){
		      // alert('window closed');
		      $("#payment-review").html('Payment was successful!, verifying...');
		      logWalletFunding(response.reference, amount);
		    },
		    onClose: function(){
		      // alert('window closed');
		    }
		});
			handler.openIframe();
		});

		function addAccount() {
			$("#add-banking").modal('show');
		}

		function addCard() {
			$("#add-card").modal('show');
		}

		function showFundForm() {
			$(".fund-form").toggle();
		}

		function logWalletFunding(reference, amount) {
			var token = $("#token").val();
			var params = {
				_token: token,
				reference: reference,
				amount: amount
			};

			$.post('{{url('log/wallet/payment')}}', params, function(data, textStatus, xhr) {
				if(data.status == "success"){
					// return true
					swal(
						"Ok",
						data.message,
						data.status
					);
					loadWalletBalance();
				}else{
					// return true
					swal(
						"oops",
						data.message,
						data.status
					);
				}
			});
		}

		// fetch all banks
		function fetchAllBanks() {
			$.get('{{ url("list/all/banks") }}', function(data) {
				$("#bank_name").html("");
				$.each(data.data, function(index, val) {
					$("#bank_name").append(`
						<option value="${val.name}">${val.name}</option>
					`);
				});
			});
		}
	</script>
@endsection