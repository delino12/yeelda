@extends('layouts.live')


@section('title')
  Commodity Market Live | Yeelda
@endsection

<link rel="stylesheet" type="text/css" href="/css/market.css">
<script src="/js/market.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


@section('contents')

<div class="market-shade">
  <section>
    <!--for demo wrap-->
    <h1 style="color: #fff; font-size: 14px;><i class="fa fa-bar-chart"></i> Commodity Stock Exchange Market</h1>
    <div >
      <table class="table" style="font-size: 12px; color:#ECB; ">
        <thead>
          <tr bgcolor="#387">
            <th>Code</th>
            <th>Commodity</th>
            <th>&#8358; Buy</th>
            <th>&#8358; Sell</th>
            <th>Qty</th>
            <th>Status</th>
            <th>Traffic</th>
            <th>Request</th>
            <td>Option</td>
            <th>Action</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>

        @foreach($commodities as $commodity)
        <tr>
          <td>{{ rand(000,999) }}</td>
          <td>{{ $commodity }}</td>
          <td>1.38</td>
          <td>1.38</td>
          <td>59,000</td>
          <td>Open</td>
          <td>High</td>
          <td><input type="text" name="" class="live-input" placeholder="10,000"></td>
          <td>
            <select class="live-input">
              <option value="buy">buy</option>
              <option value="sell">sell</option>
            </select>
          </td>

          <td>trade</td>
          <td>{{ date('M Y ') }}</td>
        </tr>
        @endforeach
          
        </tbody>
      </table>
    </div>
  </section>
</div>
@endsection
