@extends('layouts.skin')

@section('title')
  Start Payment | Yeelda
@endsection

@section('contents')

<div class="container">
  <div style="height: 50px;"></div>
  <!-- Load all equipments here --> 
  <div class="row" style="font-size: 13px;">
    <div class="col-md-12 text-center">
      <img src="/images/paystack.png" class="img" width="300" height="30%">
      <p>
        Total: &#8358; {{ number_format($amount, 2) }} Complete Payment Online using Debit/Credit. 
      </p>
      <hr />
      <center>
        <div style="width: 40%;height: 100px; box-shadow: 1px 1px 4px 1px #CCC;">
          <div id="payment-review"></div>
        </div>
      </center>
      <div class="success_msg"></div>
      <div class="error_msg"></div>
    </div>
  </div>
  <div style="height: 100px;"></div>
</div>
<div class="success_msg" style="position: fixed; z-index: 10; top: 80px; right: 20px;"></div>

{{-- paystack integration --}}
<script src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript">
  var handler = PaystackPop.setup({
    key: '{{env("PAYSTACK_PK_TEST")}}',
    email: '{{ $email }}',
    amount: {{ $amount }} * 100,
    ref: 'YLD-'+Math.floor((Math.random() * 1000000000) + 1),
    metadata: {
      custom_fields: [
        {
          display_name: '',
          variable_name: '',
          value: ''
        }
      ]
    },
    callback: function(response){
      // alert('window closed');
      $("#payment-review").html('Payment was successful!, verifying...');
      logPayment(response.reference);
    },
    onClose: function(){
      // alert('window closed');
      window.location.href = "/carts";
    }
  });
  handler.openIframe();
  

  // log payment
  function logPayment(reference){
    var email  = '{{ $email }}';
    var amount = '{{ $amount }}';
    var items  = '{{ $item_id }}';
    var token  = '{{ csrf_token() }}';

    // data to json
    var params = {
      _token: token,
      email: email,
      items: items,
      tranId: reference,
      amount: amount
    };

    // init post
    $.post('{{url('send/payment/request')}}', params, function(data) {
      console.log(data);
      if(data.status == "success"){
        window.location.href = "/produce-marketplace";
      }else{
        swal(
          "oops",
          data.message,
          data.status
        );
      }
    }).fail(function(err){
      window.location.href = "/carts";
    });
  }
</script>
@endsection

