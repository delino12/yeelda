@extends('layouts.skin')

@section('title')
  Hire Farming Equipments | Yeelda
@endsection

@section('contents')
  <br /><br /><br /><br /><br />
<div class="container" style="font-size: 11px;">
  <div class="row">
    <form method="post" action="/search">
      <div class="col-md-5">
        <div class="form-group">
          <input type="search" id="search" placeholder="search farm tools" class="form-control" required="" style="font-size: 12px;">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <button class="btn btn-primary-square"><i class="fa fa-search"></i> Search</button>
        </div>
      </div>
    </form>
  </div>
  <!-- Load all equipments here -->
  <div class="row">
    <div class="load-equipments"></div>
  </div>
</div>

<script type="text/javascript">
  $.get('/load/farmers/equipments', function (e){
    // console.log(e);
    $(".load-equipments").html("");
    var sn = 0;
    $.each(e, function (index, value){
      sn++;
      // console.log(value);
        $(".load-equipments").append(`
          <div class="col-md-3">
            <div class="card" style="padding:0.4em;">
              <img class="card-img-top img-rounded" src="/uploads/equipments/`+value.equipment_image+`" width="250" height="250" alt="Card image cap">
              <hr />
              <div class="card-block">
                <h4 class="lead" style="font-size: 13px;"> `+value.equipment_name+` <span class="badge">`+value.equipment_no+`</span></h4>
              </div>
              <table class="table">
                <tr>
                  <td>Availability</td>
                  <td>`+value.equipment_status+`</td>
                </tr>
                <tr>
                  <td>Delivery</td>
                  <td>`+value.equipment_delivery_type+`</td>
                </tr>
                <tr>
                  <td>Daily</td>
                  <td>&#8358;`+value.total+`</td>
                </tr>
              </table>
              <div class="lead">
                <span class="small" style="font-size: 13px;">
                    <a href="/ask-seller/`+value.owner_email+`">
                      <i class="fa fa-envelope"></i> Send Message
                    </a> 
                    - 
                    <a href="/request-hire/`+value.id+`">
                      <i class="fa fa-truck"></i> Hire now
                    </a>
                  </span> 
              </div>
            </div>
          </div>
        `);
    });
  });
</script>
@endsection

