@extends('layouts.skin')

@section('title')
  Hire Farming Equipments | Yeelda
@endsection

@section('contents')
<div class="container">
  <br /><br /><br /><br /><br /><br />
  <!-- Load all equipments here -->
  <div class="row" style="font-size: 13px;">
    <div class="load-equipments"></div>
  </div>
</div>
<div class="success_msg" style="position: fixed; z-index: 10; top: 80px; right: 20px;"></div>

<script type="text/javascript">
  var id = '{{ $id }}';
  $.get('/load/farmer/equipments/single/?id='+id, function (e){
    // console.log(e);
    $(".load-equipments").html('');
    var sn = 0;
    $.each(e, function (index, value){
      sn++;
      console.log(value);
        $(".load-equipments").append(`
          <div class="col-md-3">
            <div class="card" style="padding:0.4em;">
              <img class="card-img-top img-rounded" src="/uploads/equipments/`+value.equipment_image+`" width="250" height="250" alt="Card image cap">
              <hr />
              <div class="card-block">
                <h4 class="lead"> `+value.equipment_name+` <span class="badge">`+value.equipment_no+`</span></h4>
              </div>
              <table class="table">
                <tr>
                  <td>Availability</td>
                  <td>`+value.equipment_status+`</td>
                </tr>
                <tr>
                  <td>Delivery</td>
                  <td>`+value.equipment_delivery_type+`</td>
                </tr>
                <tr>
                  <td>Daily</td>
                  <td>&#8358;`+value.total+`</td>
                </tr>
                <tr>
                    <td>Contdition</td>
                    <td>
                      <span class="conditions">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>Description</td>
                    <td>`+value.equipment_descriptions+`</td>
                  </tr>
              </table>
              <div class="lead">
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <h1 class="lead">Specify Details & Duration</h1>
            <h1 class="lead small">Owner: <span class="owner_name"></span></h1>
            <p>
              <b class="text-danger">Note:</b> This transaction will be completed when users click on equipment 
              confirmation button, if on  days 3 equipment is not deliver users can cancel transaction. Hirer will receive a refund payment.
            </p>
            <table class="table">
              <tbody>
                <form>
                  <tr>
                    <td>Daily Cost</td>
                    <td>`+value.total+`</td>
                  </tr>
                  <input type="hidden" id="charges" value="`+value.equipment_price+`" />
                  <tr>
                    <td>How many days</td>
                    <td><input type="number" id="days" placeholder="No. of days" onkeyup="calculateCharge()" style="width: 100px;font-size:13px;" class="form-control" required=""></td>
                  </tr>
                  <tr>
                    <td>Discount (%)</td>
                    <td><span class="discount"></span></td>
                  </tr>
                  <tr>
                    <td>Fee</td>
                    <td><span class="fee"></span></td>
                  </tr>
                  <tr>
                    <td>Total Charges</td>
                    <td><span class="total-charges"></span></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>
                      <span>
                        <a href="javascript:void(0)" onclick="startPayment()">Pay Now</a>
                      </span> 
                    </td>
                  </tr>
                </form>
              </tbody>
            </table>
          </div>
        `);
    });
  });

  // send
  function sendHireRequest(){
    return false;
  }

  // show payment information div
  let Rave = document.$('.flwpug_getpaid');
  // Rave.dataset.PBFSecKey = 'FLWSECK-4127f15e63c9098402dcc7891798fb0f-X';
  // Rave.dataset.PBFPubKey = 'FLWPUBK-1cf610974690c2560cb4c36f4921244a-X';
  // Rave.dataset.txref = 'k983398abhsjd';
  Rave.dataset.amount = '9900';
  // Rave.dataset.customer_email = 'someone@email.com';

  // calculate price
  var amount = 0;
  function calculateCharge(){
    var charges = $('#charges').val();
    var days = $('#days').val();

    var total = days * charges;

    // return charges 
    $('.total-charges').html(` &#8358; `+total.toLocaleString());
    $('.discount').html(` &#8358; 0.00`);
    $('.fee').html(` &#8358; 0.00`);
    amount = total;
  }

  // function start payment
  function startPayment(){
    window.location.href = '/continue/payment/?amount='+amount;
  }
</script>
<script type="text/javascript" src="http://flw-pms-dev.eu-west-1.elasticbeanstalk.com/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
@endsection

