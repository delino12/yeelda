<div id="create-group-modal" class="modal" role="document" tabindex="-1">
	<div class="modal-dialog" role="dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					Create New Group
				</h5>
			</div>
			<div class="modal-body">
				<form id="create-group-form" method="post" onsubmit="return createGroup()">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label for="group_name">Enter Group Name</label>
								<input type="text" id="group_name" placeholder="Enter name..." class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<button class="btn btn-primary" id="add-group-btn">
									Add Group
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="edit-group-modal" class="modal" role="document" tabindex="-1">
	<div class="modal-dialog" role="dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<span class="edit-group-name"></span>
				</h5>
			</div>
			<div class="modal-body">
				<form id="edit-group-form" method="post" onsubmit="return updateGroup()">
					<input type="hidden" id="edit_group_id" name="">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label for="edit_group_name">Enter Group Name</label>
								<input type="text" id="edit_group_name" placeholder="Enter new name..." class="form-control" required="">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<button class="btn btn-primary" id="edit-group-btn">
									Update Group
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="member-group-modal" class="modal" role="document" tabindex="-1">
	<div class="modal-dialog" role="dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					Start Auto-Synchronization
				</h5>
			</div>
			<div class="modal-body">
				<form id="edit-group-form" method="post" onsubmit="return startAutoSync()">
					<div class="form-group">
						<select id="group_id" class="form-control">
							<option value="0">-- Select Group --</option>
						</select>
					</div>
					<div class="form-group">
						<select id="account_type" class="form-control">
							<option value="0">-- Select Account Type --</option>
							<option value="1">Farmers </option>
							<option value="2">Services Providers </option>
							<option value="3">Buyers </option>
						</select>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<button class="btn btn-primary" id="edit-group-btn">
									Start
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Search and Replace -->
<div class="modal fade" id="show-search-replace-modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="lead">Search and Replace</h4>
      </div>
      <div class="modal-body">
        <form method="post" onsubmit="return startFindReplace()">
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
        				<input type="text" class="form-control" id="l-find" placeholder="Enter search keyword" required="">
        			</div>
        			<div class="col-md-6">
        				<input type="text" class="form-control" id="r-replace" placeholder="Enter replace keyword" required="">
        			</div>
        		</div>
        		
        	</div>
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
	        			<button class="btn btn-primary" id="search-replace-btn">
	        				Search
	        			</button>
	        		</div>
	        		<div class="col-md-6">
        				<div id="fr-results"></div>
        			</div>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button class="btn btn-flat" type="button" data-dismiss="modal">
            close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Search and Replace -->
<div class="modal fade" id="show-search-replace-modal-errors" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="lead">Search and Replace</h4>
      </div>
      <div class="modal-body">
      	<p>Correct LGAs</p>
        <form method="post" onsubmit="return startFindReplaceKeywordsLga()">
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-12">
        				<input type="text" class="form-control" id="sort_keywords_lga" placeholder="Enter search keyword" required="">
        			</div>
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
        				<select onchange="getLgas()" id="sort_state" class="form-control">
                          <option value="">select state</option>
                        </select>
        			</div>
        			<div class="col-md-6">
                        <select onchange="getClusters()" id="sort_lgas" class="form-control">
                            <option value="">select lgas</option>
                        </select>
                    </div>
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
	        			<button class="btn btn-primary" id="lga-search-replace-btn">
	        				Search and Replace LGA
	        			</button>
	        		</div>
	        		<div class="col-md-6">
        				<div id="e-fr-results"></div>
        			</div>
        		</div>
        	</div>
        </form>

        <div style="height: 3rem;"></div>
        <p>Correct Clusters</p>
        <form method="post" onsubmit="return startFindReplaceKeywordsCluster()">
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
        				<input type="text" class="form-control" id="c_sort_keywords_clusters" placeholder="Enter search keyword" required="">
        			</div>
        			<div class="col-md-6">
        				<select onchange="getLgas2()" id="c_sort_state" class="form-control">
                          <option value="">select state</option>
                        </select>
        			</div>
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
        				<select onchange="getClusters2()" id="c_sort_lgas" class="form-control">
                            <option value="">select lga</option>
                        </select>
        			</div>
        			<div class="col-md-6">
                        <select class="form-control" id="c_sort_error_clusters" required="">
                        	<option value="">select cluster</option>
                        </select>
                    </div>
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="row">
        			<div class="col-md-6">
	        			<button class="btn btn-primary" id="c-search-replace-btn">
	        				Search and Replace Clusters
	        			</button>
	        		</div>
	        		<div class="col-md-6">
        				<div id="e-fr-results"></div>
        			</div>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button class="btn btn-flat" type="button" data-dismiss="modal">
            close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Search and Replace -->
<div class="modal fade" id="show-search-replace-modal-state" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="lead">Search and Replace State Keywords</h4>
      </div>
      <div class="modal-body">
        <p>Correct State with errors</p>
        <form method="post" onsubmit="return startFindReplaceKeywordsState()">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="sort_keywords_state" placeholder="Enter search keyword" required="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <select onchange="getLgas3()" id="s_sort_state_2" class="form-control">
                          <option value="">select state</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select id="s_sort_lgas_2" class="form-control">
                            <option value="">select lgas</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" id="state-search-replace-btn">
                            Search and Replace States
                        </button>
                    </div>
                    <div class="col-md-6">
                        <div id="s-fr-results"></div>
                    </div>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button class="btn btn-flat" type="button" data-dismiss="modal">
            close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>