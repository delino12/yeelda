<!-- Default Light Table -->
<div class="row">
  <div class="col-md-12">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">Admin Messaging System</h6>
      </div>
      <div class="card-body p-4 pb-3">
        <div class="row">
          <div class="col-md-8">
            <h1 class="lead"><i class="fa fa-envelope"></i> Messages (Mail)</h1>
            Sent Messages 
            <a href="/admin/view/sent/mail">view sent Mail messages</a>
            <hr />
              <div class="a_success_msg"></div>
              <div class="a_error_msg"></div>
              <form class="message-form" method="post" onsubmit="return sendMessages()">
                <div class="form-group">
                  <label>Select Recipient</label>
                  <select class="form-control" id="type" onchange="checkReceiver()">
                    <option value="all">all</option>
                    <option value="farmers">Farmers Only</option>
                    <option value="buyers">Buyers Only</option>
                            <option value="service">Service Providers Only</option>
                            <option value="single">Single User Only</option>
                    <option value="multiple">Multiple Users</option>
                  </select>
                </div>
                <div id="mail-single" class="form-group" style="display: none;">
                  <label>To</label>
                  <input type="email" id="single-mail" class="form-control" placeholder="Email here..">
                </div>
                <div id="mail-multiple" class="form-group" style="display: none;">
                  <label>To</label>
                  <textarea class="form-control" id="multiple-emails" cols="2" rows="5" placeholder="Paste all email address here... Eg, first@example.com, second@example.com.."></textarea>
                </div>
                <div class="form-group">
                  <label>Subject</label>
                  <input type="text" class="form-control" id="subject" placeholder="Subject" required="">
                </div>
                <div class="form-group">
                  <label>Message</label>
                  <textarea class="form-control" id="body" cols="15" rows="10" placeholder="Type a message.." required=""></textarea>
                </div>                       
                <div class="form-group">
                  <button class="btn btn-primary col-md-12">
                    Send Messages 
                    <img src="/svg/loading.svg" id="loading" class="pull-right" width="40" height="20" style="display: none;">
                  </button>
                </div>
              </form>
          </div>
          <div class="col-md-4">
            <h1 class="lead"><i class="fa fa-envelope-o"></i> Stamp Messages</h1><hr />
            Sent Messages <a href="/admin/view/sent/stamp">view stamp messages</a>
            <hr />
            <div class="stamp_success_msg"></div>
            <div class="stamp_error_msg"></div>
            <form class="stamp-message-form" method="post" onsubmit="return sendStampMessage()">
              <div class="form-group">
                <label>Select Recipient</label>
                <select class="form-control" id="stamp-type">
                  <option value="none"> --none-- </option>
                  <option value="all">all</option>
                  <option value="farmers">Farmers Only</option>
                  <option value="buyers">Buyers Only</option>
                  <option value="services">Service Providers Only</option>
                </select>
              </div>
              <div class="form-group">
                <label>Message</label>
                <textarea class="form-control" id="stamp-message" cols="10" rows="2" placeholder="Type a message.." required=""></textarea>
              </div>

              <div class="form-group">
                <button class="btn btn-primary col-md-12">Send Flash Message</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Default Light Table -->

<!-- Default Light Table -->
<div class="row">
  <div class="col-md-12">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">Farming equipment</h6>
      </div>
      <div class="card-body p-4 pb-3">
        <div class="row">
          <div class="col-md-6">
            <h1 class="lead"><i class="fa fa-phone"></i> SMS Messages</h1> 
                <span class="success_msg" style="margin-left: 10px;"></span>
                <span class="error_msg" style="margin-left: 10px;"></span>
                <hr />
              <form class="sms-message-form" method="post" onsubmit="return sendSMS()">
                <div class="form-group">
                  <label>Select Recipient</label>
                  <select class="form-control" onchange="setRecievers()" id="sms_type">
                            <option value="none">none</option>
                    <option value="all">all</option>
                    <option value="farmers">Farmers Only</option>
                    <option value="buyers">Buyers Only</option>
                    <option value="services">Service Providers Only</option>
                            <option value="single">Single User Only</option>
                            <option value="multiple">Multiple User Only</option>
                  </select>
                </div>

                    {{-- single & multiple --}}
                    <div class="form-group">
                        <div id="single" style="display: none;">
                            <label>To single</label>
                            <input type="text" id="single-receiver" class="form-control" placeholder="example: 08038989898" maxlength="14">
                        </div>
                        <div id="multiple" style="display: none;">
                            <label>To -> 
                                <span class="small pull-right">
                                    <b class="text-danger">Note:</b> multiple phone numbers uses comma(,) to separate numbers. Max limit is 20
                                </span>
                            </label>
                            <textarea id="multiple-receiver" class="form-control" placeholder="example: 080673673673,080773673633..." cols="10" rows="5"></textarea>

                        </div>
                    </div>

                <div class="form-group">
                  <label>Message</label>
                  <textarea class="form-control" onkeyup="caculateCharacters()" id="sms_message" cols="10" rows="2" placeholder="Type a message.." required="" maxlength="160"></textarea>
                        <span class="count-character"></span>
                </div>
                
                <div class="form-group">
                  <button class="btn btn-primary col-md-12">
                            Send Messages
                            <img src="/svg/loading.svg" id="sms_loading" class="pull-right" width="40" height="20" style="display: none;">
                        </button>
                </div>
              </form>
          </div>

          <div class="col-md-6">
              <h1 class="lead">SMS Contact Status</h1>
              <p>SMS log details. <a href="/admin/view/sent/sms">view sent SMS</a> </p>
              <hr />
              <i class="fa fa-envelope"></i> Contact <span class="total_sent_sms pull-right"></span>
              <table class="table">
                  <tbody class="load-sms-log"></tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Default Light Table -->