<!-- Default Light Table -->
<div class="row">
  <div class="col-md-12">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">
          Shipped Produce
          <a style="float: right;" href="javascript:void(0);" onclick="showAddLogistic()">
            <i class="fa fa-plus"></i> Create New 
          </a>
        </h6>
      </div>
      <div class="card-body p-0 pb-3">
        <table class="table">
          <thead>
            <tr>
              <th>Courier Service</th>
              <th>Agent Assigned</th>
              <th>Location</th>
              <th>Destination</th>
              <th>Tracking No.</th>
              <th>Status</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody class="load-tracking-products">
            <tr>
              <td>Loading...</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End Default Light Table -->

{{-- add logistics modal --}}
<div id="add_logistic" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         Add new shippment
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Product Reference</label>
              <input class="form-control" onblur="getTransactionInfo()" id="product_ref" placeholder="Enter reference no.">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Carrier Service ( Agent)</label>
              <input type="text" class="form-control" id="carrier_name" placeholder="Eg, Yeelda Express Delivery Services" name="">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Assignee</label>
              <input type="text" class="form-control" id="assignee" placeholder="Eg, John Henry">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Status</label>
              <input type="text" class="form-control" id="status" placeholder="none" readonly>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Seller</label>
              <input type="text" class="form-control" id="seller" placeholder="none" readonly>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Buyer</label>
              <input type="text" class="form-control" id="buyer" placeholder="none" readonly>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Dispatch Date</label>
              <input type="date" id="start_date" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Delivery Date</label>
              <input type="date" id="deliver_date" class="form-control">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Pickup Location</label>
              <textarea class="form-control" id="pickup_address" placeholder="Enter a pickup location, Eg. Yeelda warehouse district"></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Destination</label>
              <textarea class="form-control" id="destination_address" placeholder="Enter a pickup location, Eg. Buyer's factory"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="add_product_btn" class="btn btn-default">Create Receipt</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>

{{-- update logistics modal --}}
<div id="update_logistic" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Update <span class="bwl_no"></span>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Product Reference</label>
              <input class="form-control" id="bwl_ref" placeholder="Enter reference no." readonly>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Assignee</label>
              <input type="text" class="form-control" id="bwl_assignee" placeholder="Eg, John Henry">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Status</label>
              <select class="form-control" id="bwl_status">
                <option value="">--none--</option>
                <option value="Pick Up">Pick Up</option>
                <option value="Checkpoint">Checkpoint</option>
                <option value="In Transit">In Transit</option>
                <option value="On Hold"> Hold</option>
                <option value="Held"> Held</option>
                <option value="Seize">Seize</option>
                <option value="Delivered">Delivered</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Delivery date</label>
              <input type="date" id="bwl_delivery_date" class="form-control">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Reason for delay (Optional) </label>
              <textarea class="form-control" id="bwl_reasons" placeholder=""></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Transport System (Update on change of transportation) </label>
              <textarea class="form-control" id="bwl_transport_type" placeholder="Enter type of transportation system.."></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Current Location (New Transit location)</label>
              <textarea class="form-control" id="bwl_location_address" placeholder="Enter a pickup location, Eg. Buyer's factory"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="update_product_btn" class="btn btn-default">Update Package</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
