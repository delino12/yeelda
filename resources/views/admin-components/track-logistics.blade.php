{{-- update logistics modal --}}
<div id="update_logistic" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        Update <span class="bwl_no"></span>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Product Reference</label>
              <input class="form-control" id="bwl_ref" placeholder="Enter reference no." readonly>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Assignee</label>
              <input type="text" class="form-control" id="bwl_assignee" placeholder="Eg, John Henry">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Status</label>
              <select class="form-control" id="bwl_status">
                <option value="">--none--</option>
                <option value="Pick Up">Pick Up</option>
                <option value="Checkpoint">Checkpoint</option>
                <option value="In Transit">In Transit</option>
                <option value="On Hold"> Hold</option>
                <option value="Held"> Held</option>
                <option value="Seize">Seize</option>
                <option value="Delivered">Delivered</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Delivery date</label>
              <input type="date" id="bwl_delivery_date" class="form-control">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Reason for delay (Optional) </label>
              <textarea class="form-control" id="bwl_reasons" placeholder=""></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Transport System (Update on change of transportation) </label>
              <textarea class="form-control" id="bwl_transport_type" placeholder="Enter type of transportation system.."></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Current Location (New Transit location)</label>
              <textarea class="form-control" id="bwl_location_address" placeholder="Enter a pickup location, Eg. Buyer's factory"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="update_product_btn" class="btn btn-default">Update Package</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
