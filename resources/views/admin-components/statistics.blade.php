<!-- Default Light Table -->
<div class="row">
  <div class="col-md-6">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">Operations (Platform users)</h6>
      </div>
      <div class="card-body p-4 pb-3">
        <table class="table">
          <thead>
            <tr>
              <th>Description</th>
              <th class="text-success">Active Users</th>
              <th class="text-info">ODCA Users</th>
            </tr>
          </thead>
          <tbody class="users-statistic">
            <tr>
              <td>
                Loading...
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">Premium Subscribers</h6>
      </div>
      <div class="card-body p-4 pb-3">
        <table class="table">
          <thead>
            <tr>
              <th>Description</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Total subscribed Farmers</td>
              <td></td>
            </tr>

            <tr>
              <td>Total subscribed Services Providers</td>
              <td></td>
            </tr>

            <tr>
              <td>Total subscribed Buyers</td>
              <td></td>
            </tr>

            <tr>
              <td>Total Subscription</td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End Default Light Table -->

<!-- Default Light Table -->
<div class="row">
  <div class="col-md-6">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">Regional (Platform users)</h6>
      </div>
      <div class="card-body p-4 pb-3">
        <table class="table">
          <thead>
            <tr>
              <th>Farmers</th>
              <th>No. of Users Found</th>
            </tr>
          </thead>
          <tbody class="farmers-by-regions">
            <tr>
              <td>Loading...</td>
              <td></td>
            </tr>
          </tbody>
        </table>

        <table class="table">
          <thead>
            <tr>
              <th>Services</th>
              <th>No. of Users Found</th>
            </tr>
          </thead>
          <tbody class="services-by-regions">
            <tr>
              <td>Loading...</td>
              <td></td>
            </tr>
          </tbody>
        </table>

        <table class="table">
          <thead>
            <tr>
              <th>Buyers</th>
              <th>No. of Users Found</th>
            </tr>
          </thead>
          <tbody class="buyers-by-regions">
            <tr>
              <td>Loading...</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card card-small mb-4">
      <div class="card-header border-bottom">
        <h6 class="m-0">Extension Agent</h6>
      </div>
      <div class="card-body p-4 pb-3">
        <table class="table" id="load-agent-table"  data-pagination="true" data-search="true"data-page-size="10">
          <thead>
            <tr>
              <th>S/N</th>
              <th>Name</th>
              <th>Agent Code</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody class="load-all-agents">
            <tr>
              <td><img src="/svg/yeelda-loading.svg" height="60" width="auto" /></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- End Default Light Table -->