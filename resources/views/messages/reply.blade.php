@extends('layouts.message-skin')

@section('title')
	Messages | Yeelda
@endsection


@section('contents')
{{-- custom css --}}
<style type="text/css">
	.chat-wrapper {
		height: 280px;
    	overflow-y: scroll;
	}

	.chat-box-right {
		padding: 1em;
		font-size: 13px;
		width: 40%;
		box-shadow: 0px 0px 2px 1px #CCC;
		border-top-right-radius: 1px;
		border-bottom-right-radius: 15px;
		border-bottom-left-radius: 25px;
	}

	.chat-box-left {
		padding: 1em;
		font-size: 13px;
		width: 40%;
		position: relative;
		border-bottom-right-radius: 25px;
		border-bottom-left-radius: 15px;
		border-top-left-radius: 1px;
		left: 55%;
		box-shadow: 0px 0px 2px 1px #CCC;
	}

</style>
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		@if(Auth::check())
			    <a href="/farmer/dashboard"> {{Auth::user()->name}} </a>
			@endif
		</li>
		<li class="breadcrumb-item active">Messages</li>
		<li class="breadcrumb-item">
			<span class="pull-right">
            	<span class="topic"></span>
            </span>
        </li>
	</ol>

	<div class="row">
		<div class="col-md-10">
		   	<div class="panel panel-default widget">
	            <div class="panel-heading">
	                <h3 class="lead" style="margin-left: 10px;"><i class="fa fa-comments-o"></i> Reply 
	                	{{ $email }}
	                </h3>
	                <hr />
	            </div>
	            <div class="panel-body">
		            <div class="chat-wrapper">
		            	<div class="chat-messages"></div>
		            </div>
		            <form method="post" onsubmit="return sendReply()" id="message-form">
						{{ csrf_field() }}
						<input type="hidden" id="user_email" name="user_email" value="{{ Auth::user()->email }}">
		 				<input type="hidden" id="msg_id" name="msg_id" value="{{ $msg_id }}">
		 				<input type="hidden" id="msg_from" name="msg_from" value="{{ $email }}">
						
						<div class="form-group">
							<label for="textarea">Reply Message</label>
							<textarea class="form-control" cols="2" rows="3" id="body" placeholder="Type your message here..." required=""></textarea>
						</div>
					
						<div class="form-group">
							<button class="btn btn-primary col-md-12">Send</button>
						</div>
					</form>
		        </div>
		    </div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-8">
			<br /><br />
			
		</div>
	</div>
</div>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script type="text/javascript">
	// current logged_in user
	var loggedUser = $("#user_email").val();
	var msgId 	   = $("#msg_id").val();
	var email 	   = $("#msg_from").val();
	
	// load message
	$.get('/get/reply/'+msgId+'/'+email+'/'+loggedUser, function (data){
		// console.log(data);
		$(".chat-body").html("");
		$.each(data, function (index, value){

			if(value.from !== loggedUser){
                $('.chat-messages').append(`
                    <div class="chat-box-right">
                        <i class="fa fa-user"></i> <span class="">`+value.from+`</span><br />
                        <span class="text-warning">`+value.body+`</span><br />
                        <span class="small pull-right">`+value.date+`</span>
                        <br />
                    </div>
                    <br />
                `);
            }else{
                $('.chat-messages').append(`
                    <div class="chat-box-left">
                        <i class="fa fa-user"></i> <span class="">Me: </span><br />
                        <span class="text-success">`+value.body+`</span><br />
                        <span class="small pull-right">`+value.date+`</span>
                        <br />
                    </div>
                    <br />
                `);
            }

            $('.chat-wrapper').stop().animate({
                scrollTop: $(".chat-wrapper")[0].scrollHeight
            }, 800);

            if(value.topic){
            	$('.topic').html(`
					<b>`+value.topic+`</b>
				`);
            }
		});
	});

	// send reply
	function sendReply() {
		// body...
		var token   = '{{ csrf_token() }}';
		var msgId   = $('#msg_id').val();
		var msgFrom = $('#user_email').val();
		var msgTo   = $('#msg_from').val();
		var msgBody = $('#body').val();

		// data to json
		var data = {
			_token:token,
			msgId:msgId,
			msgFrom:msgFrom,
			msgTo:msgTo,
			msgBody:msgBody
		}

		// post data to ajax
		$.ajax({
			url: '/send/reply',
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data){
				console.log(data)
				$('#message-form')[0].reset();
			},
			error: function (data){
				console.log(data)
				alert('Fail to send reply... ');
			}
		});

		// return false
		return false;
	}

	// update messages with pusher
    // Pusher.logToConsole = true;
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
        encrypted: false,
        cluster: "eu"
    });
    
    // listent to chat on channels 
    var channel = pusher.subscribe('new-reply');
    channel.bind('Yeelda\\Events\\ReplyMessage', function(data) {
        var audio = new Audio('/audio/notify.wav');
        audio.pause();
        audio.play();

        // console.log(data);
        var value = data;
		if(value.from !== loggedUser){
            $('.chat-messages').append(`
                <div class="chat-box-right">
                    <span class="">`+value.from+`</span><br />
                    <span class="text-warning">`+value.body+`</span><br />
                    <span class="small">`+value.date+`</span>
                    <br />
                </div>
                <br />
            `);
        }else{
            $('.chat-messages').append(`
                <div class="chat-box-left">
                    <span class="">Me: </span><br />
                    <span class="text-success">`+value.body+`</span><br />
                    <span class="small">`+value.date+`</span>
                    <br />
                </div>
            `);
        }

        $('.chat-wrapper').stop().animate({
            scrollTop: $(".chat-wrapper")[0].scrollHeight
        }, 800);
	});
</script>
@endsection