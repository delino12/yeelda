@extends('layouts.message-skin')

@section('title')
	Messages | Yeelda
@endsection

@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		@if(Auth::user()->account_type == 'farmer')
			    <a href="/farmer/dashboard"> {{Auth::user()->name}} </a>
			@elseif(Auth::user()->account_type == 'buyer')
			    <a href="/investors/dashboard"> {{Auth::user()->name}} </a>
			@elseif(Auth::user()->account_type == 'service')
			    <a href="/service-provider/dashboard"> {{Auth::user()->name}} </a>
			@endif
		</li>
		<li class="breadcrumb-item active">Sent Messages</li>
	</ol>

	<div class="row">
		<div class="col-md-12">
			<h3 class="lead"><i class="fa fa-comments-o"></i> Inbox (<span class="total-msg"></span>)</h3>
			<div class="panel-body"></div>
		</div>
	</div>
</div>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script type="text/javascript">
	// current logged_in user
	var loggedUser = $("#user_email").val();

	// load
	// Pusher.logToConsole = true;
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
        encrypted: false,
        cluster: "eu"
    });

    var channel = pusher.subscribe('new-message');
    channel.bind('Yeelda\\Events\\NewMessage', function(data) {
    	var value = data;
      	// console.log(data);
      	if(value.to == loggedUser){
      		var container = $(".panel-body");   
	        var markup = `
	        	<ul class="list-group">
	                <li class="list-group-item">
	                    <div class="row">
	                        <div class="col-md-1">
	                            <img src="http://www.iconninja.com/files/929/593/210/message-email-envelope-mail-letter-icon.svg" width="80%" height="80%" class="img-circle img-responsive" alt="Message" />
	                        </div>
	                        <div class="col-md-11">
	                            <div>
	                                Subject: <a href="#">`+value.subject+`</a>
	                                <div class="mic-info">
	                                    From: <a href="#">`+value.from+`</a>  
	                                    <span class="pull-right"><i class="fa fa-clock-o"></i> `+value.date+`</span>
	                                    <br />
	                                </div>
	                            </div>
	                            <div class="comment-text">
	                                `+value.body+`
	                            </div>
		                        
							    <a class="btn btn-sm" href="/message/service/reply/`+value.id+`/`+value.from+`" class="click_reply">
							    	<i class="fa fa-reply"></i> Reply
							    </a>
							    <a href="#" class="btn btn-sm">
							      	<i class="fa fa-trash"></i> Delete
							  	</a>     
		                    </div>
		                </div>
		            </li> 
		        </ul>
	        `;

	        container.prepend(markup);
      	}
    });

	// at loaded dom
	// var id = $("#user_id").val();
	var email = $("#user_email").val();
	// load message 
	$.get('/load/messages', function (data){
		// console.log(data);
		$(".incoming-messages-div").html("");
		$.each(data, function (index, value){
			if(value.to == loggedUser){
				$(".panel-body").append(`
					<ul class="list-group">
		                <li class="list-group-item">
		                    <div class="row">
		                        <div class="col-md-1">
		                            <img src="http://www.iconninja.com/files/929/593/210/message-email-envelope-mail-letter-icon.svg" width="80%" height="80%" class="img-circle img-responsive" alt="Message" />
		                        </div>
		                        <div class="col-md-11">
		                            <div>
		                                Subject: <a href="#">`+value.subject+`</a>
		                                <div class="mic-info">
		                                    From: <a href="#">`+value.from+`</a>  
		                                    <span class="pull-right"><i class="fa fa-clock-o"></i> `+value.date+`</span>
		                                    <br />
		                                </div>
		                            </div>
		                            <div class="comment-text">
		                                `+value.body+`
		                            </div>
			                        
								    <a class="btn btn-sm" href="/message/service/reply/`+value.id+`/`+value.from+`" class="click_reply">
								    	<i class="fa fa-reply"></i> Reply
								    </a>
								    <a href="#" class="btn btn-sm">
								      	<i class="fa fa-trash"></i> Delete
								  	</a>     
			                    </div>
			                </div>
			            </li> 
			        </ul>
				`);
			}
		});
	});

	// send message from form
	function sendMessage(){
		$('.loading').show();
		var token = $("input[name=_token]").val();
		var to    = $("#to").val();
		var from  = $("#user_email").val();
		var subj  = $("#subj").val();
		var body  = $("#body").val();

		var data = {
			_token:token,
			from:from,
			to:to,
			subj:subj,
			body:body
		};

		// send  via Ajax
		$.ajax({
			type: "post",
			url: "/send/messages",
			data: data,
			cache: false,
			success: function (data){
				$("#message-stat").html(`
					`+data.message+`
				`);
				$("#message-form")[0].reset();
				$("#message-form").hide();
				$('.loading').hide();
			},
			error: function (data){
				alert('Fail to send message');
				$('.loading').hide();
			}
		});

		return false;
	}

	// save draft messages
	function saveDraft(){
		$('.loading-draft').text('Saving....');
		// form data
		var token = $("input[name=_token]").val();
		var to    = $("#to").val();
		var from  = $("#user_email").val();
		var subj  = $("#subj").val();
		var body  = $("#body").val();

		if(subj == ''){
			$('.error_subj').html(`
				<span class="text-danger">
					Subject is empty...
				</span>
			`);
			return false;
		}

		if(body == ''){
			$('.error_body').html(`
				<span class="text-danger">
					Message is empty...
				</span>
			`);
			return false;
		}

		var data = {
			_token:token,
			from:from,
			to:to,
			subj:subj,
			body:body
		};

		$.ajax({
			url: '/save/draft',
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data){
				// console.log(data);
				$('.loading-draft').text('Saved..');
			},
			error: function (data){
				$('.loading-draft').text('Error saving draft message');
				alert('Error, Fail to send save draft request');
			}
		});
	}

	// load sents options
	$.get('/load/user/sents/'+loggedUser, function(data) {
		/*optional stuff to do after success */
		console.log(data);
	});

	// load draft options
	$.get('/load/user/draft/'+loggedUser, function(data) {
		/*optional stuff to do after success */
		console.log(data);
	});

	// load trash options
	$.get('/load/user/trash/'+loggedUser, function(data) {
		/*optional stuff to do after success */
		console.log(data);
	});
</script>
@endsection