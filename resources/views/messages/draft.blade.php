@extends('layouts.message-skin')

@section('title')
	Messages | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		@if(Auth::user()->account_type == 'farmer')
			    <a href="/farmer/dashboard"> {{Auth::user()->name}} </a>
			@elseif(Auth::user()->account_type == 'buyer')
			    <a href="/investors/dashboard"> {{Auth::user()->name}} </a>
			@elseif(Auth::user()->account_type == 'service')
			    <a href="/service-provider/dashboard"> {{Auth::user()->name}} </a>
			@endif
		</li>
		<li class="breadcrumb-item active">Messages</li>
	</ol>
	<style type="text/css">
		.widget .panel-body { padding:0px; }
		.widget .list-group { margin-bottom: 0; }
		.widget .panel-title { display:inline }
		.widget .label-info { float: right; }
		.widget li.list-group-item {border-radius: 0;border: 0;border-top: 1px solid #ddd;}
		.widget li.list-group-item:hover { background-color: rgba(86,61,124,.1); }
		.widget .mic-info { color: #666666;font-size: 13px; }
		.widget .action { margin-top:5px; }
		.widget .btn-block { border-top-left-radius:0px;border-top-right-radius:0px; }
	</style>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
		 		<div class="row">
		 			<div class="col-md-7">
		 				<div class="row no-padding">
						   	<div class="panel panel-default widget">
					            <div class="panel-heading">
					                <h3 class="lead" style="margin-left: 10px;"><i class="fa fa-comments-o"></i> Draft Messages </h3>
					            </div>
					            <div class="panel-body"></div>
						    </div>
						</div>
		 			</div>
		 		</div>
			</div>
		</div>
	</div>
</div>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script type="text/javascript">
	// current logged_in user
	var loggedUser = $("#user_email").val();

	// load draft options
	$.get('/load/user/draft/'+loggedUser, function(data) {
		/*optional stuff to do after success */
		// console.log(data);
		$(".panel-body").html("");
		$.each(data, function (index, value){

			$(".panel-body").append(`
				<ul class="list-group">
	                <li class="list-group-item">
	                    <div class="row">
	                        <div class="col-md-1">
	                            <img src="http://www.iconninja.com/files/929/593/210/message-email-envelope-mail-letter-icon.svg" width="80%" height="80%" class="img-circle img-responsive" alt="Message" />
	                        </div>
	                        <div class="col-md-11">
	                            <div>
	                                Subject: <a href="#">`+value.subject+`</a>
	                                <div class="mic-info">
	                                    To: <a href="#">`+value.to+`</a>  
	                                    <span class="pull-right"><i class="fa fa-clock-o"></i> `+value.date+`</span>
	                                    <br />
	                                </div>
	                            </div>
	                            <div class="comment-text">
	                                `+value.body+`
	                            </div>
		                        
							    <a href="#" class="btn btn-sm">
							      	<i class="fa fa-trash"></i> Delete
							  	</a>     
		                    </div>
		                </div>
		            </li> 
		        </ul>
			`);
		});
	});
</script>
@endsection