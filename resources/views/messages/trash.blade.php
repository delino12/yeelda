@extends('layouts.message-skin')

@section('title')
	Messages | Yeelda
@endsection


@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		@if(Auth::guard('farmer')->check())
			    <a href="/farmer/dashboard"> {{Auth::guard('farmer')->user()->name}} </a>
			@elseif(Auth::guard('investor')->check())
			    <a href="/investors/dashboard"> {{Auth::guard('investor')->user()->name}} </a>
			@elseif(Auth::guard('sp')->check())
			    <a href="/service-provider/dashboard"> {{Auth::guard('sp')->user()->name}} </a>
			@endif
		</li>
		<li class="breadcrumb-item active">Messages</li>
	</ol>
	<style type="text/css">
		.widget .panel-body { padding:0px; }
		.widget .list-group { margin-bottom: 0; }
		.widget .panel-title { display:inline }
		.widget .label-info { float: right; }
		.widget li.list-group-item {border-radius: 0;border: 0;border-top: 1px solid #ddd;}
		.widget li.list-group-item:hover { background-color: rgba(86,61,124,.1); }
		.widget .mic-info { color: #666666;font-size: 13px; }
		.widget .action { margin-top:5px; }
		.widget .btn-block { border-top-left-radius:0px;border-top-right-radius:0px; }
	</style>
	<div class="row">
		<div class="col-12">
			<div class="row">
				<table class="table">
					<tbody class="load-msg"></tbody>
				</table>
			</div>
	 		<div class="row">
	 			<div class="col-md-7">
	 				<div class="row no-padding">
					   	<div class="panel panel-default widget">
				            <div class="panel-heading">
				                <h3 class="lead" style="margin-left: 10px;"><i class="fa fa-comments-o"></i> Inbox (<span class="total-msg"></span>)</h3>
				            </div>
				            <div class="panel-body"></div>
					    </div>
					</div>
	 			</div>
	 			<div class="col-md-5" id="create-new-div">
	 				<div id="message-stat"></div>
	 				<div id="reply-div"></div>
	 				<form method="post" onsubmit="return sendMessage()" id="message-form">
	 					{{ csrf_field() }}
	 					@if(Auth::guard('farmer')->check())
						    <input type="hidden" id="user_email" value="{{ Auth::guard('farmer')->user()->email }}">
						@elseif(Auth::guard('investor')->check())
						    <input type="hidden" id="user_email" value="{{ Auth::guard('investor')->user()->email }}">
						@elseif(Auth::guard('sp')->check())
						    <input type="hidden" id="user_email" value="{{ Auth::guard('sp')->user()->email }}">
						@endif
	 					<!-- Check compact -->
	 					@if(session('email'))
	 						
		 					<div class="form-group">
		 						<label for="to">To</label>
		 						<input type="text" id="to" placeholder="someoneemail@example.com" maxlength="25" required="" value="{{ session('email') }}" class="form-control col-md-6">
		 					</div>
	 					@else
	 						<div class="form-group">
		 						<label for="to">To</label>
		 						<input type="text" id="to" placeholder="email@example.com" maxlength="25" required="" class="form-control">
		 					</div>
	 					@endif

	 					<div class="form-group">
	 						<label for="name">Subject</label>
	 						<input class="form-control" type="text" id="subj" placeholder="Subject.." required="" />
	 					</div>

	 					<div class="form-group">
	 						<label for="textarea">Message:</label>
	 						<textarea class="form-control" cols="40" rows="8" id="body" placeholder="Type your message here..." required=""></textarea>
	 					</div>
	 				
	 					<div class="form-group">
	 						<button class="btn btn-primary">Send 
	 							<img src="/svg/three-dots.svg" width="20px" height="20px" style="display: none;" class="loading pull-right">
	 						</button> 
	 						<a class="btn btn-link" href="#">Save as Draft</a>
	 					</div>
	 				</form>
	 			</div>
	 		</div>
		</div>
	</div>
</div>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script type="text/javascript">
	// current logged_in user
	var loggedUser = $("#user_email").val();

	// load trash options
	$.get('/load/user/trash/'+loggedUser, function(data) {
		/*optional stuff to do after success */
		// console.log(data);
	});
</script>
@endsection