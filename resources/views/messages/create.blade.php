@extends('layouts.message-skin')

@section('title')
	Messages | Yeelda
@endsection

@section('contents')
<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
	  		@if(Auth::user()->account_type == 'farmer')
			    <a href="/farmer/dashboard"> {{Auth::user()->name}} </a>
			@elseif(Auth::user()->account_type == 'buyer')
			    <a href="/investors/dashboard"> {{Auth::user()->name}} </a>
			@elseif(Auth::user()->account_type == 'service')
			    <a href="/service-provider/dashboard"> {{Auth::user()->name}} </a>
			@endif
		</li>
		<li class="breadcrumb-item active">Messages</li>
	</ol>
	<style type="text/css">
		.widget .panel-body { padding:0px; }
		.widget .list-group { margin-bottom: 0; }
		.widget .panel-title { display:inline }
		.widget .label-info { float: right; }
		.widget li.list-group-item {border-radius: 0;border: 0;border-top: 1px solid #ddd;}
		.widget li.list-group-item:hover { background-color: rgba(86,61,124,.1); }
		.widget .mic-info { color: #666666;font-size: 13px; }
		.widget .action { margin-top:5px; }
		.widget .btn-block { border-top-left-radius:0px;border-top-right-radius:0px; }
	</style>
	<div class="row">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
	 				<div id="message-stat"></div>
	 				<div id="reply-div"></div>
	 				<form method="post" onsubmit="return sendMessage()" id="message-form">
	 					{{ csrf_field() }}
	 					<input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
						
	 					<!-- Check compact -->
	 					@if(session('email'))
	 						
		 					<div class="form-group">
		 						<label for="to">To</label>
		 						<input type="text" id="to" placeholder="johndoe@example.com" maxlength="25" required="" value="{{ session('email') }}" class="form-control col-md-6">
		 					</div>

		 					<div class="form-group">
		 						<label for="name">Subject</label>
		 						<input class="form-control" type="text" id="subj" placeholder="Subject.." value="Product Request" required="" />
		 						<span class="error_subj"></span>
		 					</div>

		 					<div class="form-group">
		 						<label for="textarea">Message:</label>
		 						<textarea class="form-control" cols="40" rows="8" id="message-body" placeholder="Type your message here..." required="">
		 							Hi, Please i would love to know more about this produce..
		 						</textarea>
		 						<span class="error_body"></span>
		 					</div>
		 				
		 					<div class="form-group">
		 						<span class="loading-draft"></span><br />
		 						<button class="btn btn-primary" id="y-send-message-btn">
		 							Send 
		 						</button> 
		 						{{-- <a class="btn btn-link" onclick="saveDraft()" href="javascript:void(0);">Save as Draft</a> --}}
		 					</div>
	 					@else
	 						<div class="form-group">
		 						<label for="to">To</label>
		 						<input data-step="1" data-intro="start by typing the email of the user you want to message " type="text" id="to" placeholder="email@example.com" maxlength="25" required="" class="form-control">
		 					</div>

		 					<div class="form-group">
		 						<label for="name">Subject</label>
		 						<input data-step="2" data-intro="Here you can specify message subject, eg: Purchase Order " class="form-control" type="text" id="subj" placeholder="Subject.." required="" />
		 						<span class="error_subj"></span>
		 					</div>

		 					<div class="form-group" data-step="3" data-intro="This section allows you to type messages you want to send to your receipient">
		 						<label for="textarea">Message:</label>
		 						<textarea class="form-control" cols="40" rows="8" id="message-body" placeholder="Type your message here..." required=""></textarea>
		 						<span class="error_body"></span>
		 					</div>
		 				
		 					<div class="form-group" data-step="4" data-intro="Hit the send button to send message or hit the save as draft to save the message for later !">
		 						<span class="loading-draft"></span><br />
		 						<button class="btn btn-primary" id="y-send-message-btn">
		 							Send
		 						</button> 
		 						
		 					</div>
	 					@endif
	 				</form>
	 			</div>
 			</div>
		</div>
	</div>
	<br />
</div>

<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script type="text/javascript">
	// current logged_in user
	var loggedUser = '{{ Auth::user()->email }}';
	// Pusher.logToConsole = true;
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
        encrypted: false,
        cluster: "eu"
    });

    var channel = pusher.subscribe('new-message');
    channel.bind('Yeelda\\Events\\NewMessage', function(data) {
    	var value = data;
      	// console.log(data);
      	if(value.to == loggedUser){
      		var container = $(".panel-body");   
	        var markup = `
	        	<ul class="list-group">
	                <li class="list-group-item">
	                    <div class="row">
	                        <div class="col-md-1">
	                            <img src="http://www.iconninja.com/files/929/593/210/message-email-envelope-mail-letter-icon.svg" width="80%" height="80%" class="img-circle img-responsive" alt="Message" />
	                        </div>
	                        <div class="col-md-11">
	                            <div>
	                                Subject: <a href="#">`+value.subject+`</a>
	                                <div class="mic-info">
	                                    From: <a href="#">`+value.from+`</a>  
	                                    <span class="pull-right"><i class="fa fa-clock-o"></i> `+value.date+`</span>
	                                    <br />
	                                </div>
	                            </div>
	                            <div class="comment-text">
	                                `+value.body+`
	                            </div>
		                        
							    <a class="btn btn-sm" href="/message/service/reply/`+value.id+`/`+value.from+`" class="click_reply">
							    	<i class="fa fa-reply"></i> Reply
							    </a>
							    <a href="#" class="btn btn-sm">
							      	<i class="fa fa-trash"></i> Delete
							  	</a>     
		                    </div>
		                </div>
		            </li> 
		        </ul>
	        `;
	        container.prepend(markup);
      	}
    });

	// at loaded dom
	var email = $("#user_email").val();

	// load message 
	$.get('/load/messages', function (data){
		// console.log(data);
		$(".incoming-messages-div").html("");
		$.each(data, function (index, value){
			if(value.to == loggedUser){
				$(".panel-body").append(`
					<ul class="list-group">
		                <li class="list-group-item">
		                    <div class="row">
		                        <div class="col-md-1">
		                            <img src="http://www.iconninja.com/files/929/593/210/message-email-envelope-mail-letter-icon.svg" width="80%" height="80%" class="img-circle img-responsive" alt="Message" />
		                        </div>
		                        <div class="col-md-11">
		                            <div>
		                                Subject: <a href="#">`+value.subject+`</a>
		                                <div class="mic-info">
		                                    From: <a href="#">`+value.from+`</a>  
		                                    <span class="pull-right"><i class="fa fa-clock-o"></i> `+value.date+`</span>
		                                    <br />
		                                </div>
		                            </div>
		                            <div class="comment-text">
		                                `+value.body+`
		                            </div>
			                        
								    <a class="btn btn-sm" href="/message/service/reply/`+value.id+`/`+value.from+`" class="click_reply">
								    	<i class="fa fa-reply"></i> Reply
								    </a>
								    <a href="#" class="btn btn-sm">
								      	<i class="fa fa-trash"></i> Delete
								  	</a>     
			                    </div>
			                </div>
			            </li> 
			        </ul>
				`);
			}
		});
	});

	// send message from form
	function sendMessage(){
		$('#y-send-message-btn').prop("disabled", true);
		$('#y-send-message-btn').html(`
			Sending...
		`);

		var token = $("input[name=_token]").val();
		var to    = $("#to").val();
		var from  = $("#user_email").val();
		var subj  = $("#subj").val();
		var body  = $("#message-body").val();

		var data = {
			_token:token,
			from:from,
			to:to,
			subj:subj,
			body:body
		};

		// send  via Ajax
		$.ajax({
			type: "post",
			url: "/send/messages",
			data: data,
			cache: false,
			success: function (data){
				swal(
					"success",
					data.message,
					"success"
				);
				$("#message-form")[0].reset();
				$('.loading').hide();

				$('#y-send-message-btn').prop("disabled", false);
				$('#y-send-message-btn').html(`
					Send
				`);

			},
			error: function (data){
				swal(
					"oops",
					data.message,
					"error"
				);

				$('#y-send-message-btn').prop("disabled", false);
				$('#y-send-message-btn').html(`
					Send
				`);
			}
		});

		// void
		return false;
	}

	// save draft messages
	function saveDraft(){
		$('.loading-draft').text('Saving....');
		// form data
		var token = $("input[name=_token]").val();
		var to    = $("#to").val();
		var from  = $("#user_email").val();
		var subj  = $("#subj").val();
		var body  = $("#message-body").val();

		if(subj == ''){
			$('.error_subj').html(`
				<span class="text-danger">
					Subject is empty...
				</span>
			`);
			return false;
		}

		if(body == ''){
			$('.error_body').html(`
				<span class="text-danger">
					Message is empty...
				</span>
			`);
			return false;
		}

		var data = {
			_token:token,
			from:from,
			to:to,
			subj:subj,
			body:body
		};

		$.ajax({
			url: '/save/draft',
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data){
				// console.log(data);
				$('.loading-draft').text('Saved..');
			},
			error: function (data){
				$('.loading-draft').text('Error saving draft message');
				alert('Error, Fail to send save draft request');
			}
		});
	}
</script>
@endsection