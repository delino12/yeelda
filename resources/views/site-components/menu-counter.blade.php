<section>
    <div class="container-fluid">
        <div class="row text-center y-count-row">
            <div class="col-md-3 col-xs-6 y-cl text-center small">
                <span class="y-count-number">
                    {{ $platform_stats['farmers'] }}+
                </span>
                <br />
                <span class="y-count-title">
                    Farmers
                </span>
            </div>
            <div class="col-md-3 col-xs-6 y-cl text-center small">
                <span class="y-count-number">
                    {{ $platform_stats['services'] }}+
                </span>
                <br />
                <span class="y-count-title">
                    Services Providers
                </span>
            </div>
            <div class="col-md-3 col-xs-6 y-cl text-center small">
                <span class="y-count-number">
                    {{ $platform_stats['produces'] }}+
                </span>
                <br />
                <span class="y-count-title">
                    Farm produce categories
                </span>
            </div>
            <div class="col-md-3 col-xs-6 y-cl text-center small">
                <span class="y-count-number">
                    {{ $platform_stats['states'] }}+
                </span>
                <br />
                <span class="y-count-title">
                    States
                </span>
            </div>
        </div>
    </div>
</section>