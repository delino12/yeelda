<section class="" style="font-size: 13px;">
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color:#FFFFFF;">
        <div class="container-fluid" style="padding-bottom: 5px;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a href="/" class="navbar-brand">
                    <img src="/images/img-set/logo-transparent-new.png" height="70" width="auto">
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav navbar-right y-navbar">
                    @if(Auth::check())
                    <li><a href="javascript:void(0);"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a></li>
                        @if(Auth::user()->account_type == "farmer")
                            <li><a href="{{url('farmer/dashboard')}}">Dashboard</a></li>
                        @elseif(Auth::user()->account_type == "service")
                            <li><a href="{{url('service-provider/dashboard')}}">Dashboard</a></li>
                        @elseif(Auth::user()->account_type == "buyer")
                            <li><a href="{{url('investors/dashboard')}}">Dashboard</a></li>
                        @endif
                    @endif

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle dropbtn">Solutions</a>
                        <ul class="dropdown-menu dropdown-content">
                            <li><a href="{{url('farmer')}}">Farmers</a></li>
                            <li><a href="{{url('services')}}">Services Providers</a></li>
                            <li><a href="{{url('buyers')}}">Buyers</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle dropbtn">Platform</a>
                        <ul class="dropdown-menu dropdown-content">
                            <li><a href="{{url('search-farmer')}}">Find a Farmer</a></li>
                            <li><a href="{{url('produce-marketplace')}}">Market Place</a></li>
                            <li><a href="{{url('future-produce-marketplace')}}">Future Market</a></li>
                            <li><a href="{{url('equipment-marketplace')}}">Farm Services</a></li>
                        </ul>
                    </li>
                    <li class="dropdown" class="dropdown-toggle dropbtn">
                        <a href="#">Company</a>
                        <ul class="dropdown-menu dropdown-content">
                            <li><a href="{{url('about')}}">About Us</a></li>
                            <li><a href="{{url('contact')}}">Contact Us</a></li>
                            <li><a href="{{url('terms-and-conditions')}}">Terms</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('blog')}}">Blog</a></li>

                    @if(Auth::check() == false)
                        <li>
                            <a href="{{url('signin')}}">Sign in</a>
                        </li>
                    @else
                        <li>
                            <a href="{{url('account/logout')}}">Sign out</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{url('carts')}}">
                            <span class="carts_total"></span>
                            <i class="fa fa-shopping-cart" style="font-size:24px"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="price-discovery" style="background-color:#FFFFFF;color:#CBB39B;padding:8px;box-shadow: 0px 0px 1px 0px #CCC;">
            <div class="ticker">
              <div class="ticker__list ticker">
              </div>
            </div>
        </div>
    </nav>
</section>