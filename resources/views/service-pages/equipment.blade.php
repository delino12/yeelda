@extends('layouts.main-sp')

@section('title')
	Farm Services | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/service-provider/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Upload Equipment</li>
			<li class="pull-right">
				@if(session('update_status'))
					<p class="text-success">{{ session('update_status') }}</p>
				@endif

				@if(session('update_error'))
					<p class="text-danger">{{ session('update_error') }}</p>
				@endif
			</li>
		</ol>
		@if(session('update_status'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 80px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('update_status') }}</p>
			</div>
		@endif

		@if(session('error_status'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 80px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_status') }}</p>
			</div>
		@endif

		@if(session('success_msg'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 120px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('success_msg') }}</p>
			</div>
		@endif

		@if(session('error_msg'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 120px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_msg') }}</p>
			</div>
		@endif

		{{-- add seedlings sections --}}
		<div class="row">
			<div class="col-md-6">
				<div class="card">
				<div class="card-body">
			  		<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-tree.png"> Add Seeds</h1>
			  		<button id="seedbtn" class="btn btn-warning" onclick="showSeedForm()">Add Seeds</button>

			  		<div class="seed-form" style="display: none;">
				  		<form method="post" action="/add/seeds/{{ Auth::user()->email }}" enctype="multipart/form-data">
			  				<div class="col-sm-6">
			  					{{ csrf_field() }}
					  			<div class="form-group">
					  				<label for="name">Name</label>
					  				<input type="text" class="form-control" name="s_name" placeholder="Seed's type.." required="">
					  			</div>
			  				</div>
			  				<div class="col-sm-6">
					  			<div class="form-group">
					  				<label for="f_qty">Quantity</label>
					  				<input type="number" pattern="[0-9]*" class="form-control" placeholder="20" name="s_qty" required="">
					  			</div>
					  			<div class="form-group">
					  				<label for="price">Price</label>
					  				<input type="text" class="form-control" name="s_price" placeholder="10000" required="">
					  			</div>

					  			<div class="form-group">
					  				<label for="file">Seed image</label>
					  				<input type="file" class="form-control" name="file" required="">
					  			</div>

					  			<div class="form-group">
					  				<button class="btn btn-primary">Upload</button>
					  			</div>
			  				</div>
			  			</form>
			  		</div>
			  	</div>
			  </div>
		  	</div>

			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
				  		<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-mill.png"> Add Fertilizers</h1>
				  		<button id="fertBtn" class="btn btn-warning" onclick="showFertForm()">Add Fertilizers</button>

				  		<div class="fert-form" style="display: none;">
				  			<form method="post" action="/add/fertilizers/{{ Auth::user()->email }}" enctype="multipart/form-data">
				  				<div class="col-sm-6">
				  					{{ csrf_field() }}
						  			<div class="form-group">
						  				<label for="type">Type</label>
						  				<select class="form-control" name="f_type">
						  					<option value="soil">Soil (Organic)</option>
						  					<option value="pest">Pest Control</option>
						  				</select>
						  			</div>
						  			<div class="form-group">
						  				<label for="name">Name</label>
						  				<input type="text" class="form-control" name="f_name" placeholder="Fertilizer's name.." required="">
						  			</div>
				  				</div>
				  				<div class="col-sm-6">
						  			{{-- <div class="form-group">
						  				<label for="f_qty">Quantity</label>
						  				<input type="number" pattern="[0-9]*" class="form-control" placeholder="20" name="f_qty" required="">
						  			</div> --}}
						  			<div class="form-group">
						  				<label for="price">Price</label>
						  				<input type="text" class="form-control" name="f_price" placeholder="10000" required="">
						  			</div>

						  			<div class="form-group">
						  				<label for="file">Fertilizer image (Sample)</label>
						  				<input type="file" class="form-control" name="file" required="">
						  			</div>

						  			<div class="form-group">
						  				<button class="btn btn-primary">Upload</button>
						  			</div>
				  				</div>
				  			</form>
				  		</div>
			  		</div>
			  	</div>
		  	</div>
		</div>

		<br />

		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
				  		<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Upload Equipment</h1>
				 		<p class="small">Note: Please make sure Equipment is available at the specified location filled!</p>
				 		<form method="post" action="/upload/equipment" enctype="multipart/form-data">
				 			{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12 small">
									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Equipment</label>
											</div>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="equipment_name" placeholder="Tractor, Shovel, Carrier, Cuttlass etc.. ">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>How Many</label>
											</div>
											<div class="col-sm-8">
												<input type="number" pattern="[0-9]*" class="form-control col-sm-4" name="equipment_no" placeholder="Eg. 10" required="">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Description</label>
											</div>
											<div class="col-sm-8">
												<textarea class="form-control" rows="2" cols="30" name="equipment_desc" placeholder="Describe equipment"></textarea>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Price</label>
											</div>
											<div class="col-sm-3">
												<input type="text" pattern="[0-9]*" name="equipment_price" class="form-control" placeholder="&#8358; 00.00">
											</div>
											<div class="col-sm-3">
												<span class="small"><b>Note:</b> Per Day</span>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Location</label>
											</div>
											<div class="col-sm-8">
												<textarea name="equipment_location" class="form-control" cols="20" rows="2" placeholder="Eg. 16a, farm road..."></textarea>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>State</label>
											</div>
											<div class="col-sm-8">
												<select class="form-control" name="equipment_state" id="state">
													<option value="" selected="selected">- Select -</option>
													<option value="Abuja FCT">Abuja FCT</option>
													<option value="Abia">Abia</option>
													<option value="Adamawa">Adamawa</option>
													<option value="Akwa Ibom">Akwa Ibom</option>
													<option value="Anambra">Anambra</option>
													<option value="Bauchi">Bauchi</option>
													<option value="Bayelsa">Bayelsa</option>
													<option value="Benue">Benue</option>
													<option value="Borno">Borno</option>
													<option value="Cross River">Cross River</option>
													<option value="Delta">Delta</option>
													<option value="Ebonyi">Ebonyi</option>
													<option value="Edo">Edo</option>
													<option value="Ekiti">Ekiti</option>
													<option value="Enugu">Enugu</option>
													<option value="Gombe">Gombe</option>
													<option value="Imo">Imo</option>
													<option value="Jigawa">Jigawa</option>
													<option value="Kaduna">Kaduna</option>
													<option value="Kano">Kano</option>
													<option value="Katsina">Katsina</option>
													<option value="Kebbi">Kebbi</option>
													<option value="Kogi">Kogi</option>
													<option value="Kwara">Kwara</option>
													<option value="Lagos">Lagos</option>
													<option value="Nassarawa">Nassarawa</option>
													<option value="Niger">Niger</option>
													<option value="Ogun">Ogun</option>
													<option value="Ondo">Ondo</option>
													<option value="Osun">Osun</option>
													<option value="Oyo">Oyo</option>
													<option value="Plateau">Plateau</option>
													<option value="Rivers">Rivers</option>
													<option value="Sokoto">Sokoto</option>
													<option value="Taraba">Taraba</option>
													<option value="Yobe">Yobe</option>
													<option value="Zamfara">Zamfara</option>
													<option value="Outside Nigeria">Outside Nigeria</option>
									            </select>
											</div>
										</div>
									</div>

									<hr />

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Images</label>
											</div>
											<div class="col-sm-8">
												<input type="file" name="files[]" required="" class="form-control">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Delivery</label>
											</div>
											<div class="col-sm-8">
												<select class="form-control" name="equipment_delivery_type">
													<option value="Farm Site">To Farm Site</option>
													<option value="Buyers Address">To Buyers Address</option>
													<option value="Factory Address">To Company Address</option>
													<option value="Pick Up">Pick Up (Default Hire Location)</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-2">
												<label>Note</label>
											</div>
											<div class="col-sm-8">
												<textarea class="form-control" cols="30" rows="5" placeholder="Add a note" name="equipment_note"></textarea>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-sm-2 text-center"></div>
											<div class="col-sm-8">
												<button class="btn btn-success">Upload</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-6 small">
				<div class="card">
					<div class="card-body">
						<h1  class="lead"><img class="y-img" src="/images/icon-set/farm-mill.png"> My Equipments</h1>
						<table class="table">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Equipment</th>
									<th>Status</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="load-equipments"></tbody>
						</table>
					</div>
				</div>
				<br />

				<div class="card">
					<div class="card-body">
						<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-band.png"> Equipments Hired</h1>
						<table class="table">
							<thead>
								<tr>
									<th>Equipment</th>
									<th>Status</th>
									<th>Hirer</th>
									<th>Leased Date</th>
									<th>Return Date</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="load-owner-equipment"></tbody>
						</table>
					</div>
				</div>
				<br />

				<div class="card">
					<div class="card-body">
						<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Crop Seeds </h1>
						<table class="table">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Name</th>
									<th>Amount (&#8358;)</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="load-seedling"></tbody>
						</table>
					</div>
				</div>
				<br />

				<div class="card">
					<div class="card-body">
						<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Fertilizers </h1>
						<table class="table">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Name</th>
									<th>Amount (&#8358;)</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="load-fertilizers"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
@endsection

@section('scripts')
	{{-- scripts section --}}
	<script type="text/javascript">

		// show seed form
		function showSeedForm() {
			// body...
			$('#seedbtn').hide();
			$('.seed-form').toggle().fadeIn();
		}

		var numInput = document.querySelector('input[name=equipment_no]');

		// Listen for input event on numInput.
		numInput.addEventListener('input', function(){
		    // Let's match only digits.
		    var num = this.value.match(/^\d+$/);
		    if (num === null) {
		        // If we have no match, value will be empty.
		        this.value = "";
		    }
		}, false);
		
		// show fert form
		function showFertForm() {
			// body...
			$('#fertBtn').hide();
			$('.fert-form').toggle().fadeIn();
		}
		var email = '{{ Auth::user()->email }}';

		$.get('/load/service/equipments', function (e){
			// console.log(e);
			$(".load-equipments").html('');
			var sn = 0;
			$.each(e, function (index, value){
				sn++;
				// console.log(value);
			    $(".load-equipments").append(`
			    	<tr>
						<td>`+sn+`</td>
						<td> `+value.equipment_name+`</td>
						<td> `+value.equipment_status+`</td>
						<td><a href="/view/equipments/?id=`+value.id+`">view details</a> </td>
					</tr>
			    `);
			});
		});

		// load seeds
		$.get('/load/seeds/'+email, function(data) {
			/*optional stuff to do after success */
			// console.log(data);
			$('.load-seedling').html('');
			var sn = 0;
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 sn++;
				$('.load-seedling').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+val.name+`</td>
						<td>`+val.amount+`</td>
						<td><a href="/view/seeds/${val.id}">view</a></td>
					</tr>
				`);
			});
		});

		// load fertilizers
		$.get('/load/fertilizers/'+email, function(data) {
			/*optional stuff to do after success */
			// console.log(data);
			$('.load-fertilizers').html('');
			var sn = 0;
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 sn++;
				$('.load-fertilizers').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+val.name+`</td>
						<td>`+val.amount+`</td>
						<td><a href="/view/fertilzer/${val.id}">view</a></td>
					</tr>
				`);
			});
		});
	</script>
@endsection