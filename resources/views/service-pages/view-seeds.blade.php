@extends('layouts.main-sp')

@section('title')
	Farm Services | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/service-provider/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Upload Equipment</li>
			<li class="pull-right">
				@if(session('update_status'))
					<p class="text-success">{{ session('update_status') }}</p>
				@endif

				@if(session('update_error'))
					<p class="text-danger">{{ session('update_error') }}</p>
				@endif
			</li>
		</ol>
		@if(session('update_status'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 80px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('update_status') }}</p>
			</div>
		@endif

		@if(session('error_status'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 80px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_status') }}</p>
			</div>
		@endif

		@if(session('success_msg'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 120px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('success_msg') }}</p>
			</div>
		@endif

		@if(session('error_msg'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 120px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_msg') }}</p>
			</div>
		@endif

		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<div class="card-body">
				  		<div class="row">
				  			<div class="col-sm-12">
				  				<img src="/uploads/seed-images/{{ $seeds->avatar }}" width="100%" height="300px">
				  				<hr />
				  				<table class="table small">
				  					<tr>
				  						<td>Name</td>
				  						<td>{{ $seeds->name }}</td>
				  					</tr>
				  					<tr>
				  						<td>Amount</td>
				  						<td>{{ number_format($seeds->amount, 2) }}</td>
				  					</tr>
				  				</table>
				  			</div>
				  		</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 small">
				<div class="card">
					<div class="card-body">
						<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Crop Seeds </h1>
						<table class="table">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Name</th>
									<th>Amount (&#8358;)</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody class="load-seedling"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
@endsection

@section('scripts')
	{{-- scripts section --}}
	<script type="text/javascript">
		// load seeds
		$.get('/load/seeds/'+email, function(data) {
			/*optional stuff to do after success */
			// console.log(data);
			$('.load-seedling').html('');
			var sn = 0;
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 sn++;
				$('.load-seedling').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+val.name+`</td>
						<td>`+val.amount+`</td>
						<td><a href="">view</a></td>
					</tr>
				`);
			});
		});
	</script>
@endsection