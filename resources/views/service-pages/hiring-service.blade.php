@extends('layouts.main-sp')

@section('title')
	Hiring Request | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/service-provider/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Account</li>
			<li class="pull-right">
				@if(session('update_status'))
					<p class="text-success">{{ session('update_status') }}</p>
				@endif

				@if(session('update_error'))
					<p class="text-danger">{{ session('update_error') }}</p>
				@endif
			</li>
		</ol>

		<div class="row">
			<div class="col-md-8">
				<div class="load-single-equipments"></div>
			</div>
			<div class="col-md-4">
				<table class="table">
					<thead>
						
					</thead>
					<tbody class="load-hiring-request"></tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{{-- load hiring request --}}
	<script type="text/javascript">

	</script>
@endsection