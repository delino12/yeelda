@extends('layouts.main-sp')

@section('title')
	Dashboard | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/service-provider/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>

		@if(!empty($acct_status))
			<div class="alert alert-danger">
				<p class="text-danger"> {{ $acct_status }} <span class="text-info"> Did not receive any link?</span> 
					<a href="/resend/activation/link/?email={{ Auth::user()->email }}&name={{ Auth::user()->name }}">Resend</a> 
				</p>
			</div>
		@endif
		@if(session('success_msg'))
			<div class="alert alert-success">
				<p class="text-success"> {{ session('success_msg') }}</p>
			</div>
		@endif

		<div class="row">
			<div class="col-md-6" data-step="11" data-intro="This section allows you to add services">
				<div class="card">
					<div class="card-body">
						<a href="/service-provider/equipments">
							<p class="text-info">
								<img class="y-img" src="/images/icon-set/farm-service.png"> Add other services like Fertilizers, Crop Seeds and Equipments 
							</p>
						</a>
					</div>
				</div>
				<br />
				<div data-step="14" data-intro="This section allows you to view equipments services">
					<div class="card">
						<div class="card-body">
							<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Equipments</h1>
							<table class="table small">
								<thead>
									<tr>
										<th>S/N</th>
										<th>Equipment</th>
										<th>Status</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody class="load-equipments"></tbody>
							</table>
							<br /><br />

							<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Crop Seeds </h1>
							<table class="table small">
								<thead>
									<tr>
										<th>S/N</th>
										<th>Name</th>
										<th>Amount (&#8358;)</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody class="load-seedling"></tbody>
							</table>
							<br /><br />


							<h1 class="lead"><img class="y-img" src="/images/icon-set/farm-service.png"> Fertilizers </h1>
							<table class="table small">
								<thead>
									<tr>
										<th>S/N</th>
										<th>Name</th>
										<th>Amount (&#8358;)</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody class="load-fertilizers"></tbody>
							</table>
							<br /><br />
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="small" data-step="12" data-intro="This section shows notifications !">
					<div class="card">
						<div class="card-body">
							<h3 class="lead">Notifications</h3>
							<ul class="list-group">
								<div class="notifications"></div>
							</ul>
						</div>
					</div>
				</div>
				<br />
				{{-- news feeds sections --}}
				<div data-step="13" data-intro="This section allows you to get live news updates">
					<div class="card">
						<div class="card-body">
							<h3 class="lead">News Feeds</h3>
							<ul class="list-group">
								{{-- <div class="news small"></div> --}}
								{{-- <div id="rss-feeds" class="small"></div> --}}
								<div class="load-headlines small"></div>
				        		<div class="load-headlines-two small" style="display: none;"></div>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{{-- Rss feeds --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script type="text/javascript">
		// current logged in user
		var email = '{{ Auth::user()->email }}';

		// load equipments
		$.get('/load/service/equipments', function (e){
			// console.log(e);
			$(".load-equipments").html('');
			var sn = 0;
			$.each(e, function (index, value){
				sn++;
				console.log(value);
			    $(".load-equipments").append(`
			    	<tr>
						<td>`+sn+`</td>
						<td> `+value.equipment_name+`</td>
						<td> `+value.equipment_status+`</td>
						<td><a href="/view/equipments/?id=`+value.id+`">view details</a> </td>
					</tr>
			    `);
			});
		});

		// load notifications
		$.get('/load/notifications/'+email, function (e){
			$('.notifications').html('');
			var sn = 0;
			$.each(e, function (index, value){
				// load div
				sn++;

				$('.notifications').append(`
					<li class="list-group-item">You have message from <b>`+value.contents.from+`</b> <br /> 
					<p>`+value.contents.body+`</p>
					<a href="/message/service">view now</a> <br />
					<span class="small pull-right">`+value.date+`</span><br /></li>
				`);


				if(sn > 4){
					return false;
				}

			});
			// console.log(e.contents);
		});

		// load seeds
		$.get('/load/seeds/'+email, function(data) {
			/*optional stuff to do after success */
			console.log(data);
			$('.load-seedling').html('');
			var sn = 0;
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 sn++;
				$('.load-seedling').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+val.name+`</td>
						<td>`+val.amount+`</td>
						<td><a href="">view</a></td>
					</tr>
				`);
			});
		});

		// load fertilizers
		$.get('/load/fertilizers/'+email, function(data) {
			/*optional stuff to do after success */
			console.log(data);
			$('.load-fertilizers').html('');
			var sn = 0;
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 sn++;
				$('.load-fertilizers').append(`
					<tr>
						<td>`+sn+`</td>
						<td>`+val.name+`</td>
						<td>`+val.amount+`</td>
						<td><a href="">view</a></td>
					</tr>
				`);
			});
		});

	    $.get('/load/api-news', function(data) {
	      // console.log(data);
	      $(".load-headlines").html("");
	      let sn = 0;
	      $.each(data.articles, function(index, val) {
	        // console.log(val);
	        sn++;
	        $(".load-headlines").append(`
	          <table cellpadding="2">
	            <tr>
	              <td><img src="`+val.urlToImage+`" width="70" height="50"><td>
	              <td><h4 style="font-size:13px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
	            </tr>
	          </table> 
	          <p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
	        `);
	        if(sn == 4){
	          return false;
	        }
	      });
	    });

	    // dino get data
	    $.get('/load/api-news/bloomberg', function(data) {
	      // console.log(data);
	      let sn = 0;
	      $.each(data.articles, function(index, val) {
	        // console.log(val);
	        sn++;
	        $(".load-headlines-two").append(`
	          <table cellpadding="2">
	            <tr>
	              <td><img src="`+val.urlToImage+`" width="70" height="50"><td>
	              <td><h4 style="font-size:13px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
	            </tr>
	          </table> 
	          <p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
	        `);
	        if(sn == 4){
	          return false;
	        }
	      });
	    });

		// change news card
		window.setInterval(function (){
	 		$(".load-headlines").toggle({ direction: "left" }, 1000);
	  		$(".load-headlines-two").toggle({ direction: "right" }, 1000);
		}, 1000 * 15);
	</script>
@endsection