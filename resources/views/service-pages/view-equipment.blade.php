@extends('layouts.main-sp')

@section('title')
	View Equipment | Yeelda
@endsection

@section('contents')
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
		  		<a href="/service-provider/dashboard">{{ Auth::user()->name }}</a>
			</li>
			<li class="breadcrumb-item">View</li>
			<li class="breadcrumb-item active">{{ ucfirst($equipment->equipment_name) }}</li>
			<li class="pull-right">
				@if(session('update_status'))
					<p class="text-success">{{ session('update_status') }}</p>
				@endif

				@if(session('update_error'))
					<p class="text-danger">{{ session('update_error') }}</p>
				@endif
			</li>
		</ol>
		@if(session('update_status'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 80px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('update_status') }}</p>
			</div>
		@endif

		@if(session('error_status'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 80px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_status') }}</p>
			</div>
		@endif

		@if(session('success_msg'))
			<div class="alert alert-success" role="alert" style="position: absolute; z-index: 10; top: 120px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-success">{{ session('success_msg') }}</p>
			</div>
		@endif

		@if(session('error_msg'))
			<div class="alert alert-danger" role="alert" style="position: absolute; z-index: 10; top: 120px; right: 20px; box-shadow: 1px 1px 2px 1px;">
				<p class="text-danger">{{ session('error_msg') }}</p>
			</div>
		@endif

		<div class="row">
			<div class="col-md-5">
				<div class="load-single-equipments"></div>
			</div>

			<div class="col-md-7">
				<table class="table small">
					<thead>
						<tr bgcolor="#FEAB5C;">
							<th>S/N</th>
							<th>Equipment</th>
							<th>Status</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody class="load-equipments"></tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		var equip_id = '{{ $equip_id }}';
		$.get('/load/single/equipments/?id='+equip_id, function (e){
			// console.log(e);
			$(".load-single-equipments").html('');
			var sn = 0;
			$.each(e, function (index, value){
				sn++;
			    $(".load-single-equipments").html(`
			    	<div class="card small" style="padding:2rem;">
					  <img src="/uploads/equipments/`+value.equipment_image+`" alt="Card image cap" width="100%" height="300px">
					  <hr />
					  <div class="card-block">
					    <h4 class="lead"> (`+value.equipment_no+`) `+value.equipment_name+`</h4>
					    <p class="card-text">`+value.equipment_note+`</p>
					  </div><br /><br />
					  <ul class="list-group list-group-flush">
					    <li class="list-group-item">`+value.equipment_no+` `+value.equipment_name+` at &#8358;`+value.equipment_price+` 
					    <span class="pull-right"><b>Total:</b> &#8358;`+value.total+`</span> </li>
					    <li class="list-group-item"><b>Availability</b> `+value.equipment_status+`</li>
					    <li class="list-group-item"><b>Description</b> `+value.equipment_descriptions+`</li>
					    <li class="list-group-item"><b>Location</b> `+value.equipment_address+`</li>
					    <li class="list-group-item"><b>State</b> `+value.equipment_state+`</li>
					    <li class="list-group-item"><b>Delivery Type</b> `+value.equipment_delivery_type+`</li>
					  </ul>
					</div>
					<br />
			    `);
			});
		});

		$.get('/load/service/equipments', function (e){
			console.log(e);
			$(".load-equipments").html('');
			var sn = 0;
			$.each(e, function (index, value){
				sn++;
				console.log(value);
			    $(".load-equipments").append(`
			    	<tr>
						<td>`+sn+`</td>
						<td> `+value.equipment_name+`</td>
						<td> `+value.equipment_status+`</td>
						<td><a href="/view/equipments/?id=`+value.id+`">view details</a> </td>
					</tr>
			    `);
			});
		});
	</script>
@endsection