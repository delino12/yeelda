@extends('layouts.web-skin')

@section('title')
	YEELDA | Services Providers
@endsection

@section('contents')
	<style type="text/css">
		.row {
			margin-left: 0px;
			margin-right: 0px;
		}
		.col-md-2 {
		    width: 19.66667%;
		}

		@media screen and (max-width: 768px) {
		  .step-count {
		    display: none;
		  }
		  .col-md-2 {
		    width: 97.66667%;
		  }
		}

		@media screen and (max-width: 992px) {
		  .step-count {
		    display: none;
		  }
		  .col-md-2 {
		    width: 97.66667%;
		  }

		  #platform-slide {
		  	display: none;
		  }
		}
	</style>

	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}
	<div class="container">
		<div class="row" style="margin-top: 130px;">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;">
					Service Providers
				</span>
				<span><br />Buy and Sell your farm inputs. e.g Seeds, Fertilizers, etc</span>
			</div>
		</div>

		<br /><br />
		<div class="row">
			<div class="col-md-2">
				<h5 style="font-size: 24px;font-weight: bold;">HOW IT WORKS</h5>
			</div>
		</div>
		<br />
		<div class="row text-center step-count">
			<div class="col-md-2">Step 1</div>
			<div class="col-md-2">Step 2</div>
			<div class="col-md-2">Step 3</div>
			<div class="col-md-2">Step 4</div>
			<div class="col-md-2">Step 5</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">person_add</i>
					<h5 style="font-size: 14px;font-weight: bold;">Sign Up</h5>
					<p class="small">Create an account using your email and password in less than 60secs.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">landscape</i>
					<h5 style="font-size: 14px;font-weight: bold;">Upload Inputs/Equipment</h5>
					<p class="small">
						Take a clear picture of your farm inputs/equipment and Simply click on the dashboard with the help of user guide, navigate to the section of Uploading your produce add your image and within seconds its uploaded.
					</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">forum</i>
					<h5 style="font-size: 14px;font-weight: bold;">Order Notification</h5>
					<p class="small">Sms/mail notification is received by the service provider for orders from farmers on the platform.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">verified_user</i>
					<h5 style="font-size: 14px;font-weight: bold;">Quality Control Check</h5>
					<p class="small">All farm inputs/equipment is shipped to an assigned aggregation point for proper quality check before disseminated .</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">payment</i>
					<h5 style="font-size: 14px;font-weight: bold;">Payment</h5>
					<p class="small">Service providers receive payment immediately in an escrow account which can be withdrawn into personal bank accounts after deductions of required charges.</p>
				</div>
			</div>
		</div>
	</div>

	{{-- platform features --}}
    <div id="platform-slide">
	    @include('web-components.platform-features')
	</div>

	{{-- blogs --}}
    {{-- @include('web-components.blogs') --}}

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	
@endsection
