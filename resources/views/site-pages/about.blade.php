@extends('layouts.web-skin')

@section('title')
	YEELDA | About us
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.aboutus-banner-card')

	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;color: #464444;">
					About us
				</span>
				<p class="y-aboutus-p text-justify">
					In line with our core objective which is the creation of the most innovative service exchange for agricultural input/output in Nigeria.
				</p>
				<p class="y-aboutus-p text-justify">
					We offer a bouquet of complementary products and service offerings directed at local industries (SMEs and large corporations), Bulk purchasing End-Users, Exporters of Agricultural Outputs, Offshore Commodity traders and multinationals who seek high quality Agricultural output from the West Africa Coast.
				</p>
			</div>
		</div>
	</div>

	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content">
			<div class="col-md-6 text-justify">
				<div class="y-aboutus-mission-card">
					<img src="{{asset('images/icon-set/tractor.png')}}" class="y-aboutus-div-icon">
					<span class="y-simple-title">
						Our mission
					</span>
					<hr />

					<p class="small">
						Our mission is to provide a platform to facilitate interaction and commerce in the Agriculture sector amongst farmers, off-takers and input providers, providing access to larger ready markets to all stakeholders both domestic and foreign.
					</p>

					<p class="small">
						This will be achieved with our robust digital trading platform which will harness an eco-system of ancillary services derived and designed to help farmers scale a good degree of their challenges ranging from access to credit, farm inputs, best agronomic practices, quality control, equipment and logistics.
					</p>
				</div>
			</div>
			<div class="col-md-6 text-justify">
				<div class="y-aboutus-vision-card">
					<img src="{{asset('images/icon-set/seeds.png')}}" class="y-aboutus-div-icon">
					<span class="y-simple-title">
						Our vision
					</span>
					<hr />

					<p class="small">
						Our vision is to be Africa's largest agriculture based trading platform connecting the agricultural value chain through technology.
					</p>
					<img src="{{asset('images/icon-set/produce.png')}}" width="45" height="auto">
					<br /><br />
					<p class="small">
						The driving force behind this vision is to increase smallholder farmer productivity where there ideal of ‘For Consumption Farming’ is better encouraged to include Farming for Trade and Income Generation.
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="y-aboutus-overlap"></div>
				<div class="y-aboutus-explain">
					<p class="small">
						YEELDA is a phonetic play on the English word YIELDER from the verb YIELD which means;<br />
						- To furnish as return for effort or investment;<br />
						- Be productive of;
					</p>
				</div>
			</div>
		</div>
	</div>

	{{-- parallax section --}}
	<div class="container-fluid y-shade-bg">
		<div class="row">
			<br />
			<div class="y-aboutus-corevalue" role="banner" style="background-image: url({{asset('images/img-set/12.jpg')}});background-size: cover;"></div>
			<img src="{{asset('images/img-set/08.png')}}" width="20%" height="auto" class="y-aboutus-corevalue-img">
		</div>

		<div class="row y-pad-content">
			<div class="col-md-12">
				<p style="color: #999;">
					In line with our core objective which is the creation of the most innovative service exchange for Agricultural input/output in Nigeria, we offer a bouquet of complementary products and service offerings directed at Local Industries (SMEs and Large Corporations), Bulk Purchasing End-Users, Exporters of Agricultural Outputs, Offshore Commodity Traders and Multinationals who seek high quality Agricultural output from the West African Coast.
				</p>
			</div>
		</div>

		<div class="row y-pad-content">
			<div class="col-md-6">
				<img src="{{asset('images/img-set/14.jpg')}}" class="img-responsive">
			</div>
			<div class="col-md-6">
				<img src="{{asset('images/img-set/08.jpg')}}" class="img-responsive">
			</div>
		</div>

		<div class="row y-pad-content">
			<div class="col-md-6">
				<div class="y-aboutus-simple-card small">
					<b>Out-growers Scheme Management (OSM):</b> The organization of smallholder farmers producing to pre-determined quality standards through the management of inputs, production and the eventual delivery to the off-taker
				</div>
			</div>
			<div class="col-md-6">
				<div class="y-aboutus-simple-card small">
					<b>Virtual Aggregation Service (VAS):</b> connect with YEELDA to help manage the process of virtually and physically aggregating Agricultural products for off-takers and end-users at pre-agreed frequencies over a termed duration
				</div>
			</div>
		</div>

		<div class="row y-pad-content">
			<div class="col-md-6">
				<div class="y-aboutus-simple-card small">
					<b>Sales & Marketing Facilities:</b> Provision of marketing and sales for Agric based SME products through our digital shop-in-shop medium, allowing them to focus on their core competencies
				</div>
			</div>
			<div class="col-md-6">
				<div class="y-aboutus-simple-card small">
					<b>Suite for Purpose Service (SPS):</b> Unique Taylor-Made Off-take arrangement to accommodate special need off-takers
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="y-aboutus-explain">
					<p class="small">
						<b>NOTES</b><br />
						It’s unique selling proposition is that it can generate financial value on a promissory note while guaranteeing the investment of the paying party. The data generated by YEELDA also allows its customers to make sound resource plans.
					</p>
				</div>
			</div>
		</div>

		<div class="row y-shade-bg">
			<div class="col-md-12 text-center">
				<div class="y-aboutus-objectives">
					<p class="small">
					The company’s business objectives will be supported by backward engagement and coordination of relevant stakeholders. This will ensure the integrity of the platform and provide assurance for the end-users in terms of quality and traceability.
					</p>

					<p class="small">
						YEELDA will ultimately integrate all the key players within the agricultural sector and become a one-stop ecosystem that will boost the sector’s contributions to the nation’s Gross domestic product (GDP).
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content">
			<div class="col-md-6">
				<div class="y-aboutus-strategic" style="font-size: 12px;color:#999;">

					<span class="y-aboutus-strategic-title">
						Our strategic objectives
					</span>
					<br /><br />


					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Create a market place for farmers, inputs providers and end-users.
						</span>
					</div>
					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Ensure the optimum quality and quantity of agricultural produce with effective traceability
						</span>
					</div>
					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Create a seamless delivery of produce to off-takers and end-users
						</span>
					</div>
					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Promote awareness amongst farmers on agricultural global best practices
						</span>
					</div>
					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Build capacity and provide access to resources for farmers
						</span>
					</div>
					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Facilitate independent peer to peer transactions
						</span>
					</div>
					<div class="y-aboutus-list-item">
						<i class="fa fa-tag"></i> 
						<span class="y-aboutus-item">
							Promote commodity export in Nigeria
						</span>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="y-aboutus-strategic-core">
					<p>OUR CORE VALUE</p>

					<p>
						YEELDA’S values represent the underlying principles, believes and philosophies at the heart of the value proposition.
					</p>

					<p>
						These values are aspirational and serve as a guide in YEELDA’S drive to achieve its business goals
					</p>
				</div>

				<div class="y-aboutus-strategic-win">
					<p>WIN-WIN FOR ALL</p>

					<p>
						Unique Taylor-Made Off-take arrangement to accommodate special need off-takers
					</p>
				</div>

				<div class="y-aboutus-strategic-enhance">
					<p>ENHANCE SERVICE DELIVERY IN THE VALUE CHAIN</p>

					<p>
						Potentially reduce waste by introducing efficiency arising from – aggregation, processing and potential storage.
					</p>

					<p>
						Identify opportunities and facilitate the set-up of SVPs exploit such opportunities. YEELDA will see the demand and supply trends and sell this to potential investors
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid y-shade-bg">
		<div class="row">
			<br />
			<div class="y-aboutus-corevalue" role="banner" style="background-image: url({{asset('images/img-set/13.jpg')}});background-size: cover;"></div>
			<img src="{{asset('images/img-set/09.png')}}" width="20%" height="auto" class="y-aboutus-corevalue-img">
		</div>

		<div class="row y-pad-content">
			<div class="col-md-6">
				<i class="fa fa-user" style="color: #999;padding-bottom: 10px;"></i> 
				<span style="font-size: 21px;padding-bottom: 10px;margin-left: 10px;">Gbolahan OPEODU</span>
				<div class="y-aboutus-simple-card">
					<p class="small text-justify">
						He is a professional with years of experience garnered at reputable institutions in Nigeria and the United Kingdom. Self-driven and commercially astute, with the requisite business acumen acquired through experience in different roles over the period of more than a decade in solution based equipment Leasing and logistics.
					</p>

					<p class="small text-justify">
						A strong knowledge base has been developed that cuts across multiple sectors and industries such as manufacturing, maritime, logistics and agriculture. This is coupled with an understanding of varied functions critical to the sustenance and growth of such capital sourcing and structuring as well as the development and execution of the requisite strategy and tactics in delivering corporate goals.
					</p>

					<p class="small text-justify">
						Gbolahan is a graduate in the field of Business Information Technology at the leads metropolitan University, and holds a diploma in Computing and Information System form the University of Central England Birmingham.
					</p>

					<p class="small text-justify">
						Also, he is a holder of a diploma in Employability and Management Skills and is a qualified member of the Nigeria Institute of Management. He also holds a post-graduate diploma in Professional Marketing from the Chartered Instituted of Marketing, United Kingdom.
					</p>
				</div>

				<br /><br />
				<i class="fa fa-user" style="color: #999;padding-bottom: 10px;"></i> 
				<span style="font-size: 21px;padding-bottom: 10px;margin-left: 10px;">Joseph OJO</span>
				<div class="y-aboutus-simple-card">
					<p class="small text-justify">
						Joseph Ojo is a finance specialist with expertise in structured finance and debt solutions. He has worked in a number of financial institutions in Nigeria and has been involved in several landmark transactions on a limited recourse or recourse basis. Transaction value closed overtime is in excess of US$2billion.
					</p>

					<p class="small text-justify">
						He is a Chartered Accountant and also holds Masters in Finance and International Accounting.
					</p>
				</div>
			</div>

			<div class="col-md-6">
				<i class="fa fa-user" style="color: #999;padding-bottom: 10px;"></i> 
				<span style="font-size: 21px;padding-bottom: 10px;margin-left: 10px;">Tola AKINHANMI</span>
				<div class="y-aboutus-simple-card">
					<p class="small text-justify">
						Tola is an investment banker and has had various roles within corporate and investment banking with specializations in Real Estate Finance and Project and Structured Finance. He has over ten years of in-country and regional work experience (Including Nigeria, Ghana and Cote D’Ivoire) which spans across various asset classes (with an estimated portfolio size over US$1.2 billion) within the real estate sector in the leading financial institution in Africa (in terms of market capitalization).
					</p>

					<p class="small text-justify">
						In addition, his project finance deal mandates include debt capital raisin for major infrastructure projects in the oil & gas industrial sectors. His early work experience include treasury operations and commercial banking roles in Fidelity Bank and Intercontinental Bank (now Access Bank) respectively.
					</p>

					<p class="small text-justify">
						Tola has a first degree in Economics from the University of Lagos, Nigeria and master degree in International Money, Finance & Investments from Durham University, UK. He obtained a RICS Postgraduate Diploma in Project Management and MBA in Real Estate & Construction Management from the University College of Estate Management, Reading UK.
					</p>

					<p class="small text-justify">
						Beyond his banking career, he jointly led the start-up of Primexetram a transport logistics and distribution firm in 2009 and was responsible for establishing it logistics and operational framework in delivering value to both customers and its stakeholders. Given his background in advisory and structuring funding for construction and development projects with complex logistic challenges and passion for driving and implementing strategic and innovative solutions in new products and markets, he brings a wealth of experience to yeelda.
					</p>
				</div>
			</div>
		</div>
		<br /><br />
	</div>

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
	
@endsection
