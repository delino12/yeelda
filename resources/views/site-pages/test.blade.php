@extends('layouts.web-skin')

@section('title')
	YEELDA | Farmers
@endsection

@section('contents')
	<style type="text/css">
		.row {
			margin-left: 0px;
			margin-right: 0px;
		}
		.col-md-2 {
		    width: 19.66667%;
		}
	</style>
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}

	<div class="container">
		<div class="row" style="margin-top: 130px;">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;">
					Farmers
				</span>
				<span><br />Sell and manage your farm produce.

					<span class="tooltip" title="This is my span's tooltip message!">Some text</span>
				</span>
				
			</div>
		</div>
		<br /><br />
		<div class="row">
			<div class="col-md-2">
				<h5 style="font-size: 24px;font-weight: bold;">HOW IT WORKS</h5>
			</div>
		</div>
		<br />
		<div class="row text-center">
			<div class="col-md-2">Step 1</div>
			<div class="col-md-2">Step 2</div>
			<div class="col-md-2">Step 3</div>
			<div class="col-md-2">Step 4</div>
			<div class="col-md-2">Step 5</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">person_add</i>
					<h5 style="font-size: 14px;font-weight: bold;">Sign Up</h5>
					<p class="small">Create an account using your email and password in less than 60secs.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">landscape</i>
					<h5 style="font-size: 14px;font-weight: bold;">Upload Produce</h5>
					<p class="small">
						Take a clear picture of your farm produce and Simply click on the dashboard with the help of user guide, navigate to the section of Uploading your produce add your image and within seconds its uploaded.
					</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">forum</i>
					<h5 style="font-size: 14px;font-weight: bold;">Order Notification</h5>
					<p class="small">And sms/mail notification is received by farmers for orders from off-takers</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">verified_user</i>
					<h5 style="font-size: 14px;font-weight: bold;">Quality Control Check</h5>
					<p class="small">All produce is shipped to an assigned aggregation point for proper quality check and grading.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#FFCC00;">payment</i>
					<h5 style="font-size: 14px;font-weight: bold;">Payment</h5>
					<p class="small">Farmers receive payment immediately in an escrow account which can be withdrawn into personal bank accounts after required charges deducted.</p>
				</div>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">person_add</i>
					<h5 style="font-size: 14px;font-weight: bold;">Sign Up</h5>
					<p class="small">Create an account using your email and password in less than 60secs.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">landscape</i>
					<h5 style="font-size: 14px;font-weight: bold;">Upload Produce</h5>
					<p class="small">
						Take a clear picture of your farm produce and Simply click on the dashboard with the help of user guide, navigate to the section of Uploading your produce add your image and within seconds its uploaded.
					</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">forum</i>
					<h5 style="font-size: 14px;font-weight: bold;">Order Notification</h5>
					<p class="small">And sms/mail notification is received by farmers for orders from off-takers</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">verified_user</i>
					<h5 style="font-size: 14px;font-weight: bold;">Quality Control Check</h5>
					<p class="small">All produce is shipped to an assigned aggregation point for proper quality check and grading.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">payment</i>
					<h5 style="font-size: 14px;font-weight: bold;">Payment</h5>
					<p class="small">Farmers receive payment immediately in an escrow account which can be withdrawn into personal bank accounts after required charges deducted.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#99C468;">person_add</i>
					<h5 style="font-size: 14px;font-weight: bold;">Sign Up</h5>
					<p class="small">Create an account using your email and password in less than 60secs.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#99C468;">landscape</i>
					<h5 style="font-size: 14px;font-weight: bold;">Upload Produce</h5>
					<p class="small">
						Take a clear picture of your farm produce and Simply click on the dashboard with the help of user guide, navigate to the section of Uploading your produce add your image and within seconds its uploaded.
					</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#99C468;">forum</i>
					<h5 style="font-size: 14px;font-weight: bold;">Order Notification</h5>
					<p class="small">An sms/mail notification is received by farmers for orders from off-takers</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#99C468;">verified_user</i>
					<h5 style="font-size: 14px;font-weight: bold;">Quality Control Check</h5>
					<p class="small">All produce is shipped to an assigned aggregation point for proper quality check and grading.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#99C468;">payment</i>
					<h5 style="font-size: 14px;font-weight: bold;">Payment</h5>
					<p class="small">Farmers receive payment immediately in an escrow account which can be withdrawn into personal bank accounts after required charges deducted.</p>
				</div>
			</div>
		</div>
	</div>

	{{-- platform features --}}
    @include('web-components.platform-features')

	{{-- blogs --}}
    {{-- @include('web-components.blogs') --}}

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	<script type="text/javascript">
		
	</script>
@endsection
