@extends('layouts.web-skin')

@section('title')
  YEELDA | Settings
@endsection

@section('contents')
  <link rel="stylesheet" href="{{asset('dropdown/css/dropdown.css')}}">
  <style type="text/css">
    .dropdown-toggle {
      box-shadow: none;
    }

    .navbar-nav > li .dropdown-menu {
      margin-top: 41px;
    }
  </style>
  
  {{-- include plain header --}}
  @include('web-components.plain-header')

  <input type="hidden" id="page_background_image">
  <input type="hidden" id="page_background_color">
  <input type="hidden" id="page_color">
  <input type="hidden" id="page_search">
  <input type="hidden" id="page_id" value="{{ $page_data->id }}" name="">

  <section id="y-market-section">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12" style="margin: 0px;padding: 0px;">
          
            <div class="feature-bg-image" id="feature-bg-image" style="background-image: url('{{ $page_data->config['page_background_image'] }}');background-size: cover;height: 460px;">
                <a href="javascript:void(0);" onclick="changeImageBackgroud()"><i class="material-icons text-white left-edit-icon">edit</i></a>
                <div class="profile-setting-thumbnail" style="background-image: url({{ $user->basic->avatar }});">

            </div>
          </div>
        </div>
      </div>

      <div class="row"> 
        <div class="col-md-12">
          <div class="settings-wrapper">
            <table class="table">
              <tr>
                <td>Search</td>
                <td>
                    <label class="switch">
                      <input type="checkbox" checked>
                      <span class="slider round"></span>
                    </label>
                </td>
              </tr>
              <tr>
                <td>Name</td>
                <td>
                    

                    @if($page_data->config['farmer_biz_name'] !== null)
                        <input type="text" class="form-control" id="farmer_biz_name" placeholder="Name here..." value="{{ $page_data->config['farmer_biz_name'] }}">
                    @else
                        <input type="text" class="form-control" id="farmer_biz_name" placeholder="Name here..." name="">
                    @endif
                </td>
              </tr>
              <tr>
                <td>Description</td>
                <td>
                    @if($page_data->config['farmer_biz_description'] !== null)
                        <textarea cols="5" rows="2" class="form-control" id="farmer_biz_description" placeholder="A little description">{{ $page_data->config['farmer_biz_description'] }}</textarea>
                    @else
                        <textarea cols="5" rows="2" class="form-control" id="farmer_biz_description" placeholder="A little description"></textarea>
                    @endif
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
              </tr>
            </table>

            <div align="right">
                <button class="btn btn-primary" id="save-page-btn">Apply setting</button>
            </div>
          </div>

        </div>
      </div>
    </div>
</section>


    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
    <script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('.select-dropdown').dropdown({
        multi: false,
        toggleText: 'Select items'
      });
    });

    function changeImageBackgroud() {
        swal(
            "Ok",
            "Background image updated!",
            "success"
        );
    }

    $("#save-page-btn").click((e) => {
        e.preventDefault();

        var endpoint = "/save/page/settings";
        var query = {
            _token: $("#token").val(),
            page_id: $("#page_id").val(),
            page_background_image: $("#page_background_image").val(),
            page_background_color: $("#page_background_color").val(),
            page_color: $("#page_color").val(),
            page_search: $("#page_search").val(),
            farmer_biz_name: $("#farmer_biz_name").val(),
            farmer_biz_description: $("#farmer_biz_description").val()
        }

        fetch(endpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        }).then(r => {
            return r.json();
        }).then(val => {
            // console.log(val);
            swal(
                "Ok",
                data.message,
                data.status
            );
        }).catch(err => {
            console.log(JSON.stringify(err));
        });
    });

    var myWidget = cloudinary.createUploadWidget({
        cloudName: 'delino12', 
        uploadPreset: 'znwx0uee'
    }, (error, result) => { 
        if (!error && result && result.event === "success") { 
          console.log('Done! Here is the image info: ', result.info); 
          $("#page_background_image").val(result.info.url);
          $("#feature-bg-image").css("background-image", "url('"+result.info.url+"')");
          $(".left-edit-icon").removeClass('left-edit-icon');
        }
      }
    )

    function changeImageBackgroud(){
        myWidget.open();
    }

    $.get('/load/farmers/products', function (data){
        $(".load-products").html("");
        $.each(data, function (index, value){
          // cart option
          if(value.product_status == 'Harvested'){
            var cartOption = `
              <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)">
                <i class="fa fa-shopping-cart"></i> Add to Cart
              </a>
            `;
          }else{
            var cartOption = `
               <a href="/product/details/?id=`+value.id+`">
                <i class="fa fa-book"></i> Details
              </a>
            `;
          }

          var urlToImage;
          if (value.product_image.indexOf('YEELDA-') > -1){
            urlToImage = `/uploads/products/${value.product_image}`;
          }else{
            urlToImage = value.product_image;
          }

          $(".load-products").append(`
            <div class="col-md-3">
              <div class="card h-100">
                <a href="/product/details/?id=`+value.id+`">
                  <img class="img-rounded" src="${urlToImage}" height="180" width="100%" alt="">
                </a>
                <div class="card-body">
                  <br />
                  <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
                  <p class="small">
                    <table class="table small">
                      <tr>
                        <td>Product</td>
                        <td>`+value.product_name+`</td>
                      </tr>
                      <tr>
                        <td>State</td>
                        <td>`+value.product_state+`</td>
                      </tr>
                      <tr>
                        <td>Availability</td>
                        <td>`+value.product_status+`</td>
                      </tr>
                    </table>
                    <span class="small">
                      <a href="/ask-seller/`+value.owner_email+`">
                        <i class="fa fa-envelope"></i> Send Message
                      </a> 
                      - 
                     `+cartOption+`
                    </span> 
                  </p>
                </div>
                <div class="card-footer">
                  
                </div>
              </div>
              <br />
            </div>
          `);
        });
      });


      // custom search
      function customSearch(argument) {
        // body...
        var category  = $('#listed-product').val();
        var location  = $('#listed-location').val();
        var priceFrom = $('#price-from').val();
        var priceTo   = $('#price-to').val();

        // data to json
        var data = {
          category:category,
          location:location,
          priceFrom:priceFrom,
          priceTo:priceTo      
        };

        $.ajax({
          url: '/load/search/results',
          type: 'GET',
          dataType: 'json',
          data: data,
          success: function (data){
            // console.log(data);
            // $('.load-products').html();
            $(".load-products").html("");
            $.each(data, function (index, value){
              $(".load-products").append(`
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="/product/details/?id=`+value.id+`">
                      <img class="img-rounded" src="/uploads/products/`+value.product_image+`" height="150" width="230" alt="">
                    </a>
                    <div class="card-body">
                      <br />
                      <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
                      <p class="small">
                        <table class="table small">
                          <tr>
                            <td>Product</td>
                            <td>`+value.product_name+`</td>
                          </tr>
                          <tr>
                            <td>Location</td>
                            <td>`+value.product_location+`</td>
                          </tr>

                          <tr>
                            <td>Weight</td>
                            <td>`+value.product_size_no+`  `+value.product_size_type+`</td>
                          </tr>
                        </table>
                        <span class="small">
                          <a href="/ask-seller/`+value.owner_email+`">
                            <i class="fa fa-envelope"></i> Send Message
                          </a> 
                          - 
                          <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)">
                            <i class="fa fa-shopping-cart"></i> Add to Cart
                          </a>
                        </span> 
                      </p>
                    </div>
                    <div class="card-footer">
                      
                    </div>
                  </div>
                </div>
              `);
            });
          },
          error: function (data){
            console.log(data);
          }
        });
        return false;
      }

      // main search 
      function mainSearch() {
        // body...
        var category = $("#keywords").val();

        // data to json
        var data = {
          category:category
        };

        // get data with ajax
        $.ajax({
          url: '/load/search/results',
          type: 'GET',
          dataType: 'json',
          data: data,
          success: function (data){
            console.log(data);
            $(".load-products").html("");
            $.each(data, function (index, value){
              $(".load-products").append(`
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="/product/details/?id=`+value.id+`"><img class="img-rounded" src="/uploads/products/`+value.product_image+`" height="150" width="230" alt=""></a>
                    <div class="card-body">
                      <br />
                      <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
                      <p class="small">
                        <table class="table small">
                          <tr>
                            <td>Product</td>
                            <td>`+value.product_name+`</td>
                          </tr>
                          <tr>
                            <td>Location</td>
                            <td>`+value.product_location+`</td>
                          </tr>

                          <tr>
                            <td>Weight</td>
                            <td>`+value.product_size_no+`  `+value.product_size_type+`</td>
                          </tr>
                        </table>
                        <span class="small">
                          <a href="/ask-seller/`+value.owner_email+`">
                            <i class="fa fa-envelope"></i> Send Message
                          </a> 
                          - 
                          <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)">
                            <i class="fa fa-shopping-cart"></i> Add to Cart
                          </a>
                        </span> 
                      </p>
                    </div>
                    <div class="card-footer">
                      
                    </div>
                  </div>
                </div>
              `);
            });
          },
          error: function (data){
            // console.log(data);
            alert('Fail to send search request !..');
          }
        });

        return false;
      }

      // Add to cart 
      function addToCart(x){
        // Get Elemet info
        var token     = '{{ csrf_token() }}';
        var item_id   = x;
        var item_type = 'produce';

        // Data to json
        var data = {
          _token:token,
          item_id:item_id,
          item_type:item_type
        }

        // Ajax Send
        $.ajax({
          url: '/add/to/cart',
          type: 'post',
          dataType: 'json',
          data: data,
          success: function (data){
            // console.log(data);
            if(data.status == 'success'){
              // refresh shopping cart
              loadShoppingCart();
              popMolly(data.message, data.status);
            }else{
              popMolly(data.message, data.status);
            }
          },
          error: function (data){
            alert('Error, Fail to send add to cart request !');
          }
        });
      }

      // show popup
      function popMolly(message, status) {
        swal(
          "Ok",
          message,
          status
        );
      }
  </script>
@endsection
