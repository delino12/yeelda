@extends('layouts.web-skin')

@section('title')
	YEELDA | Farm Services Marketplace
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.equipment-market')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
	<script type="text/javascript">
		$.get('/load/farmers/equipments', function (e){
		    // console.log(e);
		    $(".load-equipments").html("");
		    var sn = 0;
		    $.each(e, function (index, value){
		      sn++;
		      // console.log(value);
		        $(".load-equipments").append(`
		          <div class="col-md-3">
		            <div class="card" style="padding:0.4em;">
		              <img class="card-img-top img-rounded" src="/uploads/equipments/`+value.equipment_image+`" width="250" height="250" alt="Card image cap">
		              <hr />
		              <div class="card-block">
		                <h4 class="lead" style="font-size: 13px;"> `+value.equipment_name+` <span class="badge">`+value.equipment_no+`</span></h4>
		              </div>
		              <table class="table">
		                <tr>
		                  <td>Availability</td>
		                  <td>`+value.equipment_status+`</td>
		                </tr>
		                <tr>
		                  <td>Delivery</td>
		                  <td>`+value.equipment_delivery_type+`</td>
		                </tr>
		                <tr>
		                  <td>Daily</td>
		                  <td>&#8358;`+value.total+`</td>
		                </tr>
		              </table>
		              <div class="lead">
		                <span class="small" style="font-size: 13px;">
		                    <a href="/ask-seller/`+value.owner_email+`">
		                      <i class="fa fa-envelope"></i> Send Message
		                    </a> 
		                    - 
		                    <a href="/request-hire/`+value.id+`">
		                      <i class="fa fa-truck"></i> Hire now
		                    </a>
		                  </span> 
		              </div>
		            </div>
		          </div>
		        `);
		    });
		});

	  	// select seeds
		function showContents(x) {
			// body...
			if(x == 'seeds'){
				// alert('seeds');
				$(".y-card-menu").hide();
				$(".y-equipment-card").toggle();
				$(".load-seeds").toggle();
			}

			if(x == 'pesticide'){
				// alert('pesticide');
				$(".y-card-menu").hide();
				$(".y-equipment-card").toggle();
				$(".load-pesticide").toggle();
			}

			if(x == 'fertilizer'){
				// alert('fertilizer');
				$(".y-card-menu").hide();
				$(".y-equipment-card").toggle();
				$(".load-fertilizer").toggle();
			}

			if(x == 'equipment'){
				// alert('equipment');
				$(".y-card-menu").hide();
				$(".y-equipment-card").toggle();
				$(".load-equipments").toggle();
			}


			if(x == 'farmers'){
				window.location.href = '/search-farmer';
			}

			if(x == 'services'){
				window.location.href = '/search-service';
			}
		}

		// load all equipments
		$.get('/load/farmers/equipments', function (data){
		    // console.log(e);
		    $(".load-equipments").html(`
		    	<div class="col-md-12">
		    		<img src="/images/icon-set/equipment.png" width="50" height="auto" /> Farm Equipment
		    	<br /><br />
		    	</div>
		    `);
		    var sn = 0;
		    $.each(data, function (index, value){
		      sn++;
		      // console.log(value);
		        $(".load-equipments").append(`
		          <div class="col-md-3">
		            <div class="card" style="padding:0.4em;">
		              <img class="card-img-top" src="/uploads/equipments/`+value.equipment_image+`" width="250" height="250" alt="Card image cap">
		              <hr />
		              <div class="card-block">
		                <h4 class="lead" style="font-size: 13px;"> `+value.equipment_name+` <span class="badge">`+value.equipment_no+`</span></h4>
		              </div>
		              <table class="table">
		                <tr>
		                  <td>Availability</td>
		                  <td>`+value.equipment_status+`</td>
		                </tr>
		                <tr>
		                  <td>Delivery</td>
		                  <td>`+value.equipment_delivery_type+`</td>
		                </tr>
		                <tr>
		                  <td>Daily</td>
		                  <td>&#8358;`+value.total+`</td>
		                </tr>
		              </table>
		              <div class="lead">
		                <span class="small" style="font-size: 13px;">
		                    <a href="/ask-seller/`+value.owner_email+`">
		                      <i class="fa fa-envelope"></i> Send Message
		                    </a> 
		                    - 
		                    <a href="/request-hire/`+value.id+`">
		                      <i class="fa fa-truck"></i> Hire now
		                    </a>
		                  </span> 
		              </div>
		            </div>
		          </div>
		        `);
		    });
		});

		// load seeds 
		$.get('/load/farmers/seeds', function(data) {
			/*optional stuff to do after success */
			$(".load-seeds").html(`
		    	<div class="col-md-12">
		    		<img src="/images/icon-set/seeds.png" width="50" height="auto" /> Crop Seeds
		    		<br /><br />
		    	</div>
		    `);
		    var sn = 0;
		    $.each(data, function (index, value){
		      	sn++;
		        $(".load-seeds").append(`
		          	<div class="col-md-3">
		          	<div class="div-card">
		              	<img class="card-img-top" src="/uploads/seed-images/`+value.avatar+`" width="100%" height="200" alt="Card image cap">
		              	<hr />
		              	<table class="table small">
			                <tr>
			                  	<td>Produce</td>
			                  	<td>${value.name}</td>
			                </tr>
			                <tr>
			                  	<td>Cost</td>
			                  	<td>&#8358;${value.amount}</td>
			                </tr>
			                <tr>
			                  	<td>
			                  		<a href="/ask-seller/`+value.owner_email+`">
			                     		<i class="fa fa-envelope"></i> Contact Seller
			                    	</a>
			                    </td>
			                  	<td>
			                  		
			                	</td>
			                </tr>
		              	</table>
		            </div>
		          	</div>
		        `);
		    });
		});

		// load fertilizers
		$.get('/load/farmers/fertilizers', function(data) {
			/*optional stuff to do after success */
			// console.log(data);
			$(".load-fertilizer").html(`
		    	<div class="col-md-12">
		    		<img src="/images/icon-set/fertilizer.png" width="50" height="auto" /> Fertilizers
		    		<br /><br />
		    	</div>
		    `);
		    var sn = 0;
		    $.each(data, function (index, value){
		      	sn++;
		        $(".load-fertilizer").append(`
		        	<div class="col-md-3">
		          	<div class="div-card">
		              	<img class="card-img-top" src="/uploads/fertilizer-images/`+value.avatar+`" width="100%" height="200" alt="Card image cap">
		              	<hr />
		              	<table class="table small">
			                <tr>
			                  	<td>Produce</td>
			                  	<td>${value.name}</td>
			                </tr>
			                <tr>
			                  	<td>Cost</td>
			                  	<td>&#8358;${value.amount}</td>
			                </tr>
			                <tr>
			                  	<td>
			                  		<a href="/ask-seller/`+value.owner_email+`">
			                     		<i class="fa fa-envelope"></i> Contact Seller
			                    	</a>
			                    </td>
			                  	<td>
			                  		
			                	</td>
			                </tr>
		              	</table>
		            </div>
		          	</div>
		        `);
		    });
		});
	</script>
@endsection
