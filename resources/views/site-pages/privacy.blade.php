@extends('layouts.web-skin')

@section('title')
	YEELDA | Privacy
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}
	<div class="container">
		<div class="row" style="margin-top: 130px;">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;">
					Privacy policy
				</span>
			</div>
		</div>
	</div>

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	
@endsection
