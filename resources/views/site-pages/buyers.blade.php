@extends('layouts.web-skin')

@section('title')
	YEELDA | Buyers
@endsection

@section('contents')
	<style type="text/css">
		.row {
			margin-left: 0px;
			margin-right: 0px;
		}
		.col-md-2 {
		    width: 19.66667%;
		}

		@media screen and (max-width: 768px) {
		  .step-count {
		    display: none;
		  }
		  .col-md-2 {
		    width: 97.66667%;
		  }
		}

		@media screen and (max-width: 992px) {
		  .step-count {
		    display: none;
		  }
		  .col-md-2 {
		    width: 97.66667%;
		  }

		  #platform-slide {
		  	display: none;
		  }
		}
	</style>

	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}

	<div class="container">
		<div class="row" style="margin-top: 130px;">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;">
					Buyers
				</span>
				<span><br />Place your orders for fresh farm produce</span>
			</div>
		</div>

		<br /><br />
		<div class="row">
			<div class="col-md-2">
				<h5 style="font-size: 24px;font-weight: bold;">HOW IT WORKS</h5>
			</div>
		</div>
		<br />
		<div class="row text-center step-count">
			<div class="col-md-2">Step 1</div>
			<div class="col-md-2">Step 2</div>
			<div class="col-md-2">Step 3</div>
			<div class="col-md-2">Step 4</div>
			<div class="col-md-2">Step 5</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">person_add</i>
					<h5 style="font-size: 14px;font-weight: bold;">Sign Up</h5>
					<p class="small">Create an account using your email and password in less than 60secs.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">local_grocery_store</i>
					<h5 style="font-size: 14px;font-weight: bold;">Market Place</h5>
					<p class="small">
						This page displays all harvested and yet to be harvested farm produce by the farmers which helps you navigate to the farm produce/commodity of your choice and simply add to cart.
					</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">credit_card</i>
					<h5 style="font-size: 14px;font-weight: bold;">Make Payment</h5>
					<p class="small">This platform makes payment so easy, with different payment options.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">location_on</i>
					<h5 style="font-size: 14px;font-weight: bold;">Track Farm Produce</h5>
					<p class="small">Sms/Mail notification is sent at every drop location with a unique generated tracking code that helps you monitor your order directly to your door step.</p>
				</div>
			</div>
			<div class="col-md-2">
				<div class="text-center">
					<i class="material-icons" style="font-size: 65px;margin-top: 15px;color:#2CA7CB;">local_shipping</i>
					<h5 style="font-size: 14px;font-weight: bold;">Farm Produce Delivered</h5>
					<p class="small">All Farm Produce are received in good condition.</p>
				</div>
			</div>
		</div>
	</div>

	{{-- platform features --}}
    <div id="platform-slide">
	    @include('web-components.platform-features')
	</div>

	{{-- blogs --}}
    {{-- @include('web-components.blogs') --}}

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
	
@endsection
