@extends('layouts.web-skin')

@section('title')
	YEELDA | FAQ
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}
	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content" style="margin-top: 130px;">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;">
					FAQ
				</span>
			</div>
		</div>
	</div>
	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content">
			<div class="col-md-12">
				<div class="y-faq-image"></div>
				<div class="y-faq-card">
					<div class="panel-group">
					    <div class="panel panel-default" style="border: none;">
					      	<div class="panel-heading" style="background-color: transparent;">
					        	<h4 class="panel-title">
						          	<span class="y-simple-title">
						          		What is YEELDA?
						          	</span>

					          		<span class="pull-right">
					          			<a href="#collapse1" data-toggle="collapse">
					          				<i class="fa fa-plus"></i>
					          			</a>
					          		</span>
					        	</h4>
					      	</div>
					      	<div id="collapse1" class="panel-collapse collapse" style="border:none;">
					        	<div class="panel-body">Answer 1</div>
					      	</div>
					    </div>
					</div>

					<div class="panel-group">
					    <div class="panel panel-default" style="border: none;">
					      	<div class="panel-heading" style="background-color: transparent;">
					        	<h4 class="panel-title">
						          	<span class="y-simple-title">
						          		How can I make purchase?
						          	</span>

					          		<span class="pull-right">
					          			<a href="#collapse2" data-toggle="collapse">
					          				<i class="fa fa-plus"></i>
					          			</a>
					          		</span>
					        	</h4>
					      	</div>
					      	<div id="collapse2" class="panel-collapse collapse" style="border:none;">
					        	<div class="panel-body">Answer 2</div>
					      	</div>
					    </div>
					</div>

					<div class="panel-group">
					    <div class="panel panel-default" style="border: none;">
					      	<div class="panel-heading" style="background-color: transparent;">
					        	<h4 class="panel-title">
						          	<span class="y-simple-title">
						          		How can I join the YEELDA's Community?
						          	</span>

					          		<span class="pull-right">
					          			<a href="#collapse3" data-toggle="collapse">
					          				<i class="fa fa-plus"></i>
					          			</a>
					          		</span>
					        	</h4>
					      	</div>
					      	<div id="collapse3" class="panel-collapse collapse" style="border:none;">
					        	<div class="panel-body">Answer 3</div>
					      	</div>
					    </div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="text-center" style="background-image: url({{asset('images/img-set/pyramid.png')}});background-position: fill;background-size: cover; width: 100%; height: 250px;">
				<div style="height: 90px;"></div>
				<a href="{{url('signup')}}">
					<button class="y-btn y-btn-white-bg">
						Get Started
					</button>
				</a>
			</div>
		</div>
	</div>


    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
    
@endsection

@section('scripts')
	
@endsection
