@extends('layouts.web-skin')

@section('title')
	YEELDA | Services Providers
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}
	<div class="container">
		<div class="row" style="margin-top: 140px;">
			<div class="col-md-7">
				<div class="card" style="box-shadow: 0px 0px 4px 0px #CCC;font-size: 12px;">
					<div class="card-body">
						<h4 style="padding: 1rem;">Shopping Carts (Items)</h4>
						<table class="table text-center">
							<thead>
								<tr>
									<th>S/N</th>
									<th>Item</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Amount</th>
									<th>Total</th>
									<th>Date</th>
									<th>Option</th>
								</tr>
							</thead>
							<div class="">
								<tbody class="shopping-list"></tbody>
							</div>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="card" style="box-shadow: 0px 0px 4px 0px #CCC;padding: 10px;font-size: 12px;">
					<div class="card-body">
						<h4>Payment Summary</h4>
						<table class="table">
							<tr>
								<td><b>Total Cost:</b></td>
								<td><span class="cost"></span></td>
							</tr>
							<tr>
								<td><b>Delivery charge: </b></td>
								<td><span class="d_cost"></span></td>
							</tr>
							<tr>
								<td><b>Yeelda charge: </b></td>
								<td><span class="y_cost"></span></td>
							</tr>
							<tr>
								<td><b>Total: </b></td>
								<td><span class="t_cost"></span></td>
							</tr>
						</table>
						<div class="pay-button"></div>
						{{-- <div class="">
							<a class="btn btn-primary" style="color:#fff;border-radius: 5px;" href="javascript:void(0)" onclick="startPaymentWithWallet()"> 
				            	<i class="fa fa-credit-card"></i> Pay via wallet
				            </a>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
	</div>

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	<script type="text/javascript">
		// load shopping carts items
		var logged_email = '{{ $email }}';

		var total 	= 0;
		var itemIds = [];
		// load shopping list 
		$.get('/load/shopping/list/?email='+logged_email, function(data) {
			/*optional stuff to do after success */
			$('.shopping-list').html('');
			var sn = 0;
			// console log data
			if(data.total == 0){
				swal(
					"Oops",
					"Your shopping cart is empty!",
					"info"
				);
				window.location.href = "/produce-marketplace";
			}

			$.each(data.details, function(index, el) {
				// console.log(el);
				sn++;
				// all item id
				itemIds.push(el.item_id);
				$('.shopping-list').append(`
					<tr>
						<td>`+sn+`</td>
						<td>
						<img class="img-square" src="/uploads/products/`+el.image+`" width="120" height="80" alt=""><br />
						`+el.name+`</td>
						<td>`+el.size_no.toLocaleString()+`</td>
						<td>&#8358;`+el.price.toLocaleString()+`</td>
						<td>&#8358;`+el.amount.toLocaleString()+`</td>
						<td>&#8358;`+el.total.toLocaleString()+`</td>
						<td>`+el.date+`</td>
						<td><a href="/remove/item/`+el.id+`">Remove</a></td>
					</tr>
				`);
			});

			$('.shopping-list').append(`
				<tr>
					<td>Total</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><b>&#8358;`+data.total.toLocaleString()+`</b></td>
					<td></td>
					<td></td>
				</tr>
			`);

			$(".cost").html('&#8358;'+data.total.toLocaleString());
			$(".d_cost").html('&#8358; 0.00');
			$(".y_cost").html('&#8358; 0.00');
			$(".t_cost").html('&#8358;'+data.total.toLocaleString());

			// final total 
			total = data.total;

			$('.pay-button').append(`
	            <a class="btn btn-primary" style="color:#fff;border-radius: 5px;" href="javascript:void(0)" onclick="startPayment()"> 
	            	<i class="fa fa-money"></i> Pay Now
	            </a>
			`);
		});

		// function start payment
		function startPayment(){
			// filter to string representation
			var allPids = itemIds.join();
			// console.log('all item id: '+allPids);
			// allPids means all product item ids
			// navigate to payment system
			window.location.href = '/continue/payment/?amount='+total+'&email='+logged_email+'&pids='+allPids;
		}

		// pay via wallet
		function startPaymentWithWallet() {
			swal(
				"oops",
				"Wallet payment is currently not available at the moment",
				"info"
			);
		}
	</script>
@endsection
