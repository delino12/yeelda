@extends('layouts.web-skin')

@section('title')
	YEELDA | Marketplace
@endsection

@section('contents')
	<link rel="stylesheet" href="{{asset('dropdown/css/dropdown.css')}}">
	<style type="text/css">
		.dropdown-toggle {
			box-shadow: none;
		}

		.navbar-nav > li .dropdown-menu {
			margin-top: 41px;
		}
	</style>
	
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.future-marketplace')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.select-dropdown').dropdown({
				multi: false,
				toggleText: 'Select items'
			});
		});

		$.get('/load/farmers/future/products', function (data){
	      $(".load-products").html("");
	      $.each(data, function (index, value){
	        // cart option
	        if(value.product_status == 'Harvested'){
	          var cartOption = `
	            <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)">
	              <i class="fa fa-shopping-cart"></i> Add to Cart
	            </a>
	          `;
	        }else{
	          var cartOption = `
	             <a href="/product/details/?id=`+value.id+`">
	              <i class="fa fa-book"></i> Details
	            </a>
	          `;
	        }

	        var urlToImage;
	        if (value.product_image.indexOf('YEELDA-') > -1){
	          urlToImage = `/uploads/products/${value.product_image}`;
	        }else{
	          urlToImage = value.product_image;
	        }

	        $(".load-products").append(`
	          <div class="col-md-3">
	            <div class="card h-100">
	              <a href="/product/details/?id=`+value.id+`">
	                <img class="img-rounded" src="${urlToImage}" height="180" width="100%" alt="">
	              </a>
	              <div class="card-body">
	                <br />
	                <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
	                <p class="small">
	                  <table class="table small">
	                    <tr>
	                      <td>Product</td>
	                      <td>`+value.product_name+`</td>
	                    </tr>
	                    <tr>
	                      <td>State</td>
	                      <td>`+value.product_state+`</td>
	                    </tr>
	                    <tr>
	                      <td>Availability</td>
	                      <td>`+value.product_status+`</td>
	                    </tr>
	                  </table>
	                  <span class="small">
	                    <a href="/ask-seller/`+value.owner_email+`">
	                      <i class="fa fa-envelope"></i> Send Message
	                    </a> 
	                    - 
	                   `+cartOption+`
	                  </span> 
	                </p>
	              </div>
	              <div class="card-footer">
	                
	              </div>
	            </div>
	            <br />
	          </div>
	        `);
	      });
	    });


	    // custom search
	    function customSearch(argument) {
	      // body...
	      var category  = $('#listed-product').val();
	      var location  = $('#listed-location').val();
	      var priceFrom = $('#price-from').val();
	      var priceTo   = $('#price-to').val();

	      // data to json
	      var data = {
	        category:category,
	        location:location,
	        priceFrom:priceFrom,
	        priceTo:priceTo      
	      };

	      $.ajax({
	        url: '/load/search/results',
	        type: 'GET',
	        dataType: 'json',
	        data: data,
	        success: function (data){
	          // console.log(data);
	          // $('.load-products').html();
	          $(".load-products").html("");
	          $.each(data, function (index, value){
	            $(".load-products").append(`
	              <div class="col-lg-4 col-md-6 mb-4">
	                <div class="card h-100">
	                  <a href="/product/details/?id=`+value.id+`">
	                    <img class="img-rounded" src="/uploads/products/`+value.product_image+`" height="150" width="230" alt="">
	                  </a>
	                  <div class="card-body">
	                    <br />
	                    <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
	                    <p class="small">
	                      <table class="table small">
	                        <tr>
	                          <td>Product</td>
	                          <td>`+value.product_name+`</td>
	                        </tr>
	                        <tr>
	                          <td>Location</td>
	                          <td>`+value.product_location+`</td>
	                        </tr>

	                        <tr>
	                          <td>Weight</td>
	                          <td>`+value.product_size_no+`  `+value.product_size_type+`</td>
	                        </tr>
	                      </table>
	                      <span class="small">
	                        <a href="/ask-seller/`+value.owner_email+`">
	                          <i class="fa fa-envelope"></i> Send Message
	                        </a> 
	                        - 
	                        <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)">
	                          <i class="fa fa-shopping-cart"></i> Add to Cart
	                        </a>
	                      </span> 
	                    </p>
	                  </div>
	                  <div class="card-footer">
	                    
	                  </div>
	                </div>
	              </div>
	            `);
	          });
	        },
	        error: function (data){
	          console.log(data);
	        }
	      });
	      return false;
	    }

	    // main search 
	    function mainSearch() {
	      // body...
	      var category = $("#keywords").val();

	      // data to json
	      var data = {
	        category:category
	      };

	      // get data with ajax
	      $.ajax({
	        url: '/load/search/results',
	        type: 'GET',
	        dataType: 'json',
	        data: data,
	        success: function (data){
	          console.log(data);
	          $(".load-products").html("");
	          $.each(data, function (index, value){
	            $(".load-products").append(`
	              <div class="col-lg-4 col-md-6 mb-4">
	                <div class="card h-100">
	                  <a href="/product/details/?id=`+value.id+`"><img class="img-rounded" src="/uploads/products/`+value.product_image+`" height="150" width="230" alt=""></a>
	                  <div class="card-body">
	                    <br />
	                    <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
	                    <p class="small">
	                      <table class="table small">
	                        <tr>
	                          <td>Product</td>
	                          <td>`+value.product_name+`</td>
	                        </tr>
	                        <tr>
	                          <td>Location</td>
	                          <td>`+value.product_location+`</td>
	                        </tr>

	                        <tr>
	                          <td>Weight</td>
	                          <td>`+value.product_size_no+`  `+value.product_size_type+`</td>
	                        </tr>
	                      </table>
	                      <span class="small">
	                        <a href="/ask-seller/`+value.owner_email+`">
	                          <i class="fa fa-envelope"></i> Send Message
	                        </a> 
	                        - 
	                        <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)">
	                          <i class="fa fa-shopping-cart"></i> Add to Cart
	                        </a>
	                      </span> 
	                    </p>
	                  </div>
	                  <div class="card-footer">
	                    
	                  </div>
	                </div>
	              </div>
	            `);
	          });
	        },
	        error: function (data){
	          // console.log(data);
	          alert('Fail to send search request !..');
	        }
	      });

	      return false;
	    }

	    // Add to cart 
	    function addToCart(x){
	      // Get Elemet info
	      var token     = '{{ csrf_token() }}';
	      var item_id   = x;
	      var item_type = 'produce';

	      // Data to json
	      var data = {
	        _token:token,
	        item_id:item_id,
	        item_type:item_type
	      }

	      // Ajax Send
	      $.ajax({
	        url: '/add/to/cart',
	        type: 'post',
	        dataType: 'json',
	        data: data,
	        success: function (data){
	          // console.log(data);
	          if(data.status == 'success'){
	            // refresh shopping cart
	            loadShoppingCart();
	            popMolly(data.message, data.status);
	          }else{
	            popMolly(data.message, data.status);
	          }
	        },
	        error: function (data){
	          alert('Error, Fail to send add to cart request !');
	        }
	      });
	    }

	    // show popup
	    function popMolly(message, status) {
	      swal(
	      	"Ok",
	      	message,
	      	status
	      );
	    }
	</script>
@endsection
