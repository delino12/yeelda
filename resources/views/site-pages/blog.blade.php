@extends('layouts.web-skin')

@section('title')
	YEELDA | Blog
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}
	<div class="container-fluid y-shade-bg">
		<div class="row" style="margin-top: 140px;">
		</div>

		<div class="row y-pad-content">
			<div class="col-md-3">
				<!-- Categories Widget -->
			    <div class="card" style="padding: 1rem;">
			        <div class="card-header">
			        	<h1 class="lead" style="padding: 2rem;">Categories</h1>
			        </div>
			        <div class="card-body">
			              <ul class="list-unstyled mb-0">
			                <li style="padding: 2rem;">
			                  <a href="#">Farmers Meet-up</a>
			                </li>
			                <li style="padding: 2rem;">
			                  <a href="#">Agricultural Produce</a>
			                </li>
			                <li style="padding: 2rem;">
			                  <a href="#">More</a>
			                </li>
			                <li style="padding: 2rem;">
			                  <a href="#">Poultry</a>
			                </li>
			                <li style="padding: 2rem;">
			                  <a href="#">Livestocks</a>
			                </li>
			                <li style="padding: 2rem;">
			                  <a href="#">Reasearch</a>
			                </li>
			              </ul>
			        </div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="y-search-div">
					<input type="text" class="select-search-input" placeholder="Enter search.. Climate update, Farmers meetup and Weather Focast" name="">
					<button class="y-search-btn">
						<i class="fa fa-search"></i>
					</button>
				</div>
				<hr />
				<div class="all-blog-posts"><img src="/svg/yeelda-loading.svg" height="60px" width="auto"></div>
			</div>
		</div>
	</div>

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
	{{-- scripts here --}}
	<script type="text/javascript">
	    
	  // load blogs posts
	  function loadBlogPosts(argument) {
	    // load blogs posts 
	    $.get('/load/blog/posts', function(data) {
	      /*optional stuff to do after success */
	      // console.log(data);
	      $('.all-blog-posts').html("");
	      $.each(data, function(index, val) {
	        /* iterate through array or object */
	        $('.all-blog-posts').append(`
	          <h1 class="mt-4">`+val.title+`</h1>
	          <p class="lead small">
	            Author:
	            <a href="#">`+val.by+`</a>
	          </p>
	          <p class="small">Last updated: `+val.date+`</p>
	          <img class="img-square" src="`+val.image+`" width="100%" height="320px" alt="">

	          <hr>
	          <p class="small">`+val.body+`</p>
	          <a href="#"><i class="fa fa-share-alt"></i> Share</a>
	          <a href="https://www.facebook.com/sharer/sharer.php?u=yeelda.ng/blogs/news"><i class="fa fa-facebook"></i> facebook</a> - 
	          <a href="https://twitter.com/home?status=yeelda.ng/blogs/news"><i class="fa fa-twitter"></i> twitter</a> - 
	          <a href="https://plus.google.com/share?url=yeelda.ng/blogs/news"><i class="fa fa-google"></i> google+</a> - 
	          <span class="pull-right">
	            <a href="#" class="btn"><i class="fa fa-thumbs-up"></i>Like</a>
	            <a href="#" class="btn"><i class="fa fa-thumbs-down"></i>Unlike</a>
	          </span>
	          <hr>

	          <!-- Single Comment -->
	          <div class="load-comment-`+val.id+`"></div>

	          <!-- Comments Form -->
	          <div class="card my-4">
	            <h5 class="card-header">Leave a Comment:</h5>
	            <div class="card-body">
	              <form class="comment-form" onsubmit="return false">
	                <div class="form-group">
	                  <textarea class="form-control" id="comment_`+val.id+`" rows="3"></textarea>
	                </div>
	                <button type="submit" onclick="addCommentToPost('`+val.id+`')" class="btn btn-primary">Submit</button>
	              </form>
	              <div class="error_msg"></div>
	              <div class="success_msg"></div>
	            </div>
	          </div>
	          <br /><br />
	        `);

	        // get all comments relating to post
	        // load comments 
	        $.each(val.comments, function(index, comment) {
	          /* iterate through array or object */
	          // console.log(comment);
	          $('.load-comment-'+val.id).append(`
	            <!-- Single Comment -->
	            <div class="media mb-4">
	              <img class="d-flex mr-3 rounded-circle" src="/images/profile-image.png" alt="">
	              <div class="media-body">
	                <span class="mt-0 small">`+comment.name+`</span><br />
	                `+comment.body+`
	                <span class="small pull-right">`+comment.date+`</span>
	              </div>
	            </div>
	            <br />
	          `);
	        });
	      });
	    });
	  }

	  // add comment to blog
	  function addCommentToPost(post_id) {
	    // body...
	    var token   = '{{ csrf_token() }}'; 
	    var body    = $('#comment_'+post_id).val();
	    var blog_id = post_id;

	    // data json
	    var data = {
	      _token:token,
	      blog_id:blog_id,
	      body:body
	    };

	    $.ajax({
	      url: '/post/blog/comment',
	      type: 'POST',
	      dataType: 'json',
	      data: data,
	      success: function (data){
	        console.log(data);
	        if(data.status == 'success'){
	          $('.success_msg').html(`
	            <span class="text-success">`+data.message+`</span>
	          `);
	          loadBlogPosts();
	          $('.comment-form')[0].reset();
	        }
	        if(data.status == 'error'){
	          $('.error_msg').html(`
	            <span class="text-danger">`+data.message+`</span>
	          `);
	        }
	      },
	      error: function (data){
	        alert('Error, Fail request on post comment !');
	      }
	    });
	    return false;
	  }

	  // load recent comments
	  function loadRecentPost(){
	    // load recent side bar
	    $.get('/load/blog/posts/recents', function(data) {
	      /*optional stuff to do after success */
	      $('.all-recent-posts').html("");
	      $.each(data, function(index, val) {
	        /* iterate through array or object */
	        $('.all-recent-posts').append(`
	          <img class="img-fluid rounded" src="/blogs/images/`+val.image+`" width="30%" height="20%" alt="">
	          <p class="small">`+val.title+`</p>
	        `);
	      });
	    });
	  }

	  // init load data
	  // $(document).ready(function (){
	    loadBlogPosts();
	    loadRecentPost();
	  // });
	</script>
@endsection
