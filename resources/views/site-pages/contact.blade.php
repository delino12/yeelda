@extends('layouts.web-skin')

@section('title')
	YEELDA | Contact us
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	{{-- @include('web-components.all-farmers') --}}
	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content" style="margin-top: 90px;">
			<div class="col-md-12 text-justify">
				<span style="font-size: 90px;font-weight: 500;">
					Get in touch
				</span>
				<span class="small">
					<br />
					We reply in less than one hour. Seriously, try us.
				</span>
			</div>
		</div>
	</div>

	<div class="container-fluid y-shade-bg">
		<div class="row y-pad-content">
			<div class="y-feature-card-contact">
                <div class="col-md-3" style="padding: 0em;">
                    <div class="y-feature-sidebar-contact">
                        <p style="margin-left: 30px;padding: 3em;margin-top:20px;color:#000;font-weight: 500;">
                        	CONTACT INFORMATION
                        </p>

                        <div class="row" style="margin-left: 15px;padding: 1em;margin-top:10px;color:#000;">
	                       	<div class="col-xs-1">
	                       		<i class="fa fa-map-marker"></i>
	                       	</div>
	                       	<div class="col-xs-10">
	                       		No 7b Dr. Ezukuse close,lekki phase 1
	                       	</div>
	                    </div>

	                    <div class="row" style="margin-left: 15px;padding: 1em;margin-top:10px;color:#000;">
	                       	<div class="col-xs-1">
	                       		<i class="fa fa-phone"></i>
	                       	</div>
	                       	<div class="col-xs-10">
	                       		(+234) 7 333 4445
	                       	</div>
	                    </div>

	                    <div class="row" style="margin-left: 15px;padding: 1em;margin-top:10px;color:#000;">
	                       	<div class="col-xs-1">
	                       		<i class="fa fa-envelope"></i>
	                       	</div>
	                       	<div class="col-xs-10">
	                       		info@yeelda.com
	                       	</div>
	                    </div>
                    </div>
                </div>
                <div class="col-md-9" style="padding: 0em;">
                    <div class="y-feature-body-contact">
                    	<div class="y-contact-form-div">
	                    	<form method="post" id="y-contact-form" onsubmit="return sendContactMessage()">
	                    		
                    			<div class="row">
		                    		<div class="col-md-6">
		                    			<div class="form-group">
			                    			<label for="name">Name</label>
			                    			<input type="text" id="name" class="form-control y-contact-input" placeholder="What's your name?" name="name" required="">
			                    		</div>
			                    	</div>
			                    	<div class="col-md-6">
			                    		<div class="form-group">
			                    			<label for="email">Email</label>
			                    			<input type="email" id="email" class="form-control y-contact-input" placeholder="What's your email?" name="email" required="">
			                    		</div>
			                    	</div>
	                    		</div>

	                    		
								<div class="row">
			                    	<div class="col-md-12">
		                    			<div class="form-group">
			                    			<label for="message">Message</label>
			                    			<textarea onkeyup="countCharacters()" class="form-control y-contact-textarea" id="message" name="message" placeholder="What's your message?" cols="10" rows="7" maxlength="3000" required=""></textarea>
			                    		</div>
			                    	</div>
	                    		</div>
	                    		
                    			<div class="row">
                    				<div class="col-md-6">
                    					<div class="form-group">
	                    					<button type="submit" id="y-contact-btn" class="y-btn y-btn-info">
			                    				<i class="fa fa-location-arrow"></i> Send
			                    			</button>
	                    				</div>
                    				</div>
                    				<div class="col-md-6 text-right">
                    					<span class="small count-characters"></span>
                    				</div>
                    			</div>
	                    	</form>
	                    </div>
                    </div>
                </div>
            </div>
		</div>
	</div>

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
    
@endsection

@section('scripts')
	<script type="text/javascript">
		// count chars
		function countCharacters() {
			var limit 	= 3000;
			var message = $("#message").val();
			var remaining;

			if(message.length > limit){
				remaining = (message.length - limit);
			}else{
				remaining = (limit - message.length);
			}

			$(".count-characters").html(`
				<b>Total:</b> ${limit} / ${remaining}
			`);
		}

		// send message
		function sendContactMessage() {
			$("#y-contact-btn").prop("disabled", true);
			$("#y-contact-btn").html(`
				<i class="fa fa-spinner"></i> Sending...
			`);

			var token 	= "{{ csrf_token() }}";
			var name 	= $("#name").val();
			var email 	= $("#email").val();
			var message = $("#message").val();

			var params = {
				_token: token,
				subject: "New message from " +name,
				name: name,
				email: email,
				message: message
			}

			$.post('{{url("send/contact/message")}}', params, function(data, textStatus, xhr) {
				if(data.status == "success"){
					swal(
						"Ok",
						"Message Sent!",
						data.status
					);

					$("#y-contact-btn").prop("disabled", false);
					$("#y-contact-btn").html(`
						<i class="fa fa-location-arrow"></i> Send
					`);

					$("#y-contact-form")[0].reset();
				}else{
					swal(
						"Ok",
						"Sending message failed!",
						data.status
					);
					$("#y-contact-btn").prop("disabled", false);
					$("#y-contact-btn").html(`
						<i class="fa fa-location-arrow"></i> Send
					`);
				}
			}).fail(function(err){
				$("#y-contact-btn").prop("disabled", false);
				$("#y-contact-btn").html(`
					<i class="fa fa-location-arrow"></i> Send
				`);
			});

			// return 
			return false;
		}
	</script>
@endsection
