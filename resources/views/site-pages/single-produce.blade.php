@extends('layouts.web-skin')

@section('title')
	YEELDA | View Produce
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- response message --}}
	<div style="height: 140px;"></div>
	<div class="container-fluid">
	    <div class="row">
	      	<div class="col-md-6">
	      		<div class="success_msg" style="position: fixed; z-index: 10; top: 180px; right: 20px;"></div>
	        	<div class="load-products"></div>
	      	</div>

	      	<div class="col-md-6">	        
		        <h2 class="lead">
		        	<img src="/images/icon-set/produce.png" width="42" height="42"> view more produces
		        </h2>
	        	<hr />
	        	<div class="load-products-related">
	          		<img src="/svg/yeelda-loading.svg" width="auto" height="70px" class="loading">
	        	</div>
	      	</div>
	    </div>
	</div>
	<div style="height: 140px;"></div>

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')
@endsection

@section('scripts')
	<script type="text/javascript">
		var pid = '{{ $product_id }}';
	    $.get('/load/single/produce/?id='+pid, function (data){
		    console.log(data);
		    // cart option
	        if(data.product_status == 'ready'){
	          var cartOption = `
	            <a href="javascript:void(0);" class="pull-right" onclick="addToCart(`+data.id+`)">
	              <i class="fa fa-shopping-cart"></i> Add to Cart
	            </a>
	          `;
	        }else{
	          var cartOption = `
	            
	          `;
	        }

	        var urlToImage;
	        if (data.product_image.indexOf('YEELDA-') > -1){
	          urlToImage = `/uploads/products/${data.product_image}`;
	        }else{
	          urlToImage = data.product_image;
	        }

		    $(".load-products").html(`
		    	<div class="col-md-9">
		            <div class="card h-100">
		            	<div class="card-header">
		            		<a href="/product/details/?id=`+data.id+`">
				                <img class="img-rounded img-responsive" src="${urlToImage}" height="180" width="100%" alt="">
				            </a>
		            	</div>
		                <div class="card-body">
		                    <br />
		                    <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+data.product_total+`</h5>
		                  <table class="table small">
		                    <tr>
		                      <td>Product</td>
		                      <td>`+data.product_name+`</td>
		                    </tr>
		                    <tr>
		                      <td>State</td>
		                      <td>`+data.product_state+`</td>
		                    </tr>
		                    <tr>
		                      <td>Availability</td>
		                      <td>`+data.product_status+`</td>
		                    </tr>
		                    <tr>
		                      <td>Weight</td>
		                      <td>${data.product_size_no} ${data.product_size_type}</td>
		                    </tr>
		                  </table>
		                </div>
		                <div class="card-footer">
				            <div class="row">
				                <div class="col-md-12 small">
				                    <a href="/ask-seller/`+data.owner_email+`">
				                      <i class="fa fa-envelope"></i> Send Message
				                    </a> 
				                    
				                   `+cartOption+`
				                </div> 
				            </div>
		              	</div>
		            </div>
	            </div>
	            <br />
	        `);

		    if(data.owner_image == ""){
		        $(".load-basic").append(`
		          <img class="img-rounded" src="/uploads/farmers/`+data.owner_image+`" height="200" width="200" alt="farmer+image">
		        `);
		    }else{
		        $(".load-basic").append(`
		          <img class="img-rounded" src="/uploads/farmers/`+data.owner_image+`" height="200" width="200" alt="farmer+image">
		        `);
		   	}
	    });

	    // Add to cart 
	    function addToCart(x){
	      // Get Elemet info
	      var token     = '{{ csrf_token() }}';
	      var item_id   = x;
	      var item_type = 'produce';

	      // Data to json
	      var data = {
	        _token:token,
	        item_id:item_id,
	        item_type:item_type
	      }

	      // Ajax Send
	      $.ajax({
	        url: '/add/to/cart',
	        type: 'post',
	        dataType: 'json',
	        data: data,
	        success: function (data){
	          // console.log(data);
	          if(data.status == 'success'){
	            $(".success_msg").html(`
	              <div class="alert alert-success">
	                <p class="text-success">`+data.message+`</p>
	              </div>
	            `);
	          }

	          setTimeout(removeFlash, 1000 * 2);
	        },
	        error: function (data){
	          alert('Error, Fail to send add to cart request !');
	        }
	      });
	    }

	    $.get('{{url("load/recent/products")}}', function (data){
	      $(".load-products-related").html("");
	      var sn = 0;
	      $.each(data, function (index, value){
	        sn++;
	        // cart option
	        if(value.product_status == 'Harvested'){
	          var cartOption = `
	            <a href="javascript:void(0);" onclick="addToCart(`+value.id+`)" class="pull-right">
	              <i class="fa fa-shopping-cart"></i> Add to Cart
	            </a>
	          `;
	        }else{
	          var cartOption = `
	             <a href="/product/details/?id=`+value.id+`" class="pull-right">
	              <i class="fa fa-book"></i> Details
	            </a>
	          `;
	        }

	        var urlToImage;
	        if (value.product_image.indexOf('YEELDA-') > -1){
	          urlToImage = `/uploads/products/${value.product_image}`;
	        }else{
	          urlToImage = value.product_image;
	        }

	        $(".load-products-related").append(`
	          <div class="col-md-4 mb-4">
	            <div class="card h-100">
	              <div class="card-header">
            		<a href="/product/details/?id=`+value.id+`">
		                <img class="img-rounded" src="${urlToImage}" height="150" width="100%" alt="">
		            </a>
            	  </div>
	              <div class="card-body">
	                <br />
	                <h5 class="small"><i class="fa fa-tags"></i> &#8358; `+value.product_total+`</h5>
	                
	                  <table class="table small">
	                    <tr>
	                      <td>Product</td>
	                      <td>`+value.product_name+`</td>
	                    </tr>
	                    <tr>
	                      <td>Availability</td>
	                      <td>`+value.product_status+`</td>
	                    </tr>
	                  </table>
	              </div>
	              <div class="card-footer">
	                <div class="row">
		                <div class="col-md-12 small">
		                    <a href="/ask-seller/`+data.owner_email+`">
		                      <i class="fa fa-envelope"></i> Send Message
		                    </a> 
		                    
		                   `+cartOption+`
		                </div> 
		            </div>
	              </div>
	            </div>
	            <br />
	          </div>
	        `);
	      });
	    });
	</script>
@endsection
