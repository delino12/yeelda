@extends('layouts.web-skin')

@section('title')
	YEELDA | Search all farmers
@endsection

@section('contents')
	
	{{-- include plain header --}}
	@include('web-components.plain-header')

	{{-- include login form --}}
	@include('web-components.farmers-card')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	
@endsection
