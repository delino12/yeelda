@extends('layouts.web-skin')

@section('title')
	YEELDA | Search all services providers
@endsection

@section('contents')
	{{-- include plain header --}}
	@include('web-components.plain-header')

    {{-- include login form --}}
	@include('web-components.services-providers-card')

    {{-- footer --}}
    @include('web-components.footer')

    {{-- Social links mini footer --}}
    @include('web-components.social-media')

@endsection

@section('scripts')
	{{-- load services info --}}
	<script type="text/javascript">
		var services = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		for (var i = services.length - 1; i >= 0; i--) {
			console.log(services[i]);
			$('#load-services').append(`
				<div class="col-md-3">
					<div class="y-farmer-profile-card">
						<div class="y-farmer-image text-center">
							<img src="{{asset('images/services/service-provider-1.jpg')}}" class="img-rounded " width="120" height="auto">
						</div>
						<div class="y-farmer-initials text-center">
							<span><strong>John Nerdy</strong> | Service Providers</span>
						</div>
						<div class="y-farmer-addon text-center">
							<a href="#">
							<i class="fa fa-envelope" style="font-size: 18px;"></i>
						</div>
					</div>
					<br />
				</div>
			`);
		}
	</script>
@endsection
