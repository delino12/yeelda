<!DOCTYPE html>
<html>
<head>
	<title>Yeelda Account Activations</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
		body {
		    font-family: 'Amiko';font-size: 12px;
		}

		table, td, th {    
		    border: 1px solid #ddd;
		    text-align: left;
		}

		table {
		    border-collapse: collapse;
		    width: 50%;
		}

		th, td {
		    padding: 15px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<img class="img-square" src="https://yeelda.com/images/img-set/logo-transparent-new.png" width="20%" height="20%" alt="">
			<h2 class="lead">Yeelda Payment Alert</h2>
			<p>
				Hello {{ $data['name'] }},
			</p>
			<p> 
				<b>YEELDA, has sent your payment of {{ $data['amount'] }}<br />
				See details below.
				<br /><br />

				<div class="panel panel-default">
					<div class="panel-heading"><h4>Details</h4></div>
					<div class="panel-body">
						<br />
						<table>
							<tr>
								<td>Transaction date:</td>
								<td>{{ date("D M Y h:i a") }}</td>
							</tr>

							<tr>
								<td>Amount:</td>
								<td>{{ $data['amount'] }}</td>
							</tr>

							<tr>
								<td>Payment Method:</td>
								<td>Paystack</td>
							</tr>
						</table>
					</div>
				</div>

				<br />
				<a class="btn btn-default" href="{{ env('APP_URL') }}/sigin">Login Now</a>

				<br /><br />
				<b class="text-danger">Note:</b> 
					You have 24hrs pending credit alert. if any delay please contact Yeelda Support at info@yeelda.com
				<br />
			</p>

			
			<hr />
			<div style="height: 50px;"></div>
			<div class="features"><!-- Main Points -->
				<i class="line-font blue icon-shield"></i><!-- Main Point Icon -->
				<p class="small">All right reserved Yeelda &copy; {{ Date("Y") }} </p><!-- Main Text -->
			</div><!-- End Main Points -->
		</div>
	</div>
</div>
</body>
</html>