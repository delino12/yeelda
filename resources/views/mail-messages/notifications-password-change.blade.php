<!DOCTYPE html>
<html>
<head>
	<title>Contact Us</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
		body {
		    font-family: 'Amiko';font-size: 12px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<img class="img-square" src="https://yeelda.com/images/img-set/logo-transparent-new.png" width="20%" height="20%" alt="">
			<h2 class="lead">Password Change Notification</h2>
			<p>
				<br /><br />
				{{ $data['message'] }}
				<br />
				<a class="btn btn-default" href="{{ env('APP_URL') }}/sigin">Click here</a> to Login.
			</p>

			<p class="small">
				<span class="text-danger">Note</span> Report this by sending a mail to info@yeelda.com, if the password change request was not authorized by you.!
			</p>

			<div style="height: 50px;"></div>
			<div class="features"><!-- Main Points -->
				<i class="line-font blue icon-shield"></i><!-- Main Point Icon -->
				<p class="small">All right reserved Yeelda &copy; {{ Date("Y") }} </p><!-- Main Text -->
			</div><!-- End Main Points -->
		</div>
	</div>
</div>
</body>
</html>