<!DOCTYPE html>
<html>
<head>
	<title>Yeelda Account Activations</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
		body {
		    font-family: 'Amiko';font-size: 12px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<img class="img-square" src="https://yeelda.com/images/img-set/logo-transparent-new.png" width="20%" height="20%" alt="">
			<h2 class="lead">Account Activation</h2>

			<p> 
				Hello <b>{{ $data['name'] }}</b>,
				<br /><br />
				Thanks for joining us, yeelda will be glad to connect your services with others. 

				<br /><br />
				Click <a href="{{ env('APP_URL') }}/account/activations/?token={{ $data['token'] }}">Here</a> to activate your account 
				<br />
			</p>
			<div style="height: 50px;"></div>
			<div class="features"><!-- Main Points -->
				<i class="line-font blue icon-shield"></i><!-- Main Point Icon -->
				<p class="small">All right reserved Yeelda &copy; {{ Date("Y") }} </p><!-- Main Text -->
			</div><!-- End Main Points -->
		</div>
	</div>
</div>
</body>
</html>