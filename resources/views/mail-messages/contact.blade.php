<!DOCTYPE html>
<html>
<head>
	<title>Contact Us</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
		body {
		    font-family: 'Amiko';font-size: 12px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<img class="img-square" src="https://yeelda.com/images/img-set/logo-transparent-new.png" width="20%" height="20%" alt="">
			<h2 class="lead">{{ $data['subject'] }}</h2>
			<p> 
				Hello <b>Yeelda</b>,
				<br /><br />
				{{ $data['message'] }}
				<br /><br />
			</p>

			<table class="table">
				<tr>
					<td>Name</td>
					<td>{{ $data['name'] }}</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>{{ $data['email'] }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>
</body>
</html>