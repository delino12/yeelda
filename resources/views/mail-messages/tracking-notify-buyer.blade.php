<!DOCTYPE html>
<html>
<head>
	<title>Yeelda Tracking Notification</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
		body {
		    font-family: 'Amiko';font-size: 12px;
		}

		table, td, th {    
		    border: 1px solid #ddd;
		    text-align: left;
		}

		table {
		    border-collapse: collapse;
		    width: 50%;
		}

		th, td {
		    padding: 15px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<img class="img-square" src="https://yeelda.com/images/img-set/logo-transparent-new.png" width="20%" height="20%" alt="">
			<h2 class="lead">Yeelda Shipment Updates</h2>
			<p>
				Hello {{ $data['buyer'] }},
			</p>
			<p> 
				<b>Incoming Shipment Update! </b> <br />
				See details below.
				<br /><br />

				<div class="panel panel-default">
					<div class="panel-heading"><h4>Details</h4></div>
					<div class="panel-body">
						<br />
						<table>
							<tr>
								<td>Shipment date:</td>
								<td>{{ date("D M Y h:i a") }}</td>
							</tr>

							<tr>
								<td>Transportation Service: </td>
								<td>{{ $data['courier'] }}</td>
							</tr>

							<tr>
								<td>Transportation Method:</td>
								<td>NILL</td>
							</tr>

							<tr>
								<td>Shipment BWL No:</td>
								<td>{{ $data['bwl_no'] }}</td>
							</tr>

							<tr>
								<td>Product:</td>
								<td>{{ $data['product_name'] }}</td>
							</tr>

							<tr>
								<td>Location:</td>
								<td>{{ $data['location'] }}</td>
							</tr>
						</table>
					</div>
				</div>

				<br />
				<a class="btn btn-default" href="{{ env('APP_URL') }}/sigin">Login Now</a>

				<br /><br />
				<b class="text-danger">Note:</b> 
					Yeelda releases payment as soon as buyer 
					confirm delivery of goods. Transaction last for 5 Working Days. 
					Failure to deliver produce, payment will be cancel. 
					Customers Remark and rating on your product will be voted down.
				<br />
			</p>

			<hr />
			<div style="height: 50px;"></div>
			<div class="features"><!-- Main Points -->
				<i class="line-font blue icon-shield"></i><!-- Main Point Icon -->
				<p class="small">All right reserved Yeelda &copy; {{ Date("Y") }} </p><!-- Main Text -->
			</div><!-- End Main Points -->
		</div>
	</div>
</div>
</body>
</html>