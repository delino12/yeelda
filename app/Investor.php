<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use DB;

class Investor extends Model
{
    /*
    |-----------------------------------------
    | fetch buyers by regions
    |-----------------------------------------
    */
    public static function getBuyersByLocation(){

    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, "http://locationsng-api.herokuapp.com/api/v1/states");
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 200);
    	$res = curl_exec($ch);

    	$all_states = json_decode($res, true);
    	// return $all_states = collect($states);
    	if(count($all_states) > 0){
    		$state_box = [];
    		foreach ($all_states as $state) {
    			// body
    			$filtered_state = '%%'.$state['name'].'%%';
    			$db_search = DB::table('investor_basics')->where('state', 'LIKE', $filtered_state)->get();

    			if(count($db_search) > 0){
					$data = [
	    				'state' => $state['name'],
	    				'total' => count($db_search)
	    			];
	    			array_push($state_box, $data);
    			}	
    		}
    	}else{
    		$state_box = [];
    	}

    	
    	return $state_box;
    }
}
