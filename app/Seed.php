<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class Seed extends Model
{
    /*
    |-----------------------------------------
    | LOAD ALL SEEDS
    |-----------------------------------------
    */
    public function getAllSeeds(){
    	// body
    	$seeds = Seed::orderBy('id', 'DESC')->get();
    	if(count($seeds) > 0){
    		$seeds_box = [];
    		foreach ($seeds_box as $el) {
    			# code...
    			$data = [
                    'id'        => $el->id,
                    'email'     => $el->email,
                    'name'      => $el->name,
                    'type'      => "seeds",
                    'qty'       => $el->qty,
                    'amount'    => number_format($el->amount, 2),
                    'note'      => $el->note,
                    'avatar'    => $el->avatar,
                    'date'      => $el->created_at->diffForHumans()
                ];
                array_push($seeds_box, $data);
    		}
    	}else{
    		$seeds_box = [];
    	}

    	return $seeds_box;
    }	

    /*
    |-----------------------------------------
    | TOTAL SEEDS COST
    |-----------------------------------------
    */
    public function seedTotalCost(){
        // body
        $all_seeds = Seed::all();
        if(count($all_seeds) > 0){
            $seed_box = []; 
            foreach ($all_seeds as $el) {
                $data = [
                    'balance' => $el->qty * $el->amount 
                ];

                array_push($seed_box, $data);
            }

            // sum total
            if(count($seed_box) > 0){
                $filtered = collect($seed_box);
                $total    = $filtered->sum('balance');
            }else{
                $total = 0;
            }
        }else{
            $total = 0;
        }

        return $total;
    }

    /*
    |-----------------------------------------
    | DELETE SEED
    |-----------------------------------------
    */
    public function deleteSeed($id){
        // body
        if(Seed::find($id)->delete()){
            $data = [
                'status'    => 'success',
                'message'   => 'Deleted!'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not delete item'
            ];
        }

        // return 
        return $data;
    }
}
