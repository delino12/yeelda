<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class PlatformCrop extends Model
{
    /*
    |-----------------------------------------
    | PLATFORM CROPS
    |-----------------------------------------
    */
    public function fetchAcceptedCrops(){

    	// init default
    	$this->defaultCrops();

    	// body
    	$platform_crops = PlatformCrop::where("is_deleted", false)->get();

    	// return
    	return $platform_crops;
    }

    /*
    |-----------------------------------------
    | ADD PLATFORM GROUPS
    |-----------------------------------------
    */
    public function addPlatformCrops(){
    	// body
    	
    }


    /*
    |-----------------------------------------
    | ADD DEFAULT CROPS
    |-----------------------------------------
    */
    public function defaultCrops(){
    	// body
    	# code...
        $crops = array(
            'Rice', 
            'Wheat', 
            'Soybeans', 
            'Maize', 
            'Cotton', 
            'Cassava', 
            'Palm oil', 
            'Beans', 
            'Peanuts', 
            'Rapeseed', 
            'Rubber', 
            'Yams',
            'Peaches', 
            'Cacao',
            'Sugar', 
            'Watermelons', 
            'Carrots',
            'Coconuts',
            'Almonds',
            'Lemons', 
            'Strawberries',
            'Walnuts',
            'Pepper',
            'Tomatoes',
            'lettuce',
            'Cucumber',
            'Carrot',
            'Cabbage',
            'Cocoyam',
            'Banana',
            'Plantain',
            'Orange',
            'Pawpaw',
            'Mango',
            'Pineapple',
            'Cocoa',
            'Cashew',
            'Ginger',
            'Garlic',
            'Vegetables',
        );

        $platform_crops = PlatformCrop::all();
        if(count($platform_crops) > 0){
        	// skipped
        }else{
        	foreach ($crops as $crop) {
        		# code...

        		$new_crops 				= new PlatformCrop();
        		$new_crops->name 		= $crop;
        		$new_crops->is_deleted 	= false;
        		$new_crops->open_price 	= rand(000, 999) * (20/100);
        		$new_crops->save();
        	}
        }
    }
}
