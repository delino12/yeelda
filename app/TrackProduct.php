<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\Transaction;
use Yeelda\Product;

class TrackProduct extends Model
{
	/*
	|-----------------------------------------
	| ADD PRODUCT
	|-----------------------------------------
	*/
	public function addProduct($request){

		$trans_info = Transaction::where("trans_id", $request->product_ref)->first(); 

		// body
		$add_new 				= new TrackProduct();
		$add_new->tracking_ref 	= "YT".rand(000,999).rand(000,999).rand(000,999);
		$add_new->product_id 	= $trans_info->item_id;
		$add_new->seller 		= $trans_info->seller_email;
		$add_new->buyer 		= $trans_info->buyer_email;
		$add_new->location 		= $request->pickup_address;
		$add_new->destination 	= $request->destination_address;
		$add_new->carrier 		= $request->carrier_name;
		$add_new->assignee 		= $request->assignee;
		$add_new->status 		= $request->status;
		$add_new->pick_date 	= $request->start_date;
		$add_new->drop_date 	= $request->deliver_date;
		if($add_new->save()){
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Receipt created successfully!'
			];
		}else{
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Failed to create receipt!'
			];
		}

		// return 
		return $data;
	}

	/*
	|-----------------------------------------
	| UPDATE PRODUCT
	|-----------------------------------------
	*/
	public function updateProduct($tracking_id){
		// body
		$add_new 				= TrackProduct::find($tracking_id);
		$add_new->tracking_ref 	= "Y-TRANS-".rand(000,999).rand(000,999);
		$add_new->product_id 	= $trans_info->item_id;
		$add_new->seller 		= $trans_info->seller_email;
		$add_new->buyer 		= $trans_info->buyer_email;
		$add_new->location 		= $request->pickup_address;
		$add_new->destination 	= $request->destination_address;
		$add_new->carrier 		= $request->carrier_name;
		$add_new->assignee 		= $request->assignee;
		$add_new->status 		= $request->status;
		$add_new->pick_date 	= $request->start_date;
		$add_new->drop_date 	= $request->deliver_date;
		if($add_new->update()){
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Receipt updated successfully!'
			];
		}else{
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Failed to update receipt!'
			];
		}

		// return 
		return $data;
	}

	/*
	|-----------------------------------------
	| LOAD ALL
	|-----------------------------------------
	*/
	public function loadAll(){
		// body
		return TrackProduct::orderBy("id", "DESC")->get();
	}


	/*
    |-----------------------------------------
    | Get user infomation by email
    |-----------------------------------------
    */
    public function getUserByEmail($email){
        // body
        $check_farmers  = User::where([['email', $email], ['account_type', 'farmer']])->first();
        $check_service  = User::where([['email', $email], ['account_type', 'service']])->first();
        $check_investor = User::where([['email', $email], ['account_type', 'buyer']])->first();

        if($check_farmers !== null){
            
            $name = $check_farmers->name;
            $email = $check_farmers->email;
            $phone = $check_farmers->phone;

        }elseif($check_service !== null){
            
            $name = $check_service->name;
            $email = $check_service->email;
            $phone = $check_service->phone;
        
        }elseif($check_investor !== null){
            
            $name = $check_investor->name;
            $email = $check_investor->email;
            $phone = $check_investor->phone;
        
        }else{
            // when user doesn't exits
            $email  = "";
            $name   = "";
            $phone  = "";
        }

        // return information
        $data = [
            'name'  => $name,
            'email' => $email,
            'phone' => $phone
        ];
        // return
        return $data;
    }

	/*
	|-----------------------------------------
	| LOAD TRACKING SINGLE
	|-----------------------------------------
	*/
	public function loadSingle($tracking_id){
		// body
		$tracking 		= TrackProduct::where("id", $tracking_id)->first();
		$product 		= Product::where("id", $tracking->product_id)->first();
		$transit 		= TransitLocation::where("tracking_id", $tracking_id)->get();

		$seller_info = $this->getUserByEmail($tracking->seller);
        $buyer_info  = $this->getUserByEmail($tracking->buyer);

		$data = [
			'tracking' 		=> $tracking,
			'seller_info' 	=> $seller_info,
			'buyer_info' 	=> $buyer_info,
			'product' 		=> $product,
			'transit' 		=> $transit
		];

		// return
		return $data;
	}

	/*
	|-----------------------------------------
	| DELETE TRACKING 
	|-----------------------------------------
	*/
	public function deleteOne($tracking_id){
		// body
		$del_tracking = TrackProduct::find($tracking_id);
		$del_tracking->delete();
	}
}
