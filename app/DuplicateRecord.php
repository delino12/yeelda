<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class DuplicateRecord extends Model
{
    /*
    |-----------------------------------------
    | SAVE DUPLICATES RECORDS
    |-----------------------------------------
    */
    public function saveDuplicated($payload){
    	// body
    	$add_new 			= new DuplicateRecord();
    	$add_new->payload 	= json_encode($payload);
    	$add_new->save();
    }

    /*
    |-----------------------------------------
    | GET ALL DUPLICATED RECORDS
    |-----------------------------------------
    */
    public function fetchDuplicates(){
    	// body
    	$all_duplicates = DuplicateRecord::orderBy('created_at')->get();
    	$duplicate_box = [];
    	foreach ($all_duplicates as $key => $value) {
    		$data = [
    			'id' 			=> $value->id,
    			'payload'		=> json_decode($value->payload, true),
    			'created_at' 	=> $value->created_at,
    			'updated_at' 	=> $value->updated_at,
    		];

    		array_push($duplicate_box, json_decode($value->payload, true));
    	}

    	// return
    	return $duplicate_box;
    }
}
