<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Company extends Model
{
    /*
    |-----------------------------------------
    | LOAD COMPANY INFORMATION
    |-----------------------------------------
    */
    public function loadInfo(){
    	// body
    	$email 		= Auth::user()->email;
    	$company 	= Company::where('user_email', $email)->first();
    	// check if company exits
    	if($company == null){
    		$data = [
    			'id'		 => 'none',
    			'user_email' => 'none',
    			'name'       => 'none',
    			'location'   => 'none',
    			'state'   	 => 'none',
    			'type'       => 'none',
    			'docs'       => 'none',
    			'staff'      => 'none',
    			'date'       => 'none'
    		];
    	}else{
    		$data = [
    			'id'		 => $company->id,
    			'user_email' => $company->user_email,
    			'name'       => $company->name,
    			'location'   => $company->location,
    			'state'   	 => $company->state,
    			'type'       => $company->type,
    			'docs'       => $company->docs,
    			'staff'      => $company->staff,
    			'date'       => $company->created_at->diffForHumans()
    		];
    	}

    	// return information
    	return $data;
    }

    /*
    |-----------------------------------------
    | ADD COMPANY INFORMATION
    |-----------------------------------------
    */
    public function addCompany($request){
    	// body
    	$user_email = $request->email;
    	$name 		= $request->name;
    	$location 	= $request->address;
    	$state 		= $request->state;
    	$type 		= $request->type;
    	// $docs 		= $request->docs;
    	$staff 		= $request->employee;

    	$already_exits = Company::where([['name', $name], ['user_email', $user_email]])->first();
    	if($already_exits == null){
	    	// save company 
	    	$add_company 			 = new Company();
	    	$add_company->user_email = $user_email;
	    	$add_company->name 		 = $name;
	    	$add_company->location 	 = $location;
	    	$add_company->state 	 = $state;
	    	$add_company->type 	     = $type;
	    	$add_company->docs 		 = 'none';
	    	$add_company->staff 	 = $staff;
	    	$add_company->save();

	    	// response message
	    	$msg = $name.' added successfully !';
	    	$data = [
	    		'status'  => 'success',
	    		'message' => $msg
	    	];
    	}else{
    		// response message
	    	$msg = $name.' already added !';
	    	$data = [
	    		'status'  => 'error',
	    		'message' => $msg
	    	];
    	}

    	// return 
    	return $data;
    }
}
