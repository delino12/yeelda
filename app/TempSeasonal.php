<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;
use Yeelda\ImageHouse;

use Carbon\Carbon;
use Image;
use Storage;
use Cloudinary;
use DB;

class TempSeasonal extends Model
{
    public $image_path = "";
    

    /*
    |-----------------------------------------
    | ADD TO PRODUCE
    |-----------------------------------------
    */
    public function addNewProduce($payload){
    	// body
        $base64_string  = $payload->produce_avatar;
        $user_id        = $payload->user_id;
        $user_info      = User::where("id", $user_id)->first();

        if($user_info !== null){
            $user_names = $user_info->firstname .' '.$user_info->lastname;
            // first download the image base
            if($this->downloadProduceBase64Images($base64_string, $user_id, $user_names)){

                // filterd and resize image
                $this->resize_image($this->image_path, $width = null, $height = 400);
                $new_image = public_path($this->image_path);

                \Cloudinary::config(array(
                    "cloud_name"    => "delino12",
                    "api_key"       => "632817215533885",
                    "api_secret"    => "nZaO84cvr14RidW6sQ8gd6clzic"
                ));

                $upload_response    = \Cloudinary\Uploader::upload($new_image, $options);
                $payload->produce_avatar  = $upload_response['url']; // cloud response

                $new_produe                         = new TempSeasonal();
                $new_produe->user_id                = $payload->user_id;
                $new_produe->product_name           = $payload->produce_name;
                $new_produe->product_size_type      = "Not available";
                $new_produe->product_size_no        = "Not available";
                $new_produe->product_price          = "Not available";
                $new_produe->product_location       = $payload->produce_location;
                $new_produe->product_state          = $payload->produce_state;
                $new_produe->product_image          = $payload->produce_avatar;
                $new_produe->product_note           = "";
                $new_produe->product_planning_date  = $payload->product_planning_date;
                $new_produe->product_delivery_date  = $payload->product_delivery_date;
                $new_produe->product_delivery_type  = $payload->product_delivery_type;
                if($new_produe->save()){
                    $data = [
                        'status'    => 'success',
                        'message'   => 'Synchronization successful!'
                    ];
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Synchronization failed!'
                    ];
                }
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Failed to process produces image'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'User account not found!'
            ];
        }

        return $data;
    }

    /*
    |-----------------------------------------
    | FILTERED IMAGE
    |-----------------------------------------
    */
    public function filteredBaseImage($produce_id){
    	// body
    	
    }

    /*
    |-----------------------------------------
    | DONWLOAD IMAGE TO JPG
    |-----------------------------------------
    */
    public function downloadProduceBase64Images($base64_string, $user_id, $user_names){
        $file_path = public_path('tmp/');
        $file_data = str_replace("data:image/jpeg;base64,", "", $base64_string);
        $file_data = str_replace(" ", "+", $file_data); // filtered strings
        $file_name = $file_path.$user_names.'.jpeg'; //generating unique file name; 
        $image_data = base64_decode($file_data);
        if(file_put_contents($file_name, $image_data)){
            $new_image_name = "tmp/".$user_names.'.jpeg';

            // grab it
            $this->image_path = $new_image_name;
            
            return true;
        }else{
            return false;
        }
    }

    /*
    |-----------------------------------------
    | RESIZE IMAGE 
    |-----------------------------------------
    */
    public function resize_image($image_path, $image_width, $image_height) {
        $img = Image::make(public_path($image_path));
        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img->resize($image_width, $image_height, function ($constraint) {
            $constraint->aspectRatio();
        });
        // replace
        $img->save($image_path);
    }
}
