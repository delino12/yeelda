<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class AdminMessage extends Model
{
    /*
    |--------------------------------
    | LOAD ALL ADMIN MESSAGES
    |--------------------------------
    |
    */
    public function loadAll(){
    	$load_messages = AdminMessage::orderBy('id', 'desc')->get();
    	if(count($load_messages) > 0){
    		$msg_box = [];
    		foreach ($load_messages as $message) {
    			# code...
    			$data = [
    				'id' 		=> $message->id,
    				'reciever' 	=> $message->to,
    				'subject' 	=> $message->subject,
    				'body' 		=> $message->body,
    				'status' 	=> $message->status,
    				'date' 		=> $message->created_at->diffForHumans()
    			];

    			array_push($msg_box, $data);
    		}
    	}else{
    		$msg_box = [];
    	}

    	return $msg_box;
    }

    /*
    |--------------------------------
    | LOAD ALL ADMIN MESSAGES
    |--------------------------------
    |
    */
    public function loadOne($id){
        $message = AdminMessage::where('id', $id)->first();
        if($message !== null){
            # code...
            $data = [
                'id'        => $message->id,
                'reciever'  => $message->to,
                'subject'   => $message->subject,
                'body'      => $message->body,
                'status'    => $message->status,
                'date'      => $message->created_at->diffForHumans()
            ];
        }else{
            $data = [
                'status'  => 'error',
                'message' => 'Yeelda could not fetch this message!'
            ];;
        }

        return $data;
    }
}
