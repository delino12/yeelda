<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class Fertilizer extends Model
{
    /*
    |-----------------------------------------
    | TOTAL FERTILIZER COST
    |-----------------------------------------
    */
    public function fertilizerTotalCost(){
    	// body
    	$all_fertilizers = Fertilizer::all();
    	if(count($all_fertilizers) > 0){
    		$fert_box = [];	
	    	foreach ($all_fertilizers as $el) {
	    		$data = [
	    			'balance' => $el->qty * $el->amount 
	    		];

	    		array_push($fert_box, $data);
	    	}

	    	// sum total
	    	if(count($fert_box) > 0){
	    		$filtered = collect($fert_box);
	    		$total    = $filtered->sum('balance');
	    	}else{
	    		$total = 0;
	    	}
	    }else{
	    	$total = 0;
	    }

	    return $total;
    }

    /*
    |-----------------------------------------
    | DELETE SEED
    |-----------------------------------------
    */
    public function deleteFert($id){
        // body
        if(Fertilizer::find($id)->delete()){
            $data = [
                'status'    => 'success',
                'message'   => 'Deleted!'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not delete item'
            ];
        }

        // return 
        return $data;
    }
}
