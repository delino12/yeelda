<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\SMS;

class SmsQueue extends Model
{
    /*
    |-----------------------------------------
    | ADD SMS TO QUEUE
    |-----------------------------------------
    */
    public function addToQueue($mobile, $message){
    	// body
    	$new_sms 			= new SmsQueue();
    	$new_sms->status 	= "waiting";
    	$new_sms->mobile 	= $mobile;
    	$new_sms->message 	= $message;
    	$new_sms->save();
    }

    /*
    |-----------------------------------------
    | CLEAR WAITING SMS QUEUE
    |-----------------------------------------
    */
    public function processQueueMessage(){
    	// body
    	$all_waiting = SmsQueue::where("status", "waiting")->get();
    	if(count($all_waiting) > 0){
    		foreach ($all_waiting as $wl) {
    			$send_sms = new SMS();
    			if($send_sms->sendQueueMessages($wl->mobile, $wl->message, "YEELDA")){
    				// update status
    				$update_status 			= SmsQueue::find($wl->id);
    				$update_status->status  = 'sent';
    				$update_status->update();
    			}else{
    				// update status
    				$update_status 			= SmsQueue::find($wl->id);
    				$update_status->status  = 'failed';
    				$update_status->update();
    			}
    		}
    	}
    }
}
