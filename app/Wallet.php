<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\User;

class Wallet extends Model
{
    /*
    |-----------------------------------------
    | GET ALL WALLETS BALANCES
    |-----------------------------------------
    */
    public function getAllWallet(){
    	// body
    	$wallet = Wallet::orderBy("id", "DESC")->get();

    	// return
    	return $wallet;
    }

    /*
    |-----------------------------------------
    | GET USER WALLET BALANCES
    |-----------------------------------------
    */
    public function getUserWallet($user_id){
    	// body
    	$this->initializeWallet($user_id);
    	$wallet = Wallet::where("user_id", $user_id)->first();

        $data = [
            'balance' => number_format($wallet->balance, 2),
            'status' => $wallet->status
        ];

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | INITIALIZE WALLETS
    |-----------------------------------------
    */
    public function initializeWallet($user_id){
    	// body
    	$wallet = Wallet::where("user_id", $user_id)->first();
    	if($wallet == null){
    		// create default wallet
    		$new_wallet 			= new Wallet();
    		$new_wallet->user_id 	= $user_id;
    		$new_wallet->balance 	= 0;
    		$new_wallet->status 	= "status";
    		$new_wallet->save();
    	}
    }

    /*
    |-----------------------------------------
    | CREDIT WALLET
    |-----------------------------------------
    */
    public function creditWallet($user_id, $amount){
        // body
        $wallet = Wallet::where("user_id", $user_id)->first();
        if($wallet !== null){
            $update_wallet          = Wallet::find($wallet->id);
            $update_wallet->balance = $update_wallet->balance + $amount;
            $update_wallet->update();

            $data = [
                'status'    => 'success',
                'message'   => 'Wallet has been credited with &#8358;'.number_format($amount)
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid wallet, try again!'
            ];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | DEBIT WALLET
    |-----------------------------------------
    */
    public function debitWallet($user_id, $amount){
        // body
        $wallet = Wallet::where($user_id)->first();
        if($wallet !== null){
            if($wallet->balance < $amount){
                $data = [
                    'status'    => 'error',
                    'message'   => 'Insufficient wallet balance, top up wallet balance and try again'
                ];
            }else{
                $update_wallet          = Wallet::find($wallet->id);
                $update_wallet->balance = $update_wallet->balance - $amount;
                $update_wallet->update();

                $data = [
                    'status'    => 'success',
                    'message'   => 'Transaction successful!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid wallet, try again!'
            ];
        }

        // return
        return $data;
    }
}
