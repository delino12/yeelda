<?php

namespace Yeelda;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use Yeelda\Activation;
use Yeelda\FarmerAccount;
use Yeelda\FarmerBasic;
use Yeelda\InvestorAccount;
use Yeelda\InvestorBasic;
use Yeelda\Mail\AccountActivation;
use Yeelda\Product;
use Yeelda\ServiceAccount;
use Yeelda\ServiceBasic;
use Yeelda\User;
use Yeelda\ViewAccess;

class User extends Model
{
    protected $hidden = ['password'];
    /*
    |-----------------------------------------
    | RELATIONSHIP WITH BASIC INFORMATION
    |-----------------------------------------
     */
    public function basic()
    {
        // body
        return $this->hasOne(FarmerBasic::class);
    }

    /*
    |-----------------------------------------
    | ADD NEW USER
    |-----------------------------------------
     */
    public function addUserFromMerge($user, $account_type)
    {
        $new_user               = new User();
        $new_user->name         = $user->name;
        $new_user->email        = $user->email;
        $new_user->account_type = $account_type;
        $new_user->phone        = $user->phone;
        $new_user->password     = $user->password;
        $new_user->status       = "active";
        if ($new_user->save()) {
            // update farmer basic information
            if ($account_type == "farmer") {
                $farmer_info = FarmerBasic::where("user_id", $user->id)->first();
                if ($farmer_info !== null) {

                    $update_basic = FarmerBasic::find($farmer_info->id);
                    if ($update_basic !== null) {
                        $update_basic->user_id = $new_user->id;
                        $update_basic->update();
                    }

                    $update_basic = FarmerAccount::find($farmer_info->id);
                    if ($update_basic !== null) {
                        $update_basic->user_id = $new_user->id;
                        $update_basic->update();
                    }
                }
            }

            if ($account_type == "service") {
                $service_info = ServiceBasic::where("user_id", $user->id)->first();
                if ($service_info !== null) {
                    $update_basic = ServiceBasic::find($service_info->id);
                    if ($update_basic !== null) {
                        $update_basic->user_id = $new_user->id;
                        $update_basic->update();
                    }

                    $update_basic = ServiceAccount::find($service_info->id);
                    if ($update_basic !== null) {
                        $update_basic->user_id = $new_user->id;
                        $update_basic->update();
                    }
                }
            }

            if ($account_type == "buyer") {
                $investor_info = InvestorBasic::where("user_id", $user->id)->first();
                if ($investor_info !== null) {
                    $update_basic = InvestorBasic::find($investor_info->id);
                    if ($update_basic !== null) {
                        $update_basic->user_id = $new_user->id;
                        $update_basic->update();
                    }

                    $update_basic = InvestorAccount::find($investor_info->id);
                    if ($update_basic !== null) {
                        $update_basic->user_id = $new_user->id;
                        $update_basic->update();
                    }
                }
            }
        }
    }

    /*
    |-----------------------------------------
    | REGISTER NEW USER
    |-----------------------------------------
     */
    public function addNewUser($name, $email, $password, $phone, $account_type)
    {
        $user_exist = User::where("email", $email)->first();
        if ($user_exist == null) {
            // body
            $new_user               = new User();
            $new_user->name         = $name;
            $new_user->email        = $email;
            $new_user->account_type = $account_type;
            $new_user->phone        = $phone;
            $new_user->password     = bcrypt($password);
            $new_user->status       = "active";
            $new_user->save();

            // default image
            $new_name = 'profile.png';

            // update other related records
            $user_info = User::where('email', $email)->first();

            // update bio information
            if ($user_info->account_type == "farmer") {
                // save basic information for farmer
                $farmer_basics          = new FarmerBasic();
                $farmer_basics->user_id = $user_info->id;
                $farmer_basics->name    = $name;
                $farmer_basics->avatar  = $new_name;
                $farmer_basics->save();

                // save account information
                $farmer_accounts          = new FarmerAccount();
                $farmer_accounts->user_id = $user_info->id;
                $farmer_accounts->save();

            } elseif ($user_info->account_type == "service") {

                // create account information entry
                $service_account          = new ServiceAccount();
                $service_account->user_id = $user_info->id;
                $service_account->save();

                // create basic information entry
                $service_basic          = new ServiceBasic();
                $service_basic->user_id = $user_info->id;
                $service_basic->avatar  = $new_name;
                $service_basic->name    = $name;
                $service_basic->save();
            }

            // Generate account activation token via mail
            $this->AccountActivation($email, $name);

            return true;
        } else {
            return false;
        }
    }

    /*|
    |-----------------------------------------------------------------------
    | SEND ACTIVATION MAIL
    |-----------------------------------------------------------------------
    |
     */
    public function AccountActivation($email, $name)
    {
        // set token
        $token  = bcrypt($email);
        $status = 'inactive';

        // account activations status
        $activation         = new Activation();
        $activation->email  = $email;
        $activation->status = $status;
        $activation->token  = $token;
        $activation->save();

        // mail contents
        $data = array(
            'name'  => $name,
            'token' => $token,
        );

        // send signed up client activation mail
        \Mail::to($email)->send(new AccountActivation($data));

        return true;
    }

    /*
    |-----------------------------------------
    | UPDATE AVATAR
    |-----------------------------------------
     */
    public function updateAvatar($user_id, $avatar)
    {
        // body
        $user = User::where("id", $user_id)->first();
        if ($user !== null) {
            // check user type
            if ($user->account_type == "farmer") {
                // update farmer profile
                $basic_info = FarmerBasic::where("user_id", $user_id)->first();

                if ($basic_info !== null) {
                    $update_avatar         = FarmerBasic::find($basic_info->id);
                    $update_avatar->avatar = $avatar;
                    if ($update_avatar->update()) {
                        $data = [
                            'status'  => 'success',
                            'message' => 'Update was successful!',
                        ];
                    } else {
                        $data = [
                            'status'  => 'error',
                            'message' => 'Update was not successful!',
                        ];
                    }
                } else {
                    $data = [
                        'status'  => 'error',
                        'message' => 'User Basic information not found!',
                    ];
                }
            }
        } else {
            $data = [
                'status'  => 'error',
                'message' => 'User not found!',
            ];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | GET USERS BY ACCOUNT TYPE
    |-----------------------------------------
     */
    public static function getUserByAccount($account_type)
    {
        // body
        $all_users = User::where('account_type', $account_type)->orderBy('id', 'DESC')->limit('30')->get();
        $users_box = [];
        foreach ($all_users as $user) {
            if ($account_type == 'farmer') {
                $bio_data = DB::table('farmer_basics')->where([
                    ["user_id", $user->id],
                    ["avatar", "LIKE", "%http%"],
                ])->first();

                if ($bio_data !== null) {
                    $crops = str_replace(",", ", ", $bio_data->farm_crop);

                    $data = [
                        'id'      => $user->id,
                        'name'    => $user->name,
                        'avatar'  => $bio_data->avatar,
                        'phone'   => $user->phone,
                        'account' => ucfirst($user->account_type),
                        'crops'   => ucfirst($crops),
                        'email'   => $user->email,
                    ];

                    array_push($users_box, $data);
                }
            }

            if ($account_type == 'service') {
                $bio_data = DB::table('service_basics')->where([
                    ["user_id", $user->id],
                    ["avatar", "LIKE", "%http%"],
                ])->first();

                if ($bio_data !== null) {
                    $data = [
                        'id'      => $user->id,
                        'name'    => $user->name,
                        'avatar'  => $bio_data->avatar,
                        'phone'   => $user->phone,
                        'account' => ucfirst($user->account_type),
                        'email'   => $user->email,
                    ];

                    array_push($users_box, $data);
                }
            }

            if ($account_type == 'buyer') {
                $bio_data = DB::table('investor_basics')->where([
                    ["user_id", $user->id],
                    ["avatar", "LIKE", "%http%"],
                ])->first();

                if ($bio_data !== null) {

                    $data = [
                        'id'      => $user->id,
                        'name'    => $user->name,
                        'avatar'  => $bio_data->avatar,
                        'phone'   => $user->phone,
                        'account' => ucfirst($user->account_type),
                        'email'   => $user->email,
                    ];

                    array_push($users_box, $data);
                }
            }
        }

        // return
        return $users_box;
    }

    /*
    |-----------------------------------------
    | GET USERS WITH MEMBER ACCESS
    |-----------------------------------------
     */
    public static function getUserByAccountWithMemberAccess($account_type, $limit, $offset)
    {
        // body
        $user_view_access = ViewAccess::where('user_id', Auth::user()->id)->first();
        if ($user_view_access !== null) {
            $all_users = User::where([['account_type', $account_type], ['group_id', $user_view_access->group_id]])->orderBy('id', 'DESC')->offset($offset)->limit($limit)->get();
            $users_box = [];
            foreach ($all_users as $user) {
                if ($account_type == 'farmer') {
                    $bio_data = DB::table('farmer_basics')->where([
                        ["user_id", $user->id],
                        ["avatar", "LIKE", "%http%"],
                    ])->first();

                    if ($bio_data !== null) {
                        $crops = str_replace(",", ", ", $bio_data->farm_crop);

                        $data = [
                            'id'      => $user->id,
                            'name'    => $user->name,
                            'avatar'  => $bio_data->avatar,
                            'phone'   => $user->phone,
                            'account' => ucfirst($user->account_type),
                            'crops'   => ucfirst($crops),
                            'email'   => $user->email,
                        ];

                        array_push($users_box, $data);
                    }
                }

                if ($account_type == 'service') {
                    $bio_data = DB::table('service_basics')->where([
                        ["user_id", $user->id],
                        ["avatar", "LIKE", "%http%"],
                    ])->first();

                    if ($bio_data !== null) {
                        $data = [
                            'id'      => $user->id,
                            'name'    => $user->name,
                            'avatar'  => $bio_data->avatar,
                            'phone'   => $user->phone,
                            'account' => ucfirst($user->account_type),
                            'email'   => $user->email,
                        ];

                        array_push($users_box, $data);
                    }
                }

                if ($account_type == 'buyer') {
                    $bio_data = DB::table('investor_basics')->where([
                        ["user_id", $user->id],
                        ["avatar", "LIKE", "%http%"],
                    ])->first();

                    if ($bio_data !== null) {

                        $data = [
                            'id'      => $user->id,
                            'name'    => $user->name,
                            'avatar'  => $bio_data->avatar,
                            'phone'   => $user->phone,
                            'account' => ucfirst($user->account_type),
                            'email'   => $user->email,
                        ];

                        array_push($users_box, $data);
                    }
                }
            }
        } else {
            $users_box = [];
        }

        // return
        return $users_box;
    }


    /*
    |-----------------------------------------
    | GET USERS REPORT CAPTURED
    |-----------------------------------------
    */
    public static function getReportByAccountWithMemberAccess($account_type){
        // body
        return [];
    }

    /*
    |-----------------------------------------
    | GET STATISTICS FOR USERS ON PLATFORM
    |-----------------------------------------
     */
    public static function getStatistics()
    {
        // body
        $get_total_farmers  = User::where("account_type", 'farmer')->count();
        $get_total_service  = User::where("account_type", 'service')->count();
        $get_total_states   = 3;
        $get_total_produces = Product::count();

        $data = [
            'farmers'  => $get_total_farmers,
            'services' => $get_total_service,
            'states'   => $get_total_states,
            'produces' => $get_total_produces,
        ];

        // return response.
        return $data;
    }


    /*
    |-----------------------------------------
    | FETCH ALL PREMIUM USERS
    |-----------------------------------------
    */
    public function getAllPremiumUser($payload){
        // body
        $limit  = $payload->limit;
        $offset = $payload->offset;

        // body
        $user_view_access = ViewAccess::where('user_id', Auth::user()->id)->first();
        if ($user_view_access !== null) {
            $total = User::where([['account_type', 'farmer'], ['group_id', $user_view_access->group_id]])->count();
            $asset = $this->getUserByAccountWithMemberAccess('farmer', $limit, $offset);

            $data = [
                'data'  => $asset,
                'total' => $total,
            ];
        }else{
            $data = [
                'data'  => [],
                'total' => 0,
            ];
        }

        // body
        return $data;
    }

    /*
    |-----------------------------------------
    | GET PLATFORM USERS
    |-----------------------------------------
    */
    public function getAllPlatformUsers(){
        // body
        $users = User::orderBy("id", "desc")->where('account_type', 'farmer')->get();
        $users_box = [];
        foreach ($users as $key => $value) {
            array_push($users_box, [
                "id"        => $value->id,
                "name"      => $value->name,
                "email"     => $value->email,
                "phone"     => $value->phone
            ]);
        }

        $data = [
            'status'  => 'success',
            'message' => 'Users retrieved!',
            'data'    => $users_box
        ];

        return $data;
    }
}
