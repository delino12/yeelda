<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Message extends Model
{
	/*
	|-----------------------------------------
	| GET USER MESSAGES
	|-----------------------------------------
	*/
	public function loadMessages(){
		// body
		$logged_email  = Auth::user()->email;
    	$messages      = Message::where('to', $logged_email)->get();

        if(count($messages) > 0){
            $msg_box = [];
            foreach ($messages as $msg) {
                # scaaned message
                $data = array(
                    'id'      => $msg->id,
                    'from'    => $msg->from,
                    'to'      => $msg->to,
                    'subject' => $msg->subject,
                    'body'    => $msg->body,
                    'docs'    => $msg->docs,
                    'status'  => $msg->status,
                    'date'    => $msg->created_at->diffForHumans()
                );

                array_push($msg_box, $data);
            }
        }else{
            $msg_box = [];
        }

        return $msg_box;
	}

	/*
	|-----------------------------------------
	| GET USER MESSAGES II
	|-----------------------------------------
	*/
	public function getUserMessages(){
		// body
		$email 		= Auth::user()->email;
    	$messages 	= Message::where('to', $email)->orderBy('id', 'desc')->take('10')->get();
    	
		if(count($messages) > 0){
			$msg_box = [];
    		foreach ($messages as $message) {
    			$data = array(
					'id'       => $message->id,
					'from'     => $message->from,
					'to'       => $message->to,
					'subject'  => str_replace('RE: RE:', 'RE:', $message->subject),
					'body'     => $message->body,
					'date'     => $message->created_at->diffForHumans(),
				);
				array_push($msg_box, $data);
    		}
    	}else{
    		$msg_box = [];
    	}

    	// return
    	return $msg_box;
	}
}
