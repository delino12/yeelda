<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class News extends Model
{
	protected $key;

    // custom API keys
    protected $api_key;
    protected $api_secret;

	public function __construct()
	{
		# code...
		$this->key = '7d8d32b47d9e4631824cfa3cb64702a7';

        //put your API Key and Secret in these two variables.
        $this->api_key     = 'WSnRJVCeAH7ZGGpCTeBxpIxBdHuAjY0Q';
        $this->api_secret  = 'kjr21yqdCX2hUvg8';
	}

    // get news finance
    public function loadNews()
    {

    	$base_url = "https://newsapi.org/v2/top-headlines?sources=financial-post&apiKey=".$this->key;
    	$fetch_news = new Client();
    	$news_res 	= $fetch_news->request('GET', $base_url);

    	// filter response
        $res_code = $news_res->getStatusCode();
        $res_body = $news_res->getBody()->getContents();

        // return body contents
        return $res_body;
    }

    // get news 
    public function loadNewsBloomberg()
    {
    	$base_url = "https://newsapi.org/v2/top-headlines?sources=bloomberg&apiKey=".$this->key;
    	$fetch_news = new Client();
    	$news_res 	= $fetch_news->request('GET', $base_url);

    	// filter response
        $res_code = $news_res->getStatusCode();
        $res_body = $news_res->getBody()->getContents();

        // return body contents
        return $res_body;
    }
 
    //When called this function will request an Access Token and then return just
    //the token value. 
    public function GetOAuthToken(){
        $ch = curl_init("http://api.awhere.com/oauth/token");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Basic ".base64_encode($this->api_key.":".$this->api_secret)
        ));
     
        $result = curl_exec($ch);
        $result = json_decode($result);
        return response()->json($result);


        // // refactoring curl
        // $base_url = "http://api.awhere.com/oauth/token";
        // $http_body = array("grant_type=client_credentials");
        // $http_headers = array(
        //     "Content-Type: application/x-www-form-urlencoded",
        //     "Authorization: Basic ".base64_encode($this->api_key.":".$this->api_secret)
        // );

        // // fetch token 
        // $fetch_token = new Client();
        // $token_results = $fetch_token->request('POST', $base_url, [
        //     'headers' => $http_headers,
        //     'body' => $http_body
        // ]);

        // $results = $token_results->getBody()->getContents();
        // $result = json_decode($result);
        // return $result->access_token;
    }

    // used custom Agricultural API
    public function loadCustomNews()
    {
        # code...
        $base_url = "https://api.awhere.com/v2/fields";
        $access_token = $this->GetOAuthToken();
        $http_headers = array("Authorization: Bearer ".$access_token); 

        // fetch news using google clients
        $fetch_news = new Client();
        $news_res   = $fetch_news->request('GET', $base_url, [
            'headers' => $http_headers
        ]);

        // filter response
        $res_code = $news_res->getStatusCode();
        $res_body = $news_res->getBody()->getContents();

        // return body contents
        return $res_body;
    }
}
