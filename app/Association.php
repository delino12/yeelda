<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\User;


class Association extends Model
{
    /*
    |-----------------------------------------
    | LOAD ALL MEMBERS
    |-----------------------------------------
    */
    public static function loadAllMembers($name){
    	// body
    	$name = "ASSOC-".$name;
    	$all_assoc = Association::where("room", $name)->get();
    	if(count($all_assoc) > 0){
    		$assoc_box = [];
    		foreach ($all_assoc as $assoc) {
    			$user_info = User::where("email", $assoc->members)->first();
    			if($user_info !== null){
    				$data = [
	    				'id' => $assoc->id,
	    				'name' => $user_info->name,
	    				'email' => $user_info->email,
	    				'account_type' => $user_info->account_type
	    			];
	    			array_push($assoc_box, $data);
    			}
    		}
    	}else{
    		$assoc_box = [];
    	}

    	// return
    	return $assoc_box;
    }

    /*
    |-----------------------------------------
    | DELETE GROUP
    |-----------------------------------------
    */
    public static function deleteGroup($group_id){
        // body
        $chat_group = Association::find($group_id);
        if($chat_group->delete()){
            $data = [
                'status'    => 'success',
                'message'   => 'Deleted!'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Failed to delete chat group'
            ];
        }

        // return 
        return $data;
    }
}
