<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\User;
use Auth;
use DB;

class FarmerBasic extends Model
{
	/*
	|-----------------------------------------
	| RELATIONSHIP WITH USER INFORMATION
	|-----------------------------------------
	*/
	public function user(){
		// body
		return $this->belongsTo(User::class);
	}

    /*
    |-----------------------------------------
    | GET BASIC INFORMATION
    |-----------------------------------------
    */
    public function getBasicInfo(){
    	// body
        $id   = Auth::user()->id;
        $info = FarmerBasic::where('user_id', $id)->first();
        if($info !== null){
        	$user = User::where('id', $id)->first();

	        // check info
	        if($info->gender == null){
	            $info->gender = 'none';
	        }
	        if($info->office == null){
	            $info->office = 'none';
	        }
	        if($info->address == null){
	            $info->address = 'none';
	        }
	        if($info->zipcode == null){
	            $info->zipcode = 'none';
	        }
	        if($info->state == null){
	            $info->state = 'none';
	        }

	        # code...
	        $data = [
	            'id'      => $info->id,
	            'user_id' => $info->user_id,
	            'name'    => $info->name,
	            'email'   => $user->email,
	            'gender'  => $info->gender,
	            'address' => $info->address,
	            'state'   => $info->state,
	            'zipcode' => $info->zipcode,
	            'avatar'  => $info->avatar,
	            'office'  => $info->office,
	            'mobile'  => $user->phone,
	            'date'    => $info->created_at
	        ];
        }else{
        	$data = [];
        }

        // return response
        return $data;
    }
}
