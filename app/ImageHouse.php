<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class ImageHouse extends Model
{

	/*
	|-----------------------------------------
	| INIT CLOUD CONFIG
	|-----------------------------------------
	*/
	// public function __construct(){
	// 	// body
	// 	\Cloudinary::config(array(
	// 	    "cloud_name" 	=> "delino12",
	// 	    "api_key" 		=> "632817215533885",
	// 	    "api_secret" 	=> "nZaO84cvr14RidW6sQ8gd6clzic"
	// 	));
	// }	
    /*
    |-----------------------------------------
    | CHECK ALREADY EXISTED
    |-----------------------------------------
    */
    public static function checkAlreadyExist($temp_user_id){
    	// body
    	$already_cleaned = ImageHouse::where('image_user_id', $temp_user_id)->first();
    	if($already_cleaned !== null){
    		return false;
    	}else{
    		return true;
    	}
    }

    /*
    |-----------------------------------------
    | ADD NEW TEMP USERS
    |-----------------------------------------
    */
    public function addTempImage($temp_user_id, $temp_user_image){
    	// body
    	$new_temp_image 				= new ImageHouse();
    	$new_temp_image->image_user_id 	= $temp_user_id;
    	$new_temp_image->image_ref 		= "ODCA";
    	$new_temp_image->image_url 		= $temp_user_image;
    	$new_temp_image->save();
    }	
}
