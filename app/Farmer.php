<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use DB;

class Farmer extends Model
{
    /*
    |-----------------------------------------
    | fetch farmers by regions
    |-----------------------------------------
    */
    public static function getFarmersByLocation(){

    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, "http://locationsng-api.herokuapp.com/api/v1/states");
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 200);
    	$res = curl_exec($ch);

    	$all_states = json_decode($res, true);
    	// return $all_states = collect($states);
    	if(count($all_states) > 0){
    		$state_box = [];
    		foreach ($all_states as $state) {
    			// body
    			$filtered_state = '%%'.$state['name'].'%%';
    			$db_search = DB::table('farmer_basics')->where('state', 'LIKE', $filtered_state)->get();

    			if(count($db_search) > 0){
					$data = [
	    				'state' => $state['name'],
	    				'total' => count($db_search)
	    			];
	    			array_push($state_box, $data);
    			}	
    		}
    	}else{
    		$state_box = [];
    	}

    	
    	return $state_box;
    }

    /*
    |-----------------------------------------
    | GET MOST PLANTED CROP
    |-----------------------------------------
    */
    public function mostPlantedProduce(){

        # code...
        $crops = array(
            'Rice', 
            'Wheat', 
            'Soybeans', 
            'Maize', 
            'Cotton', 
            'Cassava', 
            'Palm oil', 
            'Beans', 
            'Peanuts', 
            'Rapeseed', 
            'Rubber', 
            'Yams',
            'Peaches', 
            'Cacao',
            'Sugar', 
            'Watermelons', 
            'Carrots',
            'Coconuts',
            'Almonds',
            'Lemons', 
            'Strawberries',
            'Walnuts',
            'Pepper',
            'Tomatoes',
            'lettuce',
            'Cucumber',
            'Carrot',
            'Cabbage',
            'Cocoyam',
            'Banana',
            'Plantain',
            'Orange',
            'Pawpaw',
            'Mango',
            'Pineapple',
            'Cocoa',
            'Cashew',
            'Ginger',
            'Garlic',
            'Vegetables',
        );

        // body
        $farmers = DB::table('farmer_basics')->get();
        if(count($crops) > 0){
            $crop_box = [];
            foreach ($crops as $crop) {
                $search = '%'.$crop.'%';
                $scanned = DB::table('farmer_basics')->where("farm_crop", "LIKE", $search)->count();
                if($scanned > 0){
                    $data = [
                        'result'    => $scanned,
                        'crop'      => $crop,
                    ];

                    // check if object already pushed
                    if(in_array($data, $crop_box)){
                        // let skip this..
                    }else{
                        array_push($crop_box, $data);
                    }
                }
            }
        }else{
            $crop_box = [];
        }

        // return 
        return $crop_box;
    }

    /*
    |-----------------------------------------
    | GET MOST PLANTED CROP
    |-----------------------------------------
    */
    public function mostPlantedProduceQuery($states, $lgas){
        # code...
        $crops = array(
            'Rice', 
            'Wheat', 
            'Soybeans', 
            'Maize', 
            'Cotton', 
            'Cassava', 
            'Palm oil', 
            'Beans', 
            'Peanuts', 
            'Rapeseed', 
            'Rubber', 
            'Yams',
            'Peaches', 
            'Cacao',
            'Sugar', 
            'Watermelons', 
            'Carrots',
            'Coconuts',
            'Almonds',
            'Lemons', 
            'Strawberries',
            'Walnuts',
            'Pepper',
            'Tomatoes',
            'lettuce',
            'Cucumber',
            'Carrot',
            'Cabbage',
            'Cocoyam',
            'Banana',
            'Plantain',
            'Orange',
            'Pawpaw',
            'Mango',
            'Pineapple',
            'Cocoa',
            'Cashew',
            'Ginger',
            'Garlic',
            'Vegetables',
        );

        // body
        $farmers = DB::table('farmer_basics')->where([['state', $states], ['lga', $lgas]])->get();
        if(count($crops) > 0){
            $crop_box = [];
            foreach ($crops as $crop) {
                $search = '%'.$crop.'%';
                $scanned = DB::table('farmer_basics')->where("farm_crop", "LIKE", $search)->count();
                if($scanned > 0){
                    $data = [
                        'result'    => $scanned,
                        'crop'      => $crop,
                    ];

                    // check if object already pushed
                    if(in_array($data, $crop_box)){
                        // let skip this..
                    }else{
                        array_push($crop_box, $data);
                    }
                }
            }
        }else{
            $crop_box = [];
        }

        // return 
        return $crop_box;
    }

    /*
    |-----------------------------------------
    | TOTAL MALE
    |-----------------------------------------
    */
    public static function getTotalMale(){
        // body
        return DB::table('farmer_basics')->where('gender', 'male')->count();
    }

    /*
    |-----------------------------------------
    | TOTAL FEMALE
    |-----------------------------------------
    */
    public static function getTotalFemale(){
        // body
        return DB::table('farmer_basics')->where('gender', 'female')->count();
    }

    /*
    |-----------------------------------------
    | TOTAL MALE BY STATE
    |-----------------------------------------
    */
    public static function getTotalMaleByState($state){
        // body
        $state = "%".$state."%";
        return DB::table('farmer_basics')->where([['gender', 'male'], ['state', $state]])->count();
    }

    /*
    |-----------------------------------------
    | TOTAL FEMALE BY STATE
    |-----------------------------------------
    */
    public static function getTotalFemaleByState($state){
        // body
        $state = "%".$state."%";
        return DB::table('farmer_basics')->where([['gender', 'female'], ['state', $state]])->count();
    }

    /*
    |-----------------------------------------
    | TOTAL MALE BY LGA
    |-----------------------------------------
    */
    public static function getTotalByLga($state, $lga){
        // body
        return DB::table('farmer_basics')->where([['state', $state], ['lga', $lga]])->count();
    }

    /*
    |-----------------------------------------
    | TOTAL FEMALE BY LGA
    |-----------------------------------------
    */
    // public static function getTotalByLga($state, $lga){
    //     // body
    //     return DB::table('farmer_basics')->where([['state', $state], ['lga', $lga]])->count();
    // }


    /*
    |-----------------------------------------
    | TOTAL MALE BY QUERY
    |-----------------------------------------
    */
    public static function getTotalMaleQuery($states, $lgas){
        // body
        return DB::table('farmer_basics')->where([['gender', 'male'], ['state', $states], ['lga', $lgas]])->count();
    }

    /*
    |-----------------------------------------
    | TOTAL FEMALE BY QUERY
    |-----------------------------------------
    */
    public static function getTotalFemaleQuery($states, $lgas){
        // body
        return DB::table('farmer_basics')->where([['gender', 'female'], ['state', $states], ['lga', $lgas]])->count();
    }

    /*
    |-----------------------------------------
    | LAND AREA MEASUREMENT
    |-----------------------------------------
    */
    public function getLandAreaMeasurement(){
        // body
        $all_farmers = DB::table('farmer_basics')->get();

        $hectare_box       = [];
        $acre_box          = [];
        $plot_box          = [];
        $greenhouse_box    = [];

        foreach ($all_farmers as $el) {

            $data = [
                'owner'     => $el->name,
                'farm_size' => (int)$el->farm_size,
                'farm_type' => $el->farm_type,
                'farm_crop' => $el->farm_crop,
                'state'     => $el->state,
                'lga'       => $el->lga,
            ];

            if($el->farm_size !== null && $el->farm_type !== null){
                if($el->farm_type == "hectare"){   
                    array_push($hectare_box, $data);
                }elseif($el->farm_type == "acre"){
                    array_push($acre_box, $data);
                }elseif($el->farm_type == "plot"){
                    array_push($plot_box, $data);
                }elseif($el->farm_type == "greenhouse(s)"){
                    array_push($greenhouse_box, $data);
                }
            }
        }


        $hectare_box    = collect($hectare_box);
        $acre_box       = collect($acre_box);
        $plot_box       = collect($plot_box);
        $greenhouse_box = collect($greenhouse_box);

        $highest_hectares   = $hectare_box->max("farm_size");
        $highest_acres      = $acre_box->max("farm_size");
        $highest_plots      = $plot_box->max("farm_size");
        $highest_greenhouse = $greenhouse_box->max("farm_size");

        $highest_hectares_owner     = $hectare_box->where("farm_size", $highest_hectares)->first();
        $highest_acres_owner        = $acre_box->where("farm_size", $highest_acres)->first();
        $highest_plots_owner        = $plot_box->where("farm_size", $highest_plots)->first();
        $highest_greenhouse_owner   = $greenhouse_box->where("farm_size", $highest_greenhouse)->first();

        $data = [
            'highest_hectres'       => number_format($highest_hectares),
            'highest_hectares_by'   => $highest_hectares_owner,
            'highest_acres'         => number_format($highest_acres),
            'highest_acres_by'      => $highest_acres_owner,
            'highest_plots'         => number_format($highest_plots),
            'highest_plots_by'      => $highest_plots_owner,
            'highest_greenhouse'    => number_format($highest_greenhouse),
            'highest_greenhouse_by' => $highest_greenhouse_owner,
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | UPDATE PROFILE INFORMATION
    |-----------------------------------------
    */
    public function updateUserRecord($payload){
        // body
        $user_id                = $payload->user_id;
        $names                  = $payload->names;
        $email                  = $payload->email;
        $phone                  = $payload->phone;
        $gender                 = $payload->gender;
        $state                  = $payload->state;
        $lga                    = $payload->lga;
        $language               = $payload->language;
        $cluster                = $payload->cluster;
        $office                 = $payload->office;
        $mobile                 = $payload->mobile;
        $address                = $payload->address;
        $farm_measurement_type  = $payload->farm_measurement_type;
        $farm_measurement_size  = $payload->farm_measurement_size;
        $farm_crop_harvested    = $payload->farm_crop_harvested;
        $cluster                = $payload->cluster;
        $prev_crop_harvested    = $payload->prev_crop_harvested;
        $prev_yield_harvested   = $payload->prev_yield_harvested;
        $prev_crops_planted     = $payload->prev_crops_planted;


        $user_data = FarmerBasic::where("user_id", $user_id)->first();
        if($user_data !== null){
            $update_data                    = FarmerBasic::find($user_data->id);
            $update_data->name              = $names; 
            $update_data->state             = $state;
            $update_data->lga               = $lga;
            $update_data->gender            = $gender;
            $update_data->office            = $office;
            $update_data->mobile            = $mobile;
            $update_data->address           = $address;
            $update_data->cluster           = $cluster;
            $update_data->farm_type         = $farm_measurement_type;
            $update_data->farm_size         = $farm_measurement_size;
            $update_data->farm_crop         = $farm_crop_harvested;
            $update_data->prevCropHarvested = $prev_crop_harvested;
            $update_data->prevYieldHarvested= $prev_yield_harvested;
            $update_data->prevCropPlanted   = $prev_crops_planted;
            if($update_data->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Record updated successfully!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Update error, Could not update record set!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Oops, the record you are trying to update does not exist!'
            ];
        }

        // return 
        return $data;
    }
}
