<?php

namespace Yeelda\Http\Middleware;

use Closure;
use Yeelda\User;
use Auth;

class GuardFarmerAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->account_type !== "farmer"){
            return redirect()->back();
        }
        return $next($request);
    }
}
