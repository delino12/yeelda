<?php

namespace Yeelda\Http\Middleware;

use Closure;

class ApiAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->verifyHeader($request) == false){
            // fail access
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid Access Token',
            ];

            // return response
            return response()->json($data, 401);
        }
        return $next($request);
    }


    /*
    |------------------------------------------
    | VERIFY ANDROID HEADER REQUEST
    |------------------------------------------
    |
    */
    public function verifyHeader($request)
    {
        # verify access token for android
        if($request->header('x-access-token') == env("JWT_TEMP_TOKEN")){
            
            # access granted 
            return true;

        }else{

            # access denied
            return false;
        }
    }
}
