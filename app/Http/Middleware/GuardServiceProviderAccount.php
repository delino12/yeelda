<?php

namespace Yeelda\Http\Middleware;

use Closure;
use Auth;

class GuardServiceProviderAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->account_type !== "service"){
            return redirect()->back();
        }
        return $next($request);
    }
}
