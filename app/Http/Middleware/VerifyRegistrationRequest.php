<?php

namespace Yeelda\Http\Middleware;

use Closure;

class VerifyRegistrationRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // body
        if(!$request->has('email')){
            $data = [
                'status'    => 'error',
                'message'   => 'email field is required!',
            ];

            // return
            // return response
            return response()->json($data, 401);
        }

        if(!$request->has('password')){
            $data = [
                'status'    => 'error',
                'message'   => 'password field is required!',
            ];
            // return response
            return response()->json($data, 401);
        }
        
        if(!$request->has('name')){
            $data = [
                'status'    => 'error',
                'message'   => 'Name field is required!',
            ];

            // return
            // return response
            return response()->json($data, 401);
        }

        if(!$request->has('phone')){
            $data = [
                'status'    => 'error',
                'message'   => 'Phone field is required!',
            ];
            // return response
            return response()->json($data, 401);
        }

        if(!$request->has('accountType')){
            $data = [
                'status'    => 'error',
                'message'   => 'Account Type field is required!',
            ];
            // return response
            return response()->json($data, 401);
        }
        return $next($request);
    }
}
