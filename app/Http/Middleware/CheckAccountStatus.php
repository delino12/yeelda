<?php

namespace Yeelda\Http\Middleware;

use Closure;
use Yeelda\Account;

class CheckAccountStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = $request->email;
        
        $account = Account::where('email', $email)->first();
        if($account !== null){
            if($account->status == 'deactivated'){
                return redirect('/account/deactivated')->with('status', 'Account has been deactivated !');
            }
        }
        return $next($request);
    }
}
