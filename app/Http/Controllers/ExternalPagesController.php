<?php

namespace Yeelda\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Yeelda\Farmer;
use Yeelda\Fertilizer;
use Yeelda\Mail\ContactUs;
use Yeelda\Mail\ContactUsResponse;
use Yeelda\Product;
use Yeelda\Rating;
use Yeelda\Referral;
use Yeelda\Seasonal;
use Yeelda\Seed;
use Yeelda\User;
use Yeelda\FarmerMarketPage;

class ExternalPagesController extends Controller
{

    /*
    |-----------------------------------------
    | SOFTWARE AUTH MIDDLEWARE
    |-----------------------------------------
     */
    public function __construct()
    {
        // body
        // $this->middleware('verify_software');
        $this->middleware('auth')->only('searchFarmerByViewAccess');
    }


    /*
    |-----------------------------------------
    | TEST PAGE
    |-----------------------------------------
    */
    public function testPage(){
        // body
        return view('site-pages.test');
    }

    /*
    |-----------------------------------------
    | SHOW INDEX PAGE
    |-----------------------------------------
     */
    public function index(Request $request)
    {
        // random images
        $images_box = ['1.jpg', '3.jpg', '4.jpg', '5.jpg', '7.jpg', '9.jpg', '10.jpg'];

        // shuffle images
        $total_images  = count($images_box);
        $shuffle_count = rand(0, $total_images - 1);
        $bg            = $images_box[$shuffle_count];

        // reward referrals
        $refer_id = $request->refId;
        if ($refer_id !== "") {
            $referrals = Referral::where('code', $refer_id)->first();
            if ($referrals !== null) {
                $update_point         = Referral::find($referrals->id);
                $update_point->amount = $update_point->amount + 500.00;
                $update_point->vote   = $update_point->vote + 1;
                $update_point->update();
            }
        }

        // return index
        // return view('test-index', compact('bg'));
        return view('index', compact('bg'));
    }

    /*
    |-----------------------------------------
    | SHOW INDEX PAGE
    |-----------------------------------------
     */
    public function indexNew(Request $request)
    {
        // random images
        $images_box = ['1.jpg', '3.jpg', '4.jpg', '5.jpg', '7.jpg', '9.jpg', '10.jpg'];

        // shuffle images
        $total_images  = count($images_box);
        $shuffle_count = rand(0, $total_images - 1);
        $bg            = $images_box[$shuffle_count];

        // reward referrals
        $refer_id = $request->refId;
        if ($refer_id !== "") {
            $referrals = Referral::where('code', $refer_id)->first();
            if ($referrals !== null) {
                $update_point         = Referral::find($referrals->id);
                $update_point->amount = $update_point->amount + 500.00;
                $update_point->vote   = $update_point->vote + 1;
                $update_point->update();
            }
        }

        $platform_stats = User::getStatistics();

        // return index
        return view('test-index', compact('bg', 'platform_stats'));
        // return view('index', compact('bg'));
    }

    /*
    |-----------------------------------------
    | FARMERS FEATURES
    |-----------------------------------------
     */
    public function farmer()
    {
        // body
        return view("site-pages.farmers");
    }

    /*
    |-----------------------------------------
    | FARMERS FEATURES
    |-----------------------------------------
     */
    public function services()
    {
        // body
        return view("site-pages.services");
    }

    /*
    |-----------------------------------------
    | FARMERS FEATURES
    |-----------------------------------------
     */
    public function buyers()
    {
        // body
        return view("site-pages.buyers");
    }

    /*
    |-----------------------------------------
    | SEARCH FOR FARMERS
    |-----------------------------------------
     */
    public function searchFarmer()
    {

        $users = User::getUserByAccount('farmer');
        // body
        return view("site-pages.search-farmers", compact("users"));
    }

    /*
    |-----------------------------------------
    | SEARCH FOR FARMERS WITH MEMBER ACCESS
    |-----------------------------------------
     */
    public function searchFarmerByViewAccess()
    {
        // body
        $users = User::getUserByAccount('farmer');
        return view("special-pages.search-premium-farmers", compact('users'));
    }

    /*
    |-----------------------------------------
    | SEARCH FOR FARMERS WITH MEMBER ACCESS
    |-----------------------------------------
     */
    public function farmerReportByViewAccess()
    {

        $users = User::getReportByAccountWithMemberAccess('farmer');
        // body
        return view("special-pages.premium-farmers-reports", compact("users"));
    }

    /*
    |-----------------------------------------
    | SEARCH SERVICES PROVIDER
    |-----------------------------------------
     */
    public function searchService()
    {
        // body
        $users = User::getUserByAccount('service');
        return view("site-pages.search-services", compact('users'));
    }

    /*
    |-----------------------------------------
    | ALL PRODUCES
    |-----------------------------------------
     */
    public function allProduces()
    {
        // body
        return view("site-pages.all-produces");
    }

    /*
    |-----------------------------------------
    | ALL PRODUCES
    |-----------------------------------------
     */
    public function farmersMarketPlace(Request $request, $user_name, $user_id)
    {
        // body
        $user = User::where("id", $user_id)->first();
        if($user === null){ return redirect()->back(); }
        
        return view("site-pages.farmer-page", compact('user'));
    }

    /*
    |-----------------------------------------
    | ALL PRODUCES
    |-----------------------------------------
     */
    public function allFutureProduces()
    {
        // body
        return view("site-pages.all-future-produces");
    }

    /*
    |-----------------------------------------
    | ALL SELECT SERVICES
    |-----------------------------------------
     */
    public function selectServices()
    {
        // body
        return view("site-pages.select-services");
    }

    /*
    |-----------------------------------------
    | TERMS AND CONDITION
    |-----------------------------------------
     */
    public function terms()
    {
        # code...
        return view('site-pages.terms');
        // return view('external-pages.terms_and_conditions');
    }

    /*
    |-----------------------------------------
    | PRIVACY POLICY
    |-----------------------------------------
     */
    public function privacy()
    {
        # code...
        return view('site-pages.privacy');
        // return view('external-pages.privacy');
    }

    /*
    |-----------------------------------------
    | CONTACT US
    |-----------------------------------------
     */
    public function contact()
    {
        # contact page here
        return view('site-pages.contact');
        // return view('external-pages.contact');
    }

    /*
    |-----------------------------------------
    | ABOUT US
    |-----------------------------------------
     */
    public function about()
    {
        # contact page here
        return view('site-pages.about');
        // return view('external-pages.contact');
    }

    /*
    |-----------------------------------------
    | BLOG
    |-----------------------------------------
     */
    public function blog()
    {
        # contact page here
        return view('site-pages.blog');
    }

    /*
    |-----------------------------------------
    | FAQs
    |-----------------------------------------
     */
    public function faq()
    {
        # contact page here
        return view('site-pages.faq');
        // return view('external-pages.contact');
    }

    // load carts pages
    public function loadCarts()
    {
        if (Auth::check()) {
            $email = Auth::user()->email;
            // return $email;
            return view('site-pages.shopping-carts', compact('email'));
        } else {
            return redirect('/signin');
        }
    }

    // seasons harvest details
    public function futureHarvestInfo($id)
    {
        if (Auth::check()) {
            $email = Auth::user()->email;
            return view('external-pages.future-harvest-info', compact('id'));
        } else {
            return redirect('/account/login');
        }
    }

    // load produces
    public function loadFutureInfo($id)
    {
        # code...
        $seasons = Seasonal::where('id', $id)->first();

        //get product owner
        $farmer = User::where('id', $seasons->user_id)->first();
        if ($farmer !== null) {
            $total = $seasons->product_size_no * $seasons->product_price;

            // get rating
            $rating = Rating::where('user_email', $farmer->email)->first();
            if ($rating == null) {
                $rating = [
                    "user_email" => "none",
                    "vote"       => 0,
                ];
            }

            # code...
            $data = array(
                "id"                => $seasons->id,
                "owner_name"        => $farmer->name,
                "owner_email"       => $farmer->email,
                "owner_contact"     => $farmer->phone,
                // "owner_rating"      => $rating->vote,
                "product_name"      => $seasons->product_name,
                "product_status"    => 'Future Harvest',
                "product_size_type" => $seasons->product_size_type,
                "product_size_no"   => $seasons->product_size_no,
                "product_price"     => $seasons->product_price,
                "product_total"     => number_format($total, 2),
                "product_location"  => $seasons->product_location,
                "product_state"     => $seasons->product_state,
                "product_image"     => $seasons->product_image,
                "product_note"      => $seasons->product_note,
                "planning_date"     => $seasons->product_planning_date,
                "delivery_date"     => $seasons->product_delivery_date,
                "delivery_type"     => $seasons->product_delivery_type,
                "created_at"        => $seasons->created_at->diffForHumans(),
            );
        } else {
            $data = [];
        }

        // return response information
        return response()->json($data);
    }

    // load products in Json
    public function shopProducts()
    {
        $products = Product::orderBy("id", "DESC")->get();
        if (count($products) > 0) {
            $product_box = [];
            foreach ($products as $product) {
                //get product owner
                $farmer = User::where('id', $product->user_id)->first();
                if ($farmer !== null) {
                    $total = $product->product_size_no * $product->product_price;

                    // get rating
                    $rating = Rating::where('user_email', $farmer->email)->first();
                    if ($rating == null) {
                        $rating = [
                            "user_email" => "none",
                            "vote"       => 0,
                        ];
                    }

                    // save data from farm produce
                    $data = array(
                        "id"                => $product->id,
                        "owner_name"        => $farmer->name,
                        "owner_email"       => $farmer->email,
                        "owner_contact"     => $farmer->phone,
                        // "owner_rating"      => $rating->vote,
                        "product_name"      => $product->product_name,
                        "product_status"    => 'Harvested',
                        "product_size_type" => $product->product_size_type,
                        "product_size_no"   => $product->product_size_no,
                        "product_price"     => $product->product_price,
                        "product_total"     => number_format($total, 2),
                        "product_location"  => $product->product_location,
                        "product_state"     => $product->product_state,
                        "product_image"     => $product->product_image,
                        "product_note"      => $product->product_note,
                        "created_at"        => $product->created_at->diffForHumans(),
                    );

                    array_push($product_box, $data);
                }
            }
        } else {
            $product_box = [];
        }

        // return responses
        return response()->json($product_box);
    }

    // load future products in Json
    public function futureShopProducts()
    {
        $product_box = [];
        $seasonal_products = Seasonal::all();
        foreach ($seasonal_products as $seasons) {
            $total = $seasons->product_size_no * $seasons->product_price;
            $farmer = User::where('id', $seasons->user_id)->first();
            if ($farmer !== null) {
                # code...
                $data = array(
                    "id"                => $seasons->id,
                    "owner_name"        => $farmer->name,
                    "owner_email"       => $farmer->email,
                    "owner_contact"     => $farmer->phone,
                    // "owner_rating"      => $rating->vote,
                    "product_name"      => $seasons->product_name,
                    "product_status"    => 'Future Harvest',
                    "product_size_type" => $seasons->product_size_type,
                    "product_size_no"   => $seasons->product_size_no,
                    "product_price"     => $seasons->product_price,
                    "product_total"     => number_format($total, 2),
                    "product_location"  => $seasons->product_location,
                    "product_state"     => $seasons->product_state,
                    "product_image"     => $seasons->product_image,
                    "product_note"      => $seasons->product_note,
                    "planning_date"     => $seasons->product_planning_date,
                    "delivery_date"     => $seasons->product_delivery_date,
                    "delivery_type"     => $seasons->product_delivery_type,
                    "created_at"        => $seasons->created_at->diffForHumans(),
                );
                array_push($product_box, $data);
            }
        }

        // return responses
        return response()->json($product_box);
    }

    // load products in Json
    public function loadRecentProduce()
    {
        $products = Product::orderBy("id", "DESC")->limit('3')->get();
        if (count($products) > 0) {
            $product_box = [];
            foreach ($products as $product) {
                //get product owner
                $farmer = User::where('id', $product->user_id)->first();
                if ($farmer !== null) {
                    $total = $product->product_size_no * $product->product_price;

                    // get rating
                    $rating = Rating::where('user_email', $farmer->email)->first();
                    if ($rating == null) {
                        $rating = [
                            "user_email" => "none",
                            "vote"       => 0,
                        ];
                    }

                    // save data from farm produce
                    $data = array(
                        "id"                => $product->id,
                        "owner_name"        => $farmer->name,
                        "owner_email"       => $farmer->email,
                        "owner_contact"     => $farmer->phone,
                        // "owner_rating"      => $rating->vote,
                        "product_name"      => $product->product_name,
                        "product_status"    => 'Harvested',
                        "product_size_type" => $product->product_size_type,
                        "product_size_no"   => $product->product_size_no,
                        "product_price"     => $product->product_price,
                        "product_total"     => number_format($total, 2),
                        "product_location"  => $product->product_location,
                        "product_state"     => $product->product_state,
                        "product_image"     => $product->product_image,
                        "product_note"      => $product->product_note,
                        "created_at"        => $product->created_at->diffForHumans(),
                    );

                    array_push($product_box, $data);
                }
            }

            # code...
            $seasonal_products = Seasonal::orderBy("id", "DESC")->limit('3')->get();
            foreach ($seasonal_products as $seasons) {
                $farmer = User::where('id', $seasons->user_id)->first();
                if ($farmer !== null) {
                    # code...
                    $data = array(
                        "id"                => $seasons->id,
                        "owner_name"        => $farmer->name,
                        "owner_email"       => $farmer->email,
                        "owner_contact"     => $farmer->phone,
                        // "owner_rating"      => $rating->vote,
                        "product_name"      => $seasons->product_name,
                        "product_status"    => 'Future Harvest',
                        "product_size_type" => $seasons->product_size_type,
                        "product_size_no"   => $seasons->product_size_no,
                        "product_price"     => $seasons->product_price,
                        "product_total"     => number_format($total, 2),
                        "product_location"  => $seasons->product_location,
                        "product_state"     => $seasons->product_state,
                        "product_image"     => $seasons->product_image,
                        "product_note"      => $seasons->product_note,
                        "planning_date"     => $seasons->product_planning_date,
                        "delivery_date"     => $seasons->product_delivery_date,
                        "delivery_type"     => $seasons->product_delivery_type,
                        "created_at"        => $seasons->created_at->diffForHumans(),
                    );
                    array_push($product_box, $data);
                }
            }
        } else {
            $product_box = [];
        }

        // return responses
        return response()->json($product_box);
    }


    // load products in Json custom search
    public function searchProduce(Request $request)
    {
        $category   = $request->category;
        $location   = $request->location;
        $price_from = $request->priceFrom;
        $price_to   = $request->priceTo;

        // using wildcard
        $category = '%' . $category . '%';

        // first init check
        $search_products = Product::where('product_name', 'LIKE', $category)->get();
        // return response()->json($search_products);
        // $products = Product::all();
        $product_box = [];
        foreach ($search_products as $product) {
            # code...
            //get product owner
            $farmer = User::where('id', $product->user_id)->first();
            $total  = $product->product_size_no * $product->product_price;

            // get rating
            $rating = Rating::where('user_email', $farmer->email)->first();

            $data = array(
                "id"                => $product->id,
                "owner_name"        => $farmer->name,
                "owner_email"       => $farmer->email,
                "owner_contact"     => $farmer->phone,
                // "owner_rating"      => $rating->vote,
                "product_name"      => $product->product_name,
                "product_size_type" => $product->product_size_type,
                "product_size_no"   => $product->product_size_no,
                "product_price"     => $product->product_price,
                "product_total"     => number_format($total, 2),
                "product_location"  => $product->product_location,
                "product_state"     => $product->product_state,
                "product_image"     => $product->product_image,
                "product_note"      => $product->product_note,
                "created_at"        => $product->created_at->diffForHumans(),
            );

            array_push($product_box, $data);
        }

        return response()->json($product_box);
    }

    // load equipment in json
    public function loadEquipments()
    {
        // fetch equipment
        return $equipments = DB::table('equipments')->get();

        // equipment box
        $equipment_box = [];
        foreach ($equipments as $tools) {

            // check if tools is hired free
            if ($tools->equipment_status == 'available') {
                # code...
                $status = "Free";
            } else {
                $status = "Occupied";
            }

            //get product owner
            $provider = DB::table('users')->where('id', $tools->user_id)->first();
            if ($provider !== null) {
                // calculate total cost
                $total = (int) $tools->equipment_no * (int) $tools->equipment_price;

                // status in used
                $data = array(
                    "id"                      => $tools->id,
                    "user_id"                 => $tools->user_id,
                    "owner_name"              => $provider->name,
                    "owner_email"             => $provider->email,
                    "owner_contact"           => $provider->phone,
                    "equipment_name"          => $tools->equipment_name,
                    "equipment_descriptions"  => $tools->equipment_descriptions,
                    "equipment_no"            => $tools->equipment_no,
                    "equipment_status"        => $status,
                    "total"                   => $total,
                    "equipment_price"         => $tools->equipment_price,
                    "equipment_address"       => $tools->equipment_address,
                    "equipment_state"         => $tools->equipment_state,
                    "equipment_image"         => $tools->equipment_image,
                    "equipment_delivery_type" => $tools->equipment_delivery_type,
                    "equipment_note"          => $tools->equipment_note,
                    "created_at"              => $tools->created_at,
                    "updated_at"              => $tools->updated_at,
                );

                array_push($equipment_box, $data);
            }
        }

        return response()->json($equipment_box);
    }

    // load seeds
    public function loadSeeds()
    {
        # code...
        $seeds = Seed::all();

        $seeds_box = [];
        foreach ($seeds as $seed) {

            $user_info = $this->getUserInfo($seed->user_email);

            # code...
            $data = array(
                'id'          => $seed->id,
                'owner_email' => $user_info['email'],
                'owner_name'  => $user_info['name'],
                'name'        => $seed->name,
                'qty'         => $seed->qty,
                'amount'      => number_format($seed->amount, 2),
                'note'        => $seed->note,
                'avatar'      => $seed->avatar,
                'date'        => $seed->created_at->diffForHumans(),
            );

            array_push($seeds_box, $data);
        }

        // return response data
        return response()->json($seeds_box);
    }

    // load seeds
    public function loadFert()
    {
        # code...
        $fertilizers = Fertilizer::all();

        $fertilizers_box = [];
        foreach ($fertilizers as $fert) {

            $user_info = $this->getUserInfo($fert->user_email);

            # code...
            $data = array(
                'id'          => $fert->id,
                'owner_email' => $user_info['email'],
                'owner_name'  => $user_info['name'],
                'name'        => $fert->name,
                'qty'         => $fert->qty,
                'amount'      => number_format($fert->amount, 2),
                'note'        => $fert->note,
                'avatar'      => $fert->avatar,
                'date'        => $fert->created_at->diffForHumans(),
            );

            array_push($fertilizers_box, $data);
        }

        // return response data
        return response()->json($fertilizers_box);
    }

    // post contact Message
    public function sendContactMsg(Request $request)
    {
        $name    = $request->name;
        $email   = $request->email;
        $subject = $request->subject;
        $message = $request->message;

        // data to array
        $data = array(
            'name'    => $name,
            'email'   => $email,
            'subject' => $subject,
            'message' => $message,
        );

        // to client
        // fired mail to client
        \Mail::to($email)->send(new ContactUsResponse($data));

        // to admin
        // fired mail to admin
        $admin = 'info@yeelda.ng';
        \Mail::to($admin)->send(new ContactUs($data));

        // Data response
        $data = array(
            'status'  => 'success',
            'message' => 'Message has been sent !',
        );

        // return data response
        return response()->json($data);
    }

    // get user info
    public function getUserInfo($email)
    {
        # code...
        // get senders name
        $check_farmers  = User::where('email', $email)->first();
        $check_service  = User::where('email', $email)->first();
        $check_investor = User::where('email', $email)->first();

        if ($check_farmers !== null) {
            $name  = $check_farmers->name;
            $phone = $check_farmers->phone;

            // basic data
            $data = array(
                'name'  => $name,
                'phone' => $phone,
                'email' => $email,
            );
        } elseif ($check_service !== null) {
            $name  = $check_service->name;
            $phone = $check_service->phone;

            // basic data
            $data = array(
                'name'  => $name,
                'phone' => $phone,
                'email' => $email,
            );

        } elseif ($check_investor !== null) {
            $name  = $check_investor->name;
            $phone = $check_investor->phone;

            // basic data
            $data = array(
                'name'  => $name,
                'phone' => $phone,
                'email' => $email,
            );
        }

        // return data
        return $data;
    }


    /*
    |-----------------------------------------
    | UPDATE MARKET PLACE PAGE SETTINGS
    |-----------------------------------------
    */
    public function updatePageSettings(Request $request){
        // body
        $page_config    = new FarmerMarketPage();
        $data           = $page_config->updatePageConfig($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | SHOW PAGE SETTINGS FOR FARMERS
    |-----------------------------------------
    */
    public function showPageSetting(Request $request, $user_id){
        // body
        $user = User::where("id", $user_id)->first();
        $page_config    = new FarmerMarketPage();
        $data           = $page_config->initPageConfig($user_id);

        $page_data      = FarmerMarketPage::where("user_id", $user_id)->first();
        $page_data->config = json_decode($page_data->config, true);
        
        return view("site-pages.farmer-page-settings", compact('user', 'page_data'));
    }

}
