<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Investor;
use Yeelda\Account;
use Yeelda\User;
use Image;
use Hash;
use Auth;
use DB;

class InvestorResponseController extends Controller
{
    // Guard page from external login
    public function __construct() {
        $this->middleware('auth');
    }

    // load basic information from investor
    public function basicInfo($id)
    {
    	# Get all basic information details
    	$info = DB::table('investor_basics')->where('user_id', $id)->first();
    	$user = DB::table('investors')->where('id', $id)->first();

    	// check info
    	if($info->gender == null){
    		$info->gender = 'not available';
    	}
    	if($info->office == null){
    		$info->office = 'not available';
    	}
    	if($info->address == null){
    		$info->address = 'not available';
    	}
    	if($info->zipcode == null){
    		$info->zipcode = 'not available';
    	}
    	if($info->state == null){
    		$info->state = 'not available';
    	}

		# code...
		$data = array(
			'id'      => $info->id,
			'user_id' => $info->user_id,
			'name'    => $info->name,
			'email'   => $user->email,
			'gender'  => $info->gender,
			'address' => $info->address,
			'state'   => $info->state,
			'zipcode' => $info->zipcode,
			'avatar'  => $info->avatar,
			'office'  => $info->office,
			'mobile'  => $user->phone,
			'date'    => $info->created_at
		);

		// return response
		return response()->json($data);
    }

    // load profile image
    public function loadProfile()
    {
        # code...
        # Get all basic information details
        // request ajax data
        $id   = Auth::user()->id;
        $info = DB::table('investor_basics')->where('user_id', $id)->first();
        $user = DB::table('users')->where('id', $id)->first();

        // check info
        if($info->gender == null){
            $info->gender = 'not available';
        }
        if($info->office == null){
            $info->office = 'not available';
        }
        if($info->address == null){
            $info->address = 'not available';
        }
        if($info->zipcode == null){
            $info->zipcode = 'not available';
        }
        if($info->state == null){
            $info->state = 'not available';
        }

        # code...
        $data = array(
            'id'      => $info->id,
            'user_id' => $info->user_id,
            'name'    => $info->name,
            'email'   => $user->email,
            'gender'  => $info->gender,
            'address' => $info->address,
            'state'   => $info->state,
            'zipcode' => $info->zipcode,
            'avatar'  => $info->avatar,
            'office'  => $info->office,
            'mobile'  => $user->phone,
            'date'    => $info->created_at
        );

        // return response
        return response()->json($data);

    }

    // update profile images 
    public function updateAvatar(Request $request)
    {
        # code...
        // get current user
        $user_id = Auth::user()->id;

        // request image file
        $image = $request->file('file');

        // rename image
        $new_name = 'yeelda-'.time().'.'.$image->getClientOriginalExtension();

        // set destination path
        $destinationPath = public_path('/uploads/buyers/');

        $thumb_img = Image::make($image->getRealPath());
        $thumb_img->resize(240, 320, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$new_name);

        // move image to destination path
        // $image->move($destinationPath, $new_name);

        // update basic information
        $update_profile = DB::table('investor_basics')->where('user_id', $user_id)->update([
            'avatar' => $new_name,
        ]);

        // // return response
        // return response($content = $new_name, $status = 200, $headers = [
        //     'Content-Type' => 'json'
        // ]);

        return redirect()->back();
    }

    // update profile information
    public function updateProfile(Request $request){

    	// request from form
    	$id      = Auth::user()->id;
		$state   = $request->state;	
		$address = $request->address;
		$postal  = $request->postal;
        $office  = $request->office;
		$mobile  = $request->mobile;
		$gender  = $request->gender;

		// find row id update record
		$update = DB::table('investor_basics')->where('user_id', $id)
		->update([
        	'state'   => $state,
        	'address' => $address,
        	'zipcode' => $postal,
        	'office'  => $office,
        	'gender'  => $gender,
    	]);

        // update mobile contact information
        $find_buyer        = User::find($id);
        $find_buyer->phone = $mobile;
        $find_buyer->update();

		// check if successful updates
        if($update){
        	$data = array(
        		'status' => 'success',
        		'message' => 'Profile updated successfully !'
        	);
        }else{
        	$data = array(
        		'status' => 'error',
        		'message' => 'Fail to update profile !'
        	);
        }

        // return response
        return response()->json($data);
    }

    // load payments details
    public function paymentInfo()
    {
        # code...
        $id = Auth::user()->id;

        # Get all basic information details
        $info = DB::table('investor_accounts')->where('user_id', $id)->first();

        // data
        $data = array(
            'id'           => $info->id,
            'bank_name'    => $info->bank_name,
            'account_name' => $info->account_name,
            'account_no'   => $info->account_no,
            'account_type' => $info->account_type,
            'card_type'    => $info->card_type,
            'card_holder'  => $info->card_holder,
            'card_no'      => $info->card_no,
            'exp'          => $info->exp,
            'cvv'          => $info->cvv,
            'date'         => $info->created_at
        );

        // return response data
        return response()->json($data);
    }

    // update payments details
    public function updatePayment(Request $request)
    {
        # code...
        // get current user
        $user_id = Auth::user()->id;

        // re-assign form vars
        $bank_name    = $request->accBank;
        $account_name = $request->accName;
        $account_no   = $request->accNumber;
        $account_type = $request->accType;

        // save account information to database
        $account = DB::table('investor_accounts')->where('user_id', $user_id)->update([
            'bank_name'    => $bank_name,
            'account_name' => $account_name,
            'account_no'   => $account_no,
            'account_type' => $account_type,
            'updated_at'   => NOW(),
        ]);

       // check if successful updates
        if($account){
            $data = array(
                'status' => 'success',
                'message' => 'Account details updated successfully !'
            );
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'Fail to update profile !'
            );
        }

        // return response
        return response()->json($data);
    }

    // update card information
    public function updateCard(Request $request)
    {
        # code...
        // get current user
        $user_id = Auth::user()->id;

        // re-assign form vars
        $card_holder = $request->holder;
        $card_no     = $request->number;
        $card_type   = $request->type;
        $card_cvv    = $request->cvv;
        $card_exp    = $request->exp;

        // save account information to database
        $account = DB::table('investor_accounts')->where('user_id', $user_id)->update([
            'card_type'   => $card_type,
            'card_holder' => $card_holder,
            'card_no'     => $card_no,
            'exp'         => $card_exp,
            'cvv'         => $card_cvv
        ]);

       // check if successful updates
        if($account){
            $data = array(
                'status' => 'success',
                'message' => 'Card details updated successfully !'
            );
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'Fail to update Debit/Credit Card information !'
            );
        }

        // return response
        return response()->json($data);
    }

    // return response controller 
    public function changePassword(Request $request)
    {
        // ajax data
        $user_id = Auth::user()->id;
        $password = $request->password;

        // encrypt password
        $password = bcrypt($password);

        $update_password           = User::find($user_id);
        $update_password->password = $password;
        $update_password->update();

        $data = array(
            'status'  => 'success',
            'message' => 'Password updated successfully !'
        );
    
        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------
    |   DEACTIVATE ACCOUNT
    |-------------------------------------------
    |
    */
    public function deactivateAccount(Request $request)
    {
        # code...
        $userid     = $request->userid;
        $password   = $request->password;
        $password   = str_replace(' ', '', $password);

        // check if user exist
        $check_buyers = User::where('id', $userid)->first();

        // return $check_buyers;
        if($check_buyers !== null){
            $password_hash = $check_buyers->password;

            // compare password
            if(Hash::check($password, $password_hash)){

                // check if already deactivate
                $account_deactivated = Account::where('email', $check_buyers->email)->first();

                if($account_deactivated !== null){
                    
                    $data = [
                        'status' => 'error',
                        'message' => 'Account has been deactivated already'
                    ];

                }else{

                    $account            = new Account();
                    $account->email     = $check_buyers->email;
                    $account->status    = "deactivated";
                    $account->save();

                    $data = [
                        'status' => 'success',
                        'message' => 'Account has been deactivated successfully !'
                    ];
                }
            }else{

                $data = [
                    'status' => 'error',
                    'message' => 'password did not match !'
                ];
            }
        }

        return response()->json($data);
    }
}
