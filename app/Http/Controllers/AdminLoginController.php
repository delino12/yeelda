<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Admin;
use Auth;

class AdminLoginController extends Controller
{
    /*
    * Admin Login 
    */

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // show admin login form
    public function showLoginForm()
    {
        // check if admin already exits
        $check_admin = Admin::find(1);
        if($check_admin == null){
            // create new admin
            $level = "Alpha";
            $email = "admin@yeelda.ng";
            $password = bcrypt('password@12345');

            $create_admin           = new Admin();
            $create_admin->level    = $level;
            $create_admin->email    = $email;
            $create_admin->password = $password;
            $create_admin->save();
        }
    	# show admin login form
    	return view("auth.admin-login");
    }

    // process admin login
    public function doLogin(Request $request){
        // ajax data
        $username = $request->username;
        $password = $request->password;

        // remember token
        $rememberToken = $request->remember;

        // check auth for Admin login Guard
        if (Auth::guard('admin')->attempt(['email' => $username, 'password' => $password], $rememberToken)) {
            // if successful 
            $data = array(
                'status'  => 'success',
                'message' => 'Login Successful !'
            );

            // return response
            return response()->json($data);

        } else {
             // if successful 
            $data = array(
                'status'  => 'error',
                'message' => 'Login Fail !'
            );

            // return response
            return response()->json($data);
        }
    }
}
