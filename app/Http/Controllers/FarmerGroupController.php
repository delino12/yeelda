<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\FarmerGroup;

class FarmerGroupController extends Controller
{
    /*
    |-----------------------------------------
    | SHOW INDEX
    |-----------------------------------------
    */
    public function index(){
    	// body
    	return view('admin-pages.view-groups');
    }


    /*
    |-----------------------------------------
    | CREATE GROUP
    |-----------------------------------------
    */
    public function create(Request $request){
    	// body
    	$group 	= new FarmerGroup();
    	$data 	= $group->addGroup($request);

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | EDIT GROUP
    |-----------------------------------------
    */
    public function edit(Request $request){
    	// body
    	$group 	= new FarmerGroup();
    	$data 	= $group->editGroup($request);

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE GROUP
    |-----------------------------------------
    */
    public function delete(Request $request){
    	// body
    	$group 	= new FarmerGroup();
    	$data 	= $group->delGroup($request);

    	// return response.
    	return response()->json($data);
    }


    /*
    |-----------------------------------------
    | LOAD ALL GROUPS
    |-----------------------------------------
    */
    public function loadAll(Request $request){
    	// body
    	$group 	= new FarmerGroup();
    	$data 	= $group->allGroups($request);

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD ONE GROUP
    |-----------------------------------------
    */
    public function loadOne(Request $request){
    	// body
    	$group 	= new FarmerGroup();
    	$data 	= $group->oneGroup($request);

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD ALL GROUP USERS
    |-----------------------------------------
    */
    public function loadGroupUsers(Request $request){
        // body
        $group  = new FarmerGroup();
        $data   = $group->allGroupsCapturedUsers($request->group_id);

        // return response.
        return response()->json($data);
    }
}
