<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\GroupChatInvite;
use Yeelda\Events\NewChatGroup;
use Yeelda\Events\NewChat;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\Chat;
use Yeelda\Association;
use Yeelda\User;
use Auth;
use DB;

class ChatResponseController extends Controller
{
    // post chat messages
    public function postChat(Request $request)
    {
    	# request param
    	$email = Auth::user()->email;
    	$body  = $request->message;

        // sanitize string
        $body = $this->sanitize($body);

    	// room by default
    	$room   = 'general';
    	$status = 'unread';

    	// save chat 
    	$chat         = new Chat();
    	$chat->email  = $email;
    	$chat->room   = $room;
    	$chat->status = $status;
    	$chat->body   = $body;
    	$chat->save();

    	// get last chats 
    	$last_chats = Chat::where([['email', $email], ['body', $body], ['room', $room]])->orderBy('id', 'desc')->first();

        $user_info = $this->getSenderInfo($email);

    	$data = array(
    		'id'     => $last_chats->id,
    		'email'  => $last_chats->email,
            'name'   => $user_info['name'],
            'image'  => $user_info['image'],
    		'status' => $last_chats->status,
    		'body'   => $last_chats->body,
    		'room'   => $last_chats->room,
    		'date'   => $last_chats->created_at->diffForHumans()
    	);

    	// Fire new chat message event
    	\Event::fire(new NewChat($data));

    	// response contents
    	$data = array(
    		'status' => 'success',
    		'message' => 'chat sent !'
    	);

    	// return response
    	return response()->json($data);
    }

    // post group chat messages
    public function postGroupChat(Request $request)
    {
        # request param
        $email = $request->email;
        $body  = $request->message;

        // sanitized
        $body = $this->sanitize($body);

        // room by default
        $room   = $request->room;
        $status = 'unread';

        // save chat 
        $chat         = new Chat();
        $chat->email  = $email;
        $chat->room   = $room;
        $chat->status = $status;
        $chat->body   = $body;
        $chat->save();

        // get last chats 
        $last_chats = Chat::where([['email', $email], ['body', $body], ['room', $room]])->orderBy('id', 'desc')->first();
        // get senders name
        $user = User::where('email', $chat->email)->first();
        if($user !== null){
            $name = $user->name;
        }

        $data = array(
            'id'     => $last_chats->id,
            'email'  => $last_chats->email,
            'name'   => $name,
            'status' => $last_chats->status,
            'body'   => $last_chats->body,
            'room'   => $last_chats->room,
            'date'   => $last_chats->created_at->diffForHumans()
        );

        // Fire new chat message event
        \Event::fire(new NewChatGroup($data));

        // response contents
        $data = array(
            'status' => 'success',
            'message' => 'chat sent !'
        );

        // return response
        return response()->json($data);
    }

    // load chat messages
    public function loadChat()
    {
    	# all chats
    	$all_chats = Chat::where('room', 'general')->orderBy('created_at', 'ASC')->get();

    	// first load all chats
    	$chat_box = []; 
    	foreach ($all_chats as $chat) {
            // get senders name
            $user = User::where('email', $chat->email)->first();
            if($user !== null){
                $name = $user->name;
            }else{
                $name = "None";
            }

    		// chat data
    		$data = array(
	    		'id'     => $chat->id,
                'name'   => $name,
	    		'email'  => $chat->email,
	    		'status' => $chat->status,
	    		'body'   => $chat->body,
	    		'room'   => $chat->room,
	    		'date'   => $chat->created_at->diffForHumans()
	    	);

	    	// array push info
	    	array_push($chat_box, $data);
    	}

    	return response()->json($chat_box);
    }

    // join group Chats
    public function joinAssoc(Request $request)
    {
        // request from mail..
        $group_name   = $request->name;
        $member       = $request->email;

        // get host
        $fetch_assoc = Association::where('name', $group_name)->first();
        $host   = $fetch_assoc->host;
        $name   = $fetch_assoc->name;
        $room   = $fetch_assoc->room;
        $status = $fetch_assoc->status;

        // add to assocs
        $associations          = new Association();
        $associations->name    = $name;
        $associations->room    = $room;
        $associations->members = $member;
        $associations->host    = $host;
        $associations->status  = $status;
        $associations->save();

        // success msg
        $msg = 'Congratulations!, You have joined '.$group_name.' Successfully !. Login or Signup to proceed...';

        return redirect('/account/login')->with('success_msg', $msg);
    }

    // load chat messages
    public function loadGroupChat(Request $request)
    {
        // group name
        $room = $request->name;

        # all chats
        $all_chats = Chat::where('room', $room)->orderBy('created_at', 'ASC')->get();

        // first load all chats
        $chat_box = []; 
        foreach ($all_chats as $chat) {

            // get senders name
            $user = User::where('email', $chat->email)->first();
            if($user !== null){
                $name = $user->name;
            }

            // chat data
            $data = array(
                'id'     => $chat->id,
                'name'   => $name,
                'email'  => $chat->email,
                'status' => $chat->status,
                'body'   => $chat->body,
                'room'   => $chat->room,
                'date'   => $chat->created_at->diffForHumans()
            );

            // array push info
            array_push($chat_box, $data);
        }

        return response()->json($chat_box);
    }

    // load users 
    public function loadChatUsers()
    {
        # load chat users
        $farmers = User::where('account_type', 'farmer')->orderBy('id', 'DESC')->limit('20')->get();
        $service = User::where('account_type', 'service')->orderBy('id', 'DESC')->limit('20')->get();
        $buyers = User::where('account_type', 'buyer')->orderBy('id', 'DESC')->limit('20')->get();

        // info of all farmers
        $chat_users = [];
        foreach ($farmers as $user) {
            $farmer_data = DB::table("farmer_basics")->where("user_id", $user->id)->first();
            if($farmer_data !== null){
                if($farmer_data->avatar == null){
                    $image = null;
                }else{
                    $image = $farmer_data->avatar;
                }
            }else{
                $image = null;
            }

            # code...
            $data = array(
                'name'          => $user->name,
                'email'         => $user->email,
                'account_type'  => $user->account_type,
                'avatar'        => $image
            );

            array_push($chat_users, $data);
        }

        foreach ($service as $user) {
            $service_data = DB::table("service_basics")->where("user_id", $user->id)->first();
            if($service_data !== null){
                if($service_data->avatar == null){
                    $image = null;
                }else{
                    $image = $farmer_data->avatar;
                }
            }else{
                $image = null;
            }

            # code...
            $data = array(
                'name'          => $user->name,
                'email'         => $user->email,
                'account_type'  => $user->account_type,
                'avatar'        => $image
            );

            array_push($chat_users, $data);
        }

        foreach ($buyers as $user) {
            $buyer = DB::table("investor_basics")->where("user_id", $user->id)->first();
            if($buyer !== null){
                if($buyer->avatar == null){
                    $image = null;
                }else{
                    $image = $farmer_data->avatar;
                }
            }else{
                $image = null;
            }
            
            # code...
            $data = array(
                'name'          => $user->name,
                'email'         => $user->email,
                'account_type'  => $user->account_type,
                'avatar'        => $image
            );

            array_push($chat_users, $data);
        }

        // return responses 
        return response()->json($chat_users);
    }

    // load associations 
    public function loadAssoc(Request $request)
    {
        # logged email
        $email = $request->email;
        
        # load associations
        $associations = Association::where('members', $email)->get();

        # load assoc box
        $assoc_box = [];
        foreach ($associations as $assoc) {
            # code...
            $data = [
                'id'        => $assoc->id,
                'name'      => $assoc->name,
                'status'    => $assoc->status,
                'date'      => $assoc->created_at->diffForHumans()
            ];

           array_push($assoc_box, $data);
        }

        // check if data is empty
        if(count($assoc_box) > 0){
            // found something return it
            return response()->json($assoc_box);
        }else{
            // array is empty
            $msg = array(
                'status'  => 'info',
                'message' => '<span class="small">You are not a member yet !, you can join or create Association, <span class="text-danger">Note:</span> admin approval is required !</span>'
            );

            return response()->json($msg);
        }
    }

    // create associations
    public function createAssoc(Request $request){
        // ajax data
        $name  = $request->name;
        $host  = $request->host;
        $state = $request->state;

        // sanitize
        $name = $this->sanitize($name);

        // send create request to admin
        $room   = 'ASSOC-'.$name;
        $status = 'pending';

        // host is also member
        $members = $host;

        // Associations
        $new_assoc          = new Association();
        $new_assoc->name    = $name;
        $new_assoc->room    = $room;
        $new_assoc->members = $members;
        $new_assoc->host    = $host;
        $new_assoc->status  = $status;
        $new_assoc->save();

        // send approval mail & Association Notification

        $data = array(
            'status'  => 'success',
            'message' => 'Association created !, <b class="text-info">'.$name.'</b> is waiting approval from yeelda'
        );

        return response()->json($data);
    }

    // load group chats users
    public function loadGroupUsers(Request $request)
    {
        $group_name = $request->name;
        $all_members = Association::where([['name', $group_name], ['status', 'approved']])->get();

        $members_box = [];
        foreach ($all_members as $member) {

            // get senders name
            $user = User::where('email', $member->members)->first();
            if($user !== null){
                $name = $user->name;
            }else{
                $name = "None";
            }
            
            # code...
            $data = array(
                'id' => $member->id,
                'name' => $name,
                'image' => null,
                'members' => $member->members
            );

            array_push($members_box, $data);
        }

        # code...
        return response()->json($members_box);
    }

    // send chat invites
    public function sendInvites(Request $request)
    {
        # from ajax
        $email_one   = $request->emailA;
        $email_two   = $request->emailB;
        $email_three = $request->emailC;
        $email_four  = $request->emailD;

        # group name
        $group_name  = $request->name;
        $assoc_name = str_replace('ASSOC-', '', $group_name);

        // check if mail one not empty
        if(!empty($email_one)){
            // mail data
            $mail_data = array(
                'email' => $email_one,
                'name' => $assoc_name
            );

            // send mail 1
            \Mail::to($email_one)->send(new GroupChatInvite($mail_data));
        }
        
        // send mail 2
        if(!empty($email_two)){
            // mail data
            $mail_data = array(
                'email' => $email_two,
                'name' => $assoc_name
            );
            \Mail::to($email_two)->send(new GroupChatInvite($mail_data));
        }
        // send mail 3
        if(!empty($email_three)){
            // mail data
            $mail_data = array(
                'email' => $email_three,
                'name' => $assoc_name
            );
            \Mail::to($email_three)->send(new GroupChatInvite($mail_data));
        }
        // send mail 4
        if(!empty($email_four)){
            // mail data
            $mail_data = array(
                'email' => $email_four,
                'name' => $assoc_name
            );
            \Mail::to($email_four)->send(new GroupChatInvite($mail_data));
        }

        // data to response
        $data = array(
            'status'  => 'success',
            'message' => 'Invitation sent !'
        );

        // return response
        return response()->json($data);
    }

    // get sender information
    public function getSenderInfo($email)
    {
        # code...
        $user = User::where("email", $email)->first();

        $data = [
            'name'          => $user->name,
            'image'         => null,
            'account_type'  => $user->account_type
        ];

        // return information
        return $data;
    }

    // filtered hack 
    public function sanitize($data)
    {
        # code...
        $data = filter_var($data, FILTER_SANITIZE_STRING);
        $data = strip_tags($data);
        $data = stripslashes($data);

        return $data;
    }

    /*
    |-----------------------------------------
    | CLEAR OLD CHAT MESSAGES
    |-----------------------------------------
    */
    public function clearOldChat(){
        // body
        $chat_message = new Chat();
        $data = $chat_message->clearOldMessages();

        // return response.
        return response()->json($data);
    }
}
