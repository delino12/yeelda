<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\SMS;
use Yeelda\Smslog;
use Carbon\Carbon;
use Auth;
use DB;

class SmsResponseController extends Controller
{

    // authenticate
    public function __construct()
    {
        # Auth middleware for admin...
        $this->middleware('auth:admin');
    }

    // initliaze contact 
    public function initializeContact($type)
    {
    	if($type == 'farmers'){

    		// get all farmers
    		$all_farmers = Farmer::all();
	       	if(count($all_farmers) > 0){

	       		$contact_box = [];
	       		foreach ($all_farmers as $farmer) {
	       			// filter wrong numbers
	       			if(strlen($farmer->phone) == 11){
	       				# code...
		       			array_push($contact_box, $farmer->phone);
	       			}
	       		}

	       		// return contact arrays
	       		return $contact_box;
    		}

       	}elseif($type == 'services'){

    		// get all services providers
    		$all_services = Service::all();
	       	if(count($all_services) > 0){

	       		$contact_box = [];
	       		foreach ($all_services as $service) {
	       			# code...
	       			// array_push($contact_box, $service->phone);
	       			// filter wrong numbers
	       			if(strlen($service->phone) == 11){
	       				# code...
		       			array_push($contact_box, $service->phone);
	       			}
	       		}

	       		// return contact arrays
	       		return $contact_box;
    		}

       	}elseif($type == 'buyers' ){

       		// get all buyers
    		$all_buyers = Investor::all();
	       	if(count($all_buyers) > 0){

	       		$contact_box = [];
	       		foreach ($all_buyers as $buyer) {
	       			# code...
	       			// array_push($contact_box, $buyer->phone);
	       			if(strlen($buyer->phone) == 11){
	       				# code...
		       			array_push($contact_box, $buyer->phone);
	       			}
	       		}

	       		// return contact arrays
	       		return $contact_box;
    		}
       	
       	}elseif ($type == 'all') {
       		# code...
       		$farmers 	= Farmer::all();
       		$services 	= Service::all();
       		$buyers  	= Investor::all();

       		if(count($farmers) > 0){

	       		$contact_box = [];

	       		// join the contact bus
	       		foreach ($farmers as $farmer) {
	       			# code...
	       			if(strlen($farmer->phone) == 11){
	       				# code...
		       			array_push($contact_box, $farmer->phone);
	       			}
	       		}

	       		// join the contact bus
       			if(count($services) > 0){

		       		foreach ($services as $service) {
		       			# code...
		       			if(strlen($service->phone) == 11){
		       				# code...
			       			array_push($contact_box, $service->phone);
		       			}
		       		}
	    		}

	    		// join the contact bus
	    		if(count($buyers) > 0){
		       		foreach ($buyers as $buyer) {
		       			# code...
		       			if(strlen($buyer->phone) == 11){
		       				# code...
		       				array_push($contact_box, $buyer->phone);
		       			}
		       		}
	    		}

	       		// return contact arrays
	       		return $contact_box;
    		}
       	}
    }

    // Get SMS status
    public function smsContactStatus()
    {
    	# code...
   		$total_farmers_contact 	= $this->initializeContact($type = 'farmers');
   		$total_services_contact = $this->initializeContact($type = 'services');
   		$total_buyers_contact 	= $this->initializeContact($type = 'buyers');

   		// count total contacts
		$total_farmers_contact 	= count($total_farmers_contact);
		$total_services_contact = count($total_services_contact);
		$total_buyers_contact 	= count($total_buyers_contact);   		

		// total active contact found
		$total_valid = $total_farmers_contact + $total_services_contact + $total_buyers_contact;

		// get total sms sent !
		$total_sms = $this->smslogs();

   		$data = array(
   			'status'    => 'success',
   			'message'   => 'contact loaded successfully !',
   			'farmers' 	=> $total_farmers_contact,
   			'services' 	=> $total_services_contact,
   			'buyers'   	=> $total_buyers_contact,
   			'valid_no'	=> $total_valid,
   			'total'		=> $total_sms
   		);

   		// return response
   		return response()->json($data);
    }

    public function smslogs()
    {

    	# code...
    	$total_sms 	= Smslog::all();
    	if(count($total_sms) > 0){
			$total_sent = $total_sms->sum('total');
    	}else{
    		$total_sent = 0;
    	}
    
    	// return response
    	return $total_sent;
    }

    // send sms messages
    public function sendSmsMessage(Request $request, SMS $send_sms)
    {
    	# code... sample numbers
    	// $all = array('08127160258', '09092367163');

    	// request 
    	$message = $request->body;
    	$type 	 = $request->type;
    	$single  = $request->single;
    	$multiple = $request->multiple;

    	// sender ID
    	$sender  = 'YEELDA';

    	// country code
    	$code = '+234';

    	if($type == 'single'){
    		// get contact using type
			$contact = $single;

			if(strlen($contact) !== 11){
				$data = array(
		    		'status' 	=> 'error',
		    		'message' 	=> 'invalid phone number'
		            // 'message'   => 'Test case: message has been sent !'
		    	);
			}else{
				// filter and add contry code
				$receivers = substr($contact, 1);
	            $receivers = $code.$receivers;
	            
	            // read response 
	            $send_sms  = new SMS();
	            $send_res  = $send_sms->sendMessages($receivers, $message, $sender);

	            // save total sms sent
				$total_sms_sent = 1;

				// log sms sent !
				$save_sms_count 		= new Smslog();
				$save_sms_count->total 	= $total_sms_sent;
				$save_sms_count->save();

				// log sms 
				$this->logSms($message, $receivers, $type);

	            $data = array(
		    		'status' 	=> 'success',
		    		'to'		=> $receivers,
		    		'message' 	=> $send_res
		            // 'message'   => 'Test case: message has been sent !'
		    	);
			}
    	}elseif($type == 'multiple'){
    		// send multiple messages
    		$contacts = explode(",", $multiple);

    		// send for all
			$number_list = [];
			for ($i=0; $i < count($contacts); $i++) {
				$contacts[$i] = str_replace(" ", "", $contacts[$i]);
				
				// filter and add contry code
				$receivers = substr($contacts[$i], 1);
				
	            $receivers = $code.$receivers;
	            
	            // read response 
	            $send_sms  = new SMS();
	            $send_res  = $send_sms->sendMessages($receivers, $message, $sender);

				array_push($number_list, $receivers);
			}

			// save total sms sent
			$total_sms_sent = count($contacts);

			// log sms sent !
			$save_sms_count 		= new Smslog();
			$save_sms_count->total 	= $total_sms_sent;
			$save_sms_count->save();

	        // array to string
			$receivers = implode(',', $number_list);

			// log sms 
			$this->logSms($message, $receivers, $type);

	    	$data = array(
	    		'status' 	=> 'success',
	    		'to'		=> $receivers,
	    		'message' 	=> $send_res
	            // 'message'   => 'Test case: message has been sent !'
	    	);

    	}else{
    		// get contact using type
			$contacts = $this->initializeContact($type);

			// send for all
			$number_list = [];
			for ($i=0; $i < count($contacts); $i++) {

				// filter and add contry code
				$receivers = substr($contacts[$i], 1);
	            $receivers = $code.$receivers;
	            
	            // read response 
	            $send_sms  = new SMS();
	            $send_res  = $send_sms->sendMessages($receivers, $message, $sender);

				array_push($number_list, $receivers);
			}


			// save total sms sent
			$total_sms_sent = count($contacts);

			// log sms sent !
			$save_sms_count 		= new Smslog();
			$save_sms_count->total 	= $total_sms_sent;
			$save_sms_count->save();

	        // array to string
			$receivers = implode(',', $number_list);

			// log sms 
			$this->logSms($message, $receivers, $type);

	    	$data = array(
	    		'status' 	=> 'success',
	    		'to'		=> $receivers,
	    		'message' 	=> $send_res
	            // 'message'   => 'Test case: message has been sent !'
	    	);
    	}

        // return response results
        return response()->json($data);
    }


    /*
    |--------------------------------
    | Log sent SMS
    |--------------------------------
    |
    */
    public function logSms($message, $contact, $type){

        // log sms information
        $log_sms = DB::table('s_m_s')->insert([
            'body'      => $message,
            'phone'     => $contact,
            'type'      => $type,
            'status'    => 'sent',
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ]);
    }
}
