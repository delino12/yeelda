<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Product;
use Yeelda\Seasonal;
use Yeelda\Activation;
use Yeelda\Farmer;
use Yeelda\Hint;
use Yeelda\User;
use Auth;
use DB;

class FarmersHomeController extends Controller {

	// Guard page from external login
	public function __construct() {
		$this->middleware('auth');
		$this->middleware('guard_farmer');
	}

	// all users internal pages request view
	public function dashboard() {
		$logged_email = Auth::user()->email;
		$activation_status = Activation::where([['email', $logged_email], ['status', 'active']])->first();
		if($activation_status !== null){
			return view('farmers-pages.dashboard');
 		}else {
			// user dashboard with activation msg
			$acct_status = "Account not activated!. Activation link has been sent to ".$logged_email;
			return view('farmers-pages.dashboard', compact('acct_status'));
		}
	}

	// load farmers profile page
	public function profile() {
		// get current user
		$user_id = Auth::user()->id;

		// fetch basic information
		$basic_info = DB::table('farmer_basics')->where('user_id', $user_id)->get();

		// user profile setting
		return view('farmers-pages.profile', compact('basic_info'));
	}

	// load transactions details 
	public function transactions(){
		# code...
		return view('farmers-pages.transactions');
	}

	// load farmers account 
	public function account() {
		// user account information
		// get current user
		$user_id = Auth::user()->id;

		$card_details = DB::table('farmer_accounts')->where('user_id', $user_id)->get();
		$account_details = $card_details;

		return view('farmers-pages.account', compact('account_details', 'card_details'));
	}

	// load farmers settings 
	public function setting() {
		// user settings
		return view('farmers-pages.setting');
	}

	// load farmers products
	public function products() {
		// users products and uploads
		return view('farmers-pages.upload-products');
	}

	// load farmers messaging page
	public function message() {
		// users messages and interactions
		return view('farmers-pages.messages');
	}

	// list farmer shopping cart
	public function cart() {
		// users shopping carts
		return view('farmers-pages.cart');
	}

	// load promotions
	public function promotions()
	{
		# code...
		return view('farmers-pages.referrals');
	}

	// view products
	public function viewProducts($id)
	{
		// return
        return view('farmers-pages.view-single', compact('id'));
	}


	// update farmers profile data
	public function updateProfile(Request $request) {
		// get current user
		$user_id = Auth::user()->id;

		// update user profile
		$name 		= $request->first_name . ' ' . $request->last_name;
		$gender 	= $request->gender;
		$address 	= $request->address;
		$state 		= $request->state;
		$zipcode 	= $request->zipcode;
		$office 	= $request->office;
		$mobile 	= $request->mobile;

		// update table
		$update_profile = DB::table('farmer_basics')->where('user_id', $user_id)->update([
			'name' 			=> $name,
			'gender' 		=> $gender,
			'address' 		=> $address,
			'state' 		=> $state,
			'zipcode' 		=> $zipcode,
			'office' 		=> $office,
			'mobile' 		=> $mobile,
			'updated_at' 	=> NOW(),
		]);

		$msg = "Profile has been updated successfully !";
		return redirect()->back()->with('update_status', $msg);
	}

	// save farmers profile pictures 
	public function updateAvatar(Request $request) {
		// get current user
		$user_id = Auth::user()->id;

		$user = new User();
		$data = $user->updateAvatar($user_id, $request->avatar);

		// return response.
		return response()->json($data);
	}

	// save farmers account information
	public function updateAccount(Request $request) {
		// get current user
		$user_id = Auth::user()->id;

		// re-assign form vars
		$bank_name    = $request->bank_name;
		$account_name = $request->account_name;
		$account_no   = $request->account_no;
		$account_type = $request->account_type;

		// verify information
		if($bank_name == 'none'){
			$msg = "Please select your Bank !";
			return redirect()->back()->with('update_error', $msg);
		}

		// verify information
		if($account_name == '' && $account_no == ''){
			$msg = "Please filled in your banking information !";
			return redirect()->back()->with('update_error', $msg);
		}

		// save account information to database
		$account = DB::table('farmer_accounts')->where('user_id', $user_id)->update([
			'bank_name' 	=> $bank_name,
			'account_name' 	=> $account_name,
			'account_no' 	=> $account_no,
			'account_type' 	=> $account_type,
			'updated_at' 	=> NOW(),
		]);

		if (!$account) {
			// error msg
			$msg = "Error saving account information !";
			return redirect()->back()->with('update_status', $msg);
		} else {
			$msg = "Account Information has been updated successfully !";
			return redirect()->back()->with('update_status', $msg);
		}
	}

	// save farmers card information
	public function updateCard(Request $request) {
		// get current user
		$user_id = Auth::user()->id;

		$card_type   = $request->card_type;
		$card_holder = $request->card_holder;
		$card_no     = $request->card_no;
		$card_cvv    = $request->card_cvv;

		// verify information
		if($card_holder == '' && $card_no == ''){
			$msg = "Please select your Card Details correctly !";
			return redirect()->back()->with('update_error', $msg);
		}

		$card_month = $request->exp_month;
		$card_year  = $request->exp_year;

		$exp = $card_month . ' / ' . $card_year;

		// save account information to database
		$account = DB::table('farmer_accounts')->where('user_id', $user_id)->update([
			'card_type'   => $card_type,
			'card_holder' => $card_holder,
			'card_no'     => $card_no,
			'cvv' 		  => $card_cvv,
			'exp' 	      => $exp,
			'updated_at'  => NOW(),
		]);

		if (!$account) {
			// error msg
			$msg = "Error saving account information !";

			return $msg;
		} else {
			$msg = "Card Payment Information has been updated successfully !";
			return redirect()->back()->with('update_status', $msg);
		}
	}

	// upload farmers products
	public function uploadProduct(Request $request, Product $products, Seasonal $seasonal) {
		// get current user
		$user_id = Auth::user()->id;

		// upload products
		$product_status    = $request->product_status;
		$product_name      = $request->product_name;
		$product_size_type = $request->product_size_type;
		$product_size_no   = $request->product_size_no;
		$product_price     = $request->product_price;
		$product_location  = $request->product_location;
		$product_state     = $request->product_state;
		$product_note      = $request->product_note;
		$product_image 	   = $request->product_image;

		// check state
		if($product_state == null){
			// return with error in extenstion
			$msg = "State is required!";
			return redirect()->back()->with('error_status', $msg)->withInput();
		}

		// check state
		if(empty($product_image)){
			// return with error in extenstion
			$msg = "Produce image is required!";
			return redirect()->back()->with('error_status', $msg)->withInput();
		}

		// switch check for season and already harvested
		// check handler
		if($product_status == 'seasonal'){
			// pass to season upload seasonal method
			$this->uploadProductSeasonal($request, $seasonal);
		}

		// save product
		$products 						= new Product();
		$products->user_id 				= $user_id;
		$products->product_status 		= $product_status;
		$products->product_name 		= $product_name;
		$products->product_size_type 	= $product_size_type;
		$products->product_size_no 		= $product_size_no;
		$products->product_price 		= $product_price;
		$products->product_location 	= $product_location;
		$products->product_state 		= $product_state;
		$products->product_image 		= $product_image;
		$products->product_note 		= $product_note;
		$products->save();

		// learn new products
		$this->learnNewProduct($product_name);

		$msg = $product_name." uploaded successfully!";
		return redirect()->back()->withInput()->with('update_status', $msg);
	}

	// learn new product
	public function learnNewProduct($name)
	{
		# code...
		$check_already_exist = Hint::where('name', $name)->first();
		if($check_already_exist == null){
			$save_hints 		= new Hint();
        	$save_hints->type 	= 'crop';
        	$save_hints->name 	= $name;
        	$save_hints->save();
		}
	}

	// upload farmers seasonals produce
	public function uploadProductSeasonal($request, $seasonal) {
		// get current user
		$user_id = Auth::user()->id;

		// upload products
		$product_name 		= $request->product_name;
		$product_size_type 	= $request->product_size_type;
		$product_size_no 	= $request->product_size_no;
		$product_price 		= $request->product_price;
		$product_location 	= $request->product_location;
		$product_state 		= $request->product_state;
		$product_note 		= $request->product_note;

		// duration
		$product_planning_date = $request->start_date;
		$product_delivery_date = $request->end_date;
		$product_delivery_type = 'Future Harvest, check maturity date !';

		// file data
		$product_image = $request->files;

		// save product
		$seasonal 						 = new Seasonal();
		$seasonal->user_id 				 = $user_id;
		$seasonal->product_name 		 = $product_name;
		$seasonal->product_size_type 	 = $product_size_type;
		$seasonal->product_size_no 		 = $product_size_no;
		$seasonal->product_price 		 = $product_price;
		$seasonal->product_location 	 = $product_location;
		$seasonal->product_state 		 = $product_state;
		$seasonal->product_image 		 = $product_image;
		$seasonal->product_planning_date = $product_planning_date;
		$seasonal->product_delivery_date = $product_delivery_date;
		$seasonal->product_delivery_type = $product_delivery_type;
		$seasonal->product_note 		 = $product_note;
		$seasonal->save();

		$msg = "Seasonal Products has been uploaded successfully !";
		return redirect()->back()->with('update_status', $msg);
	}
}
