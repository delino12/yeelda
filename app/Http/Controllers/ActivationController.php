<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\AccountActivation;
use Yeelda\Activation;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\User;
use Auth;
use DB;

class ActivationController extends Controller
{
    // resend activation link
    public function resendLink(Request $request)
    {
    	// activing email
    	$email = Auth::user()->email;
    	$name  = Auth::user()->name;

    	// get old link id
    	$old_link = Activation::where('email', $email)->first();
        if($old_link !== null){
            // exc delete
            $delete   = Activation::find($old_link->id);
            $delete->delete();
        }
    	
    	// set token
		$token = bcrypt($email);
		$status = 'inactive';
		// account activations status
		$activation         = new Activation();
		$activation->email  = $email;
		$activation->status = $status;
		$activation->token  = $token;
		$activation->save();
		
		// mail contents 
		$data = array(
			'name'  => $name,
			'token' => $token 
		);

		// send signed up client activation mail
		\Mail::to($email)->send(new AccountActivation($data));

		$msg = 'Activation link has been sent !';
		return redirect()->back()->with('success_msg', $msg);
    }

    // reset account review 
    public function resetAccount(Request $request)
    {
    	// request data 
    	$email = $request->email;
    	$token = $request->token;

    	// verifiy information and show users reset account password form 
    	$password_reset = DB::table('password_resets')->where([
    		'email' => $email,
    		'token' => $token
    	])->first();

    	// check if exist
    	if($password_reset == null ){
    		return view('errors.404');
    	}else{
    		return view('auth.reset-password-form', compact('email', 'token'));
    	}
    }

    // reset account password
    public function resetPassword(Request $request)
    {
    	# code...
    	// request data 
    	$email    = $request->email;
    	$token    = $request->resetToken;
    	$password = $request->password;

    	// encrypt data
    	$password = bcrypt($password);

    	// get all users modules
        $user = User::where('email', $email)->first();

        // check if users exits 
        if($user !== null){
        	// reset password
        	$update_user = User::find($user->id);
        	$update_user->password = $password;
        	$update_user->update();

            // send reset msg
            $msg = 'Account password reset successfully!';
        	$data = array(
        		'status'  => 'success',
        		'message' => $msg
        	);

        	
        }else{
            $msg = 'Account was not found! reset failed!';
            $data = array(
                'status'  => 'error',
                'message' => $msg
            );
        }

        // return response
        return response()->json($data);
    }

    // deactivating account 
    public function deactivatAccount(Request $request)
    {
        # code...
        
    }
}
