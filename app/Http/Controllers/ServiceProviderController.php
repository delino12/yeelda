<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Equipment;
use Yeelda\Account;
use Yeelda\Activation;
use Yeelda\Fertilizer;
use Yeelda\Seed;
use Auth;
use DB;
use Storage;

class ServiceProviderController extends Controller {
	/*
	|-----------------------------------------
	| GUARD MIDDLEWARE 
	|-----------------------------------------
	*/
	public function __construct() {
		$this->middleware('auth');
		$this->middleware('guard_service');
	}

	/*
	|-----------------------------------------
	| SHOW TRADE VIEW
	|-----------------------------------------
	*/
	public function trade() {
		# go Market
		$commodities = array(
			'Rice', 'Wheat', 'Soybeans',
			'Maize', 'Cotton', 'Cassava', 'Palm oil ',
			'Beans', 'Peanuts', 'Rapeseed', 'Rubber',
			'Yams', 'Peaches', 'Cacao', 'Sugar', 'Watermelons',
			'Carrots', 'Coconuts', 'Almonds', 'Lemons',
			'Strawberries', 'Walnuts',
		);

		// market commodities
		return view("market.live", compact('commodities'));
	}

	/*
	|-----------------------------------------
	| SHOW EQUIPMENT
	|-----------------------------------------
	*/
	public function equipments() {
		// market equipments
		return view("market.equipments");
	}

	/*
	|-----------------------------------------
	| SHOW SERVICES PROVIDERS DASHBOARD
	|-----------------------------------------
	*/
	public function dashboard() {

		$logged_email = Auth::user()->email;
		$activation_status = Activation::where([['email', $logged_email], ['status', 'active']])->first();
		if($activation_status !== null){
			return view('service-pages.dashboard');
 		}else {
			// user dashboard with activation msg
			$acct_status = "Account not activated!. Activation link has been sent to ".$logged_email;
			return view('service-pages.dashboard', compact('acct_status'));
		}
	}

	/*
	|-----------------------------------------
	| SHOW SERVICES PROVIDERS PROFILE
	|-----------------------------------------
	*/
	public function profile() {
		// user profile setting
		return view('service-pages.profile');
	}

	/*
	|-----------------------------------------
	| SHOW SERVICES PROVIDERS ACCOUNT SETTINGS
	|-----------------------------------------
	*/
	public function account() {
		// user account information
		$user_id = Auth::user()->id;

		$card_details 		= DB::table('service_accounts')->where('user_id', $user_id)->get();
		$account_details 	= $card_details;
		return view('service-pages.account', compact('card_details', 'account_details'));
	}

	/*
	|-----------------------------------------
	| SHOW SERVICES PROVIDER USER SETTINGS
	|-----------------------------------------
	*/
	public function setting() {
		// user settings
		return view('service-pages.setting');
	}

	/*
	|-----------------------------------------
	| SHOW SERVICES PROVIDER EQUIPMENT INPUTS
	|-----------------------------------------
	*/
	public function equipment() {
		// users products and uploads
		return view('service-pages.equipment');
	}

	/*
	|-----------------------------------------
	| SHOW MESSAGING SYSTEM
	|-----------------------------------------
	*/
	public function message() {
		// users messages and interactions
		return view('service-pages.messages');
	}

	/*
	|-----------------------------------------
	| SHOW HIRING REQUEST
	|-----------------------------------------
	*/
	public function showRequest()
	{
		# show hiring request
		return view('service-pages.hiring-service');
	}

	/*
	|-----------------------------------------
	| SHOW UPLOAD EQUIPMENT SECTION
	|-----------------------------------------
	*/
	public function uploadEquipment(Request $request){
		// upload equipments
		$equipment_name 	=  $request->equipment_name;
		$equipment_desc 	=  $request->equipment_desc;
		$equipment_no       =  $request->equipment_no;
		$equipment_price 	=  $request->equipment_price;
		$equipment_location =  $request->equipment_location;
		$equipment_state 	=  $request->equipment_state;
		$equipment_delivery =  $request->equipment_delivery_type;
		$equipment_note 	=  $request->equipment_note;

		// make equipment available
		$equipment_status = "available";

		// file uploading system
		$filename =  $request->files;

		// dd($request->all());

		# upload images
		$images_arr = array();
		$images_name = array();
		foreach ($_FILES['files']['name'] as $key => $val) {
			//upload and stored images
			$target_dir = "../public/uploads/equipments/";
			$ext = substr($_FILES['files']['name'][$key], strpos($_FILES['files']['name'][$key], '.'));

			// dd($ext);
			if ($ext !== ".jpg") {
				// return with error in extenstion
				$msg = "Invalid image type, please upload a valid image";
				return redirect()->back()->with('error_status', $msg);
			} else {
				$new_name = time() . rand(000, 999) . $ext;
				$target_file = $target_dir . $new_name;
				if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $target_file)) {
					$images_arr[] = $target_file;
					array_push($images_name, $new_name);
				}
			}
		}

		# set files name
		$packages_images = $images_name;

		// convert to strings all images in arrays
		$packages_images = implode(',', $packages_images);

		// using the DB driver
		$equipments = DB::table('equipments')->insert([
			'user_id'                 => Auth::user()->id,
			'equipment_name'          => $equipment_name,
			'equipment_descriptions'  => $equipment_desc,
			'equipment_no'            => $equipment_no,
			'equipment_status'        => $equipment_status,
			'equipment_price'         => number_format($equipment_price, 2),
			'equipment_address'       => $equipment_location,
			'equipment_state'         => $equipment_state,
			'equipment_image'         => $packages_images,
			'equipment_delivery_type' => $equipment_delivery,
			'equipment_note'          => $equipment_note,
			'created_at'			  => NOW(),
			'updated_at'			  => NOW()
		]);

		// return with error in extenstion
		$msg = $equipment_name." has been added successfully !";
		return redirect()->back()->with('update_status', $msg);
	}

	/*
	|-----------------------------------------
	| FETCH ALL EQUIPMENT
	|-----------------------------------------
	*/
	public function loadEquipments(){
		// get logged users
		$id = Auth::user()->id;

		// fetch equipment
		$equipments = DB::table('equipments')->where('user_id', $id)->get();

		// equipment box
		$equipment_box = [];
		foreach ($equipments as $tools) {

			// check if tools is hired free
			if($tools->equipment_status == 'available'){
				# code...
				$status = "Free";
			}else{
				$status = "Occupied";
			}

			$tools->equipment_price = str_replace(",", "", $tools->equipment_price);

			$total = $tools->equipment_no * (int)$tools->equipment_price;

			// status in used
			$data = array(
				"id"					  =>  $tools->id,
				"user_id"                 =>  $tools->user_id,
				"equipment_name"          =>  $tools->equipment_name,
				"equipment_descriptions"  =>  $tools->equipment_descriptions,
				"equipment_no"            =>  $tools->equipment_no,
				"equipment_status"        =>  $status,
				"total"                   =>  number_format($total, 2),
				"equipment_price"         =>  number_format($tools->equipment_price, 2),
				"equipment_address"       =>  $tools->equipment_address,
				"equipment_state"         =>  $tools->equipment_state,
				"equipment_image"         =>  $tools->equipment_image,
				"equipment_delivery_type" =>  $tools->equipment_delivery_type,
				"equipment_note"          =>  $tools->equipment_note,
				"created_at"              =>  $tools->created_at,
				"updated_at"              =>  $tools->updated_at
			);

			array_push($equipment_box, $data);
				
		}

		return response()->json($equipment_box);
	}

	/*
	|-----------------------------------------
	| FETCH SINGLE EQUIPMENT
	|-----------------------------------------
	*/
	public function loadSingleEquipment(Request $request){
		$id = $request->id;

		// fetch equipment
		$equipments = DB::table('equipments')->where('id', $id)->get();

		// equipment box
		$equipment_box = [];
		foreach ($equipments as $tools) {

			// check if tools is hired free
			if($tools->equipment_status == 'available'){
				# code...
				$status = "Free";
			}else{
				$status = "Occupied";
			}

			$tools->equipment_price = str_replace(",", "", $tools->equipment_price);

			$total = (int)$tools->equipment_price * $tools->equipment_no;

			// status in used
			$data = array(
				"id"					  =>  $tools->id,
				"user_id"                 =>  $tools->user_id,
				"equipment_name"          =>  $tools->equipment_name,
				"equipment_descriptions"  =>  $tools->equipment_descriptions,
				"equipment_no"            =>  $tools->equipment_no,
				"equipment_status"        =>  $status,
				"total"                   =>  number_format($total, 2),
				"equipment_price"         =>  number_format($tools->equipment_price, 2),
				"equipment_address"       =>  $tools->equipment_address,
				"equipment_state"         =>  $tools->equipment_state,
				"equipment_image"         =>  $tools->equipment_image,
				"equipment_delivery_type" =>  $tools->equipment_delivery_type,
				"equipment_note"          =>  $tools->equipment_note,
				"created_at"              =>  $tools->created_at,
				"updated_at"              =>  $tools->updated_at
			);

			array_push($equipment_box, $data);
				
		}

		return response()->json($equipment_box);
	}

	/*
	|-----------------------------------------
	| SHOW VIEW SINGLE EQUIPMENT
	|-----------------------------------------
	*/
	public function viewEquipment(Request $request){
		// get logged users
		$equip_id 	= $request->id;
		$equipment 	= DB::table('equipments')->where('id', $equip_id)->first();

		return view('service-pages.view-equipment', compact('equip_id', 'equipment'));
	}

	/*
	|-----------------------------------------
	| SHOW SINGLE FERTILIZER
	|-----------------------------------------
	*/
	public function showFertDetails(Request $request, $id){
		// body
		$fertilizer = Fertilizer::where("id", $id)->first();

		return view('service-pages.view-fertilizer', compact('fertilizer'));
	}

	/*
	|-----------------------------------------
	| SHOW SINGLE SEEDS
	|-----------------------------------------
	*/
	public function showSeedsDetails(Request $request, $id){
		// body
		$seeds = Seed::where("id", $id)->first();
		
		return view('service-pages.view-seeds', compact('seeds'));
	}
}
