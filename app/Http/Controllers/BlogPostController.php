<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Comment;
use Yeelda\Blog;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Auth;
use DB;

class BlogPostController extends Controller
{
    // load blog post 
    public function loadBlogPosts()
    {
    	# code...
    	$blog_post = Blog::orderBy('id', 'desc')->take('2')->get();

    	$blog_box = [];
    	foreach ($blog_post as $post) {
    		// filter body into paragraphs
    		// $body = str_replace('', ' <br /><br />', $post->body);
            // get comments 
            $blog_comments = Comment::where('blog_id', $post->id)->take('5')->get();
            if(count($blog_comments) > 0){
                $comment_box = [];
                foreach ($blog_comments as $comments) {
                    # code...
                    $data = array(
                        'id'      => $comments->id,
                        'blog_id' => $comments->blog_id,
                        'name'    => $comments->user_name,
                        'email'   => $comments->user_email,
                        'body'    => $comments->body,
                        'like'    => $comments->like,
                        'unlike'  => $comments->unlike,
                        'date'    => $comments->created_at->diffForHumans() 
                    );

                    array_push($comment_box, $data);
                }
            }else{
                $comment_box = [];
            }
            
    		# code...
    		$data = array(
    			'id'        => $post->id,
    			'by'        => $post->by,
				'title'     => $post->title,
				'body'      => $post->body,
				'image'     => $post->docs,
				'like'      => $post->like,
				'unlike'    => $post->unlike,
                'comments'  => $comment_box,
				'date'      => $post->created_at->diffForHumans()
    		);

    		array_push($blog_box, $data);
    	}

    	// return blog post
    	return response()->json($blog_box);
    }

    // load blog post 
    public function loadBlogPostsRecent()
    {
    	# code...
    	$blog_post = Blog::orderBy('id', 'desc')->take('5')->get();
        if(count($blog_post) > 0){
            $blog_box = [];
            foreach ($blog_post as $post) {
                // filter body into paragraphs
                $body = str_replace('.', '<br />', $post->body);

                # code...
                $data = array(
                    'id'        => $post->id,
                    'by'        => $post->by,
                    'title'     => $post->title,
                    'body'      => $body,
                    'image'     => $post->docs,
                    'like'      => $post->like,
                    'unlike'    => $post->unlike,
                    'date'      => $post->created_at->diffForHumans(),
                    'last_updated' => $post->created_at->toDateString()
                );

                array_push($blog_box, $data);
            }
        }else{
            $blog_box = [];
        }

    	// return blog post
    	return response()->json($blog_box);
    }

    // like post
    public function like($blog_id)
    {
    	# code...
    }

    // unlike post
    public function unlike($blog_id)
    {
    	# code...
    }

    // post Comments
    public function postComment(Request $request)
    {
        # code...
        if(Auth::guard('farmer')->check()){
            // get info if farmer
            $name  = Auth::guard('farmer')->user()->name;
            $email = Auth::guard('farmer')->user()->email;
        }elseif(Auth::guard('investor')->check()){
            // get info if buyer
            $name  = Auth::guard('investor')->user()->name;
            $email = Auth::guard('investor')->user()->email;
        }elseif(Auth::guard('sp')->check()){
            // get info if service
            $name  = Auth::guard('sp')->user()->name;
            $email = Auth::guard('sp')->user()->email;
        }else{
            $data = array(
                'status'  => 'error',
                'message' => 'Login or Signup to post comments !'
            );
            return response()->json($data);
        }

        // create new comment
        $comments             = new Comment();
        $comments->blog_id    = $request->blog_id;
        $comments->user_name  = $name;
        $comments->user_email = $email;
        $comments->body       = $request->body;
        $comments->like       = 0;
        $comments->unlike     = 0;
        $comments->save();

        // saved 
        $data = array(
            'status'  => 'success',
            'message' => 'comment posted !'
        );
        return response()->json($data);
    }
}
