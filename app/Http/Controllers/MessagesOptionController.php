<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Message;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\Sent;
use Yeelda\Draft;
use Yeelda\Trash;
use Auth;
use DB;

class MessagesOptionController extends Controller
{
    // load all sent message
    public function loadSent($email)
    {
    	// get sents 
    	$sents_messages = Sent::where('from', $email)->orderBy('id', 'desc')->get();
    	$sents_box = [];

    	// get all sent 
    	foreach ($sents_messages as $sents) {
    		# code...
    		$data = array(
    			'id'      => $sents->id,
    			'from'    => $sents->from,
				'to'      => $sents->to,
				'subject' => $sents->subject,
				'body'    => $sents->body,
				'docs'    => $sents->docs,
				'status'  => $sents->status,
				'date'    => $sents->created_at->diffForHumans()
    		);

    		// push data
    		array_push($sents_box, $data);
    	}
    	return response()->json($sents_box);
    }

    // load all draft message
    public function loadDraft($email)
    {
    	// get sents 
    	$drafts_messages = Draft::where('from', $email)->orderBy('id', 'desc')->get();
    	$draft_box = [];

    	// get all sent 
    	foreach ($drafts_messages as $drafts) {
    		# code...
    		$data = array(
    			'id'      => $drafts->id,
    			'from'    => $drafts->from,
				'to'      => $drafts->to,
				'subject' => $drafts->subject,
				'body'    => $drafts->body,
				'docs'    => $drafts->docs,
				'status'  => $drafts->status,
				'date'    => $drafts->created_at->diffForHumans()
    		);

    		// push data
    		array_push($draft_box, $data);
    	}

    	return response()->json($draft_box);
    }

    // save draft
    public function saveDraft(Request $request)
    {
    	$from   = $request->from;
    	$to     = $request->to;
    	$subj   = $request->subj;
    	$body   = $request->body;
    	$status = 'unread';

    	$drafts 	     = new Draft();
    	$drafts->from    = $from;
    	$drafts->to      = $to;
    	$drafts->subject = $subj;
    	$drafts->body    = $body;
    	$drafts->status  = $status;
    	$drafts->save();


    	// return data
    	$data = array(
    		'status'  => 'success',
    		'message' => 'Draft message saved !'
    	);

    	return response()->json($data);
    }

    // load all trash message
    public function loadTrash($email)
    {
    	// get sents 
    	$trash_messages = Trash::where('from', $email)->orderBy('id', 'desc')->get();
    	$trash_box = [];

    	// get all sent 
    	foreach ($trash_messages as $trash) {
    		# code...
    		$data = array(
    			'id'      => $trash->id,
    			'from'    => $trash->from,
				'to'      => $trash->to,
				'subject' => $trash->subject,
				'body'    => $trash->body,
				'docs'    => $trash->docs,
				'status'  => $trash->status,
				'date'    => $trash->created_at->diffForHumans()
    		);

    		// push data
    		array_push($trash_box, $data);
    	}

    	return response()->json($trash_box);
    }
}
