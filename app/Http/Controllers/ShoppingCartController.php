<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Events\AddToCart;
use Yeelda\Cart;
use Yeelda\Product;
use Yeelda\User;
use Auth;

class ShoppingCartController extends Controller
{
    // Add to carts
    public function addToCart(Request $request)
    {
    	# code...
    	$item_id     = $request->item_id;
    	$item_type   = $request->item_type;
    	$item_status = 'pending';

        if(Auth::check()){
          $client_email  = Auth::user()->email;
            $client_id     = Auth::user()->id;  
        }else{
            return $data = [
                'status'    => 'error',
                'message'   => 'Login to continue shopping on YEELDA'
            ];
        }

        // check to avoid self purchase
        $product = Product::where("id", $item_id)->first();
        if($product->user_id == $client_id){
            $data = [
                'status'    => 'error',
                'message'   => 'You can not purchase this produce!'
            ];
        }else{
            // client ip address
            $client_ip = \Request::ip();

            // save item
            $add_carts               = new Cart();
            $add_carts->client_email = $client_email;
            $add_carts->client_ip    = $client_ip;
            $add_carts->item_id      = $item_id;
            $add_carts->item_type    = $item_type;
            $add_carts->item_status  = $item_status;
            $add_carts->save();

            // Fire Event
            \Event::fire(new AddToCart());

            // response data
            $data = [
                'status'  => 'success',
                'message' => 'item added to cart'
            ];
        }

    	// return response 
    	return response()->json($data);
    }

    // load shopping Carts
    public function loadShoppingCart(Request $request)
    {
        // request mail 
        $email = $request->email;
        
        // scan carts 
        $carts = Cart::where('client_email', $email)->get();
        $total = $carts->count();

        // data to array 
        $data = array(
            'total' => $total
        );

        // return json response
        return response()->json($data);
    }

    // load shopping list
    public function shoppingBasket(Request $request)
    {
        # code...
        $user_email = $request->email;
        $carts = Cart::where('client_email', $user_email)->get();

        $shopping_basket = [];
        foreach($carts as $item){
            // get product information
            $product = Product::where('id', $item->item_id)->first();

            // get total 
            $amount = $product->product_size_no * $product->product_price;
            // $total  = $product->product_size_no * $amount;

            // data to array
            $data = array(
                'id'        => $item->id,
                'item_id'   => $item->item_id,
                'name'      => $product->product_name, 
                'size_type' => $product->product_size_type, 
                'size_no'   => $product->product_size_no, 
                'price'     => $product->product_price, 
                'amount'    => $amount,
                'total'     => $amount,
                'image'     => $product->product_image, 
                'date'      => $item->created_at->diffForHumans() 
            );

            // push to basket 
            array_push($shopping_basket, $data);
        }

        // cast to collection
        $shopping_basket = collect($shopping_basket);

        // sum total all 
        $sum_total = $shopping_basket->sum('total');

        // filter array
        $filter_data = array(
            'total'   => $sum_total,
            'details' => $shopping_basket
        );

        // return response on carts list
        return response()->json($filter_data);
    }

    // remove item 
    public function removeItem($id)
    {
        # code...
        $carts = Cart::find($id);
        $carts->delete();

        return redirect()->back()->with('remove_status', 'Item remove successfully !');
    }
}
