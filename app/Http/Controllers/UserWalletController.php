<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Wallet;
use Yeelda\User;
use Auth;

class UserWalletController extends Controller
{
    /*
    |-----------------------------------------
    | GET WALLET BALANCE
    |-----------------------------------------
    */
    public function getWalletBalance(){
    	// body
        $user_id    = Auth::user()->id;

        $wallet     = new Wallet();
        $data       = $wallet->getUserWallet($user_id);

        // return response.
        return response()->json($data);
    }
}
