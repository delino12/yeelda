<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function trade()
    {

        $commodities = ['Rice', 'Wheat', 'Soybeans', 
            'Maize', 'Cotton', 'Cassava', 'Palm oil ', 
            'Beans', 'Peanuts', 'Rapeseed', 'Rubber', 
            'Yams', 'Peaches', 'Cacao', 'Sugar', 'Watermelons', 
            'Carrots', 'Coconuts', 'Almonds', 'Lemons', 
            'Strawberries', 'Walnuts'];

        return view('market.live', compact('commodities'));
    }


    public function equipments()
    {
        return view("market.equipments");
    }
}
