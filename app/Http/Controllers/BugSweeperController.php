<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\PaymentSentMail;
use Yeelda\TempUser;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\Account;
use Yeelda\Equipment;
use Yeelda\Transaction;
use Yeelda\Payment;
use Yeelda\Product;
use Yeelda\Farmer;
use Yeelda\FarmerBasic;
use Yeelda\Service;
use Yeelda\Investor;
use Yeelda\Crop;
use Yeelda\SMS;
use Yeelda\Admin;
use Yeelda\Agent;
use Yeelda\AdminMessage;
use Yeelda\Stamp;
use Yeelda\Fertilizer;
use Yeelda\Seed;
use Carbon\Carbon;
use DB;

class BugSweeperController extends Controller
{
    /*
    |-----------------------------------------
    | CLEAN ODCA DATA
    |-----------------------------------------
    */
    public function cleanODCA(){
    	// body
    	$farmers = Farmer::all();
    	if(count($farmers) > 0){
    		$total = 0;
    		$counter = 0;
    		foreach ($farmers as $fa) {
    			// check if user has basic information
    			$total++;

    			$basic_info = FarmerBasic::where('user_id', $fa->id)->first();
    			if($basic_info == null){

    				$counter++;
    				
					$delete_record = Farmer::find($fa->id);
					$delete_record->delete();

    			}else{
    				// reverse case
					$farmers_basic = FarmerBasic::all();
					if(count($farmers_basic) > 0){
						foreach ($farmers_basic as $fb) {
							$farmer_record = Farmer::where("id", $fb->user_id)->first();
							if($farmer_record == null){
								$counter++;
								$delete_record = FarmerBasic::find($fb->id);
								$delete_record->delete();
							}
						}
					}
    			}
    		}

    		if($counter > 0){
				$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> $total.' House scanning completed!, '.$counter.' deleted successfully!',
	    		];
    		}else{
    			$data = [
	    			'status' 	=> 'info',
	    			'message' 	=> $total.' Scanned was successful! nothing to clean!'
	    		];
    		}
    	}else{

    		$data = [
    			'status' 	=> 'info',
    			'message' 	=> 'Nothing to clean!'
    		];
    	}

    	// return response.
    	return response()->json($data);
    }


    /*
    |-----------------------------------------
    | DELETE UNWANTED DATA
    |-----------------------------------------
    */
    public function deleteUnwanted($user_id, $account_type){
    	if($account_type == 'farmer'){
			// body
	    	$delete_record = Farmer::find($user_id);
	    	if($delete_record !== null){
				$delete_record->delete();
	    	}
    	}elseif($account_type == 'service'){
    		// body
	    	$delete_record = Service::find($user_id);
	    	if($delete_record !== null){
				$delete_record->delete();
	    	}
    	}elseif($account_type == 'buyer'){
    		// body
	    	$delete_record = Investor::find($user_id);
	    	if($delete_record !== null){
				$delete_record->delete();
	    	}
    	}
    }
}
