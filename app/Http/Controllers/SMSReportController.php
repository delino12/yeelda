<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Smslog;

class SMSReportController extends Controller
{
    // get sms total
    public function logs(Request $request)
    {
    	$api_token 	= $request->api_token;
    	$api_secret = $request->api_secret;

    	if($this->authToken($api_token, $api_secret)){
	    	# code...
	    	$total_sms 	= Smslog::all();
	    	$total_sent = $total_sms->sum('total');

	    	$data = array(
	    		'status'  => 'success',
	    		'message' => [
	    			'error' => 0,
	    			'body' 	=> 'Total SMS: '.$total_sent
	    		]
	    	);
    	}else{
    		$data = array(
	    		'status'  => 'error',
	    		'message' => [
	    			'error' => 101,
	    			'body'  => 'Invalid token/secret'
	    		]
	    	);
    	}

    	// return response
    	return response()->json($data);
    }

    // check secret 
    public function authToken($token, $secret)
    {
    	# verify client access
    	if($token == 'abcd1234' && $secret == 'secret'){
    		return true;
    	}else{
    		return false;
    	}
    }
}
