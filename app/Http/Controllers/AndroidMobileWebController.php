<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

use Yeelda\TempSeasonal;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\SMS;
use Yeelda\SmsQueue;
use Yeelda\ImageHouse;

use Carbon\Carbon;
use Image;
use Storage;
use Cloudinary;
use DB;

class AndroidMobileWebController extends Controller
{
    /*
    |-----------------------------------------
    | Sync Mobile Application Data
    |-----------------------------------------
    */
    public function syncData(Request $request){
    	// body
    	// request data
        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $email          = $request->email;
        $phone          = $request->phone;
        $avatar         = $request->avatar;
        $state          = $request->state;
        $lga            = $request->lga;
        $ward           = $request->ward;
        $language       = $request->language;
        $accountType    = $request->accountType;
        $address        = $request->address;
        $prev_crop_p    = $request->pcp;
        $prev_crop_h    = $request->pch;
        $prev_yield_h   = $request->pyh;
        $agent_id       = $request->agent_id;
        $gender         = $request->gender;
        $reg_date       = $request->reg_date;

        // account option section
        $farmSizeNo         = $request->farmSizeNo;
        $farmSizetype       = $request->farmSizetype;
        $farmSizeCrop       = $request->farmSizeCrop;
        
        $storageCapicity    = $request->storageCapicity;
        $typeOfStroage      = $request->typeOfStroage;

        $companyName        = $request->companyName;
        $comPerferPurchase  = $request->comPerferPurchase;

        // verify phone number
        if(!empty($phone) && strlen($phone) > 10){
            $code = '+234';
            $phone = substr($phone, 1);
            $phone = $code.$phone;

            // send SMS
            $sms_msg = "Welcome to YEELDA. We connect farmers, buyers and services providers. visit www.yeelda.com Today.";

            // log sms
            $this->logSms($sms_msg, $phone, $accountType);
            $this->queueSms($sms_msg, $phone);
        }else{
            $code = 401;
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid phone number!'
            ];

            // return response.
            return response()->json($data);
        }

        // save account for Temp Farmers
        if($accountType == 'farmer'){
            $new_farmer                     = new TempFarmer();
            $new_farmer->firstname          = $firstname;
            $new_farmer->lastname           = $lastname;
            $new_farmer->email              = $email;
            $new_farmer->phone              = $phone;
            $new_farmer->avatar             = $avatar;
            $new_farmer->state              = $state;
            $new_farmer->lga                = $lga;
            $new_farmer->ward               = $ward;
            $new_farmer->language           = $language;
            $new_farmer->accountType        = $accountType;
            $new_farmer->address            = $address;
            $new_farmer->farmSizeNo         = $farmSizeNo;
            $new_farmer->farmSizetype       = $farmSizetype;
            $new_farmer->farmSizeCrop       = $farmSizeCrop;
            $new_farmer->prevCropPlanted    = $prev_crop_p;
            $new_farmer->prevCropHarvested  = $prev_crop_h;
            $new_farmer->prevYieldHarvested = $prev_yield_h;
            $new_farmer->agent_id           = $agent_id;
            $new_farmer->gender             = $gender;
            $new_farmer->date_captured      = $reg_date;
            $new_farmer->save();

            // response
            $code = 200;
            $data = [
                'status'    => 'success',
                'message'   => '1 new Farmer added successfully!',
                'user_id'   => $new_farmer->id 
            ];
        }elseif($accountType == 'service'){

            $new_service                 = new TempService();
            $new_service->firstname      = $firstname;
            $new_service->lastname       = $lastname;
            $new_service->email          = $email;
            $new_service->gender         = $gender;
            $new_service->phone          = $phone;
            $new_service->avatar         = $avatar;
            $new_service->state          = $state;
            $new_service->lga            = $lga;
            $new_service->ward           = $ward;
            $new_service->language       = $language;
            $new_service->accountType    = $accountType;
            $new_service->address        = $address;
            $new_service->storageCapicity= $storageCapicity;
            $new_service->typeOfStroage  = $typeOfStroage;
            $new_service->agent_id       = $agent_id;
            $new_service->gender         = $gender;
            $new_service->date_captured  = $reg_date;
            $new_service->save();

            // clean base64 Image
            $this->filteredBase64Image($new_service->user_id, $avatar);
            
            // response
            $code = 200;
            $data = [
                'status'    => 'success',
                'message'   => '1 new Services Provider added successfully !',
                'user_id'   => $new_farmer->id 
            ];
        }elseif($accountType == 'buyer'){
            
            $new_buyer                      = new TempBuyer();
            $new_buyer->firstname           = $firstname;
            $new_buyer->lastname            = $lastname;
            $new_buyer->email               = $email;
            $new_buyer->gender              = $gender;
            $new_buyer->phone               = $phone;
            $new_buyer->avatar              = $avatar;
            $new_buyer->state               = $state;
            $new_buyer->lga                 = $lga;
            $new_buyer->ward                = "None";
            $new_buyer->language            = $language;
            $new_buyer->accountType         = $accountType;
            $new_buyer->address             = $address;
            $new_buyer->companyName         = $companyName;
            $new_buyer->comPerferPurchase   = $comPerferPurchase;
            $new_buyer->agent_id            = $agent_id;
            $new_buyer->gender              = $gender;
            $new_buyer->date_captured       = $reg_date;
            $new_buyer->save();

            // clean base64 Image
            $this->filteredBase64Image($new_buyer->user_id, $avatar);

            // response 
            $code = 200;
            $data = [
                'status'    => 'success',
                'message'   => '1 new Buyer added successfully !',
                'user_id'   => $new_farmer->id
            ];
        }else{
        	$code = 401;
            $data = [
                'status'    => 'error',
                'message'   => 'Failed to add user, Please specify User Signup Account Type !'
            ];
        }

        // save user
        return $this->jsonResponse($code, $data);
    }

    /*
    |-----------------------------------------
    | check already exist
    |-----------------------------------------
    |
    */
    public function checkExisting($email, $phone){

        // log sms information
        $log_sms = DB::table('s_m_s')->insert([
            'body'      => $message,
            'phone'     => $contact,
            'type'      => $type,
            'status'    => 'sent',
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ]);
    }

    /*
    |-----------------------------------------
    | Log sent SMS
    |-----------------------------------------
    |
    */
    public function logSms($message, $contact, $type){

        // log sms information
        DB::table('s_m_s')->insert([
            'body'      => $message,
            'phone'     => $contact,
            'type'      => $type,
            'status'    => 'sent',
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ]);
    }

    /*
    |-----------------------------------------
    | Add SMS to Waiting
    |-----------------------------------------
    |
    */
    public function queueSms($message, $mobile){
        $new_queue = new SmsQueue();
        $new_queue->addToQueue($mobile, $message);
    }

    /*
    |-----------------------------------------
    | Clear SMS Queue Messages
    |-----------------------------------------
    */
    public function clearQueue(){
        // body
        $sms_queue = new SmsQueue();
        $sms_queue->processQueueMessage();

        // job dispatch.
        return response()->json(['status' => 'success', 'message' => 'Job dispatch']);
    }

    /*
    |-----------------------------------------
    | Custom Json response
    |-----------------------------------------
    */
    public function jsonResponse($code, $data){
    	// body
        header("Access-Control-Allow-Origin: *");
    	return response($data, $status = $code, $headers = [
            'Content-Type' => 'application/json'
        ]);
    }

    /*
    |-----------------------------------------
    | CLEAN ODCA CAPTURED IMAGES
    |-----------------------------------------
    */
    public function filteredBase64Image($user_id, $base64_image){
        $options = array(
            "folder"            => "yeelda-odca-images",
            "upload_preset"     => "cv2pq27t"
        );
        
        $farmer = TempFarmer::where('id', $user_id)->first();
        if($farmer !== null){
            // get user name
            $user_names  = $farmer->firstname.'_'.$farmer->lastname.'_'.$farmer->id;

            if(!$this->already_clean($farmer->id)){
                if($farmer->avatar !== null){

                    if($this->downloadBase64Images($base64_image, $user_id, $user_names)){

                        $new_image = public_path("tmp/").$user_names.'.jpeg';

                        \Cloudinary::config(array(
                            "cloud_name"    => "delino12",
                            "api_key"       => "632817215533885",
                            "api_secret"    => "nZaO84cvr14RidW6sQ8gd6clzic"
                        ));

                        $upload_response    = \Cloudinary\Uploader::upload($new_image, $options);
                        $scanned_cloud_url  = $upload_response['url']; // cloud response

                        // process image convert to image url
                        $update_temp_farmers            = TempFarmer::find($farmer->id);
                        $update_temp_farmers->avatar    = $scanned_cloud_url;
                        $update_temp_farmers->update();

                        // add images to url
                        $this->addCleanedImage($farmer->id, $scanned_cloud_url);
                    }
                }
            }
        }
    }

    /*
    |-----------------------------------------
    | STORE TO IMAGE HOUSE
    |-----------------------------------------
    */
    public function addCleanedImage($temp_user_id, $temp_user_image){
        // body
        $new_temp_scanned = new ImageHouse();
        $new_temp_scanned->addTempImage($temp_user_id, $temp_user_image);
    }

    /*
    |-----------------------------------------
    | DONWLOAD IMAGE TO JPG
    |-----------------------------------------
    */
    public function downloadBase64Images($base64_string, $user_id, $user_names){
        $file_path = public_path('tmp/');
        $file_data = str_replace("data:image/jpeg;base64,", "", $base64_string);
        $file_data = str_replace(" ", "+", $file_data); // filtered strings
        $file_name = $file_path.$user_names.'.jpeg'; //generating unique file name; 
        $image_data = base64_decode($file_data);
        if(file_put_contents($file_name, $image_data)){
            $new_image_name = "tmp/".$user_names.'.jpeg';

            // resize image
            $this->resize_image($new_image_name, null, 400);

            return true;
        }else{
            return false;
        }
    }

    /*
    |-----------------------------------------
    | RESIZE IMAGE 
    |-----------------------------------------
    */
    public function resize_image($image_path, $image_width, $image_height) {
        $img = Image::make(public_path($image_path));
        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img->resize($image_width, $image_height, function ($constraint) {
            $constraint->aspectRatio();
        });
        // replace
        $img->save($image_path);
    }

    /*
    |-----------------------------------------
    | GET ALL ALREADY FIXED IMAGES
    |-----------------------------------------
    */
    public function alreadyScanned(){
        // body
        $data = ImageHouse::all();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD ALL UNSCANNED IMAGES
    |-----------------------------------------
    */
    public function unscannedUsers(){
        // body
        $data = DB::table('temp_farmers')->select('id')->get();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | ADD TEMP PRODUCES
    |-----------------------------------------
    */
    public function addTempProduce(Request $request){
        // body
        $add_produce    = new TempSeasonal();
        $data           = $add_produce->addNewProduce($request);

        // return response.
        return response()->json($data);
    }
}
