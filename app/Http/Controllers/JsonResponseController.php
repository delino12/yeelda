<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Events\ReplyMessage;
use Yeelda\Events\NewMessage;
use Yeelda\Mail\EnquiryMail;
use Yeelda\Notification;
use Yeelda\Message;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\Product;
use Yeelda\Sent;
use Yeelda\Payment;
use Yeelda\Transaction;
use Yeelda\Stamp;
use Yeelda\Crop;
use Yeelda\User;
use Yeelda\ViewAccess;
use Yeelda\Seasonal;
use Auth;
use DB;

class JsonResponseController extends Controller
{
    /*
    |-----------------------------------------
    | SHOW HIRING REQUEST VIEW
    |-----------------------------------------
    */
    public function requestHiring($id){
        return view('market.single-equipments', compact('id'));
    }

    /*
    |-----------------------------------------
    | LOAD SINGLE HIRING SERVICES
    |-----------------------------------------
    */
    public function loadSingleHiring(Request $request)
    {
        $id = $request->id;

        // fetch equipment
        $equipments = DB::table('equipments')->where('id', $id)->get();

        // equipment box
        $equipment_box = [];
        foreach ($equipments as $tools) {

            // check if tools is hired free
            if($tools->equipment_status == 'available'){
                # code...
                $status = "Free";
            }else{
                $status = "Occupied";
            }

            //get product owner
            $provider = DB::table('services')->where('id', $tools->user_id)->first();

            // calculate total cost
            $total = $tools->equipment_no * $tools->equipment_price;

            // status in used
            $data = array(
                "id"                      =>  $tools->id,
                "user_id"                 =>  $tools->user_id,
                "owner_name"              =>  $provider->name,
                "owner_email"             =>  $provider->email,
                "owner_contact"           =>  $provider->phone,
                "equipment_name"          =>  $tools->equipment_name,
                "equipment_descriptions"  =>  $tools->equipment_descriptions,
                "equipment_no"            =>  $tools->equipment_no,
                "equipment_status"        =>  $status,
                "total"                   =>  $total,
                "equipment_price"         =>  $tools->equipment_price,
                "equipment_address"       =>  $tools->equipment_address,
                "equipment_state"         =>  $tools->equipment_state,
                "equipment_image"         =>  $tools->equipment_image,
                "equipment_delivery_type" =>  $tools->equipment_delivery_type,
                "equipment_note"          =>  $tools->equipment_note,
                "created_at"              =>  $tools->created_at,
                "updated_at"              =>  $tools->updated_at
            );

            array_push($equipment_box, $data);
                
        }

        return response()->json($equipment_box);
    }

    /*
    |-----------------------------------------
    | LOAD USER PAYMENT
    |-----------------------------------------
    */
    public function loadUserPayment(Request $request){
        $email = $request->email;
        $all_payments = Payment::where('seller_email', $email)->get();
        if(count($all_payments) > 0){
            $payment_info = [];
            foreach ($all_payments as $payment) {
                $product_info   = Product::where("id", $payment->item_id)->first();
                $transaction    = Transaction::where("trans_id", $payment->trans_id)->first();
                $user           = User::where('email', $transaction->buyer_email)->first();

                if($user == null){
                    $name = "Guest";
                }else{
                    $name = $user->name;
                }

                # code...
                $data = [
                    'id'        => $payment->id,
                    'trans_id'  => $payment->trans_id,
                    'buyer'     => $name,
                    'amount'    => number_format($payment->amount, 2),
                    'item_id'   => $payment->item_id,
                    'pay_id'    => $payment->pay_id,
                    'trust'     => $payment->trust,               
                    'status'    => $payment->status,
                    'produce'   => $product_info,
                    'details'   => $transaction,
                    'date'      => $payment->created_at->diffForHumans()
                ];
                array_push($payment_info, $data);
            }
        }else{
            $payment_info = [];
        }

        // return response.
        return response()->json($payment_info);
    }

    /*
    |-----------------------------------------
    | LOAD BUYERS PAYMENT
    |-----------------------------------------
    */
    public function loadBuyerPayment(Request $request){
        $email = $request->email;
        $all_payments = Payment::where('buyer_email', $email)->get();
        if(count($all_payments) > 0){
            $payment_info = [];
            foreach ($all_payments as $payment) {
                $product_info = Product::where("id", $payment->item_id)->first();
                $transaction  = Transaction::where("trans_id", $payment->trans_id)->first();
                $user         = User::where('email', $transaction->buyer_email)->first();

                if($user == null){
                    $name = "Guest";
                }else{
                    $name = $user->name;
                }

                # code...
                $data = [
                    'id'        => $payment->id,
                    'trans_id'  => $payment->trans_id,
                    'seller'    => $name,
                    'amount'    => number_format($payment->amount, 2),
                    'item_id'   => $payment->item_id,
                    'pay_id'    => $payment->pay_id,
                    'trust'     => $payment->trust,               
                    'status'    => $payment->status,
                    'produce'   => $product_info,
                    'details'   => $transaction,
                    'date'      => $payment->created_at->diffForHumans()
                ];
                array_push($payment_info, $data);
            }
        }else{
            $payment_info = [];
        }

        // return response.
        return response()->json($payment_info);
    }
    
    /*
    |-----------------------------------------
    | LOAD USER TRANSACTIONS
    |-----------------------------------------
    */
    public function loadUserTransactions(Request $request){
        $email = $request->email;
        $all_transactions = Transaction::where('seller_email', $email)->get();
        if(count($all_transactions) > 0){
            $trans_box = [];
            foreach ($all_transactions as $transaction) {

                $check_farmers = Farmer::where('email', $transaction->buyer_email)->first();
                $check_service = Service::where('email', $transaction->buyer_email)->first();
                $check_investor = Investor::where('email', $transaction->buyer_email)->first();

                if($check_farmers == null){
                    if($check_service == null){
                        if($check_investor == null){
                            $name = "guest";
                        }else{
                            $name = $check_investor->name;
                        }
                    }else{
                        $name = $check_service->name;
                    }
                }else{
                    $name = $check_farmers->name;
                }

                $data = array(
                    'id'        => $transaction->id,
                    'trans_id'  => $transaction->trans_id,
                    'names'     => $name,
                    'sender'    => $transaction->buyer_email,
                    'type'      => $transaction->type,
                    'qty'       => number_format($transaction->qty),
                    'price'     => number_format($transaction->price, 2),
                    'amount'    => number_format($transaction->amount, 2),
                    'item_id'   => $transaction->item_id,                
                    'status'    => $transaction->status,
                    'date'      => $transaction->created_at->diffForHumans()
                );
                // push transaction data
                array_push($trans_box, $data);
            }
        }else{

            $trans_box = [];
        }

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD SINGLE PRODUCE INFO
    |-----------------------------------------
    */
    public function singleProduce(Request $request)
    {
        $id = $request->id;
        # load id
        $product = Product::find($id);
        if($product === null){
            $product = Seasonal::find($id);
        }else{

        }

        // get product owner
        $farmer = User::where('id', $product->user_id)->first();

        // get producer information
        $basic_info = DB::table('farmer_basics')->where('user_id', $product->user_id)->first();
        $total = $product->product_size_no * $product->product_price;

        $product->product_size_type = str_replace("(1000 g)", "(1000g)", $product->product_size_type);
        $data = array(
            "id"                => $product->id,
            "owner_name"        => $farmer->name,
            "owner_email"       => $farmer->email,
            "owner_contact"     => $farmer->phone,
            "owner_office"      => $basic_info->office,
            "owner_image"       => $basic_info->avatar,
            "owner_address"     => $basic_info->address,
            "owner_zipcode"     => $basic_info->zipcode,
            "owner_gender"      => $basic_info->gender,
            "owner_country"     => "Nigeria",
            "product_name"      => $product->product_name,
            "product_size_type" => $product->product_size_type,
            "product_size_no"   => $product->product_size_no,
            "product_price"     => number_format($product->product_price, 2),
            "product_total"     => number_format($total, 2),
            "product_location"  => $product->product_location,
            "product_state"     => $product->product_state, 
            "product_image"     => $product->product_image,
            "product_status"    => $product->product_status,
            "product_note"      => $product->product_note, 
            "created_at"        => $product->created_at->diffForHumans() 
        );

        return response()->json($data);
    }

	/*
    |-----------------------------------------
    | LOAD USER MESSAGES
    |-----------------------------------------
    */
    public function loadMsg(){
    	$messages   = new Message();
        $data       = $messages->getUserMessages();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | COUNT USER MESSAGES
    |-----------------------------------------
    */
    public function countMsg($email)
    {
        
        $user_email = $email;

        # search messages
        $messages = Message::where('to', $email)->orderBy('id', 'desc')->get();
        $total    = $messages->count();

        $data = array(
            'total' => $total
        );
        
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | SEND MESSAGE
    |-----------------------------------------
    */
    public function sendMessage(Request $request)
    {
    	$from = $request->from;
    	$to   = $request->to;
    	$subj = $request->subj;
    	$body = $request->body;
    	$status = 'unread';

    	$message = New Message();
    	$message->from    = $from;
    	$message->to      = $to;
    	$message->subject = $subj;
    	$message->body    = $body;
    	$message->status  = $status;
    	$message->save();

    	// get last messages 
    	$last_message = Message::where([['from', $from], ['to', $to]])->latest()->first();


        // get senders name
        $user  = User::where('email', $to)->first();
        if($user !== null){
            $name = $user->name;
        }else{
            $name = "there";
        }
        

		# code...
		$data = array(
            // 'active_user' = $logged_email,
			'id' 	=> $last_message->id,
			'from' 	=> $last_message->from,
            'name'  => $name,
			'to' 	=> $last_message->to,
			'subject' => $last_message->subject,
			'body' 	=> $last_message->body,
			'date' 	=> $last_message->created_at->diffForHumans()
		);

        // fire a mail
        \Mail::to($to)->send(new EnquiryMail($data));

    	// fire an Event
    	\Event::fire(new NewMessage($data));

        // Save Notifications 
        $notifications           = new Notification();
        $notifications->email    = $to;
        $notifications->contents = json_encode($data, 2, true);
        $notifications->status   = 'unread';
        $notifications->save();

        // save Sent message
        $sent          = New Sent();
        $sent->from    = $from;
        $sent->to      = $to;
        $sent->subject = $subj;
        $sent->body    = $body;
        $sent->status  = $status;
        $sent->save();

    	// return json response
        $build_msg = '<p class="text-success">Message has been sent to  <b>'.$to.'</b> </p>';
    	
        $data = array(
            'status'  => 'success',
            'message' => $build_msg
        );

        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD REPLY MESSAGES
    |-----------------------------------------
    */
    public function loadReplyMsg($msgId, $from, $to)
    {
        // get request params
        $msg_id      = $msgId; // msg id
        $msg_from    = $from; // msg came from
        $logged_user = $to; // msg going to

        // get the subject matter first
        $msg = Message::where('id', $msg_id)->first();
        $topic = $msg->subject;

        // load all messages between both to and from recp
        $load_replies = Message::where([['from', $from], ['to', $logged_user]])
                        ->orWhere([['to', $from], ['from', $logged_user]])
                        ->orderBy('id', 'ASC')
                        ->get();

        $reply_box = [];
        foreach ($load_replies as $replies) {
            $data = array(
                'id'      => $replies->id,
                'topic'   => $topic,
                'from'    => $replies->from,
                'to'      => $replies->to,
                'subject' => $replies->subject,
                'body'    => $replies->body,
                'date'    => $replies->created_at->diffForHumans(),
            );
            array_push($reply_box, $data);
        }

        return response()->json($reply_box);
    }

    /*
    |-----------------------------------------
    | SEND REPLY
    |-----------------------------------------
    */
    public function postReply(Request $request)
    {
        $from    = $request->msgFrom;
        $to      = $request->msgTo;
        $msg_id  = $request->msgId; // to get subject
        $body    = $request->msgBody;

        // get the subject matter first
        $msg = Message::where('id', $msg_id)->first();
        $topic = 'RE: '.$msg->subject;

        $status = 'unread';

        $message = New Message();
        $message->from    = $from;
        $message->to      = $to;
        $message->subject = $topic;
        $message->body    = $body;
        $message->status  = $status;
        $message->save();

        // get last messages 
        $last_message = Message::where([['from', $from], ['to', $to]])->latest()->first();

        // get senders name
        $check_farmers = Farmer::where('email', $from)->first();
        $check_service = Service::where('email', $from)->first();
        $check_investor = Investor::where('email', $from)->first();

        if($check_farmers == null){
            if($check_service == null){
                if($check_investor == null){
                    $name = "guest";
                }else{
                    $name = $check_investor->name;
                }
            }else{
                $name = $check_service->name;
            }
        }else{
            $name = $check_farmers->name;
        }

        # code...
        $data = array(
            // 'active_user' = $logged_email,
            'id'      => $last_message->id,
            'from'    => $last_message->from,
            'name'    => $name,
            'to'      => $last_message->to,
            'subject' => $last_message->subject,
            'body'    => $last_message->body,
            'date'    => $last_message->created_at->diffForHumans()
        );

        // fire a mail
        // \Mail::to($to)->send(new EnquiryMail($data));

        // fire an Event
        \Event::fire(new ReplyMessage($data));

        // Save Notifications 
        $notifications           = new Notification();
        $notifications->email    = $to;
        $notifications->contents = json_encode($data, 2, true);
        $notifications->status   = 'unread';
        $notifications->save();

        // return redirect()->back();
        $data = array(
            'status'  => 'success',
            'message' => 'chat sent !'
        );
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD MESSAGES NOTIFICATIONS
    |-----------------------------------------
    */
    public function notifications(Request $request, $email)
    {

        $all_notifications = Notification::where([['email', $email], ['status', 'unread']])
                            ->orderBy('id', 'desc')
                            ->get();

        $notify_box = [];
        foreach ($all_notifications as $notify) {
            # get all notificatons
            $data = array(
                'id'       => $notify->id,
                'email'    => $notify->email,
                'contents' => json_decode($notify->contents),
                'status'   => $notify->status,
                'date'     => $notify->created_at->diffForHumans()
            );

            array_push($notify_box, $data);
        }

        return response()->json($notify_box);
    }

    /*
    |-----------------------------------------
    | LOAD STAMP MESSAGES
    |-----------------------------------------
    */
    public function loadStampMessages(){
        $stamp  = new Stamp();
        $data   = $stamp->getStampMessages();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE OR REMOVE STAMP MESSAGES
    |-----------------------------------------
    */
    public function delStampMessages(Request $request)
    {
        # code...
        $email = $request->email;

        // get stamp 
        $stamp_message = Stamp::where('user_email', $email)->get();
        if(count($stamp_message) > 0){
            foreach ($stamp_message as $stamp) {
                # code...
                // $del_stamp = Stamp::find($stamp->id);
                // $del_stamp->delete();
            }

            $data = array(
                'status' => 'success',
                'message' => 'stamp deleted successfull !'
            );

            return response()->json($data);
        }
    }

    /*
    |-----------------------------------------
    | LOAD ALL PRODUCES
    |-----------------------------------------
    */
    public function listedProducts()
    {
        # code...
        $list = array(
            'Rice', 'Wheat', 'Soybeans', 
            'Maize', 'Cotton', 'Cassava', 'Palm oil ', 
            'Beans', 'Peanuts', 'Rapeseed', 'Rubber', 
            'Yams', 'Peaches', 'Cacao', 'Sugar', 'Watermelons', 
            'Carrots', 'Coconuts', 'Almonds', 'Lemons', 
            'Strawberries', 'Walnuts'
        );

        return response()->json($list);
    }

    /*
    |-----------------------------------------
    | CALLBACK TRANSACTIONS
    |-----------------------------------------
    */
    public function callbackTransaction($trans_id)
    {
        # scan and delete transactions
        $transaction     = Transaction::where('trans_id', $trans_id)->first();
        if($transaction !== null){
            // delete transaction
            $del_transaction = Transaction::find($transaction->id);
            $del_transaction->delete();

            // scan and delete payment system
            $payment = Payment::where('trans_id', $trans_id)->first();
            if($payment !== null){
                // delete payments 
                $del_payment = Payment::find($payment->id);
                $del_payment->delete();
            }
        }
            
        return redirect()->back();
    }

    /*
    |-----------------------------------------
    | CONFIRM PAYMENT
    |-----------------------------------------
    */ 
    public function confirmPayment($trans_id)
    {
        // return $trans_id;
        # scan and delete transactions
        $transaction     = Transaction::where('trans_id', $trans_id)->first();
        if($transaction !== null){
            // delete transaction
            $update_transaction         = Transaction::find($transaction->id);
            $update_transaction->status = "approved";
            $update_transaction->update();

            // scan and delete payment system
            $payment = Payment::where('trans_id', $trans_id)->first();
            if($payment !== null){
                // delete payments 
                $update_payment         = Payment::find($payment->id);
                $update_payment->status = "approved";
                $update_payment->update();
                // send message to Farmer
                // release payment and set transactions 
                // auto transfer amount from yeelda to receiever's account
            }
        }
            
        return redirect()->back();
    }

    /*
    |-----------------------------------------
    | LOAD ALL FARMERS
    |-----------------------------------------
    */
    public function loadAllFarmers()
    {
        # code...
        $all_farmers = Farmer::orderBy('id', 'desc')->get();
        if(count($all_farmers) > 0){

            // check basic information 
            $farmers_box = [];
            foreach ($all_farmers as $farmer) {
                $tags   = $this->getFarmerTags($farmer->email);
                $basic  = DB::table('farmer_basics')->where('user_id', $farmer->id)->first();

                # display avatar
                if(empty($basic->avatar)){
                    $image = null;
                }else{
                    $image = $basic->avatar;
                }

                # code...
                $data = [
                    'id'    => $farmer->id,
                    'name'  => $farmer->name,
                    'image' => $image,
                    'tags'  => $tags,
                    'email' => $farmer->email
                ];

                array_push($farmers_box, $data);
            }

        }else{
            $farmers_box = [];
        }

        // return response 
        return response()->json($farmers_box);
    }

    /*
    |-----------------------------------------
    | GET FARMERS BY TAGS
    |-----------------------------------------
    */
    public function getFarmerTags($email)
    {
        # code...
        $farmer_crop = Crop::where('user_email', $email)->get();
        if(count($farmer_crop) >  0){
            $tags_box = [];
            foreach ($farmer_crop as $crop) {
                # code...
                array_push($tags_box, $crop->type);
            }

            // convert tags to strings
            $tags = implode(", ", $tags_box);
        
        }else{
        
            $tags = "";
        }

        return $tags;
    }

    /*
    |-----------------------------------------
    | GET TOTAL YEELDA FARMRES
    |-----------------------------------------
    */
    public function totalPlatformUsers(){
        // body
        $farmers    = Farmer::count();
        $services   = Service::count();
        $buyers     = Investor::count();

        $data = [
            'total_farmers'     => $farmers,
            'total_services'    => $services,
            'total_buyers'      => $buyers,
            'total_overall'     => $farmers + $services + $buyers
        ];
 
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LIST ALL BANKS
    |-----------------------------------------
    */
    public function listAllBanks(){
        // body
        // charge endpoint
        $endpoint   = 'https://api.paystack.co/bank';
        $headers    = array('Content-Type: application/json', 'Authorization: Bearer '.env("PS_SK_KEY"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);

        // return response()->json($res); 
        $data = json_decode($res, true);

        // return
        return $data;

        curl_close($ch);
    }

    /*
    |-----------------------------------------
    | LOAD ACCESS 
    |-----------------------------------------
    */
    public function loadViewAccess(Request $request){
        // body
        $request->user_id = Auth::user()->id;
        $view_access    = new ViewAccess();
        $data           = $view_access->allUserViewAccess($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET ALL PREMUIM FARMERS
    |-----------------------------------------
    */
    public function fetchPremiumFarmers(Request $request){
        // body
        $premuim_user   = new User();
        $data           = $premuim_user->getAllPremiumUser($request);

        // return response.
        return response()->json($data);
    }
}
