<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Asset;
use Yeelda\PlatformCrop;

class PriceDiscoveryController extends Controller
{
    // show index commodity price
    public function updatePriceIndex()
    {
    	# code...
    	# go Market
		$commodities = array(
			'Rice', 
            'Wheat', 
            'Soybeans', 
            'Maize', 
            'Cassava', 
            'Palm oil', 
            'Beans', 
            'Peanuts', 
            'Rapeseed', 
            'Rubber', 
            'Yams',
            'Peaches', 
            'Cacao',
            'Sugar', 
            'Watermelons', 
            'Carrots',
            'Almonds',
            'Lemons', 
            'Strawberries',
            'Pepper',
            'Tomatoes',
            'lettuce',
            'Cucumber',
            'Carrot',
            'Cabbage',
            'Cocoyam',
            'Banana',
            'Plantain',
            'Orange',
            'Pawpaw',
            'Mango',
            'Pineapple',
            'Cocoa',
            'Cashew',
            'Ginger',
            'Garlic',
            'Vegetables',
		);

    	// get all assets
    	$all_assets = Asset::all();

    	// check if assets are not empty
    	if(count($all_assets) > 0){
    		foreach ($all_assets as $e) {

    			# init
    			$open     = rand(000, 999) * (20/100);
	    		$close    = $e->open;
	    		$gap      = $e->open - $open;
	    		$previous = $e->open;
	    		$status   = 'open';

    			# discover new price
				$new_price 			 = Asset::find($e->id);   		
				$new_price->open     = $open;
				$new_price->close    = $close;
				$new_price->gap      = $gap;
				$new_price->previous = $previous;
				$new_price->status   = $status;
				$new_price->update();
    		}

    		// data response
    		$data = array(
    			'status' => 'success',
    			'message' => 'New prices has been discovered !!'
    		);

    		// return response commodities 
			return response()->json($data);
    	}else{
    		// define previous
	    	foreach ($commodities as $asset) {
	    		# code...
	    		$open     = rand(000, 999) * (20/100);
	    		$close    = 00.00;
	    		$gap      = 00.00;
	    		$previous = 00.00;
	    		$status   = 'open';

	    		// insert or create new assets updates
	    		$new_asset           = new Asset();
				$new_asset->asset    = $asset;    		
				$new_asset->open     = $open;
				$new_asset->close    = $close;
				$new_asset->gap      = $gap;
				$new_asset->previous = $previous;
				$new_asset->status   = $status;
				$new_asset->save();
			}

			$data = array(
				'status'  => 'success',
				'message' => 'Price discovery has been added successfully !' 
			);

			// return response
			return response()->json($data);
    	}
    }

    // load price index 
    public function loadPriceIndex()
    {
    	# code...
    	$all_assets = Asset::all();

    	$asset_box = [];
    	foreach ($all_assets as $e) {
			# code...
			$data = array(
				'id'       => $e->id,
				'asset'    => $e->asset,
				'open'     => number_format($e->open, 2),
				'close'    => number_format($e->close, 2),
				'gap'      => number_format($e->gap, 2),
				'previous' => number_format($e->previous, 2),
				'status'   => $e->status,
				'date'     => $e->created_at->diffForHumans()
			);

			// push data
			array_push($asset_box, $data);
    	}

    	// return response
    	return response()->json($asset_box);
    }
}