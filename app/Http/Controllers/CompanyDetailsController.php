<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Company;
use Auth;

class CompanyDetailsController extends Controller
{
    /*
    |-----------------------------------------
    | LOAD COMPANY INFORMATION
    |-----------------------------------------
    */
    public function load(){
    	$company   = new Company();
        $data       = $company->loadInfo();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | ADD COMPANY
    |-----------------------------------------
    */
    public function add(Request $request){
        $company    = new Company();
        $data       = $company->addNew($request);

    	// return response
    	return response()->json($data);
    }

	// company details
    public function edit($id)
    {
    	# code...
    }

	// company details
    public function update($id)
    {
    	# code...
    }

    // company details
    public function delete($id)
    {
    	# code...
    	$del_company = Company::find($id);
    	$del_company->delete();

    	// return redirect
    	return redirect()->back();
    }
}
