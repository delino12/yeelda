<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\PasswordChangeNotification;
use Yeelda\Account;
use Yeelda\Farmer;
use Yeelda\Product;
use Yeelda\Seasonal;
use Yeelda\Message;
use Yeelda\Crop;
use Yeelda\User;
use Yeelda\FarmerBasic;
use Yeelda\Wallet;
use Hash;
use Auth;
use DB;

class FarmerJsonController extends Controller
{
	/*
	|-----------------------------------------
	| FARMER AUTHENTICATION MIDDLEWARE
	|-----------------------------------------
    */
	public function __construct() {
		$this->middleware('auth');
	}

    /*
    |-----------------------------------------
    | LOAD FARMER PRODUCT
    |-----------------------------------------
    */
    public function loadProducts(){
    	$product   = new Product();
        $data      = $product->loadProduct();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE OR REMOVE PRODUCE
    |-----------------------------------------
    */
    public function delProduce($id){
        $product    = new Product();
        $data       = $product->deleteProduce($id);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD SINGLE PRODUCE INFO
    |-----------------------------------------
    */
    public function loadSingleProducts(){
        $product    = new Product();
        $data       = $product->loadSingle();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD FARMERS MESSAGES
    |-----------------------------------------
    */
    public function loadMsg(){
    	$messages   = new Message();
        $data       = $meesages->loadMessages();
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD FARMERS BASIC INFORMATION
    |-----------------------------------------
    */
    public function updateProfile(Request $request){
        // request ajax data
        $id      = Auth::user()->id;
        $state   = $request->state; 
        $address = $request->address;
        $postal  = $request->postal;
        $office  = $request->office;
        $mobile  = $request->mobile;
        $gender  = $request->gender;

        // find row id update record
        $update = DB::table('farmer_basics')->where('user_id', $id)
        ->update([
            'state'   => $state,
            'address' => $address,
            'zipcode' => $postal,
            'office'  => $office,
            'gender'  => $gender,
        ]);

        // update mobile contact information
        $find_farmer        = User::find($id);
        $find_farmer->phone = $mobile;
        $find_farmer->update();

        // check if successful updates
        if($update){
            $data = array(
                'status' => 'success',
                'message' => 'Profile updated successfully !'
            );
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'Fail to update profile !'
            );
        }

        // return response
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPDATE FARMERS IMAGES
    |-----------------------------------------
    */
    public function uploadAvatar(Request $request)
    {
        // get current user
        $user_id = Auth::user()->id;

        // request image file
        $image = $request->file('file');

        // rename image
        $new_name = 'yeelda-'.time().'.'.$image->getClientOriginalExtension();

        // set destination path
        $destinationPath = public_path('/uploads/farmers/');

        // move image to destination path
        $image->move($destinationPath, $new_name);

        // update basic information
        $update_profile = DB::table('farmer_basics')->where('user_id', $user_id)->update([
            'avatar' => $new_name,
        ]);

        // return response
        // return response($content = $new_name, $status = 200, $headers = [
        //     'Content-Type' => 'json'
        // ]);

        return redirect()->back();
    }

    /*
    |-----------------------------------------
    | LOAD FAMERS PROFILE
    |-----------------------------------------
    */
    public function loadProfile(){
        $basic_info = new FarmerBasic();
        $data       = $basic_info->getBasicInfo();
        // return response.
        return response()->json($data);
    }

    /*
    |-------------------------------------------
    | LOAD CROP SECTION
    |-------------------------------------------
    */
    public function loadCrops(){
        $crop = new Crop();
        $data = $crop->loadCrop();
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | ADD CROP SECTION
    |-----------------------------------------
    */
    public function addCrops(Request $request){
        $crop = new Crop();
        $data = $crop->addCrop($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE OR REMOVE CROP
    |-----------------------------------------
    */
    public function deleteCrop($id){
       $crop = new Crop();
       $data = $crop->deleteCrop($id);

       // return response.
       return response()->json($data);
    }

    /*
    |-------------------------------------------
    | DEACTIVATE ACCOUNT
    |-------------------------------------------
    */
    public function deactivateAccount(Request $request){
        # code...
        $userid     = $request->userid;
        $password   = $request->password;
        $password   = str_replace(' ', '', $password);

        // check if user exist
        $check_farmer = User::where('id', $userid)->first();

        // return $check_farmer;
        if($check_farmer !== null){
            $password_hash = $check_farmer->password;

            // compare password
            if(Hash::check($password, $password_hash)){

                // check if already deactivate
                $account_deactivated = Account::where('email', $check_farmer->email)->first();

                if($account_deactivated !== null){
                    
                    $data = [
                        'status' => 'error',
                        'message' => 'Account has been deactivated already'
                    ];

                }else{
                    $account            = new Account();
                    $account->email     = $check_farmer->email;
                    $account->status    = "deactivated";
                    $account->save();

                    $data = [
                        'status' => 'success',
                        'message' => 'Account has been deactivated successfully !'
                    ];
                }
            }else{

                $data = [
                    'status' => 'error',
                    'message' => 'password did not match !'
                ];
            }
        }

        return response()->json($data);
    }

    /*
    |-------------------------------------------
    | CHANGE PASSWORD FOR FARMER
    |-------------------------------------------
    */
    public function updatePassword(Request $request) {
        // get current user
        $userid     = $request->userid;
        $password   = $request->password;

        // check if user exist
        $check_farmer = User::where('id', $userid)->first();

        // return $check_farmer;
        if($check_farmer !== null){

            // change farmer passowrd
            $update_farmer              = User::find($userid);
            $update_farmer->password    = bcrypt($password);
            $update_farmer->save();

            $data = [
                'status' => 'success',
                'message' => 'password has been change successfully !'
            ];

            $mail_data = [
                'name' => $check_farmer->name,
                'message' => 'password change successfully !'
            ];

            \Mail::to($check_farmer->email)->send(new PasswordChangeNotification($mail_data));
        }else{
           $data = [
                'status' => 'error',
                'message' => 'Fail to change password please verify authorised user !'
            ]; 
        }

        return response()->json($data);
    }
}

