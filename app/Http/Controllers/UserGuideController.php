<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\UserGuide;

class UserGuideController extends Controller
{
    /*
    |-----------------------------------------
    | START USER GUIDE
    |-----------------------------------------
    */
    public function startGuide(){
    	// body
    	$user_guide = new UserGuide();
    	$data       = $user_guide->checkAlreadyVisit();

    	// return response.
    	return response()->json($data);
    }
}
