<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Activation;
use Auth;

class UsersVerificationController extends Controller
{
    // activate all clients accounts 
    public function activate(Request $request)
    {
    	# activate users account
    	$token  = $request->token;
    	$status = 'inactive';
    	$check_status = Activation::where([['token', $token], ['status', $status]])->first();
    	if($check_status == null){
    		// account already activated

            $msg = "Failed to verify user account";
    		// redirect to home screen
    		return redirect('/')->with('verification_error', $msg);;
    	}else{
    		// activate account
    		$activate         = Activation::find($check_status->id);
    		$activate->status = 'active';
    		$activate->update();

    		$msg = 'Your account has been activated successfully ';
    		return redirect('/')->with('verification_success', $msg);
    	}
    }
}

