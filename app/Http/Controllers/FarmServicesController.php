<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Seed;
use Yeelda\Fertilizer;

class FarmServicesController extends Controller
{
    // add services seed
    public function addSeed($email, Request $request)
    {
    	# code...
		$name	= $request->s_name;    	
		$qty	= $request->s_qty;
		$price	= $request->s_price;
		$note   = 'This is a test note !..';

		// request image file
        $image 				= $request->file('file');
        $new_name 			= 'yeelda-seeds-'.time().'.'.$image->getClientOriginalExtension();
        $destinationPath 	= public_path('/uploads/seed-images/');
        $image->move($destinationPath, $new_name);

        // save seeds
        $add_seeds 				= new Seed();
        $add_seeds->user_email	= $email;
        $add_seeds->name		= $name;
        $add_seeds->qty			= $qty;
        $add_seeds->amount		= $price;
        $add_seeds->note		= $note;
        $add_seeds->avatar		= $new_name;
        $add_seeds->save();

        // response message
        $msg = $request->s_name." has been added successfully !";

    	return redirect()->back()->with('success_msg', $msg);
    }

    // add services seed
    public function addFertilizer($email, Request $request)
    {
    	# code...
    	$type 	= $request->f_type;
		$name	= $request->f_name;
		$price	= $request->f_price;
		$note   = 'This is a test note !..';
		$qty 	= 1;

		// request image file
        $image 				= $request->file('file');
        $new_name 			= 'yeelda-seeds-'.time().'.'.$image->getClientOriginalExtension();
        $destinationPath 	= public_path('/uploads/fertilizer-images/');
        $image->move($destinationPath, $new_name);

        // save fertilizers
        $save_fertilizer				= new Fertilizer();
        $save_fertilizer->user_email 	= $email;
        $save_fertilizer->name 			= $name;
        $save_fertilizer->type 			= $type;
        $save_fertilizer->qty 			= $qty;
        $save_fertilizer->amount 		= $price;
        $save_fertilizer->note 			= $note;
        $save_fertilizer->avatar 		= $new_name;
        $save_fertilizer->save();


        // response message
        $msg = $request->f_name." has been added successfully !";

    	return redirect()->back()->with('success_msg', $msg);
    }

    // load seeds
    public function loadSeeds($email)
    {
    	# code...
    	$seeds = Seed::where('user_email', $email)->get();
    	$seed_box = [];
    	foreach ($seeds as $seed) {
    		# code...
    		$data = array(
    			'id' 		=> $seed->id,
    			'email' 	=> $seed->user_email,
    			'name' 		=> $seed->name,
    			'qty' 		=> $seed->qty,
    			'amount' 	=> number_format($seed->amount, 2),
    			'note' 		=> $seed->note,
    			'image' 	=> $seed->avatar,
    			'date'		=> $seed->created_at->diffForHumans() 
    		);

    		// return response
    		array_push($seed_box, $data);
    	}

    	// return response
    	return response()->json($seed_box);
    }

    // load fertilizers
    public function loadFertilizer($email)
    {
    	# code...
    	$fertilizers = Fertilizer::where('user_email', $email)->get();
    	$fertilizer_box = [];
    	foreach ($fertilizers as $fert) {
    		# code...
    		$data = array(
    			'id' 		=> $fert->id,
    			'email' 	=> $fert->user_email,
    			'type'		=> $fert->type,
    			'name' 		=> $fert->name,
    			'qty' 		=> $fert->qty,
    			'amount' 	=> number_format($fert->amount, 2),
    			'note' 		=> $fert->note,
    			'image' 	=> $fert->avatar,
    			'date'		=> $fert->created_at->diffForHumans() 
    		);

    		array_push($fertilizer_box, $data);
    	}

    	// return response 
    	return response()->json($fertilizer_box);
    }
}