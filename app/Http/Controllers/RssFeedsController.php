<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Feeds;

class RssFeedsController extends Controller
{
    // load rss feeds
    public function rssFeeds()
    {
    	$feeds = Feeds::make('http://www.farms.com/Portals/_default/RSS_Portal/News_Crop.xml');
    	$data = array(
	      'title'       => $feeds->get_title(),
	      'description' => $feeds->get_description(),
	      'permalink'   => $feeds->get_permalink(),
	      'items'       => $feeds->get_items(),
	    );

	    // dd($data['items']);
	    $level_one = $data['items'];
	    $level_one = collect($level_one);

	    // dd($level_one);

	    $feeds_box = [];
	    foreach ($level_one as $feed) {

	    	$level_two = collect($feed->data['child']);
	    	$item_box = [];
	    	// foreach ($level_two as $item) {
	    	// 	# code...
	    	// 	dd($item);

	    	// 	$data = array(
	    	// 		'title'       => $item['title'],
	    	// 		'description' => $item['description'],
	    	// 		'date'        => $item['pubDate']
	    	// 	);
	    	// 	array_push($item_box, $data);
	    	// }

	    	dd($item_box);

	    	array_push($feeds_box, $level_two);
	    }

	    $feed_box = collect($feeds_box);
	    return $feeds_box;

	    // dd($feeds_box);

	    // $items_box = []
	    // foreach ($data['items'] as $first_layer) {
	    // 	# code...
	    // 	$data = array(
	    // 		'data' => $first_layer['data']
	    // 	);

	    // 	array_push($items_box, $data);

	    // }

	    // dd($items_box);


	    // $title = $data['items'][0]->data['child']['']['title'][0]['data'];

    	// return view('external-pages.feeds', compact('items'));
    }
}
