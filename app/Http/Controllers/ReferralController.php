<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Referral;

class ReferralController extends Controller
{
    // load referrals
    public function loadReferal($email)
    {
    	# code...
    	$referrals = Referral::where('user_email', $email)->first();

    	// check if users already have a referrals account
    	if($referrals == null){
    		// create new referrals for client

    		// code 
    		$code   = 'YLD-'.rand(000,999).rand(000,999);
    		$vote   = 0;
    		$amount = 0;

    		$referral             = new Referral();
    		$referral->user_email = $email;
    		$referral->code       = $code;
    		$referral->vote       = $vote;
    		$referral->amount     = $amount;
    		$referral->save();

            $referrals = Referral::where('user_email', $email)->first();
            if($referrals !== null){
                // data to array
                $data = array(
                    'id'     => $referrals->id,
                    'code'   => $referrals->code,
                    'vote'   => $referrals->vote,
                    'amount' => number_format($referrals->amount, 2),
                    'date'   => $referrals->created_at->diffForHumans()
                );
            }

    	}else{
    		// data to array
	    	$data = array(
	    		'id'     => $referrals->id,
	    		'code'   => $referrals->code,
	    		'vote'   => $referrals->vote,
	    		'amount' => number_format($referrals->amount, 2),
	    		'date'   => $referrals->created_at->diffForHumans()
	    	);
    	}

    	// return response
    	return response()->json($data);
    }
}
