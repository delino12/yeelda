<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\AlertPaymentSeller;
use Yeelda\Mail\AlertPaymentBuyer;
use Yeelda\Mail\AlertPaymentAdmin;
use Yeelda\AdminNotification;
use Yeelda\Farmer;
use Yeelda\Service;
use Yeelda\Investor;
use Yeelda\Payment;
use Yeelda\Transaction;
use Yeelda\Product;
use Yeelda\Cart;
use Yeelda\User;
use Yeelda\Wallet;
use Auth;
// use Yeelda\Notify;

class StartPaymentController extends Controller
{
    // start proccessing payment controller
    public function processPayment(Request $request){
    	$amount  = $request->amount;
    	$email   = $request->email;
        $item_id = $request->pids;

    	# code...
    	return view('market.payment', compact('amount', 'email', 'item_id'));
    }

    // log payment information
    public function logPayment(Request $request){
    	# code...
    	$buyer_email   = $request->email;
        $items         = $request->items;
    	$status        = 'pending';
        $trust         = 'pending';
        $trans_id      = $request->tranId;
    	$pay_id        = 'YDA-'.rand(000,999).rand(000,999);

        if($this->verifyTransactions($trans_id)){
            // check transactions
            $transactions = Transaction::where("trans_id", $trans_id)->first();
            if($transactions !== null){
                $data = [
                    'status'    => 'error',
                    'message'   => 'Transaction reference already exit'
                ];
            }else{
                // check product ids 
                $product_ids = explode(',', $items);
                foreach ($product_ids as $id) {
                    // payment product id exits as a product
                    $product = Product::where('id', $id)->first();
                    if($product !== null){
                        // get owners information
                        $check_owner = User::where('id', $product->user_id)->first();
                        if($check_owner !== null){
                            // get seller email
                            $seller_email = $check_owner->email;
                        }

                        $amount = $product->product_size_no * $product->product_price;
                        $type   = 'payment';

                        // save payment log
                        $payment_log                = new Payment();
                        $payment_log->pay_id        = $pay_id;
                        $payment_log->trans_id      = $trans_id;
                        $payment_log->item_id       = $id;
                        $payment_log->buyer_email   = $buyer_email;
                        $payment_log->seller_email  = $seller_email;
                        $payment_log->amount        = $amount;
                        $payment_log->status        = $status;
                        $payment_log->trust         = $trust;
                        $payment_log->save();

                        // save transaction log
                        $transaction                = new Transaction();
                        $transaction->buyer_email   = $buyer_email;
                        $transaction->seller_email  = $seller_email;
                        $transaction->trans_id      = $trans_id;
                        $transaction->type          = $type;
                        $transaction->qty           = $product->product_size_no;
                        $transaction->price         = $product->product_price;
                        $transaction->amount        = $amount;
                        $transaction->item_id       = $id;
                        $transaction->status        = $status;
                        $transaction->save();

                        // empty carts
                        $this->emptyCart($id, $buyer_email);
                        if($check_owner !== null){
                            $name  = $check_owner->name;
                            $phone = $check_owner->phone;

                            // get senders name
                            $check_buyer  = User::where('email', $buyer_email)->first();

                            if($check_buyer !== null){
                                $buyer  = $check_buyer->name;
                            }

                            // compose contents
                            $data = array(
                                'buyer'         => $buyer,
                                'name'          => $name,
                                'amount'        => number_format($amount, 2),
                                'product_name'  => $product->product_name,
                                'trans_id'      => $trans_id
                            );

                            // send product owners emails
                            \Mail::to($check_owner->email)->send(new AlertPaymentSeller($data));
                            \Mail::to($buyer_email)->send(new AlertPaymentBuyer($data));
                            \Mail::to(env("ADMIN_EMAIL"))->send(new AlertPaymentAdmin($data));

                            $this->alertAdmin("payment", $data);
                        }
                    }
                }

                // save status 
                $data = [
                    'status'  => 'success',
                    'message' => 'Payment successful!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid transactions reference'
            ];
        }

    	// return data
    	return response()->json($data);
    }

    // log wallet payment
    public function logWalletPayment(Request $request)
    {
        $amount     = $request->amount;
        $reference  = $request->reference;
        $user_id    = Auth::user()->id;

        $wallet = new Wallet();
        $data = $wallet->creditWallet($user_id, $amount);

        // return
        return $data;
    }

    // remove item 
    public function emptyCart($id, $email){
        # code...
        $carts = Cart::where([['client_email', $email], ['item_id', $id]])->first();
        // remove from cats
        $delete = Cart::find($carts->id);
        $delete->delete();
    }

    // verify payment reference ID 
    public function verifyTransactions($reference){
        # code...
        $endpoint = "https://api.paystack.co/transaction/verify/".$reference;
        $headers  = array('Content-Type: application/json', 'Authorization: Bearer '.env("PAYSTACK_SK_TEST"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

        $data = json_decode($res, true);

        if($data["status"] == true && $data["data"]["status"] == "success"){
            return true;
        }else{
            return false;
        }
        // close connection
        curl_close($ch);
    }

    /*
    |-----------------------------------------
    | ALERT ADMIN
    |-----------------------------------------
    */
    public function alertAdmin($type, $contents){
        // body
        $new_notification = new AdminNotification();
        $new_notification->addNewEvent(["category" => $type, "contents" => $contents]);
    }
}
