<?php

namespace Yeelda\Http\Controllers;

use Yeelda\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yeelda\Mail\AccountResetLink;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\User;
use Auth;
use DB;

class LoginClientsController extends Controller {

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	public function __construct() {
		$this->middleware('guest', [
            'except' => [
                'logoutUsers', 'logoutInvestor', 'logoutServiceProvider', 'deactivated'
            ]
        ]);
	}

	public function showLoginForm() {
		return view("auth.login");
	}

    /*
    |-----------------------------------------
    | SHOW SIGNIN FORM
    |-----------------------------------------
    */
    public function showFormSignin(){
        // body
        return view("auth.new-login");
    }

    /*
    |-----------------------------------------
    | SHOW SIGNUP FORM
    |-----------------------------------------
    */
    public function showFormSignup(){
        // body
        return view("auth.new-signup");
    }

    /*
    |-----------------------------------------
    | SHOW RESET PASSWORD FORM
    |-----------------------------------------
    */
    public function showResetPassword(){
        // body
        return view("auth.reset-password");
    }

	public function doLogin(Request $request) {
		$email            = $request->email;
		$password         = $request->password;
		$rememberToken    = $request->remember;

		// Attempt Login for farmers
		if (Auth::attempt(['email' => $email, 'password' => $password], $rememberToken)) {

            if(Auth::user()->account_type == "farmer"){
                return redirect('/farmer/dashboard');
            }elseif(Auth::user()->account_type == "service"){
                return redirect('/service-provider/dashboard');
            }elseif(Auth::user()->account_type == "buyer"){
                return redirect('/investors/dashboard');
            }
		}else{
			$msg = 'Login Failed please check email/password !';
			return redirect()->back()->with('login_status', $msg);
		}
	}

    // login via facebook 
    public function loginViaEmail(Request $request)
    {
        # code...
        $email  = $request->email;
        $data   = $this->getUserLoginInfo($email);
        
        // return response
        return response()->json($data);
    }

    // get user existence
    public function getUserLoginInfo($email)
    {
        $user = User::where('email', $email)->first();
        # login via id
        if(Auth::loginUsingId($user->id)){
            // on login successful !
            $data = [
                'status'    => 'success',
                'message'   => 'Login successful !',
                'state'     => true
            ];
        }else{
            // on login failure
            $data = [
                'status'    => 'error',
                'message'   => 'Login fail !',
                'state'     => false
            ];
        }

        return $data;
    }

	public function showFindAccountForm() {
		return view("auth.reset");
	}

	public function findAccount(Request $request) {
		// check if users exist
		$email = $request->email;

        // if already exits delete old token
        DB::table('password_resets')->where('email', $email)->delete();

		// get senders name
        $users = User::where('email', $email)->first();

        // token
        $token = bcrypt($email);

        // check if users exits 
        if($users !== null){
        	// reset link
        	$reset_link = DB::table('password_resets')->insert([
        		'email' => $email,
        		'token' => $token
        	]);

        	// mail contents
        	$data = [
        		'name'  => $users->name,
        		'email' => $email,
        		'token' => $token 
        	];

        	// fire Mailable
			\Mail::to($email)->send(new AccountResetLink($data));

            // send reset link mail
            $msg = 'Account reset link has been sent to '.$email;
        	$data = [
        		'status'  => 'success',
        		'message' => $msg
        	];

        	return response()->json($data);

        }else{

        	$msg = 'Sorry, '.$email.' does not exist, check the email address (keywords) and try again ! ';
        	$data = [
        		'status'  => 'error',
        		'message' => $msg
        	];

        	return response()->json($data);
        }
	}

    public function deactivated()
    {
        # code...
        return view('external-pages.deactivated');
    }

	public function logoutUsers() {
		Auth::logout();
		return redirect('/');
	}

	public function logoutServiceProvider() {
		Auth::logout();
		return redirect('/');
	}

	public function logoutInvestor()
	{
		Auth::logout();
		return redirect('/');
	}
}
