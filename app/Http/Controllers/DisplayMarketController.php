<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Product;

class DisplayMarketController extends Controller
{
    // show single produce
    public function showSingle(Request $request)
    {
    	# show single product
    	$product_id = $request->id;
        $produce    = new Product();
        $produce    = $produce->loadSingle($product_id);

    	return view("site-pages.single-produce", compact('product_id', 'produce'));
    }

    // show crop markets
    public function showProduce()
    {
    	# code...
    	return view("site-pages.all-produce");
    }

    // show equipment market
    public function showEquipment()
    {
    	# code...
    	return view("market.equipments");
    }

}
