<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\InboxMessage;
use Yeelda\Product;
use Yeelda\Account;
use Yeelda\Payment;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Yeelda\Chat;
use Yeelda\Association;
use Yeelda\Blog;
use Yeelda\Stamp;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\AdminMessage;
use Yeelda\User;
use Yeelda\FarmerGroup;
use Yeelda\FarmerBasic;
use DB;
use Auth;
use Image;

class AdminHomeController extends Controller
{
    protected $redirectTo = '/admin/login';

	// init authentication
	public function __construct(){
		$this->middleware('auth:admin')->except('logout');
	}

    // show admin dashboard
    public function dashboard(){
    	return view('admin-pages.admin-dashboard');
    }

    /*
    |-----------------------------------------
    | FINANCIAL VIEW
    |-----------------------------------------
    */
    public function financials(){
        // body
        return view('admin-pages.admin-financials');
    }

    /*
    |-----------------------------------------
    | INDUSTRIALS VIEW
    |-----------------------------------------
    */
    public function industrials(){
        // body
        return view('admin-pages.admin-industrials');
    }

    /*
    |-----------------------------------------
    | MESSAGING VIEW
    |-----------------------------------------
    */
    public function messaging(){
        // body
        return view('admin-pages.admin-messaging');
    }

    /*
    |-----------------------------------------
    | OPERATIONS VIEW
    |-----------------------------------------
    */
    public function managment(){
        // body
        return view('admin-pages.admin-management');
    }

    /*
    |-----------------------------------------
    | OFFLINE DATA CAPTURE SYSTEM
    |-----------------------------------------
    */
    public function odca(){
        // body
        return view('admin-pages.admin-odca-new');
    }

    /*
    |-----------------------------------------
    | PRODUCTS VIEW
    |-----------------------------------------
    */
    public function products(){
        // body
        return view('admin-pages.admin-products');
    }

    /*
    |-----------------------------------------
    | PRODUCTS VIEW
    |-----------------------------------------
    */
    public function viewDuplicates(){
        // body
        return view('admin-pages.admin-duplicated');
    }

    /*
    |-----------------------------------------
    | SHOW EDIT PROFILE
    |-----------------------------------------
    */
    public function showEditProfile($id){
        // body
        $user = User::where("id", $id)->first();

        return view('admin-pages.admin-edit-profile', compact('id', 'user'));
    }

    /*
    |-----------------------------------------
    | ANALYTICS VIEW
    |-----------------------------------------
    */
    public function analytics(){
        // body
        return view('admin-pages.admin-analytics');
    }

    /*
    |-----------------------------------------
    | STATISTIC VIEW
    |-----------------------------------------
    */
    public function statistics(){
        // body
        return view('admin-pages.admin-statistics');
    }

    /*
    |-----------------------------------------
    | REPORTS VIEW
    |-----------------------------------------
    */
    public function reports(){
        // body
        // return view('admin-pages.admin-reports');
        return view('admin-pages.admin-reports-debug');
    }

    /*
    |-----------------------------------------
    | REPORTS VIEW
    |-----------------------------------------
    */
    public function viewNotifications(){
        // body
        return view('admin-pages.admin-notifications');
    }

    /*
    |-----------------------------------------
    | SETTINGS VIEW
    |-----------------------------------------
    */
    public function settings(){
        // body
        return view('admin-pages.admin-settings');
    }

    /*
    |-----------------------------------------
    | VIEW TRACKING RECEIPT
    |-----------------------------------------
    */
    public function viewTracking($id){
        // body
        return view('admin-pages.admin-view-tracking', compact('id'));
    }

    /*
    |-----------------------------------------
    | VIEW EQUIPMENT
    |-----------------------------------------
    */
    public function viewEquipment($id){
        // body
        return view('admin-pages.admin-view-equipment', compact('id'));
    }

    // view admin users
    public function viewAdminUser($id){
        # code...
        $user_id = $id;
        return view('admin-pages.view-admin-user', compact('user_id'));
    }

    // view admin users
    public function viewAgentUser($id){
        # code...
        $agent_id = $id;
        return view('admin-pages.view-agent-user', compact('agent_id'));
    }

    // view client profile details
    public function viewProfile($id){
        $user = User::where("id", $id)->first();
        # code...
        return view('admin-pages.admin-user-profile', compact('id', 'user'));
    }

    // show account view
    public function account($value=''){
        return view('admin-pages.account');
    }

    // show all produce
    public function allProduce($value=''){
        # code...
        return view('admin-pages.all-produce');
    }

    // show all inputs
    public function allEquipment($value='')
    {
        # code...
        return view('admin-pages.all-inputs');
    }

    // all farmers
    public function allFarmers($value='')
    {
        # code...
        return view('admin-pages.all-farmers');
    }

    // all services providers
    public function allServices($value='')
    {
        # code...
        return view('admin-pages.all-services');
    }

    // all buyers
    public function allBuyers($value='')
    {
        # code...
        return view('admin-pages.all-buyers');
    }

    // show notifications view
    public function notification($value='')
    {
        return view('admin-pages.notifications');
    }

    // show blog upload view
    public function blog($value='')
    {
        return view('admin-pages.blogs');
    }

    // load payments view
    public function payments($value='')
    {
        # code...
        return view('admin-pages.admin-payments');
    }

    // show documents
    public function documents($value='')
    {
        return view('admin-pages.documents');
    }

    /*
    |--------------------------------
    | SHOW CAPTURE FARM FORM
    |--------------------------------
    |
    */
    public function captureForm(){
        return view('admin-pages.capture-form');
    }


    /*
    |--------------------------------
    | SHOW SMS Logs
    |--------------------------------
    |
    */
    public function viewSMS(){
        return view('admin-pages.view-sms-messages');
    }

    /*
    |--------------------------------
    | SHOW Mail Logs
    |--------------------------------
    |
    */
    public function viewSentMail(){
        return view('admin-pages.view-mail-messages');
    }

    /*
    |--------------------------------
    | SHOW One Mail Logs
    |--------------------------------
    |
    */
    public function viewOneMailMessage($id){
        return view('admin-pages.view-one-mail-messages', compact('id'));
    }

    /*
    |-----------------------------------------
    | SHOW STAMP MESSAGES
    |-----------------------------------------
    */
    public function viewSentStamp(){
        // body
        return view('admin-pages.view-sent-stamp');
    }


    /*
    |-----------------------------------------
    | VIEW PRODUC
    |-----------------------------------------
    */
    public function viewProducts($id){
        // body
        $product = Product::where("id", $id)->first();
        return view('admin-pages.view-one-product', compact('product', 'id'));
    }

    /*
    |--------------------------------
    | init capture image
    |--------------------------------
    |
    */
    public function initCaptureImage(Request $request){
        // request image file
        $image = $request->file('file');

        // rename image
        $new_name = 'yeelda-'.time().'.'.$image->getClientOriginalExtension();

        // set destination path
        $destinationPath = public_path('/images/captured-images/');

        // move image to destination path
        // $image->move($destinationPath, $new_name);
        $thumb_img = Image::make($image->getRealPath())->orientate();
        $thumb_img->resize(240, 300, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$new_name);

        // return response
        return response($content = $new_name, $status = 200, $headers = [
            'Content-Type' => 'json'
        ]);
    }

    /*
    |--------------------------------
    | updated captured image
    |--------------------------------
    |
    */
    public function updateCaptureImage(Request $request){
        $account_type = $request->accountType;
        $user_id = $request->userid;

        // request image file
        $image = $request->file('file');

        // rename image
        $new_name = 'yeelda-'.time().'.'.$image->getClientOriginalExtension();

        // set destination path
        $destinationPath = public_path('/images/captured-images/');

        // move image to destination path
        // $image->move($destinationPath, $new_name);
        $thumb_img = Image::make($image->getRealPath())->orientate();
        $thumb_img->resize(240, 300, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$new_name);


        // check for user account type and update tables
        if($account_type == 'farmer'){
            $update_temp_farmer         = TempFarmer::find($user_id);
            $update_temp_farmer->avatar = $new_name;
            $update_temp_farmer->update();
        }

        // check for user account type and update tables
        if($account_type == 'service'){
            $update_temp_service         = TempService::find($user_id);
            $update_temp_service->avatar = $new_name;
            $update_temp_service->update();
        }

        // check for user account type and update tables
        if($account_type == 'buyer'){
            $update_temp_buyer         = TempBuyer::find($user_id);
            $update_temp_buyer->avatar = $new_name;
            $update_temp_buyer->update();
        }

        // return response
        return response($content = $new_name, $status = 200, $headers = [
            'Content-Type' => 'json'
        ]);
    }


    /*
    |--------------------------------
    | view yeelda Farmer
    |--------------------------------
    |
    */
    public function viewYeeldaUsers($accountType, $id)
    {
        # code...
        return view('admin-pages.yeelda-captured-users', compact('accountType', 'id'));
    }

    /*
    |-----------------------------------------
    | view user by group
    |-----------------------------------------
    */
    public function viewUsersByGroup($id){
        // body
        $group = FarmerGroup::where('id', $id)->first();
        // return $group = FarmerBasic::where('lga', 'Ewekoro')->where('state', 'Ogun')->count();

        $users = User::where('group_id', $id)->first();
        // return count($users->basic->where('lga', 'Ewekoro')->get());
        
        $group_data = new FarmerGroup();
        $group_info = $group_data->allGroupsCapturedUsers($group->id);
        return view('admin-pages.view-users-by-group', compact('group_info', 'group'));
    }


    //--------------------------------
    // Request Section with data response
    //--------------------------------

    // load payment log request
    public function loadPaymentRequest()
    {
        # code...
        $all_payments = Payment::orderBy("id", "DESC")->get();
        if(count($all_payments) > 0){
            $payment_box = [];
            foreach ($all_payments as $payment) {
                // get senders name
                $check_farmers  = User::where([['email', $payment->buyer_email], ['account_type', 'farmer']])->first();
                $check_service  = User::where([['email', $payment->buyer_email], ['account_type', 'service']])->first();
                $check_investor = User::where([['email', $payment->buyer_email], ['account_type', 'buyer']])->first();

                if($check_farmers !== null){
                    $name  = $check_farmers->name;
                    $phone = $check_farmers->phone;
                }elseif($check_service !== null){
                    $name  = $check_service->name;
                    $phone = $check_service->phone;
                }elseif($check_investor !== null){
                    $name  = $check_investor->name;
                    $phone = $check_investor->phone;
                }

                # code...
                $data = array(
                    'id'     => $payment->id,
                    'pay_id' => $payment->pay_id,
                    'name'   => $name,
                    'phone'  => $phone,
                    'seller' => $payment->seller_email,
                    'buyer'  => $payment->buyer_email,
                    'status' => $payment->status,
                    'trust'  => $payment->status,
                    'amount' => number_format($payment->amount, 2),
                    'date'   => $payment->created_at->diffForHumans(),
                );

                array_push($payment_box, $data);
            }
        }else{
            $payment_box = [];
        }

        // return data
        return response()->json($payment_box);
    }

    // load payment log request
    public function loadPaymentCompleted()
    {
        # code...
        $all_payments = Payment::where('status', 'settle')->get();

        $payment_box = [];
        foreach ($all_payments as $payment) {

            // get client info
            // get senders name
            $check_farmers  = User::where([['email', $payment->buyer_email], ['account_type', 'farmer']])->first();
            $check_service  = User::where([['email', $payment->buyer_email], ['account_type', 'service']])->first();
            $check_investor = User::where([['email', $payment->buyer_email], ['account_type', 'buyer']])->first();

            if($check_farmers !== null){
                $name  = $check_farmers->name;
                $phone = $check_farmers->phone;
            }elseif($check_service !== null){
                $name  = $check_service->name;
                $phone = $check_service->phone;
            }elseif($check_investor !== null){
                $name  = $check_investor->name;
                $phone = $check_investor->phone;
            }


            $data = array(
                'id'     => $payment->id,
                'pay_id' => $payment->pay_id,
                'pay_ref'=> $payment->trans_id,
                'name'   => $name,
                'phone'  => $phone,
                'seller' => $payment->seller_email,
                'buyer'  => $payment->buyer_email,
                'status' => $payment->status,
                'trust'  => $payment->status,
                'amount' => number_format($payment->amount, 2),
                'date'   => $payment->created_at->diffForHumans(),
            );

            array_push($payment_box, $data);
        }

        // return data
        return response()->json($payment_box);
    }

    // uload documents
    public function uploadDocs()
    {
        # code...
        return response($content = 'upload successfully', $status = 200, $headers = [
            'Content-Type' => 'json'
        ]);
    }

    // uload documents
    public function uploadBlogImage(Request $request)
    {

        // request image file
        $image = $request->file('file');

        // rename image
        $new_name = 'yeelda-'.time().'.'.$image->getClientOriginalExtension();
        // set destination path
        $destinationPath = public_path('/blogs/images');
        // move image to destination path
        $image->move($destinationPath, $new_name);

        // return response
        return response($content = $new_name, $status = 200, $headers = [
            'Content-Type' => 'json'
        ]);
    }

    // upload blog post
    public function saveBlogPost(Request $request)
    {
        // request blog post
        $by     = 'Yeelda';
        $like   = 0;
        $unlike = 0;
        $title  = $request->title;
        $body   = $request->body;
        $image  = $request->image;
        $category  = $request->category;


        // save new blog post
        $blog_post              = new Blog();
        $blog_post->by          = $by;
        $blog_post->title       = $title;
        $blog_post->category    = $category;
        $blog_post->body        = $body;
        $blog_post->docs        = $image;
        $blog_post->like        = $like;
        $blog_post->unlike      = $unlike;
        $blog_post->save();

        // return response 
        $data = array(
            'status'  => 'success',
            'message' => 'Blog post has been published successfully!'
        );

        // return 
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | DELETE BLOG POST
    |-----------------------------------------
    */
    public function deleteBlogPost(Request $request){
        // body
        $post_id = $request->post_id;
        $post     = Blog::find($post_id);
        $del_post = $post->delete();

        // return response.
        return response()->json(['status' => 'success']);
    }

    // load last uploaded blog post
    public function lastUploadBlog()
    {
        // get last uploaded post
        $last_blog_post = Blog::take('1')->orderBy('id', 'desc')->first();
        
        // return last uploaded blog
        return response()->json($last_blog_post);
    }

    // Load Admin contents
    public function loadUsers(Request $request)
    {
        $account_type = $request->account_type;

        if(!empty($account_type)){
            # load chat users
            $users  = User::where("account_type", $account_type)->orderBy('id', 'desc')->get();
        }else{
            # load chat users
            $users  = User::orderBy('id', 'desc')->get();
        }

        // info of all farmers
        $chat_users_farmers = [];
        $chat_users_service = [];
        $chat_users_investor = [];

        foreach ($users as $user) {
            if($user->account_type == "farmer"){
                // get farmers basic info
                $farmers_basics = DB::table('farmer_basics')->where('user_id', $user->id)->first();

                // check account status
                $status = $this->checkBlockAccount($user->email);

                # code...
                $farmer_data = [
                    'id'    => $user->id,
                    'name'  => $user->name,
                    'email' => $user->email,
                    'type'  => 'Farmer',
                    'lock'  => $status,
                    'phone' => $user->phone,
                    'date'  => $user->created_at->diffForHumans()
                ];
                array_push($chat_users_farmers, $farmer_data);


            }elseif($user->account_type == "service"){
                
                // get farmers basic info
                $service_basic = DB::table('service_basics')->where('user_id', $user->id)->first();

                // check account status
                $status = $this->checkBlockAccount($user->email);

                # code...
                $service_data = [
                    'id'    => $user->id,
                    'name'  => $user->name,
                    'email' => $user->email,
                    'type'  => 'Service Provider',
                    'lock'  => $status,
                    'phone' => $user->phone,
                    'date'  => $user->created_at->diffForHumans()
                ];
                array_push($chat_users_service, $service_data);
            
            }elseif($user->account_type == "buyer"){
                // get investors basic info
                $investors = DB::table('investor_basics')->where('user_id', $user->id)->first();

                // check account status
                $status = $this->checkBlockAccount($user->email);

                # code...
                $investor_data = [
                    'id'    => $user->id,
                    'name'  => $user->name,
                    'email' => $user->email,
                    'type'  => 'Buyer',
                    'lock'  => $status,
                    'phone' => $user->phone,
                    'date'  => $user->created_at->diffForHumans()
                ];
                array_push($chat_users_investor, $investor_data);
            }
        }

        // all categories here
        $all_users = [
            'Farmers'  => $chat_users_farmers,
            'Service'  => $chat_users_service,
            'Investor' => $chat_users_investor
        ];

        // return responses 
        return response()->json($all_users);
    }

    // check if user account is block
    public function checkBlockAccount($email)
    {
        # code...
        $account = Account::where([['email', $email], ['status', 'deactivated']])->first();
        if($account !== null){
            $status = 'locked';
        }else{
            $status = null;
        }

        return $status;
    }

    // laod association request
    public function loadAssoc()
    {
        # load associations
        $associations = Association::all();

        # load assoc box
        $assoc_box = [];
        foreach ($associations as $assoc) {
            # code...
            $data = array(
                'id'     => $assoc->id,
                'name'   => $assoc->name,
                'status' => $assoc->status,
                'date'   => $assoc->created_at->diffForHumans()
            );

           array_push($assoc_box, $data);
        }

        // check if data is empty
        if(count($assoc_box) > 0){
            // found something return it
            return response()->json($assoc_box);
        }else{
            // array is empty
            $msg = array(
                'status'  => 'info',
                'message' => 'No association found !'
            );

            return response()->json($msg);
        }
    }

    // accept Request
    public function acceptRequest($id)
    {
        # code...
        $accept_assoc = Association::find($id);
        $accept_assoc->status = 'approved';
        $accept_assoc->update();

        $data = array(
            'status'  => 'success',
            'message' => 'Request has been approved !'
        );

        // return response
        return response()->json($data);
    }

    // decline Request
    public function declineRequest($id)
    {
        # code...
        $accept_assoc = Association::find($id);
        $accept_assoc->status = 'declined';
        $accept_assoc->update();

        $data = array(
            'status'  => 'success',
            'message' => 'Request has been declined !'
        );

        // return response
        return response()->json($data);
    }



    /*
    |--------------------------------------------------------------
    | SEND EMAIL MESSAGE SECTION
    |--------------------------------------------------------------
    |
    */
    // Send Mail 
    public function sendMessage(Request $request)
    {
        # code...
        $type       = $request->recipient;
        $subject    = $request->subject;
        $body       = $request->body;

        // for single mail
        $email      = $request->single;
        $e_multiple = $request->multiple;


        // send to all users
        if($type == 'all'){

            if($this->sendToAllUsers($subject, $body)){
                // response data
                $data = array(
                    'status'    => 'success',
                    'message'   => 'message has been sent !'
                );

                // return response
                return response()->json($data);
            }
        }

        // send to farmers only 
        if($type == 'farmers'){

            if($this->sendFarmersMessage($subject, $body)){
                // response data
                $data = array(
                    'status'    => 'success',
                    'message'   => 'message has been sent !'
                );

                // return response
                return response()->json($data);
            }
        }

        // send to buyers only
        if($type == 'service'){

            if($this->sendServicesProvidersMessage($subject, $body)){
                // response data
                $data = array(
                    'status'    => 'success',
                    'message'   => 'message has been sent !'
                );

                // return response
                return response()->json($data);
            }
        }

        // send to services providers only 
        if($type == 'buyers'){
            if($this->sendBuyersMessage($subject, $body)){
                // response data
                $data = array(
                    'status'    => 'success',
                    'message'   => 'message has been sent !'
                );

                // return response
                return response()->json($data);
            }
        }

        // send to single user
        if($type == 'single') {
            # code...
            if($this->sendSingleMessage($email, $subject, $body)){
                // response data
                $data = array(
                    'status'    => 'success',
                    'message'   => 'message has been sent !'
                );
            }else{
                // response data
                $data = array(
                    'status'    => 'error',
                    'message'   => $email.' does not match any user, please try again !'
                );
            }

            // return response
            return response()->json($data);
        }

        // send to single user
        if($type == 'multiple') {
            # code...
            if($this->sendToMultipleMessage($e_multiple, $subject, $body)){
                // response data
                $data = array(
                    'status'    => 'success',
                    'message'   => 'message has been sent !'
                );
            }else{
                // response data
                $data = array(
                    'status'    => 'error',
                    'message'   => $email.' does not match any user, please try again !'
                );
            }

            // return response
            return response()->json($data);
        }
    }

    // send to all users 
    public function sendToAllUsers($subject, $body)
    {
        # send to all farmers
        $all_farmers = Farmer::all();
        if(count($all_farmers) > 0){
            foreach ($all_farmers as $farmer) {
                # code...
                $email  = $farmer->email;
                $name   = $farmer->name;

                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // filtered emails
                $email = str_replace(" ", "", $email);

                // log message
                $this->logMailMessage($email, $subject, $body);

                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }
        }

        // send to all services 
        $all_services = Service::all();
        if(count($all_services) > 0){
            foreach ($all_services as $service) {
                # code...
                $email  = $service->email;
                $name   = $service->name;

                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }
        }

        // send messages to all buyers
        $all_buyers = Investor::all();
        if(count($all_buyers) > 0){
            foreach ($all_buyers as $buyer) {
                # code...
                $email  = $buyer->email;
                $name   = $buyer->name;

                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }
        }



        return true;
    }

    // send message to farmers
    public function sendFarmersMessage($subject, $body)
    {
        // send messages to all farmers
        $all_farmers = User::where('account_type', 'farmer')->get();
        if(count($all_farmers) > 0){
            foreach ($all_farmers as $farmer) {
                # code...
                $email  = $farmer->email;
                $name   = $farmer->name;

                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // filtered emails
                $email = str_replace(" ", "", $email);

                // log message
                $this->logMailMessage($email, $subject, $body);
                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }
        }

        return true;
    }

    // send message to services providers
    public function sendServicesProvidersMessage($subject, $body)
    {
        // send messages to all farmers
        $all_services = User::where('account_type', 'service')->get();
        if(count($all_services) > 0){
            foreach ($all_services as $service) {
                # code...
                $email  = $service->email;
                $name   = $service->name;

                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // filtered emails
                $email = str_replace(" ", "", $email);

                // log message
                $this->logMailMessage($email, $subject, $body);

                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }
        }

        return true;
    }

    // send message to buyers
    public function sendBuyersMessage($subject, $body)
    {
        // send messages to all farmers
        $all_buyers = User::where('account_type', 'buyer')->get();
        if(count($all_buyers) > 0){
            foreach ($all_buyers as $buyer) {
                # code...
                $email  = $buyer->email;
                $name   = $buyer->name;

                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // filtered emails
                $email = str_replace(" ", "", $email);

                // log message
                $this->logMailMessage($email, $subject, $body);

                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }
        }

        return true;
    }

    // send message to single User
    public function sendSingleMessage($email, $subject, $body)
    {
        # code...
        $name   = $this->getUserName($email);

        if($name !== null){
            // mail contents
            $mail_contents = array(
                'name'      => $name,
                'subject'   => $subject,
                'body'      => $body
            );

            // filtered emails
            $email = str_replace(" ", "", $email);

            // log message
            $this->logMailMessage($email, $subject, $body);

            // send mail to user
            \Mail::to($email)->send(new InboxMessage($mail_contents));

            return true;
        
        }else{
            
            return false;
        }
    }

    // send message to single User
    public function sendToMultipleMessage($emails, $subject, $body)
    {
        # code...
        // $name   = $this->getUserName($email);
        $name = "Dear";

        $emails = explode(",", $emails);
        if(count($emails) > 0){

            foreach ($emails as $email) {
                # code...
                // mail contents
                $mail_contents = array(
                    'name'      => $name,
                    'subject'   => $subject,
                    'body'      => $body
                );

                // filtered emails
                $email = str_replace(" ", "", $email);

                // log message
                $this->logMailMessage($email, $subject, $body);

                // send mail to user
                \Mail::to($email)->send(new InboxMessage($mail_contents));
            }

            return true;

        }else{
            return false;
        }
    }


    // log all admin sent message
    public function logMailMessage($to, $subject, $body)
    {
        $status = "sent";

        # code...
        $log_message            = new AdminMessage();
        $log_message->to        = $to;
        $log_message->subject   = $subject;
        $log_message->body      = $body;
        $log_message->status    = $status;
        $log_message->save();
    }
    /*
    |
    | END OF EMAIL MESSAGE SECTION
    |----------------------------------------------------------------
    */



    /*|
    |--------------------------------------------------------------
    | SEND STAMP MESSAGE SECTION
    |--------------------------------------------------------------
    |
    */
    // send stamp message
    public function SendStampMessage(Request $request)
    {
        # code...
        $type = $request->type;
        $body = $request->body;

        if($type == 'all'){

            // send stamp to buyers
            if($this->sendStampToAll($body, $type)){
                $data = array(
                    'status' => 'success',
                    'message' => 'Message has been stamp to all '.$type.' dashboard'
                );
            }else{
                 $data = array(
                    'status' => 'error',
                    'message' => 'Fail to send stamp message to all '.$type.' dashboard'
                );
            }

        }elseif($type == 'farmers'){

            // send stamp to farmers only
            if($this->sendStampToFarmers($body, $type)){
                $data = array(
                    'status' => 'success',
                    'message' => 'Message has been stamp to all '.$type.' dashboard'
                );
            }else{
                 $data = array(
                    'status' => 'error',
                    'message' => 'Fail to send stamp message to all '.$type.' dashboard'
                );
            }

        }elseif($type == 'services'){

            // send stamp to services provider only
            if($this->sendStampToServices($body, $type)){
                $data = array(
                    'status' => 'success',
                    'message' => 'Message has been stamp to all '.$type.' dashboard'
                );
            }else{
                 $data = array(
                    'status' => 'error',
                    'message' => 'Fail to send stamp message to all '.$type.' dashboard'
                );
            }

        }elseif($type == 'buyers'){

            // send stamp to buyers
            if($this->sendStampToBuyers($body, $type)){
                $data = array(
                    'status' => 'success',
                    'message' => 'Message has been stamp to all '.$type.' dashboard'
                );
            }else{
                 $data = array(
                    'status' => 'error',
                    'message' => 'Fail to send stamp message to all '.$type.' dashboard'
                );
            }
        }

        // return response
        return response()->json($data);
    }

    // send stamp dashboard messages to all
    public function sendStampToAll($body, $type)
    {
        # code...
        $this->sendStampToFarmers($body, $type);
        $this->sendStampToServices($body, $type);
        $this->sendStampToBuyers($body, $type);

        // packages run.
        return true;
    }

    // send stamp dashboard messages to farmers
    public function sendStampToFarmers($body, $type)
    {
        # code...
        // send messages to all farmers
        $all_farmers = Farmer::all();
        if(count($all_farmers) > 0){
            foreach ($all_farmers as $farmer) {
                # code...
                $email  = $farmer->email;

                $new_stamp_message              = new Stamp();
                $new_stamp_message->type        = $type;
                $new_stamp_message->body        = $body;
                $new_stamp_message->status      = 'unread';
                $new_stamp_message->user_email  = $email;
                $new_stamp_message->save();
            }
        }

        return true;
    }

    // send stamp dashboard messages to services
    public function sendStampToServices($body, $type)
    {
        // send to all services 
        $all_services = Service::all();
        if(count($all_services) > 0){
            foreach ($all_services as $service) {
                # code...
                $email  = $service->email;
                $new_stamp_message              = new Stamp();
                $new_stamp_message->type        = $type;
                $new_stamp_message->body        = $body;
                $new_stamp_message->status      = 'unread';
                $new_stamp_message->user_email  = $email;
                $new_stamp_message->save();
            }
        }

        return true;
    }

    // send stamp dashboard messages to buyers
    public function sendStampToBuyers($body, $type)
    {
        // send messages to all buyers
        $all_buyers = Investor::all();
        if(count($all_buyers) > 0){
            foreach ($all_buyers as $buyer) {
                # code...
                $email  = $buyer->email;
                $new_stamp_message              = new Stamp();
                $new_stamp_message->type        = $type;
                $new_stamp_message->body        = $body;
                $new_stamp_message->status      = 'unread';
                $new_stamp_message->user_email  = $email;
                $new_stamp_message->save();
            }
        }

        return true;
    }

    /*
    |
    | END OF STAMP MESSAGE SECTION
    |----------------------------------------------------------------
    */

    // get user information
    public function getUserName($email)
    {
        # code...
        $check_farmers  = User::where([['email', $email], ['account_type', 'farmer']])->first();
        $check_service  = User::where([['email', $email], ['account_type', 'service']])->first();
        $check_investor = User::where([['email', $email], ['account_type', 'buyer']])->first();

        if($check_farmers !== null){
            $name = $check_farmers->name;
        }elseif($check_service !== null){
            $name = $check_service->name;
        }elseif($check_investor !== null){
            $name = $check_investor->name;
        }else{
            $name = null;
        }

        return $name;
    }

    // logout admin
    public function logout()
    {
    	Auth::guard('admin')->logout();

    	// return to login screen
    	return redirect('/admin/login');
    }
}
