<?php
namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\PaymentSentMail;
use Yeelda\PlatformStatistic;
use Yeelda\Association;
use Yeelda\AdminNotification;
use Yeelda\TempUser;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\Account;
use Yeelda\Equipment;
use Yeelda\Transaction;
use Yeelda\Payment;
use Yeelda\Product;
use Yeelda\Farmer;
use Yeelda\Service;
use Yeelda\Investor;
use Yeelda\Crop;
use Yeelda\SMS;
use Yeelda\Admin;
use Yeelda\Agent;
use Yeelda\AdminMessage;
use Yeelda\Stamp;
use Yeelda\Fertilizer;
use Yeelda\Seed;
use Yeelda\User;
use Yeelda\TrackProduct;
use Yeelda\TransitLocation;
use Yeelda\SmsQueue;
use Yeelda\Chat;
use Yeelda\ViewAccess;
use Yeelda\DuplicateRecord;
use Carbon\Carbon;
use DB;
use Auth;

class AdminJsonController extends Controller
{
    /*
    |-------------------------------------------------
    | Admin Construct
    |-------------------------------------------------
    |
    */
    public function __construct(){
    	# code...
    	$this->middleware('auth:admin')->except(['fetchAllStates', 'fetchLga', 'fetchLgaClusters', 'fetchReportsDataByState', 'searchAndReplaceLga', 'getClusters', 'getClusterVillages', 'getEstimateVolume', 'getClustersReport', 'countUsersByState']);
    }

    /*
    |-------------------------------------------------
    | Admin load all admin
    |-------------------------------------------------
    |
    */
    public function loadAdminUsers(){
        $all_admin = Admin::orderBy('id', 'DESC')->get();
        if(count($all_admin) > 0){
            $admin_box = [];
            foreach ($all_admin as $admin) {
                # code...
                $data = [
                    'id' => $admin->id,
                    'names' => $admin->names,
                    'email' => $admin->email,
                    'level' => $admin->level,
                    'date' => $admin->created_at->diffForHumans()
                ];

                array_push($admin_box, $data);
            }
        }else{
            $admin_box = [];
        }

        return response()->json($admin_box);
    }

    /*
    |-------------------------------------------------
    | Admin add users
    |-------------------------------------------------
    |
    */
    public function adminAddUsers(Request $request){

        $names      = $request->names;
        $level      = $request->level;
        $email      = $request->email;
        $password   = $request->password;

        // check if user already added 
        $already_exist = Admin::where('email', $email)->first();
        if($already_exist !== null){
            $data = [
                'status' => 'error',
                'message' => 'This user already exist !'
            ];
        }else{
            $new_admin              = new Admin();
            $new_admin->names       = $names;
            $new_admin->email       = $email;
            $new_admin->level       = $level;
            $new_admin->password    = bcrypt($password);
            $new_admin->save();

            $data = [
                'status' => 'success',
                'message' => $names.' has been added to admin !'
            ];
        }

        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | Add Agent users
    |-------------------------------------------------
    */
    public function agentAddUsers(Request $request){
        // body
        $new_agent  = new Agent();
        $data       = $new_agent->addNewAgent($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | Add Agent users
    |-------------------------------------------------
    */
    public function loadAgentUsers(Request $request){
        // body
        $all_agent  = new Agent();
        $data       = $all_agent->loadAllAgent();

        // return response.
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | Load admin single user
    |-------------------------------------------------
    |
    */
    public function loadAdminSingleUser($id){
        $admin_single = Admin::where('id', $id)->first();
        if($admin_single !== null){

            $data = [
                'id'            => $admin_single->id,
                'names'         => $admin_single->names,
                'email'         => $admin_single->email,
                'level'         => $admin_single->level,
                'access_level'  => [
                                    'menu_access'   => ['dashboard|message|blog'],
                                    'permission'    => ['edit|delete|view'],
                                ],
                'date'          => $admin_single->created_at->diffForHumans()
            ];

        }else{
            $data = [
                'status' => 'error',
                'message' => 'This user does not exits, check admin list and try again... '
            ];
        }

        // return response 
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | Load admin single user
    |-------------------------------------------------
    |
    */
    public function loadAgentSingleUser($id){
        $agent_single = Agent::where('id', $id)->first();
        if($agent_single !== null){

            $capture_data = $this->getTotalCapturedUsers($agent_single->agent_code);

            $data = [
                'id'            => $agent_single->id,
                'names'         => $agent_single->name,
                'email'         => $agent_single->email,
                'phone'         => $agent_single->phone,
                'address'       => $agent_single->address,
                'gender'        => $agent_single->gender,
                'avatar'        => $agent_single->avatar,
                'agent_code'    => $agent_single->agent_code,
                'reg_users'     => $capture_data,
                'date'          => $agent_single->created_at->diffForHumans()
            ];

        }else{
            $data = [
                'status' => 'error',
                'message' => 'This user does not exits, check agent list and try again... '
            ];
        }

        // return response 
        return response()->json($data);
    }


    /*
    |-------------------------------------------------
    | Count total capture users
    |-------------------------------------------------
    */
    public function getTotalCapturedUsers($agent_id){
        $main_farmers   = User::where([["account_type", "farmer"], ["agent_id", $agent_id]])->count();
        $main_services  = User::where([["account_type", "service"], ["agent_id", $agent_id]])->count();
        $main_buyers    = User::where([["account_type", "buyer"], ["agent_id", $agent_id]])->count();

        $total_farmers  = $main_farmers;
        $total_services = $main_services;
        $total_buyers   = $main_buyers;

        $data = [
            'farmers'   => $total_farmers,
            'services'  => $total_services,
            'buyers'    => $total_buyers,
            'total'     => $total_farmers + $total_services + $total_buyers
        ];

        return $data;
    }

    /*
    |-------------------------------------------------
    | Update admin user information
    |-------------------------------------------------
    |
    */
    public function updateAdminUserInfo(Request $request){

        $userid = $request->userid;
        $names  = $request->names;
        $email  = $request->email;
        $level  = $request->level;

        // check and updates
        $check_exits = Admin::where([['id', $userid], ['email', $email]])->first();
        if($check_exits !== null){
            // find and update the information
            $update_admin = Admin::find($userid);
            $update_admin->names = $names;
            $update_admin->email = $email;
            $update_admin->level = $level;
            $update_admin->update();

            // response data success
            $data = [
                'status' => 'success',
                'message' => $names.' information has been updated successfully !'
            ];
        }else{
            // response data error
            $data = [
                'status' => 'error',
                'message' => 'Failed to update user information, try again !'
            ];
        }

        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | remove init uploaded image
    |-------------------------------------------------
    |
    */
    public function removeImage(Request $request){

        $image_name = $request->filename;

        // check if files already exist
        if(file_exists(public_path('images/captured-images/'.$image_name))){
            unlink(public_path('images/captured-images/'.$image_name));

            $data = [
                'status' => 'success',
                'message' => 'files has been remove successfully !'
            ];

        }else{

            $data = [
                'status' => 'success',
                'message' => 'could not find '.$image_name.' in images directory !'
            ];
        }

        // return response 
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | create temp user
    |-------------------------------------------------
    |
    */
    public function createTempUser(Request $request){
        // request data
        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $email          = $request->email;
        $phone          = $request->phone;
        $avatar         = $request->avatar;
        $state          = $request->state;
        $lga            = $request->lga;
        $ward           = $request->ward;
        $language       = $request->language;
        $accountType    = $request->accountType;
        $address        = $request->address;

        // account option section
        $farmSizeNo         = $request->farmSizeNo;
        $farmSizetype       = $request->farmSizetype;
        $farmSizeCrop       = $request->farmSizeCrop;
        
        $storageCapicity    = $request->storageCapicity;
        $typeOfStroage      = $request->typeOfStroage;

        $companyName        = $request->companyName;
        $comPerferPurchase  = $request->comPerferPurchase;

        // save account for Temp Farmers
        if($accountType == 'farmer'){

            $new_farmer                 = new TempFarmer();
            $new_farmer->firstname      = $firstname;
            $new_farmer->lastname       = $lastname;
            $new_farmer->email          = $email;
            $new_farmer->phone          = $phone;
            $new_farmer->avatar         = $avatar;
            $new_farmer->state          = $state;
            $new_farmer->lga            = $lga;
            $new_farmer->ward           = $ward;
            $new_farmer->language       = $language;
            $new_farmer->accountType    = $accountType;
            $new_farmer->address        = $address;
            $new_farmer->farmSizeNo     = $farmSizeNo;
            $new_farmer->farmSizetype   = $farmSizetype;
            $new_farmer->farmSizeCrop   = $farmSizeCrop;
            $new_farmer->save();

            // send SMS
            $sms_msg = "Welcome to YEELDA. We connect farmers, buyers and services providers. visit www.yeelda.com Today.";

            $code = '+234';
            $phone = substr($phone, 1);
            $phone = $code.$phone;

            // send sms
            $sender = 'YEELDA';
            $send_sms = new SMS();
            $send_sms->sendMessages($phone, $sms_msg, $sender);

            // log sms
            $this->logSms($sms_msg, $phone, $accountType);
            
            // response 
            $data = [
                'status'    => 'success',
                'message'   => '1 new Farmer added successfully !'
            ];

        }elseif($accountType == 'service'){

            $new_service                 = new TempService();
            $new_service->firstname      = $firstname;
            $new_service->lastname       = $lastname;
            $new_service->email          = $email;
            $new_service->phone          = $phone;
            $new_service->avatar         = $avatar;
            $new_service->state          = $state;
            $new_service->lga            = $lga;
            $new_service->ward           = $ward;
            $new_service->language       = $language;
            $new_service->accountType    = $accountType;
            $new_service->address        = $address;
            $new_service->storageCapicity= $storageCapicity;
            $new_service->typeOfStroage  = $typeOfStroage;
            $new_service->save();

            // send SMS
            $sms_msg = "Welcome to YEELDA. We connect farmers, buyers and services providers. visit www.yeelda.com Today.";

            $code = '+234';
            $phone = substr($phone, 1);
            $phone = $code.$phone;

            // send sms
            $sender = 'YEELDA';
            $send_sms = new SMS();
            $send_sms->sendMessages($phone, $sms_msg, $sender);

            // log sms
            $this->logSms($sms_msg, $phone, $accountType);
            
            // response 
            $data = [
                'status'    => 'success',
                'message'   => '1 new Services Provider added successfully !'
            ];

        }elseif($accountType == 'buyer'){
            
            $new_buyer                  = new TempBuyer();
            $new_buyer->firstname       = $firstname;
            $new_buyer->lastname        = $lastname;
            $new_buyer->email           = $email;
            $new_buyer->phone           = $phone;
            $new_buyer->avatar          = $avatar;
            $new_buyer->state           = $state;
            $new_buyer->lga             = $lga;
            $new_buyer->ward            = $ward;
            $new_buyer->language        = $language;
            $new_buyer->accountType     = $accountType;
            $new_buyer->address         = $address;
            $new_buyer->companyName     = $companyName;
            $new_buyer->comPerferPurchase = $comPerferPurchase;
            $new_buyer->save();

            // send SMS
            $sms_msg = "Welcome to YEELDA. We connect farmers, buyers and services providers. visit www.yeelda.com Today.";

            $code = '+234';
            $phone = substr($phone, 1);
            $phone = $code.$phone;

            // send sms
            $sender = 'YEELDA';
            $send_sms = new SMS();
            $send_sms->sendMessages($phone, $sms_msg, $sender);

            // log sms
            $this->logSms($sms_msg, $phone, $accountType);

            // response 
            $data = [
                'status'    => 'success',
                'message'   => '1 new Buyer added successfully !'
            ];

        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Failed to add user, Please specify User Signup Account Type !'
            ];
        }

        // save user
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | create temp user
    |-------------------------------------------------
    |
    */
    public function updateTempUser(Request $request){
        // request data
        $temp_id        = $request->temp_id;
        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $email          = $request->email;
        $phone          = $request->phone;
        $avatar         = $request->avatar;
        $state          = $request->state;
        $lga            = $request->lga;
        $ward           = $request->ward;
        $language       = $request->language;
        $accountType    = $request->accountType;
        $address        = $request->address;

        // account option section
        $farmSizeNo         = $request->farmSizeNo;
        $farmSizetype       = $request->farmSizetype;
        $farmSizeCrop       = $request->farmSizeCrop;
        
        $storageCapicity    = $request->storageCapicity;
        $typeOfStroage      = $request->typeOfStroage;

        $companyName        = $request->companyName;
        $comPerferPurchase  = $request->comPerferPurchase;

        // save account for Temp Farmers
        if($accountType == 'farmer'){
            $new_farmer                 = TempFarmer::find($temp_id);
            $new_farmer->firstname      = $firstname;
            $new_farmer->lastname       = $lastname;
            $new_farmer->email          = $email;
            $new_farmer->phone          = $phone;
            $new_farmer->state          = $state;
            $new_farmer->lga            = $lga;
            $new_farmer->language       = $language;
            $new_farmer->address        = $address;
            $new_farmer->update();
            
            // response 
            $data = [
                'status'    => 'success',
                'message'   => $firstname.' Update successfully!'
            ];

        }elseif($accountType == 'service'){

            $new_service                 = TempService::find($temp_id);
            $new_service->firstname      = $firstname;
            $new_service->lastname       = $lastname;
            $new_service->email          = $email;
            $new_service->phone          = $phone;
            $new_service->state          = $state;
            $new_service->lga            = $lga;
            $new_service->language       = $language;
            $new_service->address        = $address;
            $new_service->update();

            // response 
            $data = [
                'status'    => 'success',
                'message'   => $firstname.' Update successfully!'
            ];

        }elseif($accountType == 'buyer'){
            
            $new_buyer                  = TempBuyer::find($temp_id);
            $new_buyer->firstname       = $firstname;
            $new_buyer->lastname        = $lastname;
            $new_buyer->email           = $email;
            $new_buyer->phone           = $phone;
            $new_buyer->state           = $state;
            $new_buyer->lga             = $lga;
            $new_buyer->language        = $language;
            $new_buyer->address         = $address;
            $new_buyer->update();

            // response 
            $data = [
                'status'    => 'success',
                'message'   => $firstname.' Update successfully!'
            ];

        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Failed to add user, Please specify User Signup Account Type !'
            ];
        }

        // save user
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | load temp user
    |-------------------------------------------------
    |
    */
    public function loadTempUser(){

        $temp_farmers   = TempFarmer::orderBy('id', 'DESC')->get();
        $temp_service   = TempService::orderBy('id', 'DESC')->get();
        $temp_buyer     = TempBuyer::orderBy('id', 'DESC')->get();

        // all users
        $all_users = [];
        if(count($temp_farmers) > 0 ){
            $farmers_box = [];
            foreach ($temp_farmers as $farmer) {
                # code...
                $data = [
                    'id'            => $farmer->id,
                    'names'         => $farmer->firstname.' '.$farmer->lastname,
                    'email'         => $farmer->email,
                    'accountType'   => $farmer->accountType,
                    'state'         => $farmer->state,
                    'lga'           => $farmer->lga,
                    'cluster'       => $farmer->ward,
                    'phone'         => $farmer->phone
                ];

                array_push($farmers_box, $data);
            }

        }else{

            $farmers_box = [];
        }

        if(count($temp_service) > 0 ){
            $service_box = [];
            foreach ($temp_service as $service) {
                # code...
                $data = [
                    'id'            => $service->id,
                    'names'         => $service->firstname.' '.$service->lastname,
                    'email'         => $service->email,
                    'accountType'   => $service->accountType,
                    'phone'         => $service->phone,
                    'state'         => $service->state,
                    'lga'           => $service->lga,
                    'cluster'       => $farmer->ward,
                    'phone'         => $service->phone
                ];

                array_push($service_box, $data);
            }

        }else{

            $service_box = [];
        }

        // filtered for buyer
        if(count($temp_buyer) > 0 ){
            $buyer_box = [];
            foreach ($temp_buyer as $buyer) {
                # code...
                $data = [
                    'id'            => $buyer->id,
                    'names'         => $buyer->firstname.' '.$buyer->lastname,
                    'email'         => $buyer->email,
                    'accountType'   => $buyer->accountType,
                    'phone'         => $buyer->phone,
                    'state'         => $buyer->state,
                    'lga'           => $buyer->lga,
                    'cluster'       => $farmer->ward,
                    'phone'         => $buyer->phone
                ];

                array_push($buyer_box, $data);
            }

        }else{

            $buyer_box = [];
        }

        // all user data 
        $all_user_data = [
            'farmers' => $farmers_box,
            'service' => $service_box,
            'buyer' => $buyer_box
        ];
        return $all_user_data;
    }

    /*
    |-------------------------------------------------
    | count all temp users
    |-------------------------------------------------
    */
    public function countTempUser(){
        // body
        $temp_farmers   = TempFarmer::count();
        $temp_service   = TempService::count();
        $temp_buyer     = TempBuyer::count();

        $total = $temp_farmers + $temp_service + $temp_buyer;

        $data = [
            'status'    => 'success',
            'message'   => 'Count Ready!',
            'total'     => number_format($total)
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | view single user profile info
    |-------------------------------------------------
    |
    */
    public function loadYeeldaUsers($accountType, $id){

        $temp_farmers   = TempFarmer::where('id', $id)->first();
        $temp_service   = TempService::where('id', $id)->first();
        $temp_buyer     = TempBuyer::where('id', $id)->first();

        if($temp_farmers !== null){
            # code...
            $data = [
                'id'            => $temp_farmers->id,
                'names'         => $temp_farmers->firstname.' '.$temp_farmers->lastname,
                'gender'        => $temp_farmers->gender,
                'email'         => $temp_farmers->email,
                'accountType'   => $temp_farmers->accountType,
                'phone'         => $temp_farmers->phone,
                'avatar'        => $temp_farmers->avatar,
                'state'         => $temp_farmers->state,
                'lga'           => $temp_farmers->lga,
                'ward'          => $temp_farmers->ward,
                'language'      => $temp_farmers->language,
                'address'       => $temp_farmers->address,
                'farmSizeNo'    => $temp_farmers->farmSizeNo,
                'farmSizetype'  => $temp_farmers->farmSizetype,
                'farmSizeCrop'  => $temp_farmers->farmSizeCrop,
                'prevCropPlant' => $temp_farmers->prevCropPlanted,
                'prevCropHaves' => $temp_farmers->prevCropHarvested,
                'prevYieldHaves'=> number_format((int) $temp_farmers->prevCropHarvested, 2),
                'prevSizeOfHav' => ucfirst($temp_farmers->prevYieldHarvested),
                'agent_id'      => $temp_farmers->agent_id,
                'date_captured' => $temp_farmers->date_captured,
            ];

        }elseif($temp_service !== null){

            # code...
            $data = [
                'id'            => $temp_service->id,
                'names'         => $temp_service->firstname.' '.$temp_service->lastname,
                'email'         => $temp_service->email,
                'accountType'   => $temp_service->accountType,
                'phone'         => $temp_service->phone,
                'avatar'        => $temp_service->avatar,
                'state'         => $temp_service->state,
                'lga'           => $temp_service->lga,
                'ward'          => $temp_service->ward,
                'language'      => $temp_service->language,
                'address'       => $temp_service->address,
                'storageCapicity' => $temp_service->storageCapicity,              
                'typeOfStroage' => $temp_service->typeOfStroage,  
                'agent_id'      => $temp_service->agent_id,
                'date_captured' => $temp_farmers->date_captured,        
            ];

        }elseif($temp_buyer !== null){

            # code...
            $data = [
                'id'            => $temp_buyer->id,
                'names'         => $temp_buyer->firstname.' '.$temp_buyer->lastname,
                'email'         => $temp_buyer->email,
                'accountType'   => $temp_buyer->accountType,
                'phone'         => $temp_buyer->phone,
                'avatar'        => $temp_buyer->avatar,
                'state'         => $temp_buyer->state,
                'lga'           => $temp_buyer->lga,
                'ward'          => $temp_buyer->ward,
                'language'      => $temp_buyer->language,
                'accountType'   => $temp_buyer->accountType,
                'address'       => $temp_buyer->address,
                'companyName'   => $temp_buyer->companyName,
                'comPerferPurchase' => $temp_buyer->comPerferPurchase,
                'agent_id'      => $temp_buyer->agent_id, 
                'date_captured' => $temp_farmers->date_captured,  
            ];

        }else{

            $data = [];
        }

        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | load user 
    |-------------------------------------------------
    |
    */
    public function deleteTempUser(Request $request){

        $userid      = $request->userid;
        $accountType = $request->accountType;


        if($accountType == 'farmer'){
            $delete_farmer = TempFarmer::find($userid);
            $delete_farmer->delete();

            $data = [
                'status'    => 'success',
                'message'   => 'Farmer deleted successful!'
            ];
        }elseif($accountType == 'service'){
            $delete_farmer = TempService::find($userid);
            $delete_farmer->delete();

            $data = [
                'status'    => 'success',
                'message'   => 'Service provider deleted successful!'
            ];
        }elseif($accountType == 'buyer'){
            $delete_farmer = TempBuyer::find($userid);
            $delete_farmer->delete();

            $data = [
                'status'    => 'success',
                'message'   => 'Buyer deleted successful!'
            ];
        }else{
            $data = [
                'status' => 'error',
                'message' => 'failed to delete user !'
            ];
        }

        // 
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | load all products
    |-------------------------------------------------
    */
    public function allProducts($value='')
    {
    	# code...
    	$products = Product::all();

    	$products_box = [];

    	foreach ($products as $product) {
    		$farmer = User::where('id', $product->user_id)->first();
            if($farmer !== null){
                // compute total
                $amount = $product->product_size_no * $product->product_price;

                # code...
                $data = array(
                    'id'        => $product->id,
                    'owner'     => $farmer->name,
                    'phone'     => $farmer->phone,
                    'email'     => $farmer->email,
                    'amount'    => number_format($amount, 2),
                    'status'    => $product->product_status,
                    'name'      => $product->product_name,
                    'size_type' => $product->product_size_type,
                    'size_no'   => $product->product_size_no,
                    'price'     => $product->product_price,
                    'location'  => $product->product_location,
                    'state'     => $product->product_state,
                    'image'     => $product->product_image,
                    'note'      => $product->product_note,
                    'date'      => $product->created_at->diffForHumans()
                );

                array_push($products_box, $data);
            }
                
    	}

    	//	return response
    	return response()->json($products_box);
    }

    /*
    |-------------------------------------------------
    | delete products
    |-------------------------------------------------
    */
    public function deleteProduct($id)
    {
    	# code...
    	$del_product = Product::find($id);
    	$del_product->delete();

    	$data = array(
    		'status'  => 'success',
    		'message' => 'Product deleted !'
    	);

    	// return response
    	return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | load equipment
    |-------------------------------------------------
    |
    */ 
    public function loadEquipment()
    {
    	$equipment = DB::table('equipments')->get();
        if(count($equipment) > 0){
            $equipment_box = [];
            foreach ($equipment as $inputs) {
                $service = User::where('id', $inputs->user_id)->first();
                if($service !== null){
                    // compute total
                    $amount = floatval($inputs->equipment_price) * floatval($inputs->equipment_no);
                    # code...
                    $data = array(
                        'id'            => $inputs->id,
                        'owner'         => $service->name,
                        'phone'         => $service->phone,
                        'email'         => $service->email,
                        'amount'        => number_format($amount, 2),
                        'user_id'       => $inputs->user_id,
                        'name'          => $inputs->equipment_name,
                        'descriptions'  => $inputs->equipment_descriptions,
                        'no'            => $inputs->equipment_no,
                        'status'        => $inputs->equipment_status,
                        'price'         => $inputs->equipment_price,
                        'address'       => $inputs->equipment_address,
                        'state'         => $inputs->equipment_state,
                        'image'         => $inputs->equipment_image,
                        'delivery_type' => $inputs->equipment_delivery_type,
                        'note'          => $inputs->equipment_note,
                        'date'          => $inputs->created_at
                    );

                    array_push($equipment_box, $data);
                }
            }
        }else{
            $equipment_box = [];
        }
    	// return response
    	return response()->json($equipment_box);
    }

    /*
    |-------------------------------------------------
    | LOAD ONE EQUIPMENT
    |-------------------------------------------------
    */
    public function loadOneEquipment($id){
        // body
        $inputs = DB::table('equipments')->where("id", $id)->first();
        if($inputs !== null){
            $service = User::where('id', $inputs->user_id)->first();
            // compute total
            $amount = floatval($inputs->equipment_price) * floatval($inputs->equipment_no);
            # code...
            $data = array(
                'id'            => $inputs->id,
                'owner'         => $service->name,
                'phone'         => $service->phone,
                'email'         => $service->email,
                'amount'        => number_format($amount, 2),
                'user_id'       => $inputs->user_id,
                'name'          => $inputs->equipment_name,
                'descriptions'  => $inputs->equipment_descriptions,
                'no'            => $inputs->equipment_no,
                'status'        => $inputs->equipment_status,
                'price'         => $inputs->equipment_price,
                'address'       => $inputs->equipment_address,
                'state'         => $inputs->equipment_state,
                'image'         => $inputs->equipment_image,
                'delivery_type' => $inputs->equipment_delivery_type,
                'note'          => $inputs->equipment_note,
                'date'          => $inputs->created_at
            );
        }else{
            $data = [];
        }
        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | load fertilizers
    |-------------------------------------------------
    |
    */ 
    public function loadFarmServices()
    {
        $fertilizers = Fertilizer::orderBy('id', 'DESC')->get();
        $seeds = Seed::orderBy('id', 'DESC')->get();
        $fertilizer_box = [];
        if(count($fertilizers) > 0){
            foreach ($fertilizers as $el) {

                // get user information
                $user_info = $this->getUserByEmail($el->user_email);
                $data = [
                    'id'        => $el->id,
                    'email'     => $user_info['email'],
                    'owner'     => $user_info['name'],
                    'phone'     => $user_info['phone'],
                    'name'      => $el->name,
                    'type'      => "fertilizer",
                    'qty'       => $el->qty,
                    'amount'    => number_format($el->amount, 2),
                    'note'      => $el->note,
                    'avatar'    => $el->avatar,
                    'date'      => $el->created_at->diffForHumans()
                ];

                if(!empty($user_info['name'])){
                    array_push($fertilizer_box, $data);
                }
                
            }
        }
        if(count($seeds) > 0){
            foreach ($seeds as $el) {

                // get user information
                $user_info = $this->getUserByEmail($el->user_email);

                # code...
                $data = [
                    'id'        => $el->id,
                    'email'     => $user_info['email'],
                    'owner'     => $user_info['name'],
                    'phone'     => $user_info['phone'],
                    'name'      => $el->name,
                    'type'      => "seeds",
                    'qty'       => $el->qty,
                    'amount'    => number_format($el->amount, 2),
                    'note'      => $el->note,
                    'avatar'    => $el->avatar,
                    'date'      => $el->created_at->diffForHumans()
                ];
                
                if(!empty($user_info['name'])){
                    array_push($fertilizer_box, $data);
                }
                
            }
        }
        // return response
        return response()->json($fertilizer_box);
    }

    /*
    |-------------------------------------------------
    | delete products
    |-------------------------------------------------
    */ 
    public function deleteEquipment($id)
    {
    	# code...
    	// $find_equipment = DB::table('equipments')->where('id', $id)->get();
        $del_equipment = DB::table('equipments')->where('id', $id)->delete();
        if($del_equipment){
             $data = array(
                'status'  => 'success',
                'message' => 'equipment deleted !'
            );
        }else{
            $data = array(
                'status'  => 'error',
                'message' => 'fail to delete item !'
            );
        }
    
    	// return response
    	return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | delete farm services
    |-------------------------------------------------
    */
    public function deleteFarmService($id, $type)
    {
        // body
        if($type == 'seeds'){
            $seed = new Seed();
            $data = $seed->deleteSeed($id);
        }elseif($type == 'fertilizer'){
            $fert = new Fertilizer();
            $data = $fert->deleteFert($id);
        }

        // comment here.
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | settle payment
    |-------------------------------------------------
    */ 
    public function settlePayment($id)
    {
        # code...
        $settle_payments = Payment::where([['id', $id], ['status', 'settle']])->first();
        if($settle_payments == null){
            // check before authorize settlement
            $client_authorization = Payment::where([['id', $id], ['status', 'approved']])->first();
            if($client_authorization == null){
                // response message
                $msg = 'Settlement Fail, Yeelda awaits client approval !'; 
                // return response
                return redirect()->back()->with('error_msg', $msg);

            }else{
                // clear payment
                $clear_payments = Payment::find($id);
                $clear_payments->status = "settle";
                $clear_payments->update();

                // update transaction
                $update_transaction = Transaction::where('trans_id', $clear_payments->trans_id)->first();
                $transaction = Transaction::find($update_transaction->id);
                $transaction->status = "settle";
                $transaction->update();

                // send notifications to receivers account
                $product_info = Product::where("id", $client_authorization->item_id)->first();
                if($product_info !== null){

                    // get owner email
                    $owner_data = $this->getUserInfo($product_info->user_id);

                    $mail_data = [
                        'name'      => $owner_data['name'],
                        'email'     => $owner_data['email'],
                        'trans_id'  => $client_authorization->pay_id,
                        'amount'    => number_format($client_authorization->amount, 2)
                    ];

                    // send mail to User for settlement
                    \Mail::to($owner_data['email'])->send(new PaymentSentMail($mail_data));
                }

                // response message
                $msg = 'Settlement successful !'; 

                // return response
                return redirect()->back()->with('success_msg', $msg);
            }
        }else{
            // response message
            $msg = 'Already resolved !'; 

            // return response
            return redirect()->back()->with('error_msg', $msg);
        }
    }

    /*
    |-------------------------------------------------
    | load client profile details
    |-------------------------------------------------
    */ 
    public function loadProfile($id, Crop $crops)
    {
        # code...
        // get senders name
        $farmers  = User::where([['id', $id], ['account_type', 'farmer']])->first();
        $service  = User::where([['id', $id], ['account_type', 'service']])->first();
        $investor = User::where([['id', $id], ['account_type', 'buyer']])->first();

        if($farmers !== null){
            // data set for farmers profile
            $basic_info = DB::table('farmer_basics')->where('user_id', $farmers->id)->first();
            // load crops
            $farmers_crops = Crop::where('user_email', $farmers->email)->get();
            $crop_box = [];
            if(count($farmers_crops) > 0){
                foreach ($farmers_crops as $crop) {
                    # code...
                     $data = array(
                        'id'    => $crop->id,
                        'email' => $crop->user_email,
                        'type'  => $crop->type,
                        'note'  => $crop->note,
                        'date'  => $crop->created_at->diffForHumans()
                    );

                    array_push($crop_box, $data);
                }
            }
                
            $data = array(
                'id'      => $farmers->id,
                'name'    => $farmers->name,
                'email'   => $farmers->email,
                'phone'   => $farmers->phone,
                'gender'  => $basic_info->gender,
                'address' => $basic_info->address,
                'state'   => $basic_info->state,
                'zipcode' => $basic_info->zipcode,
                'avatar'  => $basic_info->avatar,
                'office'  => $basic_info->office,
                'mobile'  => $basic_info->mobile,
                'size_of_farm'          => $basic_info->farm_size,
                'type_of_farm'          => ucfirst($basic_info->farm_type),
                'type_of_produce'       => ucfirst($basic_info->farm_crop),
                'previous_harvested'    => ucfirst($basic_info->prevCropPlanted),
                'previous_harvest_size' => ucfirst($basic_info->prevYieldHarvested),
                'amount'                => number_format((int) $basic_info->prevCropHarvested, 2),
                'date_captured'         => $basic_info->date_captured,
                'type'    => 'farmer',
                'crops'   => $crop_box,
                'date'    => $farmers->created_at->diffForHumans()
            );
        }elseif($service !== null){
            
            // data set for services providers
        $basic_info = DB::table('service_basics')->where('user_id', $service->id)->first();
            $data = array(
                'id'      => $service->id,
                'name'    => $service->name,
                'email'   => $service->email,
                'phone'   => $service->phone,
                'gender'  => $basic_info->gender ?? "-",
                'address' => $basic_info->address ?? "-",
                'state'   => $basic_info->state ?? "-",
                'zipcode' => $basic_info->zipcode ?? "-",
                'avatar'  => $basic_info->avatar ?? "-",
                'office'  => $basic_info->office ?? "-",
                'mobile'  => $basic_info->mobile ?? "-",
                'type'    => 'service',
                'date_captured' => $basic_info->date_captured ?? "-",
                'date'    => $service->created_at->diffForHumans()
            );

        }elseif($investor !== null){
            // data set for buyers profile
            $basic_info = DB::table('investor_basics')->where('user_id', $investor->id)->first();
            $data = array(
                'id'      => $investor->id,
                'name'    => $investor->name,
                'email'   => $investor->email,
                'phone'   => $investor->phone,
                'gender'  => $basic_info->gender ?? "-",
                'address' => $basic_info->address ?? "-",
                'state'   => $basic_info->state ?? "-",
                'zipcode' => $basic_info->zipcode ?? "-",
                'avatar'  => $basic_info->avatar ?? "-",
                'office'  => $basic_info->office ?? "-",
                'mobile'  => $basic_info->mobile ?? "-",
                'type'    => 'buyer',
                'date_captured' => $basic_info->date_captured ?? "-",
                'date'    => $investor->created_at->diffForHumans()
            );

        }

        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | deactivated user
    |-------------------------------------------------
    |
    */ 
    public function deactivatedUser(Request $request){
        $email = $request->email;

        # check if user account has been deactivated already
        $user = Account::where('email', $email)->first();
        if($user !== null){

            // response data
            $data = array(
                'status' => 'success',
                'message' => $email.' has already been blocked !'
            );
        }else{
            // then deactivate user
            $account            = new Account();
            $account->status    = 'deactivated';
            $account->email     = $email;
            $account->save();

            // response data
            $data = array(
                'status' => 'success',
                'message' => $email.' has been block from accessing all yeelda services !'
            );
        }

        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | activate user
    |-------------------------------------------------
    |
    */ 
    public function activateUser(Request $request)
    {
        # code...
        $email = $request->email;

        # check if user account has been deactivated already
        $user = Account::where('email', $email)->first();
        if($user !== null){
            // then deactivate user
            $account = Account::find($user->id);
            $account->delete();

            // response data
            $data = array(
                'status' => 'success',
                'message' => $email.' has been unblocked successfully!'
            );
            
        }else{
            
            // response data
            $data = array(
                'status' => 'success',
                'message' => $email.' is active !'
            );
        }

        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | get sender information
    |-------------------------------------------------
    |
    */ 
    public function getUserInfo($id)
    {
        // get senders name
        $check_farmers  = User::where([['id', $id], ['account_type', 'farmer']])->first();
        $check_service  = User::where([['id', $id], ['account_type', 'service']])->first();
        $check_investor = User::where([['id', $id], ['account_type', 'buyer']])->first();

        if($check_farmers !== null){
            $name = $check_farmers->name;
            $email = $check_farmers->email;
        }elseif($check_service !== null){
            $name = $check_service->name;
            $email = $check_service->email;
        }elseif($check_investor !== null){
            $name = $check_investor->name;
            $email = $check_investor->email;
        }else{
            // when user doesn't exits
            $email = "";
            $name = "";
        }

        // return information
        $data = [
            'name' => $name,
            'email' => $email
        ];
        
        return $data;
    }

    /*
    |-------------------------------------------------
    | Get user infomation by email
    |-------------------------------------------------
    */
    public function getUserByEmail($email){
        // body
        $check_farmers  = User::where([['email', $email], ['account_type', 'farmer']])->first();
        $check_service  = User::where([['email', $email], ['account_type', 'service']])->first();
        $check_investor = User::where([['email', $email], ['account_type', 'buyer']])->first();

        if($check_farmers !== null){
            
            $name = $check_farmers->name;
            $email = $check_farmers->email;
            $phone = $check_farmers->phone;

        }elseif($check_service !== null){
            
            $name = $check_service->name;
            $email = $check_service->email;
            $phone = $check_service->phone;
        
        }elseif($check_investor !== null){
            
            $name = $check_investor->name;
            $email = $check_investor->email;
            $phone = $check_investor->phone;
        
        }else{
            // when user doesn't exits
            $email  = "";
            $name   = "";
            $phone  = "";
        }

        // return information
        $data = [
            'name'  => $name,
            'email' => $email,
            'phone' => $phone
        ];
        // return
        return $data;
    }

    /*
    |-------------------------------------------------
    | Load sms logs
    |-------------------------------------------------
    |
    */
    public function loadSmsLogs(){
        $sms_logs = DB::table('s_m_s')->orderBy('id', 'desc')->get();
        if(count($sms_logs) > 0){
            $sms_box = [];
            foreach ($sms_logs as $log) {
                # code...
                $data = [
                    'id'        => $log->id,
                    'phone'     => $log->phone,
                    'body'      => $log->body,
                    'status'    => $log->status,
                    'type'      => $log->type,
                    'date'      => $log->created_at
                ];

                array_push($sms_box, $data);
            }
        }else{
            $sms_box = [];
        }

        // return response
        return response()->json($sms_box);
    }

    /*
    |-------------------------------------------------
    | Load Sent Mail logs
    |-------------------------------------------------
    |
    */
    public function loadSentMail(){
        $admin_message  = new AdminMessage();
        $data           = $admin_message->loadAll();

        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------------
    | Load Sent Mail logs
    |-------------------------------------------------
    |
    */
    public function loadOneSentMail($id){
        $admin_message  = new AdminMessage();
        $data           = $admin_message->loadOne($id);

        // return response
        return response()->json($data);
    }
    
    /*
    |-------------------------------------------------
    | Log sent SMS
    |-------------------------------------------------
    |
    */
    public function logSms($message, $contact, $type){

        // log sms information
        $log_sms = DB::table('s_m_s')->insert([
            'body'      => $message,
            'phone'     => $contact,
            'type'      => $type,
            'status'    => 'sent',
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ]);
    }

    /*
    |-----------------------------------------
    | load stamp messages
    |-----------------------------------------
    */
    public function loadSentStamp(){
        // body
        $stamp_messages = Stamp::orderBy("id", "DESC")->get();
        if(count($stamp_messages) > 0){
            $msg_box = [];
            foreach ($stamp_messages as $el) {
                $data = [
                    'id'        => $el->id,
                    'email'     => $el->user_email,
                    'body'      => $el->body,
                    'status'    => $el->status,
                    'date'      => $el->created_at->diffForHumans()
                ];

                array_push($msg_box, $data);
            }
        }else{
            $msg_box = [];
        }

        // return response.
        return response()->json($msg_box);
    }

    /*
    |-----------------------------------------
    | load users statistic data
    |-----------------------------------------
    */
    public function loadChartStatistic(){
        // body
        $farmers    = User::where('account_type', 'farmer')->count();
        $services   = User::where('account_type', 'service')->count();
        $buyers     = User::where('account_type', 'buyer')->count();

        // body
        $temp_farmers    = TempFarmer::count();
        $temp_services   = TempService::count();
        $temp_buyers     = TempBuyer::count();

        $data = [
            'total_farmers'     => $farmers,
            'total_services'    => $services,
            'total_buyers'      => $buyers,
            'temp_farmers'      => $temp_farmers,
            'temp_services'     => $temp_services,
            'temp_buyers'       => $temp_buyers,
            'temp_overall'      => $temp_farmers + $temp_services + $temp_buyers,
            'total_overall'     => $farmers + $services + $buyers
        ];
 
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load users financial data
    |-----------------------------------------
    */
    public function loadFinancialStatistic(){
        $produce            = new Product();
        $total_farm_cost    = $produce->productTotalCost();

        $equipment          = new Equipment();
        $total_equip_cost   = $equipment->equipmentTotalCost();

        $fertilizer         = new Fertilizer();
        $total_fert_cost    = $fertilizer->fertilizerTotalCost();

        $seedling           = new Seed();
        $total_seeds_cost   = $seedling->seedTotalCost();

        $total_service_cost = $total_seeds_cost + $total_fert_cost;

        $sales_statistics   = new Transaction();
        $total_statisics    = $sales_statistics->getTotalNumbers();

        $filtered_sales         = collect($total_statisics);
            
        $highest_sold_produce   = $filtered_sales->max('result');
        $lowest_sold_produce    = $filtered_sales->min('result');

        $get_highest_demand = $filtered_sales->where("result", $highest_sold_produce)->first();
        $get_lowest_demand  = $filtered_sales->where("result", $lowest_sold_produce)->first();


        $data = [
            'total_farm_cost'       => number_format($total_farm_cost, 2),
            'total_equip_cost'      => number_format($total_equip_cost, 2),
            'total_service_cost'    => number_format($total_service_cost, 2),
            'sales_statistics'      => $total_statisics,
            'highest_sold_produce'  => $get_highest_demand,
            'lowest_sold_produce'   => $get_lowest_demand
        ];
 
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD ALL PAYMENT & TRANSACTION SUM
    |-----------------------------------------
    */
    public function loadPaymentStatistic(){
        // body
        $trans_stats    = new Transaction();
        $data           = $trans_stats->getTransStatistic();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load filterd total amount sold
    |-----------------------------------------
    */
    public function filteredProducesSold($interval){
        // body
        $trans_stats    = new Transaction();
        $data           = $trans_stats->getTransStatisticByDate($interval);
        
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load user by regions
    |-----------------------------------------
    */
    public function loadUserByRegions(){
        // body
        $services = Service::getServicesByLocation();
        $farmers = Farmer::getFarmersByLocation();
        $buyers = Investor::getBuyersByLocation();

        $data = [
            'farmers' => $farmers,
            'services' => $services,
            'buyers' => $buyers,
        ];

        // return response.
        return response()->json($data);   
    }

    /*
    |-----------------------------------------
    | LOAD ALL TRANS REF
    |-----------------------------------------
    */
    public function loadTransRef(Request $request){
        // body
        $reference      = $request->reference;
        $transactions   = new Transaction();
        $data           = $transactions->getTransactionRef($reference);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GENERATE RECEIPT
    |-----------------------------------------
    */
    public function createReceipt(Request $request){
        // body
        $product_ref    = $request->product_ref;
        $carrier_name   = $request->carrier_name;
        $assignee       = $request->assignee;
        $status         = $request->status;
        $seller         = $request->seller;
        $buyer          = $request->buyer;
        $start_date     = $request->start_date;
        $deliver_date   = $request->deliver_date;
        $pickup_address = $request->pickup_address;
        $destination_address = $request->destination_address;

        if(empty($carrier_name)){
            $data = [
                'status'    => 'error',
                'message'   => 'Carrier Name is required!'
            ];
        }elseif(empty($assignee)){
            $data = [
                'status'    => 'error',
                'message'   => 'Enter Assignee'
            ];

        }elseif(empty($start_date)){
            $data = [
                'status'    => 'error',
                'message'   => 'Enter date of dispatch'
            ];
            
        }elseif(empty($deliver_date)){
            $data = [
                'status'    => 'error',
                'message'   => 'Enter date of delivery'
            ];
            
        }elseif(empty($pickup_address)){
            $data = [
                'status'    => 'error',
                'message'   => 'Enter a pickup address, Eg; Yeelda Warehouse'
            ];
            
        }elseif(empty($destination_address)){
            $data = [
                'status'    => 'error',
                'message'   => 'Enter delivery address'
            ];
        }else{
            $tracking   = new TrackProduct();
            $data       = $tracking->addProduct($request);
        }

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD TRACKING 
    |-----------------------------------------
    */
    public function loadTracking(){
        // body
        $tracking   = new TrackProduct();
        $data       = $tracking->loadAll();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD ONE TRACKING
    |-----------------------------------------
    */
    public function loadOneTracking($id){
        // body
        $tracking   = new TrackProduct();
        $data       = $tracking->loadSingle($id);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPDATE ONE TRACKING INFORMATION
    |-----------------------------------------
    */
    public function updateTracking(Request $request){

        if(empty($request->bwl_status)){
            $data = [
                'status'    => 'error',
                'message'   => 'Package status is required!'
            ];
        }else{
            // body
            $update_transit = new TransitLocation();
            $data           = $update_transit->addNewUpdate($request); 
        }
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE ACTIVE USERS
    |-----------------------------------------
    */
    public function deleteActiveUser(Request $request){
        // body
        $user = User::where("email", $request->email)->first();
        if($user !== null){
            if(User::find($user->id)->delete()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'User Account Deleted!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Could not Delete user Account!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not find email'
            ];
        }

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE AGENT ACCOUNT
    |-----------------------------------------
    */
    public function deleteAgentUser(Request $request){
        // body
        if(Agent::find($request->agent_id)->delete()){
            $data = [
                'status'    => 'success',
                'message'   => 'Agent Account Deleted!'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not delete user Account!'
            ];
        }

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE ADMIN ACCOUNT
    |-----------------------------------------
    */
    public function deleteAdminUser(Request $request){

        $admin = Admin::where('id', $request->admin_id)->first();
        if($admin->email == "admin@yeelda.ng"){
            $data = [
                'status'    => 'error',
                'message'   => 'Default admin can not be deleted!'
            ];
        }else{
            // body
            if(Admin::find($request->admin_id)->delete()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Admin Account Deleted!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Could not delete user Account!'
                ];
            }
        }

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | CLEAR CHAT HISTORY
    |-----------------------------------------
    */
    public function clearChatHistory(){
        // body   
    }

    /*
    |-----------------------------------------
    | LOAD OVERALL STATISTIC
    |-----------------------------------------
    */
    public function loadOverallStats(Request $request){
        // body
        $statistics = new PlatformStatistic();
        $data       = $statistics->getOverallStatistics();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET GEO LOCATION AND LAND AREA
    |-----------------------------------------
    */
    public function getGeoLandArea(){
        // body
        $farmer = new Farmer();
        $data   = $farmer->getLandAreaMeasurement();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD DASHBOARD FARMERS
    |-----------------------------------------
    */
    public function loadUsersFarmers(){
        // body
        $users = User::where("account_type", "farmer")->orderBy('id', 'desc')->limit('5')->get();
        $users_box = [];
        foreach ($users as $user) {
            // get farmers basic info
            $basic_info = DB::table('farmer_basics')->where('user_id', $user->id)->first();
            if($basic_info !== null){
                // check account status
                $status = $this->checkBlockAccount($user->email);

                # code...
                $data = [
                    'id'    => $user->id,
                    'name'  => $user->name,
                    'email' => $user->email,
                    'type'  => 'Farmer',
                    'lock'  => $status,
                    'phone' => $user->phone,
                    'date'  => $user->created_at->diffForHumans()
                ];
                array_push($users_box, $data);
            }
        }

        // return responses 
        return response()->json($users_box);
    }

    /*
    |-----------------------------------------
    | LOAD DASHBOARD FARMERS
    |-----------------------------------------
    */
    public function loadUsersService(){
        // body
        $users = User::where("account_type", "service")->orderBy('id', 'desc')->limit('5')->get();
        $users_box = [];
        foreach ($users as $user) {
            // get farmers basic info
            $basic_info = DB::table('service_basics')->where('user_id', $user->id)->first();
            if($basic_info !== null){
                // check account status
                $status = $this->checkBlockAccount($user->email);

                # code...
                $data = [
                    'id'    => $user->id,
                    'name'  => $user->name,
                    'email' => $user->email,
                    'type'  => 'Service',
                    'lock'  => $status,
                    'phone' => $user->phone,
                    'date'  => $user->created_at->diffForHumans()
                ];
                array_push($users_box, $data);
            }
        }

        // return responses 
        return response()->json($users_box);
    }

    /*
    |-----------------------------------------
    | LOAD DASHBOARD FARMERS
    |-----------------------------------------
    */
    public function loadUsersBuyers(){
        // body
        $users = User::where("account_type", "buyer")->orderBy('id', 'desc')->limit('5')->get();
        $users_box = [];
        foreach ($users as $user) {
            // get farmers basic info
            $basic_info = DB::table('investor_basics')->where('user_id', $user->id)->first();
            if($basic_info !== null){
                // check account status
                $status = $this->checkBlockAccount($user->email);

                # code...
                $data = [
                    'id'    => $user->id,
                    'name'  => $user->name,
                    'email' => $user->email,
                    'type'  => 'Buyer',
                    'lock'  => $status,
                    'phone' => $user->phone,
                    'date'  => $user->created_at->diffForHumans()
                ];
                array_push($users_box, $data);
            }
        }

        // return responses 
        return response()->json($users_box);
    }

    /*
    |-----------------------------------------
    | CHECK IF ACCOUNT IS BLOCK
    |-----------------------------------------
    */
    public function checkBlockAccount($email){
        # code...
        $account = Account::where([['email', $email], ['status', 'deactivated']])->first();
        if($account !== null){
            $status = 'locked';
        }else{
            $status = null;
        }

        return $status;
    }

    /*
    |-----------------------------------------
    | DELETE GROUPS
    |-----------------------------------------
    */
    public function deleteGroup(Request $request){
        // body
        $group_id   = $request->group_id;
        $data       = Association::deleteGroup($group_id);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | CLEAR ALL CHAT
    |-----------------------------------------
    */
    public function clearAllChatMessages(){
        // body
        $data = Chat::clearAllMessages();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | FETCH REPORT VIA SORT
    |-----------------------------------------
    */
    public function fetchReportsData(Request $request){

        $states = $request->states;
        $lgas   = $request->lgas;

        $statistics = new PlatformStatistic();
        $data       = $statistics->getAreaCapturedStatistics($states, $lgas);

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | FETCH REPORT VIA BY STATES
    |-----------------------------------------
    */
    public function fetchReportsDataByState(Request $request){
        // body
        $statistics = new PlatformStatistic();
        $data       = $statistics->getAreaStatisticByState($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | FETCH ALL STATES
    |-----------------------------------------
    */
    public function fetchAllStates(Request $request){
        // body
        $all_states = new PlatformStatistic();
        $data       = $all_states->getStates();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | FETCH ALL LGAS in STATES
    |-----------------------------------------
    */
    public function fetchLga(Request $request){
        $states = $request->states;
         
        // body
        $all_states = new PlatformStatistic();
        $data       = $all_states->getLga($states);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | FETCH ALL CLUSTERS IN STATES 
    |-----------------------------------------
    */
    public function fetchLgaClusters(Request $request){
        $states = $request->states;
        $lgas   = $request->lgas;
         
        // body
        $all_states = new PlatformStatistic();
        $data       = $all_states->getLgaClusters($states, $lgas);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | SEARCH AND REPLACE
    |-----------------------------------------
    */
    public function searchAndReplaceLga(Request $request){
        // body
        $correct_data = new PlatformStatistic();
        $data         = $correct_data->fixedLgaKeywords($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | COUNT USERS BY STATES
    |-----------------------------------------
    */
    public function countUsersByState(Request $request){
        $total_users = new PlatformStatistic();
        $data        = $total_users->fetchAndMapUsersByState();
        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET CLUSTERS
    |-----------------------------------------
    */
    public function getClusters(Request $request){
        // body
        $fetch_clusters = new PlatformStatistic();
        $data           = $fetch_clusters->getClustersByState($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET CLUSTER VILLAGES
    |-----------------------------------------
    */
    public function getClusterVillages(Request $request){
        // body
        $fetch_clusters = new PlatformStatistic();
        $data           = $fetch_clusters->getVillagesByCluster($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | SEARCH AND REPLACE CLUSTERS
    |-----------------------------------------
    */
    public function searchAndReplaceCluster(Request $request){
        // body
        $correct_data = new PlatformStatistic();
        $data         = $correct_data->fixedClustersKeywords($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET ALL CLUSTERS REPORTS
    |-----------------------------------------
    */
    public function getClustersReport(Request $request){
        // body
        $cluster_report = new PlatformStatistic();
        $data           = $cluster_report->getReportByCluster($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET AGENT UPDATE REPORTS
    |-----------------------------------------
    */
    public function getAgentUpdateReport(Request $request){
        // body
        $agent_report = new Agent();
        $data         = $agent_report->getAgentUpdateReport($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET AGENT REGION REPORTS
    |-----------------------------------------
    */
    public function getAgentRegionReport(Request $request){
        // body
        $agent_report = new Agent();
        $data         = $agent_report->getReportByRegion($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET REPORT CALLBACK
    |-----------------------------------------
    */
    public function getReportCallback(Request $request){
        // body
        // body
        $agent_report = new Agent();
        $data         = $agent_report->getReportByRegionCallback($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | SEARCH AND REPLACE STATE FEATURE
    |-----------------------------------------
    */
    public function searchAndReplaceState(Request $request){
        // body
        $search_replace = new PlatformStatistic();
        $data           = $search_replace->fixedStateKeywords($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD ALLOWED PRODUCES
    |-----------------------------------------
    */
    public function loadAllowedProduce(){
        // body
        $allowed_produces = new Product();
        $data             = $allowed_produces->listAllDefaultProduce();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | DELETE ALLOWED PRODUCES
    |-----------------------------------------
    */
    public function deleteAllowedProduce(Request $request){
        // body
        $allowed_produces = new Product();
        $data             = $allowed_produces->deleteDefaultProduce($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPDATE TICKER PRICE
    |-----------------------------------------
    */
    public function updateTickerPrice(Request $request){
        // body
        $allowed_produces = new Product();
        $data             = $allowed_produces->updateDefaultPrice($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPDATE TICKER PRICE
    |-----------------------------------------
    */
    public function addAllowedProduce(Request $request){
        // body
        $allowed_produces = new Product();
        $data             = $allowed_produces->addNewProduce($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET TOTAL LAND AREA BY STATE
    |-----------------------------------------
    */
    public function getTotalLandByState(Request $request){
        // body
        $get_land_area = new PlatformStatistic();
        $data          = $get_land_area->getLandAreaByState($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET COUNT TOTAL UNREAD
    |-----------------------------------------
    */
    public function getTotalUnread(){
        // body
        $notification = new AdminNotification();
        $data         = $notification->countUnread();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | getTotalRead
    |-----------------------------------------
    */
    public function getTotalRead(){
        // body
        $notification = new AdminNotification();
        $data         = $notification->countRead();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | getAll
    |-----------------------------------------
    */
    public function getAll(){
        // body
        $notification = new AdminNotification();
        $data         = $notification->getAllNotifications();

        // return response.
        return response()->json($data);
    }
    
    /*
    |-----------------------------------------
    | getSingle
    |-----------------------------------------
    */
    public function getSingle(){
        // body
        $notification = new AdminNotification();
        $data         = $notification->getSingleNotification();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET ESTIMATE VOLUMN PRODUCE
    |-----------------------------------------
    */
    public function getEstimateVolume(Request $request){
        // body
        $statistics     = new PlatformStatistic();
        $data           = $statistics->getEstimateProduceVolume($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | CREATE MEMBERSHIP ACCESS
    |-----------------------------------------
    */
    public function createMemeberShip(Request $request){
        // body
        $view_access    = new ViewAccess();
        $data           = $view_access->createViewAccess($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET USER VIEW ACCESS
    |-----------------------------------------
    */
    public function userViewAccess(Request $request){
        // body
        $view_access    = new ViewAccess();
        $data           = $view_access->allUserViewAccess($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | BLOCK VIEW ACCESS
    |-----------------------------------------
    */
    public function blockViewAccess(Request $request){
        // body
        $view_access    = new ViewAccess();
        $data           = $view_access->lockViewAccess($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | ENABLE VIEW ACCESS
    |-----------------------------------------
    */
    public function enableViewAccess(Request $request){
        // body
        $view_access    = new ViewAccess();
        $data           = $view_access->unlockViewAccess($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET DUPLICATED RECORDS
    |-----------------------------------------
    */
    public function getAllDuplicates(){
        // body
        $duplicate_data = new DuplicateRecord();
        $data           = $duplicate_data->fetchDuplicates();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPDATE BASIC PROFILE RECORDS
    |-----------------------------------------
    */
    public function updateProfile(Request $request){
        // body
        $update_record  = new Farmer();
        $data           = $update_record->updateUserRecord($request);

        // return response.
        return response()->json($data);
    }
}

