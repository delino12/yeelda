<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\AccountResetLink;
use Yeelda\Mail\AccountActivation;
use Yeelda\Activation;

use Yeelda\Farmer;
use Yeelda\FarmerAccount;
use Yeelda\FarmerBasic;

use Yeelda\Service;
use Yeelda\ServiceAccount;
use Yeelda\ServiceBasic;

use Yeelda\Investor;
use Yeelda\InvestorAccount;
use Yeelda\InvestorBasic;

use Yeelda\User;

class MergeAccountController extends Controller
{
    /*
    |-----------------------------------------
    | MERGE ACCOUNT ALL USERS
    |-----------------------------------------
    */
    public function mergeAccount(Request $request){
    	// body
    	$account_type = $request->account_type;
		$total = 0;

    	if($account_type == "farmer"){
    		// migrate data from farmers to users
    		$farmers_table = Farmer::all();
    		if(count($farmers_table) > 0){
    			foreach ($farmers_table as $farmer) {
    				// migration single process
    				$check_already_added = User::where('email', $farmer->email)->first();
    				if($check_already_added == null){
    					$total++;
    					$user = new User();
	    				$user->addUserFromMerge($farmer, "farmer");
    				}
    			}
    		}

    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> $total.' merge from farmers to users '
    		];
    	}elseif($account_type == "service"){
    		// migrate data from farmers to users
    		$services_table = Service::all();
    		if(count($services_table) > 0){
    			foreach ($services_table as $service) {
    				// migration single process
    				$check_already_added = User::where('email', $service->email)->first();
    				if($check_already_added == null){
    					$total++;
    					$user = new User();
	    				$user->addUserFromMerge($service, "service");
    				}
    			}
    		}

    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> $total.' merge from services providers to users '
    		];
    	}elseif($account_type == "buyer"){
    		// migrate data from farmers to users
    		$investors_table = Investor::all();
    		if(count($investors_table) > 0){
    			foreach ($investors_table as $buyer) {
    				// migration single process
    				$check_already_added = User::where('email', $buyer->email)->first();
    				if($check_already_added == null){
    					$total++;
    					$user = new User();
	    				$user->addUserFromMerge($buyer, "buyer");
    				}
    			}
    		}

    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> $total.' merge from buyers to users '
    		];
    	}else{
            $data = [
                'status'    => 'error',
                'message'   => 'Unknown account type'
            ];
        }

        // return response.
        return response()->json($data);
    }
}
