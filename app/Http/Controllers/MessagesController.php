<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Message;
use Yeelda\Farmer;
use Yeelda\Investor;
use Yeelda\Service;
use Auth;

class MessagesController extends Controller
{
    // Authenticate the messaging sections
    public function index()
    {
    	// check for users in three place
    	return view('messages.index');
    }

    // Authenticate the messaging sections
    public function create()
    {
        // check for users in three place
        return view('messages.create');
    }

    // Authenticate the messaging sections
    public function sent()
    {
        // check for users in three place
        return view('messages.sent');
    }

    // Authenticate the messaging sections
    public function draft()
    {
        // check for users in three place
        return view('messages.draft');
    }

    // Authenticate the messaging sections
    public function trash()
    {
        // check for users in three place
        return view('messages.trash');
    }



    public function askSeller($email)
    {
        if(Auth::check()){
            // proceed to messages
            return redirect('/message/create')->with('email', $email);
        }else{
            return redirect('/signin');
        }
    }

    public function replyMsg($id, $email)
    {
    	$msg_id = $id;
    	$email = $email; //incoming from recip

    	return view('messages.reply', compact('email', 'msg_id'));
    }
}



