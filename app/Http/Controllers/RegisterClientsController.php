<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\AccountActivation;
use Yeelda\Activation;
use Yeelda\AdminNotification;

use Yeelda\Farmer;
use Yeelda\FarmerAccount;
use Yeelda\FarmerBasic;

use Yeelda\Service;
use Yeelda\ServiceAccount;
use Yeelda\ServiceBasic;

use Yeelda\Investor;
use Yeelda\InvestorAccount;
use Yeelda\InvestorBasic;

use Yeelda\User;


class RegisterClientsController extends Controller {
	/*
	* Register Clients
	* Get form request
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	public function showSignupForm() {
		return view("auth.register");
	}

	/*|
	|-----------------------------------------------------------------------
	| START REGISTRATION PROCESS
	|-----------------------------------------------------------------------
	|
	*/
	public function doRegistration(Request $request){
		// Before signup check account type
		$account_type = $request->accountType;
		$name     	= $request->names;
		$email    	= $request->email;
		$password 	= $request->password;
		$phone    	= $request->phone;

		$user = new User();
		if($user->addNewUser($name, $email, $password, $phone, $account_type)){
			$msg = 'Registration successful!,  account activation link has been sent!, check '.$email;
			$data = [
				'status' 	=> 'success',
				'message' 	=> $msg
			];

			$this->alertAdmin("signup", $request->all());
		}else{
			$msg = $email." already registered ! ";
			$data = [
				'status' 	=> 'error',
				'message' 	=> $msg
			];
		}

		// return response.
		return response()->json($data);
	}

	/*
	|-----------------------------------------
	| ALERT ADMIN
	|-----------------------------------------
	*/
	public function alertAdmin($type, $contents){
		// body
		$new_notification = new AdminNotification();
		$new_notification->addNewEvent(["category" => $type, "contents" => $contents]);
	}
}