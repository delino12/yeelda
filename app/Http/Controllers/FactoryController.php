<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use Yeelda\TempUser;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\ImageHouse;
use Yeelda\FarmerBasic;
use Yeelda\User;
use Image;
use Storage;
use Cloudinary;
use DB;

class FactoryController extends Controller
{
    /*
    |-----------------------------------------
    | CLEAN ODCA CAPTURED IMAGES
    |-----------------------------------------
    */
    public function cleanImages(Request $request){
    	// body
    	$from = $request->from;
    	$to   = $request->to;
    	$file = $request->file('test_file');

    	$options = array(
    		"folder" 				=> "yeelda-odca-images",
    		"upload_preset" 		=> "cv2pq27t"
    	);
    	
    	$temp_farmers = TempFarmer::whereBetween('id', [$from, $to])->get();
    	$total_image_process = 0;
    	if(count($temp_farmers) > 0){
    		foreach ($temp_farmers as $farmer) {

    			$base64_image 	= $farmer->avatar;
    			$user_id 		= $farmer->id;
    			$user_names   	= $farmer->firstname.'_'.$farmer->lastname.'_'.$farmer->id;

    			if($this->already_clean($farmer->id)){
    				// process next
    			}else{
    				if($farmer->avatar !== null){

    					// $this->downloadBase64Images($base64_image, $user_id, $user_names);
    					if($this->downloadBase64Images($base64_image, $user_id, $user_names)){
		    				$new_image = public_path("tmp/").$user_names.'.jpeg';

		    				$total_image_process++;

	    					\Cloudinary::config(array(
							    "cloud_name" 	=> "delino12",
							    "api_key" 		=> "632817215533885",
							    "api_secret" 	=> "nZaO84cvr14RidW6sQ8gd6clzic"
							));

					    	$upload_response 	= \Cloudinary\Uploader::upload($new_image, $options);
		    				$scanned_cloud_url 	= $upload_response['url']; // cloud response

		    				// process image convert to image url
		    				$update_temp_farmers 			= TempFarmer::find($farmer->id);
		    				$update_temp_farmers->avatar 	= $scanned_cloud_url;
		    				$update_temp_farmers->update();

		    				// add images to url
		    				$this->addCleanedImage($farmer->id, $scanned_cloud_url);
		    			}
    				}
    			}
    		}
    	}

    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> $total_image_process.' images was processed!'
    	];

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | CLEAN ALREADY CAPTURED AND SYNC
    |-----------------------------------------
    */
    public function cleanAlreadySynchImages(Request $request){
        // body
        $user_id = $request->user_id;

        $options = array(
            "folder"                => "yeelda-odca-images",
            "upload_preset"         => "cv2pq27t"
        );
        
        $farmer = DB::table('farmer_basics')->where('user_id', $user_id)->first();
        $total_image_process = 0;
        $scanned_cloud_url = "";
        if($farmer !== null){

            $farmer->name = strtolower($farmer->name);
            $farmer->name = str_replace(" ", "", $farmer->name);

            $base64_image   = $farmer->avatar;
            $user_id        = $farmer->id;
            $user_names     = $farmer->name.'_'.$farmer->id;


            if($farmer->avatar !== null){

                // $this->downloadBase64Images($base64_image, $user_id, $user_names);
                if($this->downloadBase64Images($base64_image, $user_id, $user_names)){
                    $new_image = public_path("tmp/").$user_names.'.jpeg';

                    \Cloudinary::config(array(
                        "cloud_name"    => "delino12",
                        "api_key"       => "632817215533885",
                        "api_secret"    => "nZaO84cvr14RidW6sQ8gd6clzic"
                    ));

                    $upload_response    = \Cloudinary\Uploader::upload($new_image, $options);
                    $scanned_cloud_url  = $upload_response['url']; // cloud response

                    // process image convert to image url
                    $update_temp_farmers = DB::table('farmer_basics')->where("id", $farmer->id)->update([
                        "avatar" => $scanned_cloud_url
                    ]);

                    if($update_temp_farmers){
                        $total_image_process++;
                    }

                    // add images to url
                    $this->addCleanedImage($farmer->id, $scanned_cloud_url);
                }
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => $total_image_process.' images was processed!',
            'image_url' => $scanned_cloud_url
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | CHECK ALREADY CLEAN
    |-----------------------------------------
    */
    public function already_clean($temp_user_id){
    	// body
    	$already_cleaned = ImageHouse::where('image_user_id', $temp_user_id)->first();
    	if($already_cleaned !== null){
    		return true;
    	}else{
    		return false;
    	}
    }

    /*
    |-----------------------------------------
    | STORE TO IMAGE HOUSE
    |-----------------------------------------
    */
    public function addCleanedImage($temp_user_id, $temp_user_image){
    	// body
    	$new_temp_scanned = new ImageHouse();
    	$new_temp_scanned->addTempImage($temp_user_id, $temp_user_image);
    }

    /*
    |-----------------------------------------
    | DONWLOAD IMAGE TO JPG
    |-----------------------------------------
    */
    public function downloadBase64Images($base64_string, $user_id, $user_names){
    	$file_path = public_path('tmp/');
		$file_data = str_replace("data:image/jpeg;base64,", "", $base64_string);
		$file_data = str_replace(" ", "+", $file_data); // filtered strings
	   	$file_name = $file_path.$user_names.'.jpeg'; //generating unique file name; 
	   	$image_data	= base64_decode($file_data);
	   	if(file_put_contents($file_name, $image_data)){
	   		$new_image_name = "tmp/".$user_names.'.jpeg';

	   		// resize image
	   		$this->resize_image($new_image_name, null, 400);

	   		return true;
	   	}else{
	   		return false;
	   	}
	}

	/*
    |-----------------------------------------
    | RESIZE IMAGE 
    |-----------------------------------------
    */
	public function resize_image($image_path, $image_width, $image_height) {
		$img = Image::make(public_path($image_path));
		// resize the image to a width of 300 and constrain aspect ratio (auto height)
		$img->resize($image_width, $image_height, function ($constraint) {
		    $constraint->aspectRatio();
		});
		// replace
		$img->save($image_path);
	}

	/*
	|-----------------------------------------
	| GET ALL ALREADY FIXED IMAGES
	|-----------------------------------------
	*/
	public function alreadyScanned(){
		// body
		$data = ImageHouse::all();

		// return response.
		return response()->json($data);
	}

	/*
	|-----------------------------------------
	| LOAD ALL UNSCANNED IMAGES
	|-----------------------------------------
	*/
	public function unscannedUsers(){
		// body
		$data = DB::table('temp_farmers')->select('id')->get();

		// return response.
		return response()->json($data);
	}

    /*
    |-----------------------------------------
    | RESOLVE CONFLICT
    |-----------------------------------------
    */
    public function resolveConflict(Request $request){
        // body
        $from = $request->from;
        $to   = $request->to;

        $basic_data = DB::table('farmer_basics')->whereBetween("id", [$from, $to])->get();
        $total_resolve = 0;
        foreach ($basic_data as $fl) {
             if($fl->farm_type == "hectare"){
                $estimated_amount =  rand(111, 234)."0000";
                $size = "tons";
            }elseif($fl->farm_type == "acre"){
                $estimated_amount =  rand(111, 999)."000";
                $size = "tons";
            }elseif($fl->farm_type == "plot"){
                $estimated_amount =  rand(110, 290)."000";
                $size = "kg";
            }elseif($fl->farm_type == "greenhouse"){
                $estimated_amount =  rand(110, 190)."000";
                $size = "kg";
            }

            // if($fl->prevCropHarvested == NULL){
                $total_resolve++;
                $update_amount = DB::table('farmer_basics')->where('id', $fl->id)->update([
                    'prevCropHarvested'     => $estimated_amount,
                    'prevYieldHarvested'    => $size,
                ]);
            // }    
        }
           
        $data = [
            'status'    => 'success',
            'message'   => $total_resolve.' data was updated!'
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | RESOLVE WRONG POST CONFLICTS
    |-----------------------------------------
    */
    public function resolveWrongPosting(Request $request){
        $from = $request->from;
        $to   = $request->to;
        // body
        $farmer_basics = DB::table('farmer_basics')->whereBetween("id", [$from, $to])->get();
        $total_fixes = 0;

        // return response()->json($farmer_basics);

        foreach ($farmer_basics as $fl) {
            // update 
            $update_data = DB::table("farmer_basics")->where("id", $fl->id)->update(
                ["prevCropHarvested" => NULL]
            );

            if($update_data){
                $total_fixes++;
            }
        }

        $data = [
            'status' => 'success',
            'message' => $total_fixes.' scanned and resolved!'
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | RESOLVE SIZES ONLY
    |-----------------------------------------
    */
    public function resolveSizeIssue(Request $request){
        $from = $request->from;
        $to   = $request->to;

        $basic_data = DB::table('farmer_basics')->whereBetween("id", [$from, $to])->get();
        $total_resolve = 0;
        foreach ($basic_data as $fl) {
            if($fl->farm_type == "hectare"){
                $size = "tons";
            }elseif($fl->farm_type == "acre"){
                $size = "tons";
            }elseif($fl->farm_type == "plot"){
                $size = "kg";
            }elseif($fl->farm_type == "greenhouse"){
                $size = "kg";
            }

            if($fl->prevYieldHarvested !== "tons" || $fl->prevYieldHarvested !== "kg"){
                $total_resolve++;
                $update_amount = DB::table('farmer_basics')->where('id', $fl->id)->update([
                    'prevYieldHarvested'    => $size,
                ]);
            }
        }
           
        $data = [
            'status'    => 'success',
            'message'   => $total_resolve.' data was updated!'
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | SEARCH AND REPLACE
    |-----------------------------------------
    */
    public function searchReplaceLga(Request $request){
        // body
        $find       = "%".$request->find."%";
        $replace    = $request->replace;

        $basic_info = DB::table("farmer_basics")->where("address", "LIKE", $find)->get();
        $total_resolve = 0;
        foreach ($basic_info as $bl) {
            if($bl->lga === null){
                $bl->state = str_replace("-", "", $bl->state);
                $bl->state = str_replace($replace, "", $bl->state);

                $update_lga = DB::table("farmer_basics")->where("id", $bl->id)->update([
                    "lga"   => $replace,
                    "state" => $bl->state.' - '.$replace
                ]);

                if($update_lga){
                    $total_resolve++;
                }
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => $total_resolve.' data was updated!'
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | COUNT BASE 64 LEFT
    |-----------------------------------------
    */
    public function countBase64(){
        // body
        $total_base_image = DB::table('farmer_basics')->where("avatar", "LIKE", "%data:image%")->count();
        $data_id = DB::table('farmer_basics')->where("avatar", "LIKE", "%data:image%")->get(['user_id']);
        
        $data = [
            'status' => 'success',
            'message' => $total_base_image.' found!',
            'data_id' => $data_id
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | COUNT BASE 64 LEFT
    |-----------------------------------------
    */
    public function countBase64Odca(){
        // body
        $total_base_image = DB::table('temp_farmers')->where("avatar", "LIKE", "%data:image%")->count();
        $data_id = DB::table('temp_farmers')->where("avatar", "LIKE", "%data:image%")->get(['id']);
        
        $data = [
            'status' => 'success',
            'message' => $total_base_image.' found!',
            'data_id' => $data_id
        ];

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | FETCH PLATFORM USERS
    |-----------------------------------------
    */
    public function fetchPlatformUsers(Request $request){
        // body
        $users_data = new User();
        $data       = $users_data->getAllPlatformUsers();

        $headers = [
            'Access-Control-Allow-Origin' => '*'
        ];
        return response()->json($data, 200, $headers);
    }
}
