<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Events\UpdateProgressBar;
use Yeelda\Mail\AccountActivation;
use Yeelda\Activation;

use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;

use Yeelda\Farmer;
use Yeelda\FarmerAccount;
use Yeelda\FarmerBasic;

use Yeelda\Service;
use Yeelda\ServiceAccount;
use Yeelda\ServiceBasic;

use Yeelda\Investor;
use Yeelda\InvestorAccount;
use Yeelda\InvestorBasic;
use Yeelda\User;

use Yeelda\FarmerGroup;
use Yeelda\DuplicateRecord;

use Yeelda\SMS;
use Storage;

class AdminSynchronizationController extends Controller
{
    /*
    |-----------------------------------------------------------------------
    | SYNC USERS ACCOUNT
    |-----------------------------------------------------------------------
    |
    */
    public function syncAccount(Request $request){
    	$userid 	 = $request->userid;
    	$accountType = $request->accountType;
    	$group_id 	 = $request->group_id;

    	// fetch account data
    	$account_data = $this->fetchAccountData($userid, $accountType);

    	// temporary email
    	$userid        = $account_data['id'];
        $names         = $account_data['names'];
        $email         = $account_data['email'];
        $accountType   = $account_data['accountType'];
        $phone         = $account_data['phone'];
        $avatar        = $account_data['avatar'];
        $state         = $account_data['state'];
        $lga           = $account_data['lga'];
        $ward          = $account_data['ward'];
        $language      = $account_data['language'];
        $accountType   = $account_data['accountType'];
        $address       = $account_data['address'];
        $agent_id 	   = $account_data['agent_id'];

        // strict validation
        if(empty($phone)){
        	$data = [
        		'status' 	=> 'error',
        		'message' 	=> 'User Phone number is required!'
        	];
        	return response()->json($data);
        }

        if(empty($names)){
        	$data = [
        		'status' 	=> 'error',
        		'message' 	=> 'User Names is required!'
        	];
        	return response()->json($data);
        }

        if(empty($state)){
        	$data = [
        		'status' 	=> 'error',
        		'message' 	=> 'User State is required!'
        	];
        	return response()->json($data);
        }

        if(empty($address)){
        	$data = [
        		'status' 	=> 'error',
        		'message' 	=> 'User address is required!'
        	];
        	return response()->json($data);
        }

        # state
        $state = $state.' - '.$lga;

        // custom email
        // custom password
        if($email == null){
        	$email = $phone.'@yeelda.ng';
        }

        // default password
        $temp_password 	= 'welcome2yeelda';
        $hash_password	= bcrypt($temp_password);

        if($accountType == 'farmer'){
        	# code...
        	$farmSizeNo    		= $account_data['farmSizeNo'];
            $farmSizeType  		= $account_data['farmSizeType'];
            $farmSizeCrop  		= $account_data['farmSizeCrop'];
            $prevCropHarvested  = $account_data['prevCropHarvested'];
            $prevCropPlanted    = $account_data['prevCropPlanted'];
           	$prevYieldHarvested = $account_data['prevYieldHarvested'];
           	$prevSizeHarvested  = $account_data['prevYieldHarvested'];

           	$payload = [
           		'names' 		=> $names, 
           		'email' 		=> $email, 
           		'phone' 		=> $phone, 
           		'hash_password' => $hash_password, 
           		'avatar' 		=> $avatar, 
           		'language' 		=> $language, 
           		'address' 		=> $address, 
           		'farmSizeNo' 	=> $farmSizeNo, 
           		'farmSizeType' 	=> $farmSizeType, 
           		'farmSizeCrop' 	=> $farmSizeCrop, 
           		'state' 		=> $state,
           		'lga' 			=> $lga,
           		'prevCropHarvested' 	=> $prevCropHarvested, 
           		'prevCropPlanted' 		=> $prevCropPlanted, 
           		'prevYieldHarvested' 	=> $prevYieldHarvested,
           		'prevSizeHarvested' 	=> $prevSizeHarvested,
           		'agent_id' 				=> $agent_id, 
           		'group_id' 				=> $group_id
           	];

            // SIGNUP PROCESS FARMER
			if($this->registerFarmer($payload)){
				// copy file image to new folder
				// update profile image
				if(!empty($avatar)){
					if (strpos($avatar, 'data:') !== false) {
						// echo 'true';
						// skip for base64
					}else{
						$dest = public_path('uploads/farmers/'.$avatar);
						$file = public_path('images/captured-images/'.$avatar);
						if(file_exists($file)){
							// copy images and unlink image from temp folder
							if(copy($file, $dest)){
								unlink(public_path('images/captured-images/'.$avatar));	
							}
						}	
					}
				}else{
					$avatar = "";
				}

				// clean up temp user on farmer
				$temp_user = TempFarmer::where('id', $userid)->first();
				if($temp_user !== null){
					$delete_temp_user = TempFarmer::find($temp_user->id);
					$delete_temp_user->delete();
				}

				// notify user via SMS
				$this->notifySms($phone, $email, $temp_password);
				
				$data = [
					'status' 	=> 'success',
					'message' 	=> $names.' synchronized successfully !'
				];
			}else{
				$data = [
					'status' 	=> 'error',
					'message' 	=> 'synchronization failed !'
				];
			}
        }elseif($accountType == 'service') {

        	# code...
        	$store_capacity	= $account_data['storageCapicity'];     
            $store_type		= $account_data['typeOfStroage'];

            # register user as services providers
            if($this->registerServiceProvider($names, $email, $phone, $hash_password, $avatar, $language, $address, $state, $store_capacity, $store_type)){

            	// copy file image to new folder
				// update profile image
				if(!empty($avatar)){
					if (strpos($avatar, 'data:') !== false) {
						// echo 'true';
						// skip for base64
					}else{
						$dest = public_path('uploads/service/'.$avatar);
						$file = public_path('images/captured-images/'.$avatar);
						if(file_exists($file)){
							// copy images and unlink image from temp folder
							if(copy($file, $dest)){
								unlink(public_path('images/captured-images/'.$avatar));	
							}
						}
					}
				}else{
					$avatar = "";
				}


				// clean up temp user on farmer
				$temp_user = TempService::where('id', $userid)->first();
				if($temp_user !== null){
					$delete_temp_user = TempService::find($temp_user->id);
					$delete_temp_user->delete();
				}

				// notify user via SMS
				$this->notifySms($phone, $email, $temp_password);
				
				$data = [
					'status' 	=> 'success',
					'message' 	=> $names.' synchronized successfully !'
				];
            }else{
            	$data = [
					'status' 	=> 'error',
					'message' 	=> 'synchronization failed !'
				];
            }
        }elseif($accountType == 'buyer') {

        	# code...
        	$company_name    = $account_data['companyName'];
        	$company_produce = $account_data['comPerferPurchase'];

        	# register user as services providers
            if($this->registerBuyers($names, $email, $phone, $hash_password, $avatar, $language, $address, $state, $company_name, $company_produce)){

            	// copy file image to new folder
				// update profile image
				if(!empty($avatar)){

					if (strpos($avatar, 'data:') !== false) {
						// echo 'true';
						// skip for base64
					}else{
						$dest = public_path('uploads/buyers/'.$avatar);
						$file = public_path('images/captured-images/'.$avatar);
						if(file_exists($file)){
							// copy images and unlink image from temp folder
							if(copy($file, $dest)){
								unlink(public_path('images/captured-images/'.$avatar));	
							}
						}
					}
				}else{
					$avatar = "";
				}


				// clean up temp user on farmer
				$temp_user = TempBuyer::where('id', $userid)->first();
				if($temp_user !== null){
					$delete_temp_user = TempBuyer::find($temp_user->id);
					$delete_temp_user->delete();
				}

				// notify user via SMS
				$this->notifySms($phone, $email, $temp_password);
				
				$data = [
					'status' 	=> 'success',
					'message' 	=> $names.' synchronized successfully !'
				];
            }else{
            	$data = [
					'status' 	=> 'error',
					'message' 	=> 'synchronization failed !'
				];
            }
        }

        // update and delete temp farmers
        return response()->json($data);  
    }

    /*|
	|-----------------------------------------------------------------------
	| REGISTRATION FOR FARMER
	|-----------------------------------------------------------------------
	|
	*/
	public function registerFarmer($payload){
		$name 			= $payload['names']; 
		$email 			= $payload['email'];
		$gender 		= $payload['gender'];
		$phone 			= $payload['phone']; 
		$password 		= $payload['hash_password']; 
		$avatar  		= $payload['avatar']; 
		$language 		= $payload['language']; 
		$address 		= $payload['address']; 
		$farmSizeNo 	= $payload['farmSizeNo']; 
		$farmSizeType 	= $payload['farmSizeType']; 
		$farmSizeCrop 	= $payload['farmSizeCrop']; 
		$state        	= $payload['state'];
		$lga        	= $payload['lga'];
		$cluster        = $payload['ward'];
		$prevCropHarvested 	= $payload['prevCropHarvested']; 
		$prevCropPlanted 	= $payload['prevCropPlanted']; 
		$prevYieldHarvested = $payload['prevYieldHarvested']; 
		$prevSizeHarvested 	= $payload['prevSizeHarvested']; 
		$agent_id 			= $payload['agent_id']; 
		$group_id           = $payload['group_id'];
		$date_captured       = $payload['date_captured'];

		# check for user exits 
		$user = User::where('email', $email)->first();
		if($user == null){
			// proceed with registration
			$user 				= new User();
			$user->name 		= $name;
			$user->email 		= $email;
			$user->password 	= $password;
			$user->phone 		= $phone;
			$user->agent_id 	= $agent_id;
			$user->group_id 	= $group_id;
			$user->status   	= "active";
			$user->account_type = "farmer";
			
			// save farmer
			$user->save();
				
			// update other related records
			$user_info = User::where('email', $email)->first();

			// save basic information for farmer
			$farmer_basics 				= new FarmerBasic();
			$farmer_basics->user_id 	= $user_info->id;
			$farmer_basics->name 		= $name;
			$farmer_basics->gender 		= $gender;
			$farmer_basics->avatar 		= $avatar;
			$farmer_basics->state 		= $state;
			$farmer_basics->lga 		= $lga;
			$farmer_basics->cluster 	= $cluster;
			$farmer_basics->address 	= $address;
			$farmer_basics->language 	= $language;
			$farmer_basics->farm_type 	= $farmSizeType;
			$farmer_basics->farm_size	= $farmSizeNo;
			$farmer_basics->farm_crop 	= $farmSizeCrop;
			$farmer_basics->prevCropHarvested 	= $prevCropHarvested;
			$farmer_basics->prevCropPlanted 	= $prevCropPlanted;
			$farmer_basics->prevYieldHarvested 	= $prevYieldHarvested;
			$farmer_basics->date_captured 		= $date_captured;
			$farmer_basics->save();

			// save account information
			$farmer_accounts 			= new FarmerAccount();
			$farmer_accounts->user_id 	= $user->id;
			$farmer_accounts->save();
			
			return true;
		}else{
			return false;
		}
	}

	/*|
	|-----------------------------------------------------------------------
	| REGISTRATION FOR SERVICES PROVIDERS
	|-----------------------------------------------------------------------
	|
	*/
	public function registerServiceProvider($name, $email, $phone, $password, $avatar, $language, $address, $state, $capacity, $type, $agent_id, $group_id){

		# check for user exits 
		$user = User::where('email', $email)->first();
		if($user == null){
			// proceed with registration
			$user 				= new User();
			$user->name 		= $name;
			$user->email 		= $email;
			$user->password 	= $password;
			$user->phone 		= $phone;
			$user->agent_id 	= $agent_id;
			$user->group_id 	= $group_id;
			$user->status   	= "active";
			$user->account_type = "service";
			
			// save farmer
			$user->save();
				
			// update other related records
			$user_info = User::where('email', $email)->first();

			// create account information entry
			$service_account 			= new ServiceAccount();
			$service_account->user_id 	= $user_info->id;
			$service_account->save();

			// create basic information entry
			$service_basic 					= new ServiceBasic();
			$service_basic->user_id 		= $user_info->id;
			$service_basic->avatar 			= $avatar;
			$service_basic->name 			= $name;
			$service_basic->state 			= $state;
			$service_basic->store_capacity 	= $capacity;
			$service_basic->store_type 		= $type;
			$service_basic->language 		= $language;
			$service_basic->save();
			return true;
		}else{
			return false;
		}
	}

	/*|
	|-----------------------------------------------------------------------
	| REGISTRATION FOR BUYERS
	|-----------------------------------------------------------------------
	|
	*/
	public function registerBuyers($name, $email, $phone, $password, $avatar, $language, $address, $state, $company_name, $company_produce, $agent_id, $group_id){

		# check for user exits 
		$user = User::where('email', $email)->first();
		if($user == null){
			// proceed with registration
			$user 				= new User();
			$user->name 		= $name;
			$user->email 		= $email;
			$user->password 	= $password;
			$user->phone 		= $phone;
			$user->agent_id 	= $agent_id;
			$user->group_id 	= $group_id;
			$user->status   	= "active";
			$user->account_type = "buyer";
			
			// save farmer
			$user->save();
				
			// update other related records
			$user_info = User::where('email', $email)->first();

			// create account information entry
			$investor_accounts          = new InvestorAccount();
			$investor_accounts->user_id = $user_info->id;
			$investor_accounts->save();

			// create basic information entry
			$investor_basics          			= new InvestorBasic();
			$investor_basics->user_id 			= $user_info->id;
			$investor_basics->name    			= $name;
			$investor_basics->avatar    		= $avatar;
			$investor_basics->company_name 		= $company_name;
			$investor_basics->company_produce 	= $company_produce;
			$investor_basics->language 			= $language;
			$investor_basics->save();
			return true;
		}else{
			return false;
		}
	}

	/*|
	|-----------------------------------------------------------------------
	| REGISTRATION FOR BUYERS
	|-----------------------------------------------------------------------
	|
	*/
	public function fetchAccountData($id, $accountType){

		$temp_farmers   = TempFarmer::where([['accountType', $accountType], ['id', $id]])->first();
        $temp_service   = TempService::where([['accountType', $accountType], ['id', $id]])->first();
        $temp_buyer     = TempBuyer::where([['accountType', $accountType], ['id', $id]])->first();

        if($temp_farmers !== null){
            
            # code...
            $data = [
                'id'            => $temp_farmers->id,
                'names'         => $temp_farmers->firstname.' '.$temp_farmers->lastname,
                'email'         => $temp_farmers->email,
                'gender'        => $temp_farmers->gender,
                'accountType'   => $temp_farmers->accountType,
                'phone'         => $temp_farmers->phone,
                'avatar'        => $temp_farmers->avatar,
                'state'         => $temp_farmers->state,
                'lga'           => $temp_farmers->lga,
                'ward'          => $temp_farmers->ward,
                'language'      => $temp_farmers->language,
                'address'       => $temp_farmers->address,
                'farmSizeNo'    => $temp_farmers->farmSizeNo,
                'farmSizeType'  => $temp_farmers->farmSizetype,
                'farmSizeCrop'  => $temp_farmers->farmSizeCrop,
                'prevCropHarvested'  	=> $temp_farmers->prevCropHarvested,
                'prevCropPlanted'  		=> $temp_farmers->prevCropPlanted,
                'prevYieldHarvested' 	=> $temp_farmers->prevYieldHarvested,
                'agent_id' 				=> $temp_farmers->agent_id,
                'date_captured'			=> $temp_farmers->date_captured,
            ];

        }elseif($temp_service !== null){

            # code...
            $data = [
                'id'            => $temp_service->id,
                'names'         => $temp_service->firstname.' '.$temp_service->lastname,
                'email'         => $temp_service->email,
                'gender'        => $temp_service->gender,
                'accountType'   => $temp_service->accountType,
                'phone'         => $temp_service->phone,
                'avatar'        => $temp_service->avatar,
                'state'         => $temp_service->state,
                'lga'           => $temp_service->lga,
                'ward'          => $temp_service->ward,
                'language'      => $temp_service->language,
                'address'       => $temp_service->address,
                'storageCapicity' => $temp_service->storageCapicity,              
                'typeOfStroage' => $temp_service->typeOfStroage, 
                'agent_id' 		=> $temp_service->agent_id,
                'date_captured' => $temp_service->date_captured,     
            ];

        }elseif($temp_buyer !== null){

            # code...
            $data = [
                'id'            => $temp_buyer->id,
                'names'         => $temp_buyer->firstname.' '.$temp_buyer->lastname,
                'email'         => $temp_buyer->email,
                'gender'        => $temp_buyer->gender,
                'accountType'   => $temp_buyer->accountType,
                'phone'         => $temp_buyer->phone,
                'avatar'        => $temp_buyer->avatar,
                'state'         => $temp_buyer->state,
                'lga'           => $temp_buyer->lga,
                'ward'          => $temp_buyer->ward,
                'language'      => $temp_buyer->language,
                'accountType'   => $temp_buyer->accountType,
                'address'       => $temp_buyer->address,
                'companyName'   => $temp_buyer->companyName,
                'comPerferPurchase' => $temp_buyer->comPerferPurchase,
                'agent_id' => $temp_buyer->agent_id,
                'date_captured' => $temp_buyer->date_captured,     
            ];

        }else{

            $data = [];
        }

        return $data;
	}

	/*|
	|-----------------------------------------------------------------------
	| SEND ACTIVATION MAIL
	|-----------------------------------------------------------------------
	|
	*/
	public function AccountActivation($email, $name)
	{
		// set token
		$token = bcrypt($email);
		$status = 'inactive';

		// account activations status
		$activation         = new Activation();
		$activation->email  = $email;
		$activation->status = $status;
		$activation->token  = $token;
		$activation->save();

		// mail contents 
		$data = array(
			'name'  => $name,
			'token' => $token 
		);

		// send signed up client activation mail
		\Mail::to($email)->send(new AccountActivation($data));

		return true;
	}

	/*|
	|-----------------------------------------------------------------------
	| SEND ACTIVATION SMS
	|-----------------------------------------------------------------------
	|
	*/
	public function notifySms($phone, $email, $password){

		if(env("APP_ENV") !== "local"){ 
			// send SMS
	        $sms_msg = "Welcome to YEELDA. Username:".$email." Password:".$password." visit www.yeelda.com and start earning Today.";

	        $code = '+234';
	        $phone = substr($phone, 1);
	        $phone = $code.$phone;

	        // send sms
	        $sender = 'YEELDA';
	        $send_sms = new SMS();
	        $send_sms->sendMessages($phone, $sms_msg, $sender);
		}
		
		return true;
	}

	/*
	|-----------------------------------------------------------------------
	| AUTO SYNCHRONIZATION 
	|-----------------------------------------------------------------------
	*/
	public function autoSyncAccount(Request $request){
		// body
		$total_data 	= $request->total;
		$group_id 		= $request->group_id;
		$account_type   = $request->account_type;

		if($account_type == 0){
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'Select Account Type'
			];
		}else{
			// for loop
			for ($i=0; $i < $total_data; $i++) {
				# code...
				if($i == 25){
					// execute synching
					$this->executeAutoSynch($group_id, $account_type, $bash = 25);

					$update_data = [
						'width_percent' => '25%',
						'sync_percent'  => '25',
					];
					\Event::fire(new UpdateProgressBar($update_data));
				}

				if($i == 50){
					// execute synching
					$this->executeAutoSynch($group_id, $account_type, $bash = 25);

					$update_data = [
						'width_percent' => '50%',
						'sync_percent'  => '50',
					];
					\Event::fire(new UpdateProgressBar($update_data));
				}

				if($i == 75){
					// execute synching
					$this->executeAutoSynch($group_id, $account_type, $bash = 25);

					$update_data = [
						'width_percent' => '75%',
						'sync_percent'  => '75',
					];
					\Event::fire(new UpdateProgressBar($update_data));
				}

				if($i == 99){
					// execute synching
					$this->executeAutoSynch($group_id, $account_type, $bash = 25);

					$update_data = [
						'width_percent' => '100%',
						'sync_percent'  => '100',
					];
					\Event::fire(new UpdateProgressBar($update_data));
				}else{
					$data = [
						'status' 	=> 'success',
						'message' 	=> 'Synchronization successful',
					];
				}
			}
		}

		// return response.
		return response()->json($data);
	}

	/*
	|-----------------------------------------------------------------------
	| DEPLOYED AUTO SYNC
	|-----------------------------------------------------------------------
	*/
	public function executeAutoSynch($group_id, $account_type, $bash){

		if($account_type == 1){
			$accountType = 'farmer';
			$users = TempFarmer::orderBy("id", "ASC")->limit('20')->get();
		}elseif($account_type == 2) {
			$accountType = 'service';
			$users = TempService::orderBy("id", "ASC")->limit('20')->get();
		}elseif($account_type == 3){
			$accountType = 'buyer';
			$users = TempBuyer::orderBy("id", "ASC")->limit('20')->get();
		}

		if(count($users) > 0){
			foreach ($users as $user) {
				// process synching
				$userid 	 = $user->id;
		    	$accountType = $user->accountType;

		    	// fetch account data
		    	$account_data = $this->fetchAccountData($userid, $accountType);

		    	// temporary email
		    	$userid        = $account_data['id'];
		        $names         = $account_data['names'];
		        $gender        = $account_data['gender'];
		        $email         = $account_data['email'];
		        $accountType   = $account_data['accountType'];
		        $phone         = $account_data['phone'];
		        $avatar        = $account_data['avatar'];
		        $state         = $account_data['state'];
		        $lga           = $account_data['lga'];
		        $ward          = $account_data['ward'];
		        $language      = $account_data['language'];
		        $accountType   = $account_data['accountType'];
		        $address       = $account_data['address'];
		        $date_captured = $account_data['date_captured'];

		        // strict validation
		        if(!empty($phone) && !empty($names)){
			        // custom email
			        // custom password
			        if($email == null){
			        	$email = $phone.'@yeelda.ng';
			        }

			        // default password
			        $temp_password 	= 'welcome2yeelda';
			        $hash_password	= bcrypt($temp_password);

			        if($accountType == 'farmer'){
			        	# code...
			        	$farmSizeNo    = $account_data['farmSizeNo'];
			            $farmSizeType  = $account_data['farmSizeType'];
			            $farmSizeCrop  = $account_data['farmSizeCrop'];
			            $prevCropHarvested  = $account_data['prevCropHarvested'];
	            		$prevCropPlanted    = $account_data['prevCropPlanted'];
	           			$prevYieldHarvested = $account_data['prevYieldHarvested'];
	           			$prevSizeHarvested  = $account_data['prevYieldHarvested'];
	           			$agent_id 			= $account_data['agent_id'];


			           	$payload = [
			           		'names' 		=> $names, 
			           		'gender' 		=> $gender, 
			           		'email' 		=> $email,
			           		'phone' 		=> $phone, 
			           		'hash_password' => $hash_password, 
			           		'avatar' 		=> $avatar, 
			           		'language' 		=> $language, 
			           		'address' 		=> $address, 
			           		'farmSizeNo' 	=> $farmSizeNo, 
			           		'farmSizeType' 	=> $farmSizeType, 
			           		'farmSizeCrop' 	=> $farmSizeCrop, 
			           		'state' 		=> $state,
			           		'lga' 			=> $lga,
			           		'ward' 			=> $ward,
			           		'prevCropHarvested' 	=> $prevCropHarvested, 
			           		'prevCropPlanted' 		=> $prevCropPlanted, 
			           		'prevYieldHarvested' 	=> $prevYieldHarvested,
			           		'prevSizeHarvested' 	=> $prevSizeHarvested,
			           		'agent_id' 				=> $agent_id, 
			           		'group_id' 				=> $group_id,
			           		'date_captured'			=> $date_captured,
			           	];

			            // SIGNUP PROCESS FARMER
						if($this->registerFarmer($payload)){
							// notify user via SMS
							$this->notifySms($phone, $email, $temp_password);
						}else{
							// save duplicates
							$this->saveDuplicates($payload);
						}

						// clean up temp user on farmer
						$temp_user = TempFarmer::where('id', $userid)->first();
						if($temp_user !== null){
							$delete_temp_user = TempFarmer::find($temp_user->id);
							$delete_temp_user->delete();
						}
			        }elseif($accountType == 'service') {

			        	# code...
			        	$store_capacity	= $account_data['storageCapicity'];     
			            $store_type		= $account_data['typeOfStroage'];

			            # register user as services providers
			            $this->registerServiceProvider($names, $email, $phone, $hash_password, $avatar, $language, $address, $state, $store_capacity, $store_type, $agent_id, $group_id);

						// clean up temp user on farmer
						$temp_user = TempService::where('id', $userid)->first();
						if($temp_user !== null){
							$delete_temp_user = TempService::find($temp_user->id);
							$delete_temp_user->delete();
						}

						// notify user via SMS
						$this->notifySms($phone, $email, $temp_password);
							
			        }elseif($accountType == 'buyer') {

			        	# code...
			        	$company_name    = $account_data['companyName'];
			        	$company_produce = $account_data['comPerferPurchase'];

			        	# register user as services providers
			            $this->registerBuyers($names, $email, $phone, $hash_password, $avatar, $language, $address, $state, $company_name, $company_produce, $agent_id, $group_id);

						// clean up temp user on farmer
						$temp_user = TempBuyer::where('id', $userid)->first();
						if($temp_user !== null){
							$delete_temp_user = TempBuyer::find($temp_user->id);
							$delete_temp_user->delete();
						}

						// notify user via SMS
						$this->notifySms($phone, $email, $temp_password);
			        }
		        }
			}
		}

		// return 
		return true;
	}

	/*
	|-----------------------------------------------------------------------
	| SHOW ADMIN FARMER GROUP 
	|-----------------------------------------------------------------------
	*/
	public function showFamersGroup(){
		// body
		$members_group = FarmerGroup::all();
		if(count($members_group) > 0){
			$members_box = [];
			foreach ($members_group as $mg) {
				$data = [
					'id' 	=> $mg->id,
					'name' 	=> ucfirst($mg->name),
				];

				array_push($members_box, $data);
			}
		}else{
			$members_box = [];
		}

		// return response.
		return response()->json($members_box);
	}

	/*
	|-----------------------------------------------------------------------
	| SAVE DUPLICATES
	|-----------------------------------------------------------------------
	*/
	public function saveDuplicates($payload){
		// body
		$save_duplicated_data = new DuplicateRecord();
		$save_duplicated_data->saveDuplicated($payload);
	}
}
