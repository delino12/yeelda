<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\AccountResetLink;
use Yeelda\Mail\AccountActivation;
use Yeelda\Activation;
use Yeelda\Notification;
use Yeelda\Product;
use Yeelda\Seasonal;
use Yeelda\Rating;

use Yeelda\Farmer;
use Yeelda\FarmerAccount;
use Yeelda\FarmerBasic;

use Yeelda\Service;
use Yeelda\ServiceAccount;
use Yeelda\ServiceBasic;

use Yeelda\Investor;
use Yeelda\InvestorAccount;
use Yeelda\InvestorBasic;

use Yeelda\User;
use Yeelda\NewsHeadline;
use Yeelda\Crop;
use Yeelda\Asset;
use Yeelda\Blog;
use Yeelda\Comment;

use JWTFactory;
use JWTAuth;

use Auth;
use DB;

class WebApiController extends Controller
{
	/*
	|-----------------------------------------
	| AUTHENTICATION MIDDLEWARE
	|-----------------------------------------
	*/
	public function __construct(){
		// body
		$this->middleware('api_auth')->except('loginUser', 'doRegistration', 'resetAccount', 'changePassword', 'produceMarketPlace', 'getBlogPosts', 'addBlogPostComment', 'categoryList', 'getBlogPostById', 'getBogPostComments');
		$this->middleware('verify_signup')->only('doRegistration');
	}

    /*
    |---------------------------------------------
    | LOGIN USER ACCOUNT
    |---------------------------------------------
    */
    public function loginUser(Request $request)
    {

    	$email 			= $request->email;
		$password 		= $request->password;
		$rememberToken 	= $request->remember;

		// verify request
		$this->verifyRequest($request);

		// Attempt Login for farmers
		if (Auth::attempt(['email' => $email, 'password' => $password], $rememberToken)) {
			$data = [
	    		'status' 	=> 'success',
	    		'message' 	=> 'Login was successful!',
	    		'token'     => env("JWT_TEMP_TOKEN")
	    	];
	    	$status = 200;

			// attempt login for services providers
		}else{
			$data = [
	    		'status' 	=> 'error',
	    		'message' 	=> 'Login Failed please check email/password !',
	    	];
	    	$status = 401;

		}

    	// return response
    	return response()->json($data, $status);
    }

    /*|
	|-----------------------------------------------------------------------
	| START REGISTRATION PROCESS
	|-----------------------------------------------------------------------
	|
	*/
	public function doRegistration(Request $request)
	{
		// Before signup check account type
		$account_type = $request->accountType;
		$name     	= $request->name;
		$email    	= $request->email;
		$password 	= $request->password;
		$phone    	= $request->phone;

		$user = new User();
		if($user->addNewUser($name, $email, $password, $phone, $account_type)){
			$msg = 'Registration successful!,  check '.$email.' for account activation link ';
			$data = [
	    		'status' 	=> 'success',
	    		'message' 	=> $msg,
	    		'token'     => env("JWT_TEMP_TOKEN")
	    	];
	    	$status = 200;
		}else{
			$msg = $email." already registered ! ";
			$data = [
	    		'status' 	=> 'success',
	    		'message' 	=> $msg,
	    	];
	    	$status = 401;
		}

		// return response
		return $data;
	}

	/*
	|---------------------------------------------
	| VERIFY REQUEST PAYLOAD
	|---------------------------------------------
	*/
	public function verifyRequest($request){
		if(!$request->has('email')){
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'email field is required!',
			];

			// return
			// return response
			return response()->json($data, 401);
		}

		if(!$request->has('password')){
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'password field is required!',
			];
			// return response
			return response()->json($data, 401);
		}
	}

	/*
	|---------------------------------------------
	| RESET USER ACCCOUNT 
	|---------------------------------------------
	*/
    public function resetAccount(Request $request)
    {
    	// check if users exist
		$email = $request->email;

        // if already exits delete old token
        DB::table('password_resets')->where('email', $email)->delete();

		// get senders name
        $user = User::where('email', $email)->first();

        $token = bcrypt($email);

        // check if users exits 
        if($user !== null){
        	// reset link
        	$reset_link = DB::table('password_resets')->insert([
        		'email' => $email,
        		'token' => $token
        	]);

        	// mail contents
        	$data = [
        		'name'  => $user->name,
        		'email' => $email,
        		'token' => $token 
        	];

        	// fire Mailable
			\Mail::to($email)->send(new AccountResetLink($data));

            // send reset link mail
            $msg = 'Account reset link has been sent to '.$email;
        	$data = [
        		'status'  => 'success',
        		'message' => $msg
        	];

            // return response
        	return response()->json($data, 200);
        }else{

        	$msg = 'Sorry, '.$email.' does not exist, check the email address (keywords) and try again!';
        	$data = [
        		'status'  => 'error',
        		'message' => $msg
        	];
            // return response
        	return response()->json($data, 401);
        }
    }

    /*
    |-----------------------------------------
    | CHANGE USER PASSWORD
    |-----------------------------------------
    */
    public function changePassword(Request $request){
    	// request data 
    	$email    = $request->email;
    	$token    = $request->verify_token;
    	$password = $request->new_password;

    	if($this->verifyAccountResetToken($token) == false){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Invalid reset token'
    		];
    		$status = 401;
    	}else{
    		// encrypt data
	    	$password = bcrypt($password);

	    	// get all users modules
	        $user = User::where('email', $email)->first();

	        // check if users exits 
	        if($user !== null){
	        	// reset password
	        	$user           = User::find($user->id);
	        	$user->password = $password;
	        	$user->update();

	            // send reset msg
	            $msg = 'Password reset successful!';
	        	$data = [
	        		'status'  => 'success',
	        		'message' => $msg
	        	];
	        	$status = 200;

	        	// remove token
	        	$this->deleteResetToken($token);

	        }else{
	        	$data = [
	        		'status'  => 'error',
	        		'message' => 'Invalid user email '.$email
	        	];
	        	$status = 401;
	        }
    	}

    	return response()->json($data, $status);
    }

    /*
    |-----------------------------------------
    | VERIFY ACCOUNT RESET TOKEN
    |-----------------------------------------
    */
    public function verifyAccountResetToken($token){
    	// body
    	$password_resets = DB::table("password_resets")->where("token", $token)->first();
    	if($password_resets == null){
    		return false;
    	}else{
    		return true;
    	}
    }

    /*
    |-----------------------------------------
    | REMOVE TOKEN
    |-----------------------------------------
    */
    public function deleteResetToken($token){
    	// remove token
    	DB::table("password_resets")->where("token", $token)->delete();
    }


    /*
    |-----------------------------------------
    | GET USER PROFILE INFORMATION
    |-----------------------------------------
    */
    public function getProfileInformation($id){

    	$user = User::where("id", $id)->first();
    	if($user !== null){
    		if($user->account_type == "farmer"){
				$details = FarmerBasic::where("user_id", $id)->first();
    		}elseif ($user->account_type == "service") {
    			$details = ServiceBasic::where("user_id", $id)->first();
    		}else{
    			$details = InvestorBasic::where("user_id", $id)->first();
    		}

    		$data = [
    			'name' 		=> $user->name,
    			'account_type' => $user->account_type,
    			'email' 	=> $user->email,
    			'phone' 	=> $user->phone,
    			'status' 	=> $user->status,
    			'biodata' 	=> $details
    		];

    		$status = 200;
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Could not find user'
    		];
    		$status = 404;
    	}

    	// return response.
    	return response()->json($data, $status);
    }

    


/*
|-----------------------------------------------------------------------|
| DASHBOARD SECTION VIEW
|-----------------------------------------------------------------------|
*/
    /*
    |---------------------------------------------
    | SHOW LATEST NEWS HEADLINES
    |---------------------------------------------
    */
    public function loadNews(){
        $news = new NewsHeadline();
        $data = $news->displayNewsHeadlines();

        // return response
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD MESSAGES NOTIFICATIONS
    |-----------------------------------------
    */
    public function getNotifications(Request $request){
        $email = $request->email;
        $all_notifications = Notification::where([['email', $email], ['status', 'unread']])
                            ->orderBy('id', 'desc')
                            ->get();
        $notify_box = [];
        foreach ($all_notifications as $notify) {
            # get all notificatons
            $data = array(
                'id'       => $notify->id,
                'email'    => $notify->email,
                'contents' => json_decode($notify->contents),
                'status'   => $notify->status,
                'date'     => $notify->created_at
            );

            array_push($notify_box, $data);
        }

        return response()->json($notify_box);
    }

    /*
    |-----------------------------------------
    | LOAD USER PRODUCES
    |-----------------------------------------
    */
    public function getProduces(Request $request){
        $product   = new Product();
        $data      = $product->loadProductFromApi($request->email);

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | LOAD USER PRODUCES
    |-----------------------------------------
    */
    public function getCrops(Request $request){
        $crop = new Crop();
        $data = $crop->loadCropFromApi($request->email)
        ;
        // return response.
        return response()->json($data);
    }


/*
|-----------------------------------------------------------------------|
| FARMERS MARKET PLACE 
|-----------------------------------------------------------------------|
*/
    /*
    |-----------------------------------------
    | ADD CROP TAG
    |-----------------------------------------
    */
    public function addCropTag(Request $request){
        $crop = new Crop();
        $data = $crop->addCropFromApi($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | REMOVE CROP TAG
    |-----------------------------------------
    */
    public function removeCropTag(Request $request){
        $id = $request->crop_id;
        $crop = new Crop();
        $data = $crop->deleteCrop($id);

       // return response.
       return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LIST CROP TAG
    |-----------------------------------------
    */
    public function listCropTag(){
        $all_assets = Asset::all();

        $asset_box = [];
        foreach ($all_assets as $e) {
            # code...
            $data = array(
                'id'       => $e->id,
                'asset'    => $e->asset,
                'open'     => number_format($e->open, 2),
                'close'    => number_format($e->close, 2),
                'gap'      => number_format($e->gap, 2),
                'previous' => number_format($e->previous, 2),
                'status'   => $e->status,
                'date'     => $e->created_at
            );

            // push data
            array_push($asset_box, $data);
        }

        // return response
        return response()->json($asset_box);
    }


/*
|-----------------------------------------------------------------------|
| FARMERS MARKET PLACE 
|-----------------------------------------------------------------------|
*/
    /*
    |-----------------------------------------
    | LOAD PRODUCES AT MARKET PLACE
    |-----------------------------------------
    */
    public function produceMarketPlace(){
        // body
        return $products = Product::orderBy("id", "DESC")->paginate('10');
        // $products = Product::orderBy("id", "DESC")->get();
        if(count($products) > 0){
            $product_box = [];
            foreach ($products as $product) {
                //get product owner
                $farmer = Farmer::where('id', $product->user_id)->first();
                $total = $product->product_size_no * $product->product_price;

                // get rating 
                $rating = Rating::where('user_email', $farmer->email)->first();
                
                if(strpos($product->product_image, "YEELDA-") !== false){
                    $product_image = "https://yeelda.com/uploads/products/".$product->product_image;
                }else{
                    $product_image = $product->product_image;
                }

                // save data from farm produce 
                $data = array(
                    "id"                => $product->id,
                    "owner_name"        => $farmer->name,
                    "owner_email"       => $farmer->email,
                    "owner_contact"     => $farmer->phone,
                    // "owner_rating"      => $rating->vote,
                    "product_name"      => $product->product_name,
                    "product_status"    => 'Harvested',
                    "product_size_type" => $product->product_size_type,
                    "product_size_no"   => $product->product_size_no,
                    "product_price"     => $product->product_price,
                    "product_total"     => number_format($total, 2),
                    "product_location"  => $product->product_location,
                    "product_state"     => $product->product_state, 
                    "product_image"     => $product_image, 
                    "product_note"      => $product->product_note, 
                    "created_at"        => $product->created_at
                );

                array_push($product_box, $data);
            }

            # code...
            $seasonal_products = Seasonal::all();
            foreach ($seasonal_products as $seasons) {
                if(strpos($seasons->product_image, "YEELDA-") !== false){
                    $product_image = "https://yeelda.com/uploads/products/".$seasons->product_image;
                }else{
                    $product_image = $seasons->product_image;
                }

                # code...
                $data = array(
                    "id"                => $seasons->id,
                    "owner_name"        => $farmer->name,
                    "owner_email"       => $farmer->email,
                    "owner_contact"     => $farmer->phone,
                    // "owner_rating"      => $rating->vote,
                    "product_name"      => $seasons->product_name,
                    "product_status"    => 'Future Harvest',
                    "product_size_type" => $seasons->product_size_type,
                    "product_size_no"   => $seasons->product_size_no,
                    "product_price"     => $seasons->product_price,
                    "product_total"     => number_format($total, 2),
                    "product_location"  => $seasons->product_location,
                    "product_state"     => $seasons->product_state, 
                    "product_image"     => $product_image, 
                    "product_note"      => $seasons->product_note,
                    "planning_date"     => $seasons->product_planning_date,
                    "delivery_date"     => $seasons->product_delivery_date,
                    "delivery_type"     => $seasons->product_delivery_type,
                    "created_at"        => $seasons->created_at
                );
                array_push($product_box, $data);
            }
        }else{
            $product_box = [];
        }

        // return responses
        return response()->json($product_box, 200);
    }

    /*
    |-----------------------------------------
    | LOAD ALL CATEGORY LIST
    |-----------------------------------------
    */
    public function categoryList(){
        // body
        $list = array(
            'Rice', 'Wheat', 'Soybeans', 
            'Maize', 'Cotton', 'Cassava', 'Palm oil ', 
            'Beans', 'Peanuts', 'Rapeseed', 'Rubber', 
            'Yams', 'Peaches', 'Cacao', 'Sugar', 'Watermelons', 
            'Carrots', 'Coconuts', 'Almonds', 'Lemons', 
            'Strawberries', 'Walnuts'
        );

        // list of produces
        return response()->json($list, 200);
    }



/*
|-----------------------------------------------------------------------|
| BLOG API
|-----------------------------------------------------------------------|
*/
    /*
    |-----------------------------------------
    | GET ALL BLOG POSTS
    |-----------------------------------------
    */
    public function getBlogPosts(){
        // body
        $blogs  = new Blog();
        $data   = $blogs->getAllBlogPosts();

        // return response.
        return response()->json($data, 200);
    }

    /*
    |-----------------------------------------
    | GET BLOG POSTS BY ID
    |-----------------------------------------
    */
    public function getBlogPostById(Request $request){
        // body
        if($request->has('post_id')){
            // post id
            $id     = $request->post_id;

            $blogs  = new Blog();
            $data   = $blogs->getBlogById($id);
            $status = 200;
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Kindly provide a blog post id'
            ];
            $status = 401;
        }

        // return response.
        return response()->json($data, $status);
    }

    /*
    |-----------------------------------------
    | ADD COMMENTS
    |-----------------------------------------
    */
    public function addBlogPostComment(Request $request){
        // body
        if($request->has('name') && $request->has('email')){
            $name   = $request->name;
            $email  = $request->email;
            if(empty($name) && empty($email)){
                // saved 
                $data = [
                    'status'  => 'error',
                    'message' => 'name/email must not be empty!'
                ];
                $status = 401;
            }else{

                if($request->has('blog_id')){
                    $comment    = new Comment();
                    $data       = $comment->addComment($request->blog_id);
                    $status     = 200;
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Could not find blog_id params '
                    ];
                    $status = 401;
                }
            }
        }else{
            // saved 
            $data = [
                'status'  => 'error',
                'message' => 'Enter name and email address!'
            ];
            $status = 401;
        }

        // return response
        return response()->json($data, $status);
    }

    /*
    |-----------------------------------------
    | GET BLOG POST COMMENTS
    |-----------------------------------------
    */
    public function getBogPostComments(Request $request){
        // body
        if($request->has('blog_id')){
            $blog_id = $request->blog_id;
            $get_comments   = new Comment();
            $data           = $get_comments->getAllComments($blog_id);
            $status         = 200;
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not find blog_id params '
            ];
            $status = 401;
        }

        // return response.
        return response()->json($data, $status);
    }
}

