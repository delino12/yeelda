<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\FarmerMarketPage;

class MarketPageSetupController extends Controller
{
    /*
    |-----------------------------------------
    | SAVE SETUP
    |-----------------------------------------
    */
    public function saveSettings(Request $request){
    	// body

    	$farmer_page = new FarmerMarketPage();
    	$data = $farmer_page->updatePageConfig($request);

    	// return response.
    	return response()->json($data);
    }
}
