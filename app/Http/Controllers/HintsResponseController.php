<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Hint;

class HintsResponseController extends Controller
{
    // load product hints 
    public function load()
    {
    	# code...
    	$crops_hints = Hint::all();

    	if(count($crops_hints) > 0){
    		
    		$hint_box = [];
    		foreach ($crops_hints as $crop) {
	    		# code...
	    		array_push($hint_box, $crop->name);
	    	}

	    	// return response
	    	return response()->json($hint_box);

    	}else{

    		$lists = array(
	            'Rice', 'Wheat', 'Soya beans', 'Beans', 'Vegetables',
	            'Maize', 'Cotton', 'Cassava', 'Palm oil ', 'Beans', 'Peanuts', 
	            'Rapeseed', 'Rubber', 'Yams', 'Peaches', 'Cacao', 'Sugar', 
	            'Watermelons', 'Carrots', 'Coconuts', 'Almonds', 'Lemons', 
	            'Strawberries', 'Walnuts'
	        );

	        foreach ($lists as $item) {

	        	# save preloaded items
	        	$save_hints 		= new Hint();
	        	$save_hints->type 	= 'crop';
	        	$save_hints->name 	= $item;
	        	$save_hints->save();
	        }

	        # code...
    		$crops_hints = Hint::all();
    		if(count($crops_hints) > 0){
		        $hint_box = [];
	    		foreach ($crops_hints as $crop) {
		    		# code...
		    		array_push($hint_box, $crop->name);
		    	}
		    }

	    	// return response
	    	return response()->json($hint_box);
    	}	
    }

    // load product size hints
    public function loadSizes()
    {
    	$lists = array(
            'Basket', 'Tons', 'Weighted Bags', 'Truck'
        );

        // return response
	    return response()->json($lists);
    }
}
