<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Mail\PasswordChangeNotification;
use Yeelda\Account;
use Yeelda\Service;
use Yeelda\User;
use Hash;
use Auth;
use DB;

class ServiceJsonController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Services Json Response Section
	|--------------------------------------------------------------------------
	|
	| This section handles all the json response from farmers
	| Guest users can not access the route.
	|
	*/
	// Guard page from external access
	public function __construct() {
		$this->middleware('auth');
	}

    // load profile
    public function loadProfile()
    {
        # Get all basic information details
        // request ajax data
        $id   = Auth::user()->id;
        $info = DB::table('service_basics')->where('user_id', $id)->first();
        $user = DB::table('users')->where('id', $id)->first();

        // check info
        if($info->gender == null){
            $info->gender = 'not available';
        }
        if($info->office == null){
            $info->office = 'not available';
        }
        if($info->address == null){
            $info->address = 'not available';
        }
        if($info->zipcode == null){
            $info->zipcode = 'not available';
        }
        if($info->state == null){
            $info->state = 'not available';
        }

        # code...
        $data = array(
            'id'      => $info->id,
            'user_id' => $info->user_id,
            'name'    => $info->name,
            'email'   => $user->email,
            'gender'  => $info->gender,
            'address' => $info->address,
            'state'   => $info->state,
            'zipcode' => $info->zipcode,
            'avatar'  => $info->avatar,
            'office'  => $info->office,
            'mobile'  => $user->phone,
            'date'    => $info->created_at
        );

        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------
    |   CHANGE PASSWORD FOR SERVICES PROVIDER
    |-------------------------------------------
    |
    */
    public function changePassword(Request $request) {
        // get current user
        $userid     = $request->userid;
        $password   = $request->password;

        $password = str_replace(' ', '', $password);

        // check if user exist
        $check_service = User::where('id', $userid)->first();

        // return $check_service;
        if($check_service !== null){

            // change service provider passowrd
            $services              = User::find($userid);
            $services->password    = bcrypt($password);
            $services->update();

            $data = [
                'status'    => 'success',
                'message'   => 'password has been change successfully !'
            ];

            $mail_data = [
                'name'      => $check_service->name,
                'message'   => 'password change successfully !'
            ];

            \Mail::to($check_service->email)->send(new PasswordChangeNotification($mail_data));
        }else{
           $data = [
                'status'    => 'error',
                'message'   => 'Fail to change password please verify authorised user !'
            ]; 
        }

        return response()->json($data);
    }

    // service upload images
    public function updateImage(Request $request)
    {
    	# code...
    	// get current user
        $user_id = Auth::user()->id;

        // request image file
        $image = $request->file('file');

        // rename image
        $new_name = 'yeelda-'.time().'.'.$image->getClientOriginalExtension();

        // set destination path
        $destinationPath = public_path('/uploads/service/');

        // move image to destination path
        $image->move($destinationPath, $new_name);

        // update basic information
        $update_profile = DB::table('service_basics')->where('user_id', $user_id)->update([
            'avatar' => $new_name,
        ]);

        return redirect()->back();
    }

    // update profile information
    public function updateProfile(Request $request){
        // request from form
        $id      = Auth::user()->id;
        $state   = $request->state; 
        $address = $request->address;
        $postal  = $request->postal;
        $office  = $request->office;
        $mobile  = $request->mobile;
        $gender  = $request->gender;

        // find row id update record
        $update = DB::table('service_basics')->where('user_id', $id)
        ->update([
            'state'   => $state,
            'address' => $address,
            'zipcode' => $postal,
            'office'  => $office,
            'gender'  => $gender,
        ]);

        // update mobile contact information
        $find_service        = User::find($id);
        $find_service->phone = $mobile;
        $find_service->update();

        // check if successful updates
        if($update){
            $data = array(
                'status' => 'success',
                'message' => 'Profile updated successfully !'
            );
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'Fail to update profile !'
            );
        }

        // return response
        return response()->json($data);
    }


    /*
    |-------------------------------------------
    |   DEACTIVATE ACCOUNT
    |-------------------------------------------
    |
    */
    public function deactivateAccount(Request $request)
    {
        # code...
        $userid     = $request->userid;
        $password   = $request->password;
        $password   = str_replace(' ', '', $password);

        // check if user exist
        $services = User::where('id', $userid)->first();

        // return $services;
        if($services !== null){
            $password_hash = $services->password;

            // compare password
            if(Hash::check($password, $password_hash)){

                // check if already deactivate
                $account_deactivated = Account::where('email', $services->email)->first();

                if($account_deactivated !== null){
                    
                    $data = [
                        'status' => 'error',
                        'message' => 'Account has been deactivated already'
                    ];

                }else{
                    $account            = new Account();
                    $account->email     = $services->email;
                    $account->status    = "deactivated";
                    $account->save();

                    $data = [
                        'status' => 'success',
                        'message' => 'Account has been deactivated successfully !'
                    ];
                }
            }else{

                $data = [
                    'status' => 'error',
                    'message' => 'password did not match !'
                ];
            }
        }

        return response()->json($data);
    }
}
