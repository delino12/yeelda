<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Activation;
use Yeelda\Account;
use Yeelda\Investor;
use Yeelda\User;
use Auth;
use DB;

class InvestorsController extends Controller
{
	// Guard page from external login
	public function __construct() {
		$this->middleware('auth');
		$this->middleware('guard_buyer');
	}

    // all users internal pages request view
	public function dashboard() {
		// $logged_email = Auth::user()->email;
		// $activation_status = Activation::where([['email', $logged_email], ['status', 'active']])->first();
		// if($activation_status !== null){
			
 	// 	}else {
		// 	// user dashboard with activation msg
		// 	$acct_status = "Account not activated, Activation link has been sent to ".$logged_email;
		// 	return view('investor-pages.dashboard', compact('acct_status'));
		// }

		return view('investor-pages.dashboard');
	}

	public function profile() {
		// investor details 
		return view('investor-pages.profile');
	}

	public function account() {
		// get current use
		// user account information
		// get current user
		$user_id = Auth::user()->id;

		$card_details = DB::table('investor_accounts')->where('user_id', $user_id)->get();
		$account_details = $card_details;

		return view('investor-pages.account', compact('account_details'));
	}

	public function setting() {
		// user settings
		return view('investor-pages.setting');
	}

	public function message() {
		// users messages and interactions
		return view('investor-pages.messages');
	}

	public function cart() {
		// users shopping carts
		return view('investor-pages.cart');
	}

	public function transaction() {
		// users shopping carts
		return view('investor-pages.transactions');
	}

	public function promotion(){
		// return referrals view
		return view('investor-pages.referrals');
	}
}
