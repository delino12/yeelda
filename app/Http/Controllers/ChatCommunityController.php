<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\Association;
use Yeelda\Chat;
use Auth;

class ChatCommunityController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/signin';

    /*
    |-----------------------------------------
    | AUTHENTICATE CHAT CONTROLLER
    |-----------------------------------------
    */
    public function __construct() {
        $this->middleware('auth');
    }
    /*
    |-----------------------------------------
    | CHAT INDEX
    |-----------------------------------------
    */
    public function index()
    {
    	return view('community.index');
    }

    /*
    |-----------------------------------------
    | HOME CHAT SCREEN
    |-----------------------------------------
    */
    public function chatHome()
    {
        # code...
    	return view('community.chat-screen');
    }

    /*
    |-----------------------------------------
    | DISPLAY GROUP CHAT
    |-----------------------------------------
    */
    public function groupChat($name)
    {

        $email = Auth::user()->email;
        $name = Auth::user()->name;

        // check if user have access
        $assoc = Association::where('members', $email)->orWhere('host', $email)->first();
        if($assoc == null){
            return redirect('/chat/community/home');
        }else{

            $group_name = $assoc->name;
            $all_members = Association::loadAllMembers($group_name);
            # code...
            return view('community.group-chat', compact('name', 'group_name'));
        }
    }
}
