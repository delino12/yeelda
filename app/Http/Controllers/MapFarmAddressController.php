<?php

namespace Yeelda\Http\Controllers;

use Illuminate\Http\Request;
use Yeelda\GoogleMapSearch;

class MapFarmAddressController extends Controller
{
    /*
    |--------------------------------
    | MAP FARM LOCATION BY ADDRESS
    |--------------------------------
    |
    */
    public function fetchByAddress(Request $request){

    	# fetch request
    	$address = $request->address;
    	$country = $request->country;

    	# fetch results
    	$address = $address.', '.$country;

    	# call google map search
    	$fetch_data		= new GoogleMapSearch();
    	$data_results 	= $fetch_data->javisLookForAddress($address);

    	# response data
    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> 'Address search return successful !',
    		'response'  => $data_results
    	];

    	// return response..
    	return response()->json($data);
    }


    /*
    |--------------------------------
    | MAP FARM LOCATION BY CORDINATE
    |--------------------------------
    |
    */
    public function fetchByCordinate(Request $request){

    	# fetch request
    	$longitude	= $request->longitude;
    	$latitude	= $request->latitude;

    	# call google map search
    	$fetch_data		= new GoogleMapSearch();
    	$data_results 	= $fetch_data->javisLookForCordinate($latitude, $longitude);

    	# response data
    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> 'Co-ordinate search return successful !',
    		'response'  => $data_results
    	];

    	// return response..
    	return response()->json($data);
    }
}
