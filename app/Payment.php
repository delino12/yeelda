<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /*
    |-----------------------------------------
    | GET TOTAL TRANSACTIONS
    |-----------------------------------------
    */
    public function getPaymentStatistic(){
    	// body
    	$total_sum 		= Payment::sum('amount');
    	$total_pending 	= Payment::where('status', 'pending')->sum('amount');
    	$total_success  = Payment::where('status', 'settle')->sum('amount');

    	$stat_data = [
    		'total_transaction' 	=> number_format($total_sum, 2),
    		'pending_transaction' 	=> number_format($total_pending, 2),
    		'success_transaction' 	=> number_format($total_success, 2)	
    	];

    	// return response.
    	return response()->json($stat_data);
    }
}
