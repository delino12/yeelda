<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\User;
use Carbon\Carbon;
use DB;

class Agent extends Model
{
    /*
    |-----------------------------------------
    | ADD NEW AGENT
    |-----------------------------------------
    */
    public function addNewAgent($payload){
    	// body
    	if($this->checkAlreadyExist($payload->agent_code)){
    		$data = [
    			'status' => 'error',
    			'message' => $payload->agent_code.' already registered! '
    		];
    	}else{
    		$add_agent 				= new Agent();
    		$add_agent->name 		= $payload->names;
    		$add_agent->email 		= $payload->email;
    		$add_agent->phone 		= $payload->phone;
    		$add_agent->agent_code 	= $payload->agent_code;
    		if($add_agent->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Agent created successfully!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Failed to create agent!'
    			];
    		}
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | CHECK IF AGENT ALREADY EXIST
    |-----------------------------------------
    */
    public function checkAlreadyExist($agent_code){
    	// body
    	$already_exist = Agent::where("agent_code", $agent_code)->first();
    	if($already_exist !== null){
    		return true;
    	}else{
    		return false;
    	}
    }

    /*
    |-----------------------------------------
    | LOAD ALL AGENTS
    |-----------------------------------------
    */
    public function loadAllAgent(){
    	// body
    	$all_agents = Agent::orderBy("id", "DESC")->get();
    	if(count($all_agents) > 0){
    		$agent_box = [];
    		foreach ($all_agents as $el) {

                $capture_data = $this->getTotalCapturedUsers($el->agent_code);

    			$data = [
    				"id" 			=> $el->id,
    				"names" 		=> $el->name,
                    "email"         => $el->email,
    				"total" 		=> $capture_data['total'],
    				"phone" 		=> $el->phone,
    				"agent_id" 	    => $el->agent_code,
    				"date" 			=> $el->created_at->diffForHumans()
    			];
    			array_push($agent_box, $data);
    		}
    	}else{
    		$agent_box = [];
    	}

    	// return 
    	return $agent_box;
    }

    /*
    |-----------------------------------------
    | COUNT TOTAL CAPTURED USERS
    |-----------------------------------------
    */
    public function getTotalCapturedUsers($agent_id){
        $main_farmers   = User::where([["agent_id", $agent_id], ["account_type", "farmer"]])->count();
        $main_services  = User::where([["agent_id", $agent_id], ["account_type", "service"]])->count();
        $main_buyers    = User::where([["agent_id", $agent_id], ["account_type", "buyer"]])->count();

        $total_farmers  = $main_farmers;
        $total_services = $main_services;
        $total_buyers   = $main_buyers;

        $data = [
            'farmers'   => $total_farmers,
            'services'  => $total_services,
            'buyers'    => $total_buyers,
            'total'     => $total_farmers + $total_services + $total_buyers
        ];

        return $data;
    }

    /*
    |-----------------------------------------
    | GET AGENT UPDATED REPORT
    |-----------------------------------------
    */
    public function getAgentUpdateReport($payload){
        $id = $payload->agent_id;
        
        $agent      = Agent::where("id", $id)->first();
        $agent_id   = $agent->agent_code;

        if($payload->update_report == 1){
            // today
            $days = 0;
            $date = Carbon::now();

        }elseif($payload->update_report == 2){
            // last month
            $days = 30;
            $date = Carbon::now()->subDays($days);

        }elseif($payload->update_report == 3){
            // last year
            $days = 365;
            $date = Carbon::now()->subDays($days);

        }else{
            // last one week
            $days = 7;
            $date = Carbon::now()->subDays($days);
        }

        $from   = $date;
        $to     = Carbon::now();

        $farmers    = User::whereBetween("created_at", [$from, $to])
                            ->where([["agent_id", $agent_id], ["account_type", "farmer"]])
                            ->count();
        $services   = User::whereBetween("created_at", [$from, $to])
                            ->where([["agent_id", $agent_id], ["account_type", "service"]])
                            ->count();
        $buyers     = User::whereBetween("created_at", [$from, $to])
                            ->where([["agent_id", $agent_id], ["account_type", "buyer"]])
                            ->count();

        $total_farmers  = $main_farmers;
        $total_services = $main_services;
        $total_buyers   = $main_buyers;

        $data = [
            'farmers'   => $total_farmers,
            'services'  => $total_services,
            'buyers'    => $total_buyers,
            'total'     => $total_farmers + $total_services + $total_buyers
        ];

        return $data;
    }

    /*
    |-----------------------------------------
    | GET REPORT BY REGION
    |-----------------------------------------
    */
    public function getReportByRegion($payload){
        // body
        $geo_area   = $payload->geo_area;
        $id         = $payload->agent_id;

        $agent      = Agent::where("id", $id)->first();
        $agent_id   = $agent->agent_code;

        $results_box = [];
        if($geo_area == "state"){
            // map data via state
            $all_users = User::where("agent_id", $agent_id)->get();
            foreach ($all_users as $user) {
                # code...
                $users_data = DB::table("farmer_basics")->where("user_id", $user->id)->get();
                foreach ($users_data as $ul) {
                    # code...
                    $total_state    = DB::table("farmer_basics")->where("state", $ul->state)->count();

                    $data = [
                        "state"         => $ul->state,
                        "state_total"   => $total_state,
                    ];

                    if($ul->state !== null){
                        if(!in_array($data, $results_box)){
                            array_push($results_box, $data);
                        }
                    }
                }
            }
        }

        // return
        return $results_box;
    }

    /*
    |-----------------------------------------
    | GET REPORT CALLBACK
    |-----------------------------------------
    */
    public function getReportByRegionCallback($payload){
        // body
        $state      = $payload->states;
        $agent_id = Agent::where("id", $payload->agent_id)->first()->agent_code;
        // map data via state

        $users_data = DB::table("farmer_basics")->where("state", $state)->get();
        $results_box = [];
        foreach ($users_data as $ul) {

            if($ul->lga !== null){
                $total = collect(DB::select("SELECT TOP (200) users.id, users.name, users.email, users.account_type, users.status, users.phone, users.password, users.remember_token, users.created_at, users.updated_at, users.group_id, users.agent_id, 
                         users.prevCropHarvested, users.prevYieldHarvested, users.prevCropPlanted, farmer_basics.lga
FROM users INNER JOIN farmer_basics ON users.id = farmer_basics.user_id WHERE (users.agent_id = '".$agent_id."') AND (farmer_basics.lga = '".$ul->lga."')"))->count();
                    
                $data = [
                    "lga"         => $ul->lga,
                    "lga_total"   => $total,
                ];

                if(!in_array($data, $results_box)){
                    array_push($results_box, $data);
                }
            }
        }
        
        // return
        return $results_box;
    }
}
