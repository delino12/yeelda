<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\FarmerGroup;
use Auth;

class ViewAccess extends Model
{
    /*
    |-----------------------------------------
    | GRAND VIEW ACCESS
    |-----------------------------------------
    */
    public function createViewAccess($payload){
    	// body
    	$user_id 	= $payload->user_id;
    	$group_id 	= $payload->group_id;

        // check if access already granted
        $already_exist = ViewAccess::where([["user_id", $user_id], ["group_id", $group_id]])->first();
        if($already_exist !== null){
            $data = [
                'status'    => 'error',
                'message'   => 'Access already granted!'
            ];
        }else{
            $create_access              = new ViewAccess();
            $create_access->user_id     = $user_id;
            $create_access->group_id    = $group_id;
            $create_access->status      = 'active';
            if($create_access->save()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'View access has been created!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Error creating view access!'
                ];
            }
        }

    	// return 
    	return $data;
    }

    /*
    |-----------------------------------------
    | LOCK VIEW ACCESS
    |-----------------------------------------
    */
    public function lockViewAccess($payload){
    	// body
    	$access_id = $payload->access_id;

    	$update_access 				= ViewAccess::find($access_id);
    	$update_access->status 		= 'inactive';
    	if($update_access->update()){
    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> 'View access has been disabled!'
    		];
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Error updating view access!'
    		];
    	}

    	// return 
    	return $data;
    }

    /*
    |-----------------------------------------
    | UNLOCK VIEW ACCESS
    |-----------------------------------------
    */
    public function unlockViewAccess($payload){
    	// body
        $access_id = $payload->access_id;

        $update_access              = ViewAccess::find($access_id);
        $update_access->status      = 'active';
        if($update_access->update()){
            $data = [
                'status'    => 'success',
                'message'   => 'View access has been enabled!'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Error updating view access!'
            ];
        }

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | CHECK ACCESS
    |-----------------------------------------
    */
    public function allUserViewAccess($payload){
        // body
        $all_access = ViewAccess::where("user_id", $payload->user_id)->get();
        $access_box = [];
        foreach ($all_access as $key => $value) {
            $group = FarmerGroup::where("id", $value->group_id)->first();
            $data = [
                "id"     => $value->id,
                "status" => $value->status,
                "group"  => $group,
            ];

            array_push($access_box, $data);
        }

        // return 
        return $access_box;
    }
}
