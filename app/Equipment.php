<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use DB;

class Equipment extends Model
{
    /*
    |-----------------------------------------
    | GET EQUIPMENT TOTAL COST
    |-----------------------------------------
    */
    public function equipmentTotalCost(){
    	// body
    	$all_equipment = DB::table("equipments")->get();
    	if(count($all_equipment) > 0){
    		$equipment_box = [];	
	    	foreach ($all_equipment as $el) {
	    		$data = [
	    			'balance' => floatval($el->equipment_no) * floatval($el->equipment_price)
	    		];

	    		array_push($equipment_box, $data);
	    	}

	    	// sum total
	    	if(count($equipment_box) > 0){
	    		$filtered = collect($equipment_box);
	    		$total    = $filtered->sum('balance');
	    	}else{
	    		$total = 0;
	    	}
	    }else{
	    	$total = 0;
	    }

	    return $total;
    }
}
