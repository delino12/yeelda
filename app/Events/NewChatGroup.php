<?php

namespace Yeelda\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewChatGroup implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    protected $data;
    
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('new-group-message');
    }


    public function broadcastWith()
    {
        return [
            'id'     => $this->data['id'],
            'email'  => $this->data['email'],
            'name'   => $this->data['name'],
            'status' => $this->data['status'],
            'body'   => $this->data['body'],
            'room'   => $this->data['room'],
            'date'   => $this->data['date']
        ];
    }
}
