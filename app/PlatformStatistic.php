<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Yeelda\TempUser;
use Yeelda\TempFarmer;
use Yeelda\TempBuyer;
use Yeelda\TempService;
use Yeelda\Account;
use Yeelda\Equipment;
use Yeelda\Transaction;
use Yeelda\Payment;
use Yeelda\Product;
use Yeelda\Farmer;
use Yeelda\Service;
use Yeelda\Investor;
use Yeelda\Crop;
use Yeelda\SMS;
use Yeelda\Admin;
use Yeelda\Agent;
use Yeelda\AdminMessage;
use Yeelda\Stamp;
use Yeelda\Fertilizer;
use Yeelda\Seed;
use Yeelda\User;
use Yeelda\TrackProduct;
use Yeelda\TransitLocation;
use Yeelda\SmsQueue;
use Carbon\Carbon;
use DB;

class PlatformStatistic extends Model
{
    /*
    |-----------------------------------------
    | GET ALL OVERALL STATISTIC
    |-----------------------------------------
    */
    public function getOverallStatistics(){
    	// body
    	// count from temporary users
    	$temp_total_male 	= TempFarmer::where('gender', 'male')->count();
    	$temp_total_female 	= TempFarmer::where('gender', 'female')->count();

    	// count from main users
    	$main_total_male    = Farmer::getTotalMale();
    	$main_total_female  = Farmer::getTotalFemale();

    	// get highest planted
    	$farmer_info 			= new Farmer();
    	$temp_highest_planted 	= $farmer_info->mostPlantedProduce();

    	$filtered_crops         = collect($temp_highest_planted);
    	$highest_planted_count 	= $filtered_crops->max('result');
    	$lowest_planted_count 	= $filtered_crops->min('result');

    	$highest_planted_crop 	= $filtered_crops->where('result', $highest_planted_count)->first();
    	$lowest_planted_crop 	= $filtered_crops->where('result', $lowest_planted_count)->first();

    	$total_male 	= $temp_total_male + $main_total_male;
    	$total_female 	= $temp_total_female + $main_total_female;

    	$data = [
    		'total_male' 		=> $total_male,
    		'total_female' 		=> $total_female,
    		'highest_planted' 	=> number_format($highest_planted_count),
    		'most_preferred'   	=> $highest_planted_crop,
    		'lowest_planted' 	=> $lowest_planted_count,
    		'least_preferred' 	=> $lowest_planted_crop,
    		'series'  			=> $filtered_crops,
    	];

    	// return 
    	return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL TIME SERIES STATISTIC
    |-----------------------------------------
    */
    public function getTimeSeriesStatistics($query){
        // body
        // count from temporary users
        $temp_total_male    = TempFarmer::where('gender', 'male')->count();
        $temp_total_female  = TempFarmer::where('gender', 'female')->count();

        // count from main users
        $main_total_male    = Farmer::getTotalMale();
        $main_total_female  = Farmer::getTotalFemale();

        // get highest planted
        $farmer_info            = new Farmer();
        $temp_highest_planted   = $farmer_info->mostPlantedProduce();

        $filtered_crops         = collect($temp_highest_planted);
        $highest_planted_count  = $filtered_crops->max('result');
        $lowest_planted_count   = $filtered_crops->min('result');

        $highest_planted_crop   = $filtered_crops->where('result', $highest_planted_count)->first();
        $lowest_planted_crop    = $filtered_crops->where('result', $lowest_planted_count)->first();

        $total_male     = $temp_total_male + $main_total_male;
        $total_female   = $temp_total_female + $main_total_female;

        $data = [
            'total_male'        => $total_male,
            'total_female'      => $total_female,
            'highest_planted'   => number_format($highest_planted_count),
            'most_preferred'    => $highest_planted_crop,
            'lowest_planted'    => $lowest_planted_count,
            'least_preferred'   => $lowest_planted_crop,
            'series'            => $filtered_crops,
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL AREA CAPTURED STATISTIC
    |-----------------------------------------
    */
    public function getAreaCapturedStatistics($states, $lgas){
        // body
        // count from temporary users
        $temp_total_male    = TempFarmer::where([['gender', 'male'], ['state', $states], ['lga', $lgas]])->count();
        $temp_total_female  = TempFarmer::where([['gender', 'female'], ['state', $states], ['lga', $lgas]])->count();

        // count from main users
        $main_total_male    = Farmer::getTotalMaleQuery($states, $lgas);
        $main_total_female  = Farmer::getTotalFemaleQuery($states, $lgas);

        // get highest planted
        $farmer_info            = new Farmer();
        $temp_highest_planted   = $farmer_info->mostPlantedProduceQuery($states, $lgas);

        $filtered_crops         = collect($temp_highest_planted);
        $highest_planted_count  = $filtered_crops->max('result');
        $lowest_planted_count   = $filtered_crops->min('result');

        $highest_planted_crop   = $filtered_crops->where('result', $highest_planted_count)->first();
        $lowest_planted_crop    = $filtered_crops->where('result', $lowest_planted_count)->first();

        $total_male     = $temp_total_male + $main_total_male;
        $total_female   = $temp_total_female + $main_total_female;

        $data = [
            'total_male'        => $total_male,
            'total_female'      => $total_female,
            'highest_planted'   => number_format($highest_planted_count),
            'most_preferred'    => $highest_planted_crop,
            'lowest_planted'    => $lowest_planted_count,
            'least_preferred'   => $lowest_planted_crop,
            'series'            => $filtered_crops,
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET AREA STATISTIC BY STATE
    |-----------------------------------------
    */
    public function getAreaStatisticByState($payload){
        $state = $payload->states;

        $all_lgas   = $this->getLga($state);

        // return $all_lgas;
        $final_box  = [];
        foreach ($all_lgas as $key => $value) {
            $all_farmers = DB::table('farmer_basics')->where('state', $state)->where('lga', $value)->get();

            if(count($all_farmers) > 0){

                $main_total = DB::table("farmer_basics")->where([['lga', $value], ['state', $state]])->count();

                $hectare_box       = [];
                $acre_box          = [];
                $plot_box          = [];
                $greenhouse_box    = [];
                
                foreach ($all_farmers as $el) {
                    $data = [
                        'farm_size' => (int)$el->farm_size,
                        'farm_type' => $el->farm_type,
                        'farm_crop' => $el->farm_crop,
                        'state'     => $el->state,
                        'lga'       => $el->lga,
                    ];

                    if($el->farm_size !== null && $el->farm_type !== null){
                        if($el->farm_type == "hectare"){   
                            array_push($hectare_box, $data);
                        }elseif($el->farm_type == "acre"){
                            array_push($acre_box, $data);
                        }elseif($el->farm_type == "plot"){
                            array_push($plot_box, $data);
                        }elseif($el->farm_type == "greenhouse"){
                            array_push($greenhouse_box, $data);
                        }
                    }
                }


                $hectare_box    = collect($hectare_box);
                $acre_box       = collect($acre_box);
                $plot_box       = collect($plot_box);
                $greenhouse_box = collect($greenhouse_box);

                $total_hectares   = $hectare_box->where("lga", $value)->where("state", $state)->sum("farm_size");
                $total_acres      = $acre_box->where("lga", $value)->where("state", $state)->sum("farm_size");
                $total_plots      = $plot_box->where("lga", $value)->where("state", $state)->sum("farm_size");
                $total_greenhouse = $greenhouse_box->where("lga", $value)->where("state", $state)->sum("farm_size");


                $total_arable = $total_acres / 2.5;
                $total_arable = $total_arable + $total_hectares;

                $data = [
                    'local' => $value,
                    'total_hectares'        => number_format($total_hectares),
                    'total_acres'           => number_format($total_acres),
                    'total_plots'           => number_format($total_plots),
                    'total_greenhouse'      => number_format($total_greenhouse),
                    'area' => $total_arable,
                    'farmers' => $main_total,
                ];

                if(!in_array($data, $final_box)){
                    array_push($final_box, $data);
                }
            }
        }

        // return 
        return $final_box;
    }

    /*
    |-----------------------------------------
    | GET STATES
    |-----------------------------------------
    */
    public function getStates(){
        // body
        $results = file_get_contents(public_path("database/location.json"));
        $data = json_decode($results, true);

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | GET LGA
    |-----------------------------------------
    */
    public function getLga($state){
        // body
        $results = file_get_contents(public_path("database/location.json"));
        $data = json_decode($results, true);

        // to collection
        $data = collect($data)->map(function($item){
            return (Object) $item;
        });

        $filtered_data = [];

        foreach ($data as $key => $value) {
            if($value->name === $state){
                array_push($filtered_data, $value->lga);
            }
        }

        // return
        return $filtered_data[0];
    }

    /*
    |-----------------------------------------
    | GET CLUSTERS
    |-----------------------------------------
    */
    public function getLgaClusters($state, $lga){
        // body
        $all_clusters = DB::table("farmer_basics")->where([["state", $state], ["lga", $lga]])->get();
        $clusters_box = [];
        foreach ($all_clusters as $cl) {
            // if($cl->cluster !== null){
                $data = [
                    "cluster" => $cl->cluster ?? 'No cluster'
                ];

                if(!in_array($data, $clusters_box)){
                    array_push($clusters_box, $data);
                }
            // }
        } 

        // return
        return $clusters_box;
    }

    /*
    |-----------------------------------------
    | FIXED LGA KEYWORDS ERRORS
    |-----------------------------------------
    */
    public function fixedLgaKeywords($payload){
        // body
        $state      = $payload->states;
        $keyword    = $payload->keyword;
        $lga        = $payload->lgas;

        $total_fixed = 0;

        $main_users = DB::table("farmer_basics")->where("lga", $keyword)->get();
        $temp_users = TempFarmer::where("lga", $keyword)->get();

        foreach ($main_users as $user) {
            # code...
            if($user->state !== null && $user->lga !== null){
                $update_main = DB::table("farmer_basics")->where('id', $user->id)->update([
                    "lga" => $lga
                ]);

                if($update_main){
                    $total_fixed++;
                }
            }
        }

        foreach ($temp_users as $user) {
            # code...
            if($user->state !== null && $user->lga !== null){
                $update_temp        = TempFarmer::find($user->id);
                $update_temp->lga   = $lga;
                if($update_temp->update()){
                    $total_fixed++;
                }
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => $total_fixed.' corrected!'
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | FIXED CLUSTERS KEYWORDS ERRORS
    |-----------------------------------------
    */
    public function fixedClustersKeywords($payload){
        // body
        $state      = $payload->state;
        $find       = "%".$payload->find."%";
        $replace    = $payload->replace;
        $lga        = $payload->lga;

        $total_fixed = 0;

        $main_users = DB::table("farmer_basics")->where([
            ["lga", $lga], 
            ["state", $state],
            ["cluster", "LIKE", $find]
        ])->get();
        $temp_users = TempFarmer::where([
            ["lga", $lga], 
            ["state", $state],
            ["ward", "LIKE", $find]
        ])->get();

        foreach ($main_users as $user) {
            # code...
            if($user->state !== null && $user->lga !== null){
                $update_main = DB::table("farmer_basics")->where('id', $user->id)->update([
                    "cluster" => $replace
                ]);

                if($update_main){
                    $total_fixed++;
                }
            }
        }

        foreach ($temp_users as $user) {
            # code...
            if($user->state !== null && $user->lga !== null){
                $update_temp       = TempFarmer::find($user->id);
                $update_temp->ward = $replace;
                if($update_temp->update()){
                    $total_fixed++;
                }
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => $total_fixed.' corrected!'
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET AND COUNT USERS BY STATE.
    |-----------------------------------------
    */
    public function fetchAndMapUsersByState(){
        // body
        $endpoint = "https://www.amcharts.com/lib/4/geodata/json/nigeriaLow.json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $results = curl_exec($ch);
        $data = json_decode($results, true);

        $map_users_box = [];
        foreach ($data["features"] as $key => $value) {
            $state       = $value["properties"]["name"];
            $main_users  = DB::table('farmer_basics')->where("state", $state)->count();
            $total_users = $main_users;

            # code...
            $value["properties"]["total_users"] = $total_users; 

            array_push($map_users_box, $value);
        }

        $new_filtered = [
            "type"      => "FeatureCollection",
            "features"  => $map_users_box
        ];

        // return
        return $new_filtered;
    }

    /*
    |-----------------------------------------
    | GET CLUSTERS BY STATE
    |-----------------------------------------
    */
    public function getClustersByState($payload){
        // body
        $farmers = DB::table('farmer_basics')->where([["state", $payload->state], ["lga", $payload->lga]])->get();
        $clusters_box = [];
        foreach ($farmers as $fl) {
            if($fl->cluster !== null){
                $data = [
                    'name' => $fl->cluster
                ];

                if(!in_array($data, $clusters_box)){
                    array_push($clusters_box, $data);
                }
            }
        }

        // return 
        return $clusters_box;
    }

    /*
    |-----------------------------------------
    | GET REPORTS BY CLUSTERS IN LGA
    |-----------------------------------------
    */
    public function getReportByCluster($payload){
        // body
        $state  = $payload->state;
        $lga    = $payload->lga;

        $result_box = [];
        $arable_box = [];
        $final_box  = [];

        $main_users = DB::table("farmer_basics")->where([["state", $state], ["lga", $lga]])->get();
        foreach ($main_users as $user) {
            # code...
            $total_users = DB::table("farmer_basics")->where([
                ["state", $state], 
                ["lga", $lga], 
                ["cluster", $user->cluster]
            ])->count();

            $user_within_clusters = DB::table("farmer_basics")->where([
                ["state", $state], 
                ["lga", $lga], 
                ["cluster", $user->cluster]
            ])->get();

            $hectare_box       = [];
            $acre_box          = [];
            $plot_box          = [];
            $greenhouse_box    = [];

            foreach ($user_within_clusters as $el) {
                # code...
                $data = [
                    'farm_size' => (int)$el->farm_size,
                    'farm_type' => $el->farm_type,
                    'farm_crop' => $el->farm_crop,
                    'state'     => $el->state,
                    'lga'       => $el->lga,
                    'cluster'   => $el->cluster
                ];

                if($el->farm_size !== null && $el->farm_type !== null){
                    if($el->farm_type == "hectare"){   
                        array_push($hectare_box, $data);
                    }elseif($el->farm_type == "acre"){
                        array_push($acre_box, $data);
                    }elseif($el->farm_type == "plot"){
                        array_push($plot_box, $data);
                    }elseif($el->farm_type == "greenhouse"){
                        array_push($greenhouse_box, $data);
                    }
                }
            }

            $hectare_box    = collect($hectare_box);
            $acre_box       = collect($acre_box);
            $plot_box       = collect($plot_box);
            $greenhouse_box = collect($greenhouse_box);

            $total_hectares   = $hectare_box->where("lga", $lga)->where("state", $state)->sum("farm_size");
            $total_acres      = $acre_box->where("lga", $lga)->where("state", $state)->sum("farm_size");
            $total_plots      = $plot_box->where("lga", $lga)->where("state", $state)->sum("farm_size");
            $total_greenhouse = $greenhouse_box->where("lga", $lga)->where("state", $state)->sum("farm_size");


            $total_arable = $total_acres / 2.5;
            $total_arable = $total_arable + $total_hectares;

            $data = [
                'area'    => $total_arable,
                'farmers' => $total_users,
                'cluster' => $user->cluster,
                'local'   => $state,
                'state'   => $lga,
            ];

            if(!in_array($data, $final_box)){
                array_push($final_box, $data);
            }
        }

        // result box
        return $final_box;
    }

    /*
    |-----------------------------------------
    | SEARCH AND REPLACE FOR STATE
    |-----------------------------------------
    */
    public function fixedStateKeywords($payload){
        // body
        $state      = $payload->state;
        $lga        = $payload->lga;
        $keyword    = $payload->keyword;

        $main_users = DB::table("farmer_basics")->where("state", $keyword)->get();

        $total_fixed = 0;
        foreach ($main_users as $user) {
            # code...
            $update_main = DB::table("farmer_basics")->where('id', $user->id)->update([
                "state"     => $state,
                "lga"       => $lga,
                "cluster"   => $lga
            ]);

            if($update_main){
                $total_fixed++;
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => $total_fixed.' corrected!'
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET CLUSTERS BY VILLAGES
    |-----------------------------------------
    */
    public function getVillagesByCluster($payload){
        // body
        $state      = $payload->state;
        $lga        = $payload->lga;
        $cluster    = $payload->cluster;

        if(empty($cluster)){
            
            $main_users = DB::table("farmer_basics")->where("state", $state)->where("lga", $lga)->get();
            $result_box = [];
            foreach ($main_users as $key => $value) {
                // if($value->cluster !== null){
                    $total_clusters = DB::table("farmer_basics")->where("state", $state)->where("lga", $lga)->where("cluster", $value->cluster)->first();
                    $data = [
                        "cluster"  => $value->cluster ?? 'No cluster',
                        "address"  => $value->address, 
                    ];

                    if(!in_array($data, $result_box)){
                        array_push($result_box, $data);
                    }
                // }
            }

            // return $result_box;

            $collect_results = collect($result_box)->map(function($item){return (Object) $item;});
            $village_box = [];
            foreach ($collect_results as $value) {
                $total_villages = DB::table("farmer_basics")
                                ->where("state", $state)
                                ->where("lga", $lga)
                                ->where("cluster", $value->cluster)
                                ->where("address", $value->address)->count();
                $village_data = [
                    "total"     => $total_villages,
                    "cluster"   => $value->cluster,
                ];

                if(!in_array($village_data, $village_box)){
                    array_push($village_box, $village_data);
                }
            }

            // return $village_box;

            $collect_village = collect($village_box)->map(function($item) {
                return (Object) $item;
            });

            $scanned_box = [];
            foreach ($collect_village as $value) {
                $total_villages = $collect_village->where("cluster", $value->cluster)->sum("total");
                $final_data = [
                    "cluster"  => $value->cluster,
                    "total"    => $total_villages
                ];

                if(!in_array($final_data, $scanned_box)){
                    array_push($scanned_box, $final_data);
                }
            }

        }else{
            $result_box = [];
            $main_users = DB::table("farmer_basics")
                        ->where("state", $state)
                        ->where("lga", $lga)
                        ->where("cluster", $cluster)
                        ->get();

            foreach ($main_users as $key => $value) {
                $total_villages = DB::table("farmer_basics")
                                ->where("state", $state)
                                ->where("lga", $lga)
                                ->where("cluster", $cluster)
                                ->where("address", "LIKE", $value->address)->count();
                $data = [
                    "total"     => $total_villages,
                    "cluster"   => $cluster,
                ];

                if(!in_array($data, $result_box)){
                    array_push($result_box, $data);
                }
            }

            $scanned_result = collect($result_box);
            $total_villages = $scanned_result->sum("total");
            $scanned_box = [];
            array_push($scanned_box, [
                "cluster"  => $cluster,
                "total"    => $total_villages
            ]);
        }

        // return
        return $scanned_box;
    }

    /*
    |-----------------------------------------
    | GET LAND AREA BY STATE
    |-----------------------------------------
    */
    public function getLandAreaByState($payload){
        // body
        $state = $payload->states;

        // body
        $all_farmers = DB::table('farmer_basics')->where('state', $state)->get();


        $hectare_box       = [];
        $acre_box          = [];
        $plot_box          = [];
        $greenhouse_box    = [];

        foreach ($all_farmers as $el) {

            $user = User::whereNotNull("agent_id")->where("id", $el->id)->first();
            // $user = User::where("id", $el->user_id)->first();
            if($user !== null && $user->agent_id !== null){
                $data = [
                    'farm_size' => (int)$el->farm_size,
                    'farm_type' => $el->farm_type,
                    'farm_crop' => $el->farm_crop,
                    'state'     => $el->state,
                    'lga'       => $el->lga,
                ];

                if($el->farm_size !== null && $el->farm_type !== null){
                    if($el->farm_type == "hectare"){   
                        array_push($hectare_box, $data);
                    }elseif($el->farm_type == "acre"){
                        array_push($acre_box, $data);
                    }elseif($el->farm_type == "plot"){
                        array_push($plot_box, $data);
                    }elseif($el->farm_type == "greenhouse(s)"){
                        array_push($greenhouse_box, $data);
                    }
                }
            }
        }


        $hectare_box    = collect($hectare_box);
        $acre_box       = collect($acre_box);
        $plot_box       = collect($plot_box);
        $greenhouse_box = collect($greenhouse_box);

        $total_hectares   = $hectare_box->sum("farm_size");
        $total_acres      = $acre_box->sum("farm_size");
        $total_plots      = $plot_box->sum("farm_size");
        $total_greenhouse = $greenhouse_box->sum("farm_size");


        $total_arable = $total_acres / 2.5;
        $total_arable = $total_arable + $total_hectares;

        $data = [
            'total_hectares'        => number_format($total_hectares),
            'total_acres'           => number_format($total_acres),
            'total_plots'           => number_format($total_plots),
            'total_greenhouse'      => number_format($total_greenhouse),
            'total_arable_hectares' => number_format($total_arable)
        ];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ESTIMATE PRODUCE
    |-----------------------------------------
    */
    public function getEstimateProduceVolume($payload){
        $state = $payload->states;
        
        $all_lgas   = $this->getLga($state);
        $final_box  = [];
        foreach ($all_lgas as $key => $value) {
            $all_farmers = DB::table('farmer_basics')->where('state', $state)->where('lga', $value)->get();

            if(count($all_farmers) > 0){

                $main_total = DB::table("farmer_basics")->where([['lga', $value], ['state', $state]])->count();

                $hectare_box       = [];
                $acre_box          = [];
                $plot_box          = [];
                $greenhouse_box    = [];
                
                foreach ($all_farmers as $el) {

                    $user = User::whereNotNull("agent_id")->where("id", $el->user_id)->first();
                    // $user = User::where("id", $el->user_id)->first();
                    if($user !== null && $user->agent_id !== null){
                        $data = [
                            'farm_size' => (int)$el->farm_size,
                            'farm_type' => $el->farm_type,
                            'farm_crop' => $el->farm_crop,
                            'state'     => $el->state,
                            'lga'       => $el->lga,
                        ];

                        if($el->farm_size !== null && $el->farm_type !== null){
                            if($el->farm_type == "hectare"){   
                                array_push($hectare_box, $data);
                            }elseif($el->farm_type == "acre"){
                                array_push($acre_box, $data);
                            }elseif($el->farm_type == "plot"){
                                array_push($plot_box, $data);
                            }elseif($el->farm_type == "greenhouse"){
                                array_push($greenhouse_box, $data);
                            }
                        }
                    }
                }


                $hectare_box    = collect($hectare_box);
                $acre_box       = collect($acre_box);
                $plot_box       = collect($plot_box);
                $greenhouse_box = collect($greenhouse_box);

                $total_hectares   = $hectare_box->where("lga", $value)->where("state", $state)->sum("farm_size");
                $total_acres      = $acre_box->where("lga", $value)->where("state", $state)->sum("farm_size");
                $total_plots      = $plot_box->where("lga", $value)->where("state", $state)->sum("farm_size");
                $total_greenhouse = $greenhouse_box->where("lga", $value)->where("state", $state)->sum("farm_size");


                $total_arable = $total_acres / 2.5;
                $total_arable = $total_arable + $total_hectares;

                $total_estimated_produce = $total_arable * 15;

                $data = [
                    'local'             => $value,
                    'total_hectares'    => number_format($total_hectares),
                    'total_acres'       => number_format($total_acres),
                    'total_plots'       => number_format($total_plots),
                    'total_greenhouse'  => number_format($total_greenhouse),
                    'area'              => $total_arable,
                    'farmers'           => $main_total,
                    'volume'            => $total_estimated_produce,
                ];

                if(!in_array($data, $final_box)){
                    array_push($final_box, $data);
                }
            }
        }

        // return 
        return $final_box;
    }
}
