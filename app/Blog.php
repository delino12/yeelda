<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\Comment;

class Blog extends Model
{
    /*
    |-----------------------------------------
    | GET BLOG BY ID
    |-----------------------------------------
    */
    public function getBlogById($id){
    	// body
    	$blog_post = Blog::where('id', $id)->first();
    	if($blog_post === null){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Could not find any blog posts with id of '.$id
    		];
    	}else{
    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> 'message'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL BLOG POST
    |-----------------------------------------
    */
    public function getAllBlogPosts(){
    	// body
    	return $blog_post = Blog::orderBy('id', 'desc')->paginate('5');
        if(count($blog_post) > 0){
            $blog_box = [];
            foreach ($blog_post as $post) {
                $blog_comments = Comment::where('blog_id', $post->id)->take('5')->get();
                if(count($blog_comments) > 0){
                    $comment_box = [];
                    foreach ($blog_comments as $comments) {
                        # code...
                        $data = array(
                            'id'      => $comments->id,
                            'blog_id' => $comments->blog_id,
                            'name'    => $comments->user_name,
                            'email'   => $comments->user_email,
                            'body'    => $comments->body,
                            'like'    => $comments->like,
                            'unlike'  => $comments->unlike,
                            'date'    => $comments->created_at
                        );

                        array_push($comment_box, $data);
                    }
                }else{
                    $comment_box = [];
                }
                
                # code...
                $data = array(
                    'id'        => $post->id,
                    'by'        => $post->by,
                    'title'     => $post->title,
                    'body'      => $post->body,
                    'image'     => $post->docs,
                    'like'      => $post->like,
                    'unlike'    => $post->unlike,
                    'category'  => $post->category,
                    'comments'  => $comment_box,
                    'date'      => $post->created_at
                );

                array_push($blog_box, $data);
            }
        }else{
            $blog_box = [];
        }

        // return blog post
        return $blog_box;
    }
}
