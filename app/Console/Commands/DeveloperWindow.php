<?php

namespace Yeelda\Console\Commands;

use Illuminate\Console\Command;

class DeveloperWindow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:command ${query}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run command from developer inputs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = $this->argument('query');

        // execute args
        try {
            if($query == 1){
                exec("php artisan migrate");   
            }elseif($query == 2){
                exec("composer dump-auto");
            }elseif($query == 3){
                exec("composer install"); 
            }elseif($query == 4){
                exec("php artisan down"); 
            }elseif($query == 5){
                exec("php artisan up");  
            }elseif($query == 6){
                exec("touch remote-config.html"); 
            }else{
                // execute default
                exec($query);
            }
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }
}
