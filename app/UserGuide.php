<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserGuide extends Model
{
    /*
    |-----------------------------------------
    | CHECK IF USER ALREADY VISITED
    |-----------------------------------------
    */
    public function checkAlreadyVisit(){
    	// body
    	$user_guide = UserGuide::where("user_id", Auth::user()->id)->first();
    	if($user_guide == null){

    		$new_user_guide 			= new UserGuide();
    		$new_user_guide->user_id 	= Auth::user()->id;
    		$new_user_guide->status 	= true;
    		$new_user_guide->save();

    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> 'New User Guide'
    		];
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'User already visited'
    		];
    	}

    	// return
    	return $data;
    }
}
