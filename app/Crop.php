<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Crop extends Model
{
    /*
    |-----------------------------------------
    | DELETE CROP
    |-----------------------------------------
    */
    public function deleteCrop($id){
    	// body
    	$email = Auth::user()->email;
    	 # code...
        $check_crop = Crop::where('id', $id)->first();
        if($check_crop == null){
            // not item found
            $data = [
                'status'    => 'error',
                'message'   => $check_crop->type.' not found!'
            ];
        }else{
            // item found and delete
            $del_crop = Crop::find($id);
            $del_crop->delete();

            $data = [
                'status'    => 'success',
                'message'   => $check_crop->type.' deleted!'
            ];
        }

        return $data;
    }

    /*
    |-----------------------------------------
    | LOAD CROP
    |-----------------------------------------
    */
    public function loadCrop(){
    	// body
    	$email = Auth::user()->email;
        $crops = Crop::where('user_email', $email)->get();
        if(count($crops) > 0){
            $crop_box = [];
            foreach ($crops as $crop) {
                $data = [
                    'id'    => $crop->id,
                    'email' => $crop->user_email,
                    'type'  => $crop->type,
                    'note'  => $crop->note,
                    'date'  => $crop->created_at->diffForHumans()
                ];

                array_push($crop_box, $data);
            }

            $data = [
                'status'    => 'success',
                'crops'     => $crop_box
            ];

        }else{

        	// farmers has no crops yet
            $data = [
                'status'    => 'info',
                'message'   => 'farmers has no tags on crops yet '
            ];
        }

        // return add crops tags
        return $data;
    }

    /*
    |-----------------------------------------
    | LOAD CROP FROM API
    |-----------------------------------------
    */
    public function loadCropFromApi($email){
        // body
        $crops = Crop::where('user_email', $email)->get();
        if(count($crops) > 0){
            $crop_box = [];
            foreach ($crops as $crop) {
                $data = [
                    'id'    => $crop->id,
                    'email' => $crop->user_email,
                    'type'  => $crop->type,
                    'note'  => $crop->note,
                    'date'  => $crop->created_at
                ];

                array_push($crop_box, $data);
            }

            $data = [
                'status'    => 'success',
                'crops'     => $crop_box
            ];

        }else{

            // farmers has no crops yet
            $data = [
                'status'    => 'info',
                'message'   => 'farmers has no tags on crops yet '
            ];
        }

        // return add crops tags
        return $data;
    }

    /*
    |-----------------------------------------
    | ADD CROP
    |-----------------------------------------
    */
    public function addCrop($request){
    	// email
    	$email = Auth::user()->email;

    	// body
        $type = $request->type;
        $note = $request->note;

        // check if already added
        $already_add = Crop::where([['type', $type], ['user_email', $email]])->first();
        if($already_add !== null){
            // already added item
            $data = [
                'status' => 'error',
                'message' => $type.' already added !'
            ];
        }else{
            // new incoming item
            $add_crop               = new Crop();
            $add_crop->user_email   = $email;
            $add_crop->type         = $type;
            $add_crop->note         = $note;
            $add_crop->save();

            // already added item
            $data = [
                'status' => 'success',
                'message' => $type.' added successfully !'
            ];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | ADD CROP WITH API
    |-----------------------------------------
    */
    public function addCropFromApi($request){
        // body
        $type   = $request->type;
        $note   = "none";
        $email  = $request->email;

        // check if already added
        $already_add = Crop::where([['type', $type], ['user_email', $email]])->first();
        if($already_add !== null){
            // already added item
            $data = [
                'status' => 'error',
                'message' => $type.' already added !'
            ];
        }else{
            // new incoming item
            $add_crop               = new Crop();
            $add_crop->user_email   = $email;
            $add_crop->type         = $type;
            $add_crop->note         = $note;
            $add_crop->save();

            // already added item
            $data = [
                'status' => 'success',
                'message' => $type.' added successfully !'
            ];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | DELETE CROP
    |-----------------------------------------
    */
    public function deleteCropFromApi($request){
        // body
        $id = $request->id;
        $email = $request->email;
         # code...
        $check_crop = Crop::where([['user_email', $email],['id', $id]])->first();
        if($check_crop == null){
            // not item found
            $data = [
                'status'    => 'error',
                'message'   => $check_crop->type.' not found!'
            ];
        }else{
            // item found and delete
            $del_crop = Crop::find($id);
            $del_crop->delete();

            $data = [
                'status'    => 'success',
                'message'   => $check_crop->type.' deleted!'
            ];
        }

        return $data;
    }
}
