<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Auth;

class FarmerMarketPage extends Model
{
    /*
    |-----------------------------------------
    | UPDATE PAGE CONFIGURATION
    |-----------------------------------------
    */
    public function updatePageConfig($payload){
    	$update_config = FarmerMarketPage::find($payload->page_id);
        $payload->config = [
            'page_id' => $payload->page_id,
            'page_background_image' => $payload->page_background_image,
            'page_background_color' => $payload->page_background_color,
            'page_color' => $payload->page_color,
            'page_search' => $payload->page_search,
            'farmer_biz_name' => $payload->farmer_biz_name,
            'farmer_biz_description' => $payload->farmer_biz_description,
        ];
    	if($update_config !== null){
    		$update_config->config = json_encode($payload->config);
    		if($update_config->update()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Updated!'
    			];
			}else{
				$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error, fail to save changes!'
    			];
			}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'No page id and user information found!'
    		];
    	}

        return $data;
    }

    /*
    |-----------------------------------------
    | INIT PAGE CONFIGURATION
    |-----------------------------------------
    */
    public function initPageConfig($user_id){
    	// body
    	$check_already_existed = FarmerMarketPage::where("user_id", $user_id)->first();
    	if($check_already_existed === null){
    		// create new page user configuration
    		$new_config 			= new FarmerMarketPage();
    		$new_config->user_id 	= $user_id;
    		$new_config->config 	= json_encode([
                'page_background_image' => env("APP_URL")."/images/bg_images/7.jpg",
                'page_background_color' => 'FFFFFF',
                'page_color' => '999999',
                'page_search' => true,
            ]);
    		$new_config->save();
            return true;
    	}
    }
}
