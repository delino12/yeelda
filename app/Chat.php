<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    /*
    |-----------------------------------------
    | CLEAR OLD CHAT MESSAGES
    |-----------------------------------------
    */
    public function clearOldMessages(){
    	// body
    	$chat_messages = Chat::where("room", "general")->get();
    	if(count($chat_messages) > 100){
    		foreach ($chat_messages as $ch) {
    			$delete_chat = Chat::find($ch->id);
    			$delete_chat->delete();
    		}
    	}

    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> 'Chat History compressed!'
    	];

    	// return
    	return $data;
    }


    /*
    |-----------------------------------------
    | CLEAR ALL CHAT MESSAGES
    |-----------------------------------------
    */
    public static function clearAllMessages(){
        // body
        Chat::where("room", "general")->delete();

        $data = [
            'status'    => 'success',
            'message'   => 'Messages has been deleted!'
        ];

        // return
        return $data;
    }
}
