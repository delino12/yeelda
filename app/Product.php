<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\User;
use Yeelda\Asset;
use Yeelda\Seasonal;
use DB;
use Auth;

class Product extends Model
{
    /*
    |-----------------------------------------
    | GET PRODUCT TOTAL COST
    |-----------------------------------------
    */
    public function productTotalCost(){
    	// body
    	$all_produce = Product::all();
    	if(count($all_produce) > 0){
    		$produce_box = [];	
	    	foreach ($all_produce as $el) {
	    		$data = [
	    			'balance' => $el->product_size_no * $el->product_price 
	    		];

	    		array_push($produce_box, $data);
	    	}

	    	// sum total
	    	if(count($produce_box) > 0){
	    		$filtered = collect($produce_box);
	    		$total    = $filtered->sum('balance');
	    	}else{
	    		$total = 0;
	    	}
	    }else{
	    	$total = 0;
	    }

	    return $total;
    }

    /*
    |-----------------------------------------
    | DELETE OR REMOVE PRODUCE
    |-----------------------------------------
    */
    public function deleteProduce($id){
    	// body
    	$products = Product::find($id);
        $products->delete();

        $data = [
			'status' 	=> 'success',
			'message' 	=> 'deleted!'
		];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | LOAD SINGLE PRODUCE
    |-----------------------------------------
    */
    public function loadSingle($id){
    	// body
        $product = Product::find($id);

        if($product === null){
            $product = Seasonal::find($id);
        }else{

        }

        // get product owner
        $farmer = User::where('id', $product->user_id)->first();
        if($farmer !== null){
        	// get producer information
	        $basic_info = DB::table('farmer_basics')->where('user_id', $product->user_id)->first();
	        $total = $product->product_size_no * $product->product_price;
	        $data = [
	            "id"                => $product->id,
	            "owner_name"        => $farmer->name,
	            "owner_email"       => $farmer->email,
	            "owner_contact"     => $farmer->phone,
	            "owner_office"      => $basic_info->office,
	            "owner_image"       => $basic_info->avatar,
	            "owner_address"     => $basic_info->address,
	            "owner_zipcode"     => $basic_info->zipcode,
	            "owner_gender"      => $basic_info->gender,
	            "owner_country"     => "Nigeria",
	            "product_name"      => $product->product_name,
	            "product_size_type" => $product->product_size_type,
	            "product_size_no"   => $product->product_size_no,
	            "product_price"     => number_format($product->product_price, 2),
	            "product_total"     => number_format($total, 2),
	            "product_location"  => $product->product_location,
	            "product_state"     => $product->product_state, 
	            "product_image"     => $product->product_image, 
	            "product_note"      => $product->product_note, 
	            "created_at"        => $product->created_at->diffForHumans() 
	        ];
        }else{
        	$data = [];
        }

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | LOAD USER PRODUCES
    |-----------------------------------------
    */
    public function loadProduct(){
    	// body
    	$id = Auth::user()->id;
    	
    	// load all products 
    	$products = Product::where('user_id', $id)->get();

    	if(count($products) > 0){
    		// scanned products
	    	$product_box = [];
	    	foreach ($products as $produce) {
	    		# code...
	    		$total_worth = $produce->product_price * $produce->product_size_no;
	    		$data = [
	    			'id'        => $produce->id,
	    			'name'      => $produce->product_name,
					'size_type' => $produce->product_size_type,
					'size_no'   => $produce->product_size_no,
					'price'     => number_format($produce->product_price, 2),
					'total'     => number_format($total_worth, 2),
					'location'  => $produce->product_location,
					'state'     => $produce->product_state,
					'image'     => $produce->product_image,
					'note'      => $produce->product_note,
					'date'      => $produce->created_at->diffForHumans()
	    		];

	    		array_push($product_box, $data);
	    	}
    	}else{
    		$product_box = [];
    	}

    	// return
    	return $product_box;
    }

    /*
    |-----------------------------------------
    | LOAD USER PRODUCES
    |-----------------------------------------
    */
    public function loadProductFromApi($email){
    	// body
    	$user 	= User::where("email", $email)->first();
    	$id 	= $user->id;
    	
    	// load all products 
    	$products = Product::where('user_id', $id)->get();
    	if(count($products) > 0){
    		// scanned products
	    	$product_box = [];
	    	foreach ($products as $produce) {
	    		# code...
	    		$total_worth = $produce->product_price * $produce->product_size_no;
	    		$data = [
	    			'id'        => $produce->id,
	    			'name'      => $produce->product_name,
					'size_type' => $produce->product_size_type,
					'size_no'   => $produce->product_size_no,
					'price'     => number_format($produce->product_price, 2),
					'total'     => number_format($total_worth, 2),
					'location'  => $produce->product_location,
					'state'     => $produce->product_state,
					'image'     => $produce->product_image,
					'note'      => $produce->product_note,
					'date'      => $produce->created_at->diffForHumans()
	    		];

	    		array_push($product_box, $data);
	    	}
    	}else{
    		$product_box = [];
    	}

    	// return
    	return $product_box;
    }

    /*
    |-----------------------------------------
    | ADD DEFAULT PRODUCES
    |-----------------------------------------
    */
    public function addDefaultProduces(){
    	// body
    	
    }

    /*
    |-----------------------------------------
    | LIST ALL DEFAULT PRODUCES
    |-----------------------------------------
    */
    public function listAllDefaultProduce(){
    	// body
    	return Asset::orderBy("asset", "ASC")->get();
    }

    /*
    |-----------------------------------------
    | DELETE DEFAULT PRODUCE
    |-----------------------------------------
    */
    public function deleteDefaultProduce($payload){
    	$produce = Asset::find($payload->produce_id);
        if($produce->delete()){
        	$data = [
				'status' 	=> 'success',
				'message' 	=> 'deleted!'
			];
        }else{
        	$data = [
				'status' 	=> 'error',
				'message' 	=> 'delete failed!'
			];
        }

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | UPDATE TICKER PRICES
    |-----------------------------------------
    */
    public function updateDefaultPrice($payload){
    	// body
    	$open_price 	= $payload->open_price;
		$close_price 	= $payload->close_price;
		$produce_id 	= $payload->produce_id;

		$produce = Asset::find($payload->produce_id);
		$produce->open 		= $open_price;
		$produce->close 	= $close_price;
		$produce->gap 		= $open_price - $close_price;
		$produce->previous 	= $produce->open;
        if($produce->update()){
        	$data = [
				'status' 	=> 'success',
				'message' 	=> 'updated!'
			];
        }else{
        	$data = [
				'status' 	=> 'error',
				'message' 	=> 'update failed!'
			];
        }

        // return 
        return $data;
    }	

    /*
    |-----------------------------------------
    | ADD NEW PRODUCE
    |-----------------------------------------
    */
    public function addNewProduce($payload){
    	// body
		$item 		= $payload->produce;

		$already_added = Asset::where("asset", $item)->first();
		if($already_added === null){
			$produce 			= new Asset();
			$produce->asset 	= $item;
			$produce->open 		= 0;
			$produce->close 	= 0;
			$produce->gap 		= 0;
			$produce->previous 	= 0;
			$produce->status    = "open";
	        if($produce->save()){
	        	$data = [
					'status' 	=> 'success',
					'message' 	=> $item.' added!'
				];
	        }else{
	        	$data = [
					'status' 	=> 'error',
					'message' 	=> 'failed to add new produce!'
				];
	        }
		}else{
			$data = [
				'status' 	=> 'error',
				'message' 	=> $item.' already added!'
			];
		}

        // return 
        return $data;
    }
}
