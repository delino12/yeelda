<?php

namespace Yeelda;

// use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Geocoder;

class GoogleMapSearch
{
    /*
    |--------------------------------
    | Handle Places search
    |--------------------------------
    |
    */
    public function javisLookForAddress($address){
    	// init guzzle search clients
    	$geo_result = Geocoder::getCoordinatesForAddress($address);
    	// return response 
    	return $geo_result;
    }

    /*
    |--------------------------------
    | Handle Places search
    |--------------------------------
    |
    */
    public function javisLookForCordinate($lat, $lng){
        // init guzzle search clients
        $geo_result = Geocoder::getAddressForCoordinates($lat, $lng);

        // return response 
        return $geo_result;
    }
}
