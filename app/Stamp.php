<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Stamp extends Model
{
    /*
    |-----------------------------------------
    | GET STAMP MESSAGES
    |-----------------------------------------
    */
    public function getStampMessages(){
    	$email = Auth::user()->email;
    	// body
        $all_stamp_messages = Stamp::where('user_email', $email)->get();
        if(count($all_stamp_messages) > 0){
            // stamp box
            $stamp_box = [];
            foreach ($all_stamp_messages as $stamp) {
                # code...
                $data = array(
                    'id'        => $stamp->id,
                    'body'      => $stamp->body,
                    'status'    => $stamp->status,
                    'date'      => $stamp->created_at->diffForHumans()
                );

                array_push($stamp_box, $data);
            }
        }else{
            $stamp_box = [];
        }

        // return 
        return $stamp_box;
    }
}
