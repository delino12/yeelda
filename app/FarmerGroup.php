<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\User;
use Yeelda\FarmerBasic;
use DB;

class FarmerGroup extends Model
{
    /*
    |-----------------------------------------
    | ADD GROUP
    |-----------------------------------------
    */
    public function addGroup($payload){
    	// body
    	// check already exist
    	$already_exist = FarmerGroup::where("name", $payload->name)->first();
    	if($already_exist == null){
    		
    		$this->name 		= $payload->name;
    		$this->is_deleted 	= false;
    		if($this->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> $payload->name.' created!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error creating group, Try again!'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $payload->name.' already exist'
    		];
    	}

    	// return
    	return $data;
    }


    /*
    |-----------------------------------------
    | GET ALL GROUPS
    |-----------------------------------------
    */
    public function allGroups($payload){
    	// body
    	$all_groups = FarmerGroup::orderBy("id", "ASC")->get();
    	$group_box = [];
    	foreach ($all_groups as $group) {

    		// get total group
    		$group->total = count($this->allGroupsCapturedUsers($group->id));

    		$data = [
    			'id'			=> $group->id,
    			'name' 			=> ucfirst($group->name),
    			'total' 		=> number_format($group->total),
    			'created_at' 	=> $group->created_at->toDateTimeString()
    		];

    		array_push($group_box, $data);
    	}

    	// return
    	return $group_box;
    }


    /*
    |-----------------------------------------
    | GET ONE GROUP
    |-----------------------------------------
    */
    public function oneGroup($payload){
    	// body
    	$one_group = FarmerGroup::find($payload->group_id);
    	if ($one_group !== null) {
    		// get total group
    		$one_group->total = User::where("group_id", $one_group->id)->count();
    		$data = [
    			'id'			=> $one_group->id,
    			'name' 			=> ucfirst($one_group->name),
    			'total' 		=> number_format($one_group->total),
    			'created_at' 	=> $one_group->created_at->toDateTimeString()
    		];
    	}else{
    		$data = [];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | EDIT GROUP
    |-----------------------------------------
    */
    public function editGroup($payload){
    	// body
    	// check already exist
    	$check_group = FarmerGroup::find($payload->group_id);
    	if($check_group !== null){
    		$check_group->name 			= $payload->group_name;
    		$check_group->is_deleted 	= false;
    		if($check_group->update()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> $payload->name.' updated!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error updating group, Try again!'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Could not find group info, Try again!'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | DELETE GROUP
    |-----------------------------------------
    */
    public function delGroup($payload){
    	// body
    	// check already exist
    	$check_group = FarmerGroup::find($payload->group_id);
    	if($check_group !== null){
    		if($check_group->delete()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Group deleted!'
    			];

    			// delete all associate
    			$this->delAssociate($payload->group_id);
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error deleting group, Try again!'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Could not find group info, Try again!'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | DELETE GROUP RELATIONSHIP
    |-----------------------------------------
    */
    public function delAssociate($group_id){
    	// body
    	$all_users = User::where("group_id", $group_id)->get();
    	if(count($all_users) > 0){
    		foreach ($all_users as $user) {
	    		$update 			= User::find($user->id);
	    		$update->group_id 	= null;
	    		$update->update();
	    	}
    	}

    	// return 
    	return true;
    }

    /*
    |-----------------------------------------
    | LOAD ALL
    |-----------------------------------------
    */
    public function allGroupsCapturedUsers($group_id){
        // body
        $user_data = collect(
            DB::select("SELECT 
            farmer_basics.id, 
            farmer_basics.user_id, 
            farmer_basics.name, 
            farmer_basics.gender, 
            farmer_basics.address, 
            farmer_basics.state, 
            farmer_basics.lga, 
            farmer_basics.cluster,
            farmer_basics.language,
            farmer_basics.prevCropHarvested,
            farmer_basics.prevYieldHarvested,
            farmer_basics.prevCropPlanted,
            farmer_basics.farm_crop,
            farmer_basics.farm_size,
            farmer_basics.farm_type,
            farmer_basics.date_captured, 
            users.id AS Expr1, 
            users.account_type, 
            users.group_id, 
            users.agent_id,
            users.phone
            FROM farmer_basics INNER JOIN users ON 
            farmer_basics.user_id = users.id 
            WHERE (users.group_id = $group_id)")
        );
        
        return $user_data;
    }
}
