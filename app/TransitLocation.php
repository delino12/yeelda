<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\Mail\NotifyTrackingMail;
use Yeelda\TrackProduct;
use Yeelda\Product;
use Yeelda\User;

class TransitLocation extends Model
{
    /*
    |-----------------------------------------
    | UPDATE TRANSIT
    |-----------------------------------------
    */
    public function addNewUpdate($payload){
    	// body
    	$track_info 	= TrackProduct::where("tracking_ref", $payload->bwl_ref)->first();
        $product_info   = Product::where("id", $track_info->product_id)->first();
        $buyer_info     = User::where("email", $track_info->buyer)->first();
        $seller_info    = User::where("email", $track_info->seller)->first();
    	$tracking_id 	= $track_info->id;

    	$new_transit 					= new TransitLocation();
    	$new_transit->tracking_id 		= $tracking_id;
    	$new_transit->assignee 			= $payload->bwl_assignee;
    	$new_transit->location 			= $payload->bwl_location_address;
    	$new_transit->reason 			= $payload->bwl_reasons;
    	$new_transit->transport_type 	= $payload->bwl_transport_type;
    	$new_transit->status 			= $payload->bwl_status;
    	if($new_transit->save()){
    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> $payload->bwl_ref.' update successful!'
    		];

            // Mail Data
            $mail_data = [
                'buyer'         => $buyer_info->name,
                'courier'       => $track_info->carrier,
                'bwl_no'        => $track_info->tracking_ref,
                'location'      => $payload->bwl_location_address,
                'product_name'  => $product_info->product_name,
                'status'        => $payload->bwl_status
            ];

            // Send Mail
            \Mail::to($track_info->buyer)->send(new NotifyTrackingMail($mail_data));

    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Failed to update '.$payload->bwl_ref
    		];
    	}

    	// return
    	return $data;
    }
}
