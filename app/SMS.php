<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class SMS
{
    protected $user;
    protected $pass;
    protected $sms_type;
    protected $url;
    protected $params;
    protected $time;

    // init SMS Factory
    public function __construct()
    {
        # code...
        $this->user     = env("SMS_API_USER");
        $this->pass     = env("SMS_API_PASS");
        $this->sms_type = 'shortSMS';
        $this->time     = time();
    }

    // send message 
    public function sendMessages($receivers, $message, $sender)
    {
        # send message request
        // query string
        // $this->params = '?user='.$this->user.'&pass='.$this->pass.'&receiver='.$receivers.'
        // &sender='.$sender.'&message='.$message.'&type='.$this->sms_type.'&schedule='.$this->time;

        $this->params = '?user='.$this->user.'&pass='.$this->pass.'&receiver='.$receivers.'&sender='.$sender.'&message='.$message;

        // base url
        $this->url = 'http://www.mobilesmsng.com/api/resellers/addons/sendsms2.php'.$this->params;

        // send message using SMS Api
        $client       = new Client();
        $sms_response = $client->request('GET', $this->url);

        // filter response
        $res_code = $sms_response->getStatusCode();
        $res_body = $sms_response->getBody()->getContents();

        return $this->readErrorCode($res_body);
    }

    // send queue message
    public function sendQueueMessages($receivers, $message, $sender)
    {

        $this->params = '?user='.$this->user.'&pass='.$this->pass.'&receiver='.$receivers.'&sender='.$sender.'&message='.$message;

        // base url
        $this->url = 'http://www.mobilesmsng.com/api/resellers/addons/sendsms2.php'.$this->params;

        // send message using SMS Api
        $client       = new Client();
        $sms_response = $client->request('GET', $this->url);

        // filter response
        $res_code = $sms_response->getStatusCode();
        $res_body = $sms_response->getBody()->getContents();

        if($res_body == '-100'){
            return true;
        }else{
            return false;
        }
    }

    // send to all users
    public function readErrorCode($sms_response)
    {
    	if($sms_response == '-100'){
            // empty username
            $msg = " <span class='text-danger'>empty username... </span>";

        }elseif($sms_response == '-200'){
            // empty password
            $msg = " <span class='text-danger'>empty username... </span>";
        }
        elseif($sms_response == '-300'){
            // empty sender id
            $msg = " <span class='text-danger'>empty sender ID </span>";

        }elseif($sms_response == '-400'){
            // sender id greater than 14 Characters
            $msg = " <span class='text-danger'>sender id is too Long... </span>";

        }elseif($sms_response == '-500'){
            // Invalid receivers phone number
            $msg = " <span class='text-danger'>Invalid receivers phone no... </span>";

        }elseif($sms_response == '-600'){
            // invalid username/password
            $msg = " <span class='text-danger'>invalid username and password... </span>";

        }elseif($sms_response == '-700'){
            // empty message
            $msg = " <span class='text-danger'>empty message </span>";

        }elseif($sms_response == '-800'){
            // insufficient credit
            $msg = " <span class='text-danger'>Insufficient credit.. </span>";

        }elseif($sms_response == '-900'){
            // Account deactivated
            $msg = " <span class='text-danger'>Account deactivated... </span>";
        }else{
            // message has been sent !
            $msg = " <span class='text-success'>Message sent successfully ! </span>";
        }

        // return response
        return $msg;
    }

    // log sms process
    public function logSMS($message, $contacts, $type)
    {
        # code...
        $this->phone    = $contacts;
        $this->body     = $message;
        $this->status   = 'sent';
        $this->type     = $type;
        $this->save();
    }
}
