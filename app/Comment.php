<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /*
    |-----------------------------------------
    | GET ALL BLOG POST COMMENTS
    |-----------------------------------------
    */
    public function getAllComments($blog_id){
    	// body
    	$comment = Comment::where('blog_id', $blog_id)->get();

    	// return
    	return $comment;
    }

    /*
    |-----------------------------------------
    | ADD NEW COMMENT
    |-----------------------------------------
    */
    public function addComment($request){
    	// body
        $comments             = new Comment();
        $comments->blog_id    = $request->blog_id;
        $comments->user_name  = $request->name;
        $comments->user_email = $request->email;
        $comments->body       = $request->body;
        $comments->like       = 0;
        $comments->unlike     = 0;
        if($comments->save()){
        	$data = [
        		'status' 	=> 'success',
        		'message' 	=> 'Comment posted!'
        	];
        }else{
        	$data = [
        		'status' 	=> 'error',
        		'message' 	=> 'Failed to post comment!'
        	];
        }

        // return
        return $data;
    }
}
