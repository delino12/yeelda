<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\Product;
use Yeelda\Farmer;
use Yeelda\Service;
use Yeelda\Investor;
use Carbon\Carbon;

class Transaction extends Model
{
    /*
    |-----------------------------------------
    | GET TOTAL TRANSACTIONS
    |-----------------------------------------
    */
    public function getTransStatistic(){
    	// body
    	$total_sum 		= Transaction::sum('amount');
    	$total_pending 	= Transaction::where('status', 'pending')->sum('amount');
    	$total_success  = Transaction::where('status', 'settle')->sum('amount');
    	$total_failed   = Transaction::where('status', 'failed')->sum('amount');
    	$total_approved = Transaction::where('status', 'approved')->sum('amount');

    	$data = [
    		'total_transaction' 	=> number_format($total_sum, 2),
    		'pending_transaction' 	=> number_format($total_pending, 2),
    		'success_transaction' 	=> number_format($total_success, 2),
    		'failed_transaction' 	=> number_format($total_failed, 2),
    		'approved_transaction' 	=> number_format($total_approved, 2)
    	];

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | GET TOTAL SUM TOTAL
    |-----------------------------------------
    */
    public function getTransStatisticByDate($interval){
        if($interval == 0){
            $days = 0;
            $date = Carbon::now();
        }elseif($interval == 1){
            $days = 7; // one week
            $date = Carbon::now()->subDays($days);
        }elseif($interval == 2){
            $days = 30; // one week
            $date = Carbon::now()->subDays($days);
        }elseif($interval == 3){
            $days = 365; // one week
            $date = Carbon::now()->subDays($days);
        }

        $end   = Carbon::now();
        $start = $date;

        // body
        $total_sum  = Transaction::whereBetween("created_at", [$start, $end])->sum('amount');

        // $total_sum  = Transaction::where("created_at", [$start, $end])->sum('amount');
        $data = [
            'total_transaction' => number_format($total_sum, 2),
            'total_fert'        => number_format(0.00, 2),
            'total_seeds'       => number_format(0.00, 2),
            'total_equip'       => number_format(0.00, 2)
        ];

        // return
        return $data;
    }


    /*
    |-----------------------------------------
    | GET NUMBERS OF SALES
    |-----------------------------------------
    */
    public function getTotalNumbers(){
        // body
        $all_transactions = Transaction::all();
        if(count($all_transactions) > 0){
            $trans_box = [];
            foreach ($all_transactions as $el) {
                // filtered for count
                $scanned = Transaction::where("item_id", $el->item_id)->count();
                $product = Product::where("id", $el->item_id)->first();
                if($product !== null){
                    
                    $user_info = $this->getUserByEmail($el->seller_email);
                    $data = [
                        "item_id"   => $el->item_id,
                        "owner"     => $user_info['name'],
                        "qty"       => $el->qty,
                        "amount"    => number_format($el->amount, 2),
                        "result"    => $scanned,
                        "produce"   => $product->product_name
                    ];

                    // check if object already pushed
                    if(in_array($data, $trans_box)){
                        // let skip this..
                    }else{
                        array_push($trans_box, $data);
                    }
                }
            }
        }else{
           $trans_box = []; 
        }

        // return transaction box
        return $trans_box;
    }


    /*
    |-----------------------------------------
    | Get user infomation by email
    |-----------------------------------------
    */
    public function getUserByEmail($email){
        // body
        $check_farmers  = User::where([['email', $email], ['account_type', 'farmer']])->first();
        $check_service  = User::where([['email', $email], ['account_type', 'service']])->first();
        $check_investor = User::where([['email', $email], ['account_type', 'buyer']])->first();

        if($check_farmers !== null){
            
            $name = $check_farmers->name;
            $email = $check_farmers->email;
            $phone = $check_farmers->phone;

        }elseif($check_service !== null){
            
            $name = $check_service->name;
            $email = $check_service->email;
            $phone = $check_service->phone;
        
        }elseif($check_investor !== null){
            
            $name = $check_investor->name;
            $email = $check_investor->email;
            $phone = $check_investor->phone;
        
        }else{
            // when user doesn't exits
            $email  = "";
            $name   = "";
            $phone  = "";
        }

        // return information
        $data = [
            'name'  => $name,
            'email' => $email,
            'phone' => $phone
        ];
        // return
        return $data;
    }


    /*
    |-----------------------------------------
    | GET ALL SETTLED REFERENCE
    |-----------------------------------------
    */
    public function getTransactionRef($reference){
        // body
        $trans_info = Transaction::where("trans_id", $reference)->first();
        if($trans_info == null){
            return $data = [
                'status'    => 'error',
                'message'   => $reference.' not found, check reference and try again!'
            ];

        }else{

            $seller_info = $this->getUserByEmail($trans_info->seller_email);
            $buyer_info  = $this->getUserByEmail($trans_info->buyer_email);

            $trans_details = [
                'seller_email'  => $trans_info->seller_email,
                'buyer_email'   => $trans_info->buyer_email,
                'seller_name'   => $seller_info["name"],
                'buyer_name'    => $buyer_info["name"],
                'status'        => $trans_info->status
            ];

            if($trans_info == "pending"){
                return $data = [
                    'status'    => 'error',
                    'message'   => 'Transaction status is pending...'
                ];
            }else{
                return $data = [
                    'status'    => 'success',
                    'data'      => $trans_details
                ];
            }
        }
    }

}
