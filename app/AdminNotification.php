<?php

namespace Yeelda;

use Illuminate\Database\Eloquent\Model;
use Yeelda\Mail\AdminNotificationMail;

class AdminNotification extends Model
{
    /*
    |-----------------------------------------
    | ADD NEW NOTIFICATIONS
    |-----------------------------------------
    */
    public function addNewEvent($payload){
    	switch ($payload["category"]) {
    		case 'signup':
    			# code...
    			$payload["category"] = 1;
    			break;
    		case 'subscription':
    			# code...
    			$payload["category"] = 2;
    			break;
    		case 'order':
    			# code...
    			$payload["category"] = 3;
    			break;
    		case 'payment':
    			# code...
    			$payload["category"] = 4;
    			break;
    		case 'chat':
    			# code...
    			$payload["category"] = 5;
    			break;
    		case 'update':
    			# code...
    			$payload["category"] = 6;
    			break;
    		default:
    			# code...
    			$payload["category"] = 0;
    			break;
    	}

    	// body
    	$notifications 				= new AdminNotification();
    	$notifications->category 	= $payload["category"];
    	$notifications->contents 	= json_encode($payload["contents"]);
    	$notifications->status      = false;
    	$notifications->save();
    }

    /*
    |-----------------------------------------
    | GET ALL NOTIFICATIONS
    |-----------------------------------------
    */
    public function getAllNotifications(){
    	// body
    	$all_notifications = AdminNotification::orderBy("created_at", "DESC")->get();
    	$notifications_box = [];
    	foreach ($all_notifications as $key => $value) {
    		$value->contents = collect(json_decode($value->contents, true));

    		if($value->category === 1){
    			// new signup
    			$value->description = $value->contents->names." has join YEELDA!";
    		}elseif ($value->category === 2) {
    			# code...
    			$value->description = $value->contents->names." has upgraded to premium subscriber!";
    		}

    		$data = [
    			'id' 			=> $value->id,
    			'category'      => $value->category,
    			'contents' 		=> $value->contents,
    			'description'   => $value->description,
    			'created_at' 	=> $value->created_at,
    			'updated_at' 	=> $value->updated_at,
    			'date' 			=> $value->created_at->diffForHumans()
    		];

    		// remove protected
    		unset($data['contents']['password']);
    		unset($data['contents']['_token']);

    		array_push($notifications_box, $data);
    	}

    	// return 
    	return $notifications_box;
    }

    /*
    |-----------------------------------------
    | GET SINGLE NOTIFICATION
    |-----------------------------------------
    */
    public function getSingleNotification($payload){
    	// body
    	return AdminNotification::where("id", $payload->id)->first();
    }

    /*
    |-----------------------------------------
    | UPDATE NOTIFICATIONS
    |-----------------------------------------
    */
    public function updateReadNotifications($payload){
    	// body
    	$update_notification 			= AdminNotification::find($payload->notification_id);
    	$update_notification->status 	= true;
    	$update_notification->update();
    }

    /*
    |-----------------------------------------
    | DELETE NOTIFICATIONS
    |-----------------------------------------
    */
    public function deleteNotification($payload){
    	// body
    	$update_notification 			= AdminNotification::find($payload->notification_id);
    	$update_notification->delete();
    }

    /*
    |-----------------------------------------
    | SEND ADMIN NOTIFICATION UDPATE MAIL
    |-----------------------------------------
    */
    public function sendAdminNotification($payload){
    	// body
    	$admin_mail = "info@yeelda.com";
    	\Mail::to($admin_mail)->send(new AdminNotificationMail($payload));

    	// return
    	return true;
    }

    /*
    |-----------------------------------------
    | COUNT UNREAD NOTIFICATIONS
    |-----------------------------------------
    */
    public function countUnread(){
    	// body
    	return AdminNotification::where("status", false)->count();
    }

    /*
    |-----------------------------------------
    | COUNT UNREAD NOTIFICATIONS
    |-----------------------------------------
    */
    public function countRead(){
    	// body
    	return AdminNotification::where("status", true)->count();
    }
}
