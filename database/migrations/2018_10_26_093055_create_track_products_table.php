<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_ref');
            $table->string('product_id');
            $table->string('seller');
            $table->string('buyer');
            $table->string('location');
            $table->string('destination');
            $table->string('carrier');
            $table->string('assignee');
            $table->string('status');
            $table->date('pick_date');
            $table->date('drop_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_products');
    }
}
