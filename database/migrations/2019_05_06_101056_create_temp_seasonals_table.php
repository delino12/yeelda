<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempSeasonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_seasonals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('product_name');
            $table->string('product_size_type');
            $table->string('product_size_no')->nullable();
            $table->string('product_price')->nullable();
            $table->string('product_location')->nullable();
            $table->string('product_state')->nullable();
            $table->text('product_image')->nullable();
            $table->string('product_note')->nullable();
            $table->string('product_planning_date')->nullable();
            $table->string('product_delivery_date')->nullable();
            $table->string('product_delivery_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_seasonals');
    }
}
