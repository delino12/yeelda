<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('product_status');
			$table->string('product_name');
			$table->string('product_size_type');
			$table->string('product_size_no');
			$table->string('product_price');
			$table->string('product_location');
			$table->string('product_state');
			$table->string('product_image');
			$table->string('product_note')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('products');
	}
}
