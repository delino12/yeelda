<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('equipments', function (Blueprint $table) {
			$table->increments('id');
			$table->string('user_id');
			$table->string('equipment_name');
			$table->string('equipment_descriptions');
			$table->string('equipment_no');
			$table->string('equipment_status');
			$table->string('equipment_price');
			$table->string('equipment_address');
			$table->string('equipment_state');
			$table->string('equipment_image');
			$table->string('equipment_delivery_type');
			$table->string('equipment_note')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('equipments');
	}
}
