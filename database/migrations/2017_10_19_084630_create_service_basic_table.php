<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceBasicTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('service_basics', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name')->nullable();
			$table->string('gender')->nullable();
			$table->string('address')->nullable();
			$table->string('state')->nullable();
			$table->integer('zipcode')->nullable();
			$table->string('avatar')->nullable();
			$table->string('office')->nullable();
			$table->string('mobile')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('service_basics');
	}
}
