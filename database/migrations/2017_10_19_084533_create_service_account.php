<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceAccount extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('service_accounts', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('bank_name')->nullable();
			$table->string('account_name')->nullable();
			$table->string('account_no')->nullable();
			$table->string('account_type')->nullable();
			$table->string('card_type')->nullable();
			$table->string('card_holder')->nullable();
			$table->string('card_no')->nullable();
			$table->string('exp')->nullable();
			$table->integer('cvv')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('service_accounts');
	}
}
