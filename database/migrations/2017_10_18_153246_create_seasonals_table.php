<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonalsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('seasonals', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('product_name');
			$table->string('product_size_type');
			$table->string('product_size_no');
			$table->string('product_price');
			$table->string('product_location');
			$table->string('product_state');
			$table->string('product_image');
			$table->string('product_note')->nullable();
			$table->string('product_planning_date');
			$table->string('product_delivery_date');
			$table->string('product_delivery_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('seasonals');
	}
}
