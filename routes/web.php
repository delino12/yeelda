<?php
/*
|--------------------------------------------------------------------------
| External Pages Informations
|--------------------------------------------------------------------------
|
| This route handle all pages related to service render
| Guest users can access the route.
|
*/

// all testing route
Route::get('test-landing', 	   			'ExternalPagesController@testingIndex');
Route::get('test', 						'ExternalPagesController@testPage')->name('test_page');
Route::get('/search-farmers',           'ExternalPagesController@findFarmer');
Route::get('/search-services',          'ExternalPagesController@findServices');
Route::get('/select-services',          'ExternalPagesController@selectService');
Route::get('/load/search/results',      'ExternalPagesController@searchProduce');
Route::get('/dev-logs', 				'\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::get('/signin',					'LoginClientsController@showFormSignin')->name('login');
Route::get('/login',					'LoginClientsController@showFormSignin');
Route::get('/signup',					'LoginClientsController@showFormSignup');
Route::get('/reset-password',			'LoginClientsController@showResetPassword');
Route::get('/account/reset',   			'ActivationController@resetAccount');


/*
|--------------------------------------------------------------------------
| Entry point
|--------------------------------------------------------------------------
|
*/
// Route::get('/',                			'ExternalPagesController@index');
Route::get('/',                			'ExternalPagesController@indexNew');
Route::get('/farmer',					'ExternalPagesController@farmer');
Route::get('/services',					'ExternalPagesController@services');
Route::get('/buyers',					'ExternalPagesController@buyers');
Route::get('/search-farmer',			'ExternalPagesController@searchFarmer');
Route::get('/search-farmer/premium',	'ExternalPagesController@searchFarmerByViewAccess');
Route::get('/search-farmer/reports',	'ExternalPagesController@farmerReportByViewAccess');
Route::get('/search-service',			'ExternalPagesController@searchService');
Route::get('/produce-marketplace',		'ExternalPagesController@allProduces');

Route::get('/farmers-marketplace/{user_name}/{user_id}', 	'ExternalPagesController@farmersMarketPlace');
Route::post('/farmers-marketplace/setting', 				'ExternalPagesController@updatePageSettings');
Route::get('/settings/marketplace/{user_id}',				'ExternalPagesController@showPageSetting');

Route::get('/future-produce-marketplace','ExternalPagesController@allFutureProduces');
Route::get('/equipment-marketplace',	'ExternalPagesController@selectServices');
Route::get('/contact',         			'ExternalPagesController@contact');
Route::get('/about',           			'ExternalPagesController@about');
Route::get('/blog',      				'ExternalPagesController@blog');
Route::get('/faq',             			'ExternalPagesController@faq');
Route::get('/terms-and-conditions',     'ExternalPagesController@terms');
Route::get('/privacy',           		'ExternalPagesController@privacy');

Route::get('/load/farmers/products',    'ExternalPagesController@shopProducts');
Route::get('/load/farmers/future/products','ExternalPagesController@futureShopProducts');
Route::get('/load/recent/products',     'ExternalPagesController@loadRecentProduce');
Route::get('/load/farmers/equipments',  'ExternalPagesController@loadEquipments');
Route::get('/load/farmers/seeds', 		'ExternalPagesController@loadSeeds');
Route::get('/load/farmers/fertilizers', 'ExternalPagesController@loadFert'); // fertilizers
Route::get('/carts',                    'ExternalPagesController@loadCarts');
Route::get('/seasons/harvest/info/{id}','ExternalPagesController@futureHarvestInfo');
Route::get('/load/future-harvest/{id}', 'ExternalPagesController@loadFutureInfo');

// send message
Route::post('/send/contact/message',    'ExternalPagesController@sendContactMsg');

// referals section
Route::get('/load/referrals/{email}', 	'ReferralController@loadReferal');

// market price discovery
Route::get('/commodities-price/discovery',  'PriceDiscoveryController@updatePriceIndex');
Route::get('/commodities-price/load-index', 'PriceDiscoveryController@loadPriceIndex');

// load blogs post
Route::get('/load/blog/posts',              'BlogPostController@loadBlogPosts');
Route::get('/load/blog/posts/recents',      'BlogPostController@loadBlogPostsRecent');
Route::post('/post/blog/comment',           'BlogPostController@postComment');
// load comments 
Route::get('/load/blog/comments/{blog_id}', 'BlogPostController@loadComment');

// Activation all users account
Route::get('/account/activations', 'UsersVerificationController@activate');


/*
|--------------------------------------------------------------------------
| Registration and Account Login Pages
|--------------------------------------------------------------------------
|
| Here is where you can register users account and also manage how the
| Users Login in.
|
 */ 
Route::get('/account/register', 			'RegisterClientsController@showSignupForm');
Route::get('/account/deactivated', 			'LoginClientsController@deactivated');
Route::get('/account/login', 				'LoginClientsController@showLoginForm');
Route::get('/account/logout', 				'LoginClientsController@logoutUsers');
Route::get('/account/logout/sp', 			'LoginClientsController@logoutServiceProvider');
Route::get('/account/logout/investor', 		'LoginClientsController@logoutInvestor');
// Route::get('/login', 						'LoginClientsController@showLoginForm');
Route::get('/account/forget', 				'LoginClientsController@showFindAccountForm');
Route::get('/resend/activation/link', 		'ActivationController@resendLink');

Route::post('/account/register', 			'RegisterClientsController@doRegistration');
Route::post('/account/login', 				'LoginClientsController@doLogin')->middleware('account-status');
Route::post('/find/user/account', 			'LoginClientsController@findAccount');

// social media login
Route::post('/login/via/email', 			'LoginClientsController@loginViaEmail');

Auth::routes();

// add other services controller
Route::post('/add/seeds/{email}', 			'FarmServicesController@addSeed');
Route::post('/add/fertilizers/{email}', 	'FarmServicesController@addFertilizer');
Route::get('/load/seeds/{email}', 			'FarmServicesController@loadSeeds');
Route::get('/load/fertilizers/{email}', 	'FarmServicesController@loadFertilizer');


// all investor json response controller 
Route::get('/investor/basic/profile/{id}',  'InvestorResponseController@basicInfo');
Route::get('/investor/load-payment',        'InvestorResponseController@paymentInfo');
Route::get('/investors/dashboard', 			'InvestorsController@dashboard');
Route::get('/investors/setting', 			'InvestorsController@setting');
Route::get('/investors/profile', 			'InvestorsController@profile');
Route::get('/investors/account', 			'InvestorsController@account');
Route::get('/investors/cart', 				'InvestorsController@cart');
Route::get('/investors/transactions', 		'InvestorsController@transaction');
Route::get('/investors/message', 			'InvestorsController@message');
Route::get('/investors/equipments', 		'InvestorsController@equipment');
Route::get('/investors/promotions', 		'InvestorsController@promotion');

Route::post('/investor/update-payment',     'InvestorResponseController@updatePayment');
Route::post('/investor/update-card', 	    'InvestorResponseController@updateCard');
Route::post('/investor/change-password',    'InvestorResponseController@changePassword');
Route::post('/investor/update/profile',     'InvestorResponseController@updateProfile');
Route::get('/investor/load/profile',        'InvestorResponseController@loadProfile');
Route::post('/buyers/upload/images', 	    'InvestorResponseController@updateAvatar');
Route::post('/deactivate/account/buyer', 	'InvestorResponseController@deactivateAccount');

// reset users account
Route::post('/reset/account/password', 'ActivationController@resetPassword');

/*
|--------------------------------------------------------------------------
| Farmers Accessable Pages Informations
|--------------------------------------------------------------------------
|
| This route handle all pages related to farmers users render
| Guest users can't access the route.
|
 */
Route::get('/farmer/dashboard',           	'FarmersHomeController@dashboard');
Route::get('/farmer/setting',             	'FarmersHomeController@setting');
Route::get('/farmer/profile',             	'FarmersHomeController@profile');
Route::get('/farmer/account',             	'FarmersHomeController@account');
Route::get('/farmer/cart',                	'FarmersHomeController@cart');
Route::get('/farmer/message',             	'FarmersHomeController@message');
Route::get('/farmer/products',            	'FarmersHomeController@products');
Route::get('/farmer/promotions',          	'FarmersHomeController@promotions');
Route::get('/farmers/products/{id}',      	'FarmersHomeController@viewProducts');
Route::get('/farmer/transactions',        	'FarmersHomeController@transactions');

// all post route
Route::post('/farmer/account/activation', 	'FarmersHomeController@activateAcount');
Route::post('/update/profile',            	'FarmersHomeController@updateProfile');
Route::post('/update/avatar',             	'FarmersHomeController@updateAvatar');
Route::post('/update/account-details',    	'FarmersHomeController@updateAccount');
Route::post('/upload/farmer/products',    	'FarmersHomeController@uploadProduct');
Route::post('/farmer/update/password',    	'FarmerJsonController@updatePassword');

// All farmer json routes
Route::get('/farmer/load/products',       	'FarmerJsonController@loadProducts');
Route::get('/farmer/load/message',        	'FarmerJsonController@loadMsg');
Route::post('/farmers/update/profile',    	'FarmerJsonController@updateProfile');
Route::get('/farmers/load/profile',       	'FarmerJsonController@loadProfile');
Route::post('/upload/profile/image',      	'FarmerJsonController@uploadAvatar');
Route::get('/delete/produce/{id}',        	'FarmerJsonController@delProduce');
Route::get('/load/farmers/crops',			'FarmerJsonController@loadCrops');
Route::post('/add/crops/{email}', 	      	'FarmerJsonController@addCrops');
Route::get('/remove/crop/{id}',   			'FarmerJsonController@deleteCrop');
Route::post('/deactivate/account/farmer', 	'FarmerJsonController@deactivateAccount');

/*
|-----------------------------------------
| SETUP MARKET PAGE ROUTES
|-----------------------------------------
*/
Route::post('save/page/settings', 			'MarketPageSetupController@saveSettings');

/*
|--------------------------------------------------------------------------
| Service Provider Accessable Pages Informations
|--------------------------------------------------------------------------
|
| This route handle all pages related to service users render
| Guest users can't access the route.
|
 */
Route::get('/service-provider/dashboard',  	'ServiceProviderController@dashboard');
Route::get('/service-provider/setting',    	'ServiceProviderController@setting');
Route::get('/service-provider/profile',    	'ServiceProviderController@profile');
Route::get('/service-provider/account',    	'ServiceProviderController@account');
Route::get('/service-provider/cart',       	'ServiceProviderController@cart');
Route::get('/service-provider/message',    	'ServiceProviderController@message');
Route::get('/service-provider/equipments', 	'ServiceProviderController@equipment');
Route::get('/hiring/service',              	'ServiceProviderController@showRequest');
Route::get('/view/fertilzer/{id}',			'ServiceProviderController@showFertDetails');
Route::get('/view/seeds/{id}',				'ServiceProviderController@showSeedsDetails');

Route::post('/upload/equipment',           	'ServiceProviderController@uploadEquipment');
Route::get('/load/service/equipments',     	'ServiceProviderController@loadEquipments');
Route::get('/view/equipments',             	'ServiceProviderController@viewEquipment');
Route::get('/load/single/equipments',      	'ServiceProviderController@loadSingleEquipment');

// load Service Json
Route::get('/load/hiring-request',         	'ServiceJsonController@loadHiringRequest');
Route::get('/service/load-profile', 	   	'ServiceJsonController@loadProfile');
Route::post('/service/upload/image',       	'ServiceJsonController@updateImage');
Route::post('/service/update/profile',     	'ServiceJsonController@updateProfile');
Route::get('/service/basic/profile/{id}',  	'ServiceJsonController@loadProfile');
Route::post('/service/change-password', 	'ServiceJsonController@changePassword');
Route::post('/deactivate/account/service', 	'ServiceJsonController@deactivateAccount'); 


/*
|--------------------------------------------------------------------------
| Wallet services
|--------------------------------------------------------------------------
*/
Route::get('/get/wallet/balance',			'UserWalletController@getWalletBalance');


/*
|--------------------------------------------------------------------------
| Messages Services
|--------------------------------------------------------------------------
|
| This route handle all pages related to service users render
| Guest users can't access the route.
|
*/
Route::get('/load/company/information',				'CompanyDetailsController@load');
Route::post('/add/company/information',				'CompanyDetailsController@add');

Route::get('/edit/company/information/{id}',		'CompanyDetailsController@edit');
Route::post('/edit/company/information/{id}',		'CompanyDetailsController@update');

Route::get('/delete/company/information/{id}',		'CompanyDetailsController@delete');


/*
|--------------------------------------------------------------------------
| Messages Services
|--------------------------------------------------------------------------
|
| This route handle all pages related to service users render
| Guest users can't access the route.
|
*/
Route::get('/message/create',          	'MessagesController@create');
Route::get('/message/service', 			'MessagesController@index');
Route::get('/message/sent', 			'MessagesController@sent');
Route::get('/message/draft', 			'MessagesController@draft');
Route::get('/message/trash', 			'MessagesController@trash');
Route::get('/ask-seller/{email}', 		'MessagesController@askSeller');
Route::get('/message/service/reply/{id}/{email}', 'MessagesController@replyMsg');

// API news
Route::get('/load/api-news', 					'NewsRoomController@loadApiNews');
Route::get('/load/api-news/bloomberg', 			'NewsRoomController@loadApiNewsBloomberg');
Route::get('/load/api-news/custom', 			'NewsRoomController@loadCustomNews');

// Load Stamp message
Route::get('/load/stamp/message', 				'JsonResponseController@loadStampMessages');
Route::post('/update/stamp/message', 			'JsonResponseController@delStampMessages');



/*
|--------------------------------------------------------------------------
| Json Response Services
|--------------------------------------------------------------------------
|
| This route handle all pages related to service users render
| Guest users can't access the route.
|
*/

Route::get('/load/messages/', 		 			'JsonResponseController@loadMsg');
Route::get('/count/messages/{email}', 		 	'JsonResponseController@countMsg');
Route::get('/get/reply/{msgId}/{from}/{to}', 	'JsonResponseController@loadReplyMsg');
Route::post('/send/messages',             		'JsonResponseController@sendMessage');
Route::post('/send/reply/',               		'JsonResponseController@postReply');
Route::get('/load/notifications/{email}', 		'JsonResponseController@notifications');
Route::get('/request-hire/{id}',              	'JsonResponseController@requestHiring');
Route::get('/load/farmer/equipments/single/', 	'JsonResponseController@loadSingleHiring');
Route::get('/load/user/payments', 				'JsonResponseController@loadUserPayment');
Route::get('/load/buyer/payments', 				'JsonResponseController@loadBuyerPayment');
Route::get('/load/user/transactions', 			'JsonResponseController@loadUserTransactions');
Route::get('/callback/transaction/{trans_id}', 	'JsonResponseController@callbackTransaction');
Route::get('/confirm/payment/{trans_id}', 		'JsonResponseController@confirmPayment');
Route::get('/load/farmers/profile', 			'JsonResponseController@loadAllFarmers');
Route::get('/load/user/statistic',				'JsonResponseController@totalPlatformUsers');
Route::get('/list/all/banks',					'JsonResponseController@listAllBanks');
Route::get('/load/view/access',					'JsonResponseController@loadViewAccess');
Route::get('/fetch/all/premium/farmers',		'JsonResponseController@fetchPremiumFarmers');

// hint response controller
Route::get('/load/products/hints', 				'HintsResponseController@load');
Route::get('/load/sizes/hints', 				'HintsResponseController@loadSizes');

// load message option
Route::get('/load/user/sents/{email}', 			'MessagesOptionController@loadSent');
Route::get('/load/user/draft/{email}', 			'MessagesOptionController@loadDraft');
Route::get('/load/user/trash/{email}', 			'MessagesOptionController@loadTrash');
// save draft messages
Route::post('/save/draft', 			   			'MessagesOptionController@saveDraft');



Route::get('/continue/payment',      			'StartPaymentController@processPayment');
Route::post('/send/payment/request', 			'StartPaymentController@logPayment');
Route::post('/log/wallet/payment',				'StartPaymentController@logWalletPayment');
Route::post('/send/transaction/log', 			'StartPaymentController@logTransactions');
// Route::get('/verify/payment/{reference}', 		'StartPaymentController@verifyTransactions');


/*
|--------------------------------------------------------------------------
| Markets and Activities Pages
|--------------------------------------------------------------------------
|
| Here is where you can browse throught permitted pages
| Pages are authenticated
|
 */
Route::get('/comodity-market', 'HomeController@trade');

// Farmers products
Route::get('/listed/products',     'JsonResponseController@listedProducts');
Route::get('/load/single/produce', 'JsonResponseController@singleProduce');
Route::get('/product/details',    	'DisplayMarketController@showSingle');
Route::get('/farm-produce',        'DisplayMarketController@showProduce');

// Service provider equipment
Route::get('/farm-equipment', 		'DisplayMarketController@showEquipment');

/*
|--------------------------------------------------------------------------
| Admin and Pages inlude admin login pages Pages
|--------------------------------------------------------------------------
|
| Here is where you can browse throught permitted pages
| Pages are authenticated
|
 */
Route::get('/admin/login', 						'AdminLoginController@showLoginForm');
Route::get('/admin', 							'AdminHomeController@dashboard');
Route::get('/admin/dashboard', 					'AdminHomeController@dashboard');
Route::get('/admin/messaging/service',			'AdminHomeController@messaging');
Route::get('/admin/account', 					'AdminHomeController@account');
Route::get('/admin/notifications', 				'AdminHomeController@notification');
Route::get('/admin/blogs', 						'AdminHomeController@blog');
Route::get('/admin/documents', 					'AdminHomeController@documents');
Route::get('/admin/products',                   'AdminHomeController@allProduce');
Route::get('/admin/equipment',                  'AdminHomeController@allEquipment');
Route::get('/admin/settle/payments', 			'AdminHomeController@payments');
Route::get('/admin/all/farmers', 				'AdminHomeController@allFarmers');
Route::get('/admin/all/services-providers', 	'AdminHomeController@allServices');
Route::get('/admin/all/buyers', 				'AdminHomeController@allBuyers');
Route::get('/admin/capture/farm', 				'AdminHomeController@captureForm');
Route::get('/admin/capture/data', 				'AdminHomeController@captureFarmerData');
Route::get('/admin/view/sent/sms',				'AdminHomeController@viewSMS');
Route::get('/admin/view/sent/mail',				'AdminHomeController@viewSentMail');
Route::get('/admin/view/sent/stamp',			'AdminHomeController@viewSentStamp');
Route::get('/admin/view-products/{id}',			'AdminHomeController@viewProducts');
Route::get('/admin/view-equipment/{id}',		'AdminHomeController@viewEquipment');
Route::get('/admin/read/mail/message/{id}',		'AdminHomeController@viewOneMailMessage');

// routes updates
Route::get('/admin/financials', 				'AdminHomeController@financials');
Route::get('/admin/industrials', 				'AdminHomeController@industrials');
Route::get('/admin/managment', 					'AdminHomeController@managment');
Route::get('/admin/data-capture', 				'AdminHomeController@odca');
Route::get('/admin/products', 					'AdminHomeController@products');
Route::get('/admin/settings', 					'AdminHomeController@settings');
Route::get('/admin/statistics', 				'AdminHomeController@statistics');
Route::get('/admin/analytics', 					'AdminHomeController@analytics');
Route::get('admin/reports', 					'AdminHomeController@reports');
Route::get('/view/group-captured/users/{id}',	'AdminHomeController@viewUsersByGroup');
Route::get('/admin/view/notifications',			'AdminHomeController@viewNotifications');

// get payment request
Route::get('/load/payment/request',  			'AdminHomeController@loadPaymentRequest');
Route::get('/load/payment/paid',     			'AdminHomeController@loadPaymentCompleted');

// load last uploaded blog
Route::get('/blog/last/uploaded',    			'AdminHomeController@lastUploadBlog');
Route::post('/save/post/blogs/', 				'AdminHomeController@saveBlogPost');
Route::post('/admin/delete/blog/post',			'AdminHomeController@deleteBlogPost');

Route::post('/admin/upload/documents', 			'AdminHomeController@uploadDocs');
Route::post('/admin/upload/images', 			'AdminHomeController@uploadBlogImage');
Route::post('/send/announce/message', 			'AdminHomeController@sendMessage');
Route::post('/send/stamp/message', 				'AdminHomeController@SendStampMessage');
Route::post('/admin/upload/captured/images',	'AdminHomeController@initCaptureImage');
Route::post('/admin/update/captured/images',	'AdminHomeController@updateCaptureImage');
Route::get('/admin/view/duplicates',			'AdminHomeController@viewDuplicates');
Route::get('/admin/edit/profile/{id}',			'AdminHomeController@showEditProfile');


// SMS Section
Route::post('/send/sms/message', 				'SmsResponseController@sendSmsMessage');
Route::get('/admin/sms-contact/status', 		'SmsResponseController@smsContactStatus');

// load admin contents json
Route::get('/load/users/yeelda', 				'AdminHomeController@loadUsers');

Route::get('/admin/users/profile/{id}', 		'AdminHomeController@viewProfile');
Route::get('/load/group/association/request', 	'AdminHomeController@loadAssoc');
Route::get('/accept/group/{id}', 				'AdminHomeController@acceptRequest');
Route::get('/decline/group/{id}', 				'AdminHomeController@declineRequest');
Route::get('/admin/users/{id}/view',			'AdminHomeController@viewAdminUser');
Route::get('/admin/agent/{id}/view',			'AdminHomeController@viewAgentUser');
Route::post('/admin/create/membership',			'AdminJsonController@createMemeberShip');
Route::get('/admin/list/membership/access',		'AdminJsonController@userViewAccess');
Route::post('/admin/block/view/access',			'AdminJsonController@blockViewAccess');
Route::post('/admin/enable/view/access',		'AdminJsonController@enableViewAccess');

// view farmers information
Route::get('/view/{accountType}/{id}', 			'AdminHomeController@viewYeeldaUsers');
Route::get('/load-temp/{accountType}/{id}', 	'AdminJsonController@loadYeeldaUsers');
Route::get('/load/user/profile/{id}', 			'AdminJsonController@loadProfile');
Route::get('/admin/load-products',              'AdminJsonController@allProducts');
Route::get('/admin/delete-products/{id}',       'AdminJsonController@deleteProduct');
Route::get('/admin/load-equipment',             'AdminJsonController@loadEquipment');
Route::get('/admin/load-equipment/{id}',		'AdminJsonController@loadOneEquipment');
Route::get('/admin/load-farm-services',         'AdminJsonController@loadFarmServices');
Route::get('/admin/delete-equipment/{id}',      'AdminJsonController@deleteEquipment');
Route::get('/admin/delete-services/{id}/{type}','AdminJsonController@deleteFarmService');
Route::get('/admin/settle/payment/{id}', 		'AdminJsonController@settlePayment');
Route::get('/admin/load/admin', 				'AdminJsonController@loadAdminUsers');
Route::post('/admin/create/admin/user', 		'AdminJsonController@adminAddUsers');
Route::post('/admin/create/agent/user', 		'AdminJsonController@agentAddUsers');
Route::get('/admin/load/admin/{id}', 			'AdminJsonController@loadAdminSingleUser');
Route::get('/admin/load/agent/{id}',			'AdminJsonController@loadAgentSingleUser');
Route::post('/admin/update/admin-info', 		'AdminJsonController@updateAdminUserInfo');
Route::get('/admin/load/sent/sms',				'AdminJsonController@loadSmsLogs');
Route::get('/admin/load/sent/mail',				'AdminJsonController@loadSentMail');
Route::get('/admin/load/sent/stamp',			'AdminJsonController@loadSentStamp');
Route::get('/admin/load/sent/mail/{id}',		'AdminJsonController@loadOneSentMail');
Route::get('/admin/load/agents',				'AdminJsonController@loadAgentUsers');
Route::get('/load/charts/statistic',			'AdminJsonController@loadChartStatistic');
Route::get('/admin/load/trans-statistic',		'AdminJsonController@loadPaymentStatistic');
Route::get('/admin/load/financial-statistic',	'AdminJsonController@loadFinancialStatistic');
Route::get('/admin/load/total/soldout/{days}',	'AdminJsonController@filteredProducesSold');
Route::get('/admin/load/users-by-location', 	'AdminJsonController@loadUserByRegions');
Route::post('/admin/delete/agent/user',			'AdminJsonController@deleteAgentUser');
Route::post('/admin/delete/admin/user',			'AdminJsonController@deleteAdminUser');
Route::get('/admin/tracking/{id}',				'AdminHomeController@viewTracking');
Route::get('/admin/load/trans_ref',				'AdminJsonController@loadTransRef');
Route::post('/admin/create/tracking',			'AdminJsonController@createReceipt');
Route::get('/admin/load/tracking',				'AdminJsonController@loadTracking');
Route::get('/admin/load/tracking/{id}',			'AdminJsonController@loadOneTracking');
Route::post('/admin/update/tracking',			'AdminJsonController@updateTracking');
Route::get('/admin/fetch/overall/statistics',   'AdminJsonController@loadOverallStats');
Route::get('/admin/fetch/geo/landarea',   		'AdminJsonController@getGeoLandArea');
Route::get('/load/users/yeelda/farmers', 		'AdminJsonController@loadUsersFarmers');
Route::get('/load/users/yeelda/service', 		'AdminJsonController@loadUsersService');
Route::get('/load/users/yeelda/buyers', 		'AdminJsonController@loadUsersBuyers');
Route::post('/delete/assoc/group',				'AdminJsonController@deleteGroup');
Route::post('/delete/general/chat/messages',	'AdminJsonController@clearAllChatMessages');
Route::get('/admin/get/reports',				'AdminJsonController@fetchReportsData');
Route::get('/admin/count/users/by/states',		'AdminJsonController@countUsersByState');
Route::get('/admin/get/clusters/reports',		'AdminJsonController@getClustersReport');
Route::get('/admin/get/all/states', 			'AdminJsonController@fetchAllStates');
Route::get('/admin/get/state/lgas', 			'AdminJsonController@fetchLga');
Route::get('/admin/get/state/lgas/clusters', 	'AdminJsonController@fetchLgaClusters');
Route::get('/admin/get/reports/by/state',		'AdminJsonController@fetchReportsDataByState');
Route::post('/admin/search/replace/lgas',		'AdminJsonController@searchAndReplaceLga');
Route::get('/admin/get/clusters',				'AdminJsonController@getClusters');
Route::get('/admin/get/cluster/villages',		'AdminJsonController@getClusterVillages');
Route::post('/admin/search/replace/clusters',	'AdminJsonController@searchAndReplaceCluster');
Route::get('/admin/get/agent/update/report',	'AdminJsonController@getAgentUpdateReport');
Route::get('/admin/get/agent/region/report',	'AdminJsonController@getAgentRegionReport');
Route::get('/admin/get/region/report/callback', 'AdminJsonController@getReportCallback');
Route::post('/admin/search/replace/states',		'AdminJsonController@searchAndReplaceState');
Route::get('/admin/load/allowed/produces',		'AdminJsonController@loadAllowedProduce');
Route::get('/admin/delete-default-products',	'AdminJsonController@deleteAllowedProduce');
Route::post('/admin/update/price/ticker',		'AdminJsonController@updateTickerPrice');
Route::post('/admin/add/allowed/produce',		'AdminJsonController@addAllowedProduce');
Route::get('/admin/get/total/landtype',			'AdminJsonController@getTotalLandByState');
Route::get('/admin/get/estimate/volume',		'AdminJsonController@getEstimateVolume');
Route::get('/admin/fetch/duplicates',			'AdminJsonController@getAllDuplicates');
Route::post('/admin/update/basic/profile',		'AdminJsonController@updateProfile');

// create temp user
Route::post('/admin/create/temp/user',			'AdminJsonController@createTempUser');
Route::post('/admin/update/temp/user',			'AdminJsonController@updateTempUser');
Route::get('/load/all/temp/user', 				'AdminJsonController@loadTempUser');
Route::get('/count/all/temp/user', 				'AdminJsonController@countTempUser');
Route::post('/admin/delete/user', 				'AdminJsonController@deleteTempUser');
Route::post('/admin/delete/active/user',		'AdminJsonController@deleteActiveUser');
Route::post('/admin/remove/image',				'AdminJsonController@removeImage');

// admin deactivated user
Route::post('/admin/deactivate/user', 			'AdminJsonController@deactivatedUser');
Route::post('/admin/activate/user', 			'AdminJsonController@activateUser');

// admin notifications system
Route::get('/admin/notification/count/unread',	'AdminJsonController@getTotalUnread');
Route::get('/admin/notification/count/read',	'AdminJsonController@getTotalRead');
Route::get('/admin/notification/get/all',		'AdminJsonController@getAll');
Route::get('/admin/notification/get/single',	'AdminJsonController@getSingle');


// admin capture famers info
Route::post('/admin/map/farmers/address',		'MapFarmAddressController@fetchByAddress');
Route::post('/admin/map/farmers/latlong',		'MapFarmAddressController@fetchByCordinate');

// admin post
Route::post('/admin/login/request', 			'AdminLoginController@doLogin');
Route::get('/admin/logout/request', 			'AdminHomeController@logout');


/*
|---------------------------------------------------------------------------
| admin sync users manual account
|---------------------------------------------------------------------------
|
*/
Route::post('/admin/sync/user/account', 		'AdminSynchronizationController@syncAccount');
Route::post('/admin/auto-sync/user/account', 	'AdminSynchronizationController@autoSyncAccount');
Route::get('/admin/load/members/group',			'AdminSynchronizationController@showFamersGroup');



/*
|---------------------------------------------------------------------------
| admin group module
|---------------------------------------------------------------------------
|
*/
Route::get('/admin/users/group',				'FarmerGroupController@index');
Route::post('/admin/group/create',				'FarmerGroupController@create');
Route::post('/admin/group/edit',				'FarmerGroupController@edit');
Route::post('/admin/group/delete',				'FarmerGroupController@delete');
Route::get('/admin/all/groups',					'FarmerGroupController@loadAll');
Route::get('/admin/all/groups/users',			'FarmerGroupController@loadGroupUsers');
Route::get('/admin/one/group',					'FarmerGroupController@loadOne');
	


/*
|--------------------------------------------------------------------------
| Shopping Carts Engine
|--------------------------------------------------------------------------
|
| Here is where you can browse throught permitted pages
| Pages are authenticated
|
 */
Route::post('/add/to/cart', 		'ShoppingCartController@addToCart');
Route::get('/load/shopping/carts/', 'ShoppingCartController@loadShoppingCart');
Route::get('/load/shopping/list', 	'ShoppingCartController@shoppingBasket');
Route::get('/remove/item/{id}', 	'ShoppingCartController@removeItem');



/*
|--------------------------------------------------------------------------
| User Guid section
|--------------------------------------------------------------------------
|
| Here is where you can browse throught permitted pages
| Pages are authenticated
|
*/
Route::get('start/user/guide',		'UserGuideController@startGuide');


/*
|--------------------------------------------------------------------------
| Chat Community Controller
|--------------------------------------------------------------------------
|
| Here is where you can browse throught permitted pages
| Pages are authenticated
|
*/
Route::get('/chat/community', 				'ChatCommunityController@index');
Route::get('/chat/community/home', 			'ChatCommunityController@chatHome');
Route::get('/chat/community/home/{name}', 	'ChatCommunityController@groupChat');

/* send  chat messages */
Route::post('/send/chat/messages', 			'ChatResponseController@postChat');
Route::post('/send/group-chat/messages', 	'ChatResponseController@postGroupChat');
Route::get('/load/chat/messages', 			'ChatResponseController@loadChat');
Route::get('/load/chat-group/messages', 	'ChatResponseController@loadGroupChat');
Route::get('/load/chat/users', 				'ChatResponseController@loadChatUsers');
Route::get('/load/group-chat/users', 		'ChatResponseController@loadGroupUsers');
Route::get('/load/users/associations', 		'ChatResponseController@loadAssoc');
Route::get('/join/community', 				'ChatResponseController@joinAssoc');
Route::post('/send/create/request', 		'ChatResponseController@createAssoc');
Route::post('/send/groups/invites/', 		'ChatResponseController@sendInvites');
Route::post('/clear/old/chat/messages',		'ChatResponseController@clearOldChat');


// commodity market
Route::get('/commodity-market', 			'ComoditityTradeController@index');

// All Artisan Commands
Route::get('/migrate', function (){
	Artisan::call('migrate');
	return redirect()->back();
});

// clear config & caches files
Route::get('/clear-config', function (){
	Artisan::call('config:clear');
	Artisan::call('view:clear');	
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	return redirect()->back();
});

// start maintainance
Route::get('/start-mode', function (){
	Artisan::call('down');
	return redirect()->back();
});

// stop maintainance
Route::get('/stop-mode', function (){
	Artisan::call('up');
	return redirect()->back();
});