<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Get total SMS resport
Route::get('/get/total/sms-report', 'SMSReportController@logs');

/*
|--------------------------------------------------------------------------
| Mobile Web - API ENTRY
|--------------------------------------------------------------------------
*/
Route::post('/sync/user/data',		'AndroidMobileWebController@syncData');
Route::get('/clear/sms/queue',		'AndroidMobileWebController@clearQueue');
Route::post('/add/temp/produce',		'AndroidMobileWebController@addTempProduce');

// /*
// |--------------------------------------------------------------------------
// | Backend Authentication URI
// |--------------------------------------------------------------------------
// */
// Route::post('/account/login', 		'WebApiController@loginUser');
// Route::post('/account/signup', 		'WebApiController@doRegistration');
// Route::post('/account/reset',		'WebApiController@resetAccount');
// Route::post('/verify/reset',		'WebApiController@changePassword');
// Route::get('/profile/info/{id}',	'WebApiController@getProfileInformation');

// /*
// |--------------------------------------------------------------------------
// | Dashboard URI
// |--------------------------------------------------------------------------
// */
// Route::get('/get/all/news',			'WebApiController@loadNews');
// Route::get('/get/notifications',	'WebApiController@getNotifications');
// Route::get('/get/produces',			'WebApiController@getProduces');
// Route::get('/get/crops',			'WebApiController@getCrops');


// |--------------------------------------------------------------------------
// | Crop Management
// |--------------------------------------------------------------------------

// Route::post('/add/crop/tag',		'WebApiController@addCropTag');
// Route::post('/remove/crop/tag',		'WebApiController@removeCropTag');
// Route::get('/list/crop/tag',		'WebApiController@listCropTag');

// /*
// |--------------------------------------------------------------------------
// | Blog Management
// |--------------------------------------------------------------------------
// */
// Route::get('/get/blog/posts',		'WebApiController@getBlogPosts');
// Route::post('/blog/add/comment',	'WebApiController@addBlogPostComment');
// Route::get('/blog/get/comments',	'WebApiController@getBogPostComments');

// /*
// |--------------------------------------------------------------------------
// | Market Place URI
// |--------------------------------------------------------------------------
// */
// Route::get('/produce/market',		'WebApiController@produceMarketPlace');
// Route::get('/produce/category',		'WebApiController@categoryList');


/*
|--------------------------------------------------------------------------
| Backend Auto-Deployment Webhook
|--------------------------------------------------------------------------
*/
Route::post('/auto-deploy', function (){
	Artisan::call('git:deploy');
	// return 
	return response()->json(['status' => 'success']);
});

Route::get('/composer-install', function (){
	Artisan::call('composer:install');
	// return 
	return response()->json(['status' => 'success']);
});

Route::post('/run-command', function (Request $request){
	$query = $request->cmd_query;
	if(empty($query)){
		$data = [
			'status' 	=> 'error',
			'message' 	=> 'Please specify input action'
		];
		$status = 401;
	}else{

		// fire command
		Artisan::call('run:command',  [
        	'query' => $query
    	]);

		$data = [
			'status' 	=> 'success',
			'message' 	=> 'Command was successful!'
		];
		$status = 200;
	}

	// return 
	return response()->json($data, $status);
});


// code clean up
Route::post('/bug/sweep', 	'BugSweeperController@cleanODCA');
Route::post('/merge/users',	'MergeAccountController@mergeAccount');


// filtered upload images to cloudinary
Route::get('/clean/images',			'FactoryController@cleanImages');
Route::get('/load/scanned/images',		'FactoryController@alreadyScanned');
Route::get('/load/all/user_id',			'FactoryController@unscannedUsers');
Route::post('/fix/odca/data-error',		'FactoryController@resolveConflict');
Route::post('/resolve/wrong/posting',	'FactoryController@resolveWrongPosting');
Route::post('/resolve/size/issues',		'FactoryController@resolveSizeIssue');
Route::post('/search/replace/lga',		'FactoryController@searchReplaceLga');

// scanned routes
Route::get('/count/base64/found',		'FactoryController@countBase64');
Route::get('/count/base64/found/odca',	'FactoryController@countBase64Odca');
Route::get('/clean/already/scanned',	'FactoryController@cleanAlreadySynchImages');
Route::get('/fetch/all/users/odca',	    'FactoryController@fetchPlatformUsers');
// this is cool